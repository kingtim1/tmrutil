package tmrutil.net;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * A client application which connects to a write server and feeds text data.
 * The text data is written to a file on the server.
 * 
 * @author Timothy Mann
 * 
 */
public class WriteClient
{
	private Socket _socket;
	private ObjectOutputStream _oos;
	
	public WriteClient(String hostname, String fileName, int port, int timeout) throws IOException
	{
		InetSocketAddress saddress = new InetSocketAddress(hostname, port);
		_socket = new Socket();
		_socket.connect(saddress, timeout);
		OutputStream os = _socket.getOutputStream();
		_oos = new ObjectOutputStream(os);
		_oos.writeObject(new File(fileName));
	}
	
	public void write(String data) throws IOException
	{
		_oos.writeObject(data);
	}
	
	public void close() throws IOException
	{
		_oos.writeObject(new WriteServer.EOF());
		_oos.close();
	}
	
	public static void main(String[] args) throws Exception
	{
		WriteClient wc = new WriteClient("localhost", "./test_file.txt", WriteServer.DEFAULT_PORT, 10000);
		wc.write("This is a test!\n");
		Thread.sleep(3000);
		wc.write("This is written after a delay\n");
		wc.close();
	}
}
