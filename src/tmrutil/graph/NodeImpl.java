package tmrutil.graph;

/**
 * Implementation of a node.
 * @author Timothy A. Mann
 *
 * @param <K> the key type
 * @param <T> the content type
 */
public class NodeImpl<K,T> implements Node<K,T>
{
	private K _key;
	private T _content;
	
	public NodeImpl(K key, T content)
	{
		_key = key;
		_content = content;
	}

	@Override
	public K key()
	{
		return _key;
	}

	@Override
	public T content()
	{
		return _content;
	}

	@Override
	public int hashCode()
	{
		return _key.hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Node){
			Node<?,?> n = (Node<?,?>)o;
			return (n.key().equals(_key));
		}else{
			return false;
		}
		
	}
}
