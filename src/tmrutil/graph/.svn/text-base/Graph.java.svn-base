package tmrutil.graph;

import java.util.Set;

/**
 * Represents a graph with a finite number of nodes.
 * @author Timothy A. Mann
 *
 */
public interface Graph<K, N extends Node<K,?>, E extends Edge<N,?>> extends Iterable<E>
{
	/**
	 * Finds a node based on it's key.
	 * @param key a key 
	 * @return the node associated with the specified key or null if no such node is found
	 */
	public N node(K key);
	
	/**
	 * Finds all edges that are associated with a specific node.
	 * @param node a node
	 * @return the set of all edges associated with the specific node
	 */
	public Set<E> edges(N node);
	
	/**
	 * Returns the set of nodes contained by this graph.
	 * @return the set of nodes
	 */
	public Set<N> nodes();
	
	/**
	 * Returns the set of edges contained by this graph.
	 * @return the set of edges
	 */
	public Set<E> edges();
	
	/**
	 * The number of nodes in this graph.
	 * @return the number of nodes in this graph
	 */
	public int numberOfNodes();
	
	/**
	 * The number of edges in this graph.
	 * @return the number of edges
	 */
	public int numberOfEdges();
	
	/**
	 * Returns true if this is a directed graph; otherwise false is returned.
	 * @return true if this is a directed graph; otherwise false
	 */
	public boolean isDirected();
}
