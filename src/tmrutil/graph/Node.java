package tmrutil.graph;

/**
 * Represents a uniquely identifiable point or node in a graph.
 * @author Timothy A. Mann
 *
 * @param <K> the key type
 * @param <T> the content type
 */
public interface Node<K, T>
{
	/**
	 * Returns this node's key.
	 * @return a key
	 */
	public K key();
	
	/**
	 * Returns this node's information content.
	 * @return content
	 */
	public T content();
}
