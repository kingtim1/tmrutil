package tmrutil.learning.anomaly;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.Classifier;
import tmrutil.math.Metric;
import tmrutil.util.Pair;

/**
 * A global distance-based anomaly detector. Given a set of non-anomaly
 * examples, this anomaly detector considers a point to be an anomaly if no
 * observed points are within <code>sigma</code> units of a new instance.
 * 
 * @author Timothy A. Mann
 * 
 * @param <X>
 *            the instance type
 */
public class DistanceBasedAnomalyDetector<X> extends AnomalyDetector<X>
{
	private Metric<X> _metric;
	private List<Pair<X, Integer>> _trainingSet;
	private double _sigma;

	/**
	 * Constructs a global distance-based anomaly detector.
	 * 
	 * @param metric
	 *            a distance metric
	 * @param sigma
	 *            the distance used to determine whether a new instance should
	 *            be considered an outlier
	 */
	public DistanceBasedAnomalyDetector(Metric<X> metric, double sigma)
	{
		super();
		_metric = metric;
		_sigma = sigma;

		_trainingSet = new ArrayList<Pair<X, Integer>>();
	}

	@Override
	public double[] scores(X instance)
	{
		double[] s = new double[NUM_CLASSES];
		boolean anomaly = true;
		for (Pair<X, Integer> tsample : _trainingSet) {
			if (tsample.getB().equals(NOT_ANOMALY_CLASS)) {
				double dist = _metric.distance(instance, tsample.getA());
				if (dist < _sigma) {
					anomaly = false;
					break;
				}
			}
		}
		if (anomaly) {
			s[ANOMALY_CLASS] = 1;
			s[NOT_ANOMALY_CLASS] = 0;
		} else {
			s[ANOMALY_CLASS] = 0;
			s[NOT_ANOMALY_CLASS] = 1;
		}
		return s;
	}

	@Override
	public void train(Collection<Pair<X, Integer>> trainingSet)
	{
		_trainingSet = new ArrayList<Pair<X, Integer>>(trainingSet);
	}

	@Override
	public void trainOnNonAnomalyInstances(Collection<X> nonAnomalyInstances)
	{
		List<Pair<X, Integer>> trainingSet = new ArrayList<Pair<X, Integer>>(
				nonAnomalyInstances.size());
		for (X instance : nonAnomalyInstances) {
			Pair<X, Integer> pInst = new Pair<X, Integer>(instance,
					NOT_ANOMALY_CLASS);
			trainingSet.add(pInst);
		}
		train(trainingSet);
	}

	public void addNonAnomalyInstance(X nonAnomalyInstance)
	{
		Pair<X, Integer> pInst = new Pair<X, Integer>(nonAnomalyInstance,
				NOT_ANOMALY_CLASS);
		_trainingSet.add(pInst);
	}

	@Override
	public DistanceBasedAnomalyDetector<X> newInstance(){
		return new DistanceBasedAnomalyDetector<X>(_metric, _sigma);
	}

}
