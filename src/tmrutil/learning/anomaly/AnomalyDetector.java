package tmrutil.learning.anomaly;

import java.util.Collection;
import tmrutil.learning.Classifier;
import tmrutil.stats.Statistics;

/**
 * Base class for anomaly detection algorithms. Anomaly detection algorithms
 * attempt to identify rare but meaningful instances based on receiving data
 * that is representative of the normal behavior of process under observation.
 * <p/>
 * 
 * This implementation assumes that anomalies belong to the
 * <code>ANOMALY_CLASS</code> (class 0) and normal instances belong to the
 * <code>NOT_ANOMALY_CLASS</code> (class 1).
 * 
 * @author Timothy A. Mann
 * 
 * @param <X>
 *            the instance type to be classified
 */
public abstract class AnomalyDetector<X> implements Classifier<X, Integer>
{
	public static final Integer ANOMALY_CLASS = new Integer(0);
	public static final Integer NOT_ANOMALY_CLASS = new Integer(1);

	public static final int NUM_CLASSES = 2;
	
	public AnomalyDetector()
	{
	}
	
	
	@Override
	public Integer classify(X instance)
	{
		return Statistics.maxIndex(scores(instance));
	}

	/**
	 * Returns the score associated with the anomaly class of the given instance.
	 * 
	 * @param instance
	 *            the instance to be scored
	 * @return the score associated with the given instance
	 */
	public double anomalyScore(X instance)
	{
		return scores(instance)[ANOMALY_CLASS];
	}
	
	public abstract void trainOnNonAnomalyInstances(Collection<X> nonAnomalyInstances);

	@Override
	public int numClasses()
	{
		return NUM_CLASSES;
	}

}
