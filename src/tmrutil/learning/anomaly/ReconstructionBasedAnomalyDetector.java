package tmrutil.learning.anomaly;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.learning.BatchFunctionApproximator;
import tmrutil.learning.Classifier;
import tmrutil.math.Metric;
import tmrutil.util.Pair;

/**
 * A simple reconstruction-based anomaly detector. The key assumption is that
 * non-anomalous instances can be reconstructed accurately, whereas anomalous
 * instances are difficult to reconstruct accurately.
 * 
 * @author Timothy A. Mann
 * 
 * @param <X>
 *            the instance type
 */
public class ReconstructionBasedAnomalyDetector<X> extends AnomalyDetector<X> {
	/**
	 * This function approximator acts as an autocorrelator for non-anomalous
	 * instances.
	 */
	private BatchFunctionApproximator<X, X> _funcApprox;
	/**
	 * This metric is used to calculate the reconstruction error.
	 */
	private Metric<X> _metric;
	private double _reconErrorThreshold;

	public ReconstructionBasedAnomalyDetector(
			BatchFunctionApproximator<X, X> funcApprox, Metric<X> metric,
			double reconErrorThreshold) {
		_funcApprox = funcApprox;
		_metric = metric;
		_reconErrorThreshold = reconErrorThreshold;
	}

	@Override
	public double[] scores(X instance) {
		X reconInstance = _funcApprox.evaluate(instance);
		double reconError = _metric.distance(instance, reconInstance);

		double[] s = new double[NUM_CLASSES];
		s[ANOMALY_CLASS] = Math.max(0, reconError - _reconErrorThreshold);
		s[NOT_ANOMALY_CLASS] = Math.max(0, _reconErrorThreshold - reconError);
		return s;
	}

	@Override
	public void train(Collection<Pair<X, Integer>> trainingSet) {
		Collection<Pair<X, X>> newTrainingSet = new ArrayList<Pair<X, X>>();
		for (Pair<X, Integer> example : trainingSet) {
			if (example.getB().equals(NOT_ANOMALY_CLASS)) {
				Pair<X, X> newExample = new Pair<X, X>(example.getA(),
						example.getA());
				newTrainingSet.add(newExample);
			}
		}
		_funcApprox.train(newTrainingSet);
	}

	@Override
	public void trainOnNonAnomalyInstances(Collection<X> nonAnomalyInstances) {
		List<Pair<X, Integer>> trainingSet = new ArrayList<Pair<X, Integer>>(
				nonAnomalyInstances.size());
		for (X instance : nonAnomalyInstances) {
			Pair<X, Integer> pInst = new Pair<X, Integer>(instance,
					NOT_ANOMALY_CLASS);
			trainingSet.add(pInst);
		}
		train(trainingSet);
	}

	@Override
	public ReconstructionBasedAnomalyDetector<X> newInstance() {
		return new ReconstructionBasedAnomalyDetector<X>(
				_funcApprox.newInstance(), _metric, _reconErrorThreshold);
	}

}
