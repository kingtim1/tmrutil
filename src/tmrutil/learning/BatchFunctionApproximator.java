package tmrutil.learning;

import java.util.Collection;

import tmrutil.util.Factory;
import tmrutil.util.Pair;

/**
 * A function approximator that trains using a large batch of training examples.
 * @author Timothy A. Mann
 *
 * @param <D> the domain type
 * @param <R> the range type
 */
public interface BatchFunctionApproximator<D, R> extends
		FunctionApproximator<D, R>, Factory<BatchFunctionApproximator<D,R>>
{
	/**
	 * Trains the function approximator using a batch of samples.
	 * @param trainingSet a collection of training examples
	 */
	public void train(Collection<Pair<D,R>> trainingSet);
	

}
