package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.math.metrics.EuclideanDistance;
import tmrutil.util.KDTree;
import tmrutil.util.Pair;

/**
 * A batch implementation of locally weighted regression.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BatchLWR implements BatchFunctionApproximator<RealVector, Double> {

	private static final long serialVersionUID = -4716690071734978761L;

	private KDTree<Double> _kdTree;
	private int _inputSize;
	private double _localRadius;
	private double _defaultValue;

	public BatchLWR(int inputSize, double localRadius, double defaultValue) {
		_inputSize = inputSize;
		_localRadius = localRadius;
		_defaultValue = defaultValue;
	}

	@Override
	public void reset() {
		_kdTree = null;
	}

	@Override
	public Double evaluate(RealVector x) throws IllegalArgumentException {
		if (_kdTree == null) {
			return _defaultValue;
		} else {
			List<Pair<double[], Double>> trainingSet = _kdTree.withinRadius(x.toArray(),
					_localRadius);

			BatchLinearApproximator fa = new BatchLinearApproximator(_inputSize);
			try {
				fa.train(fromDA(trainingSet));
				double val = fa.evaluate(x);
				return val;
			} catch (MathIllegalArgumentException ex) {
				return _defaultValue;
			}
		}
	}

	@Override
	public void train(Collection<Pair<RealVector, Double>> trainingSet) {
		_kdTree = new KDTree<Double>(fromRV(trainingSet), _inputSize);
	}

	@Override
	public BatchLWR newInstance() {
		return new BatchLWR(_inputSize, _localRadius, _defaultValue);
	}
	
	private List<Pair<RealVector,Double>> fromDA(Collection<Pair<double[],Double>> samples){
		List<Pair<RealVector,Double>> rvSamples = new ArrayList<Pair<RealVector,Double>>(samples.size());
		
		for(Pair<double[],Double> pair : samples){
			Pair<RealVector,Double> rvPair = new Pair<RealVector,Double>(new ArrayRealVector(pair.getA()), pair.getB());
			rvSamples.add(rvPair);
		}
		
		return rvSamples;
	}
	
	private List<Pair<double[],Double>> fromRV(Collection<Pair<RealVector,Double>> samples){
		List<Pair<double[],Double>> daSamples = new ArrayList<Pair<double[],Double>>(samples.size());
		
		for(Pair<RealVector,Double> pair : samples){
			Pair<double[],Double> daPair = new Pair<double[],Double>(pair.getA().toArray(), pair.getB());
			daSamples.add(daPair);
		}
		
		return daSamples;
	}

}
