package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.util.Precision;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import tmrutil.math.Constants;
import tmrutil.stats.StdEstimator;
import tmrutil.stats.VarianceEstimator;
import tmrutil.stats.vector.ZScore;
import tmrutil.util.Pair;

/**
 * RegressionData represents a set of vector-scalar pairs. The vectors represent
 * inputs and the scalar values represent target values.
 * 
 * @author Timothy A. Mann
 *
 */
public class RegressionData extends AbstractLabeledData<Double> {

	/**
	 * Constructs a regression data instance from a matrix of input vectors and
	 * a vector of target values.
	 * 
	 * @param x
	 *            a matrix where each row corresponds to an input vector
	 * @param y
	 *            a vector of target values corresponding to each row of
	 *            <code>x</code>
	 */
	public RegressionData(RealMatrix x, RealVector y) {
		super(toPairs(x, y));
	}

	/**
	 * Constructs a regression data instance from a collection of vector-target
	 * pairs.
	 * 
	 * @param data
	 *            a collection of vector-target value pairs
	 */
	public RegressionData(Collection<Pair<RealVector, Double>> data) {
		super(data);
	}

	/**
	 * Converts a matrix of input vectors and a vector of target values into a
	 * list of pairs.
	 * 
	 * @param x
	 *            a matrix where each row corresponds to an input vector
	 * @param y
	 *            a vector where each entry corresponds to a label for one row
	 *            in the matrix <code>x</code>
	 * @return a list of vector and label pairs
	 */
	public static final List<Pair<RealVector, Double>> toPairs(RealMatrix x,
			RealVector y) {
		int n = x.getRowDimension();
		if (n != y.getDimension()) {
			throw new IllegalArgumentException("The number of rows in x (" + n
					+ ") must match the dimension of y (" + y.getDimension()
					+ ").");
		}
		List<Pair<RealVector, Double>> data = new ArrayList<Pair<RealVector, Double>>(
				n);

		for (int i = 0; i < n; i++) {
			Pair<RealVector, Double> pair = new Pair<RealVector, Double>(
					x.getRowVector(i), y.getEntry(i));
			data.add(pair);
		}

		return data;
	}

	/**
	 * Returns a new {@link RegressionData} instance where the input vectors
	 * only contain the columns specified by <code>variableIndices</code>.
	 * 
	 * @param variableIndices
	 *            a list of indices of variables to include in the new
	 *            regression data set
	 * @return a new regression data instance with input vectors only containing
	 *         the variables specified by <code>variableIndices</code>
	 */
	public RegressionData subsetOfVariables(List<Integer> variableIndices) {
		List<Pair<RealVector, Double>> newPairs = new ArrayList<Pair<RealVector, Double>>(
				_data.size());
		for (Pair<RealVector, Double> pair : _data) {
			RealVector nv = extractSubset(pair.getA(), variableIndices);
			Pair<RealVector, Double> newPair = new Pair<RealVector, Double>(nv,
					pair.getB());
			newPairs.add(newPair);
		}
		return new RegressionData(newPairs);
	}

	/**
	 * Creates a RegressionData instance from this instance removing any
	 * variables from the input vectors that have zero variance.
	 * 
	 * @return a RegressionData instance with zero variance variables removed
	 */
	public RegressionData removeVariablesWithNoVariance() {
		List<Integer> activeSet = variablesWithNontrivialVariance();
		return subsetOfVariables(activeSet);
	}

	/**
	 * Returns a new regression data instance by projecting the input vectors of
	 * this regression data set using the specified linear transformation.
	 * 
	 * @param linTran
	 *            a linear transformation (where the columns are the same
	 *            dimension as the input vectors and the new dimension will be
	 *            equal to the number of columns)
	 * @return a new regression data set
	 */
	public RegressionData project(RealMatrix linTran) {
		List<Pair<RealVector, Double>> data = new ArrayList<Pair<RealVector, Double>>();
		for (Pair<RealVector, Double> pair : _data) {
			RealVector newX = project(pair.getA(), linTran);
			Pair<RealVector, Double> newPair = new Pair<RealVector, Double>(
					newX, pair.getB());
			data.add(newPair);
		}
		return new RegressionData(data);
	}

}
