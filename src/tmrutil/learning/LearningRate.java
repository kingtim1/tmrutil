package tmrutil.learning;

/**
 * This interface should be implemented by classes that represent a sequence of
 * learning rate or step-size values. The learning rate is a scalar value in the
 * interval [0, 1] used by a learning algorithm (e.g. backpropagation of error,
 * perceptron learning, self-organizing map, etc.) to determine the portion of
 * error that the learning algorithm should correct for. The learning rate may
 * either be constant or decreasing with respect to time.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface LearningRate
{
	/**
	 * Implements a learning rate sequence where the value returned is constant
	 * with respect to time.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class Constant implements LearningRate
	{
		private double _value;

		/**
		 * Constructs a constant learning rate sequence.
		 * @param value the constant value of the learning rate
		 * @throws IllegalArgumentException if <code>value < 0 || value > 1</code>
		 */
		public Constant(double value) throws IllegalArgumentException
		{
			if (value < 0 || value > 1) {
				throw new IllegalArgumentException(
						"The learning rate must be a value in the interval [0, 1], but the value "
								+ value + " was specified.");
			}
			_value = value;
		}

		@Override
		public double initValue()
		{
			return _value;
		}

		@Override
		public boolean isConstant()
		{
			return true;
		}

		@Override
		public boolean isDecreasing()
		{
			return false;
		}

		@Override
		public double value(long timeStep)
		{
			return _value;
		}
	}

	/**
	 * Returns the value of the learning rate, which is a scalar in the interval
	 * [0, 1], at time step 0.
	 * 
	 * @return a scalar value in the interval [0, 1]
	 */
	public double initValue();

	/**
	 * Returns the value of the learning rate, which is a scalar in the interval
	 * [0, 1], at a specified discrete time step.
	 * 
	 * @param timeStep
	 *            a nonnegative integer
	 * @return a scalar value in the interval [0, 1]
	 */
	public double value(long timeStep);

	/**
	 * Returns true if this learning rate sequence is decreasing over time.
	 * 
	 * @return true if the learning rate is decreasing; otherwise it is constant
	 */
	public boolean isDecreasing();

	/**
	 * Returns true if this learning rate sequence is constant over time.
	 * 
	 * @return true if the learning rate is constant over time; otherwise it is
	 *         decreasing
	 */
	public boolean isConstant();
}
