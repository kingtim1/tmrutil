package tmrutil.learning;

/**
 * A density map is used to keep track of the frequency of visits to regions of
 * a continuous space. This provides an interface for all density map classes.
 * The density of visits to a region increases when points are sampled from that
 * region.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface DensityMap
{
	/**
	 * Updates this density map by adding a sample point.
	 * 
	 * @param sample
	 *            a (non-null) sample point
	 * 
	 * @throws NullPointerException
	 *             if <code>sample == null</code>
	 * @throws IllegalArgumentException
	 *             if <code>sample.length != dimension()</code> or the sample
	 *             point is outside of the boundaries of this density map
	 */
	public void update(double[] sample);

	/**
	 * Evaluates the density at an arbitrary input point. The returned density
	 * is a non-negative scalar value.
	 * 
	 * @param input
	 *            an arbitrary (non-null) input
	 * @return a non-negative scalar value representing the density at that
	 *         point
	 * 
	 * @throws NullPointerException
	 *             if <code>input == null</code>
	 * @throws IllegalArgumentException
	 *             if <code>input.length != dimension()</code> or the sample
	 *             point is outside of the boundaries of this density map
	 */
	public double density(double[] input);

	/**
	 * Returns the number of dimensions of the density map and the number of
	 * components in a valid sample vector.
	 * 
	 * @return the number of components in a valid sample vector
	 */
	public int dimension();
}
