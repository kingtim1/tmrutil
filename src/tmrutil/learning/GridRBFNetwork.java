package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.math.Function;
import tmrutil.math.MatrixOps;
import tmrutil.math.RealVectorFunction;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.LogisticFunction;
import tmrutil.stats.Statistics;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * A grid-based radial basis function network is a radial basis function network
 * where the centers are stationary and arranged in a grid along all dimensions
 * of the input space.
 * 
 * @author Timothy A. Mann
 * 
 */
public class GridRBFNetwork extends NNet {
	private static final long serialVersionUID = 6762713704831039898L;

	public static final double DIM_MIN = -1;
	public static final double DIM_MAX = 1;

	/**
	 * A simple Gaussian basis function.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static final class BasisFunction implements
			Function<double[], Double> {
		private static final long serialVersionUID = -7756179024352666547L;
		private double[] _mu;
		private double _variance;
		private double _sixSigma2;

		public BasisFunction(double[] mu, double sigma) {
			_mu = Arrays.copyOf(mu, mu.length);
			_variance = sigma * sigma;
			_sixSigma2 = 6 * 6 * _variance;
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException {
			double d2 = VectorOps.distanceSqd(x, _mu);
			if (d2 < _sixSigma2) {
				double arg = d2 / _variance;
				return Math.exp(-arg);
			} else {
				return 0.0;
			}
		}
	}


	/** A list of basis functions. */
	private List<BasisFunction> _bfuncs;
	/**
	 * A single layer perceptron for incremental learning of the linear function.
	 */
	private SingleLayerPerceptron _slp;
	
	private List<Interval> _hypercube;
	private List<Pair<int[],Double>> _grids;

	/**
	 * Constructs a grid-based RBF network with a specified input size, output
	 * size, number of hidden units, and default basis width.
	 * 
	 * @param hypercube
	 *            determines the shape of the input size
	 * @param outputSize
	 *            the dimension of an output vector
	 * @param numHidden
	 *            the number of hidden units along each row or column of a
	 *            particular dimension
	 * @param lambda the regularization constant to use while updating the parameters
	 * @throws IllegalArgumentException
	 *             if <code>numHidden.length != inputSize</code>
	 */
	public GridRBFNetwork(List<Interval> hypercube, int outputSize,
			List<Pair<int[], Double>> grids, double lambda)
			throws IllegalArgumentException {
		super(hypercube.size(), outputSize);
		_hypercube = hypercube;
		_grids = grids;
		
		double[] dimMins = new double[hypercube.size()];
		double[] dimMaxs = new double[hypercube.size()];
		for (int i = 0; i < hypercube.size(); i++) {
			Interval range = hypercube.get(i);
			dimMins[i] = range.getMin();
			dimMaxs[i] = range.getMax();
		}

		int totalNumHidden = 0;
		for (Pair<int[], Double> g : grids) {
			int[] numHidden = g.getA();
			if (hypercube.size() != numHidden.length) {
				throw new IllegalArgumentException(
						"The number of elements specified in numHidden does not match the specified input size.");
			}

			totalNumHidden += Statistics.product(numHidden);
		}

		_slp = new SingleLayerPerceptron(totalNumHidden + hypercube.size(), outputSize, lambda);
		_bfuncs = new ArrayList<BasisFunction>(totalNumHidden);

		for (Pair<int[], Double> g : grids) {
			int[] numHidden = g.getA();
			double sigma = g.getB();
			List<double[]> grid = VectorOps.computeGrid(dimMins, dimMaxs,
					numHidden);
			for (int i = 0; i < grid.size(); i++) {
				double[] mu = grid.get(i);
				BasisFunction bfunc = new BasisFunction(mu, sigma);
				_bfuncs.add(bfunc);
			}
		}
	}

	/**
	 * Constructs a grid-based RBF network with a specified input size, output
	 * size, number of hidden units, and default basis width.
	 * 
	 * @param inputSize
	 *            the dimension of a valid input vector
	 * @param outputSize
	 *            the dimension of an output vector
	 * @param numHidden
	 *            the number of hidden units along each row or column of a
	 *            particular dimension
	 * @param sigma
	 *            the default basis width
	 * @throws IllegalArgumentException
	 *             if <code>numHidden.length != inputSize</code>
	 */
	public GridRBFNetwork(int inputSize, int outputSize, int[] numHidden,
			double sigma) throws IllegalArgumentException {
		super(inputSize, outputSize);
		if (inputSize != numHidden.length) {
			throw new IllegalArgumentException(
					"The number of elements specified in numHidden does not match the specified input size.");
		}

		int totalNumHidden = Statistics.product(numHidden);
		
		_slp = new SingleLayerPerceptron(totalNumHidden + inputSize, outputSize);

		_bfuncs = new ArrayList<BasisFunction>(totalNumHidden);

		double[] dimMins = VectorOps.multiply(DIM_MIN,
				VectorOps.ones(inputSize));
		double[] dimMaxs = VectorOps.multiply(DIM_MAX,
				VectorOps.ones(inputSize));
		List<double[]> grid = VectorOps
				.computeGrid(dimMins, dimMaxs, numHidden);
		for (int i = 0; i < grid.size(); i++) {
			double[] mu = grid.get(i);
			BasisFunction bfunc = new BasisFunction(mu, sigma);
			_bfuncs.add(bfunc);
		}
	}

	@Override
	public int numberOfWeights() {
		return _slp.numberOfWeights();
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException {
		if (weights.length != numberOfWeights()) {
			throw new IllegalArgumentException(
					"The number of weights specified does not match the number required.");
		}
		_slp.setWeights(weights);
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate) {
		double[] btarget = new double[target.length];
		for(int i=0;i<target.length;i++){
			btarget[i] = target[i];
		}
		
		double[] hidAct = evaluateHidden(input);
		double[] error = _slp.train(hidAct, btarget, learningRate);
		return error;
	}

	public double[] evaluateHidden(double[] input) {
		double[] hidAct = new double[_bfuncs.size() + getInputSize()];
		for (int j = 0; j < _bfuncs.size(); j++) {
			hidAct[j] = _bfuncs.get(j).evaluate(input);
		}
		for(int j = 0; j < getInputSize(); j++){
			hidAct[_bfuncs.size()+j] = input[j];
		}
		return hidAct;
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException {
		if(x.length != getInputSize()){
			throw new IllegalArgumentException("Invalid input x. Wrong number of dimensions.");
		}
		double[] hidAct = evaluateHidden(x);
		result = _slp.evaluate(hidAct, result);
		//result = VectorOps.add(result, _initBias);
		return result;
	}

	@Override
	public GridRBFNetwork newInstance() {
		return new GridRBFNetwork(_hypercube, getOutputSize(), _grids, _slp.getRegularizationPenalty()[0]);
	}

}
