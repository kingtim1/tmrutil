package tmrutil.learning;

import java.util.Arrays;
import tmrutil.math.VectorOps;
import tmrutil.stats.InsufficientSamplesException;
import tmrutil.stats.MeanEstimator;
import tmrutil.stats.StdEstimator;

/**
 * Provides a simple wrapper for any neural network that estimates the mean and
 * standard deviation of each component of the input and output. All of the
 * components of input variables are converted to z-scores before processing in
 * the underlying neural network. Targets are converted to z-scores during
 * training and outputs are translated back from z-scores to their regular form
 * before being returned.
 * 
 * @author Timothy A. Mann
 * 
 */
public class NNetFilter extends NNet
{
	private static final int NUM_ESTIMATE_SAMPLES = 1000;
	private static final double ESTIMATE_LEARNING_RATE = 1.0 / NUM_ESTIMATE_SAMPLES;

	/** The underlying neural network. */
	private NNet _nnet;
	
	/**
	 * Counts the number of times this neural network has been trained.
	 */
	private long _trainCount;

	/** An estimate of the input mean. */
	private double[] _inputMean;
	/** An estimate of the input standard deviation. */
	private double[] _inputStd;
	/** An estimate of the output mean. */
	private double[] _outputMean;
	/** An estimate of the output standard deviation. */
	private double[] _outputStd;

	/**
	 * Constructs a neural network filter around another neural network.
	 * 
	 * @param nnet
	 *            a neural network
	 */
	public NNetFilter(NNet nnet)
	{
		super(nnet.getInputSize(), nnet.getOutputSize());
		
		// Sets the training count to zero
		_trainCount = 0;
		
		// Get the neural network input size
		int inputSize = nnet.getInputSize();
		// Get the neural network output size
		int outputSize = nnet.getOutputSize();
		
		_nnet = nnet;
		// Initialize input mean estimate
		_inputMean = new double[inputSize];
		// Initialize input standard deviation estimate
		_inputStd = VectorOps.ones(inputSize);
		// Initialize output mean estimate
		_outputMean = new double[outputSize];
		// Initialize output standard deviation estimate
		_outputStd = VectorOps.ones(outputSize);
	}

	@Override
	public int numberOfWeights()
	{
		return _nnet.numberOfWeights();
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		_nnet.setWeights(weights);
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		// Update the training count
		_trainCount++;
		
		// Update the input mean and standard deviation estimates
		updateMean(_inputMean, input, ESTIMATE_LEARNING_RATE);
		updateStd(_inputStd, _inputMean, input, ESTIMATE_LEARNING_RATE);

		// Update output mean and standard deviation estimates
		updateMean(_outputMean, target, ESTIMATE_LEARNING_RATE);
		updateStd(_outputStd, _outputMean, target, ESTIMATE_LEARNING_RATE);

		if(_trainCount > NUM_ESTIMATE_SAMPLES){
			double[] zsInput = zscoreInput(input);
			double[] zsTarget = zscoreTarget(target);

			if (VectorOps.containsNaN(zsInput) || VectorOps.containsNaN(zsTarget)) {
				System.err.println("NaN detected during training!");
			}

			return _nnet.train(zsInput, zsTarget, learningRate);
		}else{
			return new double[getOutputSize()];
		}
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		double[] input = zscoreInput(x);
		return unzscoreOutput(_nnet.evaluate(input, result));
	}

	private double[] zscoreInput(double[] x)
	{
		double[] input = Arrays.copyOf(x, x.length);
		for (int i = 0; i < input.length; i++) {
			double mu = _inputMean[i];
			double sigma = _inputStd[i];

			input[i] = (input[i] - mu) / sigma;
		}
		return input;
	}

	private double[] zscoreTarget(double[] target)
	{
		double[] output = Arrays.copyOf(target, target.length);
		for (int i = 0; i < output.length; i++) {
			double mu = _outputMean[i];
			double sigma = _outputStd[i];

			output[i] = (output[i] - mu) / sigma;
		}
		return output;
	}

	private double[] unzscoreOutput(double[] output)
	{
		double[] out = Arrays.copyOf(output, output.length);
		for (int i = 0; i < out.length; i++) {
			double mu = _outputMean[i];
			double sigma = _outputStd[i];

			out[i] = (out[i] * sigma) + mu;
		}
		return out;
	}

	/**
	 * Updates the current estimate of the mean using a sample from the
	 * distribution.
	 * 
	 * @param currentMean
	 *            the current estimate of the mean
	 * @param sample
	 *            a sample drawn from the distribution that we are trying to
	 *            estimate the mean for
	 * @param learningRate
	 *            the learning rate (or step size parameter)
	 */
	private void updateMean(double[] currentMean, double[] sample,
			double learningRate)
	{
		for (int i = 0; i < currentMean.length; i++) {
			double error = sample[i] - currentMean[i];
			if(Double.isNaN(error)){
				System.err.println("Detected NaN while updating mean estimate.");
			}
			currentMean[i] = currentMean[i] + (learningRate * error);
		}
	}

	/**
	 * Updates the current estimate of the standard deviation using a sample
	 * from the distribution.
	 * 
	 * @param currentStd
	 *            the current estimate of the standard deviation
	 * @param currentMean
	 *            the current estimate of the mean
	 * @param sample
	 *            a sample drawn from the distribution that we are trying to
	 *            estimate the standard deviation for
	 * @param learningRate
	 *            the learning rate (or step size parameter)
	 */
	private void updateStd(double[] currentStd, double[] currentMean,
			double[] sample, double learningRate)
	{
		for (int i = 0; i < currentStd.length; i++) {
			double meanError = Math.abs(sample[i] - currentMean[i]);
			double error = meanError - currentStd[i];
			currentStd[i] = currentStd[i] + (learningRate * error);
		}
	}

	@Override
	public NNetFilter newInstance() {
		return new NNetFilter(_nnet.newInstance());
	}
}
