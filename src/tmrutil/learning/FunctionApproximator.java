package tmrutil.learning;

import tmrutil.math.Function;
import tmrutil.util.Factory;

/**
 * Provides an interface for function approximators. A function approximator is
 * a structure that estimates a function based on a hypothesis class and data
 * samples.
 * 
 * @author Timothy A. Mann
 * 
 * @param <D>
 *            the domain type
 * @param <R>
 *            the range type
 */
public interface FunctionApproximator<D, R> extends Function<D, R>
{
	/**
	 * Resets the parameterization of this function approximator to a random or
	 * initial state. This function erases the effects of learning.
	 */
	public void reset();
}
