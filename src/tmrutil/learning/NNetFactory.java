package tmrutil.learning;

import tmrutil.math.VectorOps;
import tmrutil.util.DebugException;

/**
 * An convenient mechanism for constructing common neural network structures.
 * 
 * @author Timothy A. Mann
 * 
 */
public class NNetFactory
{
	private static final double DEFAULT_CMAC_DIM_WIDTH = 0.2;
	
	public static enum NNetType {
		/** single-layer Perceptron (i.e. no hidden units) */
		SLP,
		/** general linear network with no hidden units. */
		GLN,
		/** multi-layer perceptron with 3 hidden units. */
		MLP3,
		/** multi-layer perceptron with 5 hidden units. */
		MLP5,
		/** multi-layer perceptron with 10 hidden units. */
		MLP10,
		/** multi-layer perceptron with 15 hidden units. */
		MLP15,
		/** multi-layer perceptron with 20 hidden units. */
		MLP20,
		/** radial basis function network with 5 bases. */
		RBF5,
		/** radial basis function network with 10 bases. */
		RBF10,
		/** radial basis function network with 15 bases. */
		RBF15,
		/** radial basis function network with 20 bases. */
		RBF20,
		/** radial basis function network with 50 bases. */
		RBF50,
		/** radial basis function network with 100 bases. */
		RBF100,
		/** CMAC with 3 layers. */
		CMAC3,
		/** CMAC with 5 layers. */
		CMAC5,
		/** CMAC with 10 layers. */
		CMAC10,
		/** CMAC with 15 layers. */
		CMAC15,
		/** CMAC with 20 layers. */
		CMAC20
	};

	private NNetType _type;
	private int _inputSize;
	private int _outputSize;

	/**
	 * Constructs a neural network factory that constructs neural networks with
	 * a specified type, input size, and output size.
	 * 
	 * @param type
	 *            the type of neural network that this factor constructs
	 * @param inputSize
	 *            the input size of constructed networks
	 * @param outputSize
	 *            the output size of constructed networks
	 */
	public NNetFactory(NNetType type, int inputSize, int outputSize)
	{
		_type = type;
		_inputSize = inputSize;
		_outputSize = outputSize;
	}

	/**
	 * Constructs a new neural network based on the properties of this factory.
	 * 
	 * @return a newly constructed neural network
	 */
	public NNet constructNNet()
	{
		switch (_type) {
		case SLP:
			return new SingleLayerPerceptron(_inputSize, _outputSize);
		case GLN:
			return new GeneralLinearNetwork(_inputSize, _outputSize);
		case MLP3:
			return new TwoLayerPerceptron(_inputSize, _outputSize, 3);
		case MLP5:
			return new TwoLayerPerceptron(_inputSize, _outputSize, 5);
		case MLP10:
			return new TwoLayerPerceptron(_inputSize, _outputSize, 10);
		case MLP15:
			return new TwoLayerPerceptron(_inputSize, _outputSize, 15);
		case MLP20:
			return new TwoLayerPerceptron(_inputSize, _outputSize, 20);
		case RBF5:
			return new RBFNetwork(_inputSize, _outputSize, 5);
		case RBF10:
			return new RBFNetwork(_inputSize, _outputSize, 10);
		case RBF15:
			return new RBFNetwork(_inputSize, _outputSize, 15);
		case RBF20:
			return new RBFNetwork(_inputSize, _outputSize, 20);
		case RBF50:
			return new RBFNetwork(_inputSize, _outputSize, 50);
		case RBF100:
			return new RBFNetwork(_inputSize, _outputSize, 100);
		case CMAC3:
			return new HCMAC(_inputSize, _outputSize, 3, VectorOps.multiply(
					DEFAULT_CMAC_DIM_WIDTH, VectorOps.ones(3)), 0.0);
		case CMAC5:
			return new HCMAC(_inputSize, _outputSize, 5, VectorOps.multiply(
					DEFAULT_CMAC_DIM_WIDTH, VectorOps.ones(5)), 0.0);
		case CMAC10:
			return new HCMAC(_inputSize, _outputSize, 10, VectorOps.multiply(
					DEFAULT_CMAC_DIM_WIDTH, VectorOps.ones(10)), 0.0);
		case CMAC15:
			return new HCMAC(_inputSize, _outputSize, 15, VectorOps.multiply(
					DEFAULT_CMAC_DIM_WIDTH, VectorOps.ones(15)), 0.0);
		case CMAC20:
			return new HCMAC(_inputSize, _outputSize, 20, VectorOps.multiply(
					DEFAULT_CMAC_DIM_WIDTH, VectorOps.ones(20)), 0.0);
		default:
			throw new DebugException("Unsupported neural network type : "
					+ _type);
		}
	}
}
