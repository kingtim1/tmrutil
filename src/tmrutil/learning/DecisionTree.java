package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import tmrutil.math.Function;
import tmrutil.math.VectorOps;
import tmrutil.stats.Statistics;
import tmrutil.util.DebugException;
import tmrutil.util.FourTuple;
import tmrutil.util.Pair;

/**
 * A decision tree classifies real valued feature vectors by recursively
 * evaluating rules which determine either the next rule or finally the
 * predicted classification for that vector.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DecisionTree<C> implements Function<double[], C>
{
	private static final long serialVersionUID = -2569775641329941412L;

	/**
	 * An enumeration of comparison operators.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	private static enum Operator {
		LESS_THAN, LESS_THAN_OR_EQUAL, EQUAL, GREATER_THAN, GREATER_THAN_OR_EQUAL
	};

	/**
	 * Condition is a class that allows expression of numerical comparisons
	 * based on a particular feature.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	private class Condition
	{
		private Operator _op;
		private double _base;
		private int _index;

		/**
		 * Constructs a comparison condition.
		 * 
		 * @param op
		 *            the comparison operator to use
		 * @param baseline
		 *            the baseline value to compare against
		 * @param index
		 *            the index into the input vector (specifying which feature
		 *            to decide on)
		 */
		public Condition(Operator op, double baseline, int index)
		{
			_op = op;
			_base = baseline;
			_index = index;
		}

		/**
		 * Evaluates the condition returning either true or false.
		 * 
		 * @param x
		 *            the input vector
		 * @return true if the comparison condition evaluates to true and false
		 *         otherwise
		 */
		public boolean evaluate(double[] x)
		{
			double val = x[_index];
			switch (_op) {
			case LESS_THAN:
				return (val < _base);
			case LESS_THAN_OR_EQUAL:
				return (val <= _base);
			case EQUAL:
				return (val == _base);
			case GREATER_THAN:
				return (val > _base);
			case GREATER_THAN_OR_EQUAL:
				return (val >= _base);
			default:
				throw new DebugException("Operator " + _op
						+ " is not supported!");
			}
		}
		
		/**
		 * Splits a list of inputs and class labels based on this condition's rule.
		 * @param inputs a list of input vectors
		 * @param classLabels a list of corresponding class labels
		 * @return a model
		 */
		public Map<C,Double> splitPrior(List<double[]> inputs, List<C> classLabels)
		{
			int initCapacity = inputs.size() / 2 + 10;
			List<C> successLabels = new ArrayList<C>(initCapacity);
			List<C> failureLabels = new ArrayList<C>(initCapacity);
			for(int i=0;i<inputs.size();i++){
				double[] input = inputs.get(i);
				C label = classLabels.get(i);
				if(evaluate(input)){
					successLabels.add(label);
				}else{
					failureLabels.add(label);
				}
			}
			Map<C,Double> model = new HashMap<C,Double>();
			
			C successLabel = max(computePriors(successLabels));
			C failureLabel = max(computePriors(failureLabels));
			
			if(successLabels.equals(failureLabels)){
				model.put(successLabel, 1.0);
			}else{
				model.put(successLabel, successLabels.size() / (double) classLabels.size());
				model.put(failureLabel, failureLabels.size() / (double) classLabels.size());
			}
			return model;
		}

		/**
		 * Splits a list of inputs vectors into a success list and failure
		 * list based on this condition's rule.
		 * 
		 * @param inputs a list of input vectors
		 * @param classLabels a list of corresponding class labels
		 * @return a four-tuple which contains the success list, success class labels, failure list, and failure class labels (in that order)
		 */
		public FourTuple<List<double[]>, List<C>, List<double[]>, List<C>> split(
				List<double[]> inputs, List<C> classLabels)
		{
			int initCapacity = inputs.size() / 2 + 10;
			List<double[]> successList = new ArrayList<double[]>(initCapacity);
			List<double[]> failureList = new ArrayList<double[]>(initCapacity);
			
			List<C> successLabels = new ArrayList<C>(initCapacity);
			List<C> failureLabels = new ArrayList<C>(initCapacity);
			for (int i=0;i<inputs.size();i++) {
				double[] input = inputs.get(i);
				C label = classLabels.get(i);
				if (evaluate(input)) {
					successList.add(input);
					successLabels.add(label);
				} else {
					failureList.add(input);
					failureLabels.add(label);
				}
			}
			return new FourTuple<List<double[]>, List<C>, List<double[]>, List<C>>(successList, successLabels,
					failureList, failureLabels);
		}
	}

	/**
	 * An abstract node class.
	 * 
	 * @author Timothy A. Mann
	 * 
	 * @param <C>
	 *            the classification type
	 */
	private abstract class Node
	{
		public abstract C evaluate(double[] input);
	}

	/**
	 * A decision node evaluates a condition and selects either of two direct
	 * children. The success child is selected if the condition evaluates to
	 * true, and the failure condition is selected if the condition evaluates to
	 * false.
	 * 
	 * @author Timothy A. Mann
	 * 
	 * @param <C>
	 *            the classification type
	 */
	private class DecisionNode extends Node
	{
		private Condition _cond;
		private Node _successNode;
		private Node _failureNode;

		public DecisionNode(Condition cond, Node successNode,
				Node failureNode)
		{
			_cond = cond;
			_successNode = successNode;
			_failureNode = failureNode;
		}

		@Override
		public C evaluate(double[] input)
		{
			if (_cond.evaluate(input)) {
				return _successNode.evaluate(input);
			} else {
				return _failureNode.evaluate(input);
			}
		}

		/**
		 * Sets the node selected when this decision's condition evaluates to
		 * true.
		 * 
		 * @param successNode
		 *            the node selected when this decision's condition evaluates
		 *            to true
		 */
		public void setSuccessNode(Node successNode)
		{
			if (successNode == null) {
				throw new NullPointerException(
						"The child of a decision node cannot be null.");
			}
			_successNode = successNode;
		}

		/**
		 * Returns a reference to the node selected when this decision's
		 * condition evaluates to true.
		 * 
		 * @return a node
		 */
		public Node getSuccessNode()
		{
			return _successNode;
		}

		/**
		 * Sets the node selected when this decision's condition evaluates to
		 * false.
		 * 
		 * @param failureNode
		 *            the node selected when this decision's condition evaluates
		 *            to false
		 */
		public void setFailureNode(Node failureNode)
		{
			if (failureNode == null) {
				throw new NullPointerException(
						"The child of a decision node cannot be null.");
			}
			_failureNode = failureNode;
		}

		/**
		 * Returns a reference to the node selected when this decision's
		 * condition evaluates to false.
		 * 
		 * @return a node
		 */
		public Node getFailureNode()
		{
			return _failureNode;
		}
	}

	/**
	 * A leaf node that contains a prediction.
	 * 
	 * @author Timothy A. Mann
	 * 
	 * @param <C>
	 *            the classification type
	 */
	private class PredictionNode extends Node
	{
		private C _class;

		public PredictionNode(C clss)
		{
			_class = clss;
		}

		@Override
		public C evaluate(double[] input)
		{
			return _class;
		}
	}

	/**
	 * Represents the root node of the decision tree.
	 */
	private Node _root;

	/**
	 * Specifies the a discretization of the input space where each component of
	 * the array specifies the number of bins in each input dimension.
	 */
	private int[] _discretization;
	/** The number of dimensions in a valid input vector. */
	private int _inputSize;

	private double[] _inputMean;
	private double[] _inputStd;

	/**
	 * Constructs a decision tree with a specified input vector dimension.
	 * 
	 * @param discretization
	 *            specifies a discretization of the input space where each
	 *            component of the array specifies the number of bins in each
	 *            input dimension
	 * @param defaultValue
	 *            the default value returned by the untrained decision tree
	 */
	public DecisionTree(int[] discretization, C defaultValue)
	{
		int inputSize = discretization.length;
		_discretization = Arrays.copyOf(discretization, discretization.length);
		_inputSize = inputSize;
		_inputMean = new double[inputSize];
		_inputStd = new double[inputSize];
		_root = new PredictionNode(defaultValue);
	}

	@Override
	public C evaluate(double[] x) throws IllegalArgumentException
	{
		if (x.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The dimension of the input vector does not match the number required.");
		}
		return _root.evaluate(preprocessInput(x));
	}

	/**
	 * Returns the number of dimensions/features in a valid input vector.
	 * 
	 * @return the number of dimensions/features in a valid input vector
	 */
	public int getInputSize()
	{
		return _inputSize;
	}

	/**
	 * Constructs a decision tree from a collection of training samples.
	 * 
	 * @param samples
	 *            a collection of input and class label pairs
	 */
	public void build(Collection<Pair<double[], C>> samples)
	{
		List<double[]> inputs = new ArrayList<double[]>(samples.size());
		List<C> classLabels = new ArrayList<C>(samples.size());

		for (Pair<double[], C> sample : samples) {
			inputs.add(sample.getA());
			classLabels.add(sample.getB());
		}

		build(inputs, classLabels);
	}

	/**
	 * Constructs a decision tree from a list of inputs and corresponding class
	 * labels.
	 * 
	 * @param inputs
	 *            a list of input vectors
	 * @param classLabels
	 *            a list of class labels corresponding to each input vector
	 */
	public void build(List<double[]> inputs, List<C> classLabels)
	{
		if (inputs.size() != classLabels.size()) {
			throw new IllegalArgumentException(
					"The number of inputs does not match the number of class labels.");
		}
		_inputMean = Statistics.vectorMean(inputs);
		_inputStd = Statistics.vectorStd(inputs);

		List<double[]> newInputs = preprocessInput(inputs);
		_root = buildHelper(newInputs, classLabels);
	}

	/**
	 * Constructs a subtree which constructs nodes based on maximizing information gain.
	 * @param inputs a list of inputs
	 * @param classLabels a corresponding list of class labels
	 * @return the root node of a decision subtree
	 */
	private Node buildHelper(List<double[]> inputs, List<C> classLabels)
	{
		// Try all of the conditions at each attribute and determine the information gain
		Map<Condition, Double> conditionMap = new HashMap<Condition, Double>();
		for (int attr=0;attr<_discretization.length;attr++) {
			for (int bnum = 0; bnum < _discretization[attr]; bnum++) {
				double baseline = (bnum * (2.0 / _discretization[attr])) - 1.0;
				Condition cond = new Condition(Operator.LESS_THAN, baseline,
						attr);
				Map<C,Double> splitLabels = cond.splitPrior(inputs, classLabels);
				conditionMap.put(cond, klDivergence(computePriors(classLabels), splitLabels));
			}
		}
		
		// Make a new node based on the condition which maximizes the information gain
		Condition cond = max(conditionMap);
		FourTuple<List<double[]>,List<C>,List<double[]>,List<C>> splitOut = cond.split(inputs, classLabels);
		if(splitOut.getA().size() == inputs.size()){
			return new PredictionNode(max(computePriors(splitOut.getB())));
		}else if(splitOut.getC().size() == inputs.size()){
			return new PredictionNode(max(computePriors(splitOut.getD())));
		}else{
			return new DecisionNode(cond, buildHelper(splitOut.getA(), splitOut.getB()), buildHelper(splitOut.getC(), splitOut.getD()));
		}
	}

	/**
	 * Preprocesses an input vector.
	 * 
	 * @param input
	 *            a raw input vector
	 * @return the preprocessed input vector
	 */
	private double[] preprocessInput(double[] input)
	{
		double[] newInput = VectorOps.subtract(input, _inputMean);
		for (int i = 0; i < getInputSize(); i++) {
			newInput[i] = newInput[i] / _inputStd[i];
		}
		return newInput;
	}

	/**
	 * Preprocesses a list of input vectors.
	 * 
	 * @param inputs
	 *            a list of raw input vectors
	 * @return the preprocessed input vectors
	 */
	private List<double[]> preprocessInput(List<double[]> inputs)
	{
		List<double[]> newInputs = new ArrayList<double[]>(inputs.size());
		for (double[] input : inputs) {
			newInputs.add(preprocessInput(input));
		}
		return newInputs;
	}

	/**
	 * Computes the prior probabilities for a collection of class labels.
	 * 
	 * @param classLabels
	 *            a collection of class labels
	 * @return a map of prior probabilities for each encountered class label
	 */
	private Map<C, Double> computePriors(Collection<C> classLabels)
	{
		Map<C, Double> priors = new HashMap<C, Double>();
		double inc = 1.0 / classLabels.size();
		for (C label : classLabels) {
			if (priors.containsKey(label)) {
				priors.put(label, priors.get(label) + inc);
			} else {
				priors.put(label, inc);
			}
		}

		return priors;
	}

	/**
	 * Returns the first encountered entry that maps to the maximum value.
	 * 
	 * @param tMap
	 *            a map of labels to values
	 * @return the first encountered entry that maps to the maximum value
	 */
	private static final <T> T max(Map<T, Double> tMap)
	{
		T maxT = null;
		double maxVal = 0;
		Set<T> keys = tMap.keySet();
		for (T key : keys) {
			double val = tMap.get(key);
			if (maxT == null || maxVal < val) {
				maxT = key;
				maxVal = val;
			}
		}
		return maxT;
	}

	/**
	 * Approximates the Kullback-Leibler divergence (or information gain) given
	 * true class frequencies and a model of class frequencies.
	 * 
	 * @param trueMap
	 *            class frequencies computed based on true labeled training data
	 * @param modelMap
	 *            class frequencies computed based on a hypothesized condition
	 * @return the Kullback-Leibler divergence
	 */
	private double klDivergence(Map<C, Double> trueMap, Map<C, Double> modelMap)
	{
		double kld = 0;
		Set<C> keys = new HashSet<C>(trueMap.keySet());
		keys.addAll(modelMap.keySet());
		
		for (C key : keys) {
			Double pi = trueMap.get(key);
			Double qi = modelMap.get(key);
			if(pi == null){
				pi = new Double(0.0);
			}
			if(qi == null || qi.equals(0.0)){
				qi = new Double(0.0);
			}else{
				kld = kld + (pi * Math.log(pi / qi));
			}
		}
		return kld;
	}
	
	public static void main(String[] args)
	{
		List<double[]> inputs = new ArrayList<double[]>();
		List<Integer> classLabels = new ArrayList<Integer>();
		inputs.add(new double[]{0, 0});
		classLabels.add(1);
		inputs.add(new double[]{0, 1});
		classLabels.add(0);
		inputs.add(new double[]{1, 0});
		classLabels.add(0);
		inputs.add(new double[]{1, 1});
		classLabels.add(1);
		
		DecisionTree<Integer> dtree = new DecisionTree<Integer>(new int[]{3, 3}, 0);
		
		System.out.println("Before Training : ");
		for(int i=0;i<inputs.size();i++){
			double[] input = inputs.get(i);
			System.out.println(VectorOps.toString(input) + " ==> " + dtree.evaluate(input));
		}
		
		dtree.build(inputs, classLabels);
		
		System.out.println("After Training : ");
		for(int i=0;i<inputs.size();i++){
			double[] input = inputs.get(i);
			System.out.println(VectorOps.toString(input) + " ==> " + dtree.evaluate(input));
		}
	}
}
