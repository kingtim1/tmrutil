package tmrutil.learning;

import java.util.Arrays;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.HyperbolicTangent;

/**
 * Implements a two layer perceptron where the output layer can be conveniently
 * biased.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BiasTwoLayerPerceptron extends NNet
{
	private static final long serialVersionUID = 681716927444035643L;
	private TwoLayerPerceptron _nnet;
	private double[] _outputBias;

	/**
	 * Constructs a biased two layer perceptron with zero weights.
	 * @param inputSize the number of components in a valid input vector
	 * @param outputSize the number of components in an output vector
	 * @param numHiddenUnits the number of hidden units 
	 * @param outputBias the output bias vector
	 */
	public BiasTwoLayerPerceptron(int inputSize, int outputSize,
			int numHiddenUnits, double[] outputBias)
	{
		super(inputSize, outputSize);
		_nnet = new TwoLayerPerceptron(new HyperbolicTangent(), inputSize, outputSize, numHiddenUnits, true);
		if (outputSize != outputBias.length) {
			throw new IllegalArgumentException(
					"The number of components in the output bias vector does " +
					"not match the number of output units of this neural network.");
		}
		_outputBias = Arrays.copyOf(outputBias, outputBias.length);
	}

	@Override
	public int numberOfWeights()
	{
		return _nnet.numberOfWeights() + _outputBias.length;
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		for(int i=0;i<getOutputSize();i++){
			_outputBias[i] = weights[i];
		}
		double[] leftOverWeights = new double[weights.length - getOutputSize()];
		for(int i=0;i<leftOverWeights.length;i++){
			leftOverWeights[i] = weights[i + getOutputSize()];
		}
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		double[] biasTarget = VectorOps.subtract(target, _outputBias);
		return _nnet.train(input, biasTarget, learningRate);
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		double[] output = _nnet.evaluate(x, result);
		result = VectorOps.add(output, result, result);
		return result;
	}

	@Override
	public BiasTwoLayerPerceptron newInstance() {
		return new BiasTwoLayerPerceptron(getInputSize(), getOutputSize(), this._nnet.getHiddenSize(), _outputBias);
	}

}
