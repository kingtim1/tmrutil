package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import tmrutil.util.Pair;

/**
 * A wrapper class that constructs a {@link BatchFunctionApproximator} from an
 * {@link IncrementalFunctionApproximator}.
 * 
 * @author Timothy A. Mann
 * 
 * @param <D>
 *            the domain type
 * @param <R>
 *            the range type
 */
public class BatchFromIncrementalFunctionApproximator<D, R> implements
		BatchFunctionApproximator<D, R> {

	private static final long serialVersionUID = -6090097693132921583L;

	private IncrementalFunctionApproximator<D, R> _ifa;
	private LearningRate _learningRate;
	private int _numPhases;

	/**
	 * Constructs a batch function approximator from an incremental function
	 * approximator.
	 * 
	 * @param ifa
	 *            the incremental function approximator
	 * @param learningRate
	 *            the learning rate to use during training (where the time is
	 *            determined by the current phase)
	 * @param numPhases
	 *            the number of phases to iterate over the training data
	 */
	public BatchFromIncrementalFunctionApproximator(
			IncrementalFunctionApproximator<D, R> ifa,
			LearningRate learningRate, int numPhases) {
		_ifa = ifa;
		_learningRate = learningRate;
		_numPhases = numPhases;
	}

	@Override
	public void reset() {
		_ifa.reset();
	}

	@Override
	public R evaluate(D x) throws IllegalArgumentException {
		return _ifa.evaluate(x);
	}

	@Override
	public void train(Collection<Pair<D, R>> trainingSet) {
		List<Pair<D, R>> samples = new ArrayList<Pair<D, R>>(trainingSet);
		for (int p = 0; p < _numPhases; p++) {
			Collections.shuffle(samples);
			for (Pair<D, R> sample : samples) {
				D input = sample.getA();
				R target = sample.getB();
				_ifa.train(input, target, _learningRate.value(p));
			}
		}
	}

	public static IncrementalFunctionApproximator<double[], Double> multiToSingleOutput(
			IncrementalFunctionApproximator<double[], double[]> ifa, int outputSize,
			int outputIndex) {
		return new SingleOutputWrapper(ifa, outputSize, outputIndex);
	}
	
	public static class SingleOutputWrapper implements IncrementalFunctionApproximator<double[], Double> {
		private IncrementalFunctionApproximator<double[],double[]> _ifa;
		private int _outputSize;
		private int _outputIndex;
		
		public SingleOutputWrapper(IncrementalFunctionApproximator<double[],double[]> ifa, int outputSize, int outputIndex){
			_ifa = ifa;
			_outputSize = outputSize;
			_outputIndex = outputIndex;
		}

		@Override
		public void reset() {
			_ifa.reset();
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException {
			return _ifa.evaluate(x)[_outputIndex];
		}

		@Override
		public Double train(double[] input, Double target,
				double learningRate) {
			double[] atarget = new double[_outputSize];
			atarget[_outputIndex] = target;
			double[] error = _ifa.train(input, atarget, learningRate);
			if(error == null){
				return null;
			}else{
				return error[_outputIndex];
			}
		}

		@Override
		public IncrementalFunctionApproximator<double[], Double> newInstance() {
			return new SingleOutputWrapper(_ifa, _outputSize, _outputIndex);
		}

	}

	@Override
	public BatchFunctionApproximator<D, R> newInstance() {
		return new BatchFromIncrementalFunctionApproximator<D, R>(_ifa, _learningRate, _numPhases);
	};

}
