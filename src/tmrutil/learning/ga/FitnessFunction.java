/*
 * FitnessFunction.java
 */

/* Package */
package tmrutil.learning.ga;


/* Imports */

/**
   Represents a fitness function which is used to determine the fitness score of a particular chromosome.
*/
public interface FitnessFunction<T>
{
   /**
    *  Returns the fitness score of the individual. A larger number is a better fitness score.
    */
    public double evaluate(Individual<T> individual);
}