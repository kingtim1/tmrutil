package tmrutil.learning.ga;

import tmrutil.xml.FromXML;
import tmrutil.xml.ToXML;

public interface GeneticOperators<T> extends ToXML<T>, FromXML<T>
{
	/** The default crossover probability. */
	public static final double DEFAULT_CROSSOVER_RATE = 0.5;
	/** The default mutation probability. */
	public static final double DEFAULT_MUTATION_RATE = 0.02;
	
	/**
	 * Implements crossing over for two objects of type <code>T</code>
	 * @param p1 a parent
	 * @param p2 a parent
	 * @return a child created from <code>p1</code> and <code>p2</code>
	 */
	public T crossover(T p1, T p2);
	
	/**
	 * Creates a new child by mutating a parent.
	 * @param p a parent
	 * @return a child derived from <code>p</code> by mutation
	 */
	public T mutate(T p);
	
	/**
	 * Generates a random chromosome.
	 * @return a random chromosome
	 */
	public T generate();
}
