package tmrutil.learning.ga;

import java.util.Arrays;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tmrutil.stats.Random;

public class BinaryArrayGeneticOperators implements GeneticOperators<boolean[]>
{
	
	public static final String BIN_ARRAY_ELM = GAXML.NS + ":BIN_ARRAY";
	public static final String BIN_ELM = GAXML.NS + ":BIN";
	public static final String VALUE_ATTR = GAXML.NS + ":VALUE";

	/**
	 * The number of elements of the double array.
	 */
	private int _dim;
	/**
	 * The probability of crossover.
	 */
	private double _cr;
	/**
	 * The probability of mutation.
	 */
	private double _mr;
	
	/**
	 * The probability that a bit in a generated array will be true.
	 */
	private double _trueProb;
	
	public BinaryArrayGeneticOperators(int dim, double trueProb)
	{
		this(dim, DEFAULT_CROSSOVER_RATE, DEFAULT_MUTATION_RATE, trueProb);
	}
	
	public BinaryArrayGeneticOperators(int dim, double crossoverRate, double mutationRate, double trueProb)
	{
		_dim = dim;
		_cr = crossoverRate;
		_mr = mutationRate;
		_trueProb = trueProb;
	}

	@Override
	public Element toXML(boolean[] t, Document document)
	{
		Element element = document.createElement(BIN_ARRAY_ELM);
		for (int i = 0; i < t.length; i++) {
			Element intElm = document.createElement(BIN_ELM);
			intElm.setAttribute(VALUE_ATTR, String.valueOf(t[i]));
			element.appendChild(intElm);
		}
		return element;
	}

	@Override
	public boolean[] fromXML(Element element) throws IllegalArgumentException
	{
		boolean[] t = new boolean[_dim];
		NodeList children = element.getChildNodes();
		int ind = 0;
		try {
			for (int i = 0; i < children.getLength(); i++) {
				Node node = children.item(i);
				if (node instanceof Element) {
					Element intElm = (Element) node;
					t[ind++] = Boolean.parseBoolean(intElm.getAttribute(VALUE_ATTR));
				}
			}
		} catch (IndexOutOfBoundsException ex) {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
		if (ind != _dim) {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
		return t;
	}

	@Override
	public boolean[] crossover(boolean[] p1, boolean[] p2)
	{
		if(p1.length != _dim || p2.length != _dim){
			throw new IllegalArgumentException("The length of both parent genomes must be " + _dim + ".");
		}
		boolean[] child = new boolean[p1.length];
		if (tmrutil.stats.Random.uniform() < _cr) {
			for(int i=0;i<child.length;i++){
				if(Random.nextBoolean()){
					child[i] = p1[i];
				}else{
					child[i] = p2[i];
				}
			}
		} else {
			System.arraycopy(p1, 0, child, 0, p1.length);
		}
		return child;
	}

	@Override
	public boolean[] mutate(boolean[] p)
	{
		if(p.length != _dim){
			throw new IllegalArgumentException("The length of the parent genome must be " + _dim + ".");
		}
		boolean[] child = Arrays.copyOf(p, p.length);
		for (int i = 0; i < p.length; i++) {
			if (tmrutil.stats.Random.uniform() < _mr) {
				child[i] = generateValue();
			}
		}
		return child;
	}

	@Override
	public boolean[] generate()
	{
		boolean[] genome = new boolean[_dim];
		for(int i=0;i<_dim;i++){
			genome[i] = generateValue();
		}
		return genome;
	}
	
	private boolean generateValue()
	{
		double r = Random.uniform();
		return r < _trueProb;
	}

}
