package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.math.DimensionMismatchException;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;

/**
 * An implementation of a Cerebellar Model Arithmetic Computer (CMAC) using
 * hypercube tilings. This implementation does not require the user to specify a
 * subset of the real-vector space as the valid input space because the
 * underlying hypercube tilings can associate any coordinate in the the
 * real-vector hyperspace with a value.
 * 
 * @author Timothy A. Mann
 * 
 */
public class HCMAC extends NNet {
	private static final long serialVersionUID = 3235809684540764714L;

	/** The default size of the randomly generated hash table. */
	public static final int DEFAULT_TABLE_SIZE = 200;
	/**
	 * The default maximum number of elements that can be stored in each tiling.
	 */
	public static final int DEFAULT_MEMORY_SIZE = 10000;

	/** A list of tilings. */
	private List<HypercubeTiling<double[]>> _tilings;
	/** The generalization parameter determining the number of tilings. */
	private int _numTilings;
	/** A bias added to the output. */
	private double _bias;
	private double _initBias;

	private double[] _hypercubeDims;
	private int _memorySize;
	private int _tableSize;

	/**
	 * Constructs a CMAC neural network with specified input and output
	 * dimensions.
	 * 
	 * @param inputSize
	 *            the number of dimensions in a valid input vector
	 * @param outputSize
	 *            the number of dimensions in an output vector
	 * @param numTilings
	 *            the number of tilings of the input space to use
	 * @param hypercubeDimensions
	 *            the dimensions of the hypercubes used to partition the input
	 *            space
	 * @param initBias
	 *            the initial value for all tiles in all tilings
	 * @param memorySize
	 *            the maximum number of entries stored in each hypercube tiling
	 * @param tableSize
	 *            the number of random numbers generated for hashing in each
	 *            tiling
	 */
	public HCMAC(int inputSize, int outputSize, int numTilings,
			double[] hypercubeDimensions, double initBias, int memorySize,
			int tableSize) {
		super(inputSize, outputSize);
		if (inputSize != hypercubeDimensions.length) {
			throw new DimensionMismatchException(
					"The input size must match the number of elements in the hypercube dimensions array.");
		}
		if (numTilings < 1) {
			throw new IllegalArgumentException(
					"The number of tilings must be positive.");
		}
		_initBias = initBias;
		_bias = initBias;

		_hypercubeDims = Arrays.copyOf(hypercubeDimensions,
				hypercubeDimensions.length);
		_memorySize = memorySize;
		_tableSize = tableSize;

		_numTilings = numTilings;
		_tilings = new ArrayList<HypercubeTiling<double[]>>(numTilings);
		for (int i = 0; i < numTilings; i++) {

			// Construct the offset vector
			double[] offset = new double[inputSize];
			for (int j = 0; j < inputSize; j++) {
				offset[j] = Random.uniform(-hypercubeDimensions[j],
						hypercubeDimensions[j]);
			}

			// Construct the hypercube tiling and add it to the list of tilings
			HypercubeTiling<double[]> tiling = new HypercubeTiling<double[]>(
					hypercubeDimensions, offset, tableSize, memorySize);
			_tilings.add(tiling);
		}
	}

	/**
	 * Constructs a CMAC neural network with specified input and output
	 * dimensions.
	 * 
	 * @param inputSize
	 *            the number of dimensions in a valid input vector
	 * @param outputSize
	 *            the number of dimensions in an output vector
	 * @param numTilings
	 *            the number of tilings of the input space to use
	 * @param hypercubeDimensions
	 *            the dimensions of the hypercubes used to partition the input
	 *            space
	 * @param initBias
	 *            the initial value for all tiles in all tilings
	 */
	public HCMAC(int inputSize, int outputSize, int numTilings,
			double[] hypercubeDimensions, double initBias) {
		this(inputSize, outputSize, numTilings, hypercubeDimensions, initBias,
				DEFAULT_MEMORY_SIZE, DEFAULT_TABLE_SIZE);
	}

	/**
	 * Constructs a CMAC neural network with specified input and output
	 * dimensions.
	 * 
	 * @param inputSize
	 *            the number of dimensions in a valid input vector
	 * @param outputSize
	 *            the number of dimensions in an output vector
	 * @param numTilings
	 *            the number of tilings of the input space to use
	 * @param hypercubeDimensions
	 *            the dimensions of the hypercubes used to partition the input
	 *            space
	 */
	public HCMAC(int inputSize, int outputSize, int numTilings,
			double[] hypercubeDimensions) {
		this(inputSize, outputSize, numTilings, hypercubeDimensions, 0,
				DEFAULT_MEMORY_SIZE, DEFAULT_TABLE_SIZE);
	}

	@Override
	public int numberOfWeights() {
		int numWeights = 0;
		for (int i = 0; i < _numTilings; i++) {
			numWeights += _tilings.get(i).maxMemory() * getOutputSize();
		}
		return numWeights;
	}

	public void reset() {
		_tilings.clear();
		for (int i = 0; i < _numTilings; i++) {

			// Construct the offset vector
			double[] offset = new double[getInputSize()];
			for (int j = 0; j < getInputSize(); j++) {
				offset[j] = Random.uniform(-_hypercubeDims[j],
						_hypercubeDims[j]);
			}

			// Construct the hypercube tiling and add it to the list of tilings
			HypercubeTiling<double[]> tiling = new HypercubeTiling<double[]>(
					_hypercubeDims, offset, _tableSize, _memorySize);
			_tilings.add(tiling);
		}
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException {
		throw new UnsupportedOperationException(
				"The setWeights() operation is not supported by "
						+ getClass().getName());
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate) {
		double[] output = evaluate(input);
		double[] error = VectorOps.subtract(target, output);

		for (int j = 0; j < _numTilings; j++) {
			HypercubeTiling<double[]> tiling = _tilings.get(j);
			int[] tileCoord = tiling.findTile(input);
			double[] tileData = tiling.findTileData(tileCoord);
			if (tileData == null) {
				tileData = new double[getOutputSize()];
			}
			for (int i = 0; i < getOutputSize(); i++) {
				tileData[i] += learningRate * error[i];
			}
			tiling.setTileData(tileCoord, tileData);
		}

		return error;
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException {
		// Set the result vector to the bias value
		for (int i = 0; i < getOutputSize(); i++) {
			result[i] = _numTilings * _bias;
		}

		// Sum up the values in the result vector
		for (int j = 0; j < _numTilings; j++) {
			double[] tileData = _tilings.get(j).findTileData(x);
			if (tileData != null) {
				for (int i = 0; i < getOutputSize(); i++) {
					result[i] += tileData[i];
				}
			}
		}

		// Divide the result vector by the number of tilings to obtain the
		// average
		return VectorOps.divide(result, _numTilings, result);
	}

	/**
	 * Sets the value of x to the result exactly. This can have a serious impact
	 * on the values of other nearby inputs.
	 * 
	 * @param x
	 *            an input vector
	 * @param result
	 *            the desired result vector
	 */
	public void set(double[] x, double[] result) {
		double[] resultMinusBias = new double[result.length];
		for (int i = 0; i < result.length; i++) {
			resultMinusBias[i] = result[i] - _bias;
		}

		for (int j = 0; j < _numTilings; j++) {
			_tilings.get(j).setTileData(x, resultMinusBias);
		}
	}

	@Override
	public HCMAC newInstance() {
		return new HCMAC(getInputSize(), getOutputSize(), _numTilings,
				_hypercubeDims, _initBias, _memorySize, _tableSize);
	}
}
