package tmrutil.learning;

import java.util.Arrays;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.stats.Statistics;
import tmrutil.util.Interval;

/**
 * A naive nearest neighbor function approximator.
 * @author Timothy A. Mann
 *
 */
public class NaiveApproximator extends NNet
{
	private static final long serialVersionUID = -594792409950744074L;
	
	private int[] _input;
	private NNet[] _nnets;
	private int _numWeights;
	private Interval[] _bounds;

	public NaiveApproximator(int[] input, int outputSize)
	{
		this(input, defaultBounds(input.length), outputSize);
	}
	
	public NaiveApproximator(int[] input, Interval[] bounds, int outputSize)
	{
		super(input.length, outputSize);
		_input = Arrays.copyOf(input, input.length);
		_bounds = bounds;
		int prod = Statistics.product(input);
		_nnets = new NNet[prod];
		for(int i=0;i<prod;i++){
			_nnets[i] = new SingleLayerPerceptron(input.length, outputSize);
		}
		_numWeights = outputSize * prod;
	}

	@Override
	public int numberOfWeights()
	{
		return _numWeights;
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		if(numberOfWeights() != weights.length){
			throw new IllegalArgumentException("The number of weights specified does not match the number required.");
		}
		//_weights = MatrixOps.reshape(weights, getOutputSize(), Statistics.product(_input));
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		int index = findIndex(input);
		//double[] output = evaluate(input);
		//double[] error = VectorOps.subtract(target, output);
		
		//for(int j=0;j<getOutputSize();j++){
		//	_weights[j][index] += learningRate * error[j];
		//}
		double[] error = _nnets[index].train(input, target, learningRate);
		
		return error;
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		int index = findIndex(x);
//		for(int j=0;j<getOutputSize();j++){
//			result[j] = _weights[j][index];
//		}
		
		_nnets[index].evaluate(x, result);
		return result;
	}

	private int findIndex(double[] x)
	{
		int[] coord = findTile(x);
		int index = 0;
		
		int prod = Statistics.product(_input);
		for(int i=0;i<getInputSize();i++){
			prod = prod / _input[i];
			index += prod * coord[i];
		}
		
		return index;
	}
	
	private int[] findTile(double[] x) throws IllegalArgumentException
	{
		if (x.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The number of dimensions of the input vector does not match the number required.");
		}

		int[] coord = new int[getInputSize()];

		// Convert each element of the vector into an integer
		for (int i = 0; i < getInputSize(); i++) {
			double min = _bounds[i].getMin();
			double max = _bounds[i].getMax();
			double diff = max - min;
			double ratio = (x[i] - min)/diff;
			double inc = diff / _input[i];
			for(int d=0;d<_input[i];d++){
				if(ratio < (d * inc)){
					coord[i] = d;
					break;
				}
			}
			
			if(ratio < 0){
				coord[i] = 0;
			}
			if(ratio >= 1){
				coord[i] = _input[i]-1;
			}
		}

		return coord;
	}
	
	private static final Interval[] defaultBounds(int numDimensions){
		Interval[] bounds = new Interval[numDimensions];
		for(int i=0;i<numDimensions;i++){
			bounds[i] = new Interval(-1, 1);
		}
		return bounds;
	}

	@Override
	public NaiveApproximator newInstance() {
		return new NaiveApproximator(_input, _bounds, getOutputSize());
	}
}
