package tmrutil.learning;

import java.util.ArrayList;
import java.util.List;
import tmrutil.math.VectorOps;

/**
 * A basic density map that uses radial basis functions to generalize density to
 * sample points that have not been visited.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RadialBasisDensityMap implements DensityMap
{
	/**
	 * A list of samples.
	 */
	private List<double[]> _samples;

	/**
	 * The dimension of valid sample vectors.
	 */
	private int _dim;

	/**
	 * The generalization parameter.
	 */
	private double _sigma;

	/**
	 * Constructs an instance of a radial basis density map.
	 * 
	 * @param sigma
	 *            the value of the generalization parameter
	 */
	public RadialBasisDensityMap(int dimension, double sigma)
	{
		_samples = new ArrayList<double[]>();
		setGeneralization(sigma);

		if (dimension < 1) {
			throw new IllegalArgumentException("Dimension must be positive.");
		} else {
			_dim = dimension;
		}
	}

	@Override
	public double density(double[] input)
	{
		double sum = 0;
		double sigma2 = _sigma * _sigma;
		for (int i = 0; i < _samples.size(); i++) {
			double[] sample = _samples.get(i);
			double d2 = VectorOps.distanceSqd(input, sample);
			sum += Math.exp(-d2 / sigma2);
		}
		return sum;
	}

	@Override
	public int dimension()
	{
		return _dim;
	}

	@Override
	public void update(double[] sample)
	{
		if (isSampleAccepted(sample)) {
			_samples.add(sample);
		}
	}

	/**
	 * Determines whether new samples will be added to the set of stored
	 * samples.
	 * 
	 * @param sample
	 *            a non-null sample
	 * @return true if the sample should be added; otherwise the sample should
	 *         be discarded
	 */
	public boolean isSampleAccepted(double[] sample)
	{
		return true;
	}

	/**
	 * Sets the value of the generalization parameter.
	 * 
	 * @param sigma
	 *            the positive value to set the generalization parameter to
	 * @throws IllegalArgumentException
	 *             if <code>sigma <= 0</code>
	 */
	public void setGeneralization(double sigma)
	{
		if (sigma <= 0) {
			throw new IllegalArgumentException(
					"The generalization parameter must be positive.");
		}
		_sigma = sigma;
	}

	/**
	 * Returns the value of the generalization parameter.
	 * 
	 * @return the value of the generalization parameter
	 */
	public double getGeneralization()
	{
		return _sigma;
	}
}
