package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tmrutil.math.Metric;
import tmrutil.math.metrics.EuclideanDistance;
import tmrutil.util.Pair;

/**
 * Implements simple cross-validation procedures.
 * 
 * @author Timothy A. Mann
 * 
 * @param <D>
 *            the domain type
 * @param <R>
 *            the range type
 */
public class CrossValidation<D, R> {

	/**
	 * Represents the results of running a cross-validation test.
	 * 
	 * @author Timothy A. Mann
	 * 
	 * @param <D>
	 *            the domain type
	 * @param <R>
	 *            the range type
	 */
	public static class CrossValidationResult<D, R> {
		private Map<String, Double> _scores;
		private List<String> _modelLabels;
		private Map<String, BatchFunctionApproximator<D, R>> _models;

		public CrossValidationResult(List<String> modelLabels,
				Map<String, BatchFunctionApproximator<D, R>> models,
				Map<String, Double> scores) {
			_modelLabels = modelLabels;
			_models = models;
			_scores = scores;
		}

		/**
		 * Returns the scores associated with each model.
		 * 
		 * @return a map of model scores
		 */
		public Map<String, Double> scores() {
			return _scores;
		}

		/**
		 * Returns the score associated with the model at the specified key.
		 * 
		 * @param key
		 *            the key of a model
		 * @return the score associated with the given key
		 */
		public double score(String key) {
			return _scores.get(key);
		}
		
		/**
		 * Returns the score associated with the model at the specified index.
		 * @param index the index of a model
		 * @return the score associated with the given index
		 */
		public double score(int index)
		{
			return _scores.get(_modelLabels.get(index));
		}

		/**
		 * Returns the index of the first model with the lowest score.
		 * 
		 * @return the index of the best model
		 */
		public int bestModelIndex() {
			Integer bestIndex = null;
			double bestScore = 0;
			for (int i = 0; i < _modelLabels.size(); i++) {
				double s = _scores.get(_modelLabels.get(i));
				if (bestIndex == null || s < bestScore) {
					bestIndex = i;
					bestScore = s;
				}
			}
			return bestIndex;
		}

		/**
		 * Returns the key of the first model with the lowest score.
		 * 
		 * @return the key of the best model
		 */
		public String bestModelKey() {
			String bestKey = null;
			double bestScore = 0;
			for (String key : _scores.keySet()) {
				double s = _scores.get(key);
				if (bestKey == null || s < bestScore) {
					bestKey = key;
					bestScore = s;
				}
			}
			return bestKey;
		}
		
		/**
		 * Returns the String label associated with the model at a given index.
		 * @param index the index of a model
		 * @return the String label of the model at the given index
		 */
		public String modelKey(int index)
		{
			return _modelLabels.get(index);
		}

		/**
		 * Returns the model with the lowest score. If there are ties in the
		 * score, then the model with the lowest index is returned.
		 * 
		 * @return the best model
		 */
		public BatchFunctionApproximator<D, R> bestModel() {
			String key = bestModelKey();
			return _models.get(key);
		}

		/**
		 * Returns the number of models that were compared.
		 * 
		 * @return the number of models
		 */
		public int numberOfModels() {
			return _models.size();
		}
	}

	/**
	 * A metric for calculating the error incurred by the different models.
	 */
	private Metric<R> _metric;

	/**
	 * An ordered list of the model labels. This is used for retrieving models
	 * by index.
	 */
	private List<String> _modelLabels;

	/**
	 * A collection of labeled models that we would like to evaluate/compare.
	 */
	private Map<String, BatchFunctionApproximator<D, R>> _models;

	/**
	 * The data set.
	 */
	private List<Pair<D, R>> _dataSet;

	/**
	 * Constructs a cross-validation instance.
	 * 
	 * @param metric
	 *            a metric used for computing the error between a model and test
	 *            data.
	 * @param models
	 *            a list of models
	 * @param dataSet
	 *            a data set
	 */
	public CrossValidation(Metric<R> metric,
			List<BatchFunctionApproximator<D, R>> models,
			Collection<Pair<D, R>> dataSet) {
		_metric = metric;
		
		_modelLabels = new ArrayList<String>(models.size());
		_models = new HashMap<String, BatchFunctionApproximator<D, R>>();
		for (int i = 0; i < models.size(); i++) {
			_modelLabels.add(String.valueOf(i));
			_models.put(String.valueOf(i), models.get(i));
		}
		_dataSet = new ArrayList<Pair<D, R>>(dataSet);
	}

	public CrossValidation(Metric<R> metric, List<String> modelLabels,
			Map<String, BatchFunctionApproximator<D, R>> models,
			Collection<Pair<D, R>> dataSet) {

		if (modelLabels.size() != models.size()) {
			throw new IllegalArgumentException(
					"Number of model labels does not match the number of models.");
		}
		if (!modelLabels.containsAll(models.keySet())) {
			throw new IllegalArgumentException(
					"Mismatch between provided model labels and models' key set.");
		}

		_metric = metric;
		_modelLabels = modelLabels;
		_models = models;
		_dataSet = new ArrayList<Pair<D, R>>(dataSet);
	}
	
	public CrossValidation(Metric<R> metric, Map<String,BatchFunctionApproximator<D,R>> models, Collection<Pair<D,R>> dataSet)
	{
		_metric = metric;
		_modelLabels = new ArrayList<String>(models.size());
		_models = new HashMap<String,BatchFunctionApproximator<D,R>>(models);
		
		for(String key : models.keySet()){
			_modelLabels.add(key);
		}
		
		_dataSet = new ArrayList<Pair<D,R>>(dataSet);
	}

	/**
	 * Perform cross-validation <code>numTest</code> times.
	 * 
	 * @param trainRatio
	 *            the amount of the data set to use for training
	 * @param numTests
	 *            the number of times to split the data set and rerun
	 *            cross-validation
	 * @return the result of the cross-validation test
	 */
	public CrossValidationResult<D, R> cv(double trainRatio, int numTests) {
		Map<String, Double> scores = new HashMap<String, Double>();

		for (int t = 0; t < numTests; t++) {
			Pair<Collection<Pair<D, R>>, Collection<Pair<D, R>>> dataPair = splitTrainingAndTest(trainRatio);
			for (String key : _models.keySet()) {
				Double iscore = scores.get(key);
				if (iscore == null) {
					iscore = new Double(0.0);
				}
				double tscore = modelScore(_models.get(key), dataPair.getA(),
						dataPair.getB()) / numTests;
				scores.put(key, iscore + tscore);
			}
		}

		return new CrossValidationResult<D, R>(_modelLabels, _models, scores);
	}

	/**
	 * Computes the score for a model based on a split of the dataset.
	 * 
	 * @param model
	 *            a model
	 * @param trainingSet
	 *            the set of data used for training
	 * @param testSet
	 *            the set of data used for evaluation
	 * @return a score
	 */
	private double modelScore(BatchFunctionApproximator<D, R> model,
			Collection<Pair<D, R>> trainingSet, Collection<Pair<D, R>> testSet) {
		model.reset();
		model.train(trainingSet);

		double score = 0;
		int numTests = testSet.size();
		
		for (Pair<D, R> testSample : testSet) {
			R y = testSample.getB();
			R yhat = model.evaluate(testSample.getA());
			score += _metric.distance(y, yhat);
		}

		return score / numTests;
	}

	/**
	 * Splits the data set into training and testing sets.
	 * 
	 * @param trainRatio
	 *            a scalar in the interval (0, 1) representing the proportion of
	 *            data that will go into the training set
	 * @return a pair where the first item is the training set and the second
	 *         item is the test set
	 */
	public Pair<Collection<Pair<D, R>>, Collection<Pair<D, R>>> splitTrainingAndTest(
			double trainRatio) {
		if (trainRatio <= 0 && trainRatio >= 1) {
			throw new IllegalArgumentException(
					"trainRatio must be in interval (0, 1)");
		}
		Collections.shuffle(_dataSet);
		int numTraining = (int) (trainRatio * _dataSet.size());

		List<Pair<D, R>> trainingSet = _dataSet.subList(0, numTraining);
		List<Pair<D, R>> testSet = _dataSet.subList(numTraining,
				_dataSet.size());

		return new Pair<Collection<Pair<D, R>>, Collection<Pair<D, R>>>(
				trainingSet, testSet);
	}
}
