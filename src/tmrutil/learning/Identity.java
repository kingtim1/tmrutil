package tmrutil.learning;

/**
 * An implementation of the identity function. This function does not learn. It simply returns its argument, unmodified.
 * @author Timothy A. Mann
 *
 * @param <D> the data type
 */
public class Identity<D> implements IncrementalFunctionApproximator<D, D>
{
	private static final long serialVersionUID = -6555114488364563162L;

	@Override
	public void reset()
	{
		// Do nothing
	}

	@Override
	public D evaluate(D x) throws IllegalArgumentException
	{
		return x;
	}

	@Override
	public D train(D input, D target, double learningRate)
	{
		return null;
	}

	@Override
	public IncrementalFunctionApproximator<D, D> newInstance() {
		return new Identity<D>();
	}

}
