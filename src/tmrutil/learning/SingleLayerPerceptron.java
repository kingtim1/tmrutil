/*
 * SingleLayerPerceptron.java
 */

package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.math.RealFunction;
import tmrutil.math.MatrixOps;
import tmrutil.math.SAMatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.BellCurve;
import tmrutil.math.functions.HyperbolicTangent;
import tmrutil.math.functions.Identity;
import tmrutil.stats.Statistics;
import tmrutil.util.Pair;

/**
 * A single layer perceptron is a perceptron with an input and output layer
 * only.
 */
public class SingleLayerPerceptron extends Perceptron {

	private static final long serialVersionUID = -8272626170231437196L;

	/** The weight matrix. */
	private double[][] _weights;

	/**
	 * The regularization penalty.
	 */
	private double[] _lambda;

	private double[] _inputBias;

	/**
	 * Constructs a single layer perceptron from an existing weight matrix.
	 */
	public SingleLayerPerceptron(RealFunction f, double[][] weights,
			double[] lambda) {
		super(f, MatrixOps.getNumCols(weights) - BIAS, MatrixOps
				.getNumRows(weights));
		_weights = weights;
		_inputBias = new double[getInputSize() + BIAS];

		setRegularizationPenalty(lambda);
	}
	
	public SingleLayerPerceptron(RealFunction f, double[][] weights, double lambda)
	{
		super(f, MatrixOps.getNumCols(weights) - BIAS, MatrixOps
				.getNumRows(weights));
		_weights = weights;
		_inputBias = new double[getInputSize() + BIAS];

		setRegularizationPenalty(lambda);
	}

	/**
	 * Constructs a single layer perceptron with a randomly generated weight
	 * matrix and the identity activation function.
	 * 
	 * @param inputSize
	 *            the number of components in an input vector
	 * @param outputSize
	 *            the number of components in an output vector
	 * @param lambda
	 *            the regularization penalty
	 */
	public SingleLayerPerceptron(int inputSize, int outputSize, double lambda) {
		this(new Identity(), inputSize, outputSize, lambda);
	}
	
	/**
	 * Constructs a single layer perceptron with a randomly generated weight
	 * matrix and the identity activation function.
	 * 
	 * @param inputSize
	 *            the number of components in an input vector
	 * @param outputSize
	 *            the number of components in an output vector
	 * @param lambda
	 *            the regularization penalty
	 */
	public SingleLayerPerceptron(int inputSize, int outputSize, double[] lambda) {
		this(new Identity(), inputSize, outputSize, lambda);
	}

	/**
	 * Constructs a single layer perceptron with a randomly generated weight
	 * matrix and the identity activation function.
	 * 
	 * @param inputSize
	 *            the number of components in an input vector
	 * @param outputSize
	 *            the number of components in an output vector
	 */
	public SingleLayerPerceptron(int inputSize, int outputSize) {
		this(inputSize, outputSize, 0);
	}
	
	/**
	 * Constructs a single layer perceptron with a randomly generated weight
	 * matrix and a specified activation function.
	 */
	public SingleLayerPerceptron(RealFunction f, int inputSize, int outputSize,
			double[] lambda) {
		this(f, MatrixOps.random(outputSize, inputSize + BIAS, -1, 1), lambda);
	}

	/**
	 * Constructs a single layer perceptron with a randomly generated weight
	 * matrix and a specified activation function.
	 */
	public SingleLayerPerceptron(RealFunction f, int inputSize, int outputSize,
			double lambda) {
		this(f, MatrixOps.random(outputSize, inputSize + BIAS, -1, 1), lambda);
	}

	/**
	 * Constructs a single layer perceptron with a randomly generated weight
	 * matrix and a specified activation function.
	 */
	public SingleLayerPerceptron(RealFunction f, int inputSize, int outputSize) {
		this(f, inputSize, outputSize, 0);
	}

	/**
	 * Returns the regularization penalty used during training.
	 * 
	 * @return the regularization penalty used during training
	 */
	public double[] getRegularizationPenalty() {
		return _lambda;
	}

	/**
	 * Sets the regularization penalty with custom weights for each component.
	 * 
	 * @param lambda
	 *            an array containing positive scalar values with a number of
	 *            components equal to the input size
	 */
	public void setRegularizationPenalty(double[] lambda) {
		if (lambda.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The number of regularization parameters does not match the number of inputs.");
		}
		_lambda = Arrays.copyOf(lambda, lambda.length);
	}

	/**
	 * Sets the regularization penalty so that the penalty is the same for all
	 * components.
	 * 
	 * @param lambda
	 *            a positive scalar value
	 */
	public void setRegularizationPenalty(double lambda) {
		double[] alambda = new double[getInputSize()];
		Arrays.fill(alambda, lambda);
		setRegularizationPenalty(alambda);
	}

	@Override
	public double[] evaluate(double[] input, double[] result) {
		if (input.length == getInputSize()) {
			System.arraycopy(input, 0, _inputBias, 0, input.length);
			_inputBias[input.length] = 1;
		} else {
			throw new IllegalArgumentException(
					"The dimension of the input vector does not match the input size: input.length = "
							+ input.length
							+ " AND input size is "
							+ getInputSize());
		}

		return evaluateBias(_inputBias, result);
	}

	private double[] evaluateBias(double[] inputBias, double[] result) {
		result = MatrixOps.multiply(_weights, inputBias, result);
		for (int i = 0; i < result.length; i++) {
			result[i] = _f.evaluate(result[i]);
		}
		return result;
	}

	/**
	 * Training is performed using the Least-Mean Square algorithm.
	 */
	@Override
	public double[] train(double[] input, double[] target, double learningRate)
			throws IllegalArgumentException {
		if (learningRate < 0 || learningRate > 1) {
			throw new IllegalArgumentException(
					"The learning rate must be in the range [0,1]");
		}

		System.arraycopy(input, 0, _inputBias, 0, input.length);
		_inputBias[input.length] = 1;

		double[] output = new double[getOutputSize()];
		output = evaluateBias(_inputBias, output);
		// Compute the error between the output and the target
		double[] error = VectorOps.subtract(target, output);

		trainOnError(input, error, learningRate);

		return VectorOps.abs(error);
	}

	/**
	 * Trains this single layer perceptron provided an error (target minus
	 * output) vector.
	 * 
	 * @param input
	 *            an input vector
	 * @param error
	 *            an error vector (target - output)
	 * @param learningRate
	 *            the learning rate
	 */
	public void trainOnError(double[] input, double[] error, double learningRate) {
		System.arraycopy(input, 0, _inputBias, 0, input.length);
		_inputBias[input.length] = 1;

		double[] outNoAct = MatrixOps.multiply(_weights, _inputBias);
		double[] deriv = new double[outNoAct.length];
		for (int i = 0; i < outNoAct.length; i++) {
			deriv[i] = _f.differentiate(outNoAct[i]);
		}

		double[] wadj = new double[_inputBias.length];
		for (int k = 0; k < error.length; k++) {
			// Compute the unregularized weight adjustment
			wadj = VectorOps.multiply(_inputBias, learningRate * error[k]
					* -deriv[k], wadj);
			// Regularization without penalizing the bias weight
			for (int i = 0; i < _weights[k].length - 1; i++) {
				wadj[i] = wadj[i] + (learningRate * _lambda[i] * _weights[k][i]);
			}
			_weights[k] = VectorOps.subtract(_weights[k], wadj, _weights[k]);
		}

		if (MatrixOps.containsNaN(_weights)) {
			throw new TrainingException(TrainingException.NAN_DETECTED);
		}

		if (MatrixOps.containsInfinite(_weights)) {
			throw new TrainingException(TrainingException.INFINITY_DETECTED);
		}
	}

	@Override
	public void reset() {
		SAMatrixOps.random(_weights, -1, 1);
	}

	/**
	 * Sets the weights for each layer of this single layer perceptron using
	 * <code>weights</code>.
	 * 
	 * @param weights
	 *            a set of weights
	 * @throws IllegalArgumentException
	 *             if <code>weights != this.numberOfWeights()</code>
	 */
	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException {
		int numWeights = numberOfWeights();
		if (weights.length != numWeights) {
			throw new IllegalArgumentException(
					"The length of the weight array("
							+ weights.length
							+ ") does not match the number of weights necessary for this perceptron");
		}
		_weights = MatrixOps.reshape(weights, getOutputSize(),
				(getInputSize() + BIAS));
	}

	/**
	 * Returns a vector containing a deep copy of the weight parameters used by
	 * this single layer perceptron.
	 * 
	 * @return a vector containing the weight parameters
	 */
	public double[] getWeights() {
		int numWeights = numberOfWeights();
		double[] weights = new double[numWeights];
		for (int row = 0; row < getOutputSize(); row++) {
			for (int col = 0; col < getInputSize() + 1; col++) {
				weights[row * (getInputSize() + 1) + col] = _weights[row][col];
			}
		}
		return weights;
	}

	/**
	 * Returns a copy of the weight matrix.
	 * 
	 * @return a copy of the weight matrix
	 */
	public double[][] getWeightMatrix() {
		return MatrixOps.deepCopy(_weights);
	}

	/**
	 * Checks to see if any of the weights are NaN, Infinity, or -Infinity.
	 * 
	 * @return true if any of the weights are invalid
	 */
	public boolean containsInvalidWeights() {
		return MatrixOps.containsNaN(_weights)
				|| MatrixOps.containsInfinite(_weights);
	}

	@Override
	public int numberOfWeights() {
		return (getInputSize() + BIAS) * getOutputSize();
	}

	@Override
	public void dumpWeights() {
		System.out.println("Weights : ");
		System.out.println(MatrixOps.toString(_weights));
	}

	public static void main(String[] args) {
		int iterations = 10000;
		double learningRate = 0.3;

		Perceptron p = new SingleLayerPerceptron(2, 1);
		// Perceptron p = new SingleLayerPerceptron(new Identity(), new
		// double[1][3]);
		List<double[]> inputs = new java.util.ArrayList<double[]>(4);
		inputs.add(new double[] { -1, -1 });
		inputs.add(new double[] { 1, -1 });
		inputs.add(new double[] { -1, 1 });
		inputs.add(new double[] { 1, 1 });
		List<double[]> targets = new java.util.ArrayList<double[]>(4);
		targets.add(new double[] { -1 });
		targets.add(new double[] { -1 });
		targets.add(new double[] { 1 });
		targets.add(new double[] { 1 });

		System.out.println("Before Training");
		System.out.println("===============");
		for (int i = 0; i < inputs.size(); i++) {
			System.out.println(VectorOps.toString(inputs.get(i)) + " ==> "
					+ VectorOps.toString(p.evaluate(inputs.get(i))));
		}

		p.train(inputs, targets, iterations, learningRate);

		System.out.println("After  Training");
		System.out.println("===============");
		for (int i = 0; i < inputs.size(); i++) {
			System.out.println(VectorOps.toString(inputs.get(i)) + " ==> "
					+ VectorOps.toString(p.evaluate(inputs.get(i))));
		}
	}

	@Override
	public SingleLayerPerceptron newInstance() {
		return new SingleLayerPerceptron(_f, getInputSize(), getOutputSize(), _lambda);
	}
}