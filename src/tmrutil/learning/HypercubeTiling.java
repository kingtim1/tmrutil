package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.math.Modulus;
import tmrutil.math.VectorOps;

/**
 * A hypercube tiling partitions a high-dimensional space into discrete
 * hypercubes. Data can be stored and retrieved from specific hypercubes either
 * by refering to hypercube index coordinates or a high-dimensional point
 * contained in a hypercube.
 * <p/>
 * 
 * Because no data structure can store an infinite number of elements (and there
 * are an infinite number of hypercubes) a maximum number of elements is
 * selected at construction and hashing is used to assign hypercubes to the
 * finite number of stored elements.
 * <p/>
 * 
 * All tiles start with a default value of null.
 * 
 * @author Timothy A. Mann
 * 
 * @param <D>
 *            the type of data stored at a tile
 */
public class HypercubeTiling<D> implements Tiling<D>
{
	/** The number of dimensions of the tiled hypercubes. */
	private int _numDims;
	/** The dimensions of a single hypercube. */
	private double[] _dimensions;
	/** The offset of the tiling from the origin. */
	private double[] _offset;

	/** The maximum number of tiles that can be used to store data. */
	private int _maxMemory;
	/** A list of data stored at tiles. */
	List<D> _storedTiles;

	/** A table of psuedo random integers used for hashing. */
	private int[] _randTable;

	/**
	 * Constructs a hypercube tiling with specified hypercube dimensions, offset
	 * from the origin, and a maximum number of data elements that can be
	 * stored.
	 * 
	 * @param hypercubeDimensions
	 *            the dimensions of a single hypercube
	 * @param offset
	 *            the offset of the tiling from the origin
	 * @param tableSize
	 *            the size of the hashing table
	 * @param maxMemory
	 *            the maximum number of tile data elements that can be
	 *            explicitly stored
	 */
	public HypercubeTiling(double[] hypercubeDimensions, double[] offset,
			int tableSize, int maxMemory)
	{
		_numDims = hypercubeDimensions.length;
		_dimensions = Arrays.copyOf(hypercubeDimensions,
				hypercubeDimensions.length);
		_offset = Arrays.copyOf(offset, offset.length);
		_maxMemory = maxMemory;
		_storedTiles = new ArrayList<D>(maxMemory);
		// Load null values into the stored tiles
		for (int i = 0; i < _maxMemory; i++) {
			_storedTiles.add(null);
		}

		// Populate the random table used for hashing
		_randTable = VectorOps.randperm(tableSize);
	}

	@Override
	public int[] findTile(double[] x) throws IllegalArgumentException
	{
		if (x.length != _numDims) {
			throw new IllegalArgumentException(
					"The number of dimensions of the input vector does not match the number required.");
		}

		int[] coord = new int[_numDims];

		// Subtract the offset from the high dimensional point
		double[] xoff = VectorOps.subtract(x, _offset);

		// Convert each element of the vector into an integer
		for (int i = 0; i < _numDims; i++) {
			double v = xoff[i] / _dimensions[i];
			if (v < 0) {
				coord[i] = (int) Math.ceil(v);
			} else {
				coord[i] = (int) Math.floor(v);
			}
		}

		return coord;
	}

	@Override
	public D findTileData(double[] x) throws IllegalArgumentException
	{
		int[] coord = findTile(x);
		return findTileData(coord);
	}

	@Override
	public D findTileData(int[] coord) throws IllegalArgumentException
	{
		int index = hash(coord);
		return _storedTiles.get(index);
	}

	/**
	 * Sets the data value at the hypercube specified by a point contained in
	 * that hypercube.
	 * 
	 * @param x
	 *            a high dimensional data point
	 * @param data
	 *            a data element to be stored in the specified hypercube
	 * @throws IllegalArgumentException
	 *             if the high dimensional data point does not have the correct
	 *             number of dimensions
	 */
	public void setTileData(double[] x, D data) throws IllegalArgumentException
	{
		int[] coord = findTile(x);
		setTileData(coord, data);
	}

	/**
	 * Sets the data value at the hypercube specified by a coordinate.
	 * 
	 * @param coord
	 *            a hypercube coordinate
	 * @param data
	 *            a data element to store at the specified coordinate
	 * @throws IllegalArgumentException
	 *             if the coordinates dimension is incorrect
	 */
	public void setTileData(int[] coord, D data)
			throws IllegalArgumentException
	{
		int index = hash(coord);
		_storedTiles.set(index, data);
	}

	/**
	 * The maximum number of data elements that can be stored using this
	 * hypercube tiling.
	 * 
	 * @return the maximum number of data elements stored by this hypercube
	 *         tiling
	 */
	public int maxMemory()
	{
		return _maxMemory;
	}

	/**
	 * Determines the index associated with a hypercube coordinate using a
	 * psuedo-randomly generated lookup table.
	 * 
	 * @param coord
	 *            the coordinate of a hypercube
	 * @return the index of an element in store memory
	 */
	private int hash(int[] coord)
	{
		int sum = 0;
		for (int i = 0; i < _numDims; i++) {
			sum = sum + _randTable[Modulus.mod(coord[i], _randTable.length)];
		}
		return sum % _maxMemory;
	}

}
