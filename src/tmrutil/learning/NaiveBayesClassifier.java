package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.math.Constants;
import tmrutil.stats.InsufficientSamplesException;
import tmrutil.stats.Statistics;
import tmrutil.stats.VarianceEstimator;
import tmrutil.util.Pair;

/**
 * An implementation of a Naive Bayes Classifier that uses a Gaussian event
 * model.
 * 
 * @author Timothy A. Mann
 * 
 * @param <C>
 *            the class label type
 */
public class NaiveBayesClassifier<C> implements Classifier<RealVector, C> {

	private List<C> _classes;
	private Map<C, Double> _classPriors;
	private Map<C, Map<Integer, Double>> _means;
	private Map<C, Map<Integer, Double>> _vars;

	private int _numFeatures;

	public NaiveBayesClassifier(int numFeatures, Collection<C> classes) {
		_numFeatures = numFeatures;
		_classes = new ArrayList<C>(classes);
	}

	@Override
	public C classify(RealVector instance) {
		double[] scores = scores(instance);
		int index = Statistics.maxIndex(scores);
		return _classes.get(index);
	}

	@Override
	public double[] scores(RealVector instance) {
		double[] scores = new double[_classes.size()];
		for (int ci = 0; ci < _classes.size(); ci++) {
			scores[ci] = score(instance, _classes.get(ci));
		}
		return scores;
	}

	private double score(RealVector instance, C label) {
		double score = _classPriors.get(label);
		for (int i = 0; i < _numFeatures; i++) {
			double mean = _means.get(label).get(i);
			double var = _vars.get(label).get(i);
			if (var > Constants.EPSILON) {
				double std = Math.sqrt(var);
				NormalDistribution ndist = new NormalDistribution(mean, std);
				double p = ndist.density(instance.getEntry(i));
				score *= p;
			}else{
				score *= mean;
			}
		}
		return score;
	}

	@Override
	public void train(Collection<Pair<RealVector, C>> trainingSet) {
		_classPriors = classPriors(trainingSet);
		_means = new HashMap<C, Map<Integer, Double>>();
		_vars = new HashMap<C, Map<Integer, Double>>();

		for (C label : _classes) {
			_means.put(label, new HashMap<Integer, Double>());
			_vars.put(label, new HashMap<Integer, Double>());
			for (int i = 0; i < _numFeatures; i++) {
				VarianceEstimator vest = variance(trainingSet, label, i);
				try {
					_means.get(label).put(i, vest.mean());
					_vars.get(label).put(i, vest.estimate());
				} catch (InsufficientSamplesException ex) {
					_means.get(label).put(i, 0.0);
					_vars.get(label).put(i, 0.0);
				}
			}
		}
	}

	private VarianceEstimator variance(
			Collection<Pair<RealVector, C>> trainingSet, C label, int index) {
		VarianceEstimator vest = new VarianceEstimator();
		for (Pair<RealVector, C> sample : trainingSet) {
			if (sample.getB().equals(label)) {
				vest.add(sample.getA().getEntry(index));
			}
		}
		return vest;
	}

	private Map<C, Double> classPriors(Collection<Pair<RealVector, C>> trainingSet) {
		Map<C, Integer> classCounts = classCounts(trainingSet);
		Map<C, Double> priors = new HashMap<C, Double>();
		for (C label : _classes) {
			Integer count = classCounts.get(label);
			if (count == null) {
				priors.put(label, 0.0);
			} else {
				priors.put(label, count.doubleValue() / trainingSet.size());
			}
		}
		return priors;
	}

	private Map<C, Integer> classCounts(
			Collection<Pair<RealVector, C>> trainingSet) {
		Map<C, Integer> counts = new HashMap<C, Integer>();
		for (Pair<RealVector, C> sample : trainingSet) {
			C label = sample.getB();
			if (_classes.contains(label)) {
				Integer c = counts.get(label);
				if (c == null) {
					counts.put(label, 1);
				} else {
					counts.put(label, c.intValue() + 1);
				}
			}
		}
		return counts;
	}

	@Override
	public int numClasses() {
		return _classes.size();
	}

	@Override
	public NaiveBayesClassifier<C> newInstance() {
		return new NaiveBayesClassifier<C>(_numFeatures, _classes);
	}

}
