package tmrutil.learning;

import java.util.List;

import tmrutil.util.Pair;



/**
 * A set of labeled data.
 * 
 * @author Timothy A. Mann
 *
 * @param <X>
 *            the data type
 * @param <L>
 *            the label type. Labels can be either discrete or continuous.
 */
public interface LabeledData<X, L> extends Iterable<Pair<X, L>> {
	/**
	 * Returns the number of instances in this data set.
	 * 
	 * @return the number of instances in this data set
	 */
	public int numberOfInstances();

	/**
	 * Returns the <em>ith</em> labeled data point as a pair containing the
	 * datum instance and its label.
	 * 
	 * @param i
	 *            a non-negative index
	 * @return the ith datum-label pair
	 */
	public Pair<X, L> get(int i);

	/**
	 * Returns the <em>ith</em> datum.
	 * 
	 * @param i
	 *            a non-negative index
	 * @return the ith datum
	 */
	public X getDatum(int i);

	/**
	 * Returns the <em>ith</em> label.
	 * 
	 * @param i
	 *            a non-negative index
	 * @return the ith label
	 */
	public L getLabel(int i);

	/**
	 * Sample a random datum-label pair according to a uniform distribution.
	 * 
	 * @return a random datum-label pair
	 */
	public Pair<X, L> sample();

	/**
	 * Returns this labeled data set as a list of labeled pairs.
	 * 
	 * @return a list containing this instance's datum-label pairs
	 */
	public List<Pair<X, L>> toList();

	/**
	 * Returns a slice of this instance's datum-label pairs. The slice contains
	 * the indices [beginIndex, endIndex-1].
	 * 
	 * @param beginIndex
	 *            the index of the first instance (inclusive) to include in the slice
	 * @param endIndex
	 *            the index specifying the end (EXCLUSIVE) of the slice
	 * @return a list of datum-label pairs specified by the slice
	 */
	public List<Pair<X, L>> slice(int beginIndex, int endIndex);
	
	
}
