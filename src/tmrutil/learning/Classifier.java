package tmrutil.learning;

import java.util.Collection;

import tmrutil.util.Factory;
import tmrutil.util.Pair;

/**
 * Interface for discrete classifiers.
 * 
 * @author Timothy A. Mann
 * 
 * @param <X>
 *            the instance type to classify
 * @param <C>
 *            the class label type
 */
public interface Classifier<X, C> extends Factory<Classifier<X,C>>
{
	/**
	 * Classifies a given instance.
	 * 
	 * @param instance
	 *            an instance
	 * @return a class label
	 */
	public C classify(X instance);

	/**
	 * Computes and returns an array of scores (one score for each class).
	 * Higher scores indicate that the <code>instance</code> is more likely to
	 * belong to the specified class.
	 * 
	 * @param instance
	 *            an instance
	 * @return an array containing scores of the instance for each class
	 */
	public double[] scores(X instance);

	/**
	 * Trains this classifier with the given training set.
	 * 
	 * @param trainingSet
	 *            a training set
	 */
	public void train(Collection<Pair<X, C>> trainingSet);

	/**
	 * Returns the number of possible class labels.
	 * 
	 * @return the number of possible class labels
	 */
	public int numClasses();
}
