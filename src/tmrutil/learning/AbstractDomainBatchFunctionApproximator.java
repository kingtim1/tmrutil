package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.math.Function;
import tmrutil.util.Pair;

/**
 * A wrapper class that enables function approximators to process input samples from abstract domains. 
 * @author Timothy A. Mann
 *
 * @param <D1> the desired input domain
 * @param <D2> the domain of the underlying function approximator
 */
public class AbstractDomainBatchFunctionApproximator<D1,D2> implements
		BatchFunctionApproximator<D1, Double> {
	
	private static final long serialVersionUID = -3689925616456779537L;
	
	private Function<D1,D2> _featureExtractor;
	private BatchFunctionApproximator<D2,Double> _f;
	
	public AbstractDomainBatchFunctionApproximator(Function<D1,D2> featureExtractor, BatchFunctionApproximator<D2,Double> f)
	{
		_featureExtractor = featureExtractor;
		_f = f;
	}
	
	@Override
	public void reset() {
		_f.reset();
	}

	@Override
	public Double evaluate(D1 x) throws IllegalArgumentException {
		D2 x2 = _featureExtractor.evaluate(x);
		return _f.evaluate(x2);
	}

	@Override
	public AbstractDomainBatchFunctionApproximator<D1, D2> newInstance() {
		return new AbstractDomainBatchFunctionApproximator<D1,D2>(_featureExtractor, _f.newInstance());
	}

	@Override
	public void train(Collection<Pair<D1, Double>> trainingSet) {
		List<Pair<D2, Double>> samples = new ArrayList<Pair<D2, Double>>(
				trainingSet.size());
		for (Pair<D1, Double> pair : trainingSet) {
			samples.add(new Pair<D2, Double>(_featureExtractor
					.evaluate(pair.getA()), pair.getB()));
		}
		_f.train(samples);
	}

}
