package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;

/**
 * Radial basis function networks are used to approximate nonlinear functions.
 * 
 * @author Timothy Mann
 */
public class RBFNetwork extends NNet
{
	private static final long serialVersionUID = 1668102952601074950L;

	private static final double SD_IGNORE_SCALAR = 2.5;

	/**
	 * The number of hidden units.
	 */
	private int _hiddenSize;

	/**
	 * The output weights.
	 */
	private double[][] _oweights;

	/**
	 * The centers of the hidden units.
	 */
	private double[][] _centers;

	/** The fixed width parameter. */
	private double[] _sigmas;
	private double _initSigma;

	/** Used to store the intermediate hidden unit activations. */
	private double[] _hidBuff;
	/** A list of nonzero hidden unit indices. */
	private List<Integer> _nonzeroHInds;
	/** Used to store the output vector for training. */
	private double[] _outBuff;

	/**
	 * Constructs an untrained radial basis function network with default basis
	 * width (1.0).
	 * 
	 * @param inputSize
	 *            the size of input vectors
	 * @param outputSize
	 *            the size of the output vector
	 * @param hiddenSize
	 *            the number of bases to use
	 */
	public RBFNetwork(int inputSize, int outputSize, int hiddenSize)
	{
		this(inputSize, outputSize, hiddenSize, 1.0);
	}

	/**
	 * Constructs an untrained radial basis function network with default basis
	 * width (1.0) and either random or zero weights.
	 * 
	 * @param inputSize
	 *            the size of input vectors
	 * @param outputSize
	 *            the size of output vectors
	 * @param hiddenSize
	 *            the number of bases to use
	 * @param zeroWeights
	 *            true sets the hidden unit to output weights to zero; otherwise
	 *            random weights are chosen
	 */
	public RBFNetwork(int inputSize, int outputSize, int hiddenSize,
			boolean zeroWeights)
	{
		this(inputSize, outputSize, hiddenSize, 1.0);
		if (zeroWeights) {
			for (int i = 0; i < _oweights.length; i++) {
				Arrays.fill(_oweights[i], 0.0);
			}
		}
	}

	/**
	 * Constructs an untrained radial basis function network.
	 * 
	 * @param inputSize
	 *            the number of input dimensions
	 * @param outputSize
	 *            the number of output dimensions
	 * @param hiddenSize
	 *            the number of radial bases
	 */
	public RBFNetwork(int inputSize, int outputSize, int hiddenSize,
			double sigma)
	{
		super(inputSize, outputSize);
		_hiddenSize = hiddenSize;
		_oweights = MatrixOps.random(getOutputSize(), _hiddenSize + 1, -1, 1);
		_centers = MatrixOps.random(_hiddenSize + 1, getInputSize(), -1, 1);
		for (int i = 0; i < getInputSize(); i++) {
			_centers[_hiddenSize][i] = 1;
		}
		_initSigma = sigma;
		_sigmas = new double[hiddenSize];
		for (int i = 0; i < hiddenSize; i++) {
			_sigmas[i] = sigma;
		}

		// Create the hidden unit activation buffer and add a bias component
		_hidBuff = new double[hiddenSize + 1];
		_hidBuff[hiddenSize] = 1;

		_nonzeroHInds = new ArrayList<Integer>();

		// Create an output buffer for use with training
		_outBuff = new double[getOutputSize()];
	}

	/**
	 * Determines whether the value should be considered zero.
	 * 
	 * @param val
	 *            a scalar value
	 * @return true if the value is considered zero; otherwise false
	 */
	private boolean isZero(double val)
	{
		return (Math.abs(val) < 0.001);
	}

	/**
	 * The nonlinear activation function for the hidden units.
	 */
	private double[] hact(double[] input)
	{
		_nonzeroHInds.clear();
		_nonzeroHInds.add(_hiddenSize);
		for (int i = 0; i < _hiddenSize; i++) {
			double dist = VectorOps.distanceSqd(input, _centers[i]);
			double act = Math.exp(-(dist / (2 * _sigmas[i] * _sigmas[i])));
			_hidBuff[i] = act;
			if (!isZero(act)) {
				_nonzeroHInds.add(i);
			}
		}

		return _hidBuff;
	}

	/**
	 * Evaluates this radial basis function network at <code>x</code> and stores
	 * the result in the provided vector.
	 * 
	 * @param x
	 *            a vector to be used as input
	 * @param result
	 *            a vector to store the result of the evaluation
	 * @return the result of the evaluation
	 */
	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		if (x.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The dimension of the input vector is incorrect : x.length(="
							+ x.length + ") != _inputSize(=" + getInputSize()
							+ ")");
		}
		double[] ha = hact(x);
		// double[] output = MatrixOps.multiply(_oweights, ha, result);

		double[] output = new double[getOutputSize()];
		for (int i = 0; i < getOutputSize(); i++) {
			double sum = 0;
			for (Integer j : _nonzeroHInds) {
				sum += _oweights[i][j] * ha[j];
			}
			output[i] = sum;
		}

		return output;
	}

	/**
	 * Evaluates this radial basis function network for a single dimension of the total output vector.
	 * @param x the input vector
	 * @param index the index into a dimension of the output vector in the range <code>[0, this.getOutputSize()]</code>
	 * @return the value of this network's output vector component indexed by <code>index</code>
	 */
	public double evaluateSingleOutputDimension(double[] x, int index)
	{
		if (x.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The dimension of the input vector is incorrect : x.length(="
							+ x.length + ") != _inputSize(=" + getInputSize()
							+ ")");
		}
		double[] ha = hact(x);
		double sum = 0;
		for(Integer j : _nonzeroHInds){
			sum += _oweights[index][j] * ha[j];
		}
		return sum;
	}

	/**
	 * Trains this RBF network given a single training example. This method uses
	 * gradient descent to update the output weights, radial basis centers, and
	 * radial basis width parameters.
	 * 
	 * @param input
	 *            the input vector
	 * @param target
	 *            the target vector
	 * @param learningRate
	 *            the learning rate
	 * @return the error
	 */
	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{

		double[] hidden = hact(input);
		double[] output = MatrixOps.multiply(_oweights, hidden, _outBuff);
		double[] error = VectorOps.subtract(target, output);

		trainOnError(input, hidden, error, learningRate);

		return error;
	}

	/**
	 * Trains on a single output dimension. This is useful when you only have
	 * data for one output dimension. The other dimensions are ignored.
	 * 
	 * @param input
	 *            an input vector
	 * @param index
	 *            the index of the dimension to train on
	 * @param target
	 *            the target value for a single dimension
	 * @param learningRate
	 *            the learning rate
	 * @return the error on the trained dimension
	 */
	public double trainOnSingleOutputDimension(double[] input, int index,
			double target, double learningRate)
	{
		double[] hidden = hact(input);
		double output = VectorOps.dotProduct(_oweights[index], hidden);
		double[] error = new double[getOutputSize()];
		error[index] = target - output;

		trainOnError(input, hidden, error, learningRate);

		return error[index];
	}

	/**
	 * Trains this radial basis function provided the error (instead of the
	 * desired output).
	 * 
	 * @param input
	 *            an input vector
	 * @param hidden
	 *            a vector of hidden unit activations
	 * @param error
	 *            the error vector (target - output)
	 * @param learningRate
	 *            the learning rate
	 */
	private void trainOnError(double[] input, double[] hidden, double[] error,
			double learningRate)
	{
		double[] hx = hidden;
		double[] sx = new double[_hiddenSize];
		double[][] cx = new double[_hiddenSize][input.length];
		for (int i = 0; i < _hiddenSize; i++) {
			double sigma2 = _sigmas[i] * _sigmas[i];
			double sigma3 = sigma2 * _sigmas[i];
			sx[i] = VectorOps.distanceSqd(input, _centers[i]) / sigma3;
			for (int j = 0; j < input.length; j++) {
				cx[i][j] = (input[j] - _centers[i][j]) / sigma2;
			}
		}

		// Update parameters
		for (int k = 0; k < getOutputSize(); k++) {
			if (error[k] != 0.0) {
				for (int i = 0; i < _hiddenSize; i++) {
					// Update the basis centers
					double centerDeltaPre = (learningRate/2) * error[k]
							* _oweights[k][i] * hx[i];
					for (int j = 0; j < getInputSize(); j++) {
						_centers[i][j] += centerDeltaPre * cx[i][j];
					}
					// Update the basis widths
					_sigmas[i] += (learningRate/4) * error[k] * _oweights[k][i]
							* hx[i] * sx[i];
					// Update the output weights
					_oweights[k][i] += (learningRate) * error[k] * hx[i];

					// If a basis width is less than or equal to zero, then set
					// the
					// width to zero
					if (_sigmas[i] <= 0) {
						_sigmas[i] = 1.0;
					}
				}
			}
		}

		if (MatrixOps.containsNaN(_centers) || VectorOps.containsNaN(_sigmas)
				|| MatrixOps.containsNaN(_oweights)) {
			throw new TrainingException(TrainingException.NAN_DETECTED);
		}
		if (MatrixOps.containsInfinite(_centers)
				|| VectorOps.containsInfinite(_sigmas)
				|| MatrixOps.containsInfinite(_oweights)) {
			throw new TrainingException(TrainingException.INFINITY_DETECTED);
		}
	}

	@Override
	public void setWeights(double[] weights)
	{
		int numWeights = numberOfWeights();
		if (numWeights != weights.length) {
			throw new IllegalArgumentException(
					"The length of the weight array("
							+ weights.length
							+ ") does not match the number of weights necessary for this RBF network");
		}
		int centersRows = MatrixOps.getNumRows(_centers);
		int centersCols = MatrixOps.getNumCols(_centers);
		int oweightsRows = MatrixOps.getNumRows(_oweights);
		int oweightsCols = MatrixOps.getNumCols(_oweights);
		double[] centers = new double[centersRows * centersCols];
		System.arraycopy(weights, 0, centers, 0, centers.length);
		_centers = MatrixOps.reshape(centers, centersRows, centersCols);
		double[] oweights = new double[oweightsRows * oweightsCols];
		System.arraycopy(weights, centers.length, oweights, 0, oweights.length);
		_oweights = MatrixOps.reshape(oweights, oweightsRows, oweightsCols);
		System.arraycopy(weights, (centers.length + oweights.length), _sigmas,
				0, _sigmas.length);
	}

	@Override
	public int numberOfWeights()
	{
		int centers = MatrixOps.getNumRows(_centers)
				* MatrixOps.getNumCols(_centers);
		int oweights = MatrixOps.getNumRows(_oweights)
				* MatrixOps.getNumCols(_oweights);
		return centers + oweights + _sigmas.length;
	}

	/**
	 * Sets the output weights, basis centers, and basis width parameters.
	 * 
	 * @param outputWeights
	 *            output weights
	 * @param basisCenters
	 *            basis centers
	 * @param basisWidths
	 *            basis widths
	 * @throws IllegalArgumentException
	 *             to indicate that one of the arguments is invalid because its
	 *             dimensions do not match the dimensions required for this
	 *             radial basis function network
	 */
	public void setWeights(double[][] outputWeights, double[][] basisCenters,
			double[] basisWidths) throws IllegalArgumentException
	{
		if (MatrixOps.getNumRows(_oweights) != MatrixOps.getNumRows(outputWeights)
				|| MatrixOps.getNumCols(_oweights) != MatrixOps.getNumCols(outputWeights)) {
			throw new IllegalArgumentException("Invalid output weight matrix.");
		}
		if (MatrixOps.getNumRows(_centers) != MatrixOps.getNumRows(basisCenters)
				|| MatrixOps.getNumCols(_centers) != MatrixOps.getNumCols(basisCenters)) {
			throw new IllegalArgumentException("Invalid basis center matrix.");
		}
		if (_sigmas.length != basisWidths.length) {
			throw new IllegalArgumentException("Invalid basis width vector.");
		}
		_oweights = outputWeights;
		_centers = basisCenters;
		_sigmas = basisWidths;
	}

	/**
	 * Returns the output weights used by this RBF.
	 * 
	 * @return the output weights
	 */
	public double[][] getOutputWeights()
	{
		return _oweights;
	}

	/**
	 * Returns the centers used by the hidden units.
	 * 
	 * @return the centers used by the hidden units
	 */
	public double[][] getBasisCenters()
	{
		return _centers;
	}

	/**
	 * Returns the widths used by the hidden units.
	 * 
	 * @return the widths used by the hidden units
	 */
	public double[] getBasisWidths()
	{
		return _sigmas;
	}

	@Override
	public RBFNetwork newInstance() {
		return new RBFNetwork(getInputSize(), getOutputSize(), this._hiddenSize, this._initSigma);
	}
}
