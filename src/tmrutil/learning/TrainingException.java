package tmrutil.learning;

/**
 * Thrown to indicate that an error occurred during training.
 * @author Timothy A. Mann
 *
 */
public class TrainingException extends RuntimeException
{
	private static final long serialVersionUID = 6682660504009391971L;

	public static final String NAN_DETECTED = "A NaN value was detected in the updated parameter set.";
	public static final String INFINITY_DETECTED = "A positive or negative inifinity value was detected in the updated parameter set.";
	
	/**
	 * Constructs a training exception with a specified message.
	 * @param message a message string
	 */
	public TrainingException(String message)
	{
		super(message);
	}
}
