package tmrutil.learning;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import tmrutil.math.VectorOps;
import tmrutil.stats.Filter;
import tmrutil.stats.Statistics;
import tmrutil.util.Pair;

/**
 * An abstract implementation for binary decision trees.
 * 
 * @author Timothy A. Mann
 * 
 * @param <K>
 *            the key data type
 */
public class BinaryDecisionTree<K> implements Classifier<K, Integer> {
	/**
	 * An abstract node is a superclass for internal (decision) nodes and leaf
	 * nodes.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static abstract class AbstractNode<K> implements Serializable {
		private static final long serialVersionUID = 9015722337906437398L;
		private AbstractNode<K> _parent;

		public AbstractNode(AbstractNode<K> parent) {
			_parent = parent;
		}

		/**
		 * Returns the parent of this node or <code>null</code> if this is the
		 * root node.
		 * 
		 * @return the parent of this node or <code>null</code>
		 */
		public AbstractNode<K> getParent() {
			return _parent;
		}

		public void setParent(AbstractNode<K> parent) {
			_parent = parent;
		}

		/**
		 * Determines whether or not this node is the root node.
		 * 
		 * @return true if this is the root; otherwise false
		 */
		public boolean isRoot() {
			return _parent == null;
		}

		/**
		 * Classifies the key.
		 * 
		 * @param key
		 *            an instance to classify
		 * @return an integer representing the key's class
		 */
		public abstract int classify(K key);

		/**
		 * Returns the total number of leaves under this node or 0 if this node
		 * is a leaf.
		 * 
		 * @return the number of leaves under this node.
		 */
		public abstract int numSubLeaves();

		/**
		 * Returns the number of leaves of the left child of this node or 0 if
		 * this node is a leaf.
		 * 
		 * @return the number of leaves to the left of this node
		 */
		public abstract int numSubAcceptLeaves();

		/**
		 * Returns the number of leaves of the right child of this node or 0 if
		 * this node is a leaf.
		 * 
		 * @return the number of leaves to the right of this node
		 */
		public abstract int numSubRejectLeaves();

		/**
		 * Writes this node as a dot file.
		 * 
		 * @param filename
		 *            the name of the file to save the dot file as
		 * @throws IOException
		 *             if an I/O error occurs while saving this tree to a dot
		 *             file
		 */
		public void writeDotFile(String filename) throws IOException {
			StringBuilder sb = new StringBuilder();
			sb.append("digraph BTree {");

			writeDotFileHelper(sb, this);

			sb.append("}");

			FileWriter fwriter = new FileWriter(filename);
			fwriter.write(sb.toString());
			fwriter.close();
		}

		private void writeDotFileHelper(StringBuilder sb, AbstractNode<K> node) {
			if (node instanceof DecisionNode) {
				DecisionNode<K> dnode = (DecisionNode<K>) node;

				AbstractNode<K> acceptChild = dnode.getAcceptChild();
				AbstractNode<K> rejectChild = dnode.getRejectChild();

				sb.append(node.toString() + " -> " + acceptChild.toString()
						+ "[label=T];");
				sb.append(node.toString() + " -> " + rejectChild.toString()
						+ " [label=F,style=dashed];");

				writeDotFileHelper(sb, acceptChild);
				writeDotFileHelper(sb, rejectChild);
			}
		}

		public void writeLatexFile(String filename) throws IOException {
			String newline = System.getProperty("line.separator");
			StringBuilder sb = new StringBuilder();
			sb.append("\\documentclass[usletter,landscape]{scrartcl}"
					+ newline);
			sb.append("\\usepackage{tikz}"
					+ newline);
			sb.append("\\begin{document}"
					+ newline);
			sb.append("\\resizebox{1\\textwidth}{!}{" + newline);
			sb.append("\\begin{tikzpicture}[level/.style={sibling distance=200mm/#1}]"
					+ newline);
			

			sb.append("\\node [circle,draw] {" + prepLatexString(this.toString()) + "}" + newline);
			if (this instanceof DecisionNode) {
				DecisionNode<K> dnode = (DecisionNode<K>) this;
				AbstractNode<K> acceptChild = dnode.getAcceptChild();
				AbstractNode<K> rejectChild = dnode.getRejectChild();
				if (acceptChild != null) {
					writeLatexFileHelper(sb, acceptChild);
				}
				if (rejectChild != null) {
					writeLatexFileHelper(sb, rejectChild);
				}
			}

			sb.append(";" + newline);

			sb.append("\\end{tikzpicture}}" + newline + "\\end{document}"
					+ newline);

			FileWriter fwriter = new FileWriter(filename);
			fwriter.write(sb.toString());
			fwriter.close();
		}

		private void writeLatexFileHelper(StringBuilder sb, AbstractNode<K> node) {
			String newline = System.getProperty("line.separator");
			if (node instanceof DecisionNode) {
				DecisionNode<K> dnode = (DecisionNode<K>) node;
				AbstractNode<K> acceptChild = dnode.getAcceptChild();
				AbstractNode<K> rejectChild = dnode.getRejectChild();
				if (acceptChild == null && rejectChild == null) {
					sb.append("child {node [circle,draw] {" + prepLatexString(node.toString())
							+ "}}" + newline);
				} else {
					sb.append("child {node [circle,draw] {" + prepLatexString(node.toString())
							+ "}" + newline);
					if (acceptChild != null) {
						writeLatexFileHelper(sb, acceptChild);
					}
					if (rejectChild != null) {
						writeLatexFileHelper(sb, rejectChild);
					}
					sb.append("}" + newline);
				}

			} else {
				sb.append("child {node [circle,draw] {" + prepLatexString(node.toString())
						+ "}}" + newline);
			}
		}
		
		private String prepLatexString(String msg){
			return msg.replace("_", "\\_");
		}
	}

	/**
	 * An internal (decision) node contains a rule and an accept child and a
	 * reject child. When an instance is processed the if the rule accepts the
	 * instance, then the instance is passed to the accept child. If the
	 * instance is rejected by the rule, then it is passed to the reject child.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class DecisionNode<K> extends AbstractNode<K> {
		private static final long serialVersionUID = -5571239099067758059L;
		private Filter<K> _rule;
		private AbstractNode<K> _acceptChild;
		private AbstractNode<K> _rejectChild;

		public DecisionNode(DecisionNode<K> parent, Filter<K> rule,
				AbstractNode<K> acceptChild, AbstractNode<K> rejectChild) {
			super(parent);
			_rule = rule;
			_acceptChild = acceptChild;
			_rejectChild = rejectChild;

			_acceptChild.setParent(this);
			_rejectChild.setParent(this);
		}

		/**
		 * Returns a reference to the child representing acceptance by the rule.
		 * 
		 * @return a reference to the child representing acceptance by the rule
		 */
		public AbstractNode<K> getAcceptChild() {
			return _acceptChild;
		}

		/**
		 * Returns a reference to the child representing rejection by the rule.
		 * 
		 * @return a reference to the child representing rejection by the rule
		 */
		public AbstractNode<K> getRejectChild() {
			return _rejectChild;
		}

		/**
		 * Returns a reference to the rule used by this decision node.
		 * 
		 * @return a reference to the rule used by this decision node
		 */
		public Filter<K> getRule() {
			return _rule;
		}

		@Override
		public int classify(K key) {
			if (_rule.accept(key)) {
				return _acceptChild.classify(key);
			} else {
				return _rejectChild.classify(key);
			}
		}

		@Override
		public int numSubLeaves() {
			return _acceptChild.numSubLeaves() + _rejectChild.numSubLeaves()
					+ 2;
		}

		@Override
		public int numSubAcceptLeaves() {
			return _acceptChild.numSubLeaves();
		}

		@Override
		public int numSubRejectLeaves() {
			return _rejectChild.numSubLeaves();
		}

		@Override
		public String toString() {
			//return getClass().getSimpleName() + "_" + _rule;
			return String.valueOf(_rule);
		}
	}

	/**
	 * A value (leaf) node does not have any children. Its primary purpose is to
	 * return a value.
	 * 
	 * @author Timothy A. Mann
	 *
	 */
	public static class ValueNode<K> extends AbstractNode<K> {
		private static final long serialVersionUID = -7493409815935120688L;
		private int _value;

		public ValueNode(DecisionNode<K> parent, int value) {
			super(parent);
			_value = value;
		}

		@Override
		public int classify(K key) {
			return _value;
		}

		public int value() {
			return _value;
		}

		@Override
		public int numSubLeaves() {
			return 0;
		}

		@Override
		public int numSubAcceptLeaves() {
			return 0;
		}

		@Override
		public int numSubRejectLeaves() {
			return 0;
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + "_" + _value;
		}
	}
	
	public static final double DEFAULT_MIN_GAIN = 0.0;

	/** The root of the binary decision tree. */
	private AbstractNode<K> _root;
	private List<Filter<K>> _potentialSplits;
	private int _numClasses;
	
	private double _minGain;

	public BinaryDecisionTree(int numClasses, Collection<Filter<K>> potentialSplits){
		this(numClasses, potentialSplits, DEFAULT_MIN_GAIN);
	}
	
	/**
	 * Constructs an empty binary decision tree. All keys will be classified as
	 * type 0.
	 */
	public BinaryDecisionTree(int numClasses,
			Collection<Filter<K>> potentialSplits, double minGain) {
		_root = new ValueNode<K>(null, 0);
		if (numClasses < 1) {
			throw new IllegalArgumentException(
					"The number of classes must be positive.");
		}
		_numClasses = numClasses;
		_potentialSplits = new ArrayList<Filter<K>>(potentialSplits);
		
		if(minGain < 0){
			throw new IllegalArgumentException("Minimum gain must be non-negative.");
		}
		_minGain = minGain;
	}
	
	public AbstractNode<K> root(){
		return _root;
	}

	public void writeDotFile(String filename) throws IOException {
		_root.writeDotFile(filename);
	}

	/**
	 * Returns the total number of classes.
	 * 
	 * @return total number of classes
	 */
	public int size() {
		return _root.numSubLeaves() + 1;
	}

	/**
	 * Classifies instances into classes represented by integers.
	 * 
	 * @param key
	 *            an instance to classify
	 * @return an integer representing the class of the specified instance
	 */
	@Override
	public Integer classify(K instance) {
		return _root.classify(instance);
	}

	@Override
	public double[] scores(K instance) {
		double[] scores = new double[numClasses()];
		scores[classify(instance)] = 1;
		return scores;
	}

	@Override
	public void train(Collection<Pair<K, Integer>> trainingSet) {
		_root = buildTree(null, trainingSet, _potentialSplits);
	}

	@Override
	public int numClasses() {
		return _numClasses;
	}

	protected AbstractNode<K> buildTree(DecisionNode<K> parent,
			Collection<Pair<K, Integer>> trainingSet,
			List<Filter<K>> remainingSplits) {
		List<Filter<K>> remainingSplitsCopy = new ArrayList<Filter<K>>(
				remainingSplits);

		int[] counts = new int[numClasses()];
		for (Pair<K, Integer> instance : trainingSet) {
			counts[instance.getB()]++;
		}
		int maxCount = 0;
		Integer maxClass = 0;
		for (int c = 0; c < counts.length; c++) {
			if (counts[c] > maxCount) {
				maxCount = counts[c];
				maxClass = c;
			}
		}

		if (remainingSplitsCopy.size() == 0 || maxCount == trainingSet.size()) {
			return new ValueNode<K>(parent, maxClass);
		} else {
			// Calculate the split that gives the maximum score
			SplitStats bestSplitStats = null;
			Filter<K> bestSplitRule = null;
			for (Filter<K> splitRule : remainingSplitsCopy) {
				SplitStats stats = split(trainingSet, splitRule);
				if (bestSplitStats == null
						|| bestSplitStats.score() < stats.score()) {
					bestSplitStats = stats;
					bestSplitRule = splitRule;
				}
			}

			remainingSplitsCopy.remove(bestSplitRule);

			Collection<Pair<K, Integer>> left = bestSplitStats.left();
			Collection<Pair<K, Integer>> right = bestSplitStats.right();
			if (left.size() == 0 || right.size() == 0
					|| bestSplitStats.score() < _minGain) {
				return new ValueNode<K>(parent, maxClass);
			} else {
				AbstractNode<K> leftNode = buildTree(null, left,
						remainingSplitsCopy);
				AbstractNode<K> rightNode = buildTree(null, right,
						remainingSplitsCopy);
				DecisionNode<K> dnode = new DecisionNode<K>(parent,
						bestSplitRule, leftNode, rightNode);

				return dnode;
			}
		}
	}

	protected SplitStats split(Collection<Pair<K, Integer>> trainingSet,
			Filter<K> splitRule) {
		int[] acceptCounts = new int[numClasses()];
		int[] rejectCounts = new int[numClasses()];
		int[] classCounts = new int[numClasses()];

		List<Pair<K, Integer>> left = new ArrayList<Pair<K, Integer>>();
		List<Pair<K, Integer>> right = new ArrayList<Pair<K, Integer>>();
		for (Pair<K, Integer> instance : trainingSet) {
			classCounts[instance.getB()]++;
			if (splitRule.accept(instance.getA())) {
				acceptCounts[instance.getB()]++;
				left.add(instance);
			} else {
				rejectCounts[instance.getB()]++;
				right.add(instance);
			}
		}

		// Compute the true probabilities for the training samples
		double[] baseProbs = new double[numClasses()];
		double[] branch1Probs = new double[numClasses()];
		double[] branch2Probs = new double[numClasses()];
		for (int c = 0; c < numClasses(); c++) {
			baseProbs[c] = classCounts[c] / (double) trainingSet.size();
			branch1Probs[c] = acceptCounts[c] / (double) left.size();
			branch2Probs[c] = rejectCounts[c] / (double) right.size();
		}

		// We take the negative because we are trying to minimize KL-divergence
		// In other words, we are trying to minimize information loss
		double baseEntropy = entropy(baseProbs);
		double branch1Entropy = entropy(branch1Probs);
		double branch2Entropy = entropy(branch2Probs);

		double score = ((trainingSet.size() * baseEntropy)
				- ((left.size() * branch1Entropy) + (right.size() * branch2Entropy))) / trainingSet.size();

		return new SplitStats(score, left, right);
	}

	protected double entropy(double[] dist) {
		double ent = 0;
		double norm = Math.log(dist.length);
		for (int i = 0; i < dist.length; i++) {
			if (dist[i] > 0) {
				ent += (dist[i] * Math.log(dist[i]) / norm);
			}
		}
		return -ent;
	}

	class SplitStats {
		private double _score;
		private Collection<Pair<K, Integer>> _left;
		private Collection<Pair<K, Integer>> _right;

		public SplitStats(double score, Collection<Pair<K, Integer>> left,
				Collection<Pair<K, Integer>> right) {
			_score = score;
			_left = left;
			_right = right;
		}

		public double score() {
			return _score;
		}

		public Collection<Pair<K, Integer>> left() {
			return _left;
		}

		public Collection<Pair<K, Integer>> right() {
			return _right;
		}

	}

	public static void main(String[] args) {
		int numClasses = 2;
		List<Filter<int[]>> splitRules = new ArrayList<Filter<int[]>>();
		splitRules.add(new Filter<int[]>() {

			@Override
			public boolean accept(int[] x) {
				return x[0] == 1;
			}

			@Override
			public String toString() {
				return "x[0] == 1";
			}

		});
		splitRules.add(new Filter<int[]>() {

			@Override
			public boolean accept(int[] x) {
				return x[1] == 1;
			}

			@Override
			public String toString() {
				return "x[1] == 1";
			}

		});
		splitRules.add(new Filter<int[]>() {

			@Override
			public boolean accept(int[] x) {
				return x[0] == x[1];
			}

			@Override
			public String toString() {
				return "x[0] == x[1]";
			}

		});

		List<Pair<int[], Integer>> inputs = new ArrayList<Pair<int[], Integer>>();
		inputs.add(new Pair<int[], Integer>(new int[] { 0, 0 }, 1));
		inputs.add(new Pair<int[], Integer>(new int[] { 0, 1 }, 0));
		inputs.add(new Pair<int[], Integer>(new int[] { 1, 0 }, 0));
		inputs.add(new Pair<int[], Integer>(new int[] { 1, 1 }, 1));

		BinaryDecisionTree<int[]> dtree = new BinaryDecisionTree<int[]>(
				numClasses, splitRules);

		System.out.println("Before Training : ");
		for (int i = 0; i < inputs.size(); i++) {
			int[] input = inputs.get(i).getA();
			System.out.println(VectorOps.toString(input) + " ==> "
					+ dtree.classify(input));
		}

		dtree.train(inputs);

		System.out.println("After Training : ");
		for (int i = 0; i < inputs.size(); i++) {
			int[] input = inputs.get(i).getA();
			System.out.println(VectorOps.toString(input) + " ==> "
					+ dtree.classify(input));
		}
	}

	@Override
	public BinaryDecisionTree<K> newInstance() {
		return new BinaryDecisionTree<K>(numClasses(), _potentialSplits);
	}
}
