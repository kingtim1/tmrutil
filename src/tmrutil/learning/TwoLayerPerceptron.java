package tmrutil.learning;

import tmrutil.math.MatrixOps;
import tmrutil.math.RealFunction;
import tmrutil.math.SAMatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.HyperbolicTangent;

/**
 * A two-layer perceptron is a special case of the multi-layer perceptron with a
 * single hidden layer.
 * 
 * @author Timothy Mann
 * 
 */
public class TwoLayerPerceptron extends Perceptron
{
	private int _hiddenSize;
	private double[][] _oweights;
	private double[][] _hweights;

	private double[] _oilfs;
	private double[] _hilfs;
	private double[] _hact;
	private double[] _hactBias;
	private double[] _inputBias;

	private double[] _odx;
	private double[] _hdx;
	private double[] _odeltas;
	private double[] _hdeltas;

	/**
	 * Constructs an untrained two-layer perceptron which uses the hyperbolic
	 * tangent as its neural activation function.
	 * 
	 * @param inputSize
	 *            the size of the input vectors
	 * @param outputSize
	 *            the size of the output vector
	 * @param hiddenSize
	 *            the number of hidden units
	 */
	public TwoLayerPerceptron(int inputSize, int outputSize, int hiddenSize)
	{
		this(new HyperbolicTangent(), inputSize, outputSize, hiddenSize);
	}

	/**
	 * Constructs an untrained two-layer perceptron given an activation
	 * function, input size, output size, and number of hidden units.
	 * 
	 * @param f a differentiable function to be used as the activation function
	 * @param inputSize the size of the input vectors
	 * @param outputSize the size of the output vectors
	 * @param hiddenSize the number of hidden units
	 */
	public TwoLayerPerceptron(RealFunction f, int inputSize, int outputSize,
			int hiddenSize)
	{
		this(f, inputSize, outputSize, hiddenSize, false);
	}

	/**
	 * Constructs an untrained two-layer perceptron given an activation
	 * function, input size, output size, and number of hidden units.
	 * 
	 * @param f
	 *            a differentiable function to be used as the activation
	 *            function
	 * @param inputSize
	 *            the size of the input vectors
	 * @param outputSize
	 *            the size of the output vector
	 * @param hiddenSize
	 *            the number of hidden units
	 * @param zeroWeights
	 *            true if all weights should be set to zero; false indicates
	 *            that all weights should be randomly generated
	 */
	public TwoLayerPerceptron(RealFunction f, int inputSize, int outputSize,
			int hiddenSize, boolean zeroWeights)
	{
		super(f, inputSize, outputSize);
		_hiddenSize = hiddenSize;

		_oweights = new double[getOutputSize()][getHiddenSize() + BIAS];
		_hweights = new double[getHiddenSize()][getInputSize() + BIAS];
		if (!zeroWeights) {
			reset();
		}

		_oilfs = new double[outputSize];
		_hilfs = new double[hiddenSize];
		_hact = new double[hiddenSize];
		_hactBias = new double[hiddenSize + BIAS];
		_inputBias = new double[inputSize + BIAS];

		_odx = new double[outputSize];
		_hdx = new double[hiddenSize + BIAS];
		_odeltas = new double[outputSize];
		_hdeltas = new double[hiddenSize + BIAS];
	}

	@Override
	public void dumpWeights()
	{
		System.out.println("Weights : ");
		System.out.println("==========");
		System.out.println(MatrixOps.toString(_oweights));
		System.out.println(MatrixOps.toString(_hweights));
	}

	@Override
	public void reset()
	{
		SAMatrixOps.random(_oweights, -1, 1);
		SAMatrixOps.random(_hweights, -1, 1);
	}

	@Override
	public int numberOfWeights()
	{
		return (getOutputSize() * (getHiddenSize() + BIAS))
				+ (getHiddenSize() * (getInputSize() + BIAS));
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		if (weights.length != numberOfWeights()) {
			throw new IllegalArgumentException(
					"The length of the weight array("
							+ weights.length
							+ ") does not match the number of weights necessary for this perceptron");
		}
		double[] oweights = new double[getOutputSize()
				* (getHiddenSize() + BIAS)];
		double[] hweights = new double[getHiddenSize()
				* (getInputSize() + BIAS)];
		System.arraycopy(weights, 0, oweights, 0, oweights.length);
		System.arraycopy(weights, oweights.length, hweights, 0, hweights.length);

		double[][] ow = MatrixOps.reshape(oweights, getOutputSize(),
				getHiddenSize() + BIAS);
		double[][] hw = MatrixOps.reshape(hweights, getHiddenSize(),
				getInputSize() + BIAS);

		for (int r = 0; r < ow.length; r++) {
			for (int c = 0; c < ow[r].length; c++) {
				_oweights[r][c] = ow[r][c];
			}
		}

		for (int r = 0; r < hw.length; r++) {
			for (int c = 0; c < hw[r].length; c++) {
				_hweights[r][c] = hw[r][c];
			}
		}
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		double[] output = evaluate(input);
		double[] error = VectorOps.subtract(target, output);

		for (int i = 0; i < _odx.length; i++) {
			_odx[i] = _f.differentiate(_oilfs[i]);
		}
		for (int i = 0; i < _hdx.length - 1; i++) {
			_hdx[i] = _f.differentiate(_hilfs[i]);
		}
		_hdx[_hdx.length - 1] = 1;

		_odeltas = VectorOps.multiply(_odx, error, _odeltas);

		for (int r = 0; r < _oweights.length; r++) {
			for (int c = 0; c < _oweights[r].length; c++) {
				_hdeltas[c] = _hdeltas[c] + _odeltas[r] * _oweights[r][c];
				_oweights[r][c] += learningRate * _odeltas[r] * _hactBias[c];
			}
		}

		_hdeltas = VectorOps.multiply(_hdx, _hdeltas, _hdeltas);

		for (int r = 0; r < _hweights.length; r++) {
			for (int c = 0; c < _hweights[r].length; c++) {
				_hweights[r][c] += learningRate * _hdeltas[r] * _inputBias[c];
			}
		}
		
		if(MatrixOps.containsNaN(_oweights) || MatrixOps.containsNaN(_hweights)){
			throw new TrainingException(TrainingException.NAN_DETECTED);
		}
		
		if(MatrixOps.containsInfinite(_oweights) || MatrixOps.containsInfinite(_hweights)){
			throw new TrainingException(TrainingException.INFINITY_DETECTED);
		}

		return error;
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		if (x.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The dimension of the input vector does not match the input size: input.length = "
							+ x.length + " AND input size is " + getInputSize());
		}
		// Copy the values of x into input vector with a bias
		System.arraycopy(x, 0, _inputBias, 0, x.length);
		_inputBias[_inputBias.length - 1] = 1;

		// Calculate the hidden unit activation
		_hilfs = MatrixOps.multiply(_hweights, _inputBias, _hact);
		_hact = VectorOps.apply(_f, _hilfs, _hact);
		System.arraycopy(_hact, 0, _hactBias, 0, _hact.length);
		_hactBias[_hactBias.length - 1] = 1;

		// Calculate the output
		_oilfs = MatrixOps.multiply(_oweights, _hactBias, _oilfs);
		result = VectorOps.apply(_f, _oilfs, result);
		return result;
	}

	/**
	 * Returns the number of hidden units in this network.
	 * 
	 * @return the number of hidden units
	 */
	public int getHiddenSize()
	{
		return _hiddenSize;
	}

	public static void main(String[] args)
	{

		int iterations = 10;
		double learningRate = 0.1;

		double[] x = VectorOps.range(-1, 1, 1000);
		double[] y = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			y[i] = Math.sin(4 * x[i]);
		}

		Perceptron p = new TwoLayerPerceptron(new HyperbolicTangent(), 1, 1, 10);
		for (int i = 0; i < iterations; i++) {
			int[] rperm = VectorOps.randperm(x.length);
			for (int j = 0; j < 600; j++) {
				double[] input = { x[rperm[j]] };
				double[] target = { y[rperm[j]] };
				System.out.print("Error at (" + input[0] + ") : Before ["
						+ (target[0] - p.evaluate(input)[0]) + "] --> ");
				p.train(input, target, learningRate);
				System.out.println("After ["
						+ (target[0] - p.evaluate(input)[0]) + "]");
			}
		}

		double[] yHat = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			yHat[i] = p.evaluate(new double[] { x[i] })[0];
		}
		tmrutil.graphics.plot2d.Plot2D.linePlot(x, y, java.awt.Color.BLUE, x,
				yHat, java.awt.Color.RED, "Sin(x) vs. MLP", "X", "Y");
	}

	@Override
	public TwoLayerPerceptron newInstance() {
		return new TwoLayerPerceptron(this._f, this.getInputSize(), this.getOutputSize(), this.getHiddenSize());
	}
}
