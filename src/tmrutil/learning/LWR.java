package tmrutil.learning;

import java.util.Set;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import tmrutil.math.VectorOps;
import tmrutil.util.Pair;

/**
 * Implements the locally weighted regression algorithm.
 * 
 * @author Timothy A. Mann
 * 
 */
public class LWR implements IncrementalFunctionApproximator<double[], double[]>
{
	private static final long serialVersionUID = -5763221646402280871L;

	private IncrementalNearestNeighborSearch<double[], double[]> _nns;
	private double _maxDist;
	private double[] _defaultOutput;

	/**
	 * Constructs an instance of linear weighted regression.
	 * 
	 * @param nearestNeighborSearch
	 *            the nearest neighbor search algorithm used to determine local
	 *            points to arbitrary input points
	 * @param defaultOutput
	 *            the default output values
	 * @param genDistance
	 *            the generalization distance used to determine if two points
	 *            are close enough to be considered neighbors
	 */
	public LWR(
			IncrementalNearestNeighborSearch<double[], double[]> nearestNeighborSearch,
			double[] defaultOutput, double genDistance)
	{
		_nns = nearestNeighborSearch;
		_defaultOutput = defaultOutput;
		setGeneralizationDistance(genDistance);
	}

	/**
	 * Sets the generalization distance.
	 * 
	 * @param g
	 *            the new generalization distance (must be a positive value)
	 */
	public void setGeneralizationDistance(double g)
	{
		if (g <= 0) {
			throw new IllegalArgumentException(
					"The generalization distance must be positive.");
		}
		_maxDist = g;
	}

	@Override
	public void reset()
	{
		_nns.reset();
	}

	@Override
	public double[] evaluate(double[] x) throws IllegalArgumentException
	{
		Set<Pair<double[], double[]>> neighbors = _nns.distSearchPair(x,
				_maxDist);

		double[] out = new double[_defaultOutput.length];

		// TODO Apply weighted rather than ordinary regression
		for (int i = 0; i < out.length; i++) {
			OLSMultipleLinearRegression lr = new OLSMultipleLinearRegression();

			double[][] inputs = new double[neighbors.size()][];
			double[] targets = new double[neighbors.size()];
			int j = 0;
			for (Pair<double[], double[]> n : neighbors) {
				double[] input = n.getA();
				double[] target = n.getB();
				inputs[j] = input;
				targets[j] = target[i];
				j++;
			}
			lr.newSampleData(targets, inputs);

			double[] params = lr.estimateRegressionParameters();
			out[i] = VectorOps.dotProduct(params, x);
		}

		return VectorOps.add(_defaultOutput, out);
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		double[] output = evaluate(input);

		Pair<double[], double[]> sample = new Pair<double[], double[]>(input,
				target);
		_nns.update(sample);

		double[] error = VectorOps.subtract(target, output);
		return error;
	}

	@Override
	public LWR newInstance() {
		return new LWR(_nns, _defaultOutput, _maxDist);
	}

}
