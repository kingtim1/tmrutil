package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
import tmrutil.graphics.SOMViz;
import tmrutil.math.DimensionMismatchException;
import tmrutil.math.Metric;
import tmrutil.math.SAVectorOps;
import tmrutil.math.VectorOps;
import tmrutil.math.metrics.EuclideanDistance;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.KDTree;

/**
 * Implements a self organizing map (Kohonen map). A self organizing map
 * organizes data so that similar samples are placed near each other.
 * 
 * @author Timothy Mann
 */
public class SelfOrganizingMap
{
	private static final int EXPECTED_TRAINING_SAMPLES = 5000;

	/** A set of weights. */
	private List<double[]> _weights;
	/** The total number of weights. */
	private int _numWeights;
	/** The dimensions and size of the grid. */
	private int[] _gridProperties;

	/** The number of dimensions in each sample vector. */
	private int _sampleDimensions;

	/** The time step. */
	private int _time;

	/** The neighborhood width parameter. */
	private double _sigma;
	/** The learning rate parameter. */
	private double _eta;

	/** The first time constant. */
	private double _tau1;
	/** The second time constant. */
	private double _tau2;

	/** A metric for comparing real valued vectors. */
	private Metric<double[]> _metric;

	/** A set of data samples. */
	private List<double[]> _samples;

	/**
	 * Constructs a randomly initialized self organizing map.
	 * 
	 * @param metric
	 *            a distance metric for real valued vectors
	 * @param gridProperties
	 *            an integer array where each element denotes a dimension and
	 *            the value of the integer determines how many neurons wide the
	 *            dimension is
	 * @param initSigma
	 *            the initial neighborhood width parameter
	 * @param initEta
	 *            the initial learning rate parameter
	 */
	public SelfOrganizingMap(Metric<double[]> metric, int[] gridProperties,
			int sampleDimensions, double initSigma, double initEta)
	{
		_metric = metric;
		_time = 0;
		_sigma = initSigma;
		_eta = initEta;
		_tau1 = EXPECTED_TRAINING_SAMPLES / Math.log(initSigma);
		_tau2 = EXPECTED_TRAINING_SAMPLES;
		_sampleDimensions = sampleDimensions;
		_samples = new ArrayList<double[]>();
		_gridProperties = gridProperties;
		_numWeights = Statistics.product(gridProperties);
		_weights = new ArrayList<double[]>(_numWeights);
		init();
	}

	/**
	 * Returns the learning rate for the current time step.
	 * 
	 * @return the learning rate for the current time step
	 */
	protected double getLearningRate()
	{
		return _eta * Math.exp(-(_time / _tau2));
	}

	/**
	 * Returns the neighborhood width for the current time step.
	 * 
	 * @return the neighborhood width for the current time step
	 */
	protected double getNeighborhoodWidth()
	{
		return _sigma * Math.exp(-(_time / _tau1));
	}

	/**
	 * Implements the neighborhood function for two vectors <code>i</code> and
	 * <code>j</code>.
	 * 
	 * @param j
	 *            a real valued vector
	 * @param i
	 *            a real valued vector
	 * @return a real value determining the similarity/difference of the two
	 *         vectors
	 */
	private double neighborhoodFunction(int j, int i)
	{
		double sigma = getNeighborhoodWidth();
		int[] jpos = AbstractSelfOrganizingMap.indexToCoord(j, _gridProperties);
		int[] ipos = AbstractSelfOrganizingMap.indexToCoord(i, _gridProperties);
		double[] djpos = new double[jpos.length];
		double[] dipos = new double[ipos.length];
		for(int d=0;d<_gridProperties.length;d++){
			djpos[d] = jpos[d];
			dipos[d] = ipos[d];
		}
		return Math.exp(-(_metric.distance(djpos, dipos) / (2 * sigma * sigma)));
	}

	/**
	 * Initializes the SOM using samples of the data. If there are not enough
	 * samples some of the weights are randomly generated.
	 */
	private void init()
	{
		for (int i = 0; i < _numWeights; i++) {
			if (i < _samples.size()) {
				double[] sample = _samples.get(i);
				_weights.add(Arrays.copyOf(sample, sample.length));
			} else {
				_weights.add(VectorOps.random(_sampleDimensions, -1, 1));
			}
		}
	}

	/**
	 * Returns the index of the closest weight (according to the metric) to a
	 * vector <code>x</code>.
	 * 
	 * @param x
	 *            a vector
	 * @return the index of the closest weight to <code>x</code>
	 */
	private int findClosest(double[] x)
	{
		// This is a Naive search because I don't feel like implementing a
		// binary search.
		// Also, because the weights are changing during training the sorting
		// order may not be correct and so I would have to resort (i.e. O(n
		// log(n))) each time instead of just O(n).
		double min = _metric.distance(x, _weights.get(0));
		int minInd = 0;
		for (int i = 1; i < _weights.size(); i++) {
			double dist = _metric.distance(x, _weights.get(i));
			if (dist < min) {
				min = dist;
				minInd = i;
			}
		}
		return minInd;
	}

	/**
	 * Updates the weights given a sample <code>x</code> and the closest weight.
	 * @param x a sample vector
	 * @param winner the closest weight
	 * @param winnerInd the index of the closest weight
	 */
	private void updateWeights(double[] x, double[] winner, int winnerInd)
	{
		for (int i = 0; i < _weights.size(); i++) {
			if (i != winnerInd) {
				double[] wi = _weights.get(i);
				double[] delta = VectorOps.multiply(getLearningRate()
						* neighborhoodFunction(i, winnerInd), VectorOps.subtract(
						x, wi));
				SAVectorOps.add(wi, delta);
			}
		}
		// This needs to be here in case the metric function does not return 0
		// for points that are equivalent
		SAVectorOps.add(winner, VectorOps.multiply(getLearningRate()
				* neighborhoodFunction(winnerInd, winnerInd), VectorOps.subtract(x,
				winner)));
	}

	/**
	 * Add data samples to this SOM.
	 */
	public void add(List<double[]> samples) throws DimensionMismatchException
	{
		for (int i = 0; i < samples.size(); i++) {
			double[] sample = samples.get(i);
			if (sample.length != _sampleDimensions) {
				throw new DimensionMismatchException("Samples must have "
						+ _sampleDimensions + " dimensions : " + sample.length
						+ " != " + _sampleDimensions);
			}
		}
		_samples.addAll(samples);
	}

	/**
	 * Trains this SOM for a specified number of iterations.
	 * 
	 * @param iterations
	 *            the number of iterations to train for
	 */
	public void train(int iterations) throws IllegalArgumentException
	{
		if (iterations < 1) {
			throw new IllegalArgumentException(
					"The number of training iterations must be positive : "
							+ iterations);
		}
		for (int i = 0; i < iterations; i++) {
			double[] sample = _samples.get(tmrutil.stats.Random.RAND.nextInt(_samples.size()));
			int winner = findClosest(sample);
			updateWeights(sample, _weights.get(winner), winner);
		}
	}
	
	/**
	 * Returns the number of dimensions of the SOM grid.
	 * @return the number of dimensions of the SOM grid
	 */
	public int gridDimensions()
	{
		return _gridProperties.length;
	}
	
	public SOMViz display()
	{
		JFrame frame = new JFrame("SOM");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(640,480);
		SOMViz sviz = new SOMViz(_weights, _gridProperties);
		frame.add(sviz);
		frame.setVisible(true);
		return sviz;
	}
	
	public static void main(String[] args){
		
		double eta = 0.6;
		double sigma = 1.5;
		int trainingIterations = 100000;
		int[] gridProperties = {25, 25};
		int numSamples = 40000;
		List<double[]> samples = new ArrayList<double[]>(numSamples);
		for(int i=0;i<numSamples;i++){
			samples.add(new double[]{Random.uniform(), Random.uniform()});
			//samples.add(new double[]{tmrutil.stats.Random.normal(0,0.5), tmrutil.stats.Random.normal(0,0.5)});
		}
		SelfOrganizingMap som = new SelfOrganizingMap(new EuclideanDistance(), gridProperties, 2, eta, sigma);
		SOMViz sviz = som.display();
		som.add(samples);
		
		for(int i=0;i<trainingIterations;i++){
			som.train(1);
			sviz.repaint();
		}
	}
}
