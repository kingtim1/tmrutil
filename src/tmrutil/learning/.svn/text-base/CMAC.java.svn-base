package tmrutil.learning;

import java.util.Arrays;
import java.util.List;
import tmrutil.math.RealFunction;
import tmrutil.math.MatrixOps;
import tmrutil.math.RealVectorFunction;
import tmrutil.math.SAMatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.HyperbolicTangent;
import tmrutil.math.functions.Identity;
import tmrutil.math.metrics.HammingDistance;

/**
 * Cerebellar Model Arithmetic Computer (CMAC) is a perceptron-like function
 * approximator. Unlike perceptrons CMAC does not generalize over the whole
 * state space. Instead CMAC divides the state space so that generalization
 * occurs over small neighborhoods so that learning does not affect the entire
 * network.
 * 
 * @author Timothy Mann
 * 
 */
public class CMAC extends NNet
{
	private static final double MIN_VAL = -1;
	private static final double MAX_VAL = 1;

	/** An ordered list of all possible discrete values. */
	private double[] _discreteVals;
	/** The number of nonzero elements mapped to an association vector. */
	private int _aStarSize;
	/** The number of binary digits to shift each discrete value by. */
	private int _aStarOffset;

	/** The width of an input dimension in the _weights matrix. */
	private int _dimWidth;
	/** The number of weights in each column of the _weights matrix. */
	private int _numWeightsCols;
	/** A set of weights. */
	private double[][] _weights;

	/**
	 * Constructs an instance of CMAC.
	 * 
	 * @param inputSize
	 *            the number of dimensions in a valid input vector
	 * @param outputSize
	 *            the number of dimensions of the output
	 * @param aStarSize
	 *            the number of nonzero elements mapped to an association vector
	 * @param aStarOffset
	 *            the number of binary digits to shift each discrete value by
	 * @param numDiscreteVals
	 *            the number of discrete values that each element of an input
	 *            vector can take on
	 * @param randomInit
	 *            determines whether the function should start with random
	 *            weights or zero weights
	 */
	public CMAC(int inputSize, int outputSize, int aStarSize, int aStarOffset,
			int numDiscreteVals, boolean randomInit)
	{
		super(inputSize, outputSize);
		_aStarSize = aStarSize;
		_aStarOffset = aStarOffset;

		_dimWidth = ((numDiscreteVals - 1) * aStarOffset) + aStarSize;
		_numWeightsCols = inputSize * _dimWidth;
		if (randomInit) {
			_weights = MatrixOps.random(outputSize, _numWeightsCols);
		} else {
			_weights = new double[outputSize][_numWeightsCols];
		}

		_discreteVals = new double[numDiscreteVals];
		double inc = (MAX_VAL - MIN_VAL) / numDiscreteVals;
		for (int i = 0; i < numDiscreteVals; i++) {
			double v = (inc * i) + MIN_VAL;
			_discreteVals[i] = v;
		}
	}

	/**
	 * Constructs an instance of CMAC function approximator with all weights
	 * initialized to zero.
	 * 
	 * @param inputSize
	 *            the number of dimensions of a valid input vector
	 * @param outputSize
	 *            the number of dimensions of the output
	 * @param aStarSize
	 *            the number of nonzero elements mapped to an association vector
	 * @param aStarOffset
	 *            the number of binary digits to shift each discrete value by
	 * @param numDiscreteVals
	 *            the number of discrete values that each element of an input
	 *            vector can take on
	 */
	public CMAC(int inputSize, int outputSize, int aStarSize, int aStarOffset,
			int numDiscreteVals)
	{
		this(inputSize, outputSize, aStarSize, aStarOffset, numDiscreteVals,
				false);
	}

	/**
	 * Quantizes a value into one of the discrete values stored in
	 * <code>_discreteVals</code>.
	 * 
	 * @param var
	 *            a value to be quantized
	 * @return the index of a quantized value
	 */
	private int quantize(double var)
	{
		int ind = Arrays.binarySearch(_discreteVals, var);
		if (ind < 0) {
			ind = -(ind + 1);
			if (ind == _discreteVals.length) {
				ind = ind - 1;
			}
		}
		return ind;
	}

	/**
	 * Converts an input dimension and discrete value index into the appropriate
	 * indexes of the _weight matrix.
	 * 
	 * @param inputDim
	 *            the input dimension
	 * @param quantizeIndex
	 *            the quantized value index
	 * @return the set of indexes in the weight matrix
	 */
	private int[] getBinary(int inputDim, int quantizeIndex)
	{
		int[] mi = new int[_aStarSize];

		int baseInd = inputDim * _dimWidth;
		int valInd = quantizeIndex * _aStarOffset;
		if (quantizeIndex == 0) {
			valInd = 0;
		}
		int index = baseInd + valInd;

		for (int i = 0; i < _aStarSize; i++) {
			mi[i] = index + i;
		}

		return mi;
	}

	/**
	 * The mapping from sensory input to an associative cell vector.
	 * 
	 * @param x
	 *            sensory input vector
	 * @return an associative cell vector
	 */
	private double[] f(double[] x) throws IllegalArgumentException
	{
		if (x.length != getInputSize()) {
			throw new IllegalArgumentException(
					"Invalid input vector: found length = " + x.length
							+ ", required length = " + getInputSize());
		}
		double[] m = new double[_numWeightsCols];
		for (int i = 0; i < x.length; i++) {
			int[] mi = getBinary(i, quantize(x[i]));
			for (int j = 0; j < mi.length; j++) {
				m[mi[j]] = 1;
			}
		}
		return m;
	}

	/**
	 * Calculates the output of this CMAC network given a specified input.
	 * 
	 * @param x
	 *            a specified input vector
	 * @return a response vector
	 * @throws IllegalArgumentException
	 *             if <code>x</code> is not the correct number of dimensions
	 */
	@Override
	public double[] evaluate(double[] x) throws IllegalArgumentException
	{
		return evaluate(x, new double[getOutputSize()]);
	}

	/**
	 * Calculates the output of this CMAC network given a specified input.
	 * 
	 * @param x
	 *            a specified input vector
	 * @param result
	 *            a vector to store the result of evaluation in
	 * @return a response vector
	 * @throws IllegalArgumentException
	 *             if <code>x</code> is not the correct number of dimensions
	 */
	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		return MatrixOps.multiply(_weights, f(x), result);
	}

	/**
	 * Trains this CMAC network given an input-target pair.
	 * 
	 * @param input
	 *            an input
	 * @param target
	 *            a corresponding target
	 * @return the training error for each component after the update
	 * @throws IllegalArgumentException
	 *             if <code>input.length != this.getInputSize()</code>
	 */
	@Override
	public double[] train(double[] input, double[] target, double learningRate)
			throws IllegalArgumentException
	{
		double[] m = f(input);
		double[] y = MatrixOps.multiply(_weights, m);
		double[] error = VectorOps.subtract(target, y);
		double[][] dw = MatrixOps.multiply(VectorOps.outerProduct(error, m),
				learningRate / _aStarSize);
		_weights = SAMatrixOps.add(_weights, dw);
		return VectorOps.abs(error);
	}

	/**
	 * Returns an ordered array of discrete values.
	 * 
	 * @return an ordered array of discrete values
	 */
	public double[] getDiscreteVals()
	{
		return _discreteVals;
	}

	public static void main(String[] args)
	{
		int numSamples = 4000;
		double alpha = 0.9;
		int aStarSize = 8;
		int aStarOffset = 3;
		int numDiscreteVals = 30;

		double[] x = VectorOps.range(-1, 1, 1000);
		double[] y = new double[1000];
		for (int i = 0; i < x.length; i++) {
			y[i] = Math.sin(4*x[i]);
		}
		CMAC nnet = new CMAC(1, 1, aStarSize, aStarOffset, numDiscreteVals);
		System.out.println("Total # Weights : " + nnet.numberOfWeights());

		double[] yHatCMAC = new double[1000];
		SingleLayerPerceptron slp = new SingleLayerPerceptron(
				new HyperbolicTangent(), 1, 1);
		double[] yHatSLP = new double[1000];
		MultiLayerPerceptron mlp = new MultiLayerPerceptron(
				new HyperbolicTangent(), 1, 1, new int[] { 3, 2 });
		double[] yHatMLP = new double[1000];

		for (int i = 0; i < numSamples; i++) {
			int n = (int) Math.floor(1000 * tmrutil.stats.Random.uniform());
			nnet.train(new double[] { x[n] }, new double[] { y[n] }, alpha);
			slp.train(new double[] { x[n] }, new double[] { y[n] }, alpha);
			mlp.train(new double[] { x[n] }, new double[] { y[n] }, alpha);
		}

		for (int i = 0; i < x.length; i++) {
			yHatCMAC[i] = nnet.evaluate(new double[] { x[i] })[0];
			yHatSLP[i] = slp.evaluate(new double[] { x[i] })[0];
			yHatMLP[i] = mlp.evaluate(new double[] { x[i] })[0];
		}

		// nnet.displayA();

		tmrutil.graphics.plot2d.Plot2D.linePlot(x, y, java.awt.Color.RED, x,
				yHatSLP, java.awt.Color.BLUE, "SLP vs. True Sin(x)", "x", "y");
		tmrutil.graphics.plot2d.Plot2D.linePlot(x, y, java.awt.Color.RED, x,
				yHatCMAC, java.awt.Color.BLUE, "CMAC vs. True Sin(x)", "x", "y");
		tmrutil.graphics.plot2d.Plot2D.linePlot(x, y, java.awt.Color.RED, x,
				yHatMLP, java.awt.Color.BLUE, "MLP vs. True Sin(x)", "x", "y");
	}

	@Override
	public int numberOfWeights()
	{
		return MatrixOps.getNumRows(_weights) * MatrixOps.getNumCols(_weights);
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		if (weights.length != numberOfWeights()) {
			throw new IllegalArgumentException(
					"The number of weights provided does not match the number required.");
		}
		double[][] newWeights = MatrixOps.reshape(weights,
				MatrixOps.getNumRows(_weights), MatrixOps.getNumCols(_weights));
		_weights = newWeights;
	}
}
