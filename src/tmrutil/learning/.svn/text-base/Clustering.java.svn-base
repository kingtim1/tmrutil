package tmrutil.learning;

import java.util.Collection;

/**
 * Provides an interface for algorithms that implement unsupervised learning or
 * clustering of datasets. There are generally two categories of clustering
 * algorithms :
 * <ol>
 * <li>Agglomerative (i.e. Hierarchical) Clustering, and</li>
 * <li>Falt Clustering.</li>
 * </ol>
 * In agglomerative or hierarchical clustering some clusters may be subsets of
 * others. A flat clustering, however, is non-hierarchical. That is, cluster
 * membership is considered to be mutually exclusive.<p/>
 * 
 * Valid cluster indices are integers in the range <code>0, ..., size()-1</code>.
 * 
 * @author Timothy A. Mann
 * 
 * @param <D>
 *            the type of each data sample
 * 
 */
public interface Clustering<D>
{

	/**
	 * Run the clustering algorithm given a collection of data samples. All
	 * previous data is discarded and the clustering algorithm is rerun using
	 * only the data provided in <code>samples</code>.
	 * 
	 * @param samples a collection of data samples
	 */
	public void cluster(Collection<D> samples);

	/**
	 * Returns the elements in the cluster indexed by <code>k</code>.
	 * 
	 * @param k
	 *            the index of a particular cluster
	 * @return a collection of data samples in the <code>k+1</code>th cluster
	 */
	public Collection<D> getCluster(int k);
	
	/**
	 * Returns the/a representative of the cluster index by <code>k</code>.
	 * @param k the index of a particular cluster
	 * @return a representative member of the <code>k+1</code>th cluster
	 */
	public D getClusterRepresentative(int k);

	/**
	 * Returns the current number of clusters identified by this clustering
	 * algorithm.
	 * 
	 * @return the current number of clusters
	 */
	public int size();

	/**
	 * Returns true if <code>sample</code> belongs to the cluster indexed by
	 * <code>k</code>.
	 * 
	 * @param k
	 *            the index of a particular cluster
	 * @param sample
	 *            a sample data point
	 * @return true if the sample belongs to the specified cluster; otherwise
	 *         false
	 */
	public boolean inCluster(int k, D sample);
}
