package tmrutil.learning;

import tmrutil.math.Factorial;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;

/**
 * A general linear network is a function approximator that is more general than
 * a single-layer perceptron but is linear with respect to the parameters.
 * 
 * @author Timothy A. Mann
 * 
 */
public class GeneralLinearNetwork extends NNet
{
	private static final long serialVersionUID = 2276161342826593847L;
	private double[][] _weights;
	private int _numWeights;

	/**
	 * Constructs a general linear network with a specified input size and output size.
	 * @param inputSize the valid input vector size
	 * @param outputSize the output size
	 */
	public GeneralLinearNetwork(int inputSize, int outputSize)
	{
		super(inputSize, outputSize);

		_numWeights = 1 + (2 * inputSize) + consecutiveSum(inputSize - 1);
		_weights = new double[outputSize][_numWeights];
	}

	@Override
	public int numberOfWeights()
	{
		return getOutputSize() * _numWeights;
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		if (weights.length != numberOfWeights()) {
			throw new IllegalArgumentException(
					"The number of weights specified does not match the number required.");
		}
		_weights = MatrixOps.reshape(weights, getOutputSize(), _numWeights);
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		double[] binput = buildInput(input);
		double[] output = MatrixOps.multiply(_weights, binput);
		double[] error = VectorOps.subtract(target, output);

		for (int i = 0; i < getOutputSize(); i++) {
			for (int j = 0; j < _numWeights; j++) {
				_weights[i][j] += learningRate * error[i] * binput[j];
			}
		}
		
		if(MatrixOps.containsNaN(_weights)){
			throw new TrainingException(TrainingException.NAN_DETECTED);
		}
		if(MatrixOps.containsInfinite(_weights)){
			throw new TrainingException(TrainingException.INFINITY_DETECTED);
		}

		return error;
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		if(x.length != getInputSize()){
			throw new IllegalArgumentException("The dimension of the input vector does not match the input size: x.length = "
							+ x.length
							+ " AND input size is "
							+ getInputSize());
		}
		return MatrixOps.multiply(_weights, buildInput(x), result);
	}

	/**
	 * Constructs the intermediate input which is multiplied by the parameters.
	 * @param input an input vector
	 * @return the intermediate input
	 */
	private double[] buildInput(double[] input)
	{
		double[] binput = new double[_numWeights];
		binput[0] = 1;
		int offset = 1;
		for (int i = 0; i < getInputSize(); i++) {
			binput[i + offset] = input[i];
		}
		offset = getInputSize() + 1;
		for (int i = 0; i < getInputSize(); i++) {
			binput[i + offset] = input[i] * input[i];
		}

		offset = 2 * getInputSize() + 1;
		int ind = 0;
		for (int i = 0; i < getInputSize(); i++) {
			for (int j = i + 1; j < getInputSize(); j++) {
				binput[ind + offset] = input[i] * input[j];
				ind++;
			}
		}

		return binput;
	}
	
	/**
	 * Sums consecutive integers between 1 and n (inclusive).
	 * @param n positive integer
	 * @return the sum of consecutive integers between 1 and n (inclusive)
	 */
	private int consecutiveSum(int n)
	{
		return (n * (n + 1)) / 2;
	}

	public static void main(String[] args)
	{
		double[] x = VectorOps.range(-1, 1, 1000);
		double[] y = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			y[i] = 0.5 * x[i] * x[i];
		}

		double learningRate = 0.1;
		NNet nnet = new GeneralLinearNetwork(1, 1);

		int numSamples = 300;
		for (int i = 0; i < numSamples; i++) {
			int ind = Random.nextInt(x.length);
			nnet.train(new double[] { x[ind] }, new double[] { y[ind] },
					learningRate);
		}

		double[] yhat = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			yhat[i] = nnet.evaluate(new double[] { x[i] })[0];
		}

		tmrutil.graphics.plot2d.Plot2D.linePlot(x, y, java.awt.Color.RED, x,
				yhat, java.awt.Color.BLUE, "NNet vs. Ground Truth", "X", "Y");
	}
}
