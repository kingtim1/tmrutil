package tmrutil.learning;

import java.util.ArrayList;
import java.util.List;
import tmrutil.math.Function;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.Compose;
import tmrutil.math.functions.ConstantScale;
import tmrutil.math.functions.Sin;
import tmrutil.util.Interval;

/**
 * A linear function approximator that performs least squares minimization on
 * output weights. Before computing the output vector the input is passed
 * through a set of filters which extract linear or nonlinear features, and then
 * the resulting feature vector is multiplied by the output weights.
 * 
 * @author Timothy A. Mann
 * 
 */
public class LinearApproximator extends NNet
{
	private static final long serialVersionUID = 4182109884392065037L;

	/**
	 * A function that returns a constant.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class ConstantFilter implements Function<double[], Double>
	{
		private static final long serialVersionUID = 1096696830034022071L;
		/** A constant value. */
		private double _c;

		/**
		 * Constructs a constant filter.
		 * 
		 * @param c
		 *            a constant value
		 */
		public ConstantFilter(double c)
		{
			_c = c;
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException
		{
			return _c;
		}

	}

	/**
	 * A function that returns the element at a specific index of an input
	 * vector.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class IndexFilter implements Function<double[], Double>
	{
		private static final long serialVersionUID = 283982847304194670L;
		/** The index of the component to select. */
		private int _index;

		/**
		 * Constructs an index filter.
		 * 
		 * @param index
		 *            a valid index
		 */
		public IndexFilter(int index)
		{
			if (index < 0) {
				throw new IllegalArgumentException(
						"Valid indices must be nonnegative.");
			}
			_index = index;
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException
		{
			return x[_index];
		}

	}

	/**
	 * A function that subtracts one element of the input vector from another.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class SubtractionFilter implements Function<double[], Double>
	{
		private static final long serialVersionUID = -6440444884700745633L;
		/** The index of the first component. */
		private int _indA;
		/** The index of the second component. */
		private int _indB;

		/**
		 * Constructs a difference filter where the element at index B will be
		 * subtracted from the element at index A in the input vector.
		 * 
		 * @param indA
		 *            an index
		 * @param indB
		 *            an index
		 */
		public SubtractionFilter(int indA, int indB)
		{
			if (indA < 0 || indB < 0) {
				throw new IllegalArgumentException(
						"Valid indices must be nonnegative.");
			}
			_indA = indA;
			_indB = indB;
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException
		{
			return x[_indA] - x[_indB];
		}

	}

	/**
	 * A function that returns the product of two elements specified by indices
	 * into the input vector.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class ProductFilter implements Function<double[], Double>
	{
		private static final long serialVersionUID = -7865073461772917386L;
		/** The index of the first component. */
		private int _indA;
		/** The index of the second component. */
		private int _indB;

		/**
		 * Constructs a product filter where the elements of the input vector at
		 * the specified indices will be multiplied.
		 * 
		 * @param indA
		 *            an index
		 * @param indB
		 *            an index
		 */
		public ProductFilter(int indA, int indB)
		{
			if (indA < 0 || indB < 0) {
				throw new IllegalArgumentException(
						"Valid indices must be nonnegative.");
			}
			_indA = indA;
			_indB = indB;
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException
		{
			return x[_indA] * x[_indB];
		}
	}

	/**
	 * A function that takes an element at a specified index from an input
	 * vector to a specified power.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class PowerFilter implements Function<double[], Double>
	{
		private static final long serialVersionUID = 1942345135761510173L;
		/** The index of the component in the input vector. */
		private int _index;
		/** The power to raise the component to. */
		private double _power;

		/**
		 * Constructs a power filter.
		 * 
		 * @param index
		 *            the index of an element in the input vector
		 * @param power
		 *            the power to raise the element by
		 */
		public PowerFilter(int index, double power)
		{
			if (_index < 0) {
				throw new IllegalArgumentException(
						"Valid indices must be nonnegative.");
			}
			_index = index;
			_power = power;
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException
		{
			return Math.pow(x[_index], _power);
		}
	}
	
	/**
	 * A function that determines how similar the input vector is to a center point.
	 * @author Timothy A. Mann
	 *
	 */
	public static class GaussianFilter implements Function<double[],Double>
	{
		private static final long serialVersionUID = 7456308506825123427L;
		/** The center point. */
		private double[] _mu;
		/** The variance. */
		private double _variance;
		
		/**
		 * Constructs a Gaussian filter.
		 * @param mu the center of the filter
		 * @param sigma the standard deviation
		 */
		public GaussianFilter(double[] mu, double sigma)
		{
			_mu = mu;
			_variance = sigma * sigma;
		}
		
		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException
		{
			double diff2 = VectorOps.distanceSqd(x, _mu);
			return Math.exp(-(diff2/_variance));
		}
		
	}

	/** The list of filters used to compute the feature vector. */
	private List<Function<double[], Double>> _filters;
	/** A buffer to store the features extracted from input during evaluation. */
	private double[] _features;
	/** The output weights. */
	private double[][] _oweights;

	/** A buffer for holding the output vector during training. */
	private double[] _trOutput;

	/**
	 * Constructs a linear approximator with a set of filters that are used to
	 * extract features from the input space.
	 * 
	 * @param inputSize
	 *            the number of components (elements) in a valid input vector
	 * @param outputSize
	 *            the number of components (elements) in an output vector
	 * @param filters
	 *            a list of filters used to extract features from the input
	 *            vector
	 * @param zeroWeights
	 *            true if the initial weights should be set to zero; otherwise
	 *            random weights bounded between -1 and 1 are selected
	 */
	public LinearApproximator(int inputSize, int outputSize,
			List<Function<double[], Double>> filters, boolean zeroWeights)
	{
		super(inputSize, outputSize);
		if (filters.size() < 1) {
			throw new IllegalArgumentException(
					"The number of provided features must be positive.");
		}
		_filters = new ArrayList<Function<double[], Double>>(filters);
		_features = new double[_filters.size()];
		_trOutput = new double[outputSize];
		if (zeroWeights) {
			_oweights = new double[outputSize][_filters.size()];
		} else {
			_oweights = MatrixOps.random(outputSize, _filters.size(), -1, 1);
		}
	}

	@Override
	public int numberOfWeights()
	{
		return getFeatureVectorSize() * getOutputSize();
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		_oweights = MatrixOps.reshape(weights, getOutputSize(),
				getFeatureVectorSize());
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		double[] output = evaluate(input, _trOutput);
		double[] error = VectorOps.subtract(target, output);

		for (int r = 0; r < getOutputSize(); r++) {
			for (int c = 0; c < getFeatureVectorSize(); c++) {
				double delta = learningRate * error[r] * _features[c];
				_oweights[r][c] += delta;
			}
		}

		return error;
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		double[] features = computeFeatureVector(x, _features);
		return MatrixOps.multiply(_oweights, features);
	}

	/**
	 * Given a valid input vector, computes the corresponding feature vector
	 * using the filters stored in this function approximator.
	 * 
	 * @param input
	 *            an input vector
	 * @return a vector of features extracted from the input vector using a set
	 *         of filters
	 */
	public double[] computeFeatureVector(double[] input)
	{
		double[] features = new double[getFeatureVectorSize()];
		return computeFeatureVector(input, features);
	}

	/**
	 * Given a valid input vector, computes the corresponding feature vector
	 * using the filters stored in this function approximator.
	 * 
	 * @param input
	 *            an input vector
	 * @param features
	 *            a vector to hold the features
	 * @return a vector of features extracted from the input vector using a set
	 *         of filters
	 */
	private double[] computeFeatureVector(double[] input, double[] features)
	{
		if (input == null) {
			throw new NullPointerException(
					"Cannot compute the feature vector from a null input vector.");
		}
		if (input.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The number of elements in the specified input vector does not match the number required.");
		}
		for (int i = 0; i < _filters.size(); i++) {
			Function<double[], Double> filter = _filters.get(i);
			features[i] = filter.evaluate(input);
		}
		return features;
	}

	/**
	 * Returns the number of elements in a feature vector or (equivalently) the
	 * number of filters used by this function approximator.
	 * 
	 * @return the number of elements in a feature vector
	 */
	public int getFeatureVectorSize()
	{
		return _filters.size();
	}
	
	/**
	 * Constructs a radial basis function network with uniform random (fixed) basis.
	 * @param inputSpace a vector of intervals specifying the valid range for each dimension of the input space
	 * @param numBases the number of basis functions to generate
	 * @param basisWidths the width of the basis functions (smaller values lead to skinnier bases)
	 * @param outputSize the number of components of generated output vectors
	 * @return a radial basis function network (that is a linear function approximator)
	 */
	public static final LinearApproximator makeRBF(Interval[] inputSpace, int numBases, double basisWidths, int outputSize)
	{
		List<Function<double[],Double>> filters = new ArrayList<Function<double[],Double>>(numBases);
		for(int i=0;i<numBases;i++){
			double[] center = Interval.random(inputSpace);
			filters.add(new GaussianFilter(center, basisWidths));
		}
		return new LinearApproximator(inputSpace.length, outputSize, filters, false);
	}

}
