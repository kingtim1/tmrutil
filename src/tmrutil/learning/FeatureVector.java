package tmrutil.learning;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A sparse feature vector over an infinite set of features.
 * 
 * @author Timothy A. Mann
 * 
 */
public class FeatureVector {

	private Map<Integer, Double> _features;

	/**
	 * Constructs a feature vector with all zero elements.
	 */
	public FeatureVector() {
		_features = new HashMap<Integer, Double>();
	}

	/**
	 * Copy constructor that creates a deep copy of the specified feature
	 * vector.
	 * 
	 * @param other
	 *            another feature vector
	 */
	public FeatureVector(FeatureVector other) {
		_features = new HashMap<Integer, Double>(other._features);
	}

	/**
	 * Gets the value of an element specified by an index.
	 * 
	 * @param index
	 *            an index into this vector
	 * @return the value of the element specified by <code>index</code>
	 */
	public double element(int index) {
		Double v = _features.get(index);
		if (v == null) {
			return 0.0;
		} else {
			return v.doubleValue();
		}
	}

	/**
	 * Sets the value of an element by index.
	 * 
	 * @param index
	 *            the index of the element to set
	 * @param value
	 *            the value
	 */
	public void set(int index, double value) {
		if (Math.abs(value) > tmrutil.math.Constants.EPSILON) {
			_features.put(index, value);
		}
	}

	/**
	 * Returns the number of nonzero elements in this feature vector.
	 * 
	 * @return the number of nonzero elements
	 */
	public int numberOfNonzero() {
		return _features.size();
	}

	/**
	 * Returns the indices of all elements in this vector that have nonzero
	 * values.
	 * 
	 * @return indices of elements with nonzero values
	 */
	public Set<Integer> nonzeroIndices() {
		return _features.keySet();
	}

	/**
	 * Returns the nonzero indices that are common to this vector and the other
	 * vector.
	 * 
	 * @param other
	 *            another feature vector
	 * @return the set of feature indices that are nonzero for both vectors
	 */
	public Set<Integer> commonNonzeroIndices(FeatureVector other) {
		Set<Integer> commonInds = new HashSet<Integer>(other.nonzeroIndices());
		commonInds.retainAll(nonzeroIndices());
		return commonInds;
	}

	/**
	 * Returns the union of nonzero indices for this and the other vector.
	 * 
	 * @param other
	 *            another feature vector
	 * @return the set of feature indices that are nonzero in either this vector
	 *         or the other vector
	 */
	public Set<Integer> unionNonzeroIndices(FeatureVector other) {
		Set<Integer> unionInds = new HashSet<Integer>(other.nonzeroIndices());
		unionInds.addAll(nonzeroIndices());
		return unionInds;
	}

	/**
	 * Sums the elements of this and the other feature vector and returns the
	 * result as a new instance. This instance is not modified.
	 * 
	 * @param other another feature vector
	 * @return a sparse vector containing the sum of this and the other vectors' elements
	 */
	public FeatureVector add(FeatureVector other) {
		FeatureVector sum = new FeatureVector();
		Set<Integer> unionNonzero = unionNonzeroIndices(other);
		for (Integer i : unionNonzero) {
			sum.set(i, element(i) + other.element(i));
		}
		return sum;
	}

	/**
	 * Performs the dot product between this feature vector and the other
	 * feature vector
	 * 
	 * @param other
	 *            a feature vector
	 * @return the dot product of the feature vectors
	 */
	public double dotProduct(FeatureVector other) {
		double sum = 0;
		Set<Integer> commonInds = commonNonzeroIndices(other);
		for (Integer i : commonInds) {
			sum += element(i) * other.element(i);
		}
		return sum;
	}

	/**
	 * Sets nonzero elements of this feature vector using the nonzero elements
	 * of another feature vector. The elements are offset by a specified amount.
	 * 
	 * @param offset
	 *            a nonnegative integer specifying how far the other feature
	 *            vectors elements will be offset when they are copied into this
	 *            feature vector
	 * @param other
	 *            the other feature vector
	 */
	public void set(int offset, FeatureVector other) {
		Set<Integer> otherNonzero = other.nonzeroIndices();
		for (Integer ind : otherNonzero) {
			_features.put(offset + ind, other.element(ind));
		}
	}
}
