/*
 * MultiLayerPerceptron.java
 */

package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import tmrutil.math.RealFunction;
import tmrutil.math.MatrixOps;
import tmrutil.math.SAMatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.HyperbolicTangent;
import tmrutil.util.ArrayOps;
import tmrutil.util.Pair;

/**
 * A multi-layer perceptron is contains at least one intermediate layer between
 * input and outputs. Because of its multiple layers it cannot be trained using
 * the least-mean square algorithm (as with a single layer perceptron). Instead
 * the back-propagation method is used for training.
 * <p>
 * 
 * A multi-layer perceptron can also be trained using genetic algorithms. The
 * basic procedures is to construct an instance of {@link MultiLayerPerceptron}
 * using the desired input, output dimensions and hidden units. This instance
 * can be used to find the number of weights for the multi-layer perceptron by
 * calling {@link MultiLayerPerceptron#numberOfWeights}. An array of doubles
 * <code>individual</code> can be inserted as the weights of a multi-layer
 * perceptron using {@link MultiLayerPerceptron#setWeights}.
 * <p>
 * 
 * The activation function of a multi-layer perceptron is usually an instance of
 * either the {@link tmrutil.math.functions.HyperbolicTangent} function or
 * {@link tmrutil.math.functions.BellCurve} function.
 */
public class MultiLayerPerceptron extends Perceptron
{
	// private static final int INPUT = 0;

	private static final int BINPUT = 1;

	// private static final int ILFS = 2;

	private static final int DERIV = 3;

	private static final int OUTPUT = 4;

	private List<double[][]> _weights;
	private int[] _layers;

	/**
	 * Constructs a multi-layer perceptron with specified output and hidden
	 * layer weights.
	 * 
	 * @param f
	 *            a differentiable function
	 * @param oWeights
	 *            the output weights
	 * @param hWeights
	 *            the hidden weights
	 */
	public MultiLayerPerceptron(RealFunction f, double[][] oWeights,
			double[][] hWeights)
	{
		super(f, MatrixOps.getNumCols(hWeights) - 1,
				MatrixOps.getNumRows(oWeights));
		_weights = new ArrayList<double[][]>(2);
		_weights.add(hWeights);
		_weights.add(oWeights);
		_layers = new int[] { MatrixOps.getNumRows(hWeights) };
	}

	/**
	 * Constructs a multi-layer perceptron with a specified input, output, and
	 * hidden layer dimensions. The weights are initialized randomly. The number
	 * of hidden layers is <code>layers.length</code> and each element specifies
	 * the number of hidden units in its corresponding layer. The layers are
	 * added in order of ascending index from the input layer to the output
	 * layer.
	 * 
	 * @param f
	 *            a differentiable function
	 * @param inputSize
	 *            the number of dimensions expected for input vectors
	 * @param outputSize
	 *            the number of dimensions expected for output vectors
	 * @param layers
	 *            the number of hidden units in each layer
	 * @throws IllegalArgumentException
	 *             if any of the layers specified is not positive.
	 */
	public MultiLayerPerceptron(RealFunction f, int inputSize, int outputSize,
			int[] layers) throws IllegalArgumentException
	{
		super(f, inputSize, outputSize);
		for (int i = 0; i < layers.length; i++) {
			if (layers[i] < 1) {
				throw new IllegalArgumentException(
						"All hidden layers must have a positive number of hidden units : layers["
								+ i + "] = " + layers[i]);
			}
		}
		_layers = Arrays.copyOf(layers, layers.length);
		_weights = new ArrayList<double[][]>(layers.length + 1);

		if (layers.length > 0) {
			// Create the weights connecting the inputs to the hidden units
			_weights.add(MatrixOps.random(layers[0], inputSize + BIAS, -1, 1));

			// Create the weights connecting the hidden layers
			// in increasing order
			for (int i = 0; i < (layers.length - 1); i++) {
				_weights.add(MatrixOps.random(layers[i + 1], layers[i] + BIAS,
						-1, 1));
			}

			// Create the weights connecting the last hidden
			// layer to the output layer.
			_weights.add(MatrixOps.random(outputSize, layers[layers.length - 1]
					+ BIAS, -1, 1));
		} else {
			_weights.add(MatrixOps.random(outputSize, inputSize + BIAS, -1, 1));
		}
	}

	/**
	 * Get the weights of a particular layer. Layer indexes are ordered from 0,
	 * the lowest hidden layer, to the highest output layer.
	 * 
	 * @param layer
	 *            the layer
	 */
	public double[][] getWeights(int layer)
	{
		return _weights.get(layer);
	}

	/**
	 * Returns the number of neural units in a specified layer.
	 * 
	 * @param layer
	 *            the index of a layer in the neural network
	 * @return the number of neural units in the specified layer
	 */
	public int getNumUnitsInLayer(int layer)
	{
		if (layer == 0) {
			return getInputSize();
		} else {
			if (layer == _layers.length + 1) {
				return getOutputSize();
			} else {
				return _layers[layer - 1];
			}
		}
	}

	/**
	 * Returns the number of hidden layers in this multi-layer perceptron.
	 * 
	 * @return the number of hidden layers
	 */
	public int getNumHiddenLayers()
	{
		return _weights.size();
	}

	/**
	 * The activation function for each neuron of this multilayer perceptron.
	 * 
	 * @param x
	 *            input (usually the induced local fields of a layer of neurons)
	 * @return the results of the activation function applied to each element of
	 *         x in corresponding order
	 */
	private double[] activationFunction(double[] x)
	{
		double[] out = new double[x.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = _f.evaluate(x[i]);
		}
		return out;
	}

	/**
	 * The derivative of the activation function for each neuron of this
	 * multilayer perceptron.
	 * 
	 * @param x
	 *            input (usually the induced local fields of a layer of neurons)
	 * @return the results of the derivative of the activation function applied
	 *         to each element of x in corresponding order
	 */
	private double[] activationFunctionDerivative(double[] x)
	{
		double[] out = new double[x.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = _f.differentiate(x[i]);
		}
		return out;
	}

	/**
	 * Computes the output of this multilayer perceptron at the specified point
	 * and stores the result in the provided <code>result</code> vector.
	 * 
	 * @param input
	 *            the input vector
	 * @param result
	 *            a vector to store the result of evaluation
	 * @return the result vector
	 */
	@Override
	public double[] evaluate(double[] input, double[] result)
			throws IllegalArgumentException
	{
		if (input.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The dimension of the input vector does not match the input size: input.length = "
							+ input.length
							+ " AND input size is "
							+ getInputSize());
		}

		double[] out = input;
		for (int i = 0; i < _weights.size(); i++) {
			out = feedForward(out, i);
		}

		System.arraycopy(out, 0, result, 0, out.length);
		return result;
	}

	/**
	 * Evaluates this multi-layer perceptron and returns the activations of all
	 * of the layers.
	 * 
	 * @param input
	 *            the input
	 * @return a list of all activation layers
	 */
	public List<double[]> evaluateAllLayers(double[] input)
	{
		if (input.length != getInputSize()) {
			throw new IllegalArgumentException(
					"The dimension of the input vector does not match the input size: input.length = "
							+ input.length
							+ " AND input size is "
							+ getInputSize());
		}

		List<double[]> layerActs = new ArrayList<double[]>(
				getNumHiddenLayers() + 1);
		double[] out = input;
		for (int i = 0; i < _weights.size(); i++) {
			out = feedForward(out, i);
			layerActs.add(out);
		}
		return layerActs;
	}

	/**
	 * Computes the activation of a single layer in the perceptron.
	 * 
	 * @param x
	 *            the value to use as input to the layer
	 * @param layer
	 *            a layer in the perceptron (starting with 0)
	 * @return the activation of layer <code>layer</code>
	 */
	public double[] feedForward(double[] x, int layer)
	{
		return activationFunction(ilfs(x, layer));
	}

	/**
	 * Computes the induced local fields of a single layer in the perceptron.
	 * 
	 * @param x
	 *            the value to use as input to the layer
	 * @param layer
	 *            a layer in the perceptron (starting with 0)
	 * @return the induced local fields of a layer in the perceptron
	 */
	public double[] ilfs(double[] x, int layer)
	{
		return MatrixOps.multiply(_weights.get(layer), addBias(x));
	}

	/**
	 * Computes and returns a list containing all information about each layer
	 * during the processing of the specified input. For each layer the returned
	 * list contains a list which contains
	 * <ol>
	 * <li>input values (stored at index <code>INPUT</code>)</li>
	 * <li>input values with bias (stored at index <code>BINPUT</code>)</li>
	 * <li>induced local field values (stored at index <code>ILFS</code>)</li>
	 * <li>derivative of the activation function at the induced local field
	 * values (stored at index DERIV)</li>
	 * <li>activation values (stored at index OUTPUT)</li>
	 * </ol>
	 * 
	 * @param input
	 *            the input value
	 * @return
	 */
	private List<List<double[]>> feedForwardFullInfo(double[] input)
	{
		List<List<double[]>> info = new ArrayList<List<double[]>>(
				_weights.size());
		double[] out = input;
		for (int i = 0; i < _weights.size(); i++) {
			double[] inp = out;
			double[] binp = addBias(inp);
			double[] ilfs = ilfs(inp, i);
			double[] deriv = activationFunctionDerivative(ilfs);
			out = activationFunction(ilfs);
			List<double[]> layerInfo = new ArrayList<double[]>(4);
			layerInfo.add(inp); // Add the input
			layerInfo.add(binp); // Add the input with bias
			layerInfo.add(ilfs); // Add the induced local field values
			layerInfo.add(deriv); // Add the derivative of the activation
			// function at the induced local
			// field values
			layerInfo.add(out); // Add the activation values
			info.add(layerInfo);
		}
		return info;
	}

	/**
	 * Trains the neural network using the back-propagation algorithm.
	 * 
	 * @param input
	 *            a input vector
	 * @param target
	 *            a target vector
	 * @param learningRate
	 *            the learning rate parameter
	 * @return the error of each output with respect to the target (before
	 *         training)
	 */
	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		// 1. Create a list to store the new set of weights
		List<double[][]> tmpWeights = new ArrayList<double[][]>(_weights.size());

		// 2. Calculate the inputs passed to each layer
		List<List<double[]>> info = feedForwardFullInfo(input);

		double[][] weights = _weights.get(_weights.size() - 1);
		double[] output = info.get(info.size() - 1).get(OUTPUT);
		double[] err = VectorOps.subtract(target, output);
		double[] dx = info.get(info.size() - 1).get(DERIV);
		// 3. Calculate the initial deltas
		double[] deltas = VectorOps.multiply(dx, err);

		// 4. Calculate and adjust the weights for each layer
		for (int l = _weights.size() - 1; l >= 0; l--) {
			weights = _weights.get(l);
			double[] binput = info.get(l).get(BINPUT);

			/*
			 * Modify the existing weights.
			 * 
			 * There is one delta element per row of the matrix and one input
			 * element per column.
			 */
			int rows = MatrixOps.getNumRows(weights);
			int cols = MatrixOps.getNumCols(weights);
			double[][] w = new double[rows][cols];
			for (int r = 0; r < rows; r++) {
				for (int c = 0; c < cols; c++) {
					double wadj = learningRate * deltas[r] * binput[c];
					w[r][c] = weights[r][c] + wadj;
				}
			}
			tmpWeights.add(w);

			/*
			 * Calculate the deltas for this layer
			 * 
			 * Deltas for layer l are calculated from the deltas of layer l+1
			 * (one layer closer to the output).
			 */
			if (l - 1 >= 0) {
				dx = addBias(info.get(l - 1).get(DERIV));
				double[] tmpDeltas = new double[cols];
				for (int c = 0; c < cols; c++) {
					for (int r = 0; r < rows; r++) {
						tmpDeltas[c] = tmpDeltas[c] + deltas[r] * weights[r][c];
					}
				}
				deltas = VectorOps.multiply(dx, tmpDeltas);
			}
		}
		Collections.reverse(tmpWeights);
		_weights = tmpWeights;
		return VectorOps.abs(err);
	}

	@Override
	public void reset()
	{
		for (int i = 0; i < _weights.size(); i++) {
			SAMatrixOps.random(_weights.get(i), -1, 1);
		}
	}

	/**
	 * Sets the weights for each layer of this multilayer perceptron using
	 * <code>weights</code>.
	 * 
	 * @param weights
	 *            a set of weights
	 * @throws IllegalArgumentException
	 *             if <code>weights != this.numberOfWeights()</code>
	 */
	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		int numWeights = numberOfWeights();
		if (weights.length != numWeights) {
			throw new IllegalArgumentException(
					"The length of the weight array("
							+ weights.length
							+ ") does not match the number of weights necessary for this perceptron");
		}
		if (_layers.length > 0) {
			int from = 0;
			int to = (getInputSize() + BIAS) * _layers[0];
			double[] w = Arrays.copyOfRange(weights, from, from + to);
			_weights.set(0, MatrixOps.reshape(w, _layers[0],
					(getInputSize() + BIAS)));
			for (int i = 1; i < _layers.length; i++) {
				from = from + to;
				to = (_layers[i - 1] + BIAS) * _layers[i];
				w = Arrays.copyOfRange(weights, from, from + to);
				_weights.set(i, MatrixOps.reshape(w, _layers[i],
						(_layers[i - 1] + BIAS)));
			}
			from = from + to;
			to = (_layers[_layers.length - 1] + BIAS) * getOutputSize();
			w = Arrays.copyOfRange(weights, from, from + to);
			_weights.set(_layers.length, MatrixOps.reshape(w, getOutputSize(),
					_layers[_layers.length - 1] + BIAS));
		} else {
			_weights.set(0, MatrixOps.reshape(weights, getOutputSize(),
					(getInputSize() + BIAS)));
		}
	}

	@Override
	public int numberOfWeights()
	{
		int sum = 0;
		if (_layers.length > 0) {
			sum = (getInputSize() + BIAS) * _layers[0];
			for (int i = 1; i < _layers.length; i++) {
				sum += (_layers[i - 1] + BIAS) * _layers[i];
			}
			sum += (_layers[_layers.length - 1] + BIAS) * getOutputSize();
		} else {
			sum = (getInputSize() + BIAS) * getOutputSize();
		}
		return sum;
	}

	@Override
	public void dumpWeights()
	{
		System.out.println("Weights : ");
		System.out.println("==========");
		for (int i = 0; i < _weights.size(); i++) {
			System.out.println(MatrixOps.toString(_weights.get(i)));
		}
	}

	public static void main(String[] args)
	{

		int iterations = 1;
		double learningRate = 0.1;

		 double[] x = VectorOps.range(-1, 1, 1000);
		 double[] y = new double[x.length];
		 for (int i = 0; i < x.length; i++) {
		 y[i] = Math.sin(4 * x[i]);
		 }
		 Perceptron p = new MultiLayerPerceptron(new HyperbolicTangent(), 1,
		 1,
		 new int[] { 20, 10, 10 });
		
		 for (int i = 0; i < iterations; i++) {
		 int[] rperm = VectorOps.randperm(x.length);
		 for (int j = 0; j < 600; j++) {
		 double[] input = { x[rperm[j]] };
		 double[] target = { y[rperm[j]] };
		 System.out.print("Error at (" + input[0] + ") : Before ["
		 + (target[0] - p.evaluate(input)[0]) + "] --> ");
		 p.train(input, target, learningRate);
		 System.out.println("After ["
		 + (target[0] - p.evaluate(input)[0]) + "]");
		 }
		 }
		
		 double[] yHat = new double[x.length];
		 for (int i = 0; i < x.length; i++) {
		 yHat[i] = p.evaluate(new double[] { x[i] })[0];
		 }
		 tmrutil.graphics.plot2d.Plot2D.linePlot(x, y, java.awt.Color.BLUE, x,
		 yHat, java.awt.Color.RED, "Sin(x) vs. MLP", "X", "Y");

	}

	@Override
	public MultiLayerPerceptron newInstance() {
		return new MultiLayerPerceptron(this._f, this.getInputSize(), this.getOutputSize(), this._layers);
	}
}