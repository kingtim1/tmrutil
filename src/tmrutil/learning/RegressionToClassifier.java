package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.linear.RealVector;

import tmrutil.util.Pair;

/**
 * Constructs a 2-class classifier from a function approximator.
 * @author Timothy A. Mann
 *
 */
public class RegressionToClassifier implements Classifier<RealVector, Integer> {

	public static final int POSITIVE_CLASS = 0;
	public static final int NEGATIVE_CLASS = 1;
	
	public static final int POSITIVE_TARGET = 1;
	public static final int NEGATIVE_TARGET = -1;
	
	private BatchFunctionApproximator<RealVector,Double> _fa;
	/**
	 * The classification threshold.
	 */
	private double _threshold;
	
	public RegressionToClassifier(BatchFunctionApproximator<RealVector,Double> fa, double threshold)
	{
		_fa = fa;
		_threshold = threshold;
	}
	
	@Override
	public Integer classify(RealVector instance) {
		Double v = _fa.evaluate(instance);
		if(v >= _threshold){
			return POSITIVE_CLASS; 
		}else{
			return NEGATIVE_CLASS;
		}
	}

	@Override
	public double[] scores(RealVector instance) {
		Double v = _fa.evaluate(instance);
		return new double[]{v-_threshold, _threshold - v};
	}

	@Override
	public void train(Collection<Pair<RealVector, Integer>> trainingSet) {
		List<Pair<RealVector,Double>> samples = new ArrayList<Pair<RealVector,Double>>(trainingSet.size());
		for(Pair<RealVector,Integer> tsample : trainingSet)
		{
			Double target = null;
			if(tsample.getB() == POSITIVE_CLASS){
				target = new Double(POSITIVE_TARGET);
			}else{
				target = new Double(NEGATIVE_TARGET);
			}
			Pair<RealVector,Double> sample = new Pair<RealVector,Double>(tsample.getA(), target);
			samples.add(sample);
		}
		_fa.train(samples);
	}

	@Override
	public int numClasses() {
		return 2; // This is a 2-class classifier
	}

	@Override
	public RegressionToClassifier newInstance() {
		return new RegressionToClassifier(_fa.newInstance(), _threshold);
	}
}
