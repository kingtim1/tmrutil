package tmrutil.learning;

import tmrutil.util.Pair;

public interface IncrementalNearestNeighborSearch<K, V> extends
		NearestNeighborSearch<K, V>
{
	/**
	 * Adds a sample to this set and updates the underlying data structures.
	 * @param key the key element
	 * @param value the value associated with the key
	 */
	public void update(K key, V value);
	
	/**
	 * Adds a sample to this set and updates the underlying data structures.
	 * @param sample a sample element
	 */
	public void update(Pair<K,V> sample);
	
}
