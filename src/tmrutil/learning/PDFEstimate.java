package tmrutil.learning;

import tmrutil.math.Function;
import tmrutil.math.VectorOps;
import tmrutil.stats.MeanEstimator;
import tmrutil.stats.Statistics;
import tmrutil.stats.VarianceEstimator;

/**
 * A Gaussian mixture model estimate of a probability density function
 * derived from samples of N dimensional data.
 * 
 * @author Timothy Mann
 */
public class PDFEstimate implements Function<double[], Double>
{
	private static final long serialVersionUID = -9050679411076149316L;
	private double[] _weights;
	private double[][] _centers;
	private double[] _widths;
	private int _inputSize;
	private int _numSamples;
	
	private MeanEstimator[] _meanEstimator;
	private VarianceEstimator[] _varEstimator;

	public PDFEstimate(int dimension, int totalUnits)
	{
		_inputSize = dimension;
		_numSamples = 0;
		
		_meanEstimator = new MeanEstimator[dimension];
		_varEstimator = new VarianceEstimator[dimension];
		for(int d=0;d<dimension;d++){
			_meanEstimator[d] = new MeanEstimator();
			_varEstimator[d] = new VarianceEstimator();
		}
		
		_weights = new double[totalUnits];
		_centers = new double[totalUnits][dimension];
		_widths = new double[totalUnits];
		
		init();
	}
	
	private void init()
	{
		for(int i=0;i<_weights.length;i++){
			_weights[i] = 1.0/_weights.length;
			_centers[i] = VectorOps.random(_inputSize, -1, 1);
			_widths[i] = 1.0;
		}
	}
	
	@Override
	public Double evaluate(double[] x) throws IllegalArgumentException
	{
		double sum = 0;
		for (int i = 0; i < _weights.length; i++) {
			sum = sum + (_weights[i] * gaussian(x, _centers[i], _widths[i]));
		}
		double weightSum = Statistics.sum(_weights);
		return sum / weightSum;
	}

	private double gaussian(double[] x, double[] mu, double sigma)
	{
		return (1.0 / (sigma * Math.pow(2 * Math.PI, _inputSize/2.0)))
				* Math.exp(-VectorOps.distanceSqd(x, mu) / (2 * sigma * sigma));
	}
	
	public int getInputSize()
	{
		return _inputSize;
	}
	
	public int getOutputSize()
	{
		return 1;
	}
	
	public void addSample(double[] x, double learningRate)
	{
		_numSamples++;
		
		for(int d=0;d<_inputSize;d++){
			_meanEstimator[d].add(x[d]);
			_varEstimator[d].add(x[d]);
		}
		
		// First find the winning unit
		int winner = findWinner(x);
		
		// Move the winner towards the sample
		double[] error = VectorOps.subtract(x,_centers[winner]);
		_centers[winner] = VectorOps.add(_centers[winner], VectorOps.multiply(learningRate, error), _centers[winner]);
		
		// Increase the height of the winner
		_weights[winner] += learningRate; // * (1.0 / _numSamples);
		// Normalize the weights so that the sum of weights is one
		_weights = VectorOps.normalize(_weights, _weights);
		
		// Adjust the width of the unit based on the distance of the sample to the winner
		_widths[winner] += learningRate * ((2*VectorOps.length(error)) - _widths[winner]);
	}
	
	private int findWinner(double[] x)
	{
		int minI = 0;
		double minDist = VectorOps.distanceSqd(x, _centers[0]);
		for(int i=1;i<_centers.length;i++){
			double dist = VectorOps.distanceSqd(x, _centers[i]);
			if(minDist > dist){
				minDist = dist;
				minI = i;
			}
		}
		return minI;
	}
	
	/**
	 * Returns an estimate of the mean.
	 * @return an estimate of the mean
	 */
	public double[] mean()
	{
		double[] mu = new double[_inputSize];
		for(int d=0;d<_inputSize;d++){
			mu[d] = _meanEstimator[d].estimate();
		}
		return mu;
	}
	
	/**
	 * Returns an estimate of the variance.
	 * @return an estimate of the variance
	 */
	public double[] variance()
	{
		double[] var = new double[_inputSize];
		for(int d=0;d<_inputSize;d++){
			var[d] = _varEstimator[d].estimate();
		}
		return var;
	}
	
	/**
	 * Returns an estimate of the standard deviation.
	 * @return an estimate of the standard deviation
	 */
	public double[] std()
	{
		double[] std = new double[_inputSize];
		for(int d=0;d<_inputSize;d++){
			std[d] = Math.sqrt(_varEstimator[d].estimate());
		}
		return std;
	}
	
	public static void main(String[] args){
		double learningRate = 0.1;
		int numSamples = 10000;
		
		PDFEstimate pdf = new PDFEstimate(1, 20);
		for(int i=0;i<numSamples;i++){
			pdf.addSample(new double[]{tmrutil.stats.Random.uniform()}, learningRate);
		}
		
		double[] x = VectorOps.range(-1.2, 1.2, 300);
		double[] y = new double[x.length];
		for(int i=0;i<x.length;i++){
			y[i] = pdf.evaluate(new double[]{x[i]});
		}
		tmrutil.graphics.plot2d.Plot2D.linePlot(x, y, java.awt.Color.RED, "Uniform [0, 1]", "X", "Y");
	}
}
