package tmrutil.learning.rl;

import java.util.HashSet;
import java.util.Set;

import tmrutil.learning.rl.DiscreteMarkovDecisionProcess.ArrayEstimator;
import tmrutil.stats.Filter;
import tmrutil.util.Interval;

/**
 * Implements an estimator for Markov decision processes that takes advantage of
 * hints supplied in the form of a relevance filter or envelope. The relevance
 * filter determines whether a particular state might be relevant to the task.
 * 
 * @author Timothy A. Mann
 * 
 */
public class EnvelopeExploreModelEstimator extends ArrayEstimator
{
	/**
	 * A filter that accepts everything.
	 */
	public static final Filter<Integer> ACCEPT_EVERYTHING = new Filter<Integer>()
	{
		@Override
		public boolean accept(Integer x)
		{
			return true;
		}
		
	};
	
	private Filter<Integer> _relevant;

	/**
	 * Constructs an envelope exploring model estimator where the envelope contains every state.
	 * @param numStates the number of states in the modeled MDP
	 * @param numActions the number of actions in the modeled MDP
	 * @param discountFactor the discount factor
	 * @param rewardInterval the reward interval
	 * @param numVisitsUntilKnown the number of visits required before a state-action pair is considered known
	 */
	public EnvelopeExploreModelEstimator(int numStates, int numActions, double discountFactor, Interval rewardInterval, int numVisitsUntilKnown)
	{
		this(numStates, numActions, discountFactor, ACCEPT_EVERYTHING, rewardInterval, numVisitsUntilKnown);
	}
	
	/**
	 * Constructs an envelope exploring model estimator.
	 * 
	 * @param numStates
	 *            the number of states in the modeled MDP
	 * @param numActions
	 *            the number of actions in the modeled MDP
	 * @param discountFactor the discount factor
	 * @param relevant a filter which determines whether a state might be relevant
	 * @param rewardInterval the reward interval
	 * @param numVisitsUntilKnown the number of visits required before a state-action pair is considered known
	 */
	public EnvelopeExploreModelEstimator(int numStates, int numActions,
			double discountFactor, Filter<Integer> relevant,
			Interval rewardInterval, int numVisitsUntilKnown)
	{
		super(numStates, numActions, discountFactor, rewardInterval, numVisitsUntilKnown);
		_relevant = relevant;
	}

	@Override
	public double reinforcementUnknown(Integer state, Integer action)
	{
		if(_relevant.accept(state)){
			return rmax();
		}else{
			return rmin();
		}
	}

	@Override
	public double transitionProbUnknown(Integer state, Integer action,
			Integer resultState)
	{
		if (state.equals(resultState)) {
			return 1;
		} else {
			return 0;
		}
	}
	
	@Override
	public Set<Integer> states() {
		Set<Integer> states = new HashSet<Integer>();
		for(int s=0;s<numberOfStates();s++){
			states.add(s);
		}
		return states;
	}

	@Override
	public Set<Integer> actions(Integer state) {
		Set<Integer> actions = new HashSet<Integer>();
		for(int a=0;a<numberOfActions();a++){
			actions.add(a);
		}
		return actions;
	}
}
