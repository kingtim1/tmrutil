package tmrutil.learning.rl;

/**
 * Listens for the beginning and end of episodes. Where an episode is a sequence
 * of discrete timesteps either ending when a maximum number of timesteps is
 * reached or a task related termination condition is met.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface EpisodeObserver
{
	/**
	 * Called when the beginning of a new episode is observed.
	 */
	public void observeEpisodeBegin();
	
	/**
	 * Called when the end of an episode is observed.
	 */
	public void observeEpisodeEnd();
}
