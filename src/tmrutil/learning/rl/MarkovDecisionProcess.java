package tmrutil.learning.rl;

/**
 * Represents a Markov decision process.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state description type
 * @param <A>
 *            the action encoding type
 */
public interface MarkovDecisionProcess<S, A>
{
	/**
	 * Calculates and returns the probability of being in state
	 * <code>state</code>, taking action <code>action</code>, and ending up in
	 * state <code>resultState</code>.
	 * 
	 * @param state a valid state
	 * @param action a valid action from <code>state</code>
	 * @param resultState a valid resulting state
	 * @return the probability of observing the state transition
	 */
	public double transitionProb(S state, A action, S resultState);

	/**
	 * Calculates and returns the immediate reward for observing a state transition.
	 * @param state a valid state
	 * @param action a valid action from <code>state</code>
	 * @return the immediate reward for observing the state transition
	 */
	public double reinforcement(S state, A action);
	
	/**
	 * Returns the discount factor of this Markov decision process.
	 * @return the discount factor
	 */
	public double getDiscountFactor();
}
