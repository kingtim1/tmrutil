package tmrutil.learning.rl;

/**
 * A factory for constructing new instances of reinforcement learning algorithms.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 * @param <L> the learning algorithm type
 */
public interface AlgorithmFactory<S, A, L extends LearningSystem<S,A>>
{
	/**
	 * Constructs a new instance of the specified reinforcement learning algorithm.
	 * @return a new instance of a reinforcement learning algorithm
	 */
	public L newInstance();
}
