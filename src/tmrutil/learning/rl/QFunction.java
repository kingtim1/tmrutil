package tmrutil.learning.rl;

/**
 * Interface for Q-functions.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface QFunction<S, A>{
	
	/**
	 * A greedy value function with respect to an action-value function.
	 * @author Timothy A. Mann
	 *
	 * @param <S> the state type
	 */
	public static class GreedyVFunction<S> implements VFunction<S>
	{
		private QFunction<S,?> _qfunc;
		
		public GreedyVFunction(QFunction<S,?> qfunc)
		{
			_qfunc = qfunc;
		}

		@Override
		public double value(S state) {
			return _qfunc.greedyValue(state);
		}
	}

	/**
	 * Obtains a value function from this action-value function by selecting the
	 * greedy value at each state.
	 * 
	 * @return a greedy value function
	 */
	public VFunction<S> greedyValueFunction();

	/**
	 * Returns the value of a specified state-action pair
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the value
	 */
	public double value(S state, A action);

	/**
	 * Returns the value associated with the best action at the specified state.
	 * 
	 * @param state
	 *            a state
	 * @return the greedy value at <code>state</code>
	 */
	public double greedyValue(S state);

	public boolean isMaximizing();

	public boolean isMinimizing();
}
