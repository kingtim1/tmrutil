package tmrutil.learning.rl;

import java.util.Collection;
import tmrutil.learning.TrainingException;
import tmrutil.stats.Random;
import tmrutil.util.GenericComparator;
import tmrutil.util.Heap;
import tmrutil.util.UniqueValueHeap;

/**
 * Prioritized sweeping is a model-based heuristic for reducing the time
 * required for convergence of the value iteration algorithm. Essentially,
 * instead of sweeping over all states equally, states are assigned update
 * priorities based on the amount of change an immediate successor state has
 * undergone during its last update. The effect is that unexpected changes in
 * the environment cause computation to be spent on states affected by those
 * large changes.
 * 
 * @author Timothy A. Mann
 * 
 */
public class PrioritizedSweeping
{
	public static final double MAX_PRIORITY = 1.0;
	private static final int DEFAULT_QUEUE_CAPACITY = 20;

	/** The priority queue for updating states. */
	private UniqueValueHeap<Double, Integer> _pqueue;

	/** A reference to a discrete Markov decision process. */
	private DiscreteMarkovDecisionProcess<Integer,Integer> _mdp;

	/** The enqueue threshold. */
	private double _epsilon;

	/**
	 * Constructs a prioritized sweeping instance.
	 * 
	 * @param mdp
	 *            a discrete Markov decision process
	 * @param threshold
	 *            the enqueue threshold
	 */
	public PrioritizedSweeping(DiscreteMarkovDecisionProcess<Integer,Integer> mdp,
			double threshold)
	{
		if (mdp == null) {
			throw new NullPointerException(
					"Cannot perform prioritized sweeping with a null Markove decision process.");
		}
		_mdp = mdp;
		setEnqueueThreshold(threshold);
		_pqueue = new UniqueValueHeap<Double, Integer>(DEFAULT_QUEUE_CAPACITY, new GenericComparator<Double>(), new GenericComparator<Integer>());
	}

	/**
	 * Enqueue a state for backup in the priority queue with a specified
	 * priority.
	 * 
	 * @param state
	 *            the state to update
	 * @param priority
	 *            the priority of the state backup
	 */
	public void enqueue(Integer state, double priority)
	{
		if (priority > _epsilon) {
			if(priority > MAX_PRIORITY){
				priority = MAX_PRIORITY;
			}
			_pqueue.increase(priority, state);
		}
	}

	/**
	 * Update the value function estimates with at most <code>maxBackups</code>.
	 * 
	 * @param maxBackups
	 *            the maximum number of backups to perform
	 * @param Vbuff
	 *            the value function buffer
	 * @return a reference to <code>Vbuff</code>
	 */
	public double[] valueSweep(int maxBackups, double[] Vbuff)
	{
		if (Vbuff.length != _mdp.numberOfStates()) {
			throw new IllegalArgumentException(
					"The value function buffer provided does not match the number of MDP states.");
		}

		for (int n = 0; n < maxBackups && _pqueue.size() > 0; n++) {
			Integer state = _pqueue.popValue();
			double oldV = Vbuff[state];
			double newV = valueUpdate(state, Vbuff);
			
			if (Double.isNaN(newV)) {
				throw new TrainingException(TrainingException.NAN_DETECTED);
			}
			if (Double.isInfinite(newV)) {
				throw new TrainingException(
						TrainingException.INFINITY_DETECTED);
			}
			
			Vbuff[state] = newV;
			double delta = Math.abs(newV - oldV);

			Collection<Integer> predStates = _mdp.predecessors(state);
			for (Integer pstate : predStates) {
				double tprob = maxActionProb(pstate, state);
				double priority = tprob * delta;
				enqueue(pstate, priority);
			}
		}
		return Vbuff;
	}

	private double valueUpdate(Integer state, double[] Vbuff)
	{
		int nStates = _mdp.numberOfStates();
		int nActions = _mdp.numberOfActions();

		//Collection<Integer> successors = _mdp.successors(state);
		double bestVal = 0;
		double gamma = _mdp.getDiscountFactor();
		for (int a = 0; a < nActions; a++) {
			double val = 0;
			for (int s2=0;s2<nStates;s2++) {
				double transProb = _mdp.transitionProb(state, a, s2);
				double reward = (1-gamma) * _mdp.reinforcement(state, a);
				double Vs2 = Vbuff[s2];
				val += transProb * (reward + gamma * Vs2);
			}

			if (a == 0 || bestVal < val) {
				bestVal = val;
			}
		}
		return bestVal;
	}

	private double maxActionProb(Integer prevState, Integer state)
	{
		double maxProb = 0;
		
		for (int a = 0; a < _mdp.numberOfActions(); a++) {
			double prob = _mdp.transitionProb(prevState, a, state);
			if(maxProb < prob){
				maxProb = prob;
			}
		}
		return maxProb;
	}

	/**
	 * Sets the enqueue threshold.
	 * 
	 * @param epsilon
	 *            a small positive scalar
	 */
	public void setEnqueueThreshold(double epsilon)
	{
		if (epsilon <= 0) {
			throw new IllegalArgumentException(
					"The enqueue threshold must be positive.");
		}
		_epsilon = epsilon;
	}

	/**
	 * Returns the enqueue threshold.
	 * 
	 * @return enqueue threshold
	 */
	public double getEnqueueThreshold()
	{
		return _epsilon;
	}
}
