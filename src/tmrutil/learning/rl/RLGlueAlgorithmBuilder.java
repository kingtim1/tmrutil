package tmrutil.learning.rl;

import org.rlcommunity.rlglue.codec.taskspec.TaskSpecVRLGLUE3;
import org.rlcommunity.rlglue.codec.taskspec.ranges.DoubleRange;
import org.rlcommunity.rlglue.codec.taskspec.ranges.IntRange;

import tmrutil.learning.LearningRate;
import tmrutil.learning.LearningRate.Constant;
import tmrutil.util.Interval;

/**
 * A convenience class that enables the construction of reinforcement learning
 * algorithms from a {@link TaskSpecVRLGLUE3} instance. However, some task
 * descriptions are incompatible with some RL algorithms. If the underlying task
 * description is incompatible with the algorithm we are trying to instantiate,
 * then an exception is thrown.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RLGlueAlgorithmBuilder {
	
	public static class AlgorithmTaskMismatchException extends RuntimeException
	{
		private static final long serialVersionUID = -2639319624794695688L;
	
		public AlgorithmTaskMismatchException(String message)
		{
			super(message);
		}
	}

	/**
	 * Default argument for algorithms that make use of a learning rate.
	 */
	public static final LearningRate DEFAULT_LEARNING_RATE = new LearningRate.Constant(
			0.1);
	/**
	 * Defualt argument for algorithms that use the epsilon-greedy exploration
	 * strategy.
	 */
	public static final double DEFAULT_EPSILON_GREEDY = 0.05;
	/**
	 * Default argument for eligibility traces.
	 */
	public static final double DEFAULT_LAMBDA = 0.3;
	/**
	 * Default argument for algorithms that count the number of samples until
	 * performing an update.
	 */
	public static final int DEFAULT_NUM_VISITS_UNTIL_KNOWN = 10;

	private TaskSpecVRLGLUE3 _taskSpec;

	public RLGlueAlgorithmBuilder(TaskSpecVRLGLUE3 taskSpec) {
		if (taskSpec == null) {
			throw new NullPointerException("Cannot construct "
					+ this.getClass().getSimpleName()
					+ " from a null task specification.");
		}
		_taskSpec = taskSpec;
	}
	
	private void illegalConstruction(String message)
	{
		throw new AlgorithmTaskMismatchException(message);
	}

	/**
	 * Returns true if the underlying task specification is for a task with a
	 * discrete state space and false otherwise.
	 * 
	 * @return true if the task specification is for a discrete state space
	 *         task; otherwise false
	 */
	public boolean hasDiscreteStateSpace() {
		return _taskSpec.getNumContinuousObsDims() == 0;
	}

	/**
	 * Returns true if the underlying task specification is for a task where the
	 * states are described by a single integer. Returns false for any task
	 * specification that doesn't match.
	 * 
	 * @return true if the task description specifies single integer states;
	 *         otherwise false
	 */
	public boolean hasSingleIntegerStateSpace() {
		return (_taskSpec.getNumDiscreteObsDims() == 1)
				&& (_taskSpec.getNumContinuousObsDims() == 0);
	}

	/**
	 * Returns true if the underlying task specification is for a task with a
	 * discrete action space and otherwise false.
	 * 
	 * @return true if the task specification is for a discrete action space;
	 *         otherwise false
	 */
	public boolean hasDiscreteActionSpace() {
		return _taskSpec.getNumContinuousActionDims() == 0;
	}

	public boolean hasSingleIntegerActionSpace() {
		return (_taskSpec.getNumDiscreteActionDims() == 1)
				&& (_taskSpec.getNumContinuousActionDims() == 0);
	}

	/**
	 * Returns true if the specified task has bounded reward. False indicates
	 * that the reward range is unbounded or not specified.
	 * 
	 * @return true if the specified task has bounded reward; otherwise false
	 */
	public boolean hasBoundedReward() {
		DoubleRange rrange = _taskSpec.getRewardRange();
		return !(rrange.getMaxInf() && rrange.getMinInf());
	}
	
	public double discountFactor()
	{
		return _taskSpec.getDiscountFactor();
	}

	public QLearning qlearning(double learningRate, double epsilonGreedy) {
		if (hasSingleIntegerStateSpace() && hasSingleIntegerActionSpace()) {
			IntRange stateRange = _taskSpec.getDiscreteObservationRange(0);
			IntRange actionRange = _taskSpec.getDiscreteActionRange(0);
			
			if(stateRange.getMin() != 0){
				illegalConstruction("Cannot create QLearning with states out of range [0,N].");
			}
			int numStates = stateRange.getMax();
			if(actionRange.getMin() != 0){
				illegalConstruction("Cannot create QLearning with actions out of range [0,K]");
			}
			int numActions = actionRange.getMax();
			
			QLearning ql = new QLearning(numStates, numActions, learningRate, discountFactor());
			ql.setEpsilon(epsilonGreedy);
			return ql;
		}else{
			illegalConstruction("QLearning can only be constructed for tasks with discrete state and discrete action spaces.");
			return null;
		}
	}
	
	public QLearning qlearning()
	{
		return this.qlearning(DEFAULT_LEARNING_RATE.initValue(), DEFAULT_EPSILON_GREEDY);
	}

	public RMax rmax(int numVisitsUntilKnown) {
		if(hasSingleIntegerStateSpace() && hasSingleIntegerActionSpace()){
			IntRange stateRange = _taskSpec.getDiscreteObservationRange(0);
			IntRange actionRange = _taskSpec.getDiscreteActionRange(0);
			
			if(stateRange.getMin() != 0){
				illegalConstruction("Cannot create RMax with state range that beings with state other than 0.");
			}
			int numStates = stateRange.getMax();
			if(actionRange.getMin() != 0){
				illegalConstruction("Cannot create RMax with action range that begins with action other than 0.");
			}
			int numActions = actionRange.getMax();
			
			DoubleRange rewardRange = _taskSpec.getRewardRange();
			
			RMax rmax = new RMax(numStates, numActions, discountFactor(), new Interval(rewardRange.getMin(), rewardRange.getMax()), numVisitsUntilKnown);
			return rmax;
		}else{
			illegalConstruction("RMax can only be constructed for tasks with discrete state and discrete action spaces.");
			return null;
		}
	}
	
	public RMax rmax()
	{
		return this.rmax(DEFAULT_NUM_VISITS_UNTIL_KNOWN);
	}
}
