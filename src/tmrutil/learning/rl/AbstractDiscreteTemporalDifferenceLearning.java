package tmrutil.learning.rl;

import java.util.Arrays;
import tmrutil.learning.LearningRate;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Statistics;

/**
 * An abstract temporal difference learning algorithm for discrete reinforcement
 * learning problems. This class should be extended by reinforcement learning
 * algorithms that assume a discrete state and action space. It is assumed that
 * the states are numbered from 0 to <code>numStates</code> and the actions are
 * numbered from 0 to <code>numActions</code>.
 * 
 * @author Timothy A. Mann
 * 
 */
public abstract class AbstractDiscreteTemporalDifferenceLearning extends
		AbstractTemporalDifferenceLearning<Integer, Integer> {
	private int _numStates;
	private int _numActions;
	protected double[][] _Q;
	protected double[][] _U;

	public AbstractDiscreteTemporalDifferenceLearning(int numStates,
			int numActions, LearningRate learningRate, double discountFactor,
			Optimization opType) {
		super(learningRate, discountFactor, opType);
		if (numStates < 1) {
			throw new IllegalArgumentException(
					"The number of states must be positive.");
		}
		_numStates = numStates;
		if (numActions < 1) {
			throw new IllegalArgumentException(
					"The number of actions must be positive.");
		}
		_numActions = numActions;

		_Q = new double[numStates][numActions];
		_U = null;
	}

	public AbstractDiscreteTemporalDifferenceLearning(int numStates,
			int numActions, double learningRate, double discountFactor,
			Optimization opType) {
		this(numStates, numActions, new LearningRate.Constant(learningRate),
				discountFactor, opType);
	}

	/**
	 * Returns the action that maximizes the Q-table for a specified state.
	 * 
	 * @param state
	 *            a state
	 * @return the action that maximizes the Q-table for a specified state
	 */
	public final Integer maxAction(Integer state) {
		int bestA = 0;
		double bestVal = 0;
		int[] rperm = VectorOps.randperm(_numActions);
		for (int a = 0; a < numberOfActions(); a++) {
			double val = evaluateQ(state, rperm[a]);
			if (a == 0 || val > bestVal) {
				bestA = rperm[a];
				bestVal = val;
			}
		}
		return bestA;
	}

	/**
	 * Returns the value that maximizes the Q-table for a specified state.
	 * 
	 * @param state
	 *            a state
	 * @return the value that maximizes the Q-table for a specified state
	 */
	public final double maxQ(Integer state) {
		double bestVal = 0;
		int[] rperm = VectorOps.randperm(_numActions);
		for (int a = 0; a < numberOfActions(); a++) {
			double val = evaluateQ(state, rperm[a]);
			if (a == 0 || val > bestVal) {
				bestVal = val;
			}
		}
		return bestVal;
	}

	/**
	 * Implements a greedy policy with respect to the Q-values.
	 * 
	 * @param state
	 *            a state
	 * @return the greedy action with respect to the Q-values
	 */
	@Override
	public Integer policy(Integer state) {
		return maxAction(state);
	}

	public final double evaluateQ(Integer state, Integer action) {
		int s = state.intValue();
		int a = action.intValue();
		if (_U == null) {
			return _Q[s][a];
		} else {
			return Math.min(_Q[s][a], _U[s][a]);
		}
	}

	/**
	 * Sets the action-value of a specified state-action pair.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param value
	 *            the new action-value
	 */
	public final void setQ(Integer state, Integer action, double value) {
		_Q[state.intValue()][action.intValue()] = value;
	}

	/**
	 * Fills the Q-table with a specified value.
	 * 
	 * @param value
	 *            the value to fill the Q-table with
	 */
	public final void fillQ(double value) {
		for (int s = 0; s < numberOfStates(); s++) {
			Arrays.fill(_Q[s], value);
		}
	}

	/**
	 * Sets the entire Q-table to specified values
	 * 
	 * @param Q
	 *            a 2d array of Q-values
	 */
	public final void setQ(double[][] Q) {
		for (int s = 0; s < numberOfStates(); s++) {
			for (int a = 0; a < numberOfActions(); a++) {
				setQ(s, a, Q[s][a]);
			}
		}
	}

	/**
	 * Returns a reference to the underlying Q-table.
	 * 
	 * @return a reference to the underlying Q-table
	 */
	public final double[][] getQ() {
		return _Q;
	}

	/**
	 * Sets the admissible heuristic to the specified value.
	 * 
	 * @param U
	 *            the admissible heuristic to set
	 */
	public final void setAdmissibleHeuristic(double[][] U) {
		_U = new double[numberOfStates()][numberOfActions()];
		for (int s = 0; s < numberOfStates(); s++) {
			for (int a = 0; a < numberOfActions(); a++) {
				_U[s][a] = U[s][a];
			}
		}
	}

	/**
	 * Returns the admissible heuristic (if it is not null).
	 * 
	 * @return the admissible heuristic or null
	 */
	public final double[][] getAdmissibleHeuristic() {
		return _U;
	}

	/**
	 * Returns the number of states in the target MDP.
	 * 
	 * @return the number of states
	 */
	public final int numberOfStates() {
		return _numStates;
	}

	/**
	 * Returns the number of actions in the target MDP.
	 * 
	 * @return the number of actions
	 */
	public final int numberOfActions() {
		return _numActions;
	}

	/**
	 * Returns the deterministic greedy policy with respect to this agent's
	 * current action-value function estimates.
	 * 
	 * @return the greedy policy
	 */
	public Policy<Integer, Integer> greedyPolicy() 
	{
		int[] actionMap = new int[numberOfStates()];
		for(int s=0;s<numberOfStates();s++){
			actionMap[s] = maxAction(s);
		}
		DiscretePolicy p = new DiscretePolicy(actionMap);
		return p;
	}

}
