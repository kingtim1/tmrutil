package tmrutil.learning.rl;

import java.util.HashMap;
import java.util.Map;

public class DiscreteVFunction<S> implements VFunction<S> {

	private Map<S, Double> _values;
	private double _defaultValue;

	public DiscreteVFunction() {
		this(0.0);
	}

	public DiscreteVFunction(double defaultValue) {
		_values = new HashMap<S, Double>();
		_defaultValue = defaultValue;
	}
	
	public void update(S state, double value)
	{
		_values.put(state, value);
	}

	@Override
	public double value(S state) {
		Double v = _values.get(state);
		if (v == null) {
			return _defaultValue;
		} else {
			return v.doubleValue();
		}
	}

}
