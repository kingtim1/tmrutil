package tmrutil.learning.rl;

public enum InitializationType {OPTIMISTIC, PESSIMISTIC, RANDOM}