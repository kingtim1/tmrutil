package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.List;

import tmrutil.learning.rl.sim.Simulator;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.util.DataStore;
import tmrutil.util.FileDataStore;
import tmrutil.util.Interval;

/**
 * The A-ROGUE algorithm achieves small regret even in MDPs containing
 * state-action pairs with irreversible consequences.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ARogue implements LearningSystem<Integer, Integer> {
	/**
	 * The number of states in the MDP.
	 */
	private int _numStates;

	/**
	 * The number of actions in the MDP.
	 */
	private int _numActions;

	/**
	 * The simulator used to generate rollouts.
	 */
	private Simulator<Integer, ?, Integer> _sim;

	/**
	 * The horizon time of the algorithm.
	 */
	private int _T;

	/**
	 * The parameter controlling the probability that the confidence intervals
	 * will hold.
	 */
	private double _delta;

	/**
	 * Scales the confidence intervals.
	 */
	private double _alpha;

	/**
	 * The interval containing the minimum and maximum possible immediate
	 * rewards.
	 */
	private Interval _rewardInterval;

	/**
	 * The local diameter specified during instantiation of this algorithm.
	 */
	private Integer _localDiameter;

	/**
	 * The minimum uncertainty before we count the uncertainty as zero.
	 */
	private double _minUncertainty;

	/**
	 * Reference to a file data store instance for logging experiment data.
	 */
	private FileDataStore _dstore;
	/**
	 * Tag used to prefix all data bound to the data storage object.
	 */
	private static final String DSTORE_TAG = "arogue_";

	/*
	 * =========================================================
	 * 
	 * Main algorithm parameters.
	 * 
	 * ========================================================
	 */

	/**
	 * The current timestep.
	 */
	private int _time;

	/**
	 * Counts the number of times that each state-action pair has been visited.
	 * The counts are updated at the end of each episode.
	 */
	private int[][] _totalVisitCounts;

	/**
	 * Counts the number of times that a state-action pair transitions to a
	 * particular state.
	 */
	private int[][][] _nextStateCounts;

	/**
	 * Cumulates the reward received at each state-action pair.
	 */
	private double[][] _reward;

	/**
	 * Optimistic and pessimistic estimates of the optimal state-action value
	 * function.
	 */
	private ExtendedValueIteration.QFunction _qplus, _qminus;

	/**
	 * Optimistic and pessimistic estimates of the optimal state-action value
	 * function for the backup policy.
	 */
	private ExtendedValueIteration.QFunction _qBupPlus, _qBupMinus;

	/**
	 * Records the recovery cost between states.
	 */
	private double[][] _recoveryMat;

	private List<Integer> _realTrajectory;
	private List<Integer> _allTrajectory;
	private List<Double> _rewardHistory;
	private List<Integer> _isRewardFromRollout;

	/*
	 * =======================================================
	 * 
	 * Episode specific parameters.
	 * 
	 * ======================================================
	 */

	/**
	 * Keeps track of the number of the current episode.
	 */
	private int _episodeNumber;
	/**
	 * The timestep that the current episode started on.
	 */
	private int _episodeStartTime;

	/**
	 * The state that the agent started in in the current episode.
	 */
	private Integer _episodeStartState;

	/**
	 * The budget for rollouts, which is reallocated after each episode.
	 */
	private double _budget;

	/**
	 * Counts the number of times each state-action pair has been visited during
	 * the current episode.
	 */
	private int[][] _episodeVisitCounts;

	/**
	 * Keeps track of the cumulative reward received during each episode. This
	 * is not used by the algorithm, but is included for logging purposes.
	 */
	private double _episodeCumulativeReward;

	/**
	 * Keeps track of the number of rollout timesteps performed per episode.
	 */
	private int _episodeNumRolloutTimesteps;
	
	private Integer _lastQPlusAction;

	/*
	 * =====================================================
	 * 
	 * Methods
	 * 
	 * =====================================================
	 */

	/**
	 * Constructs a new instance of the SEUCRL algorithm.
	 * 
	 * @param numStates
	 *            the number of states
	 * @param numActions
	 *            the number of actions
	 * @param sim
	 *            the simulator used to generate rollouts
	 * @param T
	 *            the horizon time of the algorithm
	 * @param dstore
	 *            reference to a data storage instance for logging detailed
	 *            information about this algorithms execution
	 * @param localDiameter
	 *            specifies the local diameter of the MDP
	 */
	public ARogue(int numStates, int numActions,
			Simulator<Integer, ?, Integer> sim, double delta, double alpha,
			int T, Interval rewardInterval, FileDataStore dstore,
			Integer localDiameter, double minUncertainty) {
		_numStates = numStates;
		_numActions = numActions;
		_sim = sim;
		_delta = delta;
		_alpha = alpha;
		_rewardInterval = rewardInterval;
		_T = T;
		_localDiameter = localDiameter;
		_minUncertainty = minUncertainty;

		_dstore = dstore;

		init();
	}

	@Override
	public Integer policy(Integer state) {
		if (_qplus == null || _time > _T) {
			return 0;
		} else {
			return _qplus.policy(_time - _episodeStartTime, state);
		}
	}

	@Override
	public void train(Integer prevState, Integer action, Integer newState,
			double reinforcement) {
		_episodeCumulativeReward += reinforcement;
		_episodeVisitCounts[prevState][action]++;

		_reward[prevState][action] += reinforcement;
		_nextStateCounts[prevState][action][newState]++;

		_time++;

		if (_realTrajectory.isEmpty()) {
			_allTrajectory.add(prevState);
			_realTrajectory.add(prevState);
		}

		if (_time <= _T) {

			_allTrajectory.add(newState);
			_realTrajectory.add(newState);
			_rewardHistory.add(reinforcement);
			_isRewardFromRollout.add(0);

			if (stopEpisode(prevState, action)) {
				_episodeStartState = newState;
				initNewEpisode();
			}

			if (_episodeStartState == null) {
				_episodeStartState = prevState;
			}

			rolloutsUntilSafe(newState);

			if (_dstore != null) {
				_dstore.bind(DSTORE_TAG + "all_trajectory", _allTrajectory);
				_dstore.bind(DSTORE_TAG + "real_trajectory", _realTrajectory);
				_dstore.bind(DSTORE_TAG + "reward_history", _rewardHistory);
				_dstore.bind(DSTORE_TAG + "is_reward_from_rollout",
						_isRewardFromRollout);
			}
		} else {
			//System.err.println("Exausted T!");
		}

	}

	private void rolloutsUntilSafe(Integer newState) {
		while (bothRolloutTests(newState)) {
			// Perform some rollouts

			Integer simState = newState;
			Integer piPlusAction = _qplus.policy(_time - _episodeStartTime,
					newState);

			List<Integer> trajectory = new ArrayList<Integer>();
			trajectory.add(simState);

			for (int t = 0; t < 4 * estimateDiameter() && (t < _T - _time); t++) {
				_episodeNumRolloutTimesteps++;

				Integer simAction = null;
				if (t == 0) {
					simAction = piPlusAction;
				} else {
					simAction = _qBupPlus.policy(t, simState);
				}
				OptionOutcome<Integer, ?> outcome = _sim.samplePrimitive(
						simState, simAction);

				Integer newSimState = outcome.terminalState();
				double newR = outcome.dCumR();
				trajectory.add(newSimState);

				_allTrajectory.add(newSimState);
				_rewardHistory.add(newR);
				_isRewardFromRollout.add(1);

				_reward[simState][simAction] += newR;
				_episodeVisitCounts[simState][simAction]++;
				_nextStateCounts[simState][simAction][newSimState]++;

				_budget = _budget - 1;
				_time++;
				simState = newSimState;

				if (stopEpisode(simState, simAction)
						|| simState.equals(_episodeStartState)) {
					_episodeStartState = newState;
					initNewEpisode();
					break;
				}
			}
		}
	}

	private boolean bothRolloutTests(Integer state) {
		boolean qdelta = qDeltaTest(state);
		boolean recover = recoveryTest(state);
		return !(qdelta || recover);
	}

	/**
	 * Returns true if there is a small high probability path back to the
	 * <code>_episodeStartState</code> from the specified state.
	 * 
	 * @param state
	 *            the state to check whether there is a path back to the episode
	 *            start state from
	 * @return true if there is a path from the specified state back to the
	 *         episode start state; otherwise false
	 */
	private boolean recoveryTest(Integer state) {
		double threshold = 2 * estimateDiameter();

		updateBackupPolicy(_episodeStartState);
		Integer piPlusAction = _qplus.policy(_time - _episodeStartTime, state);
		double recoveryCost = -_qBupMinus.value(0, state, piPlusAction);

		return recoveryCost < threshold;
	}

	/**
	 * Returns true to indicate that the optimal action for the specified state
	 * is known. Returning false indicates that the optimal action for the
	 * specified state is not yet known.
	 * 
	 * @param state
	 *            a state
	 * @return true to indicate that the optimal action is known for the
	 *         specified state; otherwise false
	 */
	private boolean qDeltaTest(Integer state) {
		Integer piPlusAction = _qplus.policy(_time - _episodeStartTime, state);
		double qPlusMax = Double.NEGATIVE_INFINITY;
		double qMinusMax = _qminus.value(_time - _episodeStartTime, state,
				piPlusAction);
		for (int a = 0; a < _numActions; a++) {
			if (a != piPlusAction) {
				double q = _qplus.value(_time - _episodeStartTime, state, a);
				if (q > qPlusMax) {
					qPlusMax = q;
				}
			}
		}
		double qDeltaScore = qMinusMax - qPlusMax;
		boolean passed = (qMinusMax - qPlusMax > 2 * eta());

		if (passed) {
			System.err.println("Passed QDelta Test (" + state + ", "
					+ piPlusAction + ") with Score : " + qDeltaScore);
		}

		return passed;
	}

	/**
	 * Returns the value of the new budget for the next episode.
	 * 
	 * @return the value for the next episodes budget
	 */
	public double newBudget() {
		double budget = 0;

		double Dk = estimateDiameter();

		for (int s = 0; s < _numStates; s++) {
			for (int a = 0; a < _numActions; a++) {
				budget += totalVisitCounts(s, a)
						* (Dk * transitionProbabilityUncertainty(s, a) + 3 * rewardUncertainty(
								s, a));
			}
		}

		budget += 2 * Dk + (2 * Dk + _episodeStartTime) * eta();

		budget += Dk
				* Math.sqrt(_episodeStartTime
						* Math.log(_T * _T / _delta) / 2);

		return Math.min(budget, 4 * estimateDiameter());
		//return 4 * estimateDiameter();
	}

	/**
	 * Returns true if the current episode should be terminated. Otherwise false
	 * is returned indicating that the current episode should continue.
	 * 
	 * @return true if the current episode should terminate; otherwise false
	 */
	public boolean stopEpisode(Integer state, Integer action) {
		double n = Math.max(0.5, totalVisitCounts(state, action));
		int en = episodeVisitCounts(state, action);
		if (_budget <= 0 || en >= 2 * n) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the average reward for a state-action pair.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return average reward for <code>(state, action)</code>
	 */
	public double reward(Integer state, Integer action) {
		int n = totalVisitCounts(state, action);
		if (n == 0) {
			return _rewardInterval.getMax();
		} else {
			return _reward[state][action] / n;
		}
	}

	public double rewardUncertainty(Integer state, Integer action) {
		int nSA = totalVisitCounts(state, action);
		if(nSA == 0){
			return _rewardInterval.getDiff();
		}
		
		double ci1 = Math.max(
				0,
				Math.log(2. * _numStates * _numActions * _episodeStartTime
						/ _delta));
		double ci2 = Math.sqrt(ci1);

		double dr = Math.sqrt(3.5 / nSA) * ci2;

		double u = Math.min(_rewardInterval.getDiff(), _alpha * dr);
		if (u < _minUncertainty) {
			return 0;
		} else {
			return u;
		}
	}

	/**
	 * Returns the state transition probability for a state-action pair and
	 * resulting state.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param resultState
	 *            another state
	 * @return the probability of executing <code>(state, action)</code> and
	 *         transitioning to <code>resultState</code>
	 */
	public double transitionProbability(Integer state, Integer action,
			Integer resultState) {
		int n = totalVisitCounts(state, action);
		if (n == 0) {
			if (state.equals(resultState)) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return _nextStateCounts[state][action][resultState] / (double) n;
		}
	}

	public double transitionProbabilityUncertainty(Integer state, Integer action) {
		int nSA = totalVisitCounts(state, action);
		if(nSA == 0){
			return 1;
		}
		
		double ci1 = Math.max(
				0,
				Math.log(2. * _numStates * _numActions * _episodeStartTime
						/ _delta));
		double ci2 = Math.sqrt(ci1);

		double dp = Math.sqrt(14. * _numStates
				/ nSA)
				* ci2;
		double u = Math.min(_alpha * dp, 1);
		if (u < _minUncertainty) {
			return 0;
		} else {
			return u;
		}
	}

	/**
	 * Returns the number of times that a specified state-action pair has been
	 * visited throughout this agent's lifetime not including the current
	 * episode.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the total number of visits to <code>(state, aciton)</code>
	 */
	public int totalVisitCounts(Integer state, Integer action) {
		return _totalVisitCounts[state][action];
	}

	/**
	 * Returns the number of times that a specified state-action pair has been
	 * visited during the current episode.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the number of times that <code>(state, action)</code> has been
	 *         visited during the current episode
	 */
	public int episodeVisitCounts(Integer state, Integer action) {
		return _episodeVisitCounts[state][action];
	}

	/**
	 * Returns the agent's current timestep.
	 * 
	 * @return the agent's current timestep
	 */
	public int timestep() {
		return _time;
	}

	public double eta() {
		return 0.0; // 0.05; //Math.sqrt(_numStates * _numActions) * (1.0 / Math.sqrt(_time)); // 0.05;
	}

	/**
	 * Returns an estimate of the diameter of the MDP.
	 * 
	 * @return an estimate of the diameter
	 */
	public int estimateDiameter() {
		if (_localDiameter == null) {
			int Dk = (int) Math.ceil(Math.log(_episodeStartTime));
			return Math.max(Dk, 5);
		} else {
			return _localDiameter;
		}
	}

	private void init() {
		if (_dstore != null) {
			_dstore.bind(DSTORE_TAG + "num_states", _numStates);
			_dstore.bind(DSTORE_TAG + "num_actions", _numActions);
			_dstore.bind(DSTORE_TAG + "delta", _delta);
			_dstore.bind(DSTORE_TAG + "confidence_interval_scaling", _alpha);
			_dstore.bind(DSTORE_TAG + "reward_min", _rewardInterval.getMin());
			_dstore.bind(DSTORE_TAG + "reward_max", _rewardInterval.getMax());
			_dstore.bind(DSTORE_TAG + "T", _T);
		}

		_totalVisitCounts = new int[_numStates][_numActions];
		_nextStateCounts = new int[_numStates][_numActions][_numStates];
		_reward = new double[_numStates][_numActions];
		_episodeVisitCounts = new int[_numStates][_numActions];

		_recoveryMat = new double[_numStates][_numStates];
		for (int s1 = 0; s1 < _numStates; s1++) {
			for (int s2 = 0; s2 < _numStates; s2++) {
				_recoveryMat[s1][s2] = Double.POSITIVE_INFINITY;
			}
		}

		_rewardHistory = new ArrayList<Double>();
		_isRewardFromRollout = new ArrayList<Integer>();
		_realTrajectory = new ArrayList<Integer>();
		_allTrajectory = new ArrayList<Integer>();

		_episodeNumber = 0;
		_episodeStartTime = 0;
		_episodeStartState = null;
		_episodeNumRolloutTimesteps = 0;

		_time = 1;
		_budget = 0;

		initNewEpisode();
	}

	private void initNewEpisode() {
		_episodeNumber++;
		_episodeStartTime = _time;

		System.out.println("Episode : " + _episodeNumber + ", Time : " + _time);

		double oldBudget = _budget;
		_budget = newBudget();

		if (_dstore != null) {
			_dstore.appendRow(DSTORE_TAG + "episode_start_time",
					new int[] { _episodeStartTime });
			_dstore.appendRow(DSTORE_TAG + "budget_at_episode_termination",
					new double[] { oldBudget });
			_dstore.appendRow(DSTORE_TAG + "budget_at_episode_initialization",
					new double[] { _budget });
			_dstore.appendRow(DSTORE_TAG
					+ "cumulative_reward_at_episode_termination",
					new double[] { _episodeCumulativeReward });
			_dstore.bind(DSTORE_TAG + "num_episodes", _episodeNumber);
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber
					+ "_num_rollouts", _episodeNumRolloutTimesteps);
		}

		_episodeCumulativeReward = 0;
		_episodeNumRolloutTimesteps = 0;

		double[][] r = new double[_numStates][_numActions];
		double[][] rd = new double[_numStates][_numActions];

		double[][][] p = new double[_numStates][_numActions][_numStates];
		double[][] pd = new double[_numStates][_numActions];

		for (int s = 0; s < _numStates; s++) {
			for (int a = 0; a < _numActions; a++) {
				_totalVisitCounts[s][a] += _episodeVisitCounts[s][a];
				_episodeVisitCounts[s][a] = 0;

				r[s][a] = reward(s, a);
				rd[s][a] = rewardUncertainty(s, a);
				for (int ns = 0; ns < _numStates; ns++) {
					p[s][a][ns] = transitionProbability(s, a, ns);
				}
				pd[s][a] = transitionProbabilityUncertainty(s, a);
			}
		}

		if (_dstore != null) {
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber
					+ "_total_visit_counts", _totalVisitCounts);
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber
					+ "_reward_uncertainty", rd);
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber
					+ "_transition_uncertainty", pd);
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber
					+ "_recovery_costs", _recoveryMat);
		}

		ExtendedValueIteration evi = new ExtendedValueIteration(r, rd, p, pd,
				_rewardInterval);
		_qplus = evi.solveQOptimistic(_T - (_time - 1));
		_qminus = evi.solveQPessimistic(_T - (_time - 1));

		if (_dstore != null) {
			double[] vDeltas = new double[_numStates];
			for (int s = 0; s < _numStates; s++) {
				vDeltas[s] = _qplus.value(0, s) - _qminus.value(0, s);
			}
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber + "_vdeltas",
					vDeltas);
		}

	}

	private void updateBackupPolicy(Integer backupState) {
		double[][] r = new double[_numStates + 2][_numActions];
		double[][] rd = new double[_numStates + 2][_numActions];

		int targetState = _numStates;
		int blackHole = _numStates + 1;

		for (int a = 0; a < _numActions; a++) {
			r[targetState][a] = 0;
			r[blackHole][a] = -1;
		}

		double[][][] p = new double[_numStates + 2][_numActions][_numStates + 2];
		double[][] pd = new double[_numStates + 2][_numActions];

		for (int a = 0; a < _numActions; a++) {
			p[targetState][a][targetState] = 1;
			p[blackHole][a][blackHole] = 1;
			pd[targetState][a] = 0;
			pd[blackHole][a] = 0;
		}

		for (int s = 0; s < _numStates; s++) {
			for (int a = 0; a < _numActions; a++) {
				r[s][a] = -1;

				double nsa = _totalVisitCounts[s][a]
						+ _episodeVisitCounts[s][a];

				if (nsa > 0) {
					for (int ns = 0; ns < _numStates; ns++) {
						if (ns == backupState) {
							p[s][a][_numStates] = _nextStateCounts[s][a][ns]
									/ (double) nsa;
						} else {
							p[s][a][ns] = _nextStateCounts[s][a][ns]
									/ (double) nsa;
						}
					}
				} else {
					p[s][a][blackHole] = 1;
				}
				pd[s][a] = transitionProbabilityUncertainty(s, a);
			}
		}

		ExtendedValueIteration evi = new ExtendedValueIteration(r, rd, p, pd,
				new Interval(-1, 0));
		int Dk = estimateDiameter();
		int horizon = 4 * Dk; // Should the horizon be T - time?
		_qBupPlus = evi.solveQOptimistic(horizon); 
		_qBupMinus = evi.solveQPessimistic(horizon);

		for (int s = 0; s < _numStates; s++) {
			Integer piPlusAction = _qplus.policy(_time - _episodeStartTime, s);
			double recoveryCost = -_qBupMinus.value(0, s, piPlusAction);
			if (recoveryCost < _recoveryMat[s][_episodeStartState]) {
				_recoveryMat[s][_episodeStartState] = recoveryCost;
			}
		}

	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}
}
