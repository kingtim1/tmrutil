package tmrutil.learning.rl.tasks;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.Task;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;

/**
 * Puddle world is a simple continuous, benchmark reinforcement learning task.
 * The agent must travel to a goal location while avoiding the puddle which has
 * a high penalty.
 * 
 * @author Timothy A. Mann
 * 
 */
public class PuddleWorld extends Task<double[], Integer> implements Drawable
{
	/**
	 * Represents a single puddle in puddle world. Provides convenience methods
	 * for calculating the cost of entering a puddle.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	private class Puddle implements Drawable
	{
		private Ellipse2D _ellipse;
		private Line2D _center;
		private double _radius;

		/**
		 * Creates a puddle from a line segment and puddle radius.
		 * 
		 * @param x1
		 *            the X-coordinate of point 1
		 * @param y1
		 *            the Y-coordinate of point 1
		 * @param x2
		 *            the X-coordinate of point 2
		 * @param y2
		 *            the Y-coordinate of point 2
		 * @param radius
		 *            the radius of the ellipse around the line segment
		 */
		public Puddle(double x1, double y1, double x2, double y2, double radius)
		{
			this(new Line2D.Double(x1, y1, x2, y2), radius);
		}

		/**
		 * Constructs a puddle from a line segment and a radius.
		 * 
		 * @param center
		 *            a line segment determining the location and length of the
		 *            puddle
		 * @param radius
		 *            the radius of the ellipse around the line segment
		 */
		public Puddle(Line2D center, double radius)
		{
			_center = center;
			double x = Math.min(_center.getX1(), _center.getX2()) - radius;
			double y = Math.min(_center.getY1(), _center.getY2()) - radius;
			double width = Math.abs(_center.getX1() - _center.getX2())
					+ (2 * radius);
			double height = Math.abs(_center.getY1() - _center.getY2())
					+ (2 * radius);

			_ellipse = new Ellipse2D.Double(x, y, width, height);
			_radius = radius;
		}

		/**
		 * Determines if a point is contained within this puddle.
		 * 
		 * @param point
		 *            a point
		 * @return true if the point is contained by this puddle; otherwise
		 *         false
		 */
		public boolean contains(Point2D point)
		{
			return _ellipse.contains(point);
		}

		/**
		 * Calculates the penetration depth of the point into the puddle. This
		 * method is only well defined if the point is inside the puddle!
		 * 
		 * @param point
		 *            a point inside this puddle
		 * @return a scalar determining the penetration depth of the point
		 *         inside the puddle
		 */
		public double depth(Point2D point)
		{
			double dist = _center.ptLineDist(point);
			return _radius - dist;
		}

		@Override
		public void draw(Graphics2D g, int width, int height)
		{
			g.draw(_ellipse);
		}
	}

	private Point2D _agent;
	private List<Puddle> _puddles;
	private boolean _finished;

	/**
	 * Constructs an instance of the puddle world task.
	 */
	public PuddleWorld()
	{
		_agent = new Point2D.Double();
		_puddles = new ArrayList<Puddle>();
		Puddle p1 = new Puddle(0.1, 0.75, 0.45, 0.75, 0.1);
		_puddles.add(p1);
		Puddle p2 = new Puddle(0.45, 0.4, 0.45, 0.8, 0.1);
		_puddles.add(p2);

		reset();
	}

	@Override
	public double evaluate()
	{
		if (inGoal()) {
			return 1;
		} else {
			double r = -1;
			for (Puddle p : _puddles) {
				double depth = p.depth(_agent);
				if (p.contains(_agent)) {
					r = Math.min(r, -1 - 400 * depth);
				}
			}
			return r;
		}
	}

	@Override
	public void execute(Integer action)
	{
		if (inGoal()) {
			_finished = true;
			return;
		}

		double dx = 0;
		double dy = 0;
		double actionMag = 0.05;
		switch (action) {
		case 0:
			dx = -actionMag;
			break;
		case 1:
			dx = actionMag;
			break;
		case 2:
			dy = -actionMag;
			break;
		case 3:
			dy = actionMag;
			break;
		default:
			throw new IllegalArgumentException("The specified action ("
					+ action + ") is not supported.");
		}

		double sigma = 0.01;
		double newX = _agent.getX() + dx + Random.normal(0, sigma);
		double newY = _agent.getY() + dy + Random.normal(0, sigma);
		if (newX < 0) {
			newX = 0;
		}
		if (newX > 1) {
			newX = 1;
		}
		if (newY < 0) {
			newY = 0;
		}
		if (newY > 1) {
			newY = 1;
		}
		_agent.setLocation(newX, newY);
	}
	
	public Set<Integer> validActions()
	{
		Set<Integer> validActions = new HashSet<Integer>();
		for(int a=0;a<numberOfActions();a++){
			validActions.add(a);
		}
		return validActions;
	}
	
	public int numberOfActions()
	{
		return 4;
	}

	@Override
	public double[] getState()
	{
		return new double[] { _agent.getX(), _agent.getY() };
	}

	@Override
	public boolean isFinished()
	{
		return _finished;
	}

	/**
	 * Determines if the agent is in the goal region. Returns true if the agent
	 * is in the goal region. Otherwise false is returned.
	 * 
	 * @return true if the agent is in the goal region; otherwise false is
	 *         returned
	 */
	public boolean inGoal()
	{
		return _agent.getX() + _agent.getY() >= 0.95 + 0.95;
	}

	@Override
	public void reset()
	{
		_finished = false;
		double x = Random.uniform(0.0, 0.95);
		double y = Random.uniform(0.0, 0.95);
		_agent.setLocation(x, y);
	}
	
	public void plotValueFunction(AbstractTemporalDifferenceLearning<double[],Integer> agent)
	{
		Set<Integer> validActions = validActions();
		double[] xs = VectorOps.range(0, 1, 100);
		double[] ys = VectorOps.range(0, 1, 100);
		double[][] V = new double[ys.length][xs.length];
		for(int xi = 0; xi < xs.length; xi++){
			for(int yi = 0; yi < ys.length; yi++){
				double[] state = {xs[xi], ys[yi]};
				double maxQ = Double.NEGATIVE_INFINITY;
				for(Integer action : validActions){
					double q = agent.evaluateQ(state, action);
					if(q > maxQ){
						maxQ = q;
					}
				}
				V[(ys.length-1) - yi][xi] = maxQ;
			}
		}
		
		MatrixOps.displayGraphically(V, "PuddleWorld Value Function");
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();

		AffineTransform xform = new AffineTransform();
		double scale = Math.min(width, height);
		xform.scale(scale, -scale);
		xform.translate(0, -1);

		g2.transform(xform);
		g2.setStroke(new BasicStroke((float) (1 / scale)));

		for (Puddle puddle : _puddles) {
			puddle.draw(g2, width, height);
		}
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 1, 1);
		g2.draw(rect);
		Line2D goal = new Line2D.Double(0.95, 1, 1, 0.95);
		g2.draw(goal);

		double diameter = 0.05;
		double radius = diameter / 2;
		Ellipse2D agentCircle = new Ellipse2D.Double(_agent.getX() - radius,
				_agent.getY() - radius, diameter, diameter);
		g2.draw(agentCircle);

		g2.dispose();
	}

}
