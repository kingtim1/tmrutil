package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.util.Interval;

/**
 * A partially observable sorting task.
 * @author Timothy A. Mann
 *
 */
public class PartiallyObservableCursorSortTask extends DiscreteTask implements Drawable
{
	private CursorSortTask _sortDriver;
	
	public PartiallyObservableCursorSortTask(int numElms)
	{
		super(3, 3, new Interval(-(numElms-1), 0));
		_sortDriver = new CursorSortTask(numElms);
	}

	@Override
	public Integer getState()
	{
		int[] state = _sortDriver.getState();
		int cursor = state[state.length-1];
		int v1 = state[cursor];
		int v2 = state[cursor+1];
		if(v1 > v2){
			return 0;
		}else if(v1 == v2){
			return 1;
		}else{
			return 2;
		}
	}

	@Override
	public void execute(Integer action)
	{
		_sortDriver.execute(action);
	}

	@Override
	public double evaluate()
	{
		return _sortDriver.evaluate();
	}

	@Override
	public void reset()
	{
		_sortDriver.reset();
	}

	@Override
	public boolean isFinished()
	{
		return _sortDriver.isFinished();
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_sortDriver.draw(g, width, height);
	}

}
