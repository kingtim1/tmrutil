package tmrutil.learning.rl.tasks;

public enum Representation {ABSOLUTE, RELATIVE, FULL}