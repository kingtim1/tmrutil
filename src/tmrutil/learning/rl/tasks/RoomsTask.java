package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import tmrutil.learning.rl.SelectiveOptimismMDPEstimator.CloseDetector;
import tmrutil.stats.Filter;
import tmrutil.stats.Random;
import tmrutil.util.Pair;

/**
 * Implementation of the rooms task.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RoomsTask extends AbstractGridworld
{	
	private static final int SIZE = 11;

	private static enum Action {
		NORTH, SOUTH, EAST, WEST
	};

	private static final int NUM_ACTIONS = Action.values().length;
	private static final Color AGENT_COLOR = Color.BLUE;

	private static enum CellType {
		EMPTY, WALL, RED_HERRING, GOAL
	};

	private static final Color[] CELL_COLORS = { Color.LIGHT_GRAY, Color.BLACK,
			Color.RED, Color.YELLOW };

	private static final double SUCCEED_PROB = 0.8;

	/**
	 * Constructs a default Red Herring domain instance.
	 */
	public RoomsTask()
	{
		super(SIZE, SIZE, buildCells(), NUM_ACTIONS, AGENT_COLOR,
				CellType.values().length, CELL_COLORS,
				buildTerminatingCellTypes(), buildImpassibleCellTypes(),
				buildCellTypeTransitionProbabilities(SUCCEED_PROB,
						(1 - SUCCEED_PROB) / 2), buildCellTypeRewardMeans(),
				new double[CellType.values().length]);
	}

	private static final int[][] buildCells()
	{
		int[][] cells = new int[SIZE][SIZE];

		int empty = indexOf(CellType.EMPTY, CellType.values());
		int wall = indexOf(CellType.WALL, CellType.values());
		int goal = indexOf(CellType.GOAL, CellType.values());

		// Set everything to empty
		for (int r = 0; r < SIZE; r++) {
			for (int c = 0; c < SIZE; c++) {
				cells[r][c] = empty;
			}
		}

		// Add the walls
		cells[0][5] = wall;
		cells[1][5] = wall;
		cells[3][5] = wall;
		cells[4][5] = wall;
		cells[5][0] = wall;
		cells[5][2] = wall;
		cells[5][3] = wall;
		cells[5][4] = wall;
		cells[5][5] = wall;
		cells[6][5] = wall;
		cells[6][6] = wall;
		cells[6][7] = wall;
		cells[6][9] = wall;
		cells[6][10] = wall;
		cells[7][5] = wall;
		cells[8][5] = wall;
		cells[10][5] = wall;

		// Add the goal cells
		cells[9][10] = goal;

		return cells;
	}

	private static final Set<Integer> buildTerminatingCellTypes()
	{
		int redHerring = indexOf(CellType.RED_HERRING, CellType.values());
		int goal = indexOf(CellType.GOAL, CellType.values());
		Set<Integer> terminatingCellTypes = new TreeSet<Integer>();
		terminatingCellTypes.add(redHerring);
		terminatingCellTypes.add(goal);
		return terminatingCellTypes;
	}

	private static final Set<Integer> buildImpassibleCellTypes()
	{
		int wall = indexOf(CellType.WALL, CellType.values());
		Set<Integer> impassibleCellTypes = new TreeSet<Integer>();
		impassibleCellTypes.add(wall);
		return impassibleCellTypes;
	}

	private static final double[] buildCellTypeRewardMeans()
	{
		double[] rewards = new double[CellType.values().length];
		int empty = indexOf(CellType.EMPTY, CellType.values());
		int wall = indexOf(CellType.WALL, CellType.values());
		int redHerring = indexOf(CellType.RED_HERRING, CellType.values());
		int goal = indexOf(CellType.GOAL, CellType.values());

		rewards[empty] = -1;
		rewards[wall] = -1;
		rewards[redHerring] = 0;
		rewards[goal] = 20;
		return rewards;
	}

	private static final double[][][] buildCellTypeTransitionProbabilities(
			double succeedProb, double halfFailProb)
	{
		int numOutcomes = AbstractGridworld.Outcome.values().length;
		double[][][] tprobs = new double[CellType.values().length][NUM_ACTIONS][numOutcomes];

		int empty = indexOf(CellType.EMPTY, CellType.values());
		int wall = indexOf(CellType.WALL, CellType.values());
		int redHerring = indexOf(CellType.RED_HERRING, CellType.values());
		int goal = indexOf(CellType.GOAL, CellType.values());

		AbstractGridworld.Outcome[] oValues = AbstractGridworld.Outcome.values();
		int stayOutcome = indexOf(AbstractGridworld.Outcome.STAY, oValues);
		int northOutcome = indexOf(AbstractGridworld.Outcome.NORTH, oValues);
		int southOutcome = indexOf(AbstractGridworld.Outcome.SOUTH, oValues);
		int eastOutcome = indexOf(AbstractGridworld.Outcome.EAST, oValues);
		int westOutcome = indexOf(AbstractGridworld.Outcome.WEST, oValues);
		int northEastOutcome = indexOf(AbstractGridworld.Outcome.NORTH_EAST,
				oValues);
		int northWestOutcome = indexOf(AbstractGridworld.Outcome.NORTH_WEST,
				oValues);
		int southEastOutcome = indexOf(AbstractGridworld.Outcome.SOUTH_EAST,
				oValues);
		int southWestOutcome = indexOf(AbstractGridworld.Outcome.SOUTH_WEST,
				oValues);

		// Fill in the wall, red herring, and goal transition probabilities
		for (int action = 0; action < NUM_ACTIONS; action++) {
			tprobs[wall][action][stayOutcome] = 1;
			tprobs[redHerring][action][stayOutcome] = 1;
			tprobs[goal][action][stayOutcome] = 1;
		}

		int northAct = indexOf(Action.NORTH, Action.values());
		int southAct = indexOf(Action.SOUTH, Action.values());
		int eastAct = indexOf(Action.EAST, Action.values());
		int westAct = indexOf(Action.WEST, Action.values());

		// Fill in the empty cell transition probabilities
		tprobs[empty][northAct][northOutcome] = succeedProb;
		//tprobs[empty][northAct][eastOutcome] = halfFailProb;
		//tprobs[empty][northAct][westOutcome] = halfFailProb;
		tprobs[empty][northAct][northEastOutcome] = halfFailProb;
		tprobs[empty][northAct][northWestOutcome] = halfFailProb;

		tprobs[empty][southAct][southOutcome] = succeedProb;
		//tprobs[empty][southAct][eastOutcome] = halfFailProb;
		//tprobs[empty][southAct][westOutcome] = halfFailProb;
		tprobs[empty][southAct][southEastOutcome] = halfFailProb;
		tprobs[empty][southAct][southWestOutcome] = halfFailProb;

		tprobs[empty][eastAct][eastOutcome] = succeedProb;
		//tprobs[empty][eastAct][northOutcome] = halfFailProb;
		//tprobs[empty][eastAct][southOutcome] = halfFailProb;
		tprobs[empty][eastAct][northEastOutcome] = halfFailProb;
		tprobs[empty][eastAct][southEastOutcome] = halfFailProb;

		tprobs[empty][westAct][westOutcome] = succeedProb;
		//tprobs[empty][westAct][northOutcome] = halfFailProb;
		//tprobs[empty][westAct][southOutcome] = halfFailProb;
		tprobs[empty][westAct][northWestOutcome] = halfFailProb;
		tprobs[empty][westAct][southWestOutcome] = halfFailProb;

		return tprobs;
	}
	
	public static final Filter<Integer> goalOnlyEnvelope()
	{
		final List<Pair<Integer,Integer>> states = new ArrayList<Pair<Integer,Integer>>();
		states.add(new Pair<Integer, Integer>(9, 10));
		
		Filter<Integer> efunc = new Filter<Integer>()
		{
			Set<Integer> _envelope = buildEnvelope(states);
			
			@Override
			public boolean accept(Integer x)
			{
				return _envelope.contains(x);
			}
		};
		return efunc;
	}
	
	public static final Filter<Integer> highlyDirectedEnvelope()
	{
		final List<Pair<Integer,Integer>> states = new ArrayList<Pair<Integer,Integer>>();
		for(int col = 0; col < SIZE; col++){
			states.add(new Pair<Integer,Integer>(9, col));
		}
		for(int row = 0; row < SIZE; row++){
			states.add(new Pair<Integer,Integer>(row, 1));
		}
		
		Filter<Integer> efunc = new Filter<Integer>()
		{
			Set<Integer> _envelope = buildEnvelope(states);
			
			@Override
			public boolean accept(Integer x)
			{
				return _envelope.contains(x);
			}
		};
		
		return efunc;
	}
	
	public static final Filter<Integer> directedEnvelope()
	{
		final List<Pair<Integer,Integer>> states = new ArrayList<Pair<Integer,Integer>>();
		for(int col = 0; col < SIZE; col++){
			states.add(new Pair<Integer,Integer>(9, col));
			states.add(new Pair<Integer,Integer>(2, col));
		}
		for(int row = 0; row < SIZE; row++){
			states.add(new Pair<Integer,Integer>(row, 1));
			states.add(new Pair<Integer,Integer>(row, 8));
		}
		
		Filter<Integer> efunc = new Filter<Integer>()
		{
			Set<Integer> _envelope = buildEnvelope(states);
			
			@Override
			public boolean accept(Integer x)
			{
				return _envelope.contains(x);
			}
		};
		
		return efunc;
	}
	
	public static final Filter<Integer> incompleteEnvelope(double skipProb)
	{
		final List<Pair<Integer,Integer>> states = new ArrayList<Pair<Integer,Integer>>();
		for(int col = 0; col < SIZE; col++){
			if(Random.uniform() > skipProb){
				states.add(new Pair<Integer,Integer>(9, col));
			}
			if(Random.uniform() <= skipProb){
				states.add(new Pair<Integer,Integer>(2, col));
			}
		}
		for(int row = 0; row < SIZE; row++){
			if(Random.uniform() > skipProb){
				states.add(new Pair<Integer,Integer>(row, 1));
			}
			if(Random.uniform() > skipProb){
				states.add(new Pair<Integer,Integer>(row, 8));
			}
		}
		// Ensure that the goal state is in the envelope
		states.add(new Pair<Integer,Integer>(9, 10));
		
		Filter<Integer> efunc = new Filter<Integer>()
		{
			Set<Integer> _envelope = buildEnvelope(states);
			
			@Override
			public boolean accept(Integer x)
			{
				return _envelope.contains(x);
			}
		};
		
		return efunc;
	}
	
	public static final Filter<Integer> fullEnvelope()
	{
		return new Filter<Integer>()
		{
			@Override
			public boolean accept(Integer x)
			{
				return true;
			}
			
		};
	}
	
	private static final Set<Integer> buildEnvelope(List<Pair<Integer,Integer>> states)
	{
		Set<Integer> envelope = new TreeSet<Integer>();
		for(int i=0;i<states.size();i++){
			Pair<Integer,Integer> location = states.get(i);
			int state = location.getB() * SIZE + location.getA();
			envelope.add(state);
		}
		return envelope;
	}
	
	@Override
	public Integer getState()
	{
		Pair<Integer, Integer> agent = agentLocation();
		return agent.getB() * cols() + agent.getA();
	}

	@Override
	public void resetImpl()
	{
		int row = Random.nextInt(5);
		int col = Random.nextInt(5);
		agentLocation(row, col);
	}

}
