package tmrutil.learning.rl.tasks;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.QLearning;
import tmrutil.learning.rl.RMax;
import tmrutil.learning.rl.Task;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;
import tmrutil.util.FourTuple;
import tmrutil.util.Interval;

/**
 * Implements the Taxi domain benchmark reinforcement learning task. The
 * objective in the taxi domain is to navigate to a passenger's location (at one
 * of four predetermined landmarks) and then drop the passenger off at one of
 * four predetermined destinations.
 * 
 * @author Timothy A. Mann
 * 
 */
public class TaxiTask extends DiscreteTask implements Drawable
{

	private static final int TAXI_WIDTH = 5;
	private static final int TAXI_HEIGHT = 5;
	private static final int NUM_STATES = TAXI_WIDTH * TAXI_HEIGHT * (Landmark.values().length + 1)
	* Landmark.values().length;
	
	private static final Interval REWARD_INTERVAL = new Interval(-10, 20);

	private static final double NAVIGATE_STRAIGHT_PROBABILITY = 0.8;
	private static final double NAVIGATE_CORNER_PROBABILITY = (1.0 - NAVIGATE_STRAIGHT_PROBABILITY) / 2.0;

	public static enum Action {
		NORTH, SOUTH, EAST, WEST, PICKUP, DROP;

		public boolean isNavigationAction()
		{
			return (this.equals(NORTH) || this.equals(SOUTH)
					|| this.equals(EAST) || this.equals(WEST));
		}

		public Point delta()
		{
			if (isNavigationAction()) {
				switch (this) {
				case NORTH:
					return deltaNorth();
				case SOUTH:
					return deltaSouth();
				case EAST:
					return deltaEast();
				case WEST:
					return deltaWest();
				}
			}
			return new Point(0, 0);
		}

		private Point deltaNorth()
		{
			double sprobCumulative = NAVIGATE_STRAIGHT_PROBABILITY;
			double c1probCumulative = sprobCumulative
					+ NAVIGATE_CORNER_PROBABILITY;

			double rand = Random.uniform();
			if (rand < sprobCumulative) {
				return new Point(0, 1);
			} else if (rand < c1probCumulative) {
				return new Point(1, 1);
			} else {
				return new Point(-1, 1);
			}
		}

		private Point deltaSouth()
		{
			double sprobCumulative = NAVIGATE_STRAIGHT_PROBABILITY;
			double c1probCumulative = sprobCumulative
					+ NAVIGATE_CORNER_PROBABILITY;

			double rand = Random.uniform();
			if (rand < sprobCumulative) {
				return new Point(0, -1);
			} else if (rand < c1probCumulative) {
				return new Point(1, -1);
			} else {
				return new Point(-1, -1);
			}
		}

		private Point deltaEast()
		{
			double sprobCumulative = NAVIGATE_STRAIGHT_PROBABILITY;
			double c1probCumulative = sprobCumulative
					+ NAVIGATE_CORNER_PROBABILITY;

			double rand = Random.uniform();
			if (rand < sprobCumulative) {
				return new Point(1, 0);
			} else if (rand < c1probCumulative) {
				return new Point(1, 1);
			} else {
				return new Point(1, -1);
			}
		}

		private Point deltaWest()
		{
			double sprobCumulative = NAVIGATE_STRAIGHT_PROBABILITY;
			double c1probCumulative = sprobCumulative
					+ NAVIGATE_CORNER_PROBABILITY;

			double rand = Random.uniform();
			if (rand < sprobCumulative) {
				return new Point(-1, 0);
			} else if (rand < c1probCumulative) {
				return new Point(-1, 1);
			} else {
				return new Point(-1, -1);
			}
		}

		public static final Action mapAction(Integer action)
		{
			switch (action) {
			case 0:
				return NORTH;
			case 1:
				return SOUTH;
			case 2:
				return EAST;
			case 3:
				return WEST;
			case 4:
				return PICKUP;
			case 5:
				return DROP;
			}
			throw new IllegalArgumentException("The specified action ("
					+ action + ") is invalid.");
		}
	};

	/**
	 * An enumeration of the four landmarks (RED, BLUE, GREEN, and YELLOW) in
	 * the taxi domain.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static enum Landmark {
		RED, BLUE, GREEN, YELLOW;

		public Point getLocation()
		{
			switch (this) {
			case RED:
				return new Point(0, 4);
			case BLUE:
				return new Point(3, 0);
			case GREEN:
				return new Point(4, 4);
			case YELLOW:
				return new Point(0, 0);
			}
			throw new DebugException("This landmark is not supported!");
		}

		public Color getColor()
		{
			switch (this) {
			case RED:
				return Color.RED;
			case BLUE:
				return Color.BLUE;
			case GREEN:
				return Color.GREEN;
			case YELLOW:
				return Color.YELLOW;
			}
			throw new DebugException("This landmark is not supported!");
		}

		public static boolean isLandmark(Point p)
		{
			for (Landmark l : values()) {
				if (l.getLocation().equals(p)) {
					return true;
				}
			}
			return false;
		}

		public static Landmark getLandmark(Point p)
		{
			for (Landmark l : values()) {
				if (l.getLocation().equals(p)) {
					return l;
				}
			}
			return null;
		}

	};

	/** The location of the agent. */
	private Point _agent;
	/** Determines whether the agent is holding the passenger. */
	private boolean _holdingPassenger;
	/** The location of the passenger. */
	private Point _passenger;
	/** The location of the passenger's destination. */
	private Point _destination;

	/** Determines whether or not the agent succeeded. */
	private boolean _succeeded;
	/** Determines whether or not this task is finished. */
	private boolean _isFinished;

	/** A collection of barriers that the agent cannot pass through. */
	private Collection<Line2D> _barriers;

	/** A copy of the previous action. */
	private Integer _prevAction;

	public TaxiTask()
	{
		super(NUM_STATES, Action.values().length, REWARD_INTERVAL);
		_agent = new Point();
		_passenger = new Point();
		_destination = new Point();

		_holdingPassenger = false;

		_barriers = new ArrayList<Line2D>();
		_barriers.add(new Line2D.Double(1, 0, 1, 2));
		_barriers.add(new Line2D.Double(2, 3, 2, 5));
		_barriers.add(new Line2D.Double(3, 0, 3, 2));

		reset();
	}

	@Override
	public double evaluate()
	{
		Action prevAction = Action.mapAction(_prevAction);
		if (_succeeded) {
			return REWARD_INTERVAL.getMax();
		} else if ((prevAction.equals(Action.DROP) || prevAction.equals(Action.PICKUP))
				&& !Landmark.isLandmark(_agent)) {
			return REWARD_INTERVAL.getMin();
		} else {
			return -1;
		}
	}

	/**
	 * Returns the reward interval.
	 * 
	 * @return the reward interval
	 */
	public static final Interval rewardInterval()
	{
		return new Interval(-10, 20);
	}

	@Override
	public void execute(Integer action)
	{
		Action act = Action.mapAction(action);

		// If the agent succeeded during the last decision epoch, then allow
		// them to perform one more action that will let the agent experience
		// the reward state.
		if (_succeeded == true) {
			_isFinished = true;
			return;
		}

		// System.out.println("Applying : " + act);
		if (act.isNavigationAction()) {
			// Compute the change caused by this action
			Point delta = act.delta();
			if (!doesActionCollide(delta)) {
				// If the action change didn't cause a collision then update
				// agent's location
				_agent.setLocation(_agent.x + delta.x, _agent.y + delta.y);

				if (_holdingPassenger) {
					_passenger.setLocation(_agent.x, _agent.y);
				}
			}
		} else {
			if (act.equals(Action.DROP)) {
				// Drop the passenger if he is being held
				if (_holdingPassenger) {

					// Only drop off the passenger at landmarks
					if (Landmark.isLandmark(_agent)) {
						_holdingPassenger = false;
					}

					// If at the final destination then goal is achieved and
					// task is finished
					if (_agent.equals(_destination)) {
						_succeeded = true;
					}
				}
			}
			if (act.equals(Action.PICKUP)) {
				// Pick up the passenger if he is in the same
				// location as the agent
				if (!_holdingPassenger && _agent.equals(_passenger)) {
					_holdingPassenger = true;
				}
			}
		}

		// Backup this action as the previous action
		_prevAction = action;
	}

	private boolean doesActionCollide(Point delta)
	{
		Point newPos = new Point(_agent.x + delta.x, _agent.y + delta.y);
		if (newPos.x < 0 || newPos.x >= TAXI_WIDTH || newPos.y < 0
				|| newPos.y >= TAXI_HEIGHT) {
			return true;
		}

		for (Line2D barrier : _barriers) {
			Line2D trajectory = new Line2D.Double(_agent.x + 0.5,
					_agent.y + 0.5, newPos.x + 0.5, newPos.y + 0.5);
			if (barrier.intersectsLine(trajectory)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public Integer getState()
	{
		// int dimXSize = TAXI_WIDTH;
		int dimYSize = TAXI_HEIGHT;
		int dimPassengerSize = Landmark.values().length + 1;
		int dimDestSize = Landmark.values().length;

		// Encode the state as an integer
		int stateBuilder = _agent.x
				* (dimYSize * dimPassengerSize * dimDestSize);
		stateBuilder += _agent.y * (dimPassengerSize * dimDestSize);
		stateBuilder += encodePassengerForState(_passenger, _holdingPassenger) * dimDestSize;
		stateBuilder += encodeDestinationForState(_destination.getLocation());

		return new Integer(stateBuilder);
	}
	
	public void setState(Point agent, Landmark goal, Landmark passenger, boolean holdingPassenger)
	{
		_agent.setLocation(agent);
		_destination.setLocation(goal.getLocation());
		_passenger.setLocation(passenger.getLocation());
		_holdingPassenger = holdingPassenger;
	}
	
	public Integer encode(Point agent, Landmark goal, Landmark passenger, boolean holdingPassenger)
	{
		// int dimXSize = TAXI_WIDTH;
		int dimYSize = TAXI_HEIGHT;
		int dimPassengerSize = Landmark.values().length + 1;
		int dimDestSize = Landmark.values().length;

		// Encode the state as an integer
		int stateBuilder = agent.x
				* (dimYSize * dimPassengerSize * dimDestSize);
		stateBuilder += agent.y * (dimPassengerSize * dimDestSize);
		stateBuilder += encodePassengerForState(passenger.getLocation(), holdingPassenger) * dimDestSize;
		stateBuilder += encodeDestinationForState(goal.getLocation());

		return new Integer(stateBuilder);
	}
	
	public Landmark getDestination()
	{
		for(int i=0;i<Landmark.values().length;i++){
			if(Landmark.values()[i].getLocation().equals(_destination)){
				return Landmark.values()[i];
			}
		}
		throw new DebugException(
		"Destination location could not be found because the destination is in an invalid location.");
	}

	/**
	 * Decodes a taxi state into its four components. The first two dimensions
	 * refer to (x, y) coordinates of the taxi, while the last two dimensions
	 * encode the passenger location (including inside the taxi), and finally
	 * the destination.
	 * 
	 * @param state a taxi state as an integer
	 * @return an integer array containing the decoded taxi state values
	 */
	public static final int[] decode(Integer state)
	{
		int numLandmarks = Landmark.values().length;
		int divisor = (TAXI_HEIGHT * (numLandmarks+1) * numLandmarks);
		int x = state / divisor;
		int remainder = state % divisor;
		divisor = ((numLandmarks+1) * numLandmarks);
		int y = remainder / divisor;
		remainder = remainder % divisor;
		divisor = (numLandmarks);
		int passenger = remainder / divisor;
		int destination = remainder % divisor;
		
		return new int[] { x, y, passenger, destination };
	}

	private int encodePassengerForState(Point passenger, boolean holdingPassenger)
	{
		if (holdingPassenger) {
			return Landmark.values().length;
		} else {
			Landmark[] values = Landmark.values();
			for (int i = 0; i < values.length; i++) {
				Landmark landmark = values[i];
				if (landmark.getLocation().equals(passenger)) {
					return i;
				}
			}
		}
		throw new DebugException(
				"Passenger location could not be encoded because the passenger is in an invalid location.");
	}

	private int encodeDestinationForState(Point goal)
	{
		Landmark[] values = Landmark.values();
		for (int i = 0; i < values.length; i++) {
			Landmark landmark = values[i];
			if (landmark.getLocation().equals(goal)) {
				return i;
			}
		}
		throw new DebugException(
				"Destination location could not be encoded because the destination is in an invalid location.");
	}

	@Override
	public boolean isFinished()
	{
		return _isFinished;
	}

	@Override
	public void reset()
	{
		_succeeded = false;
		_isFinished = false;
		_holdingPassenger = false;

		// Select the agent's initial location
		_agent.setLocation(Random.nextInt(TAXI_WIDTH),
				Random.nextInt(TAXI_HEIGHT));

		// Select the passenger location
		_passenger.setLocation(randomLandmark().getLocation());
		// Select the destination location
		_destination.setLocation(randomLandmark().getLocation());
	}
	
	public void setDestination(Landmark l)
	{
		if(l == null){
			_destination.setLocation(randomLandmark().getLocation());
		}else{
			_destination.setLocation(l.getLocation());
		}
	}

	private Landmark randomLandmark()
	{
		int index = Random.nextInt(Landmark.values().length);
		Landmark landmark = Landmark.values()[index];
		return landmark;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();

		double tsize = Math.min(width / (double) TAXI_WIDTH, height
				/ (double) TAXI_HEIGHT);

		g2.setStroke(new BasicStroke(1.0f));

		// Draw the landmarks
		for (Landmark landmark : Landmark.values()) {
			Color color = landmark.getColor();
			Point p = landmark.getLocation();
			double rx = (p.x * tsize);
			double ry = height - ((p.y + 1) * tsize);
			Rectangle2D landmarkRect = new Rectangle2D.Double(rx, ry, tsize,
					tsize);
			g2.setColor(color);
			g2.fill(landmarkRect);
		}

		// Draw the destination
		double dx = _destination.x * tsize;
		double dy = height - (((_destination.y + 1) * tsize));
		Ellipse2D destEllipse = new Ellipse2D.Double(dx + (0.2 * tsize), dy
				+ (0.2 * tsize), tsize * 0.6, tsize * 0.6);
		g2.setColor(Color.YELLOW);
		g2.fill(destEllipse);
		g2.setColor(Color.BLACK);
		g2.draw(destEllipse);

		// Draw the agent
		double ax = _agent.x * tsize;
		double ay = height - ((_agent.y + 1) * tsize);
		Rectangle2D agentRect = new Rectangle2D.Double(ax + (0.05 * tsize), ay
				+ (0.05 * tsize), tsize * 0.9, tsize * 0.9);
		g2.setColor(Color.ORANGE);
		g2.fill(agentRect);

		// Draw passenger
		double px = _passenger.x * tsize;
		double py = height - ((_passenger.y + 1) * tsize);
		Rectangle2D passengerRect = new Rectangle2D.Double(px + (0.1 * tsize),
				py + (0.1 * tsize), tsize * 0.8, tsize * 0.8);
		g2.setColor(Color.PINK);
		g2.fill(passengerRect);

		g2.setColor(Color.BLACK);
		g2.setStroke(new BasicStroke(1.0f));
		// Draw the grid
		for (int x = 0; x < TAXI_WIDTH; x++) {
			for (int y = 0; y < TAXI_HEIGHT; y++) {
				double rx = (x * tsize);
				double ry = height - ((y + 1) * tsize);
				Rectangle2D cell = new Rectangle2D.Double(rx, ry, tsize, tsize);
				g2.draw(cell);
			}
		}

		g2.setStroke(new BasicStroke(3.0f));
		// Draw the barriers
		for (Line2D barrier : _barriers) {
			double lx1 = barrier.getX1() * tsize;
			double ly1 = height - (barrier.getY1() * tsize);
			double lx2 = barrier.getX2() * tsize;
			double ly2 = height - (barrier.getY2() * tsize);
			Line2D line = new Line2D.Double(lx1, ly1, lx2, ly2);
			g2.draw(line);
		}

		g2.dispose();
	}

	public static void main(String[] args)
	{
		double learningRate = 0.3;
		double discountFactor = 0.9;
		double epsilon = 0.1;
		//QLearning agent = new QLearning(numberOfStates(), numberOfActions(),
		//		learningRate, discountFactor);
		//agent.setEpsilon(epsilon);
		 int visitsUntilKnown = 5;
		 TaxiTask task = new TaxiTask();
		 RMax agent = new RMax(task.numberOfStates(), task.numberOfActions(),
		 discountFactor, REWARD_INTERVAL, visitsUntilKnown);

		int numTrainEpisodes = 50;
		int numEpisodes = 100;
		int episodeLength = 100;

		javax.swing.JFrame frame = new javax.swing.JFrame("Taxi Domain");
		frame.setSize(400, 400);

		Task.runTask(task, agent, numTrainEpisodes, episodeLength, true, null,
				null, true);

		Task.runTask(task, agent, numEpisodes, episodeLength, true, null,
				frame, false);
	}

}
