package tmrutil.learning.rl.tasks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import tmrutil.graph.Edge;
import tmrutil.graph.EdgeImpl;
import tmrutil.graph.Graph;
import tmrutil.graph.Node;
import tmrutil.graph.NodeImpl;

/**
 * Implements a network routing simulation.
 * @author Timothy A. Mann
 *
 */
public class NetworkRouting implements Graph<Integer, Node<Integer,Integer>,Edge<Node<Integer,Integer>,Double>>
{
	
	private List<Node<Integer,Integer>> _nodes;
	
	private List<Edge<Node<Integer,Integer>,Double>> _edges;
	private List<Set<Edge<Node<Integer,Integer>,Double>>> _edgeMap;
	
	public NetworkRouting(int numNodes)
	{
		_nodes = new ArrayList<Node<Integer,Integer>>(numNodes);
		_edges = new ArrayList<Edge<Node<Integer,Integer>,Double>>();
		_edgeMap = new ArrayList<Set<Edge<Node<Integer,Integer>,Double>>>();
		for(int i=0;i<numNodes;i++){
			NodeImpl<Integer,Integer> n = new NodeImpl<Integer,Integer>(i,0);
			_nodes.add(n);
			_edgeMap.add(new HashSet<Edge<Node<Integer,Integer>,Double>>());
		}
	}
	
	public void addConnection(Node<Integer,Integer> nodeA, Node<Integer,Integer> nodeB, Double successProbability)
	{
		EdgeImpl<Node<Integer,Integer>,Double> edge = new EdgeImpl<Node<Integer,Integer>,Double>(nodeA, nodeB, successProbability);
		_edges.add(edge);
		_edgeMap.get(nodeA.key()).add(edge);
		_edgeMap.get(nodeB.key()).add(edge);
	}
	
	public void addConnection(Integer nodeKeyA, Integer nodeKeyB, Double successProbability)
	{
		addConnection(_nodes.get(nodeKeyA), _nodes.get(nodeKeyB), successProbability);
	}

	@Override
	public Iterator<Edge<Node<Integer, Integer>, Double>> iterator()
	{
		return _edges.iterator();
	}

	@Override
	public Node<Integer, Integer> node(Integer key)
	{
		return _nodes.get(key);
	}

	@Override
	public Set<Edge<Node<Integer, Integer>, Double>> edges(
			Node<Integer, Integer> node)
	{
		return _edgeMap.get(node.key());
	}

	@Override
	public Set<Node<Integer, Integer>> nodes()
	{
		return new HashSet<Node<Integer,Integer>>(_nodes);
	}

	@Override
	public Set<Edge<Node<Integer, Integer>, Double>> edges()
	{
		return new HashSet<Edge<Node<Integer,Integer>,Double>>(_edges);
	}

	@Override
	public int numberOfNodes()
	{
		return _nodes.size();
	}

	@Override
	public int numberOfEdges()
	{
		return _edges.size();
	}

	@Override
	public boolean isDirected()
	{
		return false;
	}
	
}
