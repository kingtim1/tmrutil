package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.swing.JFrame;

import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.RMax;
import tmrutil.learning.rl.RandomLearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.TaskObserver;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.learning.rl.sim.imgmt.CostFunction;
import tmrutil.learning.rl.sim.imgmt.HoldingConstraints;
import tmrutil.learning.rl.sim.imgmt.IMState;
import tmrutil.learning.rl.sim.imgmt.IndependentHoldingConstraints;
import tmrutil.learning.rl.sim.imgmt.InventoryManagementSimulator;
import tmrutil.learning.rl.sim.imgmt.LinearCostFunction;
import tmrutil.learning.rl.sim.imgmt.NonzeroCostFunction;
import tmrutil.learning.rl.sim.imgmt.WidgetType;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.math.Function;
import tmrutil.stats.GenerativeDistribution;
import tmrutil.util.ArrayOps;
import tmrutil.util.FileDataStore;
import tmrutil.util.Interval;

public class DiscreteInventoryManagementTask extends DiscreteTask implements
		Drawable {

	public static class Observer implements
			TaskObserver<Integer, Integer, DiscreteInventoryManagementTask> {
		private List<Integer> _widget1Quantity;
		private List<Integer> _widget2Quantity;
		private List<Integer> _orderQuantityWidget1;
		private List<Integer> _orderQuantityWidget2;
		private List<Integer> _demandWidget1;
		private List<Integer> _demandWidget2;

		private List<Integer> _orderCost;
		private List<Integer> _holdingCost;
		private List<Integer> _unmetDemandCost;

		private List<Double> _reward;

		private IMState _prevState;

		public Observer() {
			_widget1Quantity = new ArrayList<Integer>();
			_widget2Quantity = new ArrayList<Integer>();
			_orderQuantityWidget1 = new ArrayList<Integer>();
			_orderQuantityWidget2 = new ArrayList<Integer>();
			_demandWidget1 = new ArrayList<Integer>();
			_demandWidget2 = new ArrayList<Integer>();

			_orderCost = new ArrayList<Integer>();
			_holdingCost = new ArrayList<Integer>();
			_unmetDemandCost = new ArrayList<Integer>();
			_reward = new ArrayList<Double>();
		}

		@Override
		public void observeStep(DiscreteInventoryManagementTask task,
				Integer prevState, Integer action, Integer newState,
				double reinforcement) {
			int[] maxValues = { (MAX_WIDGET1 + 1), (MAX_WIDGET2 + 1) };

			int[] iaction = ArrayOps.decode(action, maxValues);
			_orderQuantityWidget1.add(iaction[0]);
			_orderQuantityWidget2.add(iaction[1]);

			IMState nstate = task._cstate;
			_widget1Quantity.add(nstate.inStock(WIDGET1));
			_widget2Quantity.add(nstate.inStock(WIDGET2));

			_demandWidget1.add(nstate.previousDemand(WIDGET1));
			_demandWidget2.add(nstate.previousDemand(WIDGET2));

			if (_prevState != null) {
				Map<WidgetType, Integer> demand = task._cstate.previousDemand();
				int orderCost = task._isim.orderCost(_prevState, iaction);
				_orderCost.add(orderCost);
				int udcost = task._isim.unmetDemandCost(_prevState, iaction,
						demand);
				_unmetDemandCost.add(udcost);
				int hcost = task._isim.holdingCost(_prevState, iaction, demand);
				_holdingCost.add(hcost);
			}

			_reward.add(reinforcement);
		}

		@Override
		public void observeEpisodeBegin(DiscreteInventoryManagementTask task) {
			IMState state = task._cstate;
			_widget1Quantity.add(state.inStock(WIDGET1));
			_widget2Quantity.add(state.inStock(WIDGET2));
			_prevState = task._cstate;
		}

		@Override
		public void observeEpisodeEnd(DiscreteInventoryManagementTask task) {
		}

		@Override
		public void reset() {
			_widget1Quantity.clear();
			_widget2Quantity.clear();
			_orderQuantityWidget1.clear();
			_orderQuantityWidget2.clear();
			_demandWidget1.clear();
			_demandWidget2.clear();

			_orderCost.clear();
			_holdingCost.clear();
			_unmetDemandCost.clear();

			_reward.clear();
			_prevState = null;
		}

		public void writeToDStore(FileDataStore dstore, String prefix) {
			dstore.appendRow(prefix + "_widget1_quantity", _widget1Quantity);
			dstore.appendRow(prefix + "_widget2_quantity", _widget2Quantity);

			dstore.appendRow(prefix + "_widget1_order_quantity",
					_orderQuantityWidget1);
			dstore.appendRow(prefix + "_widget2_order_quantity",
					_orderQuantityWidget2);

			dstore.appendRow(prefix + "_widget1_demand", _demandWidget1);
			dstore.appendRow(prefix + "_widget2_demand", _demandWidget2);

			dstore.appendRow(prefix + "_order_cost", _orderCost);
			dstore.appendRow(prefix + "_holding_cost", _holdingCost);
			dstore.appendRow(prefix + "_unmet_demand_cost", _unmetDemandCost);

			dstore.appendRow(prefix + "_reward", _reward);
		}
	}

	private static final Random RAND = new Random();
	private static final int ORDER_BASE_COST = 0;


	public static final Interval REWARD_INTERVAL = new Interval(0, 1);
	private static final String WIDGET1_ID = "widget1";
	private static final String WIDGET2_ID = "widget2";

	private static class DemandDistribution implements
			GenerativeDistribution<Integer> {
		private int _mu;
		private int _sigma;

		public DemandDistribution(int mu, int sigma) {
			_mu = mu;
			_sigma = sigma;
		}

		@Override
		public Integer sample() {

			return (int) (RAND.nextGaussian() * _sigma) + _mu;
		}

	};

	private static final int MAX_WIDGET1 = 15;
	private static final int MAX_WIDGET2 = 10;

	private static final WidgetType WIDGET1 = new WidgetType(
			WIDGET1_ID,
			new DemandDistribution(MAX_WIDGET1 / 5, 1));
	private static final WidgetType WIDGET2 = new WidgetType(
			WIDGET2_ID,
			new DemandDistribution(MAX_WIDGET2 / 4, 1));


	private static final int NUM_STATES = (MAX_WIDGET1 + 1) * (MAX_WIDGET2 + 1);
	private static final int NUM_ACTIONS = (MAX_WIDGET1 + 1)
			* (MAX_WIDGET2 + 1);

	private IMState _cstate;
	private double _lastReward;

	private InventoryManagementSimulator _isim;
	private Interval _orderCostRange;
	private Interval _unmetDemandCostRange;
	private Interval _holdingCostRange;

	public DiscreteInventoryManagementTask() {
		super(NUM_STATES, NUM_ACTIONS, REWARD_INTERVAL);

		WidgetType[] wtypes = {WIDGET1, WIDGET2};
		HoldingConstraints holdingConstraints = new IndependentHoldingConstraints(wtypes, new int[]{MAX_WIDGET1, MAX_WIDGET2});
		CostFunction orderCost = new NonzeroCostFunction(wtypes, 0, 0, holdingConstraints, ORDER_BASE_COST);
		CostFunction unmetDemandCost = new LinearCostFunction(wtypes, 2, 0, holdingConstraints);
		CostFunction holdingCost = new LinearCostFunction(wtypes, 1, 0, holdingConstraints);
		
		_orderCostRange = orderCost.range();
		_unmetDemandCostRange = unmetDemandCost.range();
		_holdingCostRange = holdingCost.range();
		
		_isim = new InventoryManagementSimulator(orderCost, unmetDemandCost, holdingCost,
				holdingConstraints);
		reset();
	}

	@Override
	public Integer getState() {
		return toIntState(_cstate);
	}

	public Integer toIntState(IMState state) {
		int istate = (MAX_WIDGET1 + 1) * state.inStock(WIDGET2)
				+ state.inStock(WIDGET1);
		return istate;
	}

	@Override
	public void execute(Integer action) {
		// System.out.print(_cstate);

		int[] iaction = ArrayOps.decode(action, new int[] { (MAX_WIDGET1 + 1),
				(MAX_WIDGET2 + 1) });
		OptionOutcome<IMState,IMState> outcome = _isim
				.samplePrimitive(_cstate, iaction);

		_cstate = outcome.terminalState();
		int cost = (int) outcome.dCumR();
		//System.out.println("Cost: " + cost);

		_lastReward = costToReward(cost);
		//System.out.println("Reward: " + _lastReward + "\n");
	}
	
	private double costToReward(int cost)
	{
		double den = _orderCostRange.getDiff() + _unmetDemandCostRange.getDiff() + _holdingCostRange.getDiff();
		return 1 - (cost / den);
	}

	@Override
	public double evaluate() {
		return _lastReward;
	}

	@Override
	public void reset() {
		_cstate = initState();
		_lastReward = 0;
	}

	@Override
	public boolean isFinished() {
		return false;
	}

	private IMState initState() {
		List<WidgetType> wtypes = new ArrayList<WidgetType>();
		wtypes.add(WIDGET1);
		wtypes.add(WIDGET2);
		Map<WidgetType, Integer> inventory = new HashMap<WidgetType, Integer>();
		inventory.put(WIDGET1, 0);
		inventory.put(WIDGET2, 0);
		Map<WidgetType, Integer> demand = new HashMap<WidgetType, Integer>();
		demand.put(WIDGET1, 0);
		demand.put(WIDGET2, 0);
		IMState state = new IMState(wtypes, inventory, demand);
		return state;
	}

	@Override
	public void draw(Graphics2D g, int width, int height) {
		g.setColor(Color.BLACK);
		int twidth = width / (MAX_WIDGET1 + 1);
		int theight = height / (MAX_WIDGET2 + 1);
		for (int x = 0; x < (MAX_WIDGET1 + 1); x++) {
			for (int y = 0; y < (MAX_WIDGET2 + 1); y++) {
				Rectangle2D rect = new Rectangle2D.Double(x * twidth, y
						* theight, twidth, theight);
				g.draw(rect);
			}
		}
		g.setColor(Color.RED);
		Rectangle2D arect = new Rectangle2D.Double(_cstate.inStock(WIDGET1)
				* twidth, _cstate.inStock(WIDGET2) * theight, twidth, theight);
		g.fill(arect);
	}

	private class DSimulator extends Simulator<Integer, Integer, Integer> {

		@Override
		public OptionOutcome<Integer,Integer> samplePrimitive(Integer observation,
				Integer action) {
			IMState state = buildState(observation);
			int[] iaction = ArrayOps.decode(action, new int[] {
					(MAX_WIDGET1 + 1), (MAX_WIDGET2 + 1) });

			OptionOutcome<IMState,IMState> outcome = _isim.samplePrimitive(state,
					iaction);
			double reward = costToReward((int)outcome.dCumR());
			int tstate = toIntState(outcome.terminalState());
			return new OptionOutcome<Integer,Integer>(
					tstate, tstate,
					reward, reward, outcome.duration());
		}

		@Override
		public Integer observe(Integer previousAction, Integer resultState) {
			return resultState;
		}

	}

	public Simulator<Integer, Integer, Integer> discreteSimulator() {
		return new DSimulator();
	}

	public InventoryManagementSimulator simulator() {
		return _isim;
	}

	public int[] decode(Integer istate) {
		int[] maxVals = { MAX_WIDGET1 + 1, MAX_WIDGET2 + 1 };
		return ArrayOps.decode(istate, maxVals);
	}

	public double[] decodeAndNormalize(Integer istate) {
		int[] aState = decode(istate);
		double[] dState = { aState[0] / (double) (MAX_WIDGET1 + 1),
				aState[1] / (double) (MAX_WIDGET2 + 1) };
		return dState;
	}

	public IMState buildState(Integer state) {
		int[] istate = decode(state);
		List<WidgetType> wtypes = new ArrayList<WidgetType>();
		wtypes.add(WIDGET1);
		wtypes.add(WIDGET2);
		Map<WidgetType, Integer> inventory = new HashMap<WidgetType, Integer>();
		inventory.put(WIDGET1, istate[0]);
		inventory.put(WIDGET2, istate[1]);
		Map<WidgetType, Integer> demand = new HashMap<WidgetType, Integer>();
		demand.put(WIDGET1, 0);
		demand.put(WIDGET2, 0);

		IMState imstate = new IMState(wtypes, inventory, demand);
		return imstate;
	}

	public static void main(String[] args) throws IOException {
		Collection<Integer> actions = new ArrayList<Integer>();
		for (int a = 0; a < NUM_ACTIONS; a++) {
			actions.add(a);
		}
		// RandomLearningSystem<Integer,Integer> agent = new
		// RandomLearningSystem<Integer,Integer>(actions);
		double discountFactor = 0.95;
		int numVisitsUntilKnown = 10;

		String label = "rmax";
		FileDataStore dstore = new FileDataStore(new File(
				System.getProperty("user.home") + "/Documents/data/"
						+ DiscreteInventoryManagementTask.class.getSimpleName()
						+ "/" + label));
		dstore.clear();
		int numSamples = 20;

		for (int i = 0; i < numSamples; i++) {
			RMax agent = new RMax(NUM_STATES, NUM_ACTIONS, discountFactor,
					REWARD_INTERVAL, numVisitsUntilKnown);

			DiscreteInventoryManagementTask task = new DiscreteInventoryManagementTask();

			Observer observer = new Observer();
			Task.runTask(task, agent, 1, 10000, true, observer);
			observer.writeToDStore(dstore, label);
		}

	}

}
