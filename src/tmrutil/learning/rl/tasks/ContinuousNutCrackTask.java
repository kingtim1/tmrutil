package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.learning.GeneralLinearNetwork;
import tmrutil.learning.IncrementalFunctionApproximator;
import tmrutil.learning.LearningRate;
import tmrutil.learning.MultiFunctionApproximator;
import tmrutil.learning.NNet;
import tmrutil.learning.SingleLayerPerceptron;
import tmrutil.learning.StripCMAC;
import tmrutil.learning.TwoLayerPerceptron;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.DiscreteActionTask;
import tmrutil.learning.rl.DiscreteIncrementalPolicySearch;
import tmrutil.learning.rl.InitializationType;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.continuous.CMACQLearning;
import tmrutil.learning.rl.continuous.DiscreteActionQLearning;
import tmrutil.math.RealFunction;
import tmrutil.math.functions.Compose;
import tmrutil.math.functions.ConstantScale;
import tmrutil.math.functions.HyperbolicTangent;
import tmrutil.optim.Optimization;
import tmrutil.util.Interval;

public class ContinuousNutCrackTask extends DiscreteActionTask<double[]> implements
		Drawable
{
	private NutCrackDriver _driver;
	private int _size;
	private Representation _repr;

	public ContinuousNutCrackTask(int size){
		this(size, Representation.ABSOLUTE);
	}
	
	public ContinuousNutCrackTask(int size, Representation repr)
	{
		super(NutCrackDriver.NUMBER_OF_ACTIONS);
		_driver = new NutCrackDriver(size);
		_size = size;
		_repr = repr;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_driver.draw(g, width, height);
	}

	@Override
	public double[] getState()
	{
		int isNutCracked = (_driver.isNutCracked()) ? 1 : 0;
		Point agent = _driver.whereIsAgent();
		Point rock = _driver.whereIsRock();
		Point nut = _driver.whereIsNut();
		
		double s = _size;
		double diffAN = (agent.x - nut.x) / s;
		double diffAR = (agent.x - rock.x) / s;
		double diffNR = (nut.x - rock.x) / s; 

		double[] vstate = null;
		if(_repr.equals(Representation.ABSOLUTE)){
			vstate = new double[]{ isNutCracked, agent.x / s, rock.x / s, rock.y,
				nut.x / s, nut.y };
		}
		if(_repr.equals(Representation.RELATIVE)){

			vstate = new double[]{ isNutCracked, diffAN, diffAR, diffNR, rock.y, nut.y};
		}
		if(_repr.equals(Representation.FULL)){
			vstate = new double[]{ isNutCracked, agent.x/s, rock.x/s, rock.y, nut.x/s, nut.y, diffAN, diffAR, diffNR};
		}
		return vstate;
	}
	
	public int numFeatures()
	{
		return getState().length;
	}

	@Override
	public void execute(Integer action)
	{
		_driver.execute(action);
	}

	@Override
	public double evaluate()
	{
		//return _driver.evaluate();
		double reward = _driver.evaluate();
		return reward;
	}

	@Override
	public void reset()
	{
		_driver.reset();
	}

	@Override
	public boolean isFinished()
	{
		return _driver.isFinished();
	}

	public static void main(String[] args)
	{
		int size = 10;
		double learningRate = 0.1;
		double epsilonGreedy = 0.05;
		
		int numActions = NutCrackDriver.NUMBER_OF_ACTIONS;
		double discountFactor = 0.95;

		int numEvaluationSteps = 200;
		int maxPoliciesToSearch = 50;
		Double rewardThreshold = null;

		ContinuousNutCrackTask task = new ContinuousNutCrackTask(size, Representation.FULL);
		int numFeatures = task.numFeatures();
		
//		ActionSelector.EpsilonGreedy<double[], Integer> actionSelector = ActionSelector.EpsilonGreedy.discreteEpsilonGreedy(
//				numActions, epsilonGreedy, Optimization.MAXIMIZE);
//		LearningSystem<double[], Integer> agent = new CMACQLearning(
//				numFeatures, actionSelector, NutCrackDriver.REWARD_INTERVAL,
//				InitializationType.OPTIMISTIC, new LearningRate.Constant(
//						learningRate), discountFactor);

		List<IncrementalFunctionApproximator<double[],double[]>> qfuncs = new ArrayList<IncrementalFunctionApproximator<double[],double[]>>();
		for (int a = 0; a < numActions; a++) {
			//RealFunction f = new Compose(new
			// ConstantScale(1/(1-discountFactor),0), new HyperbolicTangent());
			//NNet nnet = new SingleLayerPerceptron(numFeatures, 1);
			// NNet nnet = new GeneralLinearNetwork(numFeatures, 1);
			//int numHidden = 3;
			//NNet nnet = new TwoLayerPerceptron(f, numFeatures, 1, numHidden);
			
			 
			List<IncrementalFunctionApproximator<double[],Double>> fs = new ArrayList<IncrementalFunctionApproximator<double[],Double>>();
			List<Interval> bounds = new ArrayList<Interval>();
			List<Integer> numBins = new ArrayList<Integer>();
			for(int i=0;i<numFeatures;i++){
				bounds.add(new Interval(-1, 1));
				numBins.add(10);
			}
			fs.add(new StripCMAC(bounds, numBins, 8, 1.0/(1-discountFactor)));
			IncrementalFunctionApproximator<double[],double[]> nnet = new MultiFunctionApproximator(fs);
			qfuncs.add(nnet);
		}
		LearningSystem<double[], Integer> agent = new DiscreteActionQLearning(
				numFeatures, qfuncs, new LearningRate.Constant(learningRate),
				discountFactor, epsilonGreedy);

		int maxEpisodeLength = 50;
		Task.runTask(task, agent, 10000, maxEpisodeLength, true, null, null, true);

		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(600, 200);
		Task.runTask(task, agent, -1, maxEpisodeLength, true, null, frame, false);
	}

}
