package tmrutil.learning.rl.tasks;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteActionTask;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.DebugException;

/**
 * Implements the simple dynamics of the mountain car problem. The mountain car
 * problem is to drive an under-powered car sitting between two mountains up the
 * side of the mountain reaching a goal. A successful strategy requires the car
 * to first backup away from the goal and then use that momentum to drive the
 * car towards the goal.
 * 
 * <p/>
 * The implementation here is inspired by the implementation described in the
 * book <a href="http://www.cs.ualberta.ca/~sutton/book/ebook/the-book.html">
 * Reinforcement Learning: An Introduction</a> by Richard S. Sutton and Andrew
 * G. Barto.
 * 
 * @author Timothy Mann
 * 
 */
public class MountainCarTask extends GoalTask<double[]> implements
		Drawable
{
	private static class DefaultReinforcementSignal implements
			ReinforcementSignal<double[], Integer>
	{
		@Override
		public double evaluate(double[] prevState, Integer action)
		{
			if (prevState[0] > POS_GOAL) {
				return 1;
			} else {
				return -1;
			}
		}
	}

	/** The color of the mountain line. */
	private static final Color MOUNTAIN_COLOR = Color.BLACK;
	/** The color of the car. */
	private static final Color CAR_COLOR = Color.BLUE;
	/** The color of the car's velocity arrow. */
	private static final Color VEL_COLOR = Color.RED;
	/** The color of the goal. */
	private static final Color GOAL_COLOR = Color.RED;
	/** The width of the car. */
	private static final double CAR_WIDTH = 0.1;
	/** The height of the car. */
	private static final double CAR_HEIGHT = 0.05;

	/** The minimum allowed position of the car. */
	public static final double POS_MIN = -1.2;
	/** The maximum allowed position and goal state of the car. */
	public static final double POS_MAX = 0.6;
	/** The goal position. */
	public static final double POS_GOAL = 0.5;
	/** The minimum velocity of the car. */
	public static final double VEL_MIN = -0.07;
	/** The maximum velocity of the car. */
	public static final double VEL_MAX = 0.07;

	/** The number of components in a valid state description vector. */
	private static final int STATE_SIZE = 2;
	/** The number of components in a valid action vector. */
	private static final int ACTION_SIZE = 1;

	public static final int NUM_ACTIONS = 3;
	
	/** Position of the car. */
	private double _pos;
	/** Velocity of the car. */
	private double _vel;

	/**
	 * A reference to the reinforcement signal.
	 */
	private ReinforcementSignal<double[], Integer> _rsignal;
	private double[] _prevStateBuff;
	private Integer _actionBuff;
	
	private static final double NOISE_STD = 0.1 * (VEL_MAX - VEL_MIN);
	private boolean _stochastic;

	/**
	 * Constructs a mountain car task using the default reinforcement signal.
	 * That is, the agent receives a reward of -1 until the car is in a goal
	 * state. Once the car reaches a goal state the episode is terminated.
	 */
	public MountainCarTask()
	{
		this(new DefaultReinforcementSignal(), false);
	}
	
	/**
	 * Constructs a mountain car task that has either stochastic or deterministic transition probabilities.
	 * @param stochastic
	 */
	public MountainCarTask(boolean stochastic){
		this(new DefaultReinforcementSignal(), stochastic);
	}

	/**
	 * Constructs a mountain car task.
	 * 
	 * @param rsignal
	 *            the reinforcement signal used to determine immediate
	 *            cost/reward
	 */
	public MountainCarTask(ReinforcementSignal<double[], Integer> rsignal, boolean stochastic)
	{
		super(NUM_ACTIONS);
		_rsignal = rsignal;
		_prevStateBuff = new double[STATE_SIZE];
		_actionBuff = 0;
		
		_stochastic = stochastic;
		
		reset();
	}

	@Override
	public double evaluate()
	{
		return _rsignal.evaluate(_prevStateBuff, _actionBuff);
	}

	@Override
	public void execute(Integer action)
	{
		System.arraycopy(getState(), 0, _prevStateBuff, 0, STATE_SIZE);
		_actionBuff = action;
		step(mapAction(action));
	}
	
	public double mapAction(Integer action)
	{
		switch(action){
		case 0:
			return 0;
		case 1:
			return -1;
		case 2:
			return 1;
		default:
			return 0;
		}
	}
	
	public void setState(double position, double velocity)
	{
		_pos = position;
		_vel = velocity;
		
		if (_vel < VEL_MIN) {
			_vel = VEL_MIN;
		}
		if (_vel > VEL_MAX) {
			_vel = VEL_MAX;
		}

		if (_pos < POS_MIN) {
			_pos = POS_MIN;
		}
		if (_pos > POS_MAX) {
			_pos = POS_MAX;
		}

		if (_pos == POS_MIN && _vel < 0) {
			_vel = 0;
		}
		
	}

	/**
	 * Steps the mountain car simulation forward by one discrete time step by
	 * applying the specified action.
	 * 
	 * @param action
	 *            the amount of force to apply to the car
	 */
	private void step(double action)
	{
		// Update the velocity
		_vel = _vel + (action * 0.001) + (-0.0025 * Math.cos(3 * _pos));
		if (_vel < VEL_MIN) {
			_vel = VEL_MIN;
		}
		if (_vel > VEL_MAX) {
			_vel = VEL_MAX;
		}

		double noise = 0;
		if(_stochastic){
			//noise = Random.normal(0, NOISE_STD);
			double[] anoise = {0.0, 0.5, -0.5};
			int r = Random.nextInt(anoise.length);
			noise = anoise[r] * (VEL_MAX - VEL_MIN);
		}
		// Update the position
		_pos = _pos + _vel + (noise * _vel);
		if (_pos < POS_MIN) {
			_pos = POS_MIN;
		}
		if (_pos > POS_MAX) {
			_pos = POS_MAX;
		}

		if (_pos == POS_MIN && _vel < 0) {
			_vel = 0;
		}
	}

	@Override
	public double[] getState()
	{
		double[] state = new double[] { getPosition(), getVelocity() };
		return state;
	}

	@Override
	public void reset()
	{
		_pos = tmrutil.stats.Random.uniform(POS_MIN, POS_MAX);
		_vel = tmrutil.stats.Random.uniform(VEL_MIN, VEL_MAX);
		_actionBuff = 0;
	}

	@Override
	public boolean isFinished()
	{
		return inGoal();
	}

	/**
	 * Returns true if the car is in a goal state.
	 * 
	 * @return true if the car is in a goal state; otherwise false.
	 */
	public boolean inGoal()
	{
		return (_pos >= POS_GOAL);
	}

	/**
	 * Returns the current position of the car.
	 * 
	 * @return the current position of the car
	 */
	public double getPosition()
	{
		return _pos;
	}

	/**
	 * Returns the current velocity of the car.
	 * 
	 * @return the current velocity of the car
	 */
	public double getVelocity()
	{
		return _vel;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();

		int n = 100;
		double[] xs = VectorOps.range(POS_MIN, POS_MAX, n);
		double[] ys = calculateHeight(xs);

		double xmin = Statistics.min(xs);
		double xmax = Statistics.max(xs);
		double ymin = Statistics.min(ys) - CAR_HEIGHT;
		double ymax = Statistics.max(ys) + CAR_HEIGHT;

		AffineTransform xform = new AffineTransform();
		double scaleX = width / (xmax - xmin);
		double scaleY = height / (ymax - ymin);
		xform.scale(scaleX, -scaleY);
		xform.translate(-xmin, -ymax);

		g2.transform(xform);
		g2.setStroke(new BasicStroke((float) (1 / Math.max(scaleX, scaleY))));

		Path2D path = new Path2D.Double();
		path.moveTo(xs[0], ys[0]);
		for (int i = 1; i < xs.length; i++) {
			path.lineTo(xs[i], ys[i]);
		}
		g2.setColor(MOUNTAIN_COLOR);
		g2.draw(path);

		g2.setColor(GOAL_COLOR);
		Line2D goal = new Line2D.Double(POS_GOAL, ymin, POS_GOAL, ymax
				+ CAR_HEIGHT);
		g2.draw(goal);

		double pos = getPosition();
		Rectangle2D car = new Rectangle2D.Double(pos - (CAR_WIDTH / 2),
				calculateHeight(pos), CAR_WIDTH, CAR_HEIGHT);
		Graphics2D gcar = (Graphics2D) g2.create();
		AffineTransform carXForm = new AffineTransform();
		carXForm.rotate(Math.tanh(calculateDeriv(pos)), pos,
				calculateHeight(pos));
		gcar.transform(carXForm);
		gcar.setColor(CAR_COLOR);
		gcar.draw(car);

		gcar.setColor(VEL_COLOR);
		double vel = getVelocity();
		if (vel < 0) {
			vel = vel - (CAR_WIDTH / 2);
		} else {
			vel = vel + (CAR_WIDTH / 2);
		}
		Line2D lvel = new Line2D.Double(pos, calculateHeight(pos)
				+ (CAR_HEIGHT / 2), pos + vel, calculateHeight(pos)
				+ (CAR_HEIGHT / 2));
		gcar.draw(lvel);
		gcar.dispose();

		g2.dispose();
	}

	private double[] calculateHeight(double[] x)
	{
		double[] y = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			y[i] = calculateHeight(x[i]);
		}
		return y;
	}

	private double calculateHeight(double x)
	{
		return (1 / 3.0) * Math.sin(3 * x);
	}

	private double calculateDeriv(double x)
	{
		return Math.cos(3 * x);
	}

	@Override
	public boolean isInTaskGoal() {
		return inGoal();
	}

}
