package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcess;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcessImpl;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.Task;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.DebugException;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * An abstract implementation of the gridworld domain. This class is used to
 * derive more specific gridworld tasks. Gridworld instances are simple
 * benchmark reinforcement learning tasks where an agent can travel between
 * locations in a 2-dimensional grid. Different locations in the grid may have
 * different properties that affect the dynamics and reward received when the
 * agent applies and action or enters a new location.
 * 
 * @author Timothy A. Mann
 * 
 */
public abstract class AbstractGridworld extends DiscreteTask implements
		Drawable {
	public static enum Outcome {
		STAY, NORTH, SOUTH, EAST, WEST, NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST
	};

	/** The number of rows. */
	private int _rows;
	/** The number of columns. */
	private int _cols;
	/** The actual cells that make up the gridworld. */
	private int[][] _cells;

	/** The location of the agent. */
	private Pair<Integer, Integer> _agent;
	/** The color associated with the agent. */
	private Color _agentColor;

	/** The number of cell types in this gridworld instance. */
	private int _numCellTypes;
	/** An array of colors associated with each cell type. */
	private Color[] _cellColors;

	/** A set of cell types that terminate an episode. */
	private Set<Integer> _terminatingCellTypes;
	/** A set of cell types that cannot be entered by the agent. */
	private Set<Integer> _impassibleCellTypes;

	/**
	 * The reward vector, which determines the mean reward associated with
	 * entering a particular cell type.
	 */
	private double[] _rewardMean;
	/**
	 * The reward vector, which determines the standard deviation associated
	 * with reward for entering a particular cell type.
	 */
	private double[] _rewardStd;

	/**
	 * An set of transition probabilities for {@link Outcome}s given the current
	 * cell type and the selected action.
	 */
	private double[][][] _cellTProbs;

	private boolean _finished;

	/**
	 * Constructs an abstract gridworld instance.
	 * 
	 * @param nRows
	 *            the number of rows in the gridworld instance
	 * @param nCols
	 *            the number of columns in the gridworld instance
	 * @param cells
	 *            the two dimensional array of integers which determines the
	 *            cell type at each location (row-column pair)
	 * @param numActions
	 *            the number of valid actions that an agent can make in this
	 *            gridworld instance
	 * @param agentColor
	 *            the color to associate with the agent
	 * @param numCellTypes
	 *            the number of valid cell types in this gridworld instance
	 * @param cellTypeColors
	 *            an array of colors associated with each cell type
	 * @param terminatingCellTypes
	 *            a set containing all cell types that terminate an episode when
	 *            entered by the agent
	 * @param impassibleCellTypes
	 *            a set containing all cell types that cannot be entered by the
	 *            agent (regardless of the entries in the transition probability
	 *            array)
	 * @param cellTypeTransitionProbabilities
	 *            a three dimensional array of transition probabilities for each
	 *            cell type. The format of the array is as follows:
	 *            <OL>
	 *            <LI>the first index corresponds to the cell type</LI>
	 *            <LI>the second index corresponds to an action</LI>
	 *            <LI>the third (and final) index corresponds to the outcome</LI>
	 *            </OL>
	 * @param cellTypeRewardMeans
	 *            an array of scalar values denoting the mean reward associated
	 *            with each cell type
	 * @param cellTypeRewardStds
	 *            an array of scalar values denoting the standard deviation of
	 *            the reward associated with each cell type
	 */
	public AbstractGridworld(int nRows, int nCols, int[][] cells,
			int numActions, Color agentColor, int numCellTypes,
			Color[] cellTypeColors, Set<Integer> terminatingCellTypes,
			Set<Integer> impassibleCellTypes,
			double[][][] cellTypeTransitionProbabilities,
			double[] cellTypeRewardMeans, double[] cellTypeRewardStds) {
		super(nRows * nCols, numActions, new Interval(
				Statistics.min(cellTypeRewardMeans),
				Statistics.max(cellTypeRewardMeans)));
		// Check that the number of rows and columns of this gridworld is at
		// least 1
		if (nRows < 1 || nCols < 1) {
			throw new IllegalArgumentException(
					"The number of rows and columns of a gridworld instance must be positive. Detected (# rows="
							+ nRows + ", # cols=" + nCols + ").");
		}
		_rows = nRows;
		_cols = nCols;

		// Create the 2D cell array
		_cells = new int[nRows][nCols];
		for (int r = 0; r < nRows; r++) {
			for (int c = 0; c < nCols; c++) {
				_cells[r][c] = cells[r][c];
			}
		}

		// Check that the agent color is not null
		if (agentColor == null) {
			throw new NullPointerException(
					"The color associated with the agent cannot be null.");
		}
		_agentColor = agentColor;

		// Check that the number of cell types is greater than 1
		if (numCellTypes < 1) {
			throw new IllegalArgumentException(
					"Gridworld instances must contain at least one cell type.");
		}
		_numCellTypes = numCellTypes;

		// Check that the cell type colors array is not null, that its length
		// matches the number of valid cell types, and that each color is not
		// null
		if (cellTypeColors == null) {
			throw new NullPointerException(
					"The array of colors associated with each cell type cannot be null.");
		}
		if (cellTypeColors.length != numCellTypes) {
			throw new IllegalArgumentException(
					"The number of elements in the array of colors for each cell type does not match the number of valid cell types.");
		}
		_cellColors = new Color[numCellTypes];
		for (int c = 0; c < cellTypeColors.length; c++) {
			if (cellTypeColors[c] == null) {
				throw new NullPointerException(
						"Detected a null color in the array of colors associated with each cell type.");
			} else {
				_cellColors[c] = cellTypeColors[c];
			}
		}

		// Check that the set of terminating cell types is not null and contains
		// only valid cell types
		if (terminatingCellTypes == null) {
			throw new NullPointerException(
					"The set containing terminating cell types cannot be null.");
		}
		if (!isValidCellTypeSet(terminatingCellTypes)) {
			throw new IllegalArgumentException(
					"The terminating cell types set contains an invalid cell type.");
		}
		_terminatingCellTypes = new TreeSet<Integer>(terminatingCellTypes);

		// Check that the set of impassible cell types is not null and contains
		// only valid cell types
		if (impassibleCellTypes == null) {
			throw new NullPointerException(
					"The set containing impassible cell types cannot be null.");
		}
		if (!isValidCellTypeSet(impassibleCellTypes)) {
			throw new IllegalArgumentException(
					"The impassible cell types set contains an invalid cell type.");
		}
		_impassibleCellTypes = new TreeSet<Integer>(impassibleCellTypes);

		// Set the cell type transition probabilities
		setCellTypeTransitionProbabilities(cellTypeTransitionProbabilities);

		// Check that the cell type reward means are not null and that the
		// vector is the same length as the number of cell types
		if (cellTypeRewardMeans == null) {
			throw new NullPointerException(
					"The vector associating each cell type with a mean reward cannot be null.");
		}
		if (cellTypeRewardMeans.length != numCellTypes) {
			throw new IllegalArgumentException(
					"The number of cell types does not match the length of the vector associating each cell type with a mean reward.");
		}
		_rewardMean = Arrays.copyOf(cellTypeRewardMeans,
				cellTypeRewardMeans.length);

		// Check that the cell type standard deviations are not null, that the
		// vector is the same length as the number of cell types, and that all
		// of the standard deviations are nonnegative.
		if (cellTypeRewardStds == null) {
			throw new NullPointerException(
					"The vector associating each cell type with a standard deviation of reward cannot be null.");
		}
		if (cellTypeRewardStds.length != numCellTypes) {
			throw new IllegalArgumentException(
					"The number of cell types does not match the length of the vector associating each cell type with a standard deviation of reward.");
		}
		for (int i = 0; i < cellTypeRewardStds.length; i++) {
			if (cellTypeRewardStds[i] < 0) {
				throw new IllegalArgumentException(
						"Detected a negative standard deviation in the vector that associates standard deviations to the rewards of each cell type.");
			}
		}
		_rewardStd = Arrays.copyOf(cellTypeRewardStds,
				cellTypeRewardStds.length);

		// Creates an agent instance
		_agent = new Pair<Integer, Integer>(0, 0);

		reset();
	}

	/**
	 * Sets the cell type transition probability array. This method call is
	 * expensive because it does thorough checking to make sure that the
	 * transition probabilities are valid. The format of the array is as
	 * follows:
	 * <OL>
	 * <LI>the first index corresponds to the cell type</LI>
	 * <LI>the second index corresponds to an action</LI>
	 * <LI>the third (and final) index corresponds to the outcome</LI>
	 * </OL>
	 * 
	 * @param tprobs
	 *            a transition probability array for the cell types and each
	 *            action
	 */
	private void setCellTypeTransitionProbabilities(double[][][] tprobs) {
		if (tprobs == null) {
			throw new NullPointerException(
					"The transition probabilities matrix cannot be null.");
		}
		if (tprobs.length != numCellTypes()) {
			throw new IllegalArgumentException(
					"The first index of the cell type transition probabilities array does not match the number of cell types.");
		}
		int numOutcomes = Outcome.values().length;
		_cellTProbs = new double[numCellTypes()][numberOfActions()][numOutcomes];
		for (int cellType = 0; cellType < numCellTypes(); cellType++) {
			if (tprobs[cellType].length != numberOfActions()) {
				throw new IllegalArgumentException(
						"The second index of the cell type transition probabilities array does not match the number of valid actions.");
			}
			for (int action = 0; action < numberOfActions(); action++) {
				if (tprobs[cellType][action].length != numOutcomes) {
					throw new IllegalArgumentException(
							"The third index of the cell type transition probabilities array does not match the number of possible outcomes. Please see enum type "
									+ Outcome.class + " for more details.");
				}
				double sum = Statistics.sum(tprobs[cellType][action]);
				if (sum > 1 + tmrutil.math.Constants.EPSILON
						|| sum < 1 - tmrutil.math.Constants.EPSILON) {
					throw new IllegalArgumentException(
							"The transition probabilities of the cell type transition probabilities array for cell type ("
									+ cellType
									+ ") and action ("
									+ action
									+ ") do not add up to one. Actual sum is : "
									+ sum);
				}
				for (int outcome = 0; outcome < numOutcomes; outcome++) {
					_cellTProbs[cellType][action][outcome] = tprobs[cellType][action][outcome];
				}
			}
		}
	}

	/**
	 * Returns true if the specified cell type is valid.
	 * 
	 * @param cellType
	 *            an integer representing a cell type
	 * @return true if the integer is a valid cell type
	 */
	public final boolean isValidCellType(int cellType) {
		return cellType >= 0 && cellType < numCellTypes();
	}

	/**
	 * Returns true if the specified cell type set is valid. Meaning that it
	 * only contains valid cell types.
	 * 
	 * @param cellTypeSet
	 *            a set of integers
	 * @return true if the specified set contains only valid cell types
	 */
	public final boolean isValidCellTypeSet(Set<Integer> cellTypeSet) {
		for (Integer cellType : cellTypeSet) {
			if (!isValidCellType(cellType)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Determines if the cell at a specified row and column is terminating.
	 * 
	 * @param row
	 *            a row index
	 * @param col
	 *            a column index
	 * @return true if the cell at (row, col) is terminating
	 */
	public final boolean isTerminatingCell(int row, int col) {
		int cellType = cellType(row, col);
		return (_terminatingCellTypes.contains(cellType));
	}

	/**
	 * Determines if the cell at a specified row and column is impassible. This
	 * means that the cell cannot be entered by the agent.
	 * 
	 * @param row
	 *            a row index
	 * @param col
	 *            a column index
	 * @return true if the cell at (row, col) is impassible
	 */
	public final boolean isImpassibleCell(int row, int col) {
		int cellType = cellType(row, col);
		return (_impassibleCellTypes.contains(cellType));
	}

	/**
	 * Returns the type of a cell in this gridworld instance.
	 * 
	 * @param row
	 *            the index of a row in {0, . . . , rows()}
	 * @param col
	 *            the index of a column in {0, . . . , cols()}
	 * @return the type of the cell represented by an integer
	 */
	public final int cellType(int row, int col) {
		if (row < 0 || row >= rows()) {
			throw new IndexOutOfBoundsException("The specified row " + row
					+ " exceeds the bounds of this gridworld instance.");
		}
		if (col < 0 || col >= cols()) {
			throw new IndexOutOfBoundsException("The specified column " + col
					+ " exceeds the bounds of this gridworld instance.");
		}
		return _cells[row][col];
	}

	/**
	 * Returns the type of cell that the agent is currently visiting.
	 * 
	 * @return the type of cell that the agent is visiting
	 */
	public final int agentCellType() {
		return _cells[_agent.getA()][_agent.getB()];
	}

	/**
	 * Returns the location of the agent. The first element (A) in the pair
	 * corresponds to the agent's current row and the second element (B)
	 * corresponds to the agent's current column.
	 * 
	 * @return the location of the agent
	 */
	public final Pair<Integer, Integer> agentLocation() {
		return new Pair<Integer, Integer>(_agent.getA(), _agent.getB());
	}

	/**
	 * Sets the location of this agent.
	 * 
	 * @param row
	 *            a row
	 * @param col
	 *            a column
	 */
	public final void agentLocation(int row, int col) {
		cellType(row, col); // Called to check if (row, col) is valid
		_agent.setA(row);
		_agent.setB(col);
	}

	/**
	 * Returns the number of cell types in this gridworld instance.
	 * 
	 * @return the number of cell types
	 */
	public final int numCellTypes() {
		return _numCellTypes;
	}

	/**
	 * Returns the number of rows in this gridworld instance.
	 * 
	 * @return the number of rows
	 */
	public final int rows() {
		return _rows;
	}

	/**
	 * Returns the number of columns in this gridworld instance.
	 * 
	 * @return the number of columns
	 */
	public final int cols() {
		return _cols;
	}

	/**
	 * Extracts a location from the state encoding.
	 * 
	 * @param state
	 *            a state
	 * @return a pair containing the X and Y coordinate of the agent
	 */
	public Pair<Integer, Integer> stateToLocation(Integer state) {
		int x = state % cols();
		int y = state / cols();
		return new Pair<Integer, Integer>(x, y);
	}
	
	/**
	 * Determines the state from a location.
	 * @param x the X coordinate
	 * @param y the Y coordinate
	 * @return the corresponding state
	 */
	public int locationToState(int x, int y)
	{
		return y * cols() + x;
	}

	/**
	 * Returns the interval containing the minimum and maximum mean rewards for
	 * this task.
	 * 
	 * @return an interval containing the minimum and maximum mean rewards for
	 *         this task
	 */
	public Interval getRewardInterval() {
		double min = Statistics.min(_rewardMean);
		double max = Statistics.max(_rewardMean);
		return new Interval(min, max);
	}

	@Override
	public final double evaluate() {
		int cellType = agentCellType();
		if (_rewardStd[cellType] > 0) {
			return Random.normal(_rewardMean[cellType], _rewardStd[cellType]);
		} else {
			return _rewardMean[cellType];
		}
	}

	@Override
	public final void execute(Integer action) {
		int cellType = agentCellType();
		if (!_terminatingCellTypes.contains(cellType)) {
			double[] outcomeDist = _cellTProbs[cellType][action];
			int outcome = drawRandom(outcomeDist);
			Pair<Integer, Integer> delta = mapOutcome(outcome);
			int row = _agent.getA() + delta.getA();
			int col = _agent.getB() + delta.getB();
			try {
				int newCellType = cellType(row, col);
				if (!_impassibleCellTypes.contains(newCellType)) {
					_agent.setA(row);
					_agent.setB(col);
				}
			} catch (IndexOutOfBoundsException ex) {
				// No need to do anything
			}
		} else {
			_finished = true;
		}
	}

	@Override
	public final boolean isFinished() {
		return _finished;
	}

	@Override
	public final void reset() {
		_finished = false;
		resetImpl();
	}

	/**
	 * Implements the reset method.
	 */
	public abstract void resetImpl();

	/**
	 * Maps an outcome index to a relative change in location.
	 * 
	 * @param outcomeInd
	 *            an index between <code>0</code> and
	 *            <code>Outcome.values().length-1</code>
	 * @return the relative change in location specified by the outcome
	 */
	private Pair<Integer, Integer> mapOutcome(int outcomeInd) {
		Outcome outcome = Outcome.values()[outcomeInd];
		switch (outcome) {
		case STAY:
			return new Pair<Integer, Integer>(0, 0);
		case NORTH:
			return new Pair<Integer, Integer>(0, -1);
		case NORTH_EAST:
			return new Pair<Integer, Integer>(1, -1);
		case NORTH_WEST:
			return new Pair<Integer, Integer>(-1, -1);
		case SOUTH:
			return new Pair<Integer, Integer>(0, 1);
		case SOUTH_EAST:
			return new Pair<Integer, Integer>(1, 1);
		case SOUTH_WEST:
			return new Pair<Integer, Integer>(-1, 1);
		case EAST:
			return new Pair<Integer, Integer>(1, 0);
		case WEST:
			return new Pair<Integer, Integer>(-1, 0);
		}
		throw new DebugException("Failed to map outcome index to an outcome!");
	}

	/**
	 * Draws a random sample according to the distribution specified by
	 * <code>dist</code>.
	 * 
	 * @param dist
	 *            an array containing elements between 0 and 1 whose sum is 1
	 * @return an integer between <code>0</code> and
	 *         <code>dist.length - 1</code>
	 */
	private int drawRandom(double[] dist) {
		double r = Random.uniform();
		double s = 0;
		for (int i = 0; i < dist.length; i++) {
			s += dist[i];
			if (r <= s) {
				return i;
			}
		}
		throw new DebugException(
				"Drawing a random sample from distribution failed!");
	}

	@Override
	public void draw(Graphics2D g, int width, int height) {
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, width, height);

		double cellWidth = Math.min(width / (double) cols(), height
				/ (double) rows());
		// Draw all of the cells
		for (int col = 0; col < cols(); col++) {
			for (int row = 0; row < rows(); row++) {
				double x = col * cellWidth;
				double y = row * cellWidth;
				Rectangle2D rect = new Rectangle2D.Double(x, y, cellWidth,
						cellWidth);
				g2.setColor(_cellColors[cellType(row, col)]);
				g2.fill(rect);
				g2.setColor(Color.BLACK);
				g2.draw(rect);
			}
		}

		// Draw the agent as an ellipse
		Ellipse2D agentEllipse = new Ellipse2D.Double(
				_agent.getB() * cellWidth, _agent.getA() * cellWidth,
				cellWidth, cellWidth);
		g2.setColor(_agentColor);
		g2.fill(agentEllipse);
		g2.setColor(Color.BLACK);
		g2.draw(agentEllipse);

		g2.dispose();
	}

	/**
	 * Allows searching for the index of an element in an enumerated type.
	 * 
	 * @param <E>
	 *            the enumerated type
	 * @param element
	 *            the element to search for
	 * @param values
	 *            the array containing all values of the enumerated type
	 *            (obtained by E.values() where E is the enum type)
	 * @return the index of the element in the values array
	 */
	public static final <E extends Enum<E>> int indexOf(E element, E[] values) {
		for (int i = 0; i < values.length; i++) {
			if (element.equals(values[i])) {
				return i;
			}
		}
		throw new DebugException(
				"The enum element was not found in the values array.");
	}

	/*
	public DiscreteMarkovDecisionProcess<Integer, Integer> getMDP(
			double discountFactor) {
		int numStates = numberOfStates() + 1;
		int numActions = numberOfActions();
		int dummyState = numStates - 1;

		double[][][] tprobs = new double[numStates][numActions][numStates];
		double[][] rsignal = new double[numStates][numActions];
		
		Outcome[] ocValues = Outcome.values();
		
		for (int s = 0; s < numStates; s++) {
			for (int a = 0; a < numActions; a++) {
				if (s == dummyState) {
					tprobs[s][a][s] = 1;
					rsignal[s][a] = 0;
				}else{
					Pair<Integer,Integer> sloc = stateToLocation(s);
					int stype = cellType(sloc.getA(), sloc.getB());
					
					rsignal[s][a] = _rewardMean[stype];
					
					for(int oi = 0; oi < ocValues.length; oi++){
						Pair<Integer,Integer> agent = new Pair<Integer,Integer>();
						Pair<Integer, Integer> delta = mapOutcome(oi);
						int row = agent.getA() + delta.getA();
						int col = agent.getB() + delta.getB();
						try {
							int newCellType = cellType(row, col);
							if (!_impassibleCellTypes.contains(newCellType)) {
								agent.setA(row);
								agent.setB(col);
							}
						} catch (IndexOutOfBoundsException ex) {
							// No need to do anything
						}
						
					}
					
					for(int xd=-1;xd<2;xd++){
						for(int yd=-1;yd<2;yd++){
							Pair<Integer,Integer> outcome = new Pair<Integer,Integer>(xd,yd);
							int ns = locationToState(sloc.getA()+xd, sloc.getB()+yd);
							tprobs[s][a][ns] += 
						}
					}
				}
			}
		}

		DiscreteMarkovDecisionProcess.Model mdp = new DiscreteMarkovDecisionProcess.Model(
				numStates, numActions, tprobs, rsignal, discountFactor);
		return mdp;
	}*/

}
