package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.Task;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * Implements a figure-eight tracing task. The objective of this task is for an
 * agent to traverse the environment in the shape of a figure-eight.
 * 
 * @author Timothy A. Mann
 * 
 */
public class FigureEightTask extends DiscreteTask implements Drawable
{
	private static final int MIN_WIDTH = 8;
	private static final int MIN_HEIGHT = 8;

	private enum Action {
		NORTH, SOUTH, EAST, WEST
	};

	/** A counter used to decide when to update the time counter. */
	private int _updateTimeCounter;
	/** The task time. */
	private int _time;
	/** The number of targets sampled from the figure-eight. */
	private int _targetResolution;
	/** The number of time steps between figure-eight target changes. */
	private int _targetChangeFrequency;

	/** The number of rows in the environment. */
	private int _nRows;
	/** The number of columns in the environment. */
	private int _nCols;

	/**
	 * The location of the agent.
	 */
	private Point2D _agent;

	/**
	 * The probability that the agent successfully performs an action.
	 */
	private double _actionSuccessProb;

	/**
	 * Constructs a figure-eight tracing task.
	 * 
	 * @param nRows
	 *            the number of rows in the environment
	 * @param nCols
	 *            the number of columns in the environment
	 * @param targetResolution the number of targets sampled from the figure-eight
	 * @param targetChangeFrequency the number of time steps between target updates
	 * @param actionSuccessProbability
	 *            the probability that the agent successfully performs its
	 *            selected action
	 */
	public FigureEightTask(int nRows, int nCols, int targetResolution, int targetChangeFrequency,
			double actionSuccessProbability)
	{
		super(nRows * nCols * targetResolution, Action.values().length, new Interval(0,1));
		if (nRows < MIN_HEIGHT) {
			throw new IllegalArgumentException(
					"The number of rows cannot be less than " + MIN_HEIGHT
							+ " for this task.");
		}
		_nRows = nRows;
		if (nCols < MIN_WIDTH) {
			throw new IllegalArgumentException(
					"The number of columns cannot be less than " + MIN_WIDTH
							+ " for this task.");
		}
		_nCols = nCols;

		if(targetResolution < 2){
			throw new IllegalArgumentException("The target resolution must be greater than 1.");
		}
		_targetResolution = targetResolution;
		
		if(targetChangeFrequency < 1){
			throw new IllegalArgumentException("Target change frequency must be at least 1.");
		}
		_targetChangeFrequency = targetChangeFrequency;
		
		if(actionSuccessProbability < 0 || actionSuccessProbability > 1){
			throw new IllegalArgumentException("The action success probability must be a scalar value in the interval [0, 1].");
		}
		_actionSuccessProb = actionSuccessProbability;

		_agent = new Point2D.Double();

		reset();
	}
	
	private Point2D newPos(Action action)
	{
		double r = Random.uniform();
		double x = _agent.getX();
		double y = _agent.getY();
		
		switch(action){
		case NORTH:
			y -= 1;
			break;
		case SOUTH:
			y += 1;
			break;
		case EAST:
			x += 1;
			break;
		case WEST:
			x -= 1;
			break;
		}
		
		if(r >= _actionSuccessProb){
			r = Random.uniform();
			if(r < 0.5){
				if(action.equals(Action.NORTH) || action.equals(Action.SOUTH)){
					x += 1;
				}else{
					y += 1;
				}
			}else{
				if(action.equals(Action.NORTH) || action.equals(Action.SOUTH)){
					x -= 1;
				}else{
					y -= 1;
				}
			}
		}
		return new Point2D.Double(x, y);
	}

	/**
	 * Determines a point in the figure-eight given the current time.
	 * 
	 * @param time
	 *            the current time
	 * @param maxTime
	 *            the time required to complete the figure-eight
	 * @param width
	 *            the width of the figure-eight
	 * @param height
	 *            the height of the figure-eight
	 * @return a point on the figure-eight curve
	 */
	public static final Point2D figureEight(double time, double maxTime,
			double width, double height)
	{
		if (time < 0) {
			throw new IllegalArgumentException(
					"Cannot compute position for negative time.");
		}
		if (maxTime <= 0) {
			throw new IllegalArgumentException(
					"Cannot compute position when the maximum time (or time to complete the figure-8) is not positive.");
		}

		double halfWidth = width / 2;
		double halfHeight = height / 2;

		if (time > maxTime) {
			double base = time * (int) (time % maxTime);
			time = time - base;
		}

		double t = (time / maxTime) * (2 * Math.PI);
		double sint = Math.sin(t);
		double cost = Math.cos(t);

		double x = halfWidth * sint + halfWidth;
		double y = halfHeight * cost * sint + halfHeight;
		return new Point2D.Double(x, y);
	}

	@Override
	public double evaluate()
	{
		Point2D target = figureEight(_time, _targetResolution, cols(), rows());
		double dist2 = target.distanceSq(_agent);
		return Math.exp(-dist2);
	}
	
	/**
	 * Returns an interval bounding the minimum and maximum rewards.
	 * @return an interval bounding the minimum and maximum rewards
	 */
	public Interval getRewardInterval()
	{
		return new Interval(rmin(), rmax());
	}

	@Override
	public void execute(Integer action)
	{
		_updateTimeCounter++;
		if(_updateTimeCounter % _targetChangeFrequency == 0){
			_time++;
		}
		if(_time > _targetResolution){
			_time = 1;
		}
		
		Point2D newPoint = newPos(Action.values()[action]);
		if(newPoint.getX() >= 0 && newPoint.getX() < _nCols && newPoint.getY() >= 0 && newPoint.getY() < _nRows){
			_agent.setLocation(newPoint);
		}
	}
	
	private Pair<Integer,Integer> getAgentCell()
	{
		int row = (int)(_agent.getY());
		int col = (int)(_agent.getX());
		
		return new Pair<Integer,Integer>(row, col);
	}
	
	/**
	 * Returns the number of rows in this gridworld instance.
	 * 
	 * @return the number of rows
	 */
	public final int rows()
	{
		return _nRows;
	}

	/**
	 * Returns the number of columns in this gridworld instance.
	 * 
	 * @return the number of columns
	 */
	public final int cols()
	{
		return _nCols;
	}

	@Override
	public Integer getState()
	{
		Pair<Integer,Integer> agentCell = getAgentCell();
		return toState(_time, agentCell.getA(), agentCell.getB(), _nRows, _nCols);
	}
	
	/**
	 * Converts a set of attributes to an integer state.
	 * @param time the time
	 * @param row the agent's current row
	 * @param col the agent's current column
	 * @param nRows the number of rows
	 * @param nCols the number of columns
	 * @return an integer state
	 */
	public static final Integer toState(int time, int row, int col, int nRows, int nCols)
	{
		return (nRows * nCols * (time - 1)) + (nCols * row) + col;
	}
	
	
	/**
	 * Extracts a row and column of the agent given the state.
	 * @param state a state
	 * @param numRows the number of rows
	 * @param numCols the number of columns
	 * @return a pair containing (row, column) of the agent
	 */
	public static final Pair<Integer,Integer> locationFromState(Integer state, int numRows, int numCols)
	{
		int rowCol = state.intValue() % (numRows * numCols);
		int row = rowCol / numCols;
		int col = rowCol % numCols;
		
		return new Pair<Integer,Integer>(row, col);
	}
	
	public static final int timeFromState(Integer state, int numRows, int numCols)
	{
		return (state.intValue() / (numRows * numCols)) + 1;
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public void reset()
	{
		_time = 1;
		_updateTimeCounter = 0;
		Point2D initP = figureEight(0, _targetResolution, cols(), rows());
		_agent.setLocation(initP.getX(), initP.getY());
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, width, height);

		double cellWidth = Math.min(width / (double) cols(), height
				/ (double) rows());
		
		Path2D figureEight = new Path2D.Double();
		double[] t = VectorOps.range(0, _targetResolution, 200);
		for(int i=0;i<t.length;i++){
			Point2D p = figureEight(t[i], _targetResolution, cols(), rows());
			if(i==0){
				figureEight.moveTo(p.getX() * cellWidth, p.getY() * cellWidth);
			}else{
				figureEight.lineTo(p.getX() * cellWidth, p.getY() * cellWidth);
			}
		}
		figureEight.closePath();
		g2.setColor(Color.BLUE);
		g2.draw(figureEight);
		
		g2.setColor(Color.BLACK);
		for(int r = 0; r < rows(); r++){
			for(int c = 0; c < cols(); c++){
				Rectangle2D rect = new Rectangle2D.Double(r * cellWidth, c * cellWidth, cellWidth, cellWidth);
				g2.draw(rect);
			}
		}
		
		Pair<Integer,Integer> agentCell = getAgentCell();
		double awidth = cellWidth / 2;
		Ellipse2D agentShape = new Ellipse2D.Double((agentCell.getB() + 0.5) * cellWidth - (awidth/2), (agentCell.getA() + 0.5) * cellWidth - (awidth/2), awidth, awidth);
		g2.setColor(Color.GREEN);
		g2.fill(agentShape);
		g2.setColor(Color.BLACK);
		g2.draw(agentShape);
		
		Point2D target = figureEight(_time, _targetResolution, cols(), rows());
		double twidth = cellWidth / 3;
		Ellipse2D targetShape = new Ellipse2D.Double(target.getX() * cellWidth - (twidth/2), target.getY() * cellWidth - (twidth/2), twidth, twidth);
		g2.setColor(Color.RED);
		g2.fill(targetShape);
		g2.setColor(Color.BLACK);
		g2.draw(targetShape);
		
		g2.dispose();
	}

}
