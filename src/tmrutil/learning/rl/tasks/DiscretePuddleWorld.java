package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.QLearning;
import tmrutil.learning.rl.RMax;
import tmrutil.learning.rl.Task;
import tmrutil.math.MatrixOps;
import tmrutil.util.Interval;

/**
 * A discrete wrapper around the continuous puddle world task.
 * @author Timothy A. Mann
 *
 */
public class DiscretePuddleWorld extends Task<Integer, Integer> implements Drawable
{
	private PuddleWorld _puddleWorld;
	private int _discretization;
	
	public DiscretePuddleWorld(int discretization)
	{
		_puddleWorld = new PuddleWorld();
		_discretization = discretization;
	}
	
	@Override
	public double evaluate()
	{
		double r = _puddleWorld.evaluate();
		//if(r > -1){
		//	double[] state = _puddleWorld.getState();
		//	System.out.println("(" + state[0] + ", " + state[1] + ") ==> reward := " + r);
		//}
		return r;
	}

	@Override
	public void execute(Integer action)
	{
		_puddleWorld.execute(action);
	}

	@Override
	public Integer getState()
	{
		double[] stateVector = _puddleWorld.getState();
		int x = (int)(stateVector[0] * (_discretization-1));
		int y = (int)(stateVector[1] * (_discretization-1));
		//if(inGoal()){
		//	System.out.println("(x = " + stateVector[0] + ", y = " + stateVector[1] + ")");
		//	System.out.println("(x = " + x + ", y = " + y + ") ==> " + ((x * _discretization) + y));
		//}
		return (x * _discretization) + y;
	}
	
	/**
	 * Converts a state to a state vector.
	 * @param state a state
	 * @return a state vector
	 */
	public double[] stateToVector(Integer state)
	{
		int x = state / _discretization;
		int y = state % _discretization;
		return new double[]{x / (double) _discretization, y / (double) _discretization};
	}

	public boolean inGoal()
	{
		return _puddleWorld.inGoal();
	}
	
	@Override
	public boolean isFinished()
	{
		return _puddleWorld.isFinished();
	}

	@Override
	public void reset()
	{
		_puddleWorld.reset();
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_puddleWorld.draw(g, width, height);
	}

	public int numberOfActions()
	{
		return 4;
	}
	
	public int numberOfStates()
	{
		return _discretization * _discretization;
	}
	
	public void displayStateValues(double[] V, String title)
	{
		double[][] mapV = new double[_discretization][_discretization];
		for(int x = 0; x < _discretization; x++){
			for(int y = 0; y < _discretization; y++){
				int index = (x * _discretization) + y;
				mapV[_discretization - (y + 1)][x] = V[index]; 
			}
		}
		MatrixOps.displayGraphically(mapV, title);
	}
	
	public static void main(String[] args){
		double learningRate = 0.2;
		double discountFactor = 0.9;
		double epsilon = 0.1;
		int discretization = 20;
		DiscretePuddleWorld task = new DiscretePuddleWorld(discretization);
		//QLearning agent = new QLearning(task.numberOfStates(), task.numberOfActions(), learningRate, discountFactor);
		//agent.setEpsilon(epsilon);
		RMax agent = new RMax(task.numberOfStates(), task.numberOfActions(), discountFactor, new Interval(-40, 1), 5);
		
		int numEpisodes = 1000;
		int episodeLength = 1000;
		Task.runTask(task, agent, numEpisodes, episodeLength, true, null, null, true);
		
		//double[] V = agent.getValueFunction();
		//double[] R = agent.getStateRewards();
		//task.displayStateValues(V, "State Values");
		//task.displayStateValues(R, "State Rewards");
		
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(400,400);
		Task.runTask(task, agent, numEpisodes, episodeLength, true, null, frame, false);
	}
}
