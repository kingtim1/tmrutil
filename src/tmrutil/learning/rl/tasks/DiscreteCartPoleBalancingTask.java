package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.Task;

public class DiscreteCartPoleBalancingTask extends Task<Integer, Integer>
		implements Drawable
{
	private CartPoleBalancingTask _task;
	private int _numDiscrete;

	public DiscreteCartPoleBalancingTask(int numDiscrete)
	{
		_task = new CartPoleBalancingTask();
		_numDiscrete = numDiscrete;
	}

	@Override
	public double evaluate()
	{
		int poleAngleInd = 2;
		double poleAngle = _task.getState()[poleAngleInd];
		double zeroAngle = 0;
		double minVal = -_task.getFailAngle() * 1.1;
		double maxVal = _task.getFailAngle() * 1.1;
		int poleAngleBin = this.whichBin(poleAngle, minVal, maxVal, _numDiscrete);
		int zeroBin = this.whichBin(zeroAngle, minVal, maxVal, _numDiscrete);
		if(poleAngleBin == zeroBin){
			return 1;
		}else{
			return 0;
		}
	}

	@Override
	public void execute(Integer action)
	{
		_task.execute(actionToVector(action));
	}

	@Override
	public Integer getState()
	{
		return vectorToState(_task.getState());
	}

	@Override
	public boolean isFinished()
	{
		return _task.isFinished();
	}

	@Override
	public void reset()
	{
		_task.reset();
	}
	
	public int numberOfActions()
	{
		return 3;
	}
	
	public int numberOfStates()
	{
		return _numDiscrete * _numDiscrete * _numDiscrete * _numDiscrete;
	}

	public double[] actionToVector(Integer action)
	{
		switch (action) {
		case 0:
			return new double[] { 0 };
		case 1:
			return new double[] { -1 };
		case 2:
			return new double[] { 1 };
		default:
			throw new IllegalArgumentException(
					"The specified action does not map to a vector.");
		}
	}

//	public double[] stateToVector(Integer state)
//	{
//
//	}

	public Integer vectorToState(double[] stateVector)
	{
		int cartPosInd = 0;
		int cartVelInd = 1;
		int poleAngleInd = 2;
		int poleVelInd = 3;

		double minVel = -1;
		double maxVel = 1;

		int cartPosBin = whichBin(stateVector[cartPosInd],
				-_task.getTrackLength() / 2, _task.getTrackLength() / 2,
				_numDiscrete);
		int cartVelBin = whichBin(stateVector[cartVelInd], minVel, maxVel,
				_numDiscrete);
		int poleAngleBin = whichBin(stateVector[poleAngleInd],
				-_task.getFailAngle() * 1.1, _task.getFailAngle() * 1.1,
				_numDiscrete);
		int poleVelBin = whichBin(stateVector[poleVelInd], minVel, maxVel,
				_numDiscrete);

		return cartPosBin * (_numDiscrete * _numDiscrete * _numDiscrete)
				+ (cartVelBin * (_numDiscrete * _numDiscrete))
				+ (poleAngleBin * (_numDiscrete)) + poleVelBin;
	}

	private int whichBin(double value, double minVal, double maxVal,
			int numDiscrete)
	{
		double diff = maxVal - minVal;
		double inc = diff / numDiscrete;
		int bin = (int) ((value - minVal) / inc);
		if (bin < 0) {
			bin = 0;
		}
		if (bin >= numDiscrete) {
			bin = numDiscrete - 1;
		}
		return bin;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_task.draw(g, width, height);
	}

}
