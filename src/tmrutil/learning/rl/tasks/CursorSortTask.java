package tmrutil.learning.rl.tasks;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteActionTask;
import tmrutil.learning.rl.RandomLearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.math.VectorOps;

/**
 * The sorting task presents the learning system with a shuffled sequence of
 * numbers. The objective of the learning system is to swap pairs of values to
 * sort the sequence of numbers in ascending order. This is a very difficult
 * reinforcement learning problem because there are a large number of states and
 * actions.
 * 
 * @author Timothy A. Mann
 * 
 */
public class CursorSortTask extends DiscreteActionTask<int[]> implements
		Drawable
{
	private static final int LEFT_ACTION = 0, RIGHT_ACTION = 1,
			SWAP_ACTION = 2;
	public static final int NUM_ACTIONS = 3;

	private int[] _unsortedVals;
	private int _cursor;
	private boolean _finished;

	public CursorSortTask(int numElms)
	{
		super(NUM_ACTIONS);
		_unsortedVals = VectorOps.randperm(numElms);
		_cursor = 0;
		_finished = false;
	}

	@Override
	public int[] getState()
	{
		int[] state = Arrays.copyOf(_unsortedVals, _unsortedVals.length + 1);
		state[_unsortedVals.length] = _cursor;
		return state;
	}

	@Override
	public void execute(Integer action)
	{
		if (!isSorted()) {
			if (action == LEFT_ACTION) {
				_cursor--;
				if (_cursor < 0) {
					_cursor = _unsortedVals.length - 2;
				}
			}
			if (action == RIGHT_ACTION) {
				_cursor++;
				if (_cursor > _unsortedVals.length - 2) {
					_cursor = 0;
				}
			}
			if (action == SWAP_ACTION) {
				// System.err.println("SWAP CALLED!");
				int index = _cursor;
				int backupA = _unsortedVals[index + 1];
				int backupB = _unsortedVals[index];
				_unsortedVals[index + 1] = backupB;
				_unsortedVals[index] = backupA;
			}
		}else{
			_finished = true;
		}
	}

	@Override
	public double evaluate()
	{
		if (isSorted()) {
			//System.out.println("REWARD!");
			return 1;
		} else {
			return 0;
		}
		//return 1 - (mistakeCount() / (_unsortedVals.length - 1));
		// return -mistakeCount();
	}

	@Override
	public void reset()
	{
		int numElms = _unsortedVals.length;
		_unsortedVals = VectorOps.randperm(numElms);
		_cursor = 0;
		_finished = false;
	}

	@Override
	public boolean isFinished()
	{
		return _finished;
	}

	public boolean isSorted()
	{
		boolean sorted = true;
		int prev = _unsortedVals[0];
		for (int i = 1; i < _unsortedVals.length; i++) {
			int val = _unsortedVals[i];
			if (prev > val) {
				sorted = false;
				break;
			}
			prev = val;
		}
		return sorted;
	}

	public int mistakeCount()
	{
		int mc = 0;
		int prev = _unsortedVals[0];
		for (int i = 1; i < _unsortedVals.length; i++) {
			int val = _unsortedVals[i];
			if (prev > val) {
				mc++;
			}
			prev = val;
		}
		return mc;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		if(isSorted()){
			g.setColor(Color.GREEN);
			g.fillRect(0, 0, width, height);
		}
		
		double cwidth = width / (double) _unsortedVals.length;
		for (int i = 0; i < _unsortedVals.length; i++) {
			Rectangle2D rect = new Rectangle2D.Double(i * cwidth, 0, cwidth,
					height);

			int val = _unsortedVals[i];
			if (i + 1 < _unsortedVals.length) {
				if (val > _unsortedVals[i + 1]) {
					g.setColor(Color.RED);
					g.fill(rect);
				}
			}

			g.setColor(Color.BLACK);
			String valStr = String.valueOf(val);
			FontMetrics fm = g.getFontMetrics();
			Rectangle2D bounds = fm.getStringBounds(valStr, g);
			g.drawString(valStr,
					(int) (rect.getCenterX() - bounds.getWidth() / 2),
					(int) (rect.getCenterY() + bounds.getHeight() / 2));

			g.draw(rect);
		}

		Rectangle2D cursorRect = new Rectangle2D.Double(_cursor * cwidth, 0,
				2 * cwidth, height);
		g.setStroke(new BasicStroke(4.0f));
		g.setColor(Color.GREEN);
		g.draw(cursorRect);
	}

	public static void main(String[] args)
	{
		int numElms = 10;
		List<Integer> actions = new ArrayList<Integer>(numElms);
		for (int i = 0; i < numElms; i++) {
			actions.add(i);
		}
		RandomLearningSystem<int[], Integer> ragent = new RandomLearningSystem<int[], Integer>(
				actions);
		CursorSortTask task = new CursorSortTask(numElms);

		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(400, 100);
		Task.runTask(task, ragent, 10, 100, false, null, frame);
	}

}
