package tmrutil.learning.rl.tasks;

import tmrutil.kinematics.Arm;
import tmrutil.util.ArrayOps;

/**
 * Used for representing the high-level state of a bimanual reaching task.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BimanualState
{
	/** A reference to a copy of the left arm. */
	private Arm _leftArm;
	/** A reference to a copy of the right arm. */
	private Arm _rightArm;

	/**
	 * Constructs an instance of the bimanual state.
	 * 
	 * @param leftArm
	 *            a reference to the left arm
	 * @param rightArm
	 *            a reference to the right arm
	 */
	public BimanualState(Arm leftArm, Arm rightArm)
	{
		_leftArm = new Arm(leftArm);
		_rightArm = new Arm(rightArm);
	}

	/**
	 * Constructs a copy of an existing bimanual state.
	 * 
	 * @param state
	 *            the state to copy
	 */
	public BimanualState(BimanualState state)
	{
		this(state._leftArm, state._rightArm);
	}

	/**
	 * Returns a reference to the left arm.
	 * 
	 * @return the left arm
	 */
	public Arm leftArm()
	{
		return _leftArm;
	}

	/**
	 * Returns a reference to the right arm.
	 * 
	 * @return the right arm
	 */
	public Arm rightArm()
	{
		return _rightArm;
	}

	/**
	 * Generates a vector with noise-free proprioceptive information about the
	 * joint angles of both arms.
	 * 
	 * @return a state vector
	 */
	public double[] proprioceptiveState()
	{
		double[] leftArmJA = _leftArm.getNormalizedJointAngles();
		double[] rightArmJA = _rightArm.getNormalizedJointAngles();

		return ArrayOps.concat(leftArmJA, rightArmJA);
	}
}
