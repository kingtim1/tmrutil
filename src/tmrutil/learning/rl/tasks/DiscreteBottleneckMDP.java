package tmrutil.learning.rl.tasks;

import java.awt.Point;
import java.util.HashSet;
import java.util.Set;

import tmrutil.learning.rl.DiscreteMarkovDecisionProcess;
import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.PrimitiveAction;
import tmrutil.stats.Filter;
import tmrutil.stats.GenerativeDistribution;
import tmrutil.stats.Random;

/**
 * Defines the Discrete Bottleneck Markov decision process model.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DiscreteBottleneckMDP implements
		DiscreteMarkovDecisionProcess<Integer, Integer> {

	public static final int ENV_WIDTH = 21;
	public static final int ENV_HEIGHT = 11;
	
	public static final int NUM_STATES = ENV_WIDTH * ENV_HEIGHT;
	public static final int NUM_ACTIONS = 4;
	
	public static final Point INIT = new Point(0,0);
	public static final Point GOAL = new Point(ENV_WIDTH-1, ENV_HEIGHT-1);
	public static final Point DOORWAY = new Point(ENV_WIDTH/2, ENV_HEIGHT/2);

	private double[][] _r;
	private double[][][] _p;
	private DiscreteMarkovDecisionProcess.Model _mdp;

	public DiscreteBottleneckMDP(double discountFactor) {
		_r = defineR();
		_p = defineP();
		_mdp = new DiscreteMarkovDecisionProcess.Model(NUM_STATES, NUM_ACTIONS,
				_p, _r, discountFactor);
	}

	@Override
	public double transitionProb(Integer state, Integer action,
			Integer resultState) {
		return _mdp.transitionProb(state, action, resultState);
	}

	@Override
	public double reinforcement(Integer state, Integer action) {
		return _mdp.reinforcement(state, action);
	}

	@Override
	public double getDiscountFactor() {
		return _mdp.getDiscountFactor();
	}

	@Override
	public Set<Integer> states() {
		return _mdp.states();
	}

	@Override
	public Set<Integer> actions(Integer state) {
		return _mdp.actions(state);
	}

	@Override
	public Set<Integer> successors(Integer state) {
		return _mdp.successors(state);
	}

	@Override
	public Set<Integer> predecessors(Integer state) {
		return _mdp.predecessors(state);
	}

	@Override
	public int numberOfStates() {
		return _mdp.numberOfStates();
	}

	@Override
	public int numberOfActions() {
		return _mdp.numberOfActions();
	}

	@Override
	public double rmax() {
		return _mdp.rmax();
	}

	@Override
	public double rmin() {
		return _mdp.rmin();
	}

	@Override
	public boolean isKnown(Integer state, Integer action) {
		return _mdp.isKnown(state, action);
	}

	private static int xyToIndex(int x, int y) {
		return y * ENV_WIDTH + x;
	}

	public static int xyToIndex(Point p) {
		return xyToIndex(p.x, p.y);
	}

	public static Point indexToXY(Integer state) {
		return new Point(state % ENV_WIDTH, state / ENV_WIDTH);
	}

	private boolean inBounds(int x, int y) {
		return (x >= 0 && x < ENV_WIDTH) && (y >= 0 && y < ENV_HEIGHT);
	}

	private Set<Point> generateAbsorbing() {
		Set<Point> absorbing = new HashSet<Point>();
		return absorbing;
	}

	private Set<Point> generateWalls() {
		Set<Point> walls = new HashSet<Point>();

		for (int i = 0; i <= ENV_HEIGHT; i++) {
			if (i != ENV_HEIGHT/2) {
				walls.add(new Point(ENV_WIDTH/2, i));
			}
		}

		return walls;
	}

	public boolean wall(int x, int y) {
		return !inBounds(x, y) || generateWalls().contains(new Point(x, y));
	}

	private boolean wall(Point p) {
		return wall(p.x, p.y);
	}

	private boolean hitsWall(Integer state, Integer action) {
		Point xy = indexToXY(state);
		return hitsWall(xy.x, xy.y, action);
	}

	private boolean hitsWall(int x, int y, Integer action) {
		switch (action) {
		case NORTH_ACTION:
			y = y - 1;
			break;
		case SOUTH_ACTION:
			y = y + 1;
			break;
		case EAST_ACTION:
			x = x + 1;
			break;
		case WEST_ACTION:
			x = x - 1;
			break;
		default:
		}

		return wall(x, y);
	}

	private static final int NORTH_ACTION = 0;
	private static final int SOUTH_ACTION = 1;
	private static final int EAST_ACTION = 2;
	private static final int WEST_ACTION = 3;

	public static final Set<Integer> primitiveActions()
	{
		Set<Integer> pactions = new HashSet<Integer>();
		pactions.add(NORTH_ACTION);
		pactions.add(SOUTH_ACTION);
		pactions.add(EAST_ACTION);
		pactions.add(WEST_ACTION);
		
		return pactions;
	}
	
	public static final Set<Option<Integer,Integer>> primitiveActionsAsOptions()
	{
		Set<Option<Integer,Integer>> pactions = new HashSet<Option<Integer,Integer>>();
		
		pactions.add(new PrimitiveAction<Integer,Integer>(0, NORTH_ACTION));
		pactions.add(new PrimitiveAction<Integer,Integer>(1, SOUTH_ACTION));
		pactions.add(new PrimitiveAction<Integer,Integer>(2, EAST_ACTION));
		pactions.add(new PrimitiveAction<Integer,Integer>(3, WEST_ACTION));
		
		return pactions;
	}
	
	public static final Set<Option<Integer,Integer>> primitivePlusLeftRoomToDoorOption()
	{
		Set<Option<Integer,Integer>> options = primitiveActionsAsOptions();
		
		Set<Point> initStates = new HashSet<Point>();
		for(int x=0;x<ENV_WIDTH/2;x++){
			for(int y=0;y<ENV_HEIGHT;y++){
				initStates.add(new Point(x,y));
			}
		}
		options.add(new ToPointOption(DOORWAY, initStates));
		
		return options;
	}
	
	public static final Set<Option<Integer,Integer>> primitivePlusInitStateToDoorOption()
	{
		Set<Option<Integer,Integer>> options = primitiveActionsAsOptions();
		
		Set<Point> initStates = new HashSet<Point>();
		initStates.add(INIT);
		options.add(new ToPointOption(DOORWAY, initStates));
		
		return options;
	}
	
	public static final Set<Option<Integer,Integer>> primitivePlusDoorToGoalOptions()
	{
		Set<Option<Integer,Integer>> options = primitiveActionsAsOptions();
		
		Set<Point> initStates = new HashSet<Point>();
		initStates.add(DOORWAY);
		options.add(new ToPointOption(GOAL, initStates));
		
		return options;
	}
	
	public static final Set<Option<Integer,Integer>> primitivePlusRightRoomToGoalOptions()
	{
		Set<Option<Integer,Integer>> options = primitiveActionsAsOptions();
		
		Set<Point> initStates = new HashSet<Point>();
		for(int x=ENV_WIDTH/2;x<ENV_WIDTH;x++){
			for(int y=0;y<ENV_HEIGHT;y++){
				initStates.add(new Point(x,y));
			}
		}
		options.add(new ToPointOption(GOAL, initStates));
		
		return options;
	}
	
	public static final Set<Option<Integer,Integer>> primitivePlusRightRoomCornerToGoalOptions()
	{
		Set<Option<Integer,Integer>> options = primitiveActionsAsOptions();
		
		Set<Point> initStates = new HashSet<Point>();
		initStates.add(new Point(11,0));
		options.add(new ToPointOption(GOAL, initStates));
		
		return options;
	}
	
	public static class ToPointOption implements Option<Integer,Integer>
	{
		private Point _target;
		private Set<Integer> _initSet;

		public ToPointOption(Point target, Set<Point> initSet)
		{
			_target = target;
			_initSet = new HashSet<Integer>();
			for(Point p : initSet){
				_initSet.add(xyToIndex(p));
			}
		}
		
		@Override
		public Integer policy(Integer state) {
			Point pstate = indexToXY(state);
			int xdiff = _target.x - pstate.x;
			int ydiff = _target.y - pstate.y;
			if(Math.abs(xdiff) > Math.abs(ydiff)){
				if(xdiff > 0){
					return EAST_ACTION;
				}else{
					return WEST_ACTION;
				}
			}else{
				if(ydiff > 0){
					return SOUTH_ACTION;
				}else if(ydiff == 0){
					return Random.nextInt(4);
				}else{
					return NORTH_ACTION;
				}
			}
		}

		@Override
		public double terminationProb(Integer state, int duration) {
			Point pstate = indexToXY(state);
			return pstate.equals(_target)? 1 : 0;
		}

		@Override
		public boolean inInitialSet(Integer state) {
			return _initSet.contains(state);
		}
		
	}
	
	private double[][] defineR() {
		double[][] r = new double[NUM_STATES][NUM_ACTIONS];

		for(int s=0;s<NUM_STATES;s++){
			for(int a=0;a<NUM_ACTIONS;a++){
				r[s][a] = 0.0;
			}
		}
		
		for (int a = 0; a < NUM_ACTIONS; a++) {
			// Define main goal state
			r[xyToIndex(GOAL)][a] = 1.0;
		}

		return r;
	}
	
	private static final double USUAL_PROB = 0.8;
	private static final double ERR_PROB = (1 - USUAL_PROB)/2;

	private double[][][] defineP() {
		double[][][] p = new double[NUM_STATES][NUM_ACTIONS][NUM_STATES];

		for (int s = 0; s < NUM_STATES; s++) {
			for (int a = 0; a < NUM_ACTIONS; a++) {
				Point xy = indexToXY(s);
				if (wall(xy) || generateAbsorbing().contains(xy) || hitsWall(s, a)) {
					p[s][a][s] = 1;
				} else {
					Point usual = null;
					Point err1 = null;
					Point err2 = null;

					switch (a) {
					case NORTH_ACTION:
						usual = new Point(xy.x, xy.y - 1);
						err1 = new Point(xy.x + 1, xy.y - 1);
						err2 = new Point(xy.x - 1, xy.y - 1);

						p[s][a][xyToIndex(usual)] = USUAL_PROB;

						if (wall(err1) && wall(err2)) {
							p[s][a][xyToIndex(usual)] = 1;
						} else if (wall(err1)) {
							p[s][a][xyToIndex(err2)] = 2 * ERR_PROB;
						} else if (wall(err2)) {
							p[s][a][xyToIndex(err1)] = 2 * ERR_PROB;
						} else {
							p[s][a][xyToIndex(err1)] = ERR_PROB;
							p[s][a][xyToIndex(err2)] = ERR_PROB;
						}
						break;
					case SOUTH_ACTION:
						usual = new Point(xy.x, xy.y + 1);
						err1 = new Point(xy.x + 1, xy.y + 1);
						err2 = new Point(xy.x - 1, xy.y + 1);

						p[s][a][xyToIndex(usual)] = USUAL_PROB;

						if (wall(err1) && wall(err2)) {
							p[s][a][xyToIndex(usual)] = 1;
						} else if (wall(err1)) {
							p[s][a][xyToIndex(err2)] = 2 * ERR_PROB;
						} else if (wall(err2)) {
							p[s][a][xyToIndex(err1)] = 2 * ERR_PROB;
						} else {
							p[s][a][xyToIndex(err1)] = ERR_PROB;
							p[s][a][xyToIndex(err2)] = ERR_PROB;
						}
						break;
					case EAST_ACTION:
						usual = new Point(xy.x + 1, xy.y);
						err1 = new Point(xy.x + 1, xy.y + 1);
						err2 = new Point(xy.x + 1, xy.y - 1);

						p[s][a][xyToIndex(usual)] = USUAL_PROB;

						if (wall(err1) && wall(err2)) {
							p[s][a][xyToIndex(usual)] = 1;
						} else if (wall(err1)) {
							p[s][a][xyToIndex(err2)] = 2 * ERR_PROB;
						} else if (wall(err2)) {
							p[s][a][xyToIndex(err1)] = 2 * ERR_PROB;
						} else {
							p[s][a][xyToIndex(err1)] = ERR_PROB;
							p[s][a][xyToIndex(err2)] = ERR_PROB;
						}
						break;
					case WEST_ACTION:
						usual = new Point(xy.x - 1, xy.y);
						err1 = new Point(xy.x - 1, xy.y + 1);
						err2 = new Point(xy.x - 1, xy.y - 1);

						p[s][a][xyToIndex(usual)] = USUAL_PROB;

						if (wall(err1) && wall(err2)) {
							p[s][a][xyToIndex(usual)] = 1;
						} else if (wall(err1)) {
							p[s][a][xyToIndex(err2)] = 2 * ERR_PROB;
						} else if (wall(err2)) {
							p[s][a][xyToIndex(err1)] = 2 * ERR_PROB;
						} else {
							p[s][a][xyToIndex(err1)] = ERR_PROB;
							p[s][a][xyToIndex(err2)] = ERR_PROB;
						}
						break;
					}
				}
			}
		}

		return p;
	}
	
	public static class InitStateDistribution implements GenerativeDistribution<Integer>
	{

		@Override
		public Integer sample() {
			int x = INIT.x;
			int y = INIT.y;
			
			Integer state = y * ENV_WIDTH + x;
			return state;
		}
		
	}
	
	public static class TerminalStateFilter implements Filter<Integer>
	{

		@Override
		public boolean accept(Integer x) {
			return false;
		}
		
	}
}
