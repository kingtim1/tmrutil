package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.tasks.MazeTask.MazeTile;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * Implementation of abstract gridworld domains.
 * @author Timothy A. Mann
 *
 */
public class Gridworld extends DiscreteTask implements Drawable
{
	private static final int NUM_ACTIONS = 4;
	private static final Interval REWARD_INTERVAL = new Interval(-1, 1);
	
	private Pair<Integer, Integer> _init;
	private Pair<Integer, Integer> _goal;

	private Pair<Integer, Integer> _agent;

	private boolean _finished;

	private int _size;

	/**
	 * The probability of executing an action successfully.
	 */
	private double _successProb;

	public Gridworld(int size, Pair<Integer, Integer> init,
			Pair<Integer, Integer> goal, double successProb)
	{
		super(size * size, NUM_ACTIONS, REWARD_INTERVAL);
		if (size < 2) {
			throw new IllegalArgumentException(
					"The size of the grid world must be larger than 1x1.");
		}
		_size = size;

		if (init.equals(goal)) {
			throw new IllegalArgumentException(
					"The initial location cannot equal the goal location.");
		}
		if (init.getA() < 0 || init.getA() >= size || init.getB() < 0
				|| init.getB() >= size) {
			throw new IllegalArgumentException(
					"The initial location exceeds the bounds of the grid world.");
		}
		_init = new Pair<Integer, Integer>(init.getA(), init.getB());

		if (goal.getA() < 0 || goal.getA() >= size || goal.getB() < 0
				|| goal.getB() >= size) {
			throw new IllegalArgumentException(
					"The goal location exceeds the bounds of the grid world.");
		}
		_goal = new Pair<Integer, Integer>(goal.getA(), goal.getB());

		_agent = new Pair<Integer, Integer>(init.getA(), init.getB());

		if (successProb <= 0 || successProb > 1) {
			throw new IllegalArgumentException(
					"The probability of successfully executing an action must be in the interval (0, 1].");
		}
		_successProb = successProb;

		reset();
	}

	@Override
	public double evaluate()
	{
		if (_agent.equals(_goal)) {
			return rmax();
		} else {
			return rmin();
		}
	}

	@Override
	public void execute(Integer action)
	{
		if (_agent.equals(_goal)) {
			_finished = true;
			return;
		}

			int agentX = _agent.getA();
			int agentY = _agent.getB();
			switch (action) {
			case 0:
				agentX += 1;
				break;
			case 1:
				agentX -= 1;
				break;
			case 2:
				agentY += 1;
				break;
			case 3:
				agentY -= 1;
				break;
			default:
			}

			double random = Random.uniform();
			if (random > _successProb) {
				if(Math.abs(agentX - _agent.getA()) > 0){
					if(Random.uniform() < 0.5){
						agentY += 1;
					}else{
						agentY -= 1;
					}
				}else{
					if(Random.uniform() < 0.5){
						agentX += 1;
					}else{
						agentX -= 1;
					}
				}
			}
			
			if (agentX >= 0 && agentX < _size && agentY >= 0 && agentY < _size) {
				_agent.setA(agentX);
				_agent.setB(agentY);
			}
	}

	@Override
	public Integer getState()
	{
		return locationToState(_agent.getA(), _agent.getB());
	}
	
	/**
	 * Extracts a location from a state.
	 * @param state a state
	 * @return a location
	 */
	public Pair<Integer,Integer> stateToLocation(Integer state)
	{
		int x = state / _size;
		int y = state % _size;
		return new Pair<Integer,Integer>(x,y);
	}
	
	/**
	 * Converts a location to a state.
	 * @param x an x-coordinate
	 * @param y a y-coordinate
	 * @return the state corresponding to the specified location
	 */
	public Integer locationToState(int x, int y)
	{
		return (x * _size) + y;
	}

	@Override
	public boolean isFinished()
	{
		return _finished;
	}

	@Override
	public void reset()
	{
		_agent.setA(_init.getA());
		_agent.setB(_init.getB());
		_finished = false;
	}
	
	/**
	 * Returns the size of one side of this environment.
	 * @return the size of this environment
	 */
	public int size()
	{
		return _size;
	}
	
	/**
	 * Returns the location of the initial state.
	 * @return the initial location
	 */
	public Pair<Integer,Integer> getInit()
	{
		return new Pair<Integer,Integer>(_init.getA(), _init.getB());
	}
	
	/**
	 * Returns the location of the goal state.
	 * @return the goal location
	 */
	public Pair<Integer,Integer> getGoal()
	{
		return new Pair<Integer,Integer>(_goal.getA(), _goal.getB());
	}
	
	/**
	 * Returns the location of the agent.
	 * @return the agent location
	 */
	public Pair<Integer,Integer> getAgent()
	{
		return new Pair<Integer,Integer>(_agent.getA(), _agent.getB());
	}
	
	/**
	 * Sets the location of the agent.
	 * @param loc the new location of the agent
	 */
	public void setAgent(Pair<Integer,Integer> loc)
	{
		int x = loc.getA();
		int y = loc.getB();
		if(x < 0 || x >= size() || y < 0 || y >= size()){
			throw new IllegalArgumentException("Invalid agent location specified (" + x + "," + y + ")");
		}
		_agent.setA(x);
		_agent.setB(y);
	}

	/**
	 * Generates the collection of valid actions for a grid world instance.
	 * 
	 * @return a collection of valid actions
	 */
	public static final Collection<Integer> generateActions()
	{
		Collection<Integer> actions = new ArrayList<Integer>();
		actions.add(0);
		actions.add(1);
		actions.add(2);
		actions.add(3);
		return actions;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Color agentColor = Color.BLUE;
		Color goalColor = Color.RED;
		Color initColor = Color.GREEN;
		Color emptyColor = Color.LIGHT_GRAY;

		Graphics2D g2 = (Graphics2D) g.create();
		double rwidth = Math.min(width / (double) _size, height
				/ (double) _size);

		for (int x = 0; x < _size; x++) {
			for (int y = 0; y < _size; y++) {
				g2.setColor(emptyColor);
				Rectangle2D rect = new Rectangle2D.Double(x * rwidth, y
						* rwidth, rwidth, rwidth);
				g2.fill(rect);
				g2.setColor(Color.BLACK);
				g2.draw(rect);
			}
		}

		// Draw the init
		g2.setColor(initColor);
		Rectangle2D initRect = new Rectangle2D.Double(_init.getA() * rwidth,
				_init.getB() * rwidth, rwidth, rwidth);
		g2.fill(initRect);
		g2.setColor(Color.BLACK);
		g2.draw(initRect);

		// Draw the goal
		g2.setColor(goalColor);
		Rectangle2D goalRect = new Rectangle2D.Double(_goal.getA() * rwidth,
				_goal.getB() * rwidth, rwidth, rwidth);
		g2.fill(goalRect);
		g2.setColor(Color.BLACK);
		g2.draw(goalRect);

		// Draw the agent
		g2.setColor(agentColor);
		Rectangle2D agentRect = new Rectangle2D.Double(_agent.getA() * rwidth,
				_agent.getB() * rwidth, rwidth, rwidth);
		g2.fill(agentRect);
		g2.setColor(Color.BLACK);
		g2.draw(agentRect);
	}

}
