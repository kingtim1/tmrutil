package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;
import tmrutil.graphics.Drawable;
import tmrutil.graphics.plot2d.Plot2D;
import tmrutil.learning.rl.DelayedQLearning;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.QLearning;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.Translator;
import tmrutil.learning.rl.tasks.GoldMiningTask.Action;
import tmrutil.learning.rl.tasks.GoldMiningTask.GoldMiningState;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;
import tmrutil.util.Interval;

/**
 * A simple task for demonstrating the value of exploration transfer.
 * 
 * @author Timothy A. Mann
 * 
 */
public class GoldMiningTask extends Task<GoldMiningState, Action> implements
		Drawable
{
	public static enum SiteDescription {
		QUARTZ, GRANITE, MARBLE, SAND, EMPTY
	};

	public static final Map<SiteDescription, Double> SITE_GOLD_PROBABILITIES = new HashMap<SiteDescription, Double>();
	static {
		SITE_GOLD_PROBABILITIES.put(SiteDescription.QUARTZ, 0.3);
		SITE_GOLD_PROBABILITIES.put(SiteDescription.GRANITE, 0.1);
		SITE_GOLD_PROBABILITIES.put(SiteDescription.MARBLE, 0.05);
		SITE_GOLD_PROBABILITIES.put(SiteDescription.SAND, 0.01);
		SITE_GOLD_PROBABILITIES.put(SiteDescription.EMPTY, 0.0);
	};

	public static enum Action {
		NORTH, SOUTH, EAST, WEST, EXPLORE_DIG, FULL_DIG
	};

	public static final double MOVE_COST = 10;
	public static final double EXPLORE_DIG_COST = 100;
	public static final double FULL_DIG_COST = 500;
	public static final double FIND_GOLD = 1000;

	public static final double EXPLORE_DIG_SUCCESS_PROB = 0.5;
	public static final double FULL_DIG_SUCCESS_PROB = 0.95;

	/**
	 * A gold mining state is a rich representation of the current state in a
	 * gold mining task.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class GoldMiningState
	{
		private int _x;
		private int _y;
		private int _width;
		private int _height;
		private SiteDescription _desc;

		public GoldMiningState(int x, int y, int width, int height,
				SiteDescription description)
		{
			_x = x;
			_y = y;
			_width = width;
			_height = height;
			_desc = description;
		}

		/**
		 * Get the agent's X-coordinate.
		 * @return the agent's X-coordinate
		 */
		public int getX()
		{
			return _x;
		}

		/**
		 * Get the agent's Y-coordinate.
		 * @return the agent's Y-coordinate
		 */
		public int getY()
		{
			return _y;
		}

		/**
		 * Get a description of the site that the agent is visiting.
		 * @return a description of the site
		 */
		public SiteDescription getDescription()
		{
			return _desc;
		}

		/**
		 * Maps this state into a single integer.
		 * @return an integer representation of this state
		 */
		public int toInteger()
		{
			return toInteger(_x, _y, _width, _height, _desc);
		}
		
		/**
		 * Maps the components that make up a state to a single integer.
		 * @param x the agent's X-coordinate
		 * @param y the agent's Y-coordinate
		 * @param width the width of the entire environment
		 * @param height the height of the entire environment
		 * @param desc a description of the site being visited
		 * @return an integer representation of the state described by the specified components
		 */
		public static final int toInteger(int x, int y, int width, int height, SiteDescription desc)
		{
			return x * height + y;
		}

		/**
		 * Maps this state to a vector.
		 * @return a vector representation of this state
		 */
		public double[] toVector()
		{
			double desc = _desc.ordinal()
					/ (double) SiteDescription.values().length;
			return new double[] { _x / (double) _width, _y / (double) _height,
					desc };
		}
	}

	private SiteDescription[][] _cells;
	private double[][] _rewards;
	private int _width;
	private int _height;
	private Point _agent;
	private boolean _finished;

	/**
	 * The last reward generated.
	 */
	private double _reward;

	/**
	 * Constructs a gold mining task with a specified width, height, and
	 * generation key. The generation key allows many tasks to be created while
	 * at the same time allowing the exact same task to be created at will.
	 * 
	 * @param width
	 *            the width of the task grid
	 * @param height
	 *            the height of the task grid
	 * @param generationKey
	 *            the generation key
	 */
	public GoldMiningTask(int width, int height, int generationKey)
	{
		_cells = new SiteDescription[width][height];
		_rewards = new double[width][height];
		_width = width;
		_height = height;
		_agent = new Point();

		initialize(generationKey);
		reset();
	}

	/**
	 * Initializes the cells with descriptions based on the generation key.
	 * Initializing based on a generation key provides a convenient way to
	 * recreate the same task, but also be able to generate many novel versions
	 * of the same task.
	 * 
	 * @param generationKey
	 *            a generation key that corresponds to a specific pattern of
	 *            descriptions
	 */
	public void initialize(int generationKey)
	{
		java.util.Random rand = new java.util.Random(generationKey);

		double rx = 0.1 * _width;
		double ry = 0.1 * _height;
		double rw = 0.4 * _width;
		double rh = 0.4 * _height;
		Ellipse2D sandEllipse = new Ellipse2D.Double(rx, ry, rw, rh);
		rx = 0.1 * _width;
		ry = 0.5 * _height;
		Ellipse2D marbleEllipse = new Ellipse2D.Double(rx, ry, rw, rh);
		rx = 0.5 * _width;
		ry = 0.1 * _height;
		Ellipse2D graniteEllipse = new Ellipse2D.Double(rx, ry, rw, rh);
		rx = 0.5 * _width;
		ry = 0.5 * _height;
		Ellipse2D quartzEllipse = new Ellipse2D.Double(rx, ry, rw, rh);

		for (int x = 0; x < _width; x++) {
			for (int y = 0; y < _height; y++) {
				// int site = rand.nextInt(numDescriptions);
				// _cells[x][y] = SiteDescription.values()[site];
				_cells[x][y] = SiteDescription.EMPTY;
				Point2D p = new Point2D.Double(x + 0.5, y + 0.5);
				if (quartzEllipse.contains(p)) {
					_cells[x][y] = SiteDescription.QUARTZ;
				} else if (graniteEllipse.contains(p)) {
					_cells[x][y] = SiteDescription.GRANITE;
				} else if (marbleEllipse.contains(p)) {
					_cells[x][y] = SiteDescription.MARBLE;
				} else if (sandEllipse.contains(p)) {
					_cells[x][y] = SiteDescription.SAND;
				}
				double r = rand.nextDouble();
				if (r < SITE_GOLD_PROBABILITIES.get(_cells[x][y])) {
					_rewards[x][y] = FIND_GOLD;
				}
			}
		}
	}

	/**
	 * Decodes an integer state into a rich gold mining state.
	 * 
	 * @param state
	 *            an integer state
	 * @return a gold mining state instance
	 */
	public GoldMiningState decodeIntegerState(Integer state)
	{
		int x = state / _height;
		int y = state % _height;
		SiteDescription desc = _cells[x][y];
		return new GoldMiningState(x, y, _width, _height, desc);
	}

	@Override
	public double evaluate()
	{
		return _reward;
	}

	@Override
	public void execute(Action action)
	{
		double r = 0;
		switch (action) {
		case NORTH:
			if (_agent.y + 1 < _height) {
				_agent.y += 1;
			}
			_reward = -MOVE_COST;
			break;
		case SOUTH:
			if (_agent.y - 1 >= 0) {
				_agent.y -= 1;
			}
			_reward = -MOVE_COST;
			break;
		case EAST:
			if (_agent.x + 1 < _width) {
				_agent.x += 1;
			}
			_reward = -MOVE_COST;
			break;
		case WEST:
			if (_agent.x - 1 >= 0) {
				_agent.x -= 1;
			}
			_reward = -MOVE_COST;
			break;
		case EXPLORE_DIG:
			r = Random.uniform();
			if (r < EXPLORE_DIG_SUCCESS_PROB) {
				_reward = _rewards[_agent.x][_agent.y] - EXPLORE_DIG_COST;
				_finished = true;
			} else {
				_reward = -EXPLORE_DIG_COST;
			}
			break;
		case FULL_DIG:
			r = Random.uniform();
			if (r < FULL_DIG_SUCCESS_PROB) {
				_reward = _rewards[_agent.x][_agent.y] - FULL_DIG_COST;
				_finished = true;
			} else {
				_reward = -FULL_DIG_COST;
			}
			break;
		default:
			throw new DebugException("The specified action is not supported.");
		}
	}

	@Override
	public GoldMiningState getState()
	{
		int x = _agent.x;
		int y = _agent.y;
		GoldMiningState state = new GoldMiningState(x, y, _width, _height,
				_cells[x][y]);
		return state;
	}

	@Override
	public boolean isFinished()
	{
		return _finished;
	}

	@Override
	public void reset()
	{
		int x = Random.nextInt(_width);
		int y = Random.nextInt(_height);
		_agent.setLocation(x, y);
		_finished = false;
		_reward = 0.0;
	}

	private static final Map<SiteDescription, Color> COLOR_MAP = new HashMap<SiteDescription, Color>();
	static {
		COLOR_MAP.put(SiteDescription.EMPTY, Color.WHITE);
		COLOR_MAP.put(SiteDescription.QUARTZ, Color.LIGHT_GRAY);
		COLOR_MAP.put(SiteDescription.GRANITE, Color.GRAY);
		COLOR_MAP.put(SiteDescription.MARBLE, Color.CYAN);
		COLOR_MAP.put(SiteDescription.SAND, Color.YELLOW);
	};

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();
		double tsize = Math.min(width / (double) _width, height
				/ (double) _height);

		// Draw the tiles
		for (int x = 0; x < _width; x++) {
			for (int y = 0; y < _height; y++) {
				double px = x * tsize;
				double py = height - ((y + 1) * tsize);
				Rectangle2D rect = new Rectangle2D.Double(px, py, tsize, tsize);
				g2.setColor(COLOR_MAP.get(_cells[x][y]));
				g2.fill(rect);
				g2.setColor(Color.BLACK);
				g2.draw(rect);
			}
		}

		// Draw the agent
		Ellipse2D agent = new Ellipse2D.Double(_agent.x * tsize, height
				- ((_agent.y + 1) * tsize), tsize, tsize);
		g2.setColor(Color.RED);
		g2.fill(agent);
		g2.setColor(Color.BLACK);
		g2.draw(agent);

		g2.dispose();
	}

	/**
	 * The reward interval for the gold mining task.
	 * 
	 * @return an interval with the minimum and maximum reward for the gold
	 *         mining task
	 */
	public static final Interval rewardInterval()
	{
		return new Interval(-FULL_DIG_COST, FIND_GOLD);
	}

	public static void main(String[] args)
	{
		int randSeed = -13063460;
		int numEpisodes = 2000;
		int episodeLength = 100;
		int width = 25;
		int height = 25;
		int numStates = width * height;
		int numActions = Action.values().length;
		double learningRate = 0.1;
		double discountFactor = 0.9;
		LearningSystem<Integer, Integer> agent = new QLearning(numStates,
				numActions, learningRate, discountFactor);
		// double sensitivity = 0.05;
		// int numVisitsUntilKnown = 5;
		// LearningSystem<Integer, Integer> agent = new DelayedQLearning(
		// numStates, numActions, rewardInterval().getMax(), sensitivity,
		// discountFactor, numVisitsUntilKnown);

		Translator<GoldMiningState, Integer> stateTrans = new Translator<GoldMiningState, Integer>() {

			@Override
			public Integer translate(GoldMiningState action)
			{
				return action.toInteger();
			}

		};

		Translator<Integer, Action> actionTrans = new Translator<Integer, Action>() {
			@Override
			public Action translate(Integer action)
			{
				return Action.values()[action];
			}
		};

		GoldMiningTask gtask = new GoldMiningTask(width, height, randSeed);
		TranslatedTask<Integer, GoldMiningState, Integer, Action> task = new TranslatedTask<Integer, GoldMiningState, Integer, Action>(
				gtask, stateTrans, actionTrans);
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(500, 400);
		SumOfReinforcementsTaskObserver<Integer, Integer, Task<Integer, Integer>> observer = new SumOfReinforcementsTaskObserver<Integer, Integer, Task<Integer, Integer>>();
		Task.runTask(task, agent, numEpisodes, episodeLength, true, observer,
				frame);
		Plot2D.linePlot(observer.getSumOfReinforcementsAtEachEpisode(),
				java.awt.Color.RED, "", "Episode #", "Reward");
	}
}
