package tmrutil.learning.rl.tasks;

import java.util.HashMap;
import java.util.Map;
import tmrutil.learning.rl.tasks.DegradingDeliveryTask.DamageType;
import tmrutil.stats.Random;
import tmrutil.util.Interval;

/**
 * A bandit task where the expected reward degrades with time.
 * @author Timothy A. Mann
 *
 */
public class DegradingBanditTask extends BanditTask<Integer>
{
	public static class DegradingRewardDistribution implements BanditTask.RewardDistribution
	{
		private Interval _rewardInterval;
		private DamageType _dtype;
		private int _maxDamage;
		//private int _timestep;
		private int _tryCount;
		private double _noise;
		
		public DegradingRewardDistribution(Interval rewardInterval, DamageType dtype, int maxDamage, double noise)
		{
			_rewardInterval = rewardInterval;
			_dtype = dtype;
			_maxDamage = maxDamage;
			_noise = noise;
			
			//_timestep = 0;
			_tryCount = 0;
		}

		@Override
		public double sample()
		{
			return expectedValue() + Random.normal(0, _noise);
		}

		@Override
		public double expectedValue()
		{
			switch(_dtype){
			case ABRUPT:
				return (_tryCount > _maxDamage)? _rewardInterval.getMin() : _rewardInterval.getMax();
			case GRADUAL:
				return ((1-(_maxDamage - _tryCount) / (double) _maxDamage)) * _rewardInterval.getDiff() + _rewardInterval.getMin();
			case NONE:
				return _rewardInterval.getMax();
			}
			return 0;
		}

		@Override
		public void update(int timestep, int tryCount)
		{
			//_timestep = timestep;
			_tryCount = tryCount;
		}
		
	}

	public DegradingBanditTask(DamageType dtype)
	{
		super(buildArms(dtype));
		
	}
	
	public static int MAX_DAMAGE = 1000;
	public static double NOISE = 0.2;
	
	public static final Map<Integer,BanditTask.RewardDistribution> buildArms(DamageType dtype)
	{
		Map<Integer,BanditTask.RewardDistribution> arms = new HashMap<Integer,BanditTask.RewardDistribution>();
		
		DegradingRewardDistribution arm0 = new DegradingRewardDistribution(new Interval(0.35, 0.4), dtype, MAX_DAMAGE, NOISE);
		arms.put(0, arm0);
		DegradingRewardDistribution arm1 = new DegradingRewardDistribution(new Interval(0.30, 0.5), dtype, MAX_DAMAGE, NOISE);
		arms.put(1, arm1);
		DegradingRewardDistribution arm2 = new DegradingRewardDistribution(new Interval(0.20, 0.6), dtype, MAX_DAMAGE, NOISE);
		arms.put(2, arm2);
		DegradingRewardDistribution arm3 = new DegradingRewardDistribution(new Interval(0.10, 0.7), dtype, MAX_DAMAGE, NOISE);
		arms.put(3, arm3);
		DegradingRewardDistribution arm4 = new DegradingRewardDistribution(new Interval(0.05, 0.8), dtype, MAX_DAMAGE, NOISE);
		arms.put(4, arm4);
		DegradingRewardDistribution arm5 = new DegradingRewardDistribution(new Interval(0.01, 0.9), dtype, MAX_DAMAGE, NOISE);
		arms.put(5, arm5);
		
		return arms;
	}

}
