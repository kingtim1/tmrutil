package tmrutil.learning.rl.tasks;

import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.TaskObserver;

/**
 * Provides a wrapper for observers when the delayed task wrapper is used.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 * @param <R> the reinforcement type
 */
public final class DelayedTaskObserver<S, A, T extends Task<S,A>>
		implements TaskObserver<S, A, DelayedTask<S,A,T>>
{
	/**
	 * A reference to the underlying task observer.
	 */
	private TaskObserver<S, A, T> _taskObserver;
	
	/**
	 * Constructs a delayed task observer wrapper around another task observer.
	 * @param taskObserver a task observer
	 */
	public DelayedTaskObserver(TaskObserver<S, A, T> taskObserver) throws NullPointerException
	{
		if(taskObserver == null){
			throw new NullPointerException("The underlying task observer cannot be null.");
		}
		_taskObserver = taskObserver;
	}

	@Override
	public void observeEpisodeBegin(DelayedTask<S,A,T> task)
	{
		_taskObserver.observeEpisodeBegin(task.getUndelayedTask());
	}

	@Override
	public void observeEpisodeEnd(DelayedTask<S,A,T> task)
	{
		_taskObserver.observeEpisodeEnd(task.getUndelayedTask());
	}

	@Override
	public void observeStep(DelayedTask<S,A,T> task, S prevState, A action, S newState,
			double reinforcement)
	{
		_taskObserver.observeStep(task.getUndelayedTask(), prevState, action, newState, reinforcement);
	}

	@Override
	public void reset()
	{
		_taskObserver.reset();
	}

	/**
	 * Returns a reference to the underlying task observer.
	 * @return a reference to the underlying task observer
	 */
	public TaskObserver<S,A,T> getUnderlyingTaskObserver()
	{
		return _taskObserver;
	}
}
