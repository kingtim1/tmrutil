package tmrutil.learning.rl.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcessImpl;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.MarkovDecisionProcess;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.util.Interval;

/**
 * Implements the class of GARNET (Generic Average Reward Non-stationary
 * Environment Testbed) tasks described by the technical report Shalabh
 * Bhatnagar, Richard S. Sutton, Mohammad Ghavamzadeh, and Mark Lee.
 * "Natural-Gradient Actor-Critic Algorithms", 2007.
 * 
 * @author Timothy A. Mann
 * 
 */
public class GarnetTask extends DiscreteTask
{
	private class NormalRewardSignal implements
			ReinforcementSignal<Integer, Integer>
	{

		private double[][] _rewardTable;
		private double _rewardStd;

		public NormalRewardSignal(double[][] rewardTable, double rewardStd)
		{
			_rewardTable = rewardTable;
			_rewardStd = rewardStd;
		}

		@Override
		public double evaluate(Integer state, Integer action)
		{
			double rewardMean = _rewardTable[state][action];
			double reward = Random.normal(rewardMean, _rewardStd);
			return reward;
		}

	}

	private int _numStates;
	private int _numActions;
	private int _branchFactor;
	private double _rewardStd;
	private double _nonstationary;

	private double[][][] _tprobs;
	private double[][] _rsignalTable;
	private ReinforcementSignal<Integer, Integer> _rsignal;
	private DiscreteMarkovDecisionProcessImpl<Integer, Integer> _mdp;

	/**
	 * Constructs a GARNET task.
	 * 
	 * @param numStates
	 *            the number of states
	 * @param numActions
	 *            the number of actions
	 * @param branchFactor
	 *            the branch factor determining how stochastic the transition
	 *            probabilities may be
	 * @param rewardStd
	 *            the standard deviation of the reward function (a non-negative
	 *            scalar)
	 * @param nonstationary
	 *            a value in the interval [0, 1/n] where n is the number of
	 *            states determining the probability that some state will be
	 *            modified
	 */
	public GarnetTask(int numStates, int numActions, int branchFactor,
			double rewardStd, double nonstationary)
	{
		super(numStates, numActions, new Interval(-4*rewardStd,4*rewardStd));
		if (numStates < 1) {
			throw new IllegalArgumentException(
					"The number of states must be greater than zero.");
		}
		_numStates = numStates;
		if (numActions < 1) {
			throw new IllegalArgumentException(
					"The number of actions must be greater than zero.");
		}
		_numActions = numActions;

		if (branchFactor < 0 || branchFactor > numStates) {
			throw new IllegalArgumentException(
					"The branching factor must be greater than zero and less than the number of states.");
		}
		_branchFactor = branchFactor;

		if (rewardStd < 0) {
			throw new IllegalArgumentException(
					"The reward standard deviation must be nonnegative.");
		}
		_rewardStd = rewardStd;

		if (nonstationary < 0 || nonstationary > (1.0 / numStates)) {
			throw new IllegalArgumentException(
					"The degree of nonstationarity must be a value in the interval [0, 1/n] where n is the number of states in the MDP.");
		}
		_nonstationary = nonstationary;

		constructMDP(numStates, numActions, branchFactor, rewardStd);
	}

	private void constructMDP(int numStates, int numActions, int branchFactor,
			double rewardStd)
	{
		List<Integer> states = new ArrayList<Integer>(numStates);
		for (int s = 0; s < numStates; s++) {
			states.add(s);
		}
		List<Integer> initStates = states;
		List<Integer> finalStates = new ArrayList<Integer>();
		List<Integer> actions = new ArrayList<Integer>(numActions);
		for (int a = 0; a < numActions; a++) {
			actions.add(a);
		}

		_tprobs = new double[numStates][numActions][numStates];
		for (int s = 0; s < numStates; s++) {
			generateStateTransitionProbabilities(_tprobs, branchFactor, s);
		}

		_rsignalTable = new double[numStates][numActions];
		for (int s = 0; s < numStates; s++) {
			for (int a = 0; a < numActions; a++) {
				_rsignalTable[s][a] = Random.normal(0, rewardStd);
			}
		}
		_rsignal = new NormalRewardSignal(_rsignalTable, rewardStd);
		_mdp = new DiscreteMarkovDecisionProcessImpl<Integer, Integer>(states,
				actions, initStates, finalStates, _tprobs, _rsignal);
	}

	private static final void generateStateTransitionProbabilities(
			double[][][] tprobs, int branchFactor, int state)
	{
		int numStates = tprobs.length;
		for (int a = 0; a < tprobs[state].length; a++) {
			int[] rpermStates = VectorOps.randperm(numStates);

			double[] v = new double[branchFactor];
			for (int bi = 0; bi < branchFactor; bi++) {
				v[bi] = Random.uniform();
			}
			double[] probs = VectorOps.divide(v, VectorOps.sum(v));

			for (int bi = 0; bi < branchFactor; bi++) {
				tprobs[state][a][rpermStates[bi]] = probs[bi];
			}
		}
	}

	@Override
	public Integer getState()
	{
		return _mdp.getState();
	}

	@Override
	public void execute(Integer action)
	{
		_mdp.execute(action);

		double rand = Random.uniform();
		if (rand < (_nonstationary * _numStates)) {
			int state = Random.nextInt(_numStates);
			for (int a = 0; a < _numActions; a++) {
				_rsignalTable[state][a] = Random.normal(0, _rewardStd);

				for (int ns = 0; ns < _numStates; ns++) {
					_tprobs[state][a][ns] = 0;
				}
			}

			generateStateTransitionProbabilities(_tprobs, _branchFactor, state);
		}
	}

	@Override
	public double evaluate()
	{
		return _mdp.evaluate();
	}

	@Override
	public void reset()
	{
		_mdp.reset();
	}

	@Override
	public boolean isFinished()
	{
		return _mdp.isFinished();
	}

}
