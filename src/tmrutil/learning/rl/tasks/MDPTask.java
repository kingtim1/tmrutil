package tmrutil.learning.rl.tasks;

import java.util.HashSet;
import java.util.Set;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcess;
import tmrutil.learning.rl.Task;
import tmrutil.stats.Random;

public class MDPTask extends Task<Integer, Integer> implements DiscreteMarkovDecisionProcess<Integer,Integer>
{
	private int _numActions;
	private int _prevState;
	private int _prevAction;
	private int _currentState;
	private DiscreteMarkovDecisionProcess<Integer,Integer> _mdp;
	
	public MDPTask(DiscreteMarkovDecisionProcess<Integer,Integer> mdp)
	{
		if(mdp == null){
			throw new NullPointerException("Cannot construct an instance of " + getClass() + " with a null MDP.");
		}
		_mdp = mdp;
		
		reset();
	}

	@Override
	public Integer getState()
	{
		return _currentState;
	}

	@Override
	public void execute(Integer action)
	{
		// Update the action counter
		_numActions++;
		
		// Sample a random value in [0, 1]
		double r = Random.uniform();
		
		// Determine which state to transition to
		double rsum = 0;
		for(int nextState=0;nextState<_mdp.numberOfStates();nextState++){
			double tprob = _mdp.transitionProb(_currentState, action, nextState);
			rsum += tprob;
			if(r <= rsum){
				_prevState = _currentState;
				_prevAction = action;
				_currentState = nextState;
				break;
			}
		}
	}

	@Override
	public double evaluate()
	{
		if(_numActions == 0){
			return 0;
		}else{
			return _mdp.reinforcement(_prevState, _prevAction);
		}
	}

	@Override
	public void reset()
	{
		_numActions = 0;
		_prevState = 0;
		_prevAction = 0;
		_currentState = 0;
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}
	
	/**
	 * Returns a reference to the underlying MDP.
	 * @return a reference to the underlying MDP
	 */
	public DiscreteMarkovDecisionProcess<Integer,Integer> getMDP()
	{
		return _mdp;
	}

	@Override
	public double transitionProb(Integer state, Integer action,
			Integer resultState)
	{
		return _mdp.transitionProb(state, action, resultState);
	}

	@Override
	public double reinforcement(Integer state, Integer action)
	{
		return _mdp.reinforcement(state, action);
	}

	@Override
	public double getDiscountFactor()
	{
		return _mdp.getDiscountFactor();
	}

	@Override
	public Set<Integer> successors(Integer state)
	{
		return _mdp.successors(state);
	}

	@Override
	public Set<Integer> predecessors(Integer state)
	{
		return _mdp.predecessors(state);
	}

	@Override
	public int numberOfStates()
	{
		return _mdp.numberOfStates();
	}

	@Override
	public int numberOfActions()
	{
		return _mdp.numberOfActions();
	}

	@Override
	public double rmax()
	{
		return _mdp.rmax();
	}

	@Override
	public double rmin()
	{
		return _mdp.rmin();
	}

	@Override
	public boolean isKnown(Integer state, Integer action)
	{
		return true;
	}

	@Override
	public Set<Integer> states() {
		Set<Integer> states = new HashSet<Integer>();
		for(int s=0;s<numberOfStates();s++){
			states.add(s);
		}
		return states;
	}

	@Override
	public Set<Integer> actions(Integer state) {
		Set<Integer> actions = new HashSet<Integer>();
		for(int a=0;a<numberOfActions();a++){
			actions.add(a);
		}
		return actions;
	}

}
