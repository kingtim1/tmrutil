package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DelayedQLearning;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.RMax;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.ram.RAMNextStateFunction;
import tmrutil.learning.rl.ram.RAMTask;
import tmrutil.learning.rl.ram.RAMTypeFunction;
import tmrutil.math.MatrixOps;
import tmrutil.stats.Random;
import tmrutil.util.Interval;

/**
 * Implements the reset task. The reset task is a Markov decision process with a
 * sequence of states and two actions. One of the two actions moves the agent
 * forward in the sequence of states and the other action will send the agent
 * back to the initial state. The agent only receives a reward upon reaching the
 * final state in the sequence.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ResetTask extends RAMTask implements Drawable
{
	public static final int DEFAULT_NUM_STATES = 10;
	public static final int DEFAULT_NUM_ACTIONS = 5;
	public static final double DEFAULT_SUCCESS_PROB = 0.8;

	/** The probability of successfully executing an action. */
	private double _successProb;

	/** The current state. */
	private int _currentState;
	
	/** The last executed action. */
	private int _lastAction;
	/** The previous state. */
	private int _lastState;

	/**
	 * Contains the action that progresses the agent forward at each state.
	 */
	private int[] _progressAction;

	/**
	 * Constructs an instance of the reset task with a specified number of
	 * states and a probability of an action being successful.
	 * 
	 * @param numStates
	 *            the number of states
	 * @param numActions
	 *            the number of actions
	 * @param successProb
	 *            the probability of an action being executed successfully
	 */
	public ResetTask(int numStates, int numActions, int numProgressActionCandidates, double successProb)
	{
		super(numStates, numActions, new Interval(0, 1));

		if (successProb <= 0 || successProb > 1) {
			throw new IllegalArgumentException(
					"The probability of executing an action successfully must be in the interval (0, 1].");
		}
		_successProb = successProb;

		_currentState = 0;
		_lastAction = 0;
		_lastState = 0;

		_progressAction = new int[numStates];
		for (int s = 0; s < numStates; s++) {
			_progressAction[s] = Random.nextInt(numProgressActionCandidates);
		}

		reset();
	}

	/**
	 * Constructs an instance of the reset task with specified number of states
	 * and actions. The default success probability
	 * {@link ResetTask.DEFAULT_SUCCESS_PROB} is used.
	 * 
	 * @param numStates
	 *            the number of states
	 * @param numActions
	 *            the number of actions
	 */
	public ResetTask(int numStates, int numActions)
	{
		this(numStates, numActions, numActions, DEFAULT_SUCCESS_PROB);
	}

	/**
	 * Constructs an instance of the reset task with the specified number of
	 * states. The default number of actions
	 * {@link ResetTask.DEFAULT_NUM_ACTIONS
	 * } and the default success probability
	 * {@link ResetTask.DEFAULT_SUCCESS_PROB} are used.
	 * 
	 * @param numStates
	 *            the number of states
	 */
	public ResetTask(int numStates)
	{
		this(numStates, DEFAULT_NUM_ACTIONS, DEFAULT_NUM_ACTIONS, DEFAULT_SUCCESS_PROB);
	}

	/**
	 * Constructs an instance of the reset task with all default values.
	 */
	public ResetTask()
	{
		this(DEFAULT_NUM_STATES, DEFAULT_NUM_ACTIONS, DEFAULT_NUM_ACTIONS, DEFAULT_SUCCESS_PROB);
	}

	@Override
	public double evaluate()
	{
		if (_currentState == (numberOfStates() - 1)) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public void execute(Integer action)
	{
		int pAction = _progressAction[_currentState];
		if (action == pAction) {
			double r = Random.uniform();
			if (r <= _successProb) {
				if (_currentState < (numberOfStates() - 1)) {
					_currentState++;
				} else {
					_currentState = 0;
				}
			}
		} else {
			_currentState = 0;
		}
		_lastAction = action;
	}

	@Override
	public Integer getState()
	{
		return _currentState;
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public void reset()
	{
		_currentState = 0;
		_lastAction = 0;
		_lastState = 0;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();

		double swidth = width / (double) numberOfStates();

		for (int i = 0; i < numberOfStates(); i++) {
			Rectangle2D srect = new Rectangle2D.Double(i * swidth, 0, swidth,
					swidth);

			if (i == _currentState) {
				g2.setColor(Color.RED);
			} else if (i == 0) {
				g2.setColor(Color.BLUE);
			} else if (i == (numberOfStates() - 1)) {
				g2.setColor(Color.GREEN);
			} else {
				g2.setColor(Color.LIGHT_GRAY);
			}
			g2.fill(srect);

			g2.setColor(Color.BLACK);
			g2.draw(srect);
		}

		g2.dispose();
	}

	public static void main(String[] args)
	{
		double discountFactor = 0.95;
		Interval rewardInterval = new Interval(0, 1);
		int numVisitsBeforeKnown = 5;

		int numEpisodes = 10000;
		int episodeLength = 100;

		ResetTask task = new ResetTask(20, 5, 5, 0.8);

		double sensitivity = 0.01;
		//DelayedQLearning agent = new DelayedQLearning(task.numberOfStates(), task.numberOfActions(), rewardInterval.getMax(), sensitivity, discountFactor, numVisitsBeforeKnown);
		RMax agent = new RMax(task.numberOfStates(), task.numberOfActions(),
				discountFactor, rewardInterval, numVisitsBeforeKnown);

		Task.runTask(task, agent, numEpisodes, episodeLength, true, null, null,
				true);

		MatrixOps.displayGraphically(agent.getQ(), "Q-Values");
		//MatrixOps.displayGraphically(agent.knownMatrix(), "Known Matrix");

		javax.swing.JFrame frame = new javax.swing.JFrame("Reset Task");
		frame.setSize(400, 100);
		Task.runTask(task, agent, numEpisodes, episodeLength, true, null,
				frame, false);
	}

	@Override
	public RAMTypeFunction<Integer> typeFunction()
	{
		return new RAMTypeFunction<Integer>(){

			public static final int START_STATE = 0;
			public static final int OTHER_STATE = 1;
			public static final int GOAL_STATE = 2;
			
			@Override
			public int classify(Integer state)
			{
				if(state == 0){
					return START_STATE;
				}else if(state == numberOfStates()-1){
					return GOAL_STATE;
				}else{
					return OTHER_STATE;
				}
			}

			@Override
			public int numberOfClasses()
			{
				return 3;
			}

			@Override
			public String classToString(Integer ramClass)
			{
				return ramClass.toString();
			}
			
		};
	}

	@Override
	public RAMNextStateFunction<Integer, Integer> nextStateFunction()
	{
		return new RAMNextStateFunction<Integer,Integer>()
		{
			public static final int NULL_OUTCOME = 0;
			public static final int FORWARD_OUTCOME = 1;
			public static final int RESET_OUTCOME = 2;
			
			public static final int NUM_OUTCOMES = 3;

			@Override
			public Integer nextState(Integer state, Integer outcome)
			{
				if(outcome == NULL_OUTCOME){
					return state;
				}else if(outcome == RESET_OUTCOME){
					return 0;
				}else{
					return state+1;
				}
			}

			@Override
			public Integer outcomeMap(Integer state, Integer nextState)
			{
				if(state == nextState){
					return NULL_OUTCOME;
				}else if(state+1 == nextState){
					return FORWARD_OUTCOME;
				}else{
					return RESET_OUTCOME;
				}
			}

			@Override
			public String outcomeToString(Integer outcome)
			{
				return outcome.toString();
			}

			@Override
			public Integer nullOutcome()
			{
				return NULL_OUTCOME;
			}

			@Override
			public int numberOfOutcomes()
			{
				return NUM_OUTCOMES;
			}
			
		};
	}

	@Override
	public ReinforcementSignal<Integer, Integer> reinforcementSignal()
	{
		
		return new ReinforcementSignal<Integer,Integer>(){

			@Override
			public double evaluate(Integer state, Integer action)
			{
				if(state == (numberOfStates()-1)){
					return 1;
				}else{
					return 0;
				}
			}
			
		};
	}

}
