package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import tmrutil.graphics.Drawable;
import tmrutil.graphics.DrawableComponent;
import tmrutil.stats.Random;
import tmrutil.util.Interval;

/**
 * An infinite armed bandit problem with a Lipschitz constant. The reward
 * distribution of each arm is a Bernoulli random variable. So the expected
 * rewards are bound to the interval [0, 1].
 * 
 * @author Timothy A. Mann
 * 
 */
public class LipschitzInfiniteArmedBanditTask extends
		InfiniteArmBanditTask<Double> implements Drawable
{
	private static final Interval REWARD_INTERVAL = new Interval(0, 1);

	private Interval _armSpace;
	private List<Double> _nodes;
	private int _numNodes;
	private Double _lipschitz = null;

	/**
	 * Constructs a Lipschitz infinite armed bandit task over a one-dimensional
	 * arm space. The expected rewards are given by a list of nodes, which are
	 * evenly spaced throughout the arm space and interpolated between linearly.
	 * 
	 * @param armSpace
	 *            the interval of valid actions
	 * @param nodes
	 *            a list of expected reward nodes where each value is in the
	 *            range [0, 1]
	 */
	public LipschitzInfiniteArmedBanditTask(Interval armSpace,
			List<Double> nodes)
	{
		_armSpace = armSpace;
		_nodes = new ArrayList<Double>(nodes);
		_numNodes = nodes.size();
	}

	@Override
	public double sampleReward(Double action)
	{
		double rand = Random.uniform();
		if (rand <= meanReward(action)) {
			return REWARD_INTERVAL.getMax();
		} else {
			return REWARD_INTERVAL.getMin();
		}
	}

	/**
	 * Returns the minimal Lipschitz constant for the expected reward function
	 * induced by actions.
	 * 
	 * @return the minimal Lipschitz constant
	 */
	public double lipschitzConstant()
	{
		if (_lipschitz == null) {
			double ndist = nodeDistance();
			double maxChange = 0;
			for (int i = 1; i < _numNodes; i++) {
				double change = Math.abs(_nodes.get(i) - _nodes.get(i - 1));
				if (maxChange < change) {
					maxChange = change;
				}
			}
			_lipschitz = maxChange / ndist;
		}
		return _lipschitz.doubleValue();
	}

	@Override
	public double meanReward(Double action)
	{
		double ndist = nodeDistance();
		double div = ((action - _armSpace.getMin()) / ndist);
		int lowerNode = (int) Math.floor(div);
		if (lowerNode <= div) {
			return _nodes.get(lowerNode);
		} else {
			int upperNode = lowerNode + 1;
			double firstVal = _nodes.get(lowerNode);
			double secondVal = _nodes.get(upperNode);

			double distBetween = (div - lowerNode);
			return firstVal + (distBetween * (secondVal - firstVal));
		}
	}

	/**
	 * The distance between nodes.
	 * 
	 * @return the distance between nodes
	 */
	private double nodeDistance()
	{
		return _armSpace.getDiff() / _numNodes;
	}

	@Override
	public Interval rewardInterval()
	{
		return REWARD_INTERVAL;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		
		double apxHeight = 0.95 * height;
		double heightBuff = (height - apxHeight) / 2;
		
		g2.setColor(Color.BLACK);
		Line2D oneLine = new Line2D.Double(0, heightBuff, width, heightBuff);
		g2.draw(oneLine);
		Line2D zeroLine = new Line2D.Double(0, apxHeight + heightBuff, width, apxHeight + heightBuff);
		g2.draw(zeroLine);
		
		g2.setColor(Color.BLUE);
		double ndist = width / (_numNodes-1);
		for(int i=1;i<_numNodes;i++){
			double x1 = (i-1) * ndist;
			double y1 = apxHeight * (1 - _nodes.get(i-1)) + heightBuff;
			double x2 = i * ndist;
			double y2 = apxHeight * (1 - _nodes.get(i)) + heightBuff;
			Line2D line = new Line2D.Double(x1, y1, x2, y2);
			g2.draw(line);
		}
		
		Double lastAction = lastAction();
		double xwidth = 0.05 * Math.min(width, height);
		double ywidth = 0.05 * Math.min(width, height);
		if(lastAction != null)
		{
			g2.setColor(Color.RED);
			double x = width * ((_armSpace.getDiff() - (lastAction.doubleValue() - _armSpace.getMin())) / _armSpace.getDiff()) - xwidth/2;
			double y = apxHeight * (REWARD_INTERVAL.getMax() - evaluate()) - ywidth/2 + heightBuff;
			Ellipse2D action = new Ellipse2D.Double(x, y, xwidth, ywidth);
			g2.fill(action);
		}
		
		g2.dispose();
	}
	
	public static void main(String[] args)
	{
		Interval rewardInterval = new Interval(0.05, 0.95);
		final Interval armSpace = new Interval(0, 10);
		int numNodes = 50;
		double walkVal = 0.1;
		List<Double> nodes = new ArrayList<Double>();
		double prev = Random.uniform();
		for(int i=0;i<numNodes;i++){
			double nval = rewardInterval.clip(prev + Random.uniform(-walkVal, walkVal));
			nodes.add(nval);
			prev = nval;
		}
		
		final LipschitzInfiniteArmedBanditTask task = new LipschitzInfiniteArmedBanditTask(armSpace, nodes);
		task.execute(armSpace.random());
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 200);
		final DrawableComponent dc = new DrawableComponent(task);
		frame.add(dc);
		frame.setVisible(true);
		
		System.out.println("Lipschitz Constant : " + task.lipschitzConstant());
		
		dc.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e)
			{
				task.execute(armSpace.random());
				dc.repaint();
			}

			@Override
			public void mousePressed(MouseEvent e)
			{
			}

			@Override
			public void mouseReleased(MouseEvent e)
			{
			}

			@Override
			public void mouseEntered(MouseEvent e)
			{
			}

			@Override
			public void mouseExited(MouseEvent e)
			{		
			}
			
		});
	}

}
