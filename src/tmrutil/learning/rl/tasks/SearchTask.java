package tmrutil.learning.rl.tasks;

import tmrutil.learning.rl.DiscreteActionTask;
import tmrutil.learning.rl.factored.FactoredState;

public class SearchTask extends DiscreteActionTask<FactoredState>
{
	public static final int LEFT_ACTION = 0, RIGHT_ACTION = 1,
			COMPARE_ACTION = 2, LOWER_HALF_ACTION = 3, UPPER_HALF_ACTION = 4,
			RESET_ACTION = 5;
	public static final int NUM_ACTIONS = 6;

	private int _cursor;
	private int[] _environment;
	
	public SearchTask()
	{
		super(NUM_ACTIONS);
	}

	@Override
	public FactoredState getState()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void execute(Integer action)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public double evaluate()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void reset()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isFinished()
	{
		// TODO Auto-generated method stub
		return false;
	}

}
