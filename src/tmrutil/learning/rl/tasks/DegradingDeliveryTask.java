package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.AbstractDiscreteTemporalDifferenceLearning;
import tmrutil.learning.rl.DelayedQLearning;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.QLearning;
import tmrutil.learning.rl.RMax;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.ram.RAMNextStateFunction;
import tmrutil.learning.rl.ram.RAMRMax;
import tmrutil.learning.rl.ram.RAMTask;
import tmrutil.learning.rl.ram.RAMTypeFunction;
import tmrutil.math.MatrixOps;
import tmrutil.stats.Random;
import tmrutil.util.ArrayOps;
import tmrutil.util.Interval;

/**
 * A simulation of an autonomous delivery robot with a degrading suspension. The
 * robot's task is to pick up and deliver raw materials from a pickup location
 * to a drop off location. However, the raw materials are fragile and can be
 * damaged by driving over rough terrain. This problem only becomes worse over
 * time as the robot's suspension becomes damaged.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DegradingDeliveryTask extends RAMTask implements Drawable
{
	private double FORWARD_PROB = 0.95; //0.8;
	private double LEFT_PROB = 0.025; //0.1;
	private double RIGHT_PROB = 0.025; //0.1;

	private int ROCKY_DAMAGE = 2;
	private int ROUGH_DAMAGE = 1;
	private int SMOOTH_DAMAGE = 0;

	private static final int NUM_ROWS = 10;
	private static final int NUM_COLS = 10;

	private static final int[] MAX_STATE_VALUES = { NUM_COLS, NUM_ROWS, 2 };

	private static final int NUM_STATES = 2 * NUM_ROWS * NUM_COLS;

	private static final int NORTH_ACTION = 0;
	private static final int SOUTH_ACTION = 1;
	private static final int EAST_ACTION = 2;
	private static final int WEST_ACTION = 3;
	private static final int PICKUP_ACTION = 4;
	private static final int DROP_ACTION = 5;
	private static final int NUM_ACTIONS = 6;

	private static final Interval REWARD_INTERVAL = new Interval(0, 1);

	public static enum DamageType {
		NONE, GRADUAL, ABRUPT
	};

	private static final Interval SMOOTH_RUIN_PROB = new Interval(0, 0.01);
	private static final Interval ROUGH_RUIN_PROB = new Interval(0, 0.25);
	private static final Interval ROCKY_RUIN_PROB = new Interval(0, 0.5);

	private static final int MAX_DAMAGE = 10000;

	private static enum Terrain {
		SOURCE, TARGET, SMOOTH, ROUGH, ROCKY
	};

	/**
	 * Represents the amount of damage to the vehicles suspension.
	 */
	private int _damageCount;

	/**
	 * Represents the agent's location.
	 */
	private Point _agentLocation;

	/**
	 * True if the agent is holding raw materials; otherwise the agent is not
	 * holding anything.
	 */
	private boolean _holdingMaterials;

	/**
	 * True if the agent just dropped off raw materials at the target location.
	 */
	private boolean _receivedReward;

	/**
	 * A map of the environment's terrain.
	 */
	private Terrain[][] _map;

	/**
	 * The type of damage that occurs to the vehicle.
	 */
	private DamageType _dtype;

	public DegradingDeliveryTask(DamageType dtype)
	{
		super(NUM_STATES, NUM_ACTIONS, REWARD_INTERVAL);

		_dtype = dtype;

		_agentLocation = new Point();

		_map = new Terrain[NUM_ROWS][NUM_COLS];
		for (int r = 0; r < NUM_ROWS; r++) {
			for (int c = 0; c < NUM_COLS; c++) {
				_map[r][c] = Terrain.SMOOTH;
			}
		}
		setupMap();

		reset();
	}

	private void setupMap()
	{
		_map[0][0] = Terrain.SOURCE;
		_map[NUM_ROWS - 1][NUM_COLS - 1] = Terrain.TARGET;

		_map[1][3] = Terrain.ROUGH;
		_map[1][7] = Terrain.ROUGH;
		_map[2][4] = Terrain.ROUGH;
		_map[3][6] = Terrain.ROUGH;
		_map[3][8] = Terrain.ROUGH;
		_map[4][2] = Terrain.ROUGH;
		_map[4][5] = Terrain.ROUGH;
		_map[4][7] = Terrain.ROUGH;
		_map[5][1] = Terrain.ROUGH;
		_map[6][5] = Terrain.ROUGH;
		_map[6][8] = Terrain.ROUGH;
		_map[7][4] = Terrain.ROUGH;
		_map[7][7] = Terrain.ROUGH;
		_map[7][9] = Terrain.ROUGH;
		_map[8][5] = Terrain.ROUGH;
		_map[9][0] = Terrain.ROUGH;
		_map[9][1] = Terrain.ROUGH;

		_map[2][7] = Terrain.ROCKY;
		_map[3][7] = Terrain.ROCKY;
		_map[4][3] = Terrain.ROCKY;
		_map[4][4] = Terrain.ROCKY;
		_map[5][3] = Terrain.ROCKY;
		_map[5][5] = Terrain.ROCKY;
		_map[5][6] = Terrain.ROCKY;
		_map[5][7] = Terrain.ROCKY;
		_map[6][4] = Terrain.ROCKY;
		_map[6][7] = Terrain.ROCKY;
		_map[7][8] = Terrain.ROCKY;
	}

	@Override
	public Integer getState()
	{
		int[] aState = { _agentLocation.x, _agentLocation.y,
				_holdingMaterials ? 1 : 0 };
		return ArrayOps.encode(aState, MAX_STATE_VALUES);
	}

	@Override
	public void execute(Integer action)
	{
		_receivedReward = false;

		Terrain t = _map[_agentLocation.y][_agentLocation.x];
		if (_holdingMaterials) {
			double r = Random.uniform();
			if (r < cargoRuinProbability(t)) {
				_holdingMaterials = false;
			}
		}

		switch (action) {
		case NORTH_ACTION:
			moveNorth();
			break;
		case SOUTH_ACTION:
			moveSouth();
			break;
		case EAST_ACTION:
			moveEast();
			break;
		case WEST_ACTION:
			moveWest();
			break;
		case PICKUP_ACTION:
			if (!_holdingMaterials && t.equals(Terrain.SOURCE)) {
				_holdingMaterials = true;
			}
			break;
		case DROP_ACTION:
			if (_holdingMaterials && t.equals(Terrain.TARGET)) {
				_receivedReward = true;
			}
			_holdingMaterials = false;
			break;
		}

		updateSuspensionDamage(_map[_agentLocation.y][_agentLocation.x]);
	}

	private boolean inMap(int x, int y)
	{
		return (x >= 0 && x < NUM_COLS && y >= 0 && y < NUM_ROWS);
	}

	private void moveNorth()
	{
		double r = Random.uniform();
		int y = _agentLocation.y - 1;
		int x = _agentLocation.x;
		if (r > FORWARD_PROB) {
			if (r < (FORWARD_PROB + LEFT_PROB)) {
				x--;
			} else {
				x++;
			}
		}

		if (inMap(x, y)) {
			_agentLocation.setLocation(x, y);
		}
	}

	private void moveSouth()
	{
		double r = Random.uniform();
		int y = _agentLocation.y + 1;
		int x = _agentLocation.x;
		if (r > FORWARD_PROB) {
			if (r < (FORWARD_PROB + LEFT_PROB)) {
				x++;
			} else {
				x--;
			}
		}

		if (inMap(x, y)) {
			_agentLocation.setLocation(x, y);
		}
	}

	private void moveEast()
	{
		double r = Random.uniform();
		int y = _agentLocation.y;
		int x = _agentLocation.x + 1;
		if (r > FORWARD_PROB) {
			if (r < (FORWARD_PROB + LEFT_PROB)) {
				y--;
			} else {
				y++;
			}
		}

		if (inMap(x, y)) {
			_agentLocation.setLocation(x, y);
		}
	}

	private void moveWest()
	{
		double r = Random.uniform();
		int y = _agentLocation.y;
		int x = _agentLocation.x - 1;
		if (r > FORWARD_PROB) {
			if (r < (FORWARD_PROB + LEFT_PROB)) {
				y++;
			} else {
				y--;
			}
		}

		if (inMap(x, y)) {
			_agentLocation.setLocation(x, y);
		}
	}

	public void drawValue(AbstractDiscreteTemporalDifferenceLearning agent)
	{
		double[][] V = new double[NUM_ROWS][2 * NUM_COLS];
		int[] maxVals = { NUM_COLS, NUM_ROWS, 2 };
		for (int x = 0; x < NUM_COLS; x++) {
			for (int y = 0; y < NUM_ROWS; y++) {
				for (int h = 0; h < 2; h++) {
					int[] vals = { x, y, h };
					int state = ArrayOps.encode(vals, maxVals);
					int vx = x;
					if (h == 1) {
						vx = vx + NUM_COLS;
					}
					V[y][vx] = agent.maxQ(state);
				}
			}
		}
		MatrixOps.displayGraphically(V, "Values");
	}

	@Override
	public double evaluate()
	{
		if (_receivedReward) {
			return rmax();
		} else {
			return rmin();
		}
	}

	private double cargoRuinProbability(Terrain t)
	{
		double d = Math.max(1, _damageCount / MAX_DAMAGE);
		if (_dtype.equals(DamageType.GRADUAL)) {
			switch (t) {
			case ROCKY:
				return ROCKY_RUIN_PROB.getMin() + d * ROCKY_RUIN_PROB.getDiff();
			case ROUGH:
				return ROUGH_RUIN_PROB.getMin() + d * ROUGH_RUIN_PROB.getDiff();
			case SMOOTH:
				return SMOOTH_RUIN_PROB.getMin() + d
						* SMOOTH_RUIN_PROB.getDiff();
			default:
				return 0;
			}
		}

		if (_dtype.equals(DamageType.ABRUPT)) {
			boolean isDamaged = (_damageCount >= MAX_DAMAGE);
			switch (t) {
			case ROCKY:
				if (isDamaged) {
					return ROCKY_RUIN_PROB.getMax();
				} else {
					return ROCKY_RUIN_PROB.getMin();
				}
			case ROUGH:
				if (isDamaged) {
					return ROUGH_RUIN_PROB.getMax();
				} else {
					return ROUGH_RUIN_PROB.getMin();
				}
			case SMOOTH:
				if (isDamaged) {
					return SMOOTH_RUIN_PROB.getMax();
				} else {
					return SMOOTH_RUIN_PROB.getMin();
				}
			default:
				return 0;
			}
		}

		if (_dtype.equals(DamageType.NONE)) {
			switch (t) {
			case ROCKY:
				return ROCKY_RUIN_PROB.getMin();
			case ROUGH:
				return ROUGH_RUIN_PROB.getMin();
			case SMOOTH:
				return SMOOTH_RUIN_PROB.getMin();
			default:
				return 0;
			}
		}

		return 0;
	}

	private void updateSuspensionDamage(Terrain t)
	{
		switch (t) {
		case ROCKY:
			_damageCount += ROCKY_DAMAGE;
			break;
		case ROUGH:
			_damageCount += ROUGH_DAMAGE;
			break;
		case SMOOTH:
			_damageCount += SMOOTH_DAMAGE;
			break;
		default:
			// No damage
		}
	}

	@Override
	public void reset()
	{
		_damageCount = 0;

		_holdingMaterials = false;
		_receivedReward = false;

		int ax = Random.nextInt(NUM_COLS);
		int ay = Random.nextInt(NUM_ROWS);
		_agentLocation.setLocation(ax, ay);
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	public static final int[][] RAM_TYPES = {
			{ 0, 12, 12, 12, 12, 12, 12, 12, 12, 9 },
			{ 11, 4, 4, 2, 4, 4, 4, 2, 4, 8 },
			{ 11, 4, 4, 4, 2, 4, 4, 3, 4, 8 },
			{ 11, 4, 4, 4, 4, 4, 2, 3, 2, 8 },
			{ 11, 4, 2, 3, 3, 2, 4, 2, 4, 8 },
			{ 11, 2, 4, 3, 4, 3, 3, 3, 4, 8 },
			{ 11, 4, 4, 4, 3, 2, 4, 3, 2, 8 },
			{ 11, 4, 4, 4, 2, 4, 4, 2, 3, 5 },
			{ 11, 4, 4, 4, 4, 2, 4, 4, 4, 8 },
			{ 7, 6, 10, 10, 10, 10, 10, 10, 10, 1 } };

	@Override
	public RAMTypeFunction<Integer> typeFunction()
	{
		RAMTypeFunction<Integer> tfunc = new RAMTypeFunction<Integer>() {

			@Override
			public int classify(Integer state)
			{
				int[] values = ArrayOps.decode(state, MAX_STATE_VALUES);
				int row = values[0];
				int col = values[1];
				if(values[2] == 0){
					return RAM_TYPES[row][col];
				}else{
					return RAM_TYPES[row][col] + 13;
				}
			}

			@Override
			public int numberOfClasses()
			{
				return 2 * 13;
			}

			@Override
			public String classToString(Integer ramClass)
			{
				return ramClass.toString();
			}

		};
		return tfunc;
	}

	@Override
	public RAMNextStateFunction<Integer, Integer> nextStateFunction()
	{
		RAMNextStateFunction<Integer, Integer> nsfunc = new RAMNextStateFunction<Integer, Integer>() {

			public final int[] MAX_OUTCOME_VALUES = { 3, 3, 2 };

			@Override
			public Integer nextState(Integer state, Integer outcome)
			{
				int[] decodedState = ArrayOps.decode(state, MAX_STATE_VALUES);
				int[] decodedOutcome = ArrayOps.decode(outcome,
						MAX_OUTCOME_VALUES);

				int x = decodedState[0] + (decodedOutcome[0] - 1);
				if (x < 0) {
					x = 0;
				}
				if (x >= NUM_COLS) {
					x = NUM_COLS - 1;
				}
				int y = decodedState[1] + (decodedOutcome[1] - 1);
				if (y < 0) {
					y = 0;
				}
				if (y >= NUM_ROWS) {
					y = NUM_ROWS - 1;
				}
				boolean holdingMaterials = false;
				if (decodedOutcome[2] == 0) {
					holdingMaterials = (decodedState[2] == 1);
				} else {
					holdingMaterials = (decodedState[2] != 1);
				}
				int[] newState = { x, y, holdingMaterials ? 1 : 0 };

				return ArrayOps.encode(newState, MAX_STATE_VALUES);
			}

			@Override
			public int numberOfOutcomes()
			{
				return 18;
			}

			@Override
			public Integer outcomeMap(Integer state, Integer nextState)
			{

				int[] sArray = ArrayOps.decode(state, MAX_STATE_VALUES);
				int[] nsArray = ArrayOps.decode(nextState, MAX_STATE_VALUES);

				int[] outcomeArray = { (nsArray[0] - sArray[0]) + 1,
						(nsArray[1] - sArray[1]) + 1,
						(sArray[2] != nsArray[2]) ? 1 : 0 };

				return ArrayOps.encode(outcomeArray, MAX_OUTCOME_VALUES);
			}

			@Override
			public Integer nullOutcome()
			{
				int[] outcomeArray = { 1, 1, 0 };
				return ArrayOps.encode(outcomeArray, MAX_OUTCOME_VALUES);
			}

			@Override
			public String outcomeToString(Integer outcome)
			{
				return outcome.toString();
			}

		};
		return nsfunc;
	}

	@Override
	public ReinforcementSignal<Integer, Integer> reinforcementSignal()
	{
		ReinforcementSignal<Integer, Integer> rsignal = new ReinforcementSignal<Integer, Integer>() {

			@Override
			public double evaluate(Integer state, Integer action)
			{
				int[] value = { NUM_COLS - 1, NUM_ROWS - 1, 1 };
				int deliveryState = ArrayOps.encode(value, MAX_STATE_VALUES);
				if (state == deliveryState && action == DROP_ACTION) {
					return rmax();
				} else {
					return rmin();
				}
			}

		};
		return rsignal;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();

		int twidth = Math.min(width, height) / Math.max(NUM_ROWS, NUM_COLS);

		for (int r = 0; r < NUM_ROWS; r++) {
			for (int c = 0; c < NUM_COLS; c++) {
				Rectangle2D tile = new Rectangle2D.Double(c * twidth, r
						* twidth, twidth, twidth);
				Terrain t = _map[r][c];
				switch (t) {
				case SMOOTH:
					g2.setColor(Color.WHITE);
					break;
				case ROUGH:
					g2.setColor(Color.LIGHT_GRAY);
					break;
				case ROCKY:
					g2.setColor(Color.GRAY);
					break;
				case SOURCE:
					g2.setColor(Color.BLUE);
					break;
				case TARGET:
					g2.setColor(Color.GREEN);
					break;
				}
				g2.fill(tile);
				g2.setColor(Color.BLACK);
				g2.draw(tile);
			}
		}

		Ellipse2D agent = new Ellipse2D.Double(_agentLocation.x * twidth,
				_agentLocation.y * twidth, twidth, twidth);
		if (_holdingMaterials) {
			g2.setColor(Color.YELLOW);
		} else {
			g2.setColor(Color.RED);
		}
		g2.fill(agent);
		g2.setColor(Color.BLACK);
		g2.draw(agent);

		g2.dispose();
	}

	public static void main(String[] args)
	{
		DegradingDeliveryTask task = new DegradingDeliveryTask(DamageType.GRADUAL);
		// double learningRate = 0.05;
		// double epsilon = 0.01;
		double discountFactor = 0.95;
		double sensitivity = 0.01;
		int numVisitsUntilKnown = 10;
		// QLearning agent = new QLearning(task.numberOfStates(),
		// task.numberOfActions(), learningRate, discountFactor);
		// agent.setEpsilon(epsilon);
		// DelayedQLearning agent = new DelayedQLearning(task.numberOfStates(),
		// task.numberOfActions(), task.rmax(), sensitivity,
		// discountFactor, numVisitsUntilKnown);
		// RMax agent = new RMax(task.numberOfStates(), task.numberOfActions(),
		// discountFactor, new Interval(task.rmin(), task.rmax()),
		// numVisitsUntilKnown, true);

		RAMRMax agent = new RAMRMax(task.numberOfStates(),
				task.numberOfActions(), task.typeFunction(),
				task.nextStateFunction(), task.reinforcementSignal(),
				new Interval(task.rmin(), task.rmax()), discountFactor,
				numVisitsUntilKnown, true);

		Task.runTask(task, agent, 1, 10000, true, null, null, true);

		task.drawValue(agent);
		//agent.plotKnown();
		// MatrixOps.displayGraphically(new double[][]{agent.knownStates()},
		// "Known States");

		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		Task.runTask(task, agent, 1, 100000, true, null, frame);
	}

}
