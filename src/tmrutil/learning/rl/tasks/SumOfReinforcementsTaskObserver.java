package tmrutil.learning.rl.tasks;

import java.util.ArrayList;
import java.util.List;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.TaskObserver;
import tmrutil.stats.Statistics;

/**
 * A simple task observer for collecting the sum of reinforcements received by a learning system.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 * @param <T> the task type to observer
 */
public class SumOfReinforcementsTaskObserver<S, A, T extends Task<S, A>> implements
		TaskObserver<S, A, T>
{
	/**
	 * A list of the sum of reinforcements received at each episode.
	 */
	private List<Double> _sumOfReinforcements;
	/**
	 * A counter for the sum of reinforcements received during the current episode.
	 */
	private double _episodeSum;
	
	/**
	 * Constructs a task observer for recording the sum of reinforcements received by a learning system.
	 */
	public SumOfReinforcementsTaskObserver()
	{
		_sumOfReinforcements = new ArrayList<Double>();
		_episodeSum = 0;
	}
	
	@Override
	public void observeEpisodeBegin(T task)
	{
		_episodeSum = 0;
	}

	@Override
	public void observeEpisodeEnd(T task)
	{
		_sumOfReinforcements.add(_episodeSum);
	}

	@Override
	public void observeStep(T task, S prevState, A action, S newState,
			double reinforcement)
	{
		_episodeSum += reinforcement;
	}

	@Override
	public void reset()
	{
		_sumOfReinforcements.clear();
		_episodeSum = 0;
	}
	
	/**
	 * Returns the sum of reinforcements collected at each episode.
	 * @return a list of sums of reinforcements
	 */
	public List<Double> getSumOfReinforcementsAtEachEpisode()
	{
		return new ArrayList<Double>(_sumOfReinforcements);
	}

	/**
	 * Returns the average sum of reinforcements collected at each episode.
	 * @return mean sum of reinforcements
	 */
	public Double getSumOfReinforcements()
	{
		return Statistics.mean(_sumOfReinforcements);
	}
}
