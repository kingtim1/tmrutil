package tmrutil.learning.rl.tasks;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.Task;
import tmrutil.stats.Random;

/**
 * An implementation of the cart-pole balancing task. A vertically aligned pole
 * is connected to the top of a cart by a hinge joint. Given a slight initial
 * perturbation the objective is to apply forces to the cart to keep the pole
 * balanced (the angle of the pole does not exceed + or - some threshold angle).
 * 
 * The unit of length used is meters. Mass is measured by kilograms. Time is
 * measured in seconds. Force is applied in Newtons.
 * 
 * @author Timothy A. Mann
 * 
 */
public class CartPoleBalancingTask extends Task<double[], double[]>
		implements Drawable
{
	/** The gravitational constant in meters/second^2. */
	public static final double G = -9.81;
	/** The default integration time step in seconds. */
	public static final double DEFAULT_TIME_DELTA = 0.02;
	/** The default mass of the cart in kilograms. */
	public static final double DEFAULT_CART_MASS = 1.0;
	/** The default mass of the pole in kilograms. */
	public static final double DEFAULT_POLE_MASS = 0.1;
	/** The default pole length in meters. */
	public static final double DEFAULT_POLE_LENGTH = 1.0;
	/** The default track length in meters. */
	public static final double DEFAULT_TRACK_LENGTH = 4.8;
	/** The default failure angle for the pole in radians. */
	public static final double DEFAULT_FAIL_ANGLE = Math.toRadians(12 /* degrees */);

	/** The current time. */
	private double _time;
	/** The integration time step. */
	private double _timeDelta;

	/** The failure angle for the pole. */
	private double _failAngle;

	/** The angle of the pole. */
	private double _poleAngle;
	/** The velocity of the pole. */
	private double _poleVel;
	/** The acceleration of the pole. */
	private double _poleAccel;
	/**
	 * The distance from the top of the cart to the pole's center of mass. This
	 * is equal to half of the pole's actual length.
	 */
	private double _poleCenterOfMass;
	/** The mass of the pole. */
	private double _poleMass;

	/** The position of the cart. */
	private double _cartPos;
	/** The velocity of the cart. */
	private double _cartVel;
	/** The acceleration of the cart. */
	private double _cartAccel;
	/** The mass of the cart. */
	private double _cartMass;

	/** Force applied to the center of the cart. */
	private double _cartForce;

	/** Half of the length of the entire cart's track in meters. */
	private double _halfTrackLength;

	/** Set to true if the agent controlling the task failed. */
	private boolean _hasFailed;

	/**
	 * Constructs an instance of the cart-pole balancing problem.
	 * 
	 * @param cartMass
	 *            the mass of the cart in kilograms
	 * @param poleMass
	 *            the mass of the pole in kilograms
	 * @param poleLength
	 *            the length of the pole in meters
	 * @param trackLength
	 *            the length of the track in meters
	 * @param failAngle
	 *            the failure angle for the pole in radians
	 * @param timeDelta
	 *            the integration time step in seconds
	 */
	public CartPoleBalancingTask(double cartMass, double poleMass,
			double poleLength, double trackLength, double failAngle,
			double timeDelta)
	{
		setCartMass(cartMass);
		setPoleMass(poleMass);
		setPoleLength(poleLength);
		setTrackLength(trackLength);
		setFailAngle(failAngle);
		setTimeStep(timeDelta);

		reset();
	}

	/**
	 * Constructs an instance of the cart-pole balancing problem with default
	 * settings.
	 */
	public CartPoleBalancingTask()
	{
		this(DEFAULT_CART_MASS, DEFAULT_POLE_MASS, DEFAULT_POLE_LENGTH,
				DEFAULT_TRACK_LENGTH, DEFAULT_FAIL_ANGLE, DEFAULT_TIME_DELTA);
	}

	@Override
	public double evaluate()
	{
		if (Math.abs(_poleAngle) < _failAngle) {
			return _poleAngle * _poleAngle;
		} else {
			return 1.0;
		}
	}

	@Override
	public void execute(double[] action)
	{
		_cartForce = 10 * action[0];
		if(_cartPos >= _halfTrackLength && _cartForce > 0){
			_cartForce = 0;
		}
		if(_cartPos <= -_halfTrackLength && _cartForce < 0){
			_cartForce = 0;
		}
		step(getTimeStep());
		_cartForce = 0;
		
		//System.out.println("Pole Vel : " + _poleVel);
		//System.out.println("Cart Vel : " + _cartVel);
	}

	/**
	 * Steps the simulation forward by a specified time constant.
	 * 
	 * @param timeStep
	 *            a time step
	 */
	private void step(double timeStep)
	{
		double t = getTime();
		double h = timeStep;

		_cartAccel = cartAccelUpdate(t, _cartPos, _cartVel);
		_poleAccel = poleAccelUpdate(t, _poleAngle, _poleVel);

		_cartPos = _cartPos + h * _cartVel;
		_poleAngle = _poleAngle + h * _poleVel;

		_cartVel = _cartVel + h * _cartAccel;
		_poleVel = _poleVel + h * _poleAccel;

		if (Math.abs(_cartPos) > _halfTrackLength) {
			_cartAccel = 0;
			_cartVel = 0;
			_cartPos = Math.signum(_cartPos) * _halfTrackLength;
		}

		if (Math.abs(_poleAngle) > Math.PI / 2) {
			_poleAccel = 0;
			_poleVel = 0;
			_poleAngle = Math.signum(_poleAngle) * (Math.PI / 2);
		}

		if (isInFailureState() && !hasFailed()) {
			_hasFailed = true;
		}

		_time += timeStep;
	}

	private double cartAccelUpdate(double t, double cartPos, double cartVelocity)
	{
		double sinPoleAngle = Math.sin(_poleAngle);
		double cosPoleAngle = Math.cos(_poleAngle);
		double num = _cartForce
				+ _poleMass
				* _poleCenterOfMass
				* (Math.pow(_poleVel, 2) * sinPoleAngle - _poleAccel
						* cosPoleAngle);
		double den = _cartMass + _poleMass;

		return num / den;
	}

	private double poleAccelUpdate(double t, double poleAngle,
			double poleVelocity)
	{
		double sinPoleAngle = Math.sin(poleAngle);
		double cosPoleAngle = Math.cos(poleAngle);
		double cartAndPoleMass = _cartMass + _poleMass;

		double num = -G
				* sinPoleAngle
				+ cosPoleAngle
				* ((-_cartForce - (_poleMass * _poleCenterOfMass
						* Math.pow(poleVelocity, 2) * sinPoleAngle)) / cartAndPoleMass);
		double den = _poleCenterOfMass
				* ((4.0 / 3.0) - (_poleMass * Math.pow(cosPoleAngle, 2))
						/ cartAndPoleMass);

		return num / den;
	}

	@Override
	public double[] getState()
	{
		return new double[] { _cartPos, _cartVel, _poleAngle, _poleVel };
	}

	@Override
	public void reset()
	{
		_time = 0;

		_poleAngle = Random.normal(0, getFailAngle() / 4);
		_poleVel = 0;
		_poleAccel = 0;

		_cartPos = 0;
		_cartVel = 0;
		_cartAccel = 0;

		_cartForce = 0;

		_hasFailed = false;
	}

	/**
	 * Set the length of the pole in meters. The length of the pole must be
	 * positive.
	 * 
	 * @param length
	 *            the length of the pole in meters
	 */
	public void setPoleLength(double length)
	{
		if (length <= 0) {
			throw new IllegalArgumentException(
					"The pole length must be positive.");
		}
		_poleCenterOfMass = length / 2;
	}

	/**
	 * Returns the length of the pole in meters.
	 * 
	 * @return the length of the pole
	 */
	public double getPoleLength()
	{
		return 2 * _poleCenterOfMass;
	}

	/**
	 * Sets the mass of the cart in kilograms.
	 * 
	 * @param mass
	 *            the mass of the cart
	 */
	public void setCartMass(double mass)
	{
		if (mass <= 0) {
			throw new IllegalArgumentException("Mass must be positive.");
		}
		_cartMass = mass;
	}

	/**
	 * Returns the mass of the cart in kilograms.
	 * 
	 * @return the mass of the cart
	 */
	public double getCartMass()
	{
		return _cartMass;
	}

	/**
	 * Sets the mass of the pole in kilograms.
	 * 
	 * @param mass
	 *            the mass of the pole
	 */
	public void setPoleMass(double mass)
	{
		if (mass <= 0) {
			throw new IllegalArgumentException("Mass must be positive.");
		}
		_poleMass = mass;
	}

	/**
	 * Returns the mass of the pole in kilograms.
	 * 
	 * @return the mass of the pole
	 */
	public double getPoleMass()
	{
		return _poleMass;
	}

	/**
	 * Sets the cart track length in meters.
	 * 
	 * @param length
	 *            the length of the cart track
	 */
	public void setTrackLength(double length)
	{
		if (length <= 0) {
			throw new IllegalArgumentException(
					"The track length must be positive.");
		}
		_halfTrackLength = length / 2;
	}

	/**
	 * Returns the cart track length in meters.
	 * 
	 * @return the cart track length
	 */
	public double getTrackLength()
	{
		return 2 * _halfTrackLength;
	}

	/**
	 * Sets the failure angle (in radians) for the pole. If the angle of the
	 * pole reaches this angle then the controller has failed to balance the
	 * pole.
	 * 
	 * @param angle
	 *            the failure angle in radians
	 */
	public void setFailAngle(double angle)
	{
		if (angle <= 0) {
			throw new IllegalArgumentException(
					"The angle of failure must be positive.");
		}
		_failAngle = angle;
	}

	/**
	 * Returns the failure angle (in radians) for the pole. If the angle of the
	 * pole reaches this angle then the controller has failed to balance the
	 * pole.
	 * 
	 * @return the failure angle in radians
	 */
	public double getFailAngle()
	{
		return _failAngle;
	}

	/**
	 * Sets the integration time step length in seconds. The value must be
	 * positive.
	 * 
	 * @param timeDelta
	 *            the integration time step length
	 */
	public void setTimeStep(double timeDelta)
	{
		if (timeDelta <= 0) {
			throw new IllegalArgumentException(
					"The integration time step length must be positive.");
		}
		_timeDelta = timeDelta;
	}

	/**
	 * Returns the integration time step length in seconds.
	 * 
	 * @return the integration time step length
	 */
	public double getTimeStep()
	{
		return _timeDelta;
	}

	/**
	 * Returns the simulation time in seconds.
	 * 
	 * @return time in seconds
	 */
	public double getTime()
	{
		return _time;
	}

	/**
	 * Returns true if the pole angle is beyond the failure angle; otherwise
	 * false is returned.
	 * 
	 * @return true if the pole angle is beyond the failure angle; otherwise
	 *         false
	 */
	public boolean isInFailureState()
	{
		return Math.abs(_poleAngle) >= getFailAngle();
	}

	/**
	 * Returns true if the pole angle has ever gone beyond the failure angle;
	 * otherwise false is returned.
	 * 
	 * @return true if the pole angle has ever gone beyond the failure angle;
	 *         otherwise false
	 */
	public boolean hasFailed()
	{
		return _hasFailed;
	}
	
	@Override
	public boolean isFinished()
	{
		return hasFailed();
	}

	@Override
	public void draw(Graphics2D g2, int width, int height)
	{
		double pixelsPerMeter = width / getTrackLength();
		AffineTransform xform = new AffineTransform();
		xform.translate(width / 2, 0.9 * height);
		xform.scale(pixelsPerMeter, -pixelsPerMeter);
		g2.transform(xform);

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		BasicStroke solidStroke = new BasicStroke((float) (1 / pixelsPerMeter));
		BasicStroke thickStroke = new BasicStroke((float) (2 / pixelsPerMeter));
		BasicStroke dashedStroke = new BasicStroke(
				(float) (1 / pixelsPerMeter), BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_MITER, 1.0f, new float[] {
						(float) (0.1 / pixelsPerMeter),
						(float) (0.1 / pixelsPerMeter) }, 0.0f);

		double cartHeight = 0.1 * getPoleLength();
		double cartWidth = 2 * cartHeight;

		g2.setStroke(solidStroke);
		g2.setColor(Color.BLACK);

		// Draw the track
		Line2D track = new Line2D.Double(-getTrackLength() / 2, 0,
				getTrackLength() / 2, 0);
		g2.draw(track);

		g2.setStroke(dashedStroke);
		g2.setColor(Color.GRAY);

		// Draw the failure angles
		Line2D failLeft = new Line2D.Double(_cartPos, cartHeight, _cartPos
				+ getPoleLength() * Math.sin(-getFailAngle()), getPoleLength()
				* Math.cos(-getFailAngle()) + cartHeight);
		Line2D failRight = new Line2D.Double(_cartPos, cartHeight, _cartPos
				+ getPoleLength() * Math.sin(getFailAngle()), getPoleLength()
				* Math.cos(getFailAngle()) + cartHeight);
		g2.draw(failLeft);
		g2.draw(failRight);

		// Draw the pole
		g2.setStroke(thickStroke);
		g2.setColor(Color.BLACK);
		Line2D pole = new Line2D.Double(_cartPos, cartHeight, _cartPos
				+ getPoleLength() * Math.sin(_poleAngle), getPoleLength()
				* Math.cos(_poleAngle) + cartHeight);
		g2.draw(pole);

		// Draw the cart
		g2.setColor(Color.BLUE);
		Rectangle2D cart = new Rectangle2D.Double(_cartPos - (cartWidth / 2),
				0, cartWidth, cartHeight);
		g2.fill(cart);
	}
}
