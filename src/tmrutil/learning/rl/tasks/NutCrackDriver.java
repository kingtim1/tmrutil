package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.Task;
import tmrutil.math.VectorOps;
import tmrutil.util.Interval;

public class NutCrackDriver implements Drawable
{
	public static final Interval REWARD_INTERVAL = new Interval(0, 1);
	public static final int NUMBER_OF_ACTIONS = 4;
	
	private static final int MOVE_LEFT = 0;
	private static final int MOVE_RIGHT = 1;
	private static final int PICKUP = 2;
	private static final int DROP = 3;
	
	private static final int X = 0;
	private static final int Y = 1;
	private static final int NUT_INDEX = 0;
	private static final int ROCK_INDEX = 1;
	private static final int AGENT_INDEX = 2;
	
	/** The state of the environment. */
	private int[][] _state;
	private int[][] _prevState;
	private boolean _nutCracked;
	private int _size;

	public NutCrackDriver(int size)
	{
		_size = size;
		reset();
	}
	
	private void deepCopyState()
	{
		_prevState[NUT_INDEX][X] = _state[NUT_INDEX][X];
		_prevState[NUT_INDEX][Y] = _state[NUT_INDEX][Y];
		_prevState[ROCK_INDEX][X] = _state[ROCK_INDEX][X];
		_prevState[ROCK_INDEX][Y] = _state[ROCK_INDEX][Y];
		_prevState[AGENT_INDEX][X] = _state[AGENT_INDEX][X];
		_prevState[AGENT_INDEX][Y] = _state[AGENT_INDEX][Y];
	}
	
	public void reset()
	{
		_nutCracked = false;
		int[] rperm = VectorOps.randperm(_size);
		_state = new int[3][2];
		_state[NUT_INDEX][X] = rperm[0];
		_state[NUT_INDEX][Y] = 0;
		_state[ROCK_INDEX][X] = rperm[1];
		_state[ROCK_INDEX][Y] = 0;
		_state[AGENT_INDEX][X] = rperm[2];
		_state[AGENT_INDEX][Y] = 1;
		_prevState = new int[3][2];
	}
	
	public boolean isNutCracked()
	{
		return _nutCracked;
	}
	
	public Point whereIsNut()
	{
		return new Point(_state[NUT_INDEX][X], _state[NUT_INDEX][Y]);
	}
	
	public Point whereIsRock(){
		return new Point(_state[ROCK_INDEX][X], _state[ROCK_INDEX][Y]);
	}
	
	public Point whereIsAgent(){
		return new Point(_state[AGENT_INDEX][X], _state[AGENT_INDEX][Y]);
	}

	public boolean isAgentHoldingNut()
	{
		return (_state[AGENT_INDEX][X] == _state[NUT_INDEX][X] && _state[AGENT_INDEX][Y] == _state[NUT_INDEX][Y]);
	}

	public boolean isAgentHoldingRock()
	{
		return (_state[AGENT_INDEX][X] == _state[ROCK_INDEX][X] && _state[AGENT_INDEX][Y] == _state[ROCK_INDEX][Y]);
	}

	public void execute(Integer action)
	{

		//
		// Move Left
		//
		if (action == MOVE_LEFT) {
			boolean holdingNut = isAgentHoldingNut();
			boolean holdingRock = isAgentHoldingRock();
			_state[AGENT_INDEX][X] -= 1;
			if (_state[AGENT_INDEX][X] < 0) {
				_state[AGENT_INDEX][X] = 0;
			}
			if (holdingNut) {
				_state[NUT_INDEX][X] = _state[AGENT_INDEX][X];
			}
			if (holdingRock) {
				_state[ROCK_INDEX][X] = _state[AGENT_INDEX][X];
			}
		}

		//
		// Move Right
		//
		if (action == MOVE_RIGHT) {
			boolean holdingNut = isAgentHoldingNut();
			boolean holdingRock = isAgentHoldingRock();
			_state[AGENT_INDEX][X] += 1;
			if (_state[AGENT_INDEX][X] > _size - 1) {
				_state[AGENT_INDEX][X] = _size - 1;
			}
			if (holdingNut) {
				_state[NUT_INDEX][X] = _state[AGENT_INDEX][X];
			}
			if (holdingRock) {
				_state[ROCK_INDEX][X] = _state[AGENT_INDEX][X];
			}
		}

		//
		// Pickup
		//
		if (action == PICKUP && !isAgentHoldingNut()
				&& !isAgentHoldingRock()) {
			if (_state[AGENT_INDEX][X] == _state[NUT_INDEX][X]) {
				_state[NUT_INDEX][Y] = 1;
			} else if (_state[AGENT_INDEX][X] == _state[ROCK_INDEX][X]) {
				_state[ROCK_INDEX][Y] = 1;
			}
		}

		//
		// Drop
		//
		if (action == DROP) {
			if (_state[NUT_INDEX][Y] > 0) {
				_state[NUT_INDEX][Y] = 0;
			}
			if (_state[ROCK_INDEX][Y] > 0) {
				_state[ROCK_INDEX][Y] = 0;
				if (_state[ROCK_INDEX][X] == _state[NUT_INDEX][X]) {
					_nutCracked = true;
				}
			}
		}
		deepCopyState();
	}
	
	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Color backupColor = g.getColor();
		Color agentColor = Color.RED;
		Color rockColor = Color.DARK_GRAY;
		Color nutColor = Color.ORANGE;

		int swidth = width / _size;
		int sheight = height / 4;

		int x = _state[NUT_INDEX][X] * swidth;
		int y = (_state[NUT_INDEX][Y] > 0) ? sheight : (3 * sheight);
		g.setColor(nutColor);
		g.drawRect(x, y, swidth - 1, sheight - 1);

		x = _state[ROCK_INDEX][X] * swidth;
		y = (_state[ROCK_INDEX][Y] > 0) ? sheight : (3 * sheight);
		g.setColor(rockColor);
		g.drawRect(x, y, swidth - 1, sheight - 1);

		x = _state[AGENT_INDEX][X] * swidth;
		y = (_state[AGENT_INDEX][Y] > 0) ? 0 : (3 * sheight);
		g.setColor(agentColor);
		g.drawRect(x, y, swidth - 1, sheight - 1);

		g.setColor(backupColor);
	}

	public double evaluate()
	{
		if(_nutCracked){
			return REWARD_INTERVAL.getMax();
		}else{
			return REWARD_INTERVAL.getMin();
		}
	}
	
	public static final Interval rewardInterval()
	{
		return REWARD_INTERVAL;
	}

	public boolean isFinished()
	{
		return _nutCracked;
	}
	
	/**
	 * Return the number of valid actions for a nut cracking task.
	 * @return a number of actions
	 */
	public int getNumberOfActions()
	{
		return NUMBER_OF_ACTIONS;
	}

	/**
	 * Returns a collection of valid actions.
	 * @return a collection of valid actions
	 */
	public static Collection<Integer> generateActions()
	{
		List<Integer> actions = new ArrayList<Integer>();
		actions.add(MOVE_LEFT);
		actions.add(MOVE_RIGHT);
		actions.add(PICKUP);
		actions.add(DROP);
		
		return actions;
	}
}
