package tmrutil.learning.rl;

import tmrutil.optim.Optimization;
import tmrutil.util.DataStore;

/**
 * An implementation of the Delayed Q-learning algorithm.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DelayedQLearning extends
		AbstractDiscreteTemporalDifferenceLearning implements Explorer<Integer,Integer>
{
	private static final double DEFAULT_LEARNING_RATE = 0.0;

	/**
	 * The number of times each state-action pair must be visited before it is
	 * considered "known".
	 */
	private int _visitsUntilKnown;

	/**
	 * The time of the most recent state-action value change.
	 */
	private long _mostRecentUpdate;

	/**
	 * The maximum possible single timestep reward.
	 */
	private double _rmax;

	/**
	 * The sensitivity threshold.
	 */
	private double _sensitivity;

	/**
	 * Accumulators for updates on each state-action pair.
	 */
	private double[][] _AU;
	/**
	 * Counters for each state-action pair.
	 */
	private long[][] _l;
	/**
	 * Beginning timestep for an attempted update for each state-action pair.
	 */
	private long[][] _b;

	/**
	 * Flags used to determine whether a state-action pair is eligible for
	 * updates.
	 */
	private boolean[][] _learn;

	/**
	 * Constructs an instance of delayed Q-learning.
	 * 
	 * @param numStates
	 *            the number of states
	 * @param numActions
	 *            the number of actions
	 * @param rmax
	 *            the maximum possible immediate reward
	 * @param sensitivity
	 *            the sensitivity threshold used to determine when to stop
	 *            exploring
	 * @param discountFactor
	 *            the discount factor
	 * @param numVisitsUntilKnown
	 *            the number of visits required before a state-action pair
	 *            becomes known
	 */
	public DelayedQLearning(int numStates, int numActions, double rmax,
			double sensitivity, double discountFactor, int numVisitsUntilKnown)
	{
		super(numStates, numActions, DEFAULT_LEARNING_RATE, discountFactor, Optimization.MAXIMIZE);
		_rmax = rmax;
		_sensitivity = sensitivity;
		_visitsUntilKnown = numVisitsUntilKnown;

		_AU = new double[numberOfStates()][numberOfActions()];
		_l = new long[numberOfStates()][numberOfActions()];
		_b = new long[numberOfStates()][numberOfActions()];
		_learn = new boolean[numberOfStates()][numberOfActions()];

		reset();
	}

	@Override
	protected void resetImpl()
	{
		_mostRecentUpdate = 0;
		double vmax = rmax() / (1 - getDiscountFactor());
		for (int s = 0; s < numberOfStates(); s++) {
			for (int a = 0; a < numberOfActions(); a++) {
				setQ(s, a, vmax);
				_AU[s][a] = 0;
				_l[s][a] = 0;
				_b[s][a] = 0;
				_learn[s][a] = true;
			}
		}
	}

	@Override
	protected void trainImpl(Integer prevState, Integer action,
			Integer newState, double reinforcement)
	{
		if (_b[prevState][action] <= _mostRecentUpdate) {
			_learn[prevState][action] = true;
		}
		if (_learn[prevState][action]) {
			if (_l[prevState][action] == 0) {
				_b[prevState][action] = getTime();
			}
			_l[prevState][action] += 1;
			_AU[prevState][action] += reinforcement + getDiscountFactor()
					* maxQ(newState);
			if (_l[prevState][action] == _visitsUntilKnown) {
				double update = _AU[prevState][action] / _visitsUntilKnown;
				double error = evaluateQ(prevState, action) - update;
				if (error >= (2 * _sensitivity)) {
					setQ(prevState, action, update + _sensitivity);
					_mostRecentUpdate = getTime();
				} else if (_b[prevState][action] > _mostRecentUpdate) {
					_learn[prevState][action] = false;
				}
				_AU[prevState][action] = 0;
				_l[prevState][action] = 0;
			}
		}
	}

	/**
	 * Returns the maximum possible immediate (one step) reward.
	 * 
	 * @return the maximum reward
	 */
	public double rmax()
	{
		return _rmax;
	}

	@Override
	public boolean isExploration(Integer state, Integer action)
	{
		return _learn[state][action];
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<Integer> greedyValueFunction() {
		return new QFunction.GreedyVFunction<Integer>(this);
	}

	@Override
	public double value(Integer state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(Integer state) {
		return maxQ(state);
	}
}
