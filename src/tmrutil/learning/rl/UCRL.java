package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.List;

import tmrutil.util.DataStore;
import tmrutil.util.FileDataStore;
import tmrutil.util.Interval;

/**
 * An implementation of the UCRL2 algorithm introduced and analyzed in:
 * 
 * <p>
 * Thomas Jaksch, Ronald Ortner, and Peter Auer. Near-Optimal Regret Bounds for
 * Reinforcement Learning. <em>Journal of Machine Learning Research</em> 11
 * (2010) 1563--1600.
 * </p>
 * 
 * @author Timothy A. Mann
 * 
 */
public class UCRL implements LearningSystem<Integer, Integer> {

	/**
	 * The number of states in the MDP.
	 */
	private int _numStates;

	/**
	 * The number of actions in the MDP.
	 */
	private int _numActions;

	/**
	 * The horizon time of the algorithm.
	 */
	private int _T;

	/**
	 * The parameter controlling the probability that the confidence intervals
	 * will hold.
	 */
	private double _delta;

	/**
	 * Scales the confidence intervals.
	 */
	private double _alpha;

	/**
	 * The interval containing the minimum and maximum possible immediate
	 * rewards.
	 */
	private Interval _rewardInterval;

	/**
	 * Reference to a file data store instance for logging experiment data.
	 */
	private FileDataStore _dstore;
	/**
	 * Tag used to prefix all data bound to the data storage object.
	 */
	private static final String DSTORE_TAG = "ucrl_";

	/*
	 * =========================================================
	 * 
	 * Main algorithm parameters.
	 * 
	 * ========================================================
	 */

	/**
	 * The current timestep.
	 */
	private int _time;

	/**
	 * Counts the number of times that each state-action pair has been visited.
	 * The counts are updated at the end of each episode.
	 */
	private int[][] _totalVisitCounts;

	/**
	 * Counts the number of times that a state-action pair transitions to a
	 * particular state.
	 */
	private int[][][] _nextStateCounts;

	/**
	 * Cumulates the reward received at each state-action pair.
	 */
	private double[][] _reward;

	/**
	 * Optimistic estimate of the optimal state-action value function.
	 */
	private ExtendedValueIteration.QFunction _qplus;
	
	private List<Integer> _realTrajectory;
	private List<Double> _rewardHistory;

	/*
	 * =======================================================
	 * 
	 * Episode specific parameters.
	 * 
	 * ======================================================
	 */

	/**
	 * Keeps track of the number of the current episode.
	 */
	private int _episodeNumber;
	/**
	 * The timestep that the current episode started on.
	 */
	private int _episodeStartTime;

	/**
	 * Counts the number of times each state-action pair has been visited during
	 * the current episode.
	 */
	private int[][] _episodeVisitCounts;

	/**
	 * Keeps track of the cumulative reward received during each episode. This
	 * is not used by the algorithm, but is included for logging purposes.
	 */
	private double _episodeCumulativeReward;

	/*
	 * =====================================================
	 * 
	 * Methods
	 * 
	 * =====================================================
	 */

	/**
	 * Constructs a new instance of the UCRL2 algorithm.
	 * 
	 * @param numStates
	 *            the number of states in the MDP
	 * @param numActions
	 *            the number of actions in the MDP
	 * @param delta
	 *            a parameter in (0,1] controlling the confidence interval sizes
	 *            (i.e., how quickly learning occurs)
	 * @param alpha
	 *            a parameter that scales the confidence intervals (this is
	 *            needed because the theoretically derived confidence intervals
	 *            are too large). Typical values for this parameter are in the
	 *            interval (0, 1).
	 * @param T
	 *            the total number of timesteps to run the algorithm for
	 * @param rewardInterval
	 *            an interval containing the minimum and maximum possible
	 *            immediate rewards
	 * @param dstore
	 *            a reference to a data storage instance that stores details
	 *            about the algorithms execution
	 */
	public UCRL(int numStates, int numActions, double delta, double alpha,
			int T, Interval rewardInterval, FileDataStore dstore) {
		_numStates = numStates;
		_numActions = numActions;
		_delta = delta;
		_alpha = alpha;
		_T = T;
		_rewardInterval = rewardInterval;
		_dstore = dstore;

		init();
	}

	@Override
	public Integer policy(Integer state) {
		if (_qplus == null) {
			return 0;
		} else {
			return _qplus.policy(_T - _time, state);
		}
	}

	@Override
	public void train(Integer prevState, Integer action, Integer newState,
			double reinforcement) {
		_episodeCumulativeReward += reinforcement;
		_episodeVisitCounts[prevState][action]++;

		_reward[prevState][action] += reinforcement;
		_nextStateCounts[prevState][action][newState]++;

		if(_realTrajectory.isEmpty()){
			_realTrajectory.add(prevState);
		}
		
		_realTrajectory.add(newState);
		_rewardHistory.add(reinforcement);
		
		_time++;

		if (stopEpisode(prevState, action)) {
			initNewEpisode();
		}
		
		if(_dstore != null){
			_dstore.bind(DSTORE_TAG + "real_trajectory", _realTrajectory);
			_dstore.bind(DSTORE_TAG + "reward_history", _rewardHistory);
		}
		
	}

	/**
	 * Returns true if the current episode should be terminated. Otherwise false
	 * is returned indicating that the current episode should continue.
	 * 
	 * @return true if the current episode should terminate; otherwise false
	 */
	public boolean stopEpisode(Integer state, Integer action) {
		int n = Math.max(1, totalVisitCounts(state, action));
		int en = episodeVisitCounts(state, action);
		if (en > 2 * n) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the average reward for a state-action pair.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return average reward for <code>(state, action)</code>
	 */
	public double reward(Integer state, Integer action) {
		int n = totalVisitCounts(state, action);
		if (n == 0) {
			return _rewardInterval.getMax();
		} else {
			return _reward[state][action] / n;
		}
	}

	public double rewardUncertainty(Integer state, Integer action) {
		/*double ci1 = Math.max(
				0,
				Math.log(2. * _numStates * _numActions * _episodeStartTime
						/ _delta));
		double ci2 = Math.sqrt(ci1);

		double dr = Math.sqrt(3.5 / totalVisitCounts(state, action)) * ci2;

		return Math.min(_rewardInterval.getDiff(), _alpha * dr);*/
		
		double ci1 = Math.log(2. / _delta); // Math.log(2. * _numStates * _numActions / _delta);
		double d = 2. * totalVisitCounts(state, action);
		double u = Math.min(_rewardInterval.getDiff(), _alpha * Math.sqrt(ci1 / d));
		return u;
	}

	/**
	 * Returns the state transition probability for a state-action pair and
	 * resulting state.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param resultState
	 *            another state
	 * @return the probability of executing <code>(state, action)</code> and
	 *         transitioning to <code>resultState</code>
	 */
	public double transitionProbability(Integer state, Integer action,
			Integer resultState) {
		int n = totalVisitCounts(state, action);
		if (n == 0) {
			if (state.equals(resultState)) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return _nextStateCounts[state][action][resultState] / (double) n;
		}
	}

	public double transitionProbabilityUncertainty(Integer state, Integer action) {
		/*double ci1 = Math.max(
				0,
				Math.log(2. * _numStates * _numActions * _episodeStartTime
						/ _delta));
		double ci2 = Math.sqrt(ci1);

		double dp = Math.sqrt(14. * _numStates
				/ totalVisitCounts(state, action))
				* ci2;
		return Math.min(_alpha * dp, 1);*/
		
		double ci1 = Math.log(2. / _delta); //Math.max(0, Math.log(Math.pow(2., _numStates+1) * _numStates * _numActions / _delta));
		double d = 2. * totalVisitCounts(state, action);
		double u = Math.min(_alpha * Math.sqrt(ci1 / d), 1);
		return u;
	}

	/**
	 * Returns the number of times that a specified state-action pair has been
	 * visited throughout this agent's lifetime not including the current
	 * episode.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the total number of visits to <code>(state, aciton)</code>
	 */
	public int totalVisitCounts(Integer state, Integer action) {
		return _totalVisitCounts[state][action];
	}

	/**
	 * Returns the number of times that a specified state-action pair has been
	 * visited during the current episode.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the number of times that <code>(state, action)</code> has been
	 *         visited during the current episode
	 */
	public int episodeVisitCounts(Integer state, Integer action) {
		return _episodeVisitCounts[state][action];
	}

	protected void init() {
		if (_dstore != null) {
			_dstore.bind(DSTORE_TAG + "num_states", _numStates);
			_dstore.bind(DSTORE_TAG + "num_actions", _numActions);
			_dstore.bind(DSTORE_TAG + "delta", _delta);
			_dstore.bind(DSTORE_TAG + "confidence_interval_scaling", _alpha);
			_dstore.bind(DSTORE_TAG + "reward_min", _rewardInterval.getMin());
			_dstore.bind(DSTORE_TAG + "reward_max", _rewardInterval.getMax());
			_dstore.bind(DSTORE_TAG + "T", _T);
		}

		_time = 1;
		_totalVisitCounts = new int[_numStates][_numActions];
		_episodeVisitCounts = new int[_numStates][_numActions];
		_nextStateCounts = new int[_numStates][_numActions][_numStates];
		_reward = new double[_numStates][_numActions];
		
		_rewardHistory = new ArrayList<Double>();
		_realTrajectory = new ArrayList<Integer>();

		initNewEpisode();
	}

	protected void initNewEpisode() {
		_episodeNumber++;
		_episodeStartTime = _time;

		if (_dstore != null) {
			_dstore.appendRow(DSTORE_TAG + "episode_start_time",
					new int[] { _episodeStartTime });
			_dstore.appendRow(DSTORE_TAG
					+ "cumulative_reward_at_episode_termination",
					new double[] { _episodeCumulativeReward });
			_dstore.bind(DSTORE_TAG + "num_episodes", _episodeNumber);
		}

		_episodeCumulativeReward = 0;

		double[][] r = new double[_numStates][_numActions];
		double[][] rd = new double[_numStates][_numActions];

		double[][][] p = new double[_numStates][_numActions][_numStates];
		double[][] pd = new double[_numStates][_numActions];

		for (int s = 0; s < _numStates; s++) {
			for (int a = 0; a < _numActions; a++) {
				_totalVisitCounts[s][a] += _episodeVisitCounts[s][a];
				_episodeVisitCounts[s][a] = 0;

				r[s][a] = reward(s, a);
				rd[s][a] = rewardUncertainty(s, a);
				for (int ns = 0; ns < _numStates; ns++) {
					p[s][a][ns] = transitionProbability(s, a, ns);
				}
				pd[s][a] = transitionProbabilityUncertainty(s, a);
			}
		}

		if (_dstore != null) {
			/*
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber
					+ "_total_visit_counts", _totalVisitCounts);
			
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber
					+ "_reward_uncertainty", rd);
			_dstore.bind(DSTORE_TAG + "episode_" + _episodeNumber
					+ "_transition_uncertainty", pd);
			*/
		}

		ExtendedValueIteration evi = new ExtendedValueIteration(r, rd, p, pd, _rewardInterval);
		_qplus = evi.solveQOptimistic(_T - (_time - 1));
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

}
