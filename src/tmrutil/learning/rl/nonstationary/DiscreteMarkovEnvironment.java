package tmrutil.learning.rl.nonstationary;

import java.util.Set;

/**
 * A discrete Markov environment.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface DiscreteMarkovEnvironment extends
		MarkovEnvironment<Integer, Integer>
{
	/**
	 * The number of states in this environment.
	 * 
	 * @return the number of states in this environment
	 */
	public int numberOfStates();

	/**
	 * The number of actions in this environment.
	 * 
	 * @return the number of actions in this environment
	 */
	public int numberOfActions();

	/**
	 * Returns the set of states that have positive probability of being
	 * immediately transitioned to from the specified state.
	 * 
	 * @param state
	 *            a state
	 * @return a set of successor states
	 */
	public Set<Integer> successors(Integer state);

	/**
	 * Returns a set of states that have actions that transition to the
	 * specified state with positive probability.
	 * 
	 * @param state
	 *            a state
	 * @return a set of predecessor states
	 */
	public Set<Integer> predecessors(Integer state);

}
