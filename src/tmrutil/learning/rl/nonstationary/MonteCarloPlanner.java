package tmrutil.learning.rl.nonstationary;

import tmrutil.stats.Statistics;

/**
 * A Monte Carlo tree-based planning algorithm.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 * @param <E>
 *            the environment type
 */
public class MonteCarloPlanner<S, E extends MarkovEnvironment<S, Integer>>
		implements Planner<S, Integer, E>
{
	private int _depth;
	private int _width;

	private int _numActions;

	private GenerativeModel<S, Integer> _gm;

	public MonteCarloPlanner(int depth, int width,
			GenerativeModel<S, Integer> gm, int numActions)
	{
		setDepth(depth);
		setWidth(width);
		setGenerativeModel(gm);
		_numActions = numActions;
	}

	public void setDepth(int depth)
	{
		if (depth < 1) {
			throw new IllegalArgumentException(
					"Planning depth must be positive.");
		}
		_depth = depth;
	}

	public int depth()
	{
		return _depth;
	}

	public void setWidth(int width)
	{
		if (_width < 1) {
			throw new IllegalArgumentException("Sample width must be positive.");
		}
		_width = width;
	}

	public int width()
	{
		return _width;
	}

	public void setGenerativeModel(GenerativeModel<S, Integer> gm)
	{
		if (gm == null) {
			throw new NullPointerException(
					"Cannot plan with a null generative model.");
		}
		_gm = gm;
	}

	public GenerativeModel<S, Integer> generativeModel()
	{
		return _gm;
	}

	@Override
	public Integer plan(S state, TryCounts<S> tcounts, long time, E environment,
			ReinforcementSignal<S, Integer> rsignal)
	{
		double[] Qs = estimateQ(state, tcounts, time, environment, rsignal,
				depth());
		return Statistics.maxIndex(Qs);
	}

	public double[] estimateQ(S state, TryCounts<S> tcounts, long time,
			E environment, ReinforcementSignal<S, Integer> rsignal, int depth)
	{
		double[] Qs = new double[_numActions];

		if (depth > 0) {
			for (int a = 0; a < _numActions; a++) {
				// Copy the try counts data structure and increment this state-aciton pair
				TryCounts<S> nextTCounts = new TryCounts<S>(tcounts);
				nextTCounts.increment(state, a);
				
				double sum = 0;
				for (int i = 0; i < width(); i++) {
					sum += rsignal.evaluate(state, a,
							tcounts.tryCount(state, a), time)
							+ environment.discountFactor()
							* estimateV(state, nextTCounts, time + 1,
									environment, rsignal, depth - 1);
				}
			}
		}

		return Qs;
	}

	public double estimateV(S state, TryCounts<S> tcounts, long time,
			E environment, ReinforcementSignal<S, Integer> rsignal, int depth)
	{
		return Statistics.max(estimateQ(state, tcounts, time, environment,
				rsignal, depth));
	}
}
