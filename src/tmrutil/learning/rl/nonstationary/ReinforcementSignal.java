package tmrutil.learning.rl.nonstationary;

/**
 * Interface for nonstationary reinforcement signals.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface ReinforcementSignal<S, A>
{
	/**
	 * Evaluate this reinforcement signal, which depends on the number of times
	 * the state-action pair has been tried and the current timestep.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param tryCount
	 *            the number of times the state-action pair has been tried
	 * @param time
	 *            a timestep
	 * @return a reinforcement sample
	 */
	public double evaluate(S state, A action, int tryCount, long time);
	
	/**
	 * The maximum possible immediate reward.
	 * @return the maximum possible immediate reward
	 */
	public double rmax();
	
	/**
	 * The minimum possible immediate reward.
	 * @return the minimum possible immediate reward
	 */
	public double rmin();

	/**
	 * Returns true if this state-action pair's reward distribution is
	 * stationary. If the distribution is nonstationary, then false is returned.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return true if this state-action pair's reward distribution is
	 *         stationary; otherwise it is nonstationary
	 */
	public boolean isStationary(S state, A action);

	/**
	 * Returns true if this state-action pair's reward distribution is
	 * nonstationary. If the distribution is stationary, then false is returned.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return true if this state-action pair's reward distribution is
	 *         nonstationary; otherwise it is stationary
	 */
	public boolean isNonstationary(S state, A action);

	/**
	 * Returns true if this state-action pair's reward distribution is
	 * nonstationary and depends on the "try count".
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return true if this state-action pair's reward distribution depends on
	 *         the "try count"; otherwise false
	 */
	public boolean dependsOnTryCount(S state, A action);

	/**
	 * Returns true if this state-action pair's reward distribution is
	 * nonstationary and depends on time.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return true if this state-action pair's reward distribution depends on
	 *         time; otherwise false
	 */
	public boolean dependsOnTime(S state, A action);
}
