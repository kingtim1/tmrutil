package tmrutil.learning.rl.nonstationary;

import java.util.Set;
import tmrutil.stats.Statistics;

/**
 * Implements a planner for tasks with nonstationary reward signals, where the
 * nonstationarity is caused by endogenous factors (such as the agent's own
 * actions).
 * 
 * @author Timothy A. Mann
 * 
 */
public class EndogenousPlanner extends AbstractDiscretePlanner
{

	public EndogenousPlanner(int horizon)
	{
		super(horizon);
	}

	@Override
	public Integer plan(Integer state, TryCounts<Integer> tcounts, long time,
			DiscreteMarkovEnvironment environment,
			ReinforcementSignal<Integer, Integer> rsignal)
	{
		double[][] Q = recursivePlanner(tcounts, time, environment, rsignal,
				horizon());
		return Statistics.maxIndex(Q[state]);
	}

	public double[][] recursivePlanner(TryCounts<Integer> tcounts, long time,
			DiscreteMarkovEnvironment environment,
			ReinforcementSignal<Integer, Integer> rsignal, int horizon)
	{
		int numStates = environment.numberOfStates();
		int numActions = environment.numberOfActions();
		double[][] Q = new double[numStates][numActions];

		if (horizon > 0) {
			for (int s = 0; s < numStates; s++) {
				Set<Integer> resultStates = environment.successors(s);
				for (int a = 0; a < numActions; a++) {
					double reward = rsignal.evaluate(s, a,
							tcounts.tryCount(s, a), time);
					TryCounts<Integer> nextTCounts = new TryCounts<Integer>(tcounts);
					nextTCounts.increment(s, a);

					double[][] nextQ = recursivePlanner(nextTCounts, time + 1,
							environment, rsignal, horizon - 1);
					double newQ = reward;
					for (int rs : resultStates) {
						newQ += environment.discountFactor()
								* environment.transitionProbability(s, a, rs)
								* Statistics.maxIndex(nextQ[rs]);
					}
					Q[s][a] = newQ;
				}
			}
		}

		return Q;
	}

}
