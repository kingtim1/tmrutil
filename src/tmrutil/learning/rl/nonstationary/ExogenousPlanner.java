package tmrutil.learning.rl.nonstationary;

import java.util.Set;
import tmrutil.stats.Statistics;

/**
 * Implements a planner for tasks with nonstationary reward signals, where the
 * nonstationarity is caused by exogenous factors. In other words, the change in
 * the reward distribution is dependent on time, but not on the agent's actions.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ExogenousPlanner extends AbstractDiscretePlanner
{

	public ExogenousPlanner(int horizon)
	{
		super(horizon);
	}

	@Override
	public Integer plan(Integer state, TryCounts<Integer> tcounts, long time,
			DiscreteMarkovEnvironment environment,
			ReinforcementSignal<Integer, Integer> rsignal)
	{
		int numStates = environment.numberOfStates();
		int numActions = environment.numberOfActions();
		double[][] backupQ = new double[numStates][numActions];
		double[][] Q = new double[numStates][numActions];

		for (int t = horizon() - 1; t >= 0; t--) {
			// Make a backup of the Q-table
			for (int s = 0; s < numStates; s++) {
				System.arraycopy(Q[s], 0, backupQ[s], 0, numActions);
			}

			// Update the Q-table
			for (int s = 0; s < numStates; s++) {
				Set<Integer> resultStates = environment.successors(s);
				for (int a = 0; a < numActions; a++) {
					// Sample the reward signal
					double reward = rsignal.evaluate(s, a,
							tcounts.tryCount(s, a), time + t);

					// Evaluate the action-value for (s, a)
					double newQ = reward;
					for (int rs : resultStates) {
						newQ += environment.discountFactor()
								* environment.transitionProbability(s, a, rs)
								* Statistics.max(backupQ[rs]);
					}

					// Update the Q-table's state-action pair
					Q[s][a] = newQ;
				}
			}
		}

		return Statistics.maxIndex(Q[state]);
	}

}
