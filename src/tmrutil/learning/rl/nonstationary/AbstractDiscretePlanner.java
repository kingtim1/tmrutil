package tmrutil.learning.rl.nonstationary;

/**
 * An abstract implementation for discrete planners.
 * @author Timothy A. Mann
 *
 */
public abstract class AbstractDiscretePlanner implements Planner<Integer,Integer,DiscreteMarkovEnvironment>
{
	private int _horizon;
	
	public AbstractDiscretePlanner(int horizon)
	{
		setHorizon(horizon);
	}
	
	public void setHorizon(int horizon)
	{
		if(horizon < 1){
			throw new IllegalArgumentException("Horizon must be positive."); 
		}
		_horizon = horizon;
	}
	
	public int horizon()
	{
		return _horizon;
	}

}
