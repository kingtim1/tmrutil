package tmrutil.learning.rl.nonstationary;

/**
 * Represents a Markov environment, which is similar to a Markov decision
 * process, but is separated from the concept of a reinforcement signal.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface MarkovEnvironment<S, A>
{
	/**
	 * Returns the transition probability of transitioning to a result state by
	 * taking <code>action</code> from </code>state</code>.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param resultState
	 *            a state
	 * @return the transition probability
	 */
	public double transitionProbability(S state, A action, S resultState);

	/**
	 * Returns the discount factor.
	 * 
	 * @return the discount factor
	 */
	public double discountFactor();

	/**
	 * Resets this Markov environment either to a reset state or according to
	 * some reset distribution.
	 */
	public void reset();
}
