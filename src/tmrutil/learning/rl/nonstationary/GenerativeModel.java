package tmrutil.learning.rl.nonstationary;

/**
 * This is a generative model for MDPs with nonstationary reward signals. A
 * generative model is a sampling model that enables a learning system to
 * receive next state and reward samples from any state-action pair, without
 * having to transition to that state-action pair.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface GenerativeModel<S, A>
{
	/**
	 * Samples from the next state distribution of the specified state-action
	 * pair.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return a state sampled from the next state distribution of the specified
	 *         state-action pair.
	 */
	public S sampleNextState(S state, A action);

	/**
	 * Samples a reward from the specified state-action pair's reward
	 * distribution. The reward distribution may or may not be nonstationary and
	 * depend on the number of times this state-action pair has been tried or
	 * the time.
	 * 
	 * @param state a state
	 * @param action an action
	 * @param tryCount the number of times this state-action pair has been visited previously by the agent
	 * @param time the timestep at which the state-action pair is executed
	 * @return a reward sample from this state-action pair's reward distribution
	 */
	public double sampleReward(S state, A action, int tryCount, long time);
}
