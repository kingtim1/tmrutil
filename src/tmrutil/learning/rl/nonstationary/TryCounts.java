package tmrutil.learning.rl.nonstationary;

import java.util.HashMap;
import java.util.Map;
import tmrutil.util.Pair;

/**
 * A datastructure for recording the number of times that each state-action pair in an environment has been tried.
 * @author Timothy A. Mann
 *
 */
public class TryCounts<S>
{
	private Map<Pair<S,Integer>,Integer> _tcount;
	
	/**
	 * Creates a default "try counts" datastructure with all counts set to zero.
	 */
	public TryCounts()
	{
		_tcount = new HashMap<Pair<S,Integer>,Integer>();
	}
	
	/**
	 * Constructs a copy of a "try counts" datastructure.
	 * @param copy the instance to copy
	 */
	public TryCounts(TryCounts<S> copy)
	{
		_tcount = new HashMap<Pair<S,Integer>, Integer>(copy._tcount);
	}
	
	/**
	 * Increments the number of "try counts" for a specified state-action pair.
	 * @param state a state
	 * @param action an action
	 */
	public void increment(S state, Integer action)
	{
		Pair<S,Integer> pair = new Pair<S,Integer>(state, action);
		Integer c = _tcount.get(pair);
		if(c == null){
			_tcount.put(pair, 1);
		}else{
			_tcount.put(pair, c.intValue() + 1);
		}
	}
	
	/**
	 * Returns the number of times that the specified state-action pair has been tried.
	 * @param state a state
	 * @param action an action
	 * @return the number of times that the specified state-action pair has been tried
	 */
	public int tryCount(S state, Integer action)
	{
		Pair<S,Integer> pair = new Pair<S,Integer>(state,action);
		Integer c = _tcount.get(pair);
		if(c == null){
			return 0;
		}else{
			return c.intValue();
		}
	}
	
	/**
	 * Resets all "try counts" to zero.
	 */
	public void reset()
	{
		_tcount.clear();
	}
}
