package tmrutil.learning.rl;

import java.util.Arrays;

/**
 * Implements the policy iteration dynamic programming algorithm. This algorithm
 * is guaranteed to converge in a finite number of steps.
 * 
 * @author Timothy A. Mann
 * 
 */
public class PolicyIteration
{
	/** The current policy. */
	private int[] _cpolicy;
	/** An estimate of the value function for the current policy. */
	private double[] _V;

	/** A reference to a Markov decision process. */
	private DiscreteMarkovDecisionProcess<Integer,Integer> _mdp;

	/** Convergence sensitivity parameter. */
	private double _theta;

	/**
	 * Constructs a policy iteration instance from a discrete Markov decision
	 * process.
	 * 
	 * @param mdp
	 *            a discrete Markov decision process
	 * @param convergenceSensitivity
	 *            a small scalar value (e.g. 0.001) that determines the
	 *            tolerable error at which to stop policy evaluation
	 */
	public PolicyIteration(DiscreteMarkovDecisionProcess<Integer,Integer> mdp,
			double convergenceSensitivity)
	{
		if (mdp == null) {
			throw new NullPointerException(
					"Policy iteration cannot be performed on a null Markov decision process.");
		}
		_mdp = mdp;
		_cpolicy = new int[_mdp.numberOfStates()];
		_V = new double[_mdp.numberOfActions()];
		setConvergenceSensitivity(convergenceSensitivity);
	}

	public int[] policyIteration()
	{
		boolean policyChanged = false;
		do {
			policyChanged = false;

			// Run policy evaluation
			PolicyEvaluation peval = new PolicyEvaluation(_mdp, _cpolicy,
					false, _theta);
			_V = peval.evaluatePolicy(_V);

			// Update the current policy
			for (int s1 = 0; s1 < _mdp.numberOfStates(); s1++) {
				int b = _cpolicy[s1];
				_cpolicy[s1] = argMaxNewV(s1);
				if(b != _cpolicy[s1]){
					policyChanged = true;
				}
			}

		} while (policyChanged);

		return Arrays.copyOf(_cpolicy, _cpolicy.length);
	}
	
	private int argMaxNewV(int state)
	{
		int maxA = 0;
		double maxV = 0;
		for(int a=0;a <_mdp.numberOfActions();a++){
			double v = 0;
			for(int s2=0;s2<_mdp.numberOfStates();s2++){
				v += _mdp.transitionProb(state, a, s2) * _mdp.reinforcement(state, a) + _mdp.getDiscountFactor() * _V[s2];
			}
			if(maxV < v || a == 0){
				maxA = a;
				maxV = v;
			}
		}
		return maxA;
	}

	/**
	 * Sets the convergence sensitivity of policy evaluation.
	 * 
	 * @param theta
	 *            a small scalar value (e.g. 0.001) that determines the
	 *            tolerable error at which to stop policy evaluation
	 */
	public void setConvergenceSensitivity(double theta)
	{
		if (theta <= 0) {
			throw new IllegalArgumentException(
					"Convergence sensitivity must be a small positive scalar value.");
		}
		_theta = theta;
	}

	/**
	 * Returns a small scalar value (e.g. 0.001) that determines the tolerable
	 * error at which to stop policy evaluation.
	 * 
	 * @return a small scalar value (e.g. 0.001) that determines the tolerable
	 *         error at which to stop policy evaluation
	 */
	public double getConvergenceSensitivity()
	{
		return _theta;
	}
}
