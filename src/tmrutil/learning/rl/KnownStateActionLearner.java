package tmrutil.learning.rl;

/**
 * Interface for reinforcement learning algorithms that make use of the known state or known state-action pair concept. 
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public interface KnownStateActionLearner<S,A>
{
	/**
	 * Returns true if the specified state-action pair is known. If the state-action pair is not known, then false is returned.
	 * @param state a state
	 * @param action an action
	 * @return true if the state-action pair is known; otherwise false
	 */
	public boolean isKnown(S state, A action);
}
