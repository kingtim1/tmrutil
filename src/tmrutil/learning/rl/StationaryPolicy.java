package tmrutil.learning.rl;

/**
 * A stationary policy is a {@link Policy} that does not change over time.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public interface StationaryPolicy<S, A> extends Policy<S, A> {

	/**
	 * Returns true to indicate that this policy is a deterministic mapping from
	 * states to actions. Otherwise false is returned indicating that this
	 * policy may return different actions for the same state.
	 * 
	 * @return true if this is a deterministic policy; otherwise false
	 */
	public boolean isDeterministic();

	
	/**
	 * Determines the probability of selecting action <code>action</code>
	 * according to this policy given that the current state is
	 * <code>state</code>.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the probability of selecting action <code>action</code> given the
	 *         specified state
	 */
	public double actionProb(S state, A action);
}
