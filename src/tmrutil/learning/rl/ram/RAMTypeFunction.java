package tmrutil.learning.rl.ram;

/**
 * Interface for Relocatable Action Model type functions.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 */
public interface RAMTypeFunction<S>
{
	/**
	 * Classifies the specified state into one of a finite number of relocatable action models.
	 * @param state a state
	 * @return the class that the specified state belongs to
	 */
	public int classify(S state);
	
	/**
	 * Specifies the number of classes that states can belong to.
	 * @return the number of classes
	 */
	public int numberOfClasses();
	
	/**
	 * Returns a string representation of the specified RAM class.
	 * @param ramClass a ram class
	 * @return a string representation of the specified RAM class
	 */
	public String classToString(Integer ramClass);
}
