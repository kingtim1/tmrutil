package tmrutil.learning.rl.ram;

import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.util.Interval;

/**
 * Relocatable Action Model Tasks.
 * @author Timothy A. Mann
 *
 */
public abstract class RAMTask extends DiscreteTask
{

	public RAMTask(int numStates, int numActions, Interval reinforcementInterval)
	{
		super(numStates, numActions, reinforcementInterval);
	}

	public abstract RAMTypeFunction<Integer> typeFunction();
	
	public abstract RAMNextStateFunction<Integer,Integer> nextStateFunction();
	
	public abstract ReinforcementSignal<Integer,Integer> reinforcementSignal();
}
