package tmrutil.learning.rl.ram;

/**
 * A relocatable action model (RAM) is a useful tool for modularizing large
 * state spaces in reinforcement learning.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface RAM<A, O>
{

	/**
	 * Returns the probability of an outcome based on a RAM class and an action.
	 * 
	 * @param ramClass
	 *            a relocatable action model class
	 * @param action
	 *            an action
	 * @param outcome
	 *            an outcome
	 * @return the probability of the specified outcome
	 */
	public double transitionProb(Integer ramClass, A action, O outcome);

	/**
	 * Returns the number of RAM classes.
	 * 
	 * @return number of classes
	 */
	public int numberOfClasses();

	/**
	 * Returns the number of actions.
	 * 
	 * @return the number of actions
	 */
	public int numberOfActions();

	/**
	 * Returns the number of possible outcomes.
	 * 
	 * @return the number of possible outcomes
	 */
	public int numberOfOutcomes();

	/**
	 * Returns true if the specified class-action pair has been sampled enough
	 * times to be considered known. This method for estimators and fixed RAMs
	 * can simply return true.
	 * 
	 * @param ramClass
	 *            a RAM class
	 * @param action
	 *            an action
	 * @return true of the class-action pair is known
	 */
	public boolean isKnown(Integer ramClass, A action);

	/**
	 * Retursn true if the specified class-action pair should be ignored.
	 * 
	 * @param ramClass
	 *            a RAM class
	 * @param action
	 *            an action
	 * @return true if the class-action pair should be ignored
	 */
	public boolean ignoreClassAction(Integer ramClass, A action);

	/**
	 * Determines if the specified outcome has a positive probability of
	 * occurring from a RAM type state.
	 * 
	 * @param ramClass
	 *            a RAM class
	 * @param outcome
	 *            an outcome
	 * @return true if the outcome has a positive probability of occurring;
	 *         otherwise false
	 */
	public boolean isOutcomeSuccessorOfClass(Integer ramClass, O outcome);
}
