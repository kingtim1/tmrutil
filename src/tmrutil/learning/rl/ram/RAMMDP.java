package tmrutil.learning.rl.ram;

import java.util.HashSet;
import java.util.Set;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcess;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.util.Interval;

public class RAMMDP implements DiscreteMarkovDecisionProcess<Integer,Integer>
{
	private int _numStates;
	private DiscreteRAM _ram;
	private RAMTypeFunction<Integer> _tfunc;
	private RAMNextStateFunction<Integer, Integer> _nsfunc;
	private ReinforcementSignal<Integer, Integer> _rsignal;
	private Interval _rewardInterval;
	private double _discountFactor;

	public RAMMDP(int numStates, DiscreteRAM ram,
			RAMTypeFunction<Integer> tfunc,
			RAMNextStateFunction<Integer, Integer> nsfunc,
			ReinforcementSignal<Integer, Integer> rsignal,
			Interval rewardInterval, double discountFactor)
	{
		_numStates = numStates;
		_ram = ram;
		_tfunc = tfunc;
		_nsfunc = nsfunc;
		_rsignal = rsignal;
		_rewardInterval = rewardInterval;
		_discountFactor = discountFactor;
	}
	
	public DiscreteRAM getRAM()
	{
		return _ram;
	}
	
	public RAMTypeFunction<Integer> typeFunction()
	{
		return _tfunc;
	}
	
	public RAMNextStateFunction<Integer,Integer> nextStateFunction()
	{
		return _nsfunc;
	}
	
	public ReinforcementSignal<Integer,Integer> reinforcementSignal()
	{
		return _rsignal;
	}

	@Override
	public double transitionProb(Integer state, Integer action,
			Integer resultState)
	{
		Integer ramClass = _tfunc.classify(state);
		Integer outcome = _nsfunc.outcomeMap(state, resultState);
		return _ram.transitionProb(ramClass, action, outcome);
	}

	@Override
	public double reinforcement(Integer state, Integer action)
	{
		Integer ramClass = _tfunc.classify(state);
		if(_ram.isKnown(ramClass, action)){
			if(_ram.ignoreClassAction(ramClass, action)){
				return _rewardInterval.getMin();
			}else{
				return _rsignal.evaluate(state, action);
			}
		}else{
			return _rewardInterval.getMax();
		}
	}

	@Override
	public double getDiscountFactor()
	{
		return _discountFactor;
	}

	@Override
	public Set<Integer> successors(Integer state)
	{
		int numOutcomes = _nsfunc.numberOfOutcomes();
		Integer ramClass = _tfunc.classify(state);
		Set<Integer> succs = new HashSet<Integer>();
		for(int o=0;o<numOutcomes;o++){
			if(o == _nsfunc.nullOutcome() || _ram.isOutcomeSuccessorOfClass(ramClass, o)){
				Integer nextState = _nsfunc.nextState(state, o);
				succs.add(nextState);
			}
		}
		return succs;
	}

	@Override
	public Set<Integer> predecessors(Integer state)
	{
		Set<Integer> preds = new HashSet<Integer>();
		for(int s = 0; s < numberOfStates(); s++){
			preds.add(s);
		}
		return preds;
	}

	@Override
	public int numberOfStates()
	{
		return _numStates;
	}

	@Override
	public int numberOfActions()
	{
		return _ram.numberOfActions();
	}

	@Override
	public double rmax()
	{
		return _rewardInterval.getMax();
	}

	@Override
	public double rmin()
	{
		return _rewardInterval.getMin();
	}

	@Override
	public boolean isKnown(Integer state, Integer action)
	{
		Integer ramClass = _tfunc.classify(state);
		return _ram.isKnown(ramClass, action);
	}
	
	@Override
	public Set<Integer> states() {
		Set<Integer> states = new HashSet<Integer>();
		for(int s=0;s<numberOfStates();s++){
			states.add(s);
		}
		return states;
	}

	@Override
	public Set<Integer> actions(Integer state) {
		Set<Integer> actions = new HashSet<Integer>();
		for(int a=0;a<numberOfActions();a++){
			actions.add(a);
		}
		return actions;
	}

}
