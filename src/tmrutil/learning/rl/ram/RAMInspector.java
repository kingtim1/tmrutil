package tmrutil.learning.rl.ram;

import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import tmrutil.graphics.DrawableComponent;
import tmrutil.graphics.MatrixDisplay;

/**
 * A graphical component for inspecting relocatable action models.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RAMInspector extends JComponent implements ItemListener
{
	private static final long serialVersionUID = -6870194919347814223L;

	private RAMMDP _mdp;
	private double[][] _Q;
	private JComboBox _classBox;
	private JComboBox _actionBox;

	private JComboBox _stateBox;
	private JLabel _Rval;
	private JLabel _Qval;

	private MatrixDisplay _matDisplay;

	public RAMInspector(RAMMDP mdp, double[][] Q)
	{
		_mdp = mdp;
		_Q = Q;
		setupUI();
	}

	private void setupUI()
	{
		int numClasses = _mdp.getRAM().numberOfClasses();
		int numActions = _mdp.getRAM().numberOfActions();
		int numOutcomes = _mdp.getRAM().numberOfOutcomes();

		Object[] cs = new Object[numClasses];
		for (int c = 0; c < numClasses; c++) {
			cs[c] = _mdp.typeFunction().classToString(c);
		}
		Object[] as = new Object[numActions];
		for (int a = 0; a < numActions; a++) {
			as[a] = new Integer(a);
		}

		//
		// First Column
		//
		_classBox = new JComboBox(cs);
		_classBox.addItemListener(this);
		_actionBox = new JComboBox(as);
		_actionBox.addItemListener(this);
		JPanel controlBoxes = new JPanel();
		controlBoxes.setLayout(new GridLayout(2, 2));
		controlBoxes.add(new JLabel("RAM Class:"));
		controlBoxes.add(_classBox);
		controlBoxes.add(new JLabel("Action:"));
		controlBoxes.add(_actionBox);

		//
		// Second Column
		//
		_matDisplay = new MatrixDisplay(new double[1][numOutcomes]);
		DrawableComponent outcomeProbs = new DrawableComponent(_matDisplay);
		outcomeProbs.addMouseMotionListener(_matDisplay);
		JPanel matPanel = new JPanel();
		matPanel.setLayout(new GridLayout(2, 1));
		matPanel.add(new JLabel("Pr[Outcome]:"));
		matPanel.add(outcomeProbs);

		//
		// Third Column
		//
		int numStates = _mdp.numberOfStates();
		List<Integer> states = new ArrayList<Integer>();
		for (int s = 0; s < numStates; s++) {
			if (_mdp.typeFunction().classify(s) == 0) {
				states.add(new Integer(s));
			}
		}
		_stateBox = new JComboBox(states.toArray());
		_stateBox.addItemListener(this);
		JPanel valPanel = new JPanel();
		valPanel.setLayout(new GridLayout(2, 1));
		valPanel.add(_stateBox);
		_Qval = new JLabel("");
		_Rval = new JLabel("");
		JPanel qrPanel = new JPanel();
		qrPanel.setLayout(new GridLayout(1, 2));
		qrPanel.add(_Qval);
		qrPanel.add(_Rval);
		valPanel.add(qrPanel);

		updateMatDisplay(0, 0);
		setLayout(new GridLayout(1, 3));

		add(controlBoxes);
		add(matPanel);
		add(valPanel);
	}

	public static final void inspect(RAMMDP mdp, double[][] Q)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(800, 100);

		frame.add(new RAMInspector(mdp, Q));

		frame.setVisible(true);
	}

	private void updateMatDisplay(Integer ramClass, Integer action)
	{
		int numOutcomes = _mdp.getRAM().numberOfOutcomes();
		double[] oprobs = new double[numOutcomes];

		for (int o = 0; o < numOutcomes; o++) {
			oprobs[o] = _mdp.getRAM().transitionProb(ramClass, action, o);
		}

		_matDisplay.setMatrix(new double[][] { oprobs });
	}
	
	private static final NumberFormat FORMAT = new DecimalFormat("###.0#");

	private void updateQR(Integer state, Integer action)
	{
		if (state != null && action != null) {
			_Qval.setText("Q(" + state + "," + action + ")="
					+ FORMAT.format(_Q[state][action]));
			_Rval.setText("R(" + state + "," + action + ")="
					+ FORMAT.format(_mdp.reinforcement(state, action)));
		}else{
			_Qval.setText("Q(-,-)");
			_Rval.setText("R(-,-)");
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e)
	{
		Object src = e.getSource();

		if (src == _classBox || src == _actionBox) {
			Integer ramClass = _classBox.getSelectedIndex();
			Integer action = (Integer) _actionBox.getSelectedItem();

			DefaultComboBoxModel model = (DefaultComboBoxModel) _stateBox.getModel();
			model.removeAllElements();
			int numStates = _mdp.numberOfStates();
			for (int s = 0; s < numStates; s++) {
				if (_mdp.typeFunction().classify(s) == ramClass) {
					model.addElement(new Integer(s));
				}
			}

			// System.out.println("Updating (RAM Class:" + ramClass +
			// ", Action:" + action + ")");
			updateMatDisplay(ramClass, action);
		}
		if (src == _stateBox || src == _actionBox) {
			Integer state = (Integer) _stateBox.getSelectedItem();
			Integer action = (Integer) _actionBox.getSelectedItem();
			updateQR(state, action);
		}
		repaint();
	}
}
