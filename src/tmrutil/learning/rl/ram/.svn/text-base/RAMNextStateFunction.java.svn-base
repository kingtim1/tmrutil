package tmrutil.learning.rl.ram;

/**
 * The next state function for Relocatable Action Models.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <O> the outcome type
 */
public interface RAMNextStateFunction<S,O>
{
	/**
	 * Specifies the next state given the current state and an outcome.
	 * @param state the current state
	 * @param outcome an outcome
	 * @return the next state
	 */
	public S nextState(S state, O outcome);
	
	/**
	 * Maps the relationship between a state and a next state to a specified outcome.
	 * @param state a state
	 * @param nextState the next state
	 * @return an outcome
	 */
	public O outcomeMap(S state, S nextState);
	
	/**
	 * Returns a string representation of the specified outcome.
	 * @param outcome an outcome
	 * @return a string representation of the outcome
	 */
	public String outcomeToString(O outcome);
	
	/**
	 * Returns the outcome representing no change.
	 * @return the null outcome
	 */
	public O nullOutcome();
	
	/**
	 * Returns the number of possible outcomes.
	 * @return the number of possible outcomes
	 */
	public int numberOfOutcomes();
}
