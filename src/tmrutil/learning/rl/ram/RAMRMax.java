package tmrutil.learning.rl.ram;

import java.util.Set;

import tmrutil.learning.LearningRate;
import tmrutil.learning.rl.AbstractDiscreteTemporalDifferenceLearning;
import tmrutil.learning.rl.Explorer;
import tmrutil.learning.rl.KnownStateActionLearner;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.VFunction;
import tmrutil.math.MatrixOps;
import tmrutil.optim.Optimization;
import tmrutil.util.DataStore;
import tmrutil.util.Interval;

/**
 * Implements relocatable action model (RAM) R-MAX algorithm.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RAMRMax extends AbstractDiscreteTemporalDifferenceLearning
		implements Explorer<Integer, Integer>,
		KnownStateActionLearner<Integer, Integer>
{
	public static final double CONVERGENCE_THRESHOLD = 0.01;
	public static final int MAX_ITERATIONS_PER_UPDATE = 50;

	private DifferentialExplorationRAMEstimator _rams;
	private RAMTypeFunction<Integer> _tfunc;
	private RAMNextStateFunction<Integer, Integer> _nsfunc;
	private RAMMDP _mdp;

	private double _rmax;
	private double _vmax;
	private double[] _V;

	public RAMRMax(int numStates, int numActions, RAMTypeFunction<Integer> tfunc,
			RAMNextStateFunction<Integer,Integer> nsfunc, ReinforcementSignal<Integer,Integer> rsignal, Interval rewardInterval, double discountFactor, int numVisitsUntilKnown)
	{
		this(numStates, numActions, tfunc, nsfunc, rsignal, rewardInterval, discountFactor, numVisitsUntilKnown, false);
	}
	
	public RAMRMax(int numStates, int numActions,
			RAMTypeFunction<Integer> tfunc,
			RAMNextStateFunction<Integer, Integer> nsfunc,
			ReinforcementSignal<Integer, Integer> rsignal,
			Interval rewardInterval, double discountFactor,
			int numVisitsUntilKnown, boolean refreshSamples)
	{
		this(numStates, numActions, tfunc, nsfunc, rsignal, rewardInterval,
				discountFactor,
				DifferentialExplorationRAMEstimator.buildNumVisitsTable(
						tfunc.numberOfClasses(), numActions,
						numVisitsUntilKnown), refreshSamples);
	}

	public RAMRMax(int numStates, int numActions,
			RAMTypeFunction<Integer> tfunc,
			RAMNextStateFunction<Integer, Integer> nsfunc,
			ReinforcementSignal<Integer, Integer> rsignal,
			Interval rewardInterval, double discountFactor,
			int[][] numVisitsUntilKnown, boolean refreshSamples)
	{
		super(numStates, numActions, new LearningRate.Constant(0.0),
				discountFactor, Optimization.MAXIMIZE);

		_tfunc = tfunc;
		_nsfunc = nsfunc;
		_rams = new DifferentialExplorationRAMEstimator(
				_tfunc.numberOfClasses(), numActions,
				_nsfunc.numberOfOutcomes(), _nsfunc.nullOutcome(),
				numVisitsUntilKnown, refreshSamples);

		_mdp = new RAMMDP(numStates, _rams, tfunc, nsfunc, rsignal,
				rewardInterval, discountFactor);
		_rmax = rewardInterval.getMax();
		_vmax = _rmax / (1 - discountFactor);
		_V = new double[numStates];

		reset();
	}

	public double vmax()
	{
		return _vmax;
	}

	public double rmax()
	{
		return _rmax;
	}

	@Override
	protected void resetImpl()
	{
		_rams.reset();
		fillQ(vmax());
	}

	@Override
	protected void trainImpl(Integer prevState, Integer action,
			Integer newState, double reinforcement)
	{
		Integer ramClass = _tfunc.classify(prevState);
		Integer outcome = _nsfunc.outcomeMap(prevState, newState);
		boolean update = _rams.addSample(ramClass, action, outcome);
		if (update) {
			valueIteration(MAX_ITERATIONS_PER_UPDATE, CONVERGENCE_THRESHOLD);
		}
	}

	private void valueIteration(int maxIterations, double threshold)
	{
		// _V = new double[numberOfStates()];
		// for(int s=0;s<numberOfStates();s++){
		// _V[s] = _vmax;
		// }

		boolean converged = false;
		double gamma = _mdp.getDiscountFactor();

		double[][] U = getAdmissibleHeuristic();

		for (int i = 0; i < maxIterations && !converged; i++) {

			double delta = 0;
			for (int s = 0; s < numberOfStates(); s++) {
				int ramClass = _tfunc.classify(s);
				Set<Integer> nextStates = _mdp.successors(s);
				for (int a = 0; a < numberOfActions(); a++) {
					double oldValue = evaluateQ(s, a);
					if (_rams.isKnown(ramClass, a)) {
						double saValue = _mdp.reinforcement(s, a);
						for (Integer ns : nextStates) {
							double tprob = _mdp.transitionProb(s, a, ns);
							double Vns = _V[ns];
							saValue += gamma * tprob * Vns;
						}

						double diff = Math.abs(saValue - oldValue);
						delta = Math.max(delta, diff);
						setQ(s, a, saValue);
					} else {
						double newQ = (U == null) ? _vmax : U[s][a];
						double diff = Math.abs(newQ - oldValue);
						delta = Math.max(delta, diff);
						setQ(s, a, newQ);
					}
				}
				_V[s] = maxQ(s);
			}

			if (delta <= threshold) {
				converged = true;
			}
		}
	}

	public void plotKnown()
	{
		int numClasses = _tfunc.numberOfClasses();
		double[][] known = new double[numClasses][numberOfActions()];
		for (int c = 0; c < numClasses; c++) {
			for (int a = 0; a < numberOfActions(); a++) {
				if (_rams.isKnown(c, a)) {
					known[c][a] = 1.0;
				}
			}
		}
		MatrixOps.displayGraphically(known, "Known Class-Actions");
	}
	
	public void inspectModel()
	{
		RAMInspector.inspect(_mdp, getQ());
	}

	@Override
	public boolean isExploration(Integer state, Integer action)
	{
		int c = _tfunc.classify(state);
		return !_rams.isKnown(c, action);
	}

	@Override
	public boolean isKnown(Integer state, Integer action)
	{
		int c = _tfunc.classify(state);
		return _rams.isKnown(c, action);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<Integer> greedyValueFunction() {
		return new QFunction.GreedyVFunction<Integer>(this);
	}

	@Override
	public double value(Integer state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(Integer state) {
		return maxQ(state);
	}

}
