package tmrutil.learning.rl.ram;

/**
 * An estimator for a discrete RAM.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RAMEstimator implements DiscreteRAM
{

	private int _numClasses;
	private int _numActions;
	private int _numOutcomes;

	private int _nullOutcome;

	private int _numSamples;
	private boolean _refreshSamples;

	private int[][] _classActionRefresh;
	private int[][][] _outcomeRefresh;

	private boolean[][] _knownClassActions;
	private int[][][] _outcomeCounts;

	/**
	 * Constructs an estimator for a relocatable action model (RAM).
	 * 
	 * @param numClasses
	 *            the number of RAM classes
	 * @param numActions
	 *            the number of actions
	 * @param numOutcomes
	 *            the number of outcomes
	 * @param numSamples
	 *            the number of samples
	 * @param refreshSamples
	 *            determines whether the ram will be updated with fresh samples
	 *            or not
	 */
	public RAMEstimator(int numClasses, int numActions, int numOutcomes,
			Integer nullOutcome, int numSamples, boolean refreshSamples)
	{
		if (numClasses < 1) {
			throw new IllegalArgumentException(
					"The number of classes must be greater than or equal to 1.");
		}
		_numClasses = numClasses;
		if (numActions < 1) {
			throw new IllegalArgumentException(
					"The number of actions must be greater than or equal to 1.");
		}
		_numActions = numActions;
		if (numOutcomes < 1) {
			throw new IllegalArgumentException(
					"The number of outcomes must be greater than or equal to 1.");
		}
		_numOutcomes = numOutcomes;

		if (numSamples < 1) {
			throw new IllegalArgumentException(
					"The number of samples used to estimate a transition probability must be greater than or equal to 1.");
		}
		_numSamples = numSamples;
		_refreshSamples = refreshSamples;
		_nullOutcome = nullOutcome;

		_classActionRefresh = new int[numClasses][numActions];
		_outcomeRefresh = new int[numClasses][numActions][numOutcomes];

		_knownClassActions = new boolean[numClasses][numActions];
		_outcomeCounts = new int[numClasses][numActions][numOutcomes];
		
		reset();
	}

	/**
	 * Adds a sample to this RAM estimator.
	 * 
	 * @param ramClass
	 *            an observed RAM class
	 * @param action
	 *            an observed action
	 * @param outcome
	 *            an observed outcome
	 * @return true if the RAM model has been updated; otherwise false
	 */
	public boolean addSample(Integer ramClass, Integer action, Integer outcome)
	{
		boolean updated = false;
		boolean known = isKnown(ramClass, action);
		if (!known || _refreshSamples) {

			_classActionRefresh[ramClass][action]++;
			_outcomeRefresh[ramClass][action][outcome]++;

			if (_classActionRefresh[ramClass][action] == _numSamples) {
				_knownClassActions[ramClass][action] = true;
				_classActionRefresh[ramClass][action] = 0;

				for (int o = 0; o < numberOfOutcomes(); o++) {
					int oValInt = _outcomeRefresh[ramClass][action][o];
					int cValInt = _outcomeCounts[ramClass][action][o];

					if (oValInt != cValInt) {
						_outcomeCounts[ramClass][action][o] = oValInt;
						updated = true;
					}
					_outcomeRefresh[ramClass][action][o] = 0;
				}
			}
		}
		return updated;
	}

	@Override
	public double transitionProb(Integer ramClass, Integer action,
			Integer outcome)
	{
		if (isKnown(ramClass, action)) {
			int caCount = classActionCount(ramClass, action);
			int oCount = outcomeCount(ramClass, action, outcome);
			double tprob = oCount / (double) caCount;
			return tprob;
		} else {
			if (outcome == _nullOutcome) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	/**
	 * Determines whether or not the specified class-action pair is "known". In
	 * other words, it determines whether or not the class-action pair has been
	 * sampled enough times to be trusted.
	 * 
	 * @param ramClass
	 *            a relocatable action model class
	 * @param action
	 *            an action
	 * @return true if the class-action pair is "known"; otherwise false
	 */
	@Override
	public boolean isKnown(Integer ramClass, Integer action)
	{
		return _knownClassActions[ramClass][action];
	}

	public int classActionCount(Integer ramClass, Integer action)
	{
		if (_knownClassActions[ramClass][action]) {
			return _numSamples;
		} else {
			return _classActionRefresh[ramClass][action];
		}

	}

	public int outcomeCount(Integer ramClass, Integer action, Integer outcome)
	{
		if (_knownClassActions[ramClass][action]) {
			return _outcomeCounts[ramClass][action][outcome];
		} else {
			return _outcomeRefresh[ramClass][action][outcome];
		}
	}

	@Override
	public int numberOfClasses()
	{
		return _numClasses;
	}

	@Override
	public int numberOfActions()
	{
		return _numActions;
	}

	@Override
	public int numberOfOutcomes()
	{
		return _numOutcomes;
	}

	public void reset()
	{
		for(int c=0;c<_numClasses;c++){
			for(int a=0;a<_numActions;a++){
				_knownClassActions[c][a] = false;
				_classActionRefresh[c][a] = 0;
				for(int o=0;o<_numOutcomes;o++){
					_outcomeRefresh[c][a][o] = 0;
					_outcomeCounts[c][a][o] = 0;
				}
			}
		}
	}

	@Override
	public boolean isOutcomeSuccessorOfClass(Integer ramClass, Integer outcome)
	{
		for(int a=0;a<_numActions;a++){
			if(_outcomeCounts[ramClass][a][outcome] > 0){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean ignoreClassAction(Integer ramClass, Integer action)
	{
		return false;
	}
}
