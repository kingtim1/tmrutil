package tmrutil.learning.rl;

import tmrutil.math.MatrixOps;
import tmrutil.math.SAMatrixOps;
import tmrutil.stats.Statistics;

/**
 * Implements the SARSA on-policy reinforcement learning algorithm.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Sarsa extends QLearning
{
	/**
	 * Constructs a SARSA on-line learning algorithm.
	 * 
	 * @param numStates
	 *            the number of states
	 * @param numActions
	 *            the number of actions
	 * @param alpha
	 *            the learning rate
	 * @param gamma
	 *            the discount factor
	 * @param epsilon
	 *            the value of epsilon used for the epsilon-greed policy method
	 */
	public Sarsa(int numStates, int numActions, double alpha, double gamma,
			double epsilon)
	{
		super(numStates, numActions, alpha, gamma);
		setEpsilon(epsilon);
	}

	/**
	 * Constructs a SARSA on-line learning algorithm.
	 * 
	 * @param q
	 *            a Q-table
	 * @param alpha
	 *            the learning rate
	 * @param gamma
	 *            the discount factor
	 * @param epsilon
	 *            the probability of selecting an action at random instead of
	 *            following the policy
	 */
	public Sarsa(double[][] q, double alpha, double gamma, double epsilon)
	{
		super(q, alpha, gamma);
		setEpsilon(epsilon);
	}

	@Override
	public void trainImpl(Integer prevState, Integer action,
			Integer newState, double reward)
	{
		double[][] q = getQ();
		double qsa = q[prevState][action];
		double qs1a = q[newState][policy(newState)];
		q[prevState][action] = ((1.0 - getLearningRate()) * qsa)
				+ getLearningRate() * (reward + (getDiscountFactor() * qs1a));
	}
}
