package tmrutil.learning.rl;

/**
 * A convenience translator implementation that performs the identity translation (no translation).
 * @author Timothy A. Mann
 *
 * @param <T> a type
 */
public class IdentityTranslator<T> implements Translator<T, T>
{

	@Override
	public T translate(T action)
	{
		return action;
	}

}
