package tmrutil.learning.rl;

import tmrutil.learning.LearningRate;

/**
 * This interface should be implemented by temporal difference learning
 * algorithms.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface TemporalDifferenceLearning<S, A> extends
		ValueBasedLearningSystem<S, A>
{
	/**
	 * Returns the current time step, which is a nonnegative integer denoting
	 * the number of times that the training method has been called since this
	 * object was constructed or the reset method was called (whichever is more
	 * recent).
	 * 
	 * @return a nonnegative integer
	 */
	public long getTime();

	/**
	 * Returns a scalar value from the interval [0, 1] that determines the
	 * portion of the error to correct for during training.
	 * 
	 * @return a scalar value from the interval [0, 1]
	 */
	public double getLearningRate();

	/**
	 * Sets the learning rate sequence.
	 * 
	 * @param learningRate
	 *            a learning rate sequence
	 */
	public void setLearningRate(LearningRate learningRate);

	/**
	 * Sets the learning rate sequence to a constance scalar value in the
	 * interval [0, 1].
	 * 
	 * @param learningRate
	 *            a scalar value in the interval [0, 1]
	 * @throws IllegalArgumentException
	 *             if <code>learningRate < 0 || learningRate > 1</code>
	 */
	public void setLearningRate(double learningRate)
			throws IllegalArgumentException;

	/**
	 * Returns a scalar value from the interval [0, 1] that determines the
	 * amount of emphasis placed on future costs/rewards. A value close to 1
	 * signifies that the agent places high emphasis on costs/rewards that are
	 * far in the future, while a value close to 0 indicates that the agent
	 * places low emphasis on future costs/rewards and will select actions to
	 * improve its current condition. The behavior of an individual with
	 * discount factor close to 0 is often described as greedy.
	 * 
	 * @return a scalar value from the interval [0, 1]
	 */
	public double getDiscountFactor();

	/**
	 * Sets the discount factor to a scalar value from the interval [0, 1] that
	 * determines the amount of emphasis placed on future costs/rewards. A value
	 * close to 1 signifies that the agent places high emphasis on costs/rewards
	 * that are far in the future, while a value close to 0 indicates that the
	 * agent places low emphasis on future costs/rewards and will select actions
	 * to improve its current condition. The behavior of an individual with
	 * discount factor close to 0 is often described as greedy.
	 * 
	 * @param discountFactor
	 *            a scalar value from the interval [0, 1]
	 * @throws IllegalArgumentException
	 *             if <code>discountFactor < 0 || discountFactor > 1</code>
	 */
	public void setDiscountFactor(double discountFactor)
			throws IllegalArgumentException;

	/**
	 * Returns true if this learning system is attempting to maximize the sum of
	 * reinforcements (i.e. rewards) over the long term.
	 * 
	 * @return true if maximizing the sum of reinforcements
	 */
	public boolean isMaximizing();

	/**
	 * Returns true if this learning system is attempting to minimize the sum of
	 * reinforcements (i.e. costs) over the long term.
	 * 
	 * @return true if minimizing the sum of reinforcements
	 */
	public boolean isMinimizing();

	/**
	 * Resets this temporal difference algorithm so that the time step is set
	 * back to 0 and the weights of the function approximators or table entries
	 * are reset to either random or initial values.
	 */
	public void reset();
}
