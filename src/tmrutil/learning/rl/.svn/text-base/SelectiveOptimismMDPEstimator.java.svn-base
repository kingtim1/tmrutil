package tmrutil.learning.rl;

import java.util.Collection;
import java.util.Set;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcess.ArrayEstimator;
import tmrutil.math.MatrixOps;
import tmrutil.stats.Filter;
import tmrutil.util.Interval;

public class SelectiveOptimismMDPEstimator extends ArrayEstimator
{

	/**
	 * An interface for implementing domain specific filters that determine
	 * whether or not two states are close.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static interface CloseDetector
	{
		/**
		 * Return true if the <code>currentState</code> is close to the
		 * <code>otherState</code>; otherwise false is returned. This method is
		 * similar to a distance metric in that a state must be close to itself
		 * and it is symmetric. However, this method is not transitive.
		 * 
		 * @param currentState
		 *            the current state
		 * @param otherState
		 *            another state
		 * @return true if the two states are close; otherwise false is returned
		 */
		public boolean areClose(Integer currentState, Integer otherState);

		/**
		 * Returns the number of states that are close to this state.
		 * 
		 * @param state
		 *            a state
		 * @return the number of states that are close to this state
		 */
		public int numCloseStates(Integer state);

		/**
		 * Classifies the relationship between the <code>currentState</code> and
		 * another state specified by <code>otherState</code>. The relationships
		 * are classified as follows:
		 * <ol>
		 * <li>-1 : indicates that the states are not close</li>
		 * <li>0 : indicates that the states are identical</li>
		 * <li>1-N : indicate other potential relationships</li>
		 * </ol>
		 * 
		 * @param currentState
		 *            the current state
		 * @param otherState
		 *            another state
		 * @return an integer classifying the relationship between the specified
		 *         states
		 */
		public int classifyRelationship(Integer currentState, Integer otherState);

		/**
		 * Returns the number of relationships that can occur between close
		 * states.
		 * 
		 * @return the number of relationships that can occur between close
		 *         states
		 */
		public int numRelationships();
	}

	private Filter<Integer> _relevant;
	private CloseDetector _closeDetector;

	private int[][] _actionRelationCount;
	private int[] _actionCount;

	public SelectiveOptimismMDPEstimator(int numStates, int numActions, double discountFactor,
			int numVisitsUntilKnown, Filter<Integer> relevanceFilter,
			CloseDetector closeDetector, Interval rewardInterval)
	{
		super(numStates, numActions, discountFactor, rewardInterval,
				numVisitsUntilKnown);
		_relevant = relevanceFilter;
		_closeDetector = closeDetector;

		_actionRelationCount = new int[numActions][_closeDetector.numRelationships()];
		_actionCount = new int[numActions];
	}

	@Override
	public boolean update(Integer state, Integer action, Integer resultState,
			double reinforcement)
	{
		boolean retVal = super.update(state, action, resultState, reinforcement);

		int r = _closeDetector.classifyRelationship(state, resultState);
		_actionRelationCount[action][r]++;

		_actionCount[action]++;

		return retVal;
	}

	@Override
	public void reset()
	{
		super.reset();
		for (int a = 0; a < numberOfActions(); a++) {
			_actionCount[a] = 0;
			for (int r = 0; r < _closeDetector.numRelationships(); r++) {
				_actionRelationCount[a][r] = 0;
			}
		}
	}

	@Override
	public double reinforcementUnknown(Integer state, Integer action)
	{
		if (_relevant.accept(state)) {
			return rmax();
		} else {
			return rmin();
		}
	}

	@Override
	public double transitionProbUnknown(Integer state, Integer action,
			Integer resultState)
	{
		if (_relevant.accept(state)) {
			// If a state is relevant, then treat it as an absorbing state. This
			// will encourage visiting this state.

			if (state.equals(resultState)) {
				return 1;
			} else {
				return 0;
			}
		} else {
			// Otherwise the state is irrelevant, so we want to provide the best
			// guess about the effects of actions so that the agent can get away
			// from this state and back to relevant states.

			Integer relationship = _closeDetector.classifyRelationship(state,
					resultState);
			if (relationship >= 0) {
				int actCount = _actionCount[action];
				if (actCount > 0) {
					return _actionRelationCount[action][relationship]
							/ (double) actCount;
				} else {
					if (state.equals(resultState)) {
						return 1;
					} else {
						return 0;
					}
				}
			} else {
				return 0;
			}
		}
	}

	public void displayUniversalActionModel()
	{
		double[][] actRelTable = new double[numberOfActions()][_closeDetector.numRelationships()];
		for (int a = 0; a < numberOfActions(); a++) {
			for (int r = 0; r < _closeDetector.numRelationships(); r++) {
				actRelTable[a][r] = _actionRelationCount[a][r]
						/ (double) _actionCount[a];
			}
		}
		MatrixOps.displayGraphically(actRelTable, "Universal Action Model");
	}

	public boolean isRelevant(Integer state)
	{
		return _relevant.accept(state);
	}

	@Override
	public Set<Integer> predecessors(Integer state)
	{
		Set<Integer> preds = super.predecessors(state);
		if (preds.contains(state)) {
			return preds;
		} else {
			preds.add(state);
			return preds;
		}
	}

	@Override
	public Set<Integer> successors(Integer state)
	{
		Set<Integer> succs = super.successors(state);
		if (succs.contains(state)) {
			return succs;
		} else {
			succs.add(state);
			return succs;
		}
	}
}
