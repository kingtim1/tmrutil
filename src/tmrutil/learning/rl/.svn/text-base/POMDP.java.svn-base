package tmrutil.learning.rl;

/**
 * Represents a Partially Observable Markov Decision Process (POMDP).
 * @author Timothy A. Mann
 *
 * @param <O> the observation type
 * @param <S> the state type
 * @param <A> the action type
 */
public interface POMDP<O,S,A> extends MarkovDecisionProcess<S,A>
{
	/**
	 * The probability of making a particular observation given the current state.
	 * @param state a state
	 * @param observation an observation
	 * @return the probability of making the specified observation in <code>state</code>
	 */
	public double observationProbs(S state, O observation);
}
