package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import tmrutil.learning.Optimization;
import tmrutil.util.Pair;

/**
 * An abstract Monte-Carlo planning algorithm for generative models.
 * 
 * @author Timothy A. Mann
 * 
 */
public abstract class MonteCarloPlanner<S, A> implements LearningSystem<S, A>
{
	protected class DepthNode
	{
		private int _totalVisits;
		private Map<Pair<S, A>, Double> _cumVals;
		private Map<S, Integer> _sCounts;
		private Map<Pair<S, A>, Integer> _saCounts;

		public DepthNode()
		{
			_totalVisits = 0;
			_cumVals = new HashMap<Pair<S, A>, Double>();
			_sCounts = new HashMap<S, Integer>();
			_saCounts = new HashMap<Pair<S, A>, Integer>();
		}

		public int totalVisits()
		{
			return _totalVisits;
		}

		public int visitCount(S state)
		{
			Integer visitCount = _sCounts.get(state);
			if (visitCount == null) {
				return 0;
			} else {
				return visitCount.intValue();
			}
		}

		public int visitCount(S state, A action)
		{
			Pair<S, A> key = new Pair<S, A>(state, action);
			Integer visitCount = _saCounts.get(key);
			if (visitCount == null) {
				return 0;
			} else {
				return visitCount.intValue();
			}
		}

		public double reward(S state, A action)
		{
			Pair<S, A> key = new Pair<S, A>(state, action);
			int visitCount = visitCount(state, action);
			if (visitCount == 0) {
				return 0.0;
			} else {
				Double cumVal = _cumVals.get(key);
				return cumVal.doubleValue() / visitCount;
			}
		}

		public void update(S state, A action, double value)
		{
			_totalVisits++;

			Integer sCount = _sCounts.get(state);
			if (sCount == null) {
				_sCounts.put(state, new Integer(1));
			} else {
				_sCounts.put(state, new Integer(sCount.intValue() + 1));
			}

			Pair<S, A> key = new Pair<S, A>(state, action);
			Integer visitCount = _saCounts.get(key);
			if (visitCount == null) {
				_saCounts.put(key, new Integer(1));
			} else {
				_saCounts.put(key, new Integer(visitCount.intValue() + 1));
			}

			Double cumVal = _cumVals.get(key);
			if (cumVal == null) {
				_cumVals.put(key, new Double(value));
			} else {
				_cumVals.put(key, new Double(cumVal.doubleValue() + value));
			}
		}
	}

	private GenerativeModel<S, A> _gmodel;
	private double _discountFactor;
	private int _maxDepth;

	private Set<A> _validActions;
	private List<DepthNode> _q;
	private Map<S, A> _knownPolicy;
	private Optimization _opType;

	private int _numSamples;

	/**
	 * Constructs a Monte-Carlo planner from a generative model.
	 * 
	 * @param gmodel
	 *            a generative model
	 */
	public MonteCarloPlanner(GenerativeModel<S, A> gmodel, Set<A> validActions,
			int maxDepth, double discountFactor, Optimization opType,
			int numSamples)
	{
		_gmodel = gmodel;
		_validActions = validActions;
		_maxDepth = maxDepth;
		_discountFactor = discountFactor;
		_opType = opType;

		_knownPolicy = new HashMap<S, A>();
		_q = new ArrayList<DepthNode>();
		for (int d = 0; d < _maxDepth; d++) {
			_q.add(new DepthNode());
		}

		_numSamples = numSamples;
	}

	/**
	 * Determines whether this planner is maximizing reinforcements.
	 * 
	 * @return true if this planner maximizes reinforcements; otherwise false
	 */
	public boolean isMaximizing()
	{
		return _opType.equals(Optimization.MAXIMIZE);
	}

	/**
	 * Determines whether this planner is minimizing reinforcements.
	 * 
	 * @return true if this planner minimizes reinforcements; otherwise false
	 */
	public boolean isMinimizing()
	{
		return _opType.equals(Optimization.MINIMIZE);
	}

	/**
	 * Returns a reference to the generative model.
	 * 
	 * @return a reference to the generative model
	 */
	public GenerativeModel<S, A> generativeModel()
	{
		return _gmodel;
	}

	public Set<A> validActions()
	{
		return _validActions;
	}

	private A bestAction(S state)
	{
		boolean maximize = isMaximizing();
		double bestVal = Double.POSITIVE_INFINITY;
		if (maximize) {
			bestVal = Double.NEGATIVE_INFINITY;
		}
		A bestAction = null;

		for (A action : _validActions) {
			double val = actionValue(0, state, action);
			if (maximize) {
				if (val > bestVal) {
					bestVal = val;
					bestAction = action;
				}
			} else {
				if (val < bestVal) {
					bestVal = val;
					bestAction = action;
				}
			}
		}
		_knownPolicy.put(state, bestAction);
		return bestAction;
	}

	@Override
	public A policy(S state)
	{
		A action = _knownPolicy.get(state);
		if (action == null) {
			for(int s=0;s<_numSamples;s++){
				search(state, 0);
			}
			return bestAction(state);
		} else {
			return action;
		}
	}

	/**
	 * Performs Monte-Carlo search at a specified state and depth in the search
	 * tree.
	 * 
	 * @param state
	 *            a state
	 * @param depth
	 *            a depth in the tree
	 * @return an estimate of the state's value
	 */
	protected double search(S state, int depth)
	{
		if (depth == _maxDepth || terminateSearch(state, depth)) {
			return 0;
		}
		A action = selectAction(depth, state);
		Pair<S, Double> nextPair = _gmodel.simulate(state, action);
		double q = nextPair.getB() + _discountFactor
				* search(nextPair.getA(), depth + 1);
		update(depth, state, action, q);
		return q;
	}

	/**
	 * Determines whether or not to terminate the search process.
	 * 
	 * @param state
	 *            the current search state
	 * @param depth
	 * @return true to terminate; otherwise false
	 */
	public abstract boolean terminateSearch(S state, int depth);

	/**
	 * Selects an action to continue the Monte-Carlo search process.
	 * 
	 * @param depth
	 *            a depth of the tree
	 * @param state
	 *            a state
	 * 
	 * @return an action
	 */
	public abstract A selectAction(int depth, S state);

	/**
	 * Returns the estimated value of the specified state at a depth in the
	 * search tree.
	 * 
	 * @param depth
	 *            the depth of the search tree to return the value from
	 * @param state
	 *            the state to determine the value for
	 * @return the value of the specified state at the specified depth
	 */
	public double value(int depth, S state)
	{
		boolean maximize = isMaximizing();
		double bestValue = Double.POSITIVE_INFINITY;
		if (maximize) {
			bestValue = Double.NEGATIVE_INFINITY;
		}

		for (A action : _validActions) {
			double qval = actionValue(depth, state, action);
			if (maximize) {
				if (qval > bestValue) {
					bestValue = qval;
				}
			} else {
				if (qval < bestValue) {
					bestValue = qval;
				}
			}
		}
		return bestValue;
	}

	/**
	 * Returns the estimated action value at the specified depth.
	 * 
	 * @param depth
	 *            the depth of the search tree to return the value from
	 * @param state
	 *            the state to determine the value for
	 * @param action
	 *            the action used to select the appropriate state-action pair
	 * @return the action value for the specified state-action pair at the
	 *         specified depth
	 */
	public double actionValue(int depth, S state, A action)
	{
		return _q.get(depth).reward(state, action);
	}

	public void update(int depth, S state, A action, double value)
	{
		_q.get(depth).update(state, action, value);
	}

	/**
	 * Returns the number of visits made to a particular state and depth.
	 * 
	 * @param depth
	 *            a depth of the search tree
	 * @param state
	 *            a state
	 * @return the number of times the state has been visited at the specified
	 *         depth
	 */
	public int stateCount(int depth, S state)
	{
		return _q.get(depth).visitCount(state);
	}

	/**
	 * Returns the number of visits made to a particular state-action pair and
	 * depth.
	 * 
	 * @param depth
	 *            a depth of the search tree
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the number of times the state-action pair has been visited at the
	 *         specified depth
	 */
	public int stateActionCount(int depth, S state, A action)
	{
		return _q.get(depth).visitCount(state, action);
	}
}
