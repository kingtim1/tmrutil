package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcess.Estimator;
import tmrutil.stats.Statistics;
import tmrutil.util.Interval;

/**
 * An estimator for discrete Markov decision processes that refreshes its
 * estimates as new batches of samples become available.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RefreshEstimator extends Estimator
{
	private Interval _rewardInterval;

	private double[][] _rewardSums;
	private int[][] _saCounts;
	private int[][][] _sasCounts;

	private double[][] _rewardRefresh;
	private int[][] _saRefresh;
	private int[][][] _sasRefresh;

	private boolean _refreshSamples;

	private List<Set<Integer>> _succs;
	private List<Set<Integer>> _preds;

	public RefreshEstimator(int numStates, int numActions,
			Interval rewardInterval, double discountFactor,
			int numVisitsUntilKnown, boolean refreshSamples)
	{
		this(numStates, numActions, rewardInterval, discountFactor,
				Estimator.buildNumVisitsArray(numStates, numActions,
						numVisitsUntilKnown), refreshSamples);
	}

	public RefreshEstimator(int numStates, int numActions,
			Interval rewardInterval, double discountFactor,
			int[][] numVisitsUntilKnown, boolean refreshSamples)
	{
		super(numStates, numActions, discountFactor, numVisitsUntilKnown);
		_rewardInterval = rewardInterval;
		_refreshSamples = refreshSamples;

		reset();
	}

	@Override
	public Set<Integer> successors(Integer state)
	{
		return _succs.get(state);
	}

	@Override
	public Set<Integer> predecessors(Integer state)
	{
		return _preds.get(state);
	}

	@Override
	public double rmax()
	{
		return _rewardInterval.getMax();
	}

	@Override
	public double rmin()
	{
		return _rewardInterval.getMin();
	}

	@Override
	public double reinforcement(Integer state, Integer action)
	{
		if (isKnown(state, action)) {
			if (_saCounts[state][action] > 0) {
				return _rewardSums[state][action] / _saCounts[state][action];
			} else {
				return rmin();
			}
		} else {
			return rmax();
		}
	}

	@Override
	public boolean update(Integer state, Integer action, Integer resultState,
			double reinforcement)
	{
		boolean update = false;
		boolean known = isKnown(state, action);
		if (!known || _refreshSamples) {

			_rewardRefresh[state][action] += reinforcement;
			_saRefresh[state][action]++;
			_sasRefresh[state][action][resultState]++;

			_succs.get(state).add(resultState);
			_preds.get(resultState).add(state);

			if (_saRefresh[state][action] >= visitsUntilKnown(state, action)) {
				_rewardSums[state][action] = _rewardRefresh[state][action];
				_saCounts[state][action] = _saRefresh[state][action];

				_rewardRefresh[state][action] = 0;
				_saRefresh[state][action] = 0;

				for (int rs = 0; rs < numberOfStates(); rs++) {
					if (_sasCounts[state][action][rs] != _sasRefresh[state][action][rs]) {
						_sasCounts[state][action][rs] = _sasRefresh[state][action][rs];
						update = true;
					}
					_sasRefresh[state][action][rs] = 0;
				}
			}
		}

		return update;
	}

	@Override
	public double transitionProbUnknown(Integer state, Integer action,
			Integer resultState)
	{
		if (state.equals(resultState)) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public int getCount(Integer state)
	{
		return Statistics.sum(_saCounts[state]);
	}

	@Override
	public int getCount(Integer state, Integer action)
	{
		return _saCounts[state][action];
	}

	@Override
	public int getCount(Integer state, Integer action, Integer resultState)
	{
		return _sasCounts[state][action][resultState];
	}

	@Override
	public void reset()
	{
		int numStates = numberOfStates();
		int numActions = numberOfActions();

		_rewardSums = new double[numStates][numActions];
		_saCounts = new int[numStates][numActions];
		_sasCounts = new int[numStates][numActions][numStates];

		_rewardRefresh = new double[numStates][numActions];
		_saRefresh = new int[numStates][numActions];
		_sasRefresh = new int[numStates][numActions][numStates];

		_succs = new ArrayList<Set<Integer>>(numStates);
		_preds = new ArrayList<Set<Integer>>(numStates);
		for (int s = 0; s < numStates; s++) {
			Set<Integer> succs = new HashSet<Integer>();
			succs.add(s);
			Set<Integer> preds = new HashSet<Integer>();
			preds.add(s);
			_succs.add(succs);
			_preds.add(preds);
		}
	}

}
