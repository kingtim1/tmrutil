package tmrutil.learning.rl;

/**
 * Represents a single reinforcement learning sample.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class RLInstance<S, A> {
	private S _state;
	private A _action;
	private S _nextState;
	private double _reinforcement;
	private int _duration;
	private boolean _terminal;
	
	/**
	 * Constructs an instance.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param nextState
	 *            the state transitioned to after executing
	 *            <code>(state, action)</code>
	 * @param reinforcement
	 *            the reinforcement received for executing
	 *            <code>(state, action)</code>
	 */
	public RLInstance(S state, A action, S nextState, double reinforcement)
	{
		this(state, action, nextState, reinforcement, 1);
	}
	
	/**
	 * Constructs an instance.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param nextState
	 *            the state transitioned to after executing
	 *            <code>(state, action)</code>
	 * @param reinforcement
	 *            the reinforcement received for executing
	 *            <code>(state, action)</code>
	 * @param duration
	 *            the number of timesteps that the action executed for (
	 *            <code>executionTime == 1</code> for primitive actions)
	 */
	public RLInstance(S state, A action, S nextState, double reinforcement, int duration){
		this(state, action, nextState, reinforcement, duration, false);
	}

	/**
	 * Constructs an instance.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param nextState
	 *            the state transitioned to after executing
	 *            <code>(state, action)</code>
	 * @param reinforcement
	 *            the reinforcement received for executing
	 *            <code>(state, action)</code>
	 * @param duration
	 *            the number of timesteps that the action executed for (
	 *            <code>executionTime == 1</code> for primitive actions)
	 * @param terminal true if this sample corresponds to the end of an episode
	 */
	public RLInstance(S state, A action, S nextState, double reinforcement,
			int duration, boolean terminal) {
		_state = state;
		_action = action;
		_nextState = nextState;
		_reinforcement = reinforcement;
		_duration = duration;
		_terminal = terminal;
	}

	/**
	 * Return the state.
	 * 
	 * @return the state
	 */
	public S state() {
		return _state;
	}

	/**
	 * Return the action.
	 * 
	 * @return the action
	 */
	public A action() {
		return _action;
	}

	/**
	 * Return the next state.
	 * 
	 * @return the next state
	 */
	public S nextState() {
		return _nextState;
	}

	/**
	 * Return the reinforcement.
	 * 
	 * @return the reinforcement
	 */
	public double reinforcement() {
		return _reinforcement;
	}

	/**
	 * Return the number of timesteps that the action executed for. If the
	 * action is a primitive action, then the execution time is a 1.
	 * 
	 * @return the number of timesteps that the action executed for
	 */
	public int duration() {
		return _duration;
	}
	
	/**
	 * Return true if this instance occurred at the end of an episode.
	 * @return true if this instance is the end of an episode; otherwise false
	 */
	public boolean terminal(){
		return _terminal;
	}
}
