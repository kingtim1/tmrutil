package tmrutil.learning.rl;

import java.util.Arrays;

import tmrutil.learning.rl.SelectiveOptimismMDPEstimator.CloseDetector;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Filter;
import tmrutil.util.DataStore;
import tmrutil.util.Interval;

/**
 * An implementation of model-based reinforcement learning for discrete
 * state-action spaces that exploits the optimism in the face of uncertainty
 * heuristic for a subset of total states determined relevant by a domain
 * specific filter. This algorithm begins with an inherent transition model
 * based on the assumption that state transitions are only nonzero to nearby
 * states, where nearby states are defined by a domain specific filter.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DiscreteModelBasedLearning extends
		AbstractTemporalDifferenceLearning<Integer, Integer>
{
	private static final double ENQUEUE_THRESHOLD = 0.05;
	private static final int MAX_VALUE_ITERATIONS_PER_UPDATE = 10;

	/** The possible reward interval. */
	private Interval _rewardInterval;

	/**
	 * A reference to the prioritized sweeping instance used to approximate the
	 * value function.
	 */
	//private PrioritizedSweeping _viter;
	private ValueIteration _viter;

	private double[] _V;
	private SelectiveOptimismMDPEstimator _mdp;

	public DiscreteModelBasedLearning(int numStates, int numActions,
			Interval rewardInterval, Filter<Integer> relevanceFilter,
			CloseDetector closeDetector, int visitsUntilKnown, double learningRate, double discountFactor)
	{
		super(learningRate, discountFactor, Optimization.MAXIMIZE);

		if (numStates <= 0) {
			throw new IllegalArgumentException(
					"The number of states must be a positive integer.");
		}
		if (numActions <= 0) {
			throw new IllegalArgumentException(
					"The number of actions must be a positive integer.");
		}
		_mdp = new SelectiveOptimismMDPEstimator(numStates, numActions, discountFactor, visitsUntilKnown,
				relevanceFilter, closeDetector, rewardInterval);

		_V = new double[numStates];

		_rewardInterval = rewardInterval;

		//_viter = new PrioritizedSweeping(_mdp, ENQUEUE_THRESHOLD);
		_viter = new ValueIteration(_mdp, ENQUEUE_THRESHOLD);

		reset();
	}

	@Override
	protected void resetImpl()
	{
		_mdp.reset();

		for (int s1 = 0; s1 < numberOfStates(); s1++) {
			_V[s1] = _rewardInterval.getMin();
		}
	}

	@Override
	protected void trainImpl(Integer prevState, Integer action,
			Integer newState, double reinforcement)
	{
		// Update the MDP model
		boolean update = _mdp.update(prevState, action, newState, reinforcement);

		if(update){
			// Update with value iteration
			updateVFunc(prevState, action, newState);
		}

	}

	/**
	 * Update the value function using the value iteration algorithm.
	 */
	private void updateVFunc(Integer prevState, Integer action,
			Integer resultState)
	{
		_V = _viter.valueIteration(MAX_VALUE_ITERATIONS_PER_UPDATE, _V);
		
		//_viter.enqueue(resultState, PrioritizedSweeping.MAX_PRIORITY);
		//_V = _viter.valueSweep(MAX_VALUE_ITERATIONS_PER_UPDATE, _V);
	}


	public double evaluateQ(Integer state, Integer action)
	{
		if (_viter == null) {
			return 0.0;
		} else {
			double r = 0;
			double newV = 0;
			for (int ns = 0; ns < numberOfStates(); ns++) {
				double tprob = _mdp.transitionProb(state, action, ns);
				r += tprob * _mdp.reinforcement(state, action);
				newV += tprob * _V[ns];
			}
			return r + getDiscountFactor() * newV;
		}
	}

	public double evaluateV(Integer state)
	{
		return _V[state];
	}

	public double[] getValueFunction()
	{
		return Arrays.copyOf(_V, _V.length);
	}

	public double[] getStateRewards()
	{
		double[] stateRewards = new double[numberOfStates()];
		for (int s1 = 0; s1 < numberOfStates(); s1++) {
			for (int a = 0; a < numberOfActions(); a++) {
				for (int s2 = 0; s2 < numberOfStates(); s2++) {
					stateRewards[s2] += _mdp.transitionProb(s1, a, s2)
							* _mdp.reinforcement(s1, a);
				}
			}
		}
		return stateRewards;
	}

	public double[] getStateVisitCount()
	{
		double[] stateVisits = new double[numberOfStates()];
		for (int s1 = 0; s1 < numberOfStates(); s1++) {
			stateVisits[s1] = _mdp.getCount(s1);
		}
		return stateVisits;
	}

	@Override
	public Integer policy(Integer state)
	{
		int bestAct = 0;
		double bestVal = 0;
		int[] actions = VectorOps.randperm(numberOfActions());
		for (int a = 0; a < numberOfActions(); a++) {
			double val = evaluateQ(state, actions[a]);
			if (a == 0 || bestVal < val) {
				bestVal = val;
				bestAct = actions[a];
			}
		}
		return bestAct;
	}

	public boolean isKnown(Integer state)
	{
		for (int a = 0; a < numberOfActions(); a++) {
			if (isKnown(state, a)) {
				return true;
			}
		}
		return false;
	}

	public boolean isKnown(Integer state, Integer action)
	{
		return _mdp.getCount(state, action) >= _mdp.visitsUntilKnown(state, action);
	}

	public int numberOfKnownPairs()
	{
		int count = 0;
		for (int s = 0; s < numberOfStates(); s++) {
			for (int a = 0; a < numberOfActions(); a++) {
				if (isKnown(s, a)) {
					count++;
				}
			}
		}
		return count;
	}

	public int numberOfStates()
	{
		return _mdp.numberOfStates();
	}

	public int numberOfActions()
	{
		return _mdp.numberOfActions();
	}

	/**
	 * Returns the minimum possible reward.
	 * 
	 * @return minimum reward
	 */
	public double rmin()
	{
		return _rewardInterval.getMin();
	}

	/**
	 * Returns the maximum possible reward.
	 * 
	 * @return maximum reward
	 */
	public double rmax()
	{
		return _rewardInterval.getMax();
	}

	public void displayUniversalActionModel()
	{
		_mdp.displayUniversalActionModel();
	}

	public double maxQ(Integer state)
	{
		return _V[state];
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<Integer> greedyValueFunction() {
		return new QFunction.GreedyVFunction<Integer>(this);
	}

	@Override
	public double value(Integer state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(Integer state) {
		return maxQ(state);
	}

}
