package tmrutil.learning.rl;

/**
 * An interface for a control policy.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state description type
 * @param <A>
 *            the action encoding type
 * 
 */
public interface Policy<S, A>
{
	/**
	 * Selects an action given the current state <code>state</code> according to
	 * this policies distribution.
	 * 
	 * @param state
	 *            the current state
	 * @return an action
	 */
	public A policy(S state);

	/*
	/**
	 * Returns true to indicate that this policy is a deterministic mapping from
	 * states to actions. Otherwise false is returned indicating that this
	 * policy may return different actions for the same state.
	 * 
	 * @return true if this is a deterministic policy; otherwise false
	 */
	//public boolean isDeterministic();

	/*
	/**
	 * Determines the probability of selecting action <code>action</code>
	 * according to this policy given that the current state is
	 * <code>state</code>.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the probability of selecting action <code>action</code> given the
	 *         specified state
	 */
	//public double actionProb(S state, A action);

}
