package tmrutil.learning.rl.sim;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.RandomLearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.learning.rl.smdp.PrimitiveAction;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;

/**
 * The bottleneck simulator simulates a continuous environment with noisy
 * actions where two rooms are separated by a wall. In the middle of the wall,
 * there is a gap that the agent can pass through. To complete the task the
 * agent must travel from the first room through the gap in the wall to the goal
 * region.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BottleneckSimulator extends Simulator<double[], double[], Integer> {

	public static final double WIDTH = 1.0;
	public static final double HEIGHT = 1.0;
	public static final Rectangle2D ENV_REGION = new Rectangle2D.Double(0, 0, WIDTH, HEIGHT);
	public static final Rectangle2D INIT_REGION = new Rectangle2D.Double(0, 0, 0.1 * WIDTH, 0.1 * HEIGHT);
	public static final Rectangle2D GOAL_REGION = new Rectangle2D.Double(0.9 * WIDTH, 0.9 * HEIGHT, 0.1 * WIDTH, 0.1 * HEIGHT);

	public static final double WALL_XPOS = 0.5;
	
	public static final double MEAN_PRIM_ACTION_DIST = 0.05;
	public static final double SIGMA_PRIM_ACTION = 0.025;
	
	public static final int NORTH = 0;
	public static final int SOUTH = 1;
	public static final int EAST = 2;
	public static final int WEST = 3;
	public static final int NUM_ACTIONS = 4;

	
	private double _actionMean;
	private double _actionSigma;
	
	private double _wallGapSize;
	private Line2D _lowerWall;
	private Line2D _upperWall;

	public BottleneckSimulator(double wallGapSize)
	{
		this(wallGapSize, MEAN_PRIM_ACTION_DIST, SIGMA_PRIM_ACTION);
	}
	
	public BottleneckSimulator(double wallGapSize, double actionMean, double actionSigma) {
		wallGapSize(wallGapSize);
		_actionMean = actionMean;
		_actionSigma = actionSigma;
	}
	
	public BottleneckSimulator deterministicCopy()
	{
		return new BottleneckSimulator(_wallGapSize, _actionMean, 0);
	}

	public double wallGapSize() {
		return _wallGapSize;
	}
	
	public double meanPrimitiveActionDistance()
	{
		return _actionMean;
	}
	
	public double primitiveActionStandardDeviation()
	{
		return _actionSigma;
	}

	public void wallGapSize(double gapSize) {
		if (gapSize <= 0 || gapSize > HEIGHT) {
			throw new IllegalArgumentException(
					"Wall gap size must be positive, but less than the bottleneck rooms height (" + HEIGHT + ").");
		}
		_wallGapSize = gapSize;
		
		_lowerWall = new Line2D.Double(WALL_XPOS, 0, WALL_XPOS, HEIGHT/2 - _wallGapSize/2);
		_upperWall = new Line2D.Double(WALL_XPOS, HEIGHT, WALL_XPOS, HEIGHT/2 + _wallGapSize/2);
	}
	
	public List<Integer> validActions()
	{
		List<Integer> actions = new ArrayList<Integer>();
		actions.add(NORTH);
		actions.add(SOUTH);
		actions.add(EAST);
		actions.add(WEST);
		
		return actions;
	}

	@Override
	public double[] observe(Integer previousAction, double[] resultState) {
		return resultState;
	}

	@Override
	public OptionOutcome<double[], double[]> samplePrimitive(double[] state,
			Integer action) {
		
		double[] newState = null;
		switch(action){
		case NORTH:
			newState = sampleNorth(state);
			break;
		case SOUTH:
			newState = sampleSouth(state);
			break;
		case EAST:
			newState = sampleEast(state);
			break;
		case WEST:
			newState = sampleWest(state);
			break;
		}
		
		Line2D path = new Line2D.Double(state[0], state[1], newState[0], newState[1]);
		if(!ENV_REGION.contains(path.getP2()) || path.intersectsLine(_lowerWall) || path.intersectsLine(_upperWall)){
			System.arraycopy(state, 0, newState, 0, state.length);
		}
		
		double reward = 0;
		if(GOAL_REGION.contains(path.getP2())){
			reward = 1;
		}
		
		return new OptionOutcome<double[],double[]>(newState, newState, reward, reward, 1);
	}
	
	private double[] sampleNorth(double[] state)
	{
		double mux = Random.normal(state[0], _actionSigma);
		double muy = Random.normal(state[1]+_actionMean, _actionSigma);
		return new double[]{mux, muy};
	}

	private double[] sampleSouth(double[] state){
		double mux = Random.normal(state[0], _actionSigma);
		double muy = Random.normal(state[1]-_actionMean, _actionSigma);
		return new double[]{mux, muy};
	}
	
	private double[] sampleEast(double[] state){
		double mux = Random.normal(state[0]+_actionMean, _actionSigma);
		double muy = Random.normal(state[1], _actionSigma);
		return new double[]{mux, muy};
	}
	
	private double[] sampleWest(double[] state){
		double mux = Random.normal(state[0]-_actionMean, _actionSigma);
		double muy = Random.normal(state[1], _actionSigma);
		return new double[]{mux, muy};
	}
	
	public List<Integer> primitiveActions()
	{
		return validActions();
	}
	
	public List<Option<double[],Integer>> primitiveActionsAsOptions()
	{
		List<Option<double[],Integer>> prims = new ArrayList<Option<double[],Integer>>();
		
		for(Integer action : primitiveActions()){
			prims.add(new PrimitiveAction<double[],Integer>(action, action));
		}
		
		return prims;
	}
	
	public List<Option<double[],Integer>> primitivesPlusGapOption()
	{
		List<Option<double[],Integer>> aug = primitiveActionsAsOptions();
		
		aug.add(new ToGapOption(_wallGapSize));
		
		return aug;
	}
	
	public List<Option<double[],Integer>> primitivesPlusGoalOption()
	{
		List<Option<double[],Integer>> aug = primitiveActionsAsOptions();
		
		aug.add(new ToAreaOption(GOAL_REGION, new Rectangle2D.Double(WIDTH/2, 0, WIDTH/2, HEIGHT)));
		
		return aug;
	}
	
	public List<Option<double[],Integer>> primitivesPlusGapAndGoalOptions()
	{
		List<Option<double[],Integer>> aug = primitiveActionsAsOptions();
		
		aug.add(new ToGapOption(_wallGapSize));
		aug.add(new ToAreaOption(GOAL_REGION, new Rectangle2D.Double(WIDTH/2, 0, WIDTH/2, HEIGHT)));
		
		return aug;
	}
	
	public static class ToAreaOption implements Option<double[],Integer>
	{
		private Rectangle2D _area;
		private Rectangle2D _init;
		private double _actionMean;
		
		public ToAreaOption(Rectangle2D target, Rectangle2D init)
		{
			this(target, init, MEAN_PRIM_ACTION_DIST);
		}
		
		public ToAreaOption(Rectangle2D target, Rectangle2D init, double actionMean)
		{
			_area = target;
			_init = init;
			_actionMean = actionMean;
		}

		@Override
		public Integer policy(double[] state) {
			Point2D center = new Point2D.Double(_area.getCenterX(), _area.getCenterY());
			
			double[] dists = new double[NUM_ACTIONS];
			dists[NORTH] = center.distanceSq(state[0], state[1]+_actionMean);
			dists[SOUTH] = center.distanceSq(state[0], state[1]-_actionMean);
			dists[EAST] = center.distanceSq(state[0]+_actionMean, state[1]);
			dists[WEST] = center.distanceSq(state[0]-_actionMean, state[1]);
			
			return Statistics.minIndex(dists);
		}

		@Override
		public double terminationProb(double[] state, int duration) {
			Point2D p = new Point2D.Double(state[0], state[1]);
			if(_area.contains(p)){
				return 1;
			}else{
				return 0;
			}
		}

		@Override
		public boolean inInitialSet(double[] state) {
			Point2D p = new Point2D.Double(state[0], state[1]);
			return _init.contains(p) && !_area.contains(p);
		}
		
	}
	
	public static class ToGapOption extends ToAreaOption
	{
		
		public ToGapOption(double wallGapSize)
		{
			super(gapRegion(wallGapSize), ENV_REGION);
		}
		
		public static Rectangle2D gapRegion(double wallGapSize)
		{
			double x = WALL_XPOS - wallGapSize/2;
			double y = HEIGHT / 2 - wallGapSize/2;
			return new Rectangle2D.Double(x, y, wallGapSize, wallGapSize);
		}		
	}
	
	public static class BottleneckTask extends Task<double[],Integer> implements Drawable
	{
		private double[] _state;
		private double _lastReward;
		
		private BottleneckSimulator _sim;
		
		public BottleneckTask(double wallGapSize)
		{
			_sim = new BottleneckSimulator(wallGapSize);
			_state = sampleInitState();
			_lastReward = 0;
		}
		
		private double[] sampleInitState()
		{
			double x = Random.uniform(INIT_REGION.getX(),INIT_REGION.getX() + INIT_REGION.getWidth());
			double y = Random.uniform(INIT_REGION.getY(),INIT_REGION.getY() + INIT_REGION.getHeight());
			return new double[]{x,y};
		}

		@Override
		public double[] getState() {
			return _state;
		}

		@Override
		public void execute(Integer action) {
			OptionOutcome<double[],double[]> oc = _sim.samplePrimitive(_state, action);
			_state = oc.terminalState();
			_lastReward = oc.dCumR();
		}

		@Override
		public double evaluate() {
			return _lastReward;
		}

		@Override
		public void reset() {
			_state = sampleInitState();
			_lastReward = 0;
		}

		@Override
		public boolean isFinished() {
			return false;
		}

		@Override
		public void draw(Graphics2D g, int width, int height) {
			AffineTransform xform = new AffineTransform();
			xform.translate(0, height);
			xform.scale(width, -height);
			g.transform(xform);
			g.setStroke(new BasicStroke(1/Math.max(width, height)));
			
			g.setColor(Color.BLACK);
			g.draw(ENV_REGION);
			g.draw(_sim._lowerWall);
			g.draw(_sim._upperWall);
			
			g.setColor(Color.BLUE);
			g.fill(INIT_REGION);
			g.setColor(Color.GREEN);
			g.fill(GOAL_REGION);
			
			g.setColor(Color.RED);
			double awidth = SIGMA_PRIM_ACTION;
			double halfAWidth = awidth/2;
			double cx = _state[0];
			double cy = _state[1];
			Ellipse2D agent = new Ellipse2D.Double(cx - halfAWidth, cy - halfAWidth, awidth, awidth);
			g.fill(agent);
		}

		public List<Integer> validActions()
		{
			return _sim.validActions();
		}
	}
	
	public static void main(String[] args){
		double wallGapSize = 0.08;
		BottleneckTask task = new BottleneckTask(wallGapSize);
		RandomLearningSystem<double[],Integer> agent = new RandomLearningSystem<double[],Integer>(task.validActions());
		
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(600, 400);
		Task.runTask(task, agent, 100, 1000, false, null, frame);
	}
}
