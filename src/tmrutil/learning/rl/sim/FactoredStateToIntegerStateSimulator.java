package tmrutil.learning.rl.sim;

import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.util.Mapping;

/**
 * A wrapper for simulators that use factored states to simulators that use
 * integer states.
 * 
 * @author Timothy A. Mann
 * 
 */
public class FactoredStateToIntegerStateSimulator extends
		Simulator<Integer, Integer, Integer> {
	private Simulator<FactoredState, ?, Integer> _sim;
	private Mapping<Integer, FactoredState> _translator;

	public FactoredStateToIntegerStateSimulator(
			Simulator<FactoredState, ?, Integer> sim,
			Mapping<Integer, FactoredState> translator) {
		_sim = sim;
		_translator = translator;
	}

	@Override
	public Integer observe(Integer previousAction, Integer resultState) {
		return resultState;
	}

	@Override
	public OptionOutcome<Integer, Integer> samplePrimitive(Integer state,
			Integer action) {

		FactoredState fstate = _translator.map(state);
		OptionOutcome<FactoredState, ?> foc = _sim.samplePrimitive(fstate,
				action);

		FactoredState ftstate = foc.terminalState();
		Integer tstate = ftstate.uniqueID();
		double reward = foc.dCumR();

		return new OptionOutcome<Integer, Integer>(tstate, tstate, reward,
				reward, 1);
	}

}
