package tmrutil.learning.rl.sim;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.RandomLearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.factored.FactoredAction;
import tmrutil.learning.rl.factored.FactoredActionImpl;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.learning.rl.smdp.PrimitiveAction;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.util.Pair;

/**
 * Simulator for a simple matching game.
 * 
 * @author Timothy A. Mann
 * 
 */
public class TriplesSimulator extends
		Simulator<FactoredState, FactoredState, FactoredAction> {

	public static final int ACTION_ROW_INDEX = 0;
	public static final int ACTION_COL_INDEX = 1;

	public static final int EMPTY_WTYPE = 0;
	public static final int BASE_SCORE = 1;
	public static final int MIN_TILES_TO_COMBINE = 3;

	private int _numRows;
	private int _numCols;

	private double[] _widgetProbs;

	private int[] _maxValues;

	public TriplesSimulator(int numRows, int numCols, double[] widgetProbs) {
		_numRows = numRows;
		_numCols = numCols;
		_widgetProbs = Arrays.copyOf(widgetProbs, widgetProbs.length);

		_maxValues = new int[numRows * numCols + 1];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				_maxValues[r * numCols + c] = widgetProbs.length;
			}
		}
		_maxValues[numRows * numCols] = widgetProbs.length;
	}

	public int numberOfRows() {
		return _numRows;
	}

	public int numberOfColumns() {
		return _numCols;
	}

	@Override
	public FactoredState observe(FactoredAction previousAction,
			FactoredState resultState) {
		return resultState;
	}

	@Override
	public OptionOutcome<FactoredState, FactoredState> samplePrimitive(
			FactoredState state, FactoredAction action) {
		int row = action.component(ACTION_ROW_INDEX);
		int col = action.component(ACTION_COL_INDEX);
		Point actionPoint = new Point(row, col);

		Pair<FactoredState, Double> pair = applyAction(state, actionPoint);

		OptionOutcome<FactoredState, FactoredState> oc = new OptionOutcome<FactoredState, FactoredState>(
				pair.getA(), pair.getA(), pair.getB(), pair.getB(), 1);
		return oc;
	}

	public Pair<FactoredState, Double> applyAction(FactoredState state,
			Point actionPoint) {
		Map<Integer, Integer> combinedCounts = new HashMap<Integer, Integer>();

		int[] newStateA = new int[_numRows * _numCols + 1];
		System.arraycopy(state.toArray(), 0, newStateA, 0, newStateA.length);

		int wtype = state.component(_numRows * _numCols);

		if (widgetType(newStateA, actionPoint) == EMPTY_WTYPE) {

			List<Point> ctiles = findConnectedTiles(newStateA, actionPoint,
					wtype);
			if (ctiles.size() < MIN_TILES_TO_COMBINE) {
				newStateA[pointToIndex(actionPoint)] = wtype;
			} else {
				while (ctiles.size() >= MIN_TILES_TO_COMBINE) {

					if (wtype < _widgetProbs.length) {
						combinedCounts.put(wtype, ctiles.size());
						for (Point p : ctiles) {
							newStateA[pointToIndex(p)] = EMPTY_WTYPE;
						}
						wtype = wtype + 1;
						newStateA[pointToIndex(actionPoint)] = wtype;

						ctiles = findConnectedTiles(newStateA, actionPoint,
								wtype);

					} else {
						newStateA[pointToIndex(actionPoint)] = wtype;
						break;
					}
				}
			}

		}

		newStateA[newStateA.length - 1] = sampleWidgetType();
		FactoredState newState = new FactoredStateImpl(newStateA, _maxValues);

		double reward = 0;
		for (Integer w : combinedCounts.keySet()) {
			reward += reward(combinedCounts.get(w), w);
		}
		return new Pair<FactoredState, Double>(newState, reward);
	}

	public int pointToIndex(Point p) {
		return p.x * _numCols + p.y;
	}

	public boolean outOfBounds(Point p) {
		return (p.x < 0 || p.x >= _numRows) || (p.y < 0 || p.y >= _numCols);
	}

	public boolean inBounds(Point p) {
		return !outOfBounds(p);
	}

	private int widgetType(int[] state, Point p) {
		return state[pointToIndex(p)];
	}

	public int maxWidgetType() {
		return _widgetProbs.length;
	}

	public boolean isGameOver(FactoredState state) {
		boolean openStates = false;
		for (int r = 0; r < _numRows; r++) {
			for (int c = 0; c < _numCols; c++) {
				Point p = new Point(r, c);
				if (state.component(pointToIndex(p)) == EMPTY_WTYPE) {
					openStates = true;
				}
			}
		}
		return !openStates;
	}

	private List<Point> findConnectedTiles(int[] state, Point actionPoint,
			int widgetType) {
		List<Point> ctiles = new ArrayList<Point>();
		List<Point> unchecked = new ArrayList<Point>();
		ctiles.add(actionPoint);
		unchecked.add(actionPoint);

		while (!unchecked.isEmpty()) {
			Point p = unchecked.remove(0);
			List<Point> aps = new ArrayList<Point>(4);
			aps.add(new Point(p.x-1, p.y));
			aps.add(new Point(p.x+1, p.y));
			aps.add(new Point(p.x, p.y-1));
			aps.add(new Point(p.x, p.y+1));
			
			for(Point ap : aps){
				if(inBounds(ap) && widgetType(state, ap) == widgetType && !ctiles.contains(ap)){
					ctiles.add(ap);
					unchecked.add(ap);
				}
			}
		}

		return ctiles;
	}

	public int reward(int numberCombined, int widgetType) {
		return widgetType * BASE_SCORE * numberCombined;
	}

	public FactoredState emptyState() {
		int[] empty = new int[_numRows * _numCols + 1];
		empty[empty.length - 1] = sampleWidgetType();
		return new FactoredStateImpl(empty, _maxValues);
	}

	public int sampleWidgetType() {
		double sum = VectorOps.sum(_widgetProbs);
		double r = Random.uniform(0, sum);
		double rsum = 0;
		for (int i = 0; i < _widgetProbs.length; i++) {
			rsum += _widgetProbs[i];
			if (r < rsum) {
				return i + 1;
			}
		}
		return 1;
	}

	public List<FactoredAction> validActions() {
		List<FactoredAction> actions = new ArrayList<FactoredAction>();

		int[] maxValues = { _numRows - 1, _numCols - 1 };

		for (int r = 0; r < _numRows; r++) {
			for (int c = 0; c < _numCols; c++) {
				int[] v = { r, c };
				actions.add(new FactoredActionImpl(v, maxValues));
			}
		}

		return actions;
	}
	
	public List<FactoredAction> validActions(FactoredState state) {
		List<FactoredAction> actions = new ArrayList<FactoredAction>();
		int[] maxValues = {_numRows - 1, _numCols - 1};
		for(int r=0;r<_numRows;r++){
			for(int c=0;c<_numCols;c++){
				Point p = new Point(r, c);
				if(state.component(pointToIndex(p)) == EMPTY_WTYPE){
					int[] v = { r, c };
					actions.add(new FactoredActionImpl(v, maxValues));
				}
			}
		}
		return actions;
	}

	public ActionSet<FactoredState, Option<FactoredState,FactoredAction>> primitiveActions() {
		return new ActionSet<FactoredState, Option<FactoredState,FactoredAction>>() {

			@Override
			public List<Option<FactoredState, FactoredAction>> validList(
					FactoredState state) {
				List<Option<FactoredState, FactoredAction>> options =new ArrayList<Option<FactoredState,FactoredAction>>();
				List<FactoredAction> actions = validActions(state);
				
				for(FactoredAction fa : actions){
					options.add(new PrimitiveAction<FactoredState,FactoredAction>(fa.uniqueID(), fa));
				}
				
				return options;
			}

			@Override
			public List<Integer> validIndices(FactoredState state) {
				List<Integer> indices = new ArrayList<Integer>();
				List<FactoredAction> actions = validActions(state);
				
				for(FactoredAction fa : actions){
					indices.add( fa.uniqueID());
				}
				return indices;
			}

			@Override
			public Option<FactoredState, FactoredAction> action(int index) {
				int[] maxValues = {_numRows - 1, _numCols - 1};
				int[] vals = {index / _numCols, index % _numCols};
				FactoredAction fa = new FactoredActionImpl(vals, maxValues);
				return new PrimitiveAction<FactoredState,FactoredAction>(fa.uniqueID(),fa);
			}

			@Override
			public int numberOfActions() {
				return _numRows * _numCols;
			}

		};
	}

	public static class TriplesTask extends Task<FactoredState, FactoredAction>
			implements Drawable {
		private TriplesSimulator _sim;
		private FactoredState _state;
		private double _lastScore;

		public TriplesTask() {
			super();

			_sim = new TriplesSimulator(8, 8, new double[] { 0.7, 0.23, 0.04,
					0.02, 0.01 });
			_state = _sim.emptyState();
			_lastScore = 0;
		}

		public TriplesSimulator simulator() {
			return _sim;
		}

		@Override
		public FactoredState getState() {
			return _state;
		}

		@Override
		public void execute(FactoredAction action) {
			OptionOutcome<FactoredState, FactoredState> oc = _sim
					.samplePrimitive(_state, action);

			_state = oc.terminalState();
			_lastScore = oc.dCumR();
		}

		@Override
		public double evaluate() {
			return _lastScore;
		}

		@Override
		public void reset() {
			_state = _sim.emptyState();
			_lastScore = 0;
		}

		@Override
		public boolean isFinished() {
			return _sim.isGameOver(_state);
		}

		@Override
		public void draw(Graphics2D g, int width, int height) {
			g.setColor(Color.BLACK);

			int cwidth = width / (_sim.numberOfColumns() + 1);
			int cheight = height / (_sim.numberOfRows());

			for (int r = 0; r < _sim.numberOfRows(); r++) {
				for (int c = 0; c < _sim.numberOfColumns(); c++) {
					int wtype = _state.component(_sim.pointToIndex(new Point(r,
							c)));

					Rectangle2D rect = new Rectangle2D.Double(cwidth * c,
							cheight * r, cwidth, cheight);
					g.draw(rect);

					String wtypeStr = String.valueOf(wtype);
					g.drawString(wtypeStr, cwidth * c + cwidth / 2, cheight * r
							+ cheight / 2);
				}
			}

			String wtypeStr = String
					.valueOf(_state.component(_state.size() - 1));
			g.drawString(wtypeStr,
					cwidth * _sim.numberOfColumns() + cwidth / 2, cheight / 2);

			String rewardStr = String.valueOf(_lastScore);
			g.setColor(Color.RED);
			g.drawString(rewardStr, cwidth * _sim.numberOfColumns() + cwidth
					/ 2, cheight + cheight / 2);
		}

		public List<FactoredAction> validActions() {
			return _sim.validActions();
		}

	}

	public static void main(String[] args) {
		TriplesTask task = new TriplesTask();
		List<FactoredAction> validActions = task.validActions();
		RandomLearningSystem<FactoredState, FactoredAction> agent = new RandomLearningSystem<FactoredState, FactoredAction>(
				validActions);

		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(400, 450);
		Task.runTask(task, agent, 100, 10000, false, null, frame);
	}
}
