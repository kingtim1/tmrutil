package tmrutil.learning.rl.sim;

import java.util.Random;

import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.Rollout;
import tmrutil.learning.rl.RolloutImpl;
import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.OptionOutcome;

/**
 * A simulator interface for planning with a generative model of a Markov
 * Decision Process or a Partially Observable Markov Decision Process.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <O>
 *            the observation type
 * @param <A>
 *            the primitive action type
 */
public abstract class Simulator<S, O, A> {

	private Random _rand;

	public Simulator() {
		_rand = new Random();
	}

	/**
	 * Samples an observation based on the previously executed action and the
	 * state that resulted from the transition.
	 * 
	 * @param previousAction
	 *            the previously executed action
	 * @param resultState
	 *            the state that the agent transitioned to
	 * @return an observation
	 */
	public abstract O observe(A previousAction, S resultState);

	/**
	 * Samples the simulator for the outcome of executing a primitive action at
	 * a specific observation.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            the action to execute
	 * @return the outcome of executing the specified action
	 */
	public abstract OptionOutcome<S, O> samplePrimitive(S state, A action);

	/**
	 * Samples the simulator for the outcome of executing an ``option'' over MDP
	 * states at the specific observation.
	 * 
	 * @param state
	 *            a state
	 * @param option
	 *            the option to execute
	 * @param discountFactor
	 *            the discount factor used while accumulating reinforcements
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps to execute the option for
	 * @return the outcome of executing the specified option
	 */
	public final OptionOutcome<S, O> sample(S state, Option<S, A> option,
			double discountFactor, int maxTrajectoryLength) {
		S s = state;
		A lastAction = null;
		double cumR = 0;
		double dCumR = 0;
		S terminalState = s;
		int lifetime = 0;

		double gamma = 1;

		for (int t = 1; t <= maxTrajectoryLength; t++) {
			lifetime = t;

			A a = option.policy(s);
			OptionOutcome<S, O> po = samplePrimitive(s, a);

			cumR = cumR + po.cumR();
			dCumR = dCumR + gamma * po.dCumR();
			terminalState = po.terminalState();

			s = terminalState;
			gamma *= discountFactor;

			lastAction = a;
			if (bernoulli(option.terminationProb(terminalState, t))) {
				break;
			}
		}
		O tobs = observe(lastAction, terminalState);
		OptionOutcome<S, O> outcome = new OptionOutcome<S, O>(terminalState,
				tobs, dCumR, cumR, lifetime);
		return outcome;
	}

	/**
	 * Samples the simulator for the outcome of an ``option'' over observations
	 * rather than the Markov states starting from a specific Markov state and
	 * observation.
	 * 
	 * @param state
	 *            the state where the option is initialized
	 * @param observation
	 *            the observation where this state starts
	 * @param option
	 *            the option to be executed
	 * @param discountFactor
	 *            the discount factor used while accumulating reinforcements
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps to execute the option for
	 * @return the outcome of executing the specified option
	 */
	public final OptionOutcome<S, O> pomdpSample(S state, O observation,
			Option<O, A> option, double discountFactor, int maxTrajectoryLength) {
		S s = state;
		O obs = observation;
		A lastAction = null;
		double cumR = 0;
		double dCumR = 0;
		S terminalState = s;
		int lifetime = 0;

		double gamma = 1;

		for (int t = 1; t <= maxTrajectoryLength; t++) {
			lifetime = t;

			A a = option.policy(obs);
			OptionOutcome<S, O> po = samplePrimitive(s, a);

			cumR = cumR + po.cumR();
			dCumR = dCumR + gamma * po.dCumR();
			terminalState = po.terminalState();
			obs = po.terminalObservation();

			s = terminalState;
			gamma *= discountFactor;

			lastAction = a;
			if (bernoulli(option.terminationProb(obs, t))) {
				break;
			}
		}
		O tobs = observe(lastAction, terminalState);
		OptionOutcome<S, O> outcome = new OptionOutcome<S, O>(terminalState,
				tobs, dCumR, cumR, lifetime);
		return outcome;
	}

	/**
	 * Samples the simulator for the outcome of executing a ``policy'' at a
	 * specific observation for a fixed trajectory length.
	 * 
	 * @param state
	 *            a state
	 * @param policy
	 *            a policy
	 * @param trajectoryLength
	 *            the length of the trajectory
	 * @return the outcome of executing the policy for
	 *         <code>trajectoryLength</code> timesteps
	 */
	public final OptionOutcome<S, O> samplePolicy(S state, Policy<S, A> policy,
			double discountFactor, int trajectoryLength) {
		S s = state;
		A lastAction = null;
		double cumR = 0;
		double dCumR = 0;
		S terminalState = s;

		double gamma = 1;

		for (int t = 1; t <= trajectoryLength; t++) {
			A a = policy.policy(s);
			OptionOutcome<S, O> po = samplePrimitive(s, a);

			cumR = cumR + po.cumR();
			dCumR = dCumR + gamma * po.dCumR();
			terminalState = po.terminalState();

			s = terminalState;
			gamma *= discountFactor;

			lastAction = a;
		}
		O tobs = observe(lastAction, terminalState);
		OptionOutcome<S, O> outcome = new OptionOutcome<S, O>(terminalState,
				tobs, dCumR, cumR, trajectoryLength);
		return outcome;
	}

	/**
	 * Samples the simulator for a rollout generated by executing an ``option''
	 * at the specified observation.
	 * 
	 * @param state
	 *            a state
	 * @param option
	 *            the option to execute
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps to execute the option for
	 * @return the rollout generated by executing the specified option
	 */
	public final Rollout<S, A> sampleRollout(S state, Option<S, A> option,
			double discountFactor, int maxTrajectoryLength) {
		S s = state;

		RolloutImpl<S, A> rollout = new RolloutImpl<S, A>(state, 0);

		for (int t = 1; t <= maxTrajectoryLength; t++) {

			A a = option.policy(s);
			OptionOutcome<S, O> po = samplePrimitive(s, a);

			rollout.append(a, po.terminalState(), po.dCumR());
			s = po.terminalState();

			if (bernoulli(option.terminationProb(po.terminalState(), t))) {
				break;
			}
		}
		return rollout;
	}

	/**
	 * Returns the result of a Bernoulli random experiment with a specified
	 * probability of success.
	 * 
	 * @param successProb
	 *            the probability of success
	 * @return true with probability <code>successProb</code> and false with
	 *         probability <code>1-successProb</code>
	 */
	public final boolean bernoulli(double successProb) {
		double r = _rand.nextDouble();
		return (r <= successProb);
	}

}
