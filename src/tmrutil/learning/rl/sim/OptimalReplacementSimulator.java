package tmrutil.learning.rl.sim;

import org.apache.commons.math3.distribution.ExponentialDistribution;

import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.math.Function;

/**
 * A simple optimal replacement problem from: Munos and Szepesvari,
 * "Finite-Time Bounds for Fitted Value Iteration", Journal of Machine Learning
 * Research. 2008.
 * 
 * @author Timothy A. Mann
 * 
 */
public class OptimalReplacementSimulator extends Simulator<Double, Double, Integer> {
	public static final int KEEP_ACTION = 0;
	public static final int REPLACE_ACTION = 1;
	public static final int NUM_ACTIONS = 2;

	public static final double MAX_STATE = 10;

	public static final double DEFAULT_BETA = 0.5;
	public static final double DEFAULT_REPLACEMENT_COST = 30;
	public static final Function<Double, Double> DEFAULT_COST_FUNCTION = new Function<Double, Double>() {

		@Override
		public Double evaluate(Double x) throws IllegalArgumentException {
			return 4 * x;
		}

	};

	private ExponentialDistribution _edist;
	private double _replacementCost;
	private Function<Double, Double> _costFunction;

	public OptimalReplacementSimulator() {
		this(DEFAULT_BETA, DEFAULT_REPLACEMENT_COST, DEFAULT_COST_FUNCTION);
	}

	public OptimalReplacementSimulator(double beta, double replacementCost,
			Function<Double, Double> costFunction) {
		_replacementCost = replacementCost;
		_costFunction = costFunction;

		_edist = new ExponentialDistribution(1/beta);
	}

	@Override
	public OptionOutcome<Double,Double> samplePrimitive(Double state,
			Integer action) {

		double reward = 0;
		double newState = 0;

		double erand = _edist.sample();
		if (action == KEEP_ACTION) {
			reward = -_costFunction.evaluate(state);
			newState = state + erand;
		} else {
			reward = -_replacementCost - _costFunction.evaluate(0.0);
			newState = erand;
		}

		if (newState < 0) {
			newState = 0;
		}
		if (newState > MAX_STATE) {
			newState = MAX_STATE;
		}
		return new OptionOutcome<Double,Double>(newState, newState, reward, reward, 1);
	}
	
	public static class AlwaysReplaceOption implements Option<Double,Integer>
	{
		public AlwaysReplaceOption()
		{
		}

		@Override
		public Integer policy(Double state) {
			return REPLACE_ACTION;
		}

		@Override
		public double terminationProb(Double state, int duration) {
			return 0.3;
		}

		@Override
		public boolean inInitialSet(Double state) {
			return true;
		}
	}
	
	public static class AlwaysKeepOption implements Option<Double,Integer>
	{
		public AlwaysKeepOption()
		{
		}

		@Override
		public Integer policy(Double state) {
			return KEEP_ACTION;
		}

		@Override
		public double terminationProb(Double state, int duration) {
			return 0.3;
		}

		@Override
		public boolean inInitialSet(Double state) {
			return true;
		}
	}
	
	public static class ReplaceOption implements Option<Double,Integer>
	{
		private double _replacePoint;
		
		public ReplaceOption(double replacePoint)
		{
			_replacePoint = replacePoint;
		}

		@Override
		public Integer policy(Double state) {
			if(state >= _replacePoint){
				return REPLACE_ACTION;
			}else{
				return KEEP_ACTION;
			}
		}

		@Override
		public double terminationProb(Double state, int duration) {
			return (state >= _replacePoint)? 1 : 0;
		}

		@Override
		public boolean inInitialSet(Double state) {
			return true;
		}
		
	}

	@Override
	public Double observe(Integer previousAction, Double resultState) {
		return resultState;
	}

}
