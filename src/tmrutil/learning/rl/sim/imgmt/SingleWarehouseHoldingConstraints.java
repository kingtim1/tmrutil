package tmrutil.learning.rl.sim.imgmt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.stats.Random;

/**
 * Represents the case where a collection of various widget types are stored in
 * a single warehouse. Because the storage space is shared, adding widgets of
 * one type decreases the number of widgets of other types that can be stored in
 * the warehouse.
 * 
 * @author Timothy A. Mann
 * 
 */
public class SingleWarehouseHoldingConstraints implements HoldingConstraints {

	private int _maxInventory;
	private Map<WidgetType, Integer> _storageSpaceWeights;

	public SingleWarehouseHoldingConstraints(int maxInventory,
			Map<WidgetType, Integer> storageSpaceWeights) {
		_maxInventory = maxInventory;
		_storageSpaceWeights = storageSpaceWeights;
	}

	@Override
	public boolean isAdmissible(Map<WidgetType, Integer> inventory) {
		int inventorySum = 0;

		Set<WidgetType> wtypes = inventory.keySet();
		for (WidgetType wtype : wtypes) {
			int wsum = inventory.get(wtype) * _storageSpaceWeights.get(wtype);
			inventorySum += wsum;
		}
		if (inventorySum <= _maxInventory) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Map<WidgetType, Integer> nearestAdmissibleInventory(
			Map<WidgetType, Integer> proposedInventory) {
		Set<WidgetType> wtypes = proposedInventory.keySet();
		Map<WidgetType, Integer> ainv = new HashMap<WidgetType, Integer>();
		int inventorySum = 0;
		for (WidgetType wtype : wtypes) {
			int winv = proposedInventory.get(wtype);
			int wsum = winv * _storageSpaceWeights.get(wtype);
			if (inventorySum + wsum > _maxInventory) {
				ainv.put(wtype, _maxInventory - inventorySum);
				break;
			}
			inventorySum += wsum;
			ainv.put(wtype, winv);
		}

		return ainv;
	}

	@Override
	public int maxHoldings(WidgetType wtype) {
		return _maxInventory;
	}

	@Override
	public Map<WidgetType, Integer> sampleAdmissibleInventory() {
		Map<WidgetType, Integer> inventory = new HashMap<WidgetType, Integer>();
		
		List<WidgetType> wtypes = new ArrayList<WidgetType>(_storageSpaceWeights.keySet());
		Collections.shuffle(wtypes);
		
		int isum = 0;
		for (WidgetType wtype : wtypes) {
			if (isum < _maxInventory) {
				int value = Random.nextInt(_maxInventory - isum + 1);
				isum += value;
				inventory.put(wtype, value);
			}else{
				inventory.put(wtype, 0);
			}
		}
		return inventory;
	}

}
