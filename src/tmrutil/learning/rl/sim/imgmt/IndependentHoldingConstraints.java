package tmrutil.learning.rl.sim.imgmt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.stats.Random;
import tmrutil.util.ListUtil;

/**
 * Implements holding constraints that are independent for each
 * {@link WidgetType}. Each widget is assumed to have its own storage space that
 * can not be filled or diminished by any other type of widget.
 * 
 * @author Timothy A. Mann
 * 
 */
public class IndependentHoldingConstraints implements HoldingConstraints {
	private Map<WidgetType, Integer> _maxInventory;

	public IndependentHoldingConstraints(WidgetType[] wtypes, int[] maxInventory) {
		this(ListUtil.toList(wtypes), ListUtil.toList(maxInventory));
	}

	public IndependentHoldingConstraints(List<WidgetType> wtypes,
			List<Integer> maxInventory) {
		_maxInventory = new HashMap<WidgetType, Integer>();
		for (int i = 0; i < wtypes.size(); i++) {
			_maxInventory.put(wtypes.get(i), maxInventory.get(i));
		}
	}

	public IndependentHoldingConstraints(Map<WidgetType, Integer> maxInventory) {
		_maxInventory = maxInventory;
	}

	@Override
	public boolean isAdmissible(Map<WidgetType, Integer> inventory) {
		boolean admissible = true;
		Set<WidgetType> wtypes = inventory.keySet();
		for (WidgetType wtype : wtypes) {
			int inum = inventory.get(wtype);
			int mnum = _maxInventory.get(wtype);

			if (inum > mnum) {
				admissible = false;
				break;
			}
		}
		return admissible;
	}

	@Override
	public Map<WidgetType, Integer> nearestAdmissibleInventory(
			Map<WidgetType, Integer> proposedInventory) {
		Map<WidgetType, Integer> inventory = new HashMap<WidgetType, Integer>();
		Set<WidgetType> wtypes = proposedInventory.keySet();
		for (WidgetType wtype : wtypes) {
			int inum = proposedInventory.get(wtype);
			int mnum = _maxInventory.get(wtype);
			if (inum >= 0 && inum < mnum) {
				inventory.put(wtype, inum);
			} else {
				if (inum < 0) {
					inventory.put(wtype, 0);
				} else {
					inventory.put(wtype, mnum);
				}
			}
		}
		return inventory;
	}

	@Override
	public int maxHoldings(WidgetType wtype) {
		return _maxInventory.get(wtype);
	}

	@Override
	public Map<WidgetType, Integer> sampleAdmissibleInventory() {
		Map<WidgetType,Integer> inventory = new HashMap<WidgetType,Integer>();
		for(WidgetType wtype : _maxInventory.keySet()){
			int value = Random.nextInt(_maxInventory.get(wtype)+1);
			inventory.put(wtype, value);
		}
		return inventory;
	}

}