package tmrutil.learning.rl.sim.imgmt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import tmrutil.learning.rl.sim.Simulator;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.stats.GenerativeDistribution;
import tmrutil.util.ListUtil;

/**
 * Implements a multistage inventory/supply-chain management simulator with one
 * or more types of widgets.
 * 
 * @author Timothy A. Mann
 * 
 */
public class InventoryManagementSimulator extends Simulator<IMState, IMState, int[]> {

	private CostFunction _orderCost;
	private CostFunction _unmetDemandCost;
	private CostFunction _holdingCost;

	private HoldingConstraints _holdingConstraints;

	public InventoryManagementSimulator(CostFunction orderCost,
			CostFunction unmetDemandCost, CostFunction holdingCost,
			HoldingConstraints holdingConstraints) {

		_orderCost = orderCost;
		_unmetDemandCost = unmetDemandCost;
		_holdingCost = holdingCost;

		_holdingConstraints = holdingConstraints;
	}

	/**
	 * Computes the cost of ordering a collection of widgets given a state.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an array where each element represents the number of items
	 *            ordered for a separate WidgetType
	 * @return the order cost
	 */
	public int orderCost(IMState state, int[] action) {
		List<WidgetType> wtypes = state.wtypes();
		Map<WidgetType, Integer> order = new HashMap<WidgetType, Integer>();
		for (int i = 0; i < wtypes.size(); i++) {
			WidgetType wtype = wtypes.get(i);
			Integer amount = action[i];
			order.put(wtype, amount);
		}
		int orderCost = _orderCost.evaluate(order);
		return orderCost;
	}

	/**
	 * Computes the cost of unmet demands.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an array where each element represents the number of items
	 *            ordered for a separate WidgetType
	 * @param demand
	 *            a realization of the demand vector
	 * @return the cost of unmet demand
	 */
	public int unmetDemandCost(IMState state, int[] action,
			Map<WidgetType, Integer> demand) {
		Map<WidgetType, Integer> newInventory = newInventory(state, action,
				demand);
		return unmetDemandCost(newInventory, demand);
	}

	/**
	 * Computes the cost of unmet demands.
	 * 
	 * @param newInventory
	 *            the new inventory amounts
	 * @param demand
	 *            a realization of the demand vector
	 * @return the cost of unmet demand
	 */
	public int unmetDemandCost(Map<WidgetType, Integer> newInventory,
			Map<WidgetType, Integer> demand) {
		Map<WidgetType, Integer> unmetDemand = new HashMap<WidgetType, Integer>();

		for (WidgetType wtype : newInventory.keySet()) {

			int numW = newInventory.get(wtype) - demand.get(wtype);

			if (numW < 0) {
				unmetDemand.put(wtype, -numW);
			} else {
				unmetDemand.put(wtype, new Integer(0));
			}
		}
		int udcost = _unmetDemandCost.evaluate(unmetDemand);
		return udcost;
	}

	/**
	 * Computes the cost of storing the inventory left over after one cycle of
	 * ordering and demand have occurred.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action representing an order
	 * @param demand
	 *            a realization of the demand vector
	 * @return the cost of storing the left over inventory
	 */
	public int holdingCost(IMState state, int[] action,
			Map<WidgetType, Integer> demand) {
		Map<WidgetType, Integer> newInventory = newInventory(state, action,
				demand);
		return _holdingCost.evaluate(newInventory);
	}

	private Map<WidgetType, Integer> newInventory(IMState state, int[] action,
			Map<WidgetType, Integer> demand) {
		List<WidgetType> wtypes = state.wtypes();
		Map<WidgetType, Integer> newInventory = new HashMap<WidgetType, Integer>();

		for (int i = 0; i < wtypes.size(); i++) {
			WidgetType wtype = wtypes.get(i);

			int numW = Math.max(
					state.inStock(wtype) + action[i] - demand.get(wtype), 0);

			newInventory.put(wtype, numW);
		}

		newInventory = _holdingConstraints
				.nearestAdmissibleInventory(newInventory);
		return newInventory;
	}

	private Map<WidgetType, Integer> sampleDemand(IMState state) {
		List<WidgetType> wtypes = state.wtypes();
		Map<WidgetType, Integer> demand = new HashMap<WidgetType, Integer>();
		for (WidgetType wtype : wtypes) {

			int d = wtype.sampleDemand();
			d = clip(d, 0, _holdingConstraints.maxHoldings(wtype));
			demand.put(wtype, d);
		}

		return demand;
	}

	@Override
	public OptionOutcome<IMState,IMState> samplePrimitive(IMState state,
			int[] action) {
		List<WidgetType> wtypes = state.wtypes();
		if (wtypes.size() != action.length) {
			throw new IllegalArgumentException(
					"The specified action does not specify order amounts for the correct number of widget types.");
		}

		int orderCost = orderCost(state, action);
		Map<WidgetType, Integer> demand = sampleDemand(state);
		Map<WidgetType, Integer> newInventory = newInventory(state,
				action, demand);

		int unmetDemandCost = unmetDemandCost(newInventory, demand);
		int holdingCost = _holdingCost.evaluate(newInventory);

		IMState newState = new IMState(state.wtypes(), newInventory,
				demand);
		int totalCost = (orderCost + holdingCost + unmetDemandCost);
		int lifetime = 1;

		OptionOutcome<IMState,IMState> outcome = new OptionOutcome<IMState,IMState>(newState,newState,
				totalCost, totalCost, lifetime);
		return outcome;
	}

	private int clip(int val, int min, int max) {
		if (val < min) {
			return min;
		} else if (val > max) {
			return max;
		} else {
			return val;
		}
	}

	/**
	 * Returns the widget types managed in this simulation.
	 * 
	 * @return the widget types managed in this simulation
	 */
	public List<WidgetType> wtypes() {
		return _orderCost.wtypes();
	}
	
	public IMState sampleStateWithZeroPrevDemand()
	{
		Map<WidgetType,Integer> inventory = _holdingConstraints.sampleAdmissibleInventory();
		return new IMState(_orderCost.wtypes(), inventory, new HashMap<WidgetType,Integer>());
	}

	public static InventoryManagementSimulator kItemSimulator(int k) {
		GenerativeDistribution<Integer> demand = new GenerativeDistribution<Integer>() {

			@Override
			public Integer sample() {
				Random rand = new Random();
				double r = rand.nextGaussian() * 20 + 50;
				return new Integer((int) r);
			}

		};
		
		int[] storageCost = new int[k];
		int[] zeros = new int[k];
		WidgetType[] wtypes = new WidgetType[k];
		Map<WidgetType, Integer> storageCostWeights = new HashMap<WidgetType, Integer>();
		for (int i = 0; i < k; i++) {
			WidgetType wtype = new WidgetType("widget" + (i + 1), demand);
			wtypes[i] = wtype;
			storageCostWeights.put(wtype, 1);
			
			storageCost[i] = (i+1);
		}

		int maxStorage = k * 100;

		HoldingConstraints holdingConstraints = new SingleWarehouseHoldingConstraints(
				maxStorage, storageCostWeights);
		
		int baseOrderCost = 10;
		int perUnitOrderCost = 1;
		CostFunction orderCost = new NonzeroCostFunction(wtypes, perUnitOrderCost, 0, holdingConstraints, baseOrderCost);
		int unmetDemandCostSlope = 2;
		CostFunction unmetDemandCost = new LinearCostFunction(wtypes, unmetDemandCostSlope, 0,
				holdingConstraints);

		CostFunction sf = new LinearCostFunction(ListUtil.toList(wtypes), storageCost, zeros,
				holdingConstraints);

		InventoryManagementSimulator isim = new InventoryManagementSimulator(
				orderCost, unmetDemandCost, sf, holdingConstraints);

		return isim;
	}

	@Override
	public IMState observe(int[] previousAction, IMState resultState) {
		return resultState;
	}

}
