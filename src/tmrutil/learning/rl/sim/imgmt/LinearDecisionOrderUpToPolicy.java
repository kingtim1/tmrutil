package tmrutil.learning.rl.sim.imgmt;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import tmrutil.learning.rl.Policy;
import tmrutil.math.VectorOps;
import tmrutil.util.ListUtil;

/**
 * Implements an order-up-to policy that decides when to order based on a linear
 * function of the inventory at the current state.
 * 
 * @author Timothy A. Mann
 * 
 */
public class LinearDecisionOrderUpToPolicy implements Policy<IMState, int[]> {

	private List<WidgetType> _wtypes;
	private int[] _weights;
	private int _threshold;
	private int[] _target;

	public LinearDecisionOrderUpToPolicy(WidgetType[] wtypes, int[] target, int threshold, int[] weights)
	{
		this(ListUtil.toList(wtypes), target, threshold, weights);
	}
	
	public LinearDecisionOrderUpToPolicy(List<WidgetType> wtypes, int[] target, int threshold, int[] weights)
	{
		_wtypes = wtypes;
		_threshold = threshold;
		_target = target;
		_weights = weights;
	}
	
	public LinearDecisionOrderUpToPolicy(List<WidgetType> wtypes,
			Map<WidgetType, Integer> target, int threshold,
			Map<WidgetType, Integer> weights) {
		_wtypes = wtypes;
		_threshold = threshold;

		_target = new int[wtypes.size()];
		_weights = new int[wtypes.size()];
		for (int i = 0; i < wtypes.size(); i++) {
			WidgetType wtype = wtypes.get(i);
			_target[i] = target.get(wtype);
			_weights[i] = weights.get(wtype);
		}
	}
	
	public static final LinearDecisionOrderUpToPolicy newInstanceWithUnitWeights(WidgetType[] wtypes, int[] target, int threshold)
	{
		int[] ones = new int[wtypes.length];
		Arrays.fill(ones, 1);
		LinearDecisionOrderUpToPolicy p = new LinearDecisionOrderUpToPolicy(wtypes, target, threshold, ones);
		return p;
	}

	public boolean orderAt(IMState state) {
		int sum = 0;
		for (int i = 0; i < _wtypes.size(); i++) {
			WidgetType wtype = _wtypes.get(i);
			sum += _weights[i] * state.inStock(wtype);
		}
		return (sum < _threshold);
	}

	public int[] orderUpToAmount(IMState state) {
		int[] orderA = new int[_wtypes.size()];
		for (int i = 0; i < _wtypes.size(); i++) {
			WidgetType wtype = _wtypes.get(i);
			int desired = _target[i];
			int inStock = state.inStock(wtype);
			orderA[i] = Math.max(desired - inStock, 0);
		}
		return orderA;
	}

	public int[] zeroOrder() {
		int[] zeroA = new int[_wtypes.size()];
		return zeroA;
	}

	@Override
	public int[] policy(IMState state) {

		if (orderAt(state)) {
			return orderUpToAmount(state);
		} else {
			return zeroOrder();
		}
	}

}
