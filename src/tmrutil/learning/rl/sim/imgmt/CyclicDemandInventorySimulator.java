package tmrutil.learning.rl.sim.imgmt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tmrutil.learning.rl.factored.FactoredAction;
import tmrutil.learning.rl.factored.FactoredActionImpl;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.learning.rl.smdp.PrimitiveAction;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.Interval;

/**
 * A simulator for multiple commodity inventory management problems with cyclic
 * demand. The simulator has a cycle time after which the demand distribution
 * repeats. In this problem domain, all inventory is stored in the same
 * warehouse. This means that storing large quantities of one commodity
 * decreases the available space for other commodities.
 * 
 * @author Timothy A. Mann
 * 
 */
public class CyclicDemandInventorySimulator extends
		Simulator<FactoredState, FactoredState, FactoredAction> {

	/**
	 * A commodity represents a type of product under management.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class Commodity {
		private double _unitCost;
		private int _peakTime;
		private Interval _demandInterval;
		private double _demandStd;

		/**
		 * Constructs a new commodity.
		 * 
		 * @param unitCost
		 *            the cost to buy a single unit of this commodity
		 * @param peakTime
		 *            the time during the cycle when this commodity's demand is
		 *            highest
		 * @param demandInterval
		 *            the minimum and maximum expected demand
		 * @param demandStd
		 *            the standard deviation around the expected demand
		 */
		public Commodity(double unitCost, int peakTime,
				Interval demandInterval, double demandStd) {
			_unitCost = unitCost;
			_peakTime = peakTime;
			_demandInterval = demandInterval;
			_demandStd = demandStd;
		}

		public double unitCost() {
			return _unitCost;
		}

		public int peakTime() {
			return _peakTime;
		}

		/**
		 * Calculates the expected demand for a specific period during the
		 * demand cycle.
		 * 
		 * @param timeperiod
		 *            an integer in [0, cycleTime]
		 * @param cycleTime
		 *            the number of rounds before the demand distribution
		 *            repeats
		 * @return the expected demand
		 */
		public double expectedDemand(int timeperiod, int cycleTime) {
			double x = ((timeperiod + _peakTime) / (double) cycleTime) * 2
					* Math.PI;
			double y = _demandInterval.getDiff() * ((Math.cos(x) + 1) / 2)
					+ _demandInterval.getMin();
			return y;
		}

		/**
		 * Returns a stochastic sample of the demand for this commodity at a
		 * specific time period, given the cycle time.
		 * 
		 * @param timeperiod
		 *            the time period is an integer in [0, cycleTime]
		 * @param cycleTime
		 *            the number of steps until the demand distribution repeats
		 * @return a sample of demand for this commodity
		 */
		public int sampleDemand(int timeperiod, int cycleTime) {
			double y = expectedDemand(timeperiod, cycleTime);
			int z = (int) Math.max(0, y + Random.normal(0, _demandStd));
			return z;
		}

		/**
		 * Returns a copy of this commodity with deterministic demand. The
		 * demand distribution changes with time, but the standard deviation is
		 * 0.
		 * 
		 * @return a deterministic copy of this commodity
		 */
		public Commodity deterministicDemandCopy() {
			return new Commodity(_unitCost, _peakTime, _demandInterval, 0);
		}
	}

	private int _cycleTime;
	private List<Commodity> _commodities;
	private int _maxInventory;
	private int[] _maxStateValues;
	private int[] _maxActionValues;

	private double _orderBaseCost;
	private double _unmetDemandBaseCost;
	private double _unmetDemandSlopeCost;
	private double _storageCost;

	private int[] _demandBuff;
	private int[] _afterDemandBuff;
	private int[] _unmetDemandBuff;

	public CyclicDemandInventorySimulator(int cycleTime, int maxInventory,
			List<Commodity> commodities) {
		_cycleTime = cycleTime;
		_maxInventory = maxInventory;
		_commodities = commodities;

		for (Commodity c : commodities) {
			if (c.peakTime() < 0 || c.peakTime() > cycleTime) {
				throw new IllegalArgumentException(
						"Detected invalid peak time. Peak time cannot be negative or larger than the cycle time.");
			}
		}

		_maxStateValues = new int[commodities.size() + 1];
		_maxStateValues[0] = cycleTime;
		_maxActionValues = new int[commodities.size()];
		for (int i = 0; i < commodities.size(); i++) {
			_maxStateValues[i + 1] = _maxInventory + 1;
			_maxActionValues[i] = _maxInventory + 1;
		}

		_demandBuff = new int[commodities.size()];
		_afterDemandBuff = new int[commodities.size()];
		_unmetDemandBuff = new int[commodities.size()];

		setOrderBaseCost(DEFAULT_ORDER_BASE_COST);
		setUnmetDemandBaseCost(DEFAULT_UNMET_DEMAND_BASE_COST);
		setUnmetDemandSlopeCost(DEFAULT_UNMET_DEMAND_SLOPE_COST);
		setStorageSlopeCost(DEFAULT_STORAGE_COST);
	}

	/**
	 * Sets the maximum inventory for all quantities.
	 * 
	 * @param maxInventory
	 *            max inventory level
	 */
	public void setMaxInventory(int maxInventory) {
		_maxInventory = maxInventory;
		for (int i = 0; i < _commodities.size(); i++) {
			_maxStateValues[i + 1] = maxInventory + 1;
			_maxActionValues[i] = maxInventory + 1;
		}
	}

	/**
	 * Returns a copy of this simulator with deterministic demands (rather than
	 * stochastic demand distributions) at each period in the demand cycle.
	 * 
	 * @return a copy of this simulator with deterministic demands
	 */
	public CyclicDemandInventorySimulator deterministicDemandCopy() {
		List<Commodity> detCommodities = new ArrayList<Commodity>();
		for (Commodity c : _commodities) {
			detCommodities.add(c.deterministicDemandCopy());
		}
		CyclicDemandInventorySimulator sim = new CyclicDemandInventorySimulator(
				_cycleTime, _maxInventory, detCommodities);

		sim.setOrderBaseCost(orderBaseCost());
		sim.setUnmetDemandBaseCost(unmetDemandBaseCost());
		sim.setUnmetDemandSlopeCost(unmetDemandSlopeCost());
		sim.setStorageSlopeCost(storageSlopeCost());

		return sim;
	}

	/**
	 * Conveniently enables the construction of subproblem simulators. The
	 * subproblems are divided up by the commodities.
	 * 
	 * @param numSubproblems
	 *            the number of subproblems to create (this should be a positive
	 *            number but less than or equal to the total number of
	 *            commodities in this simulator
	 * @return a collection of subproblems dividing the problem up by
	 *         commodities
	 */
	public List<CyclicDemandInventorySimulator> createSubproblems(
			int numSubproblems) {
		if (numSubproblems > _commodities.size()) {
			throw new IllegalArgumentException(
					"Cannot create more subproblems than commodities.");
		}
		List<CyclicDemandInventorySimulator> subproblems = new ArrayList<CyclicDemandInventorySimulator>(
				numSubproblems);
		int commoditiesPerSubproblem = _commodities.size() / numSubproblems;
		for (int s = 0; s < numSubproblems; s++) {
			List<Commodity> commodities = new ArrayList<Commodity>();
			if (s == numSubproblems - 1) {
				for (int j = s * commoditiesPerSubproblem; j < _commodities
						.size(); j++) {
					commodities.add(_commodities.get(j));
				}
			} else {
				for (int j = s * commoditiesPerSubproblem; j < (s + 1)
						* commoditiesPerSubproblem; j++) {
					commodities.add(_commodities.get(j));
				}
			}
			CyclicDemandInventorySimulator sim = new CyclicDemandInventorySimulator(
					_cycleTime, _maxInventory / numSubproblems, commodities);

			sim.setOrderBaseCost(orderBaseCost());
			sim.setUnmetDemandBaseCost(unmetDemandBaseCost());
			sim.setUnmetDemandSlopeCost(unmetDemandSlopeCost());
			sim.setStorageSlopeCost(storageSlopeCost());

			subproblems.add(sim);
		}

		return subproblems;
	}

	/**
	 * Returns the number of commodities being managed by the agent in this
	 * simulation.
	 * 
	 * @return the number of commodities being managed by the agent
	 */
	public int numberOfCommodities() {
		return _commodities.size();
	}

	/**
	 * Returns the expected demand for a commodity during a specified time
	 * period in the cycle.
	 * 
	 * @param commodityIndex
	 *            the index of a commodity
	 * @param timePeriod
	 *            an integer in [0, cycleTime()-1]
	 * @return the expected demand for the specified commodity
	 */
	public double expectedDemand(int commodityIndex, int timePeriod) {
		return _commodities.get(commodityIndex).expectedDemand(timePeriod,
				cycleTime());
	}

	/**
	 * Returns the maximum number of units that can be stored in inventory. The
	 * number of units in inventory is the sum of the quantities of each
	 * commodity being managed.
	 * 
	 * @return maximum number of units that can be stored in inventory
	 */
	public int maxInventory() {
		return _maxInventory;
	}

	@Override
	public FactoredState observe(FactoredAction previousAction,
			FactoredState resultState) {
		return resultState;
	}

	@Override
	public OptionOutcome<FactoredState, FactoredState> samplePrimitive(
			FactoredState state, FactoredAction action) {

		if (state.size() != _commodities.size() + 1) {
			throw new IllegalArgumentException(
					"State must contain the time period at index 0 and the inventory levels for each commodity.");
		}

		// Find out where we are in the demand cycle
		int timeperiod = state.component(0);

		// Sample the demand for all commodities
		int[] demand = sampleDemand(state);

		// Determine the inventory levels after the demand is subtracted and
		// determine the level of unmet demand for each commodity
		int[] afterDemand = _afterDemandBuff;
		int[] unmetDemand = _unmetDemandBuff;
		int afterDemandSum = 0;
		for (int i = 0; i < demand.length; i++) {
			int v = state.component(i + 1) - demand[i];
			if (v >= 0) {
				afterDemand[i] = v;
				unmetDemand[i] = 0;
				afterDemandSum += v;
			} else {
				afterDemand[i] = 0;
				unmetDemand[i] = -v;
			}
		}

		// Create the next state
		int[] newState = new int[_commodities.size() + 1];
		timeperiod++; // Update the time period
		if (timeperiod >= _cycleTime) {
			timeperiod = 0;
		}
		newState[0] = timeperiod;

		// Add the newly purchased inventory to the next state, if some of the
		// inventory doesn't fit, then we just throw it out
		for (int i = 0; i < afterDemand.length; i++) {
			if (afterDemandSum + action.component(i) <= _maxInventory) {
				afterDemandSum += action.component(i);
				newState[i + 1] = afterDemand[i] + action.component(i);
			} else {
				if (_maxInventory - afterDemandSum > 0) {
					newState[i + 1] = afterDemand[i]
							+ (_maxInventory - afterDemandSum);
				} else {
					newState[i + 1] = afterDemand[i];
				}
			}
		}

		FactoredState fnewState = new FactoredStateImpl(newState,
				_maxStateValues);

		// Compute the immediate cost
		double cost = orderCost(action) + unmetDemandCost(unmetDemand)
				+ storageCost(fnewState);

		OptionOutcome<FactoredState, FactoredState> outcome = new OptionOutcome<FactoredState, FactoredState>(
				fnewState, fnewState, cost, cost, 1);
		return outcome;
	}

	/**
	 * Samples the demand distribution given a state.
	 * 
	 * @param state
	 *            a state
	 * @return a vector indicating the demand for each commodity
	 */
	public int[] sampleDemand(FactoredState state) {
		int timeperiod = state.component(0);
		int[] demand = _demandBuff;
		for (int i = 0; i < demand.length; i++) {
			demand[i] = _commodities.get(i)
					.sampleDemand(timeperiod, _cycleTime);
		}
		return demand;
	}

	/**
	 * Calculates the order cost associated with an action.
	 * 
	 * @param action
	 *            an action
	 * @return the order cost associated with <code>action</code>
	 */
	public double orderCost(FactoredAction action) {
		if (action.size() != _commodities.size()) {
			throw new IllegalArgumentException(
					"Cannot compute order cost because the length of the action vector does not match the number of commodities.");
		}

		boolean nonzero = false;
		double sum = 0;
		for (int i = 0; i < action.size(); i++) {
			int a = action.component(i);
			if (a > 0) {
				sum += a * _commodities.get(i).unitCost();
				nonzero = true;
			}
			if (a < 0) {
				throw new IllegalArgumentException(
						"Cannot compute order cost for a negative amount.");
			}
		}
		if (nonzero) {
			return sum + orderBaseCost();
		} else {
			return 0;
		}
	}

	/**
	 * Calculates the storage cost associated with a specific state.
	 * 
	 * @param state
	 *            a state
	 * @return the storage cost associated with <code>state</code>
	 */
	public double storageCost(FactoredState state) {
		double sum = 0;
		for (int i = 1; i < state.size(); i++) {
			int l = state.component(i);
			sum += l * storageSlopeCost();
		}
		return sum;
	}

	/**
	 * Computes the cost of unmet demand.
	 * 
	 * @param unmetDemand
	 *            an integer array with one element per commodity where positive
	 *            elements represent the number of units of a commodity that
	 *            were demanded but were denied because there were not enough in
	 *            current inventory (negative entries in the array are ignored)
	 * @return the cost of unmet demand
	 */
	public double unmetDemandCost(int[] unmetDemand) {
		if (unmetDemand.length != _commodities.size()) {
			throw new IllegalArgumentException(
					"Cannot compute unmet demand cost because the length of the unmet demand vector does not match the number of commodities.");
		}

		boolean nonzero = false;
		double sum = 0;
		for (int i = 0; i < unmetDemand.length; i++) {
			int ud = unmetDemand[i];
			if (ud > 0) {
				sum += ud * unmetDemandSlopeCost();
				nonzero = true;
			}
		}
		if (nonzero) {
			return sum + unmetDemandBaseCost();
		} else {
			return 0;
		}
	}

	/**
	 * Returns the number of timesteps needed for the demand distributions to
	 * repeat.
	 * 
	 * @return the number of timesteps before the demand distributions repeat
	 */
	public int cycleTime() {
		return _cycleTime;
	}

	/**
	 * Returns the base cost for placing any non-zero order.
	 * 
	 * @return the base cost for placing a non-zero order
	 */
	public double orderBaseCost() {
		return _orderBaseCost;
	}

	public void setOrderBaseCost(double orderBaseCost) {
		_orderBaseCost = orderBaseCost;
	}

	/**
	 * Returns the base unmet demand cost applied on any timestep where there is
	 * insufficient demand for at least one commodity. If there is enough
	 * inventory to meet the demand for all commodities, then the unmet demand
	 * cost is 0 (no base unmet demand cost is applied).
	 * 
	 * @return the base unmet demand cost
	 */
	public double unmetDemandBaseCost() {
		return _unmetDemandBaseCost;
	}

	public void setUnmetDemandBaseCost(double unmetDemandBaseCost) {
		if (unmetDemandBaseCost < 0) {
			throw new IllegalArgumentException(
					"Unmet demand base cost cannot be negative.");
		}
		_unmetDemandBaseCost = unmetDemandBaseCost;
	}

	public double unmetDemandSlopeCost() {
		return _unmetDemandSlopeCost;
	}

	public void setUnmetDemandSlopeCost(double unmetDemandSlopeCost) {
		if (unmetDemandSlopeCost < 0) {
			throw new IllegalArgumentException(
					"Unmet demand slope cost cannot be negative.");
		}
		_unmetDemandSlopeCost = unmetDemandSlopeCost;
	}

	public double storageSlopeCost() {
		return _storageCost;
	}

	public void setStorageSlopeCost(double storageCost) {
		_storageCost = storageCost;
	}

	public FactoredState zeroInventoryState(int timeperiod) {
		int[] state = new int[_commodities.size() + 1];
		state[0] = timeperiod;
		return new FactoredStateImpl(state, _maxStateValues);
	}

	/**
	 * Constructs a state where equal quantities of each commodity are being
	 * stored.
	 * 
	 * @param timeperiod
	 *            the order period of the state
	 * @param quantity
	 *            the quantity of each commodity being stored
	 * @return a new state
	 */
	public FactoredState uniformInventoryState(int timeperiod, int quantity) {
		int[] state = new int[_commodities.size() + 1];
		state[0] = timeperiod;
		for (int i = 0; i < _commodities.size(); i++) {
			state[i + 1] = quantity;
		}
		return new FactoredStateImpl(state, _maxStateValues);
	}

	/**
	 * Samples a state from the simulation's state space. This method tries to
	 * sample states in an approximately uniform fashion.
	 * 
	 * @return a state
	 */
	public FactoredState sampleState() {
		int[] state = new int[_commodities.size() + 1];

		int time = Random.nextInt(_cycleTime);
		state[0] = time;

		int[] rperm = VectorOps.randperm(_commodities.size());

		int max = Random.nextInt(_maxInventory);
		int sum = 0;
		for (int i = 0; i < rperm.length && sum < max; i++) {
			state[rperm[i] + 1] = Random.nextInt(max - sum);
			sum += state[rperm[i] + 1];
		}
		return new FactoredStateImpl(state, _maxStateValues);
	}

	/**
	 * Generates a primitive action that stochastically orders commodities for a
	 * subset of the commodities being managed.
	 * 
	 * @param commodityIndices
	 *            the indices of a subset of commodities being managed
	 * @param orderProb
	 *            the probability of ordering more of the specified commodities
	 * @param orderQuantity
	 *            the quantity to order if an order goes through
	 * @return a stochastic primitive action for ordering over a subset of
	 *         commodities
	 */
	public Option<FactoredState, FactoredAction> generateStochasticOrderPrimitive(
			final Set<Integer> commodityIndices, final double orderProb,
			final int orderQuantity) {
		return new Option<FactoredState, FactoredAction>() {

			@Override
			public FactoredAction policy(FactoredState state) {
				int[] action = new int[state.size() - 1];
				for (Integer cInd : commodityIndices) {
					double rand = Random.uniform();
					if (rand < orderProb) {
						action[cInd] = orderQuantity;
					}
				}
				return new FactoredActionImpl(action, _maxActionValues);
			}

			@Override
			public double terminationProb(FactoredState state, int duration) {
				return 1;
			}

			@Override
			public boolean inInitialSet(FactoredState state) {
				return true;
			}

		};

	}

	public Set<FactoredAction> generateSingleCommodityOrderPrimitives(
			int density) {
		Set<FactoredAction> prims = new HashSet<FactoredAction>();

		// Add the zero action
		int[] zeroAction = new int[_commodities.size()];
		prims.add(new FactoredActionImpl(zeroAction, _maxActionValues));

		for (int c = 0; c < _commodities.size(); c++) {
			for (int i = 0; i < density; i++) {
				int[] action = new int[_commodities.size()];
				action[c] = (i + 1) * (_maxInventory / (density));
				prims.add(new FactoredActionImpl(action, _maxActionValues));
			}
		}
		return prims;
	}

	public Set<Option<FactoredState, FactoredAction>> generateSingleCommodityOrderPrimitivesAsOptions(
			int density) {
		Set<Option<FactoredState, FactoredAction>> prims = new HashSet<Option<FactoredState, FactoredAction>>();

		// Add the zero action
		int[] zeroAction = new int[_commodities.size()];
		FactoredActionImpl fzeroAction = new FactoredActionImpl(zeroAction,
				_maxActionValues);
		prims.add(new PrimitiveAction<FactoredState, FactoredAction>(fzeroAction.uniqueID(),
				fzeroAction));

		for (int c = 0; c < _commodities.size(); c++) {
			for (int i = 0; i < density; i++) {
				int[] action = new int[_commodities.size()];
				action[c] = (i + 1) * (_maxInventory / (density));
				FactoredActionImpl faction = new FactoredActionImpl(action,
						_maxActionValues);
				prims.add(new PrimitiveAction<FactoredState, FactoredAction>(faction.uniqueID(),
						faction));
			}
		}
		return prims;
	}

	public Set<Option<FactoredState, FactoredAction>> generateAugmentedOptions(
			int density, int maxWidgetOrder) {
		Set<Option<FactoredState, FactoredAction>> options = generateSingleCommodityOrderPrimitivesAsOptions(density);

		for (int c = 0; c < _commodities.size(); c++) {
			for (int i = 0; i < density; i++) {
				int threshold = (int) (i * (_maxInventory / 2.0) / (density - 1));
				options.add(new CDIWaitUntilThresholdOption(c, threshold,
						_maxActionValues));
			}
		}

		return options;
	}

	public static class CDIBuildUpOption implements
			Option<FactoredState, FactoredAction> {
		private List<FactoredAction> _validActions;
		private int _maxWidgetOrder;
		private int _maxInventory;

		public CDIBuildUpOption(List<FactoredAction> validActions,
				int maxWidgetOrder, int maxInventory) {
			_validActions = new ArrayList<FactoredAction>();
			for (FactoredAction fa : validActions) {
				if (Statistics.sum(fa.toArray()) <= maxWidgetOrder) {
					_validActions.add(fa);
				}
			}
			_maxWidgetOrder = maxWidgetOrder;
			_maxInventory = maxInventory;
		}

		public FactoredAction policy(FactoredState state) {
			// Copy the state ignoring the time component
			int[] oldState = Arrays.copyOfRange(state.toArray(), 1,
					state.size());
			// Find the component with the minimum quantity
			int minIndex = Statistics.minIndex(oldState);

			int bestIncrease = -1;
			FactoredAction bestAction = null;
			int[] newState = new int[oldState.length];
			for (int i = 0; i < _validActions.size(); i++) {
				FactoredAction action = _validActions.get(i);
				for (int j = 0; j < oldState.length; j++) {
					newState[j] = oldState[j] + action.component(j);
				}
				if (!exceedsMaxWidgetOrder(oldState, newState)
						&& !exceedsMax(newState)) {
					int increase = newState[minIndex] - oldState[minIndex];
					if (increase > bestIncrease) {
						bestIncrease = increase;
						bestAction = action;
					}
				}
			}

			return bestAction;
		}

		private boolean exceedsMaxWidgetOrder(int[] oldState, int[] newState) {
			int sum = 0;
			for (int i = 0; i < oldState.length; i++) {
				sum += newState[i] - oldState[i];
			}
			return (sum > _maxWidgetOrder);
		}

		private boolean exceedsMax(int[] state) {
			int sum = 0;
			for (int i = 0; i < state.length; i++) {
				if (state[i] > _maxInventory) {
					return true;
				}
				sum += state[i];
				if (sum > _maxInventory) {
					return true;
				}
			}
			return false;
		}

		@Override
		public double terminationProb(FactoredState state, int duration) {
			return 0.1;
		}

		@Override
		public boolean inInitialSet(FactoredState state) {
			return true;
		}
	}

	/**
	 * An option that waits (orders nothing) until the inventory level of a
	 * particular commodity falls below a specified threshold value.
	 * 
	 * <p>
	 * CDI stands for Cyclic Demand Inventory
	 * </p>
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class CDIWaitUntilThresholdOption implements
			Option<FactoredState, FactoredAction> {
		private FactoredAction _noOrderAction;
		private int _index;
		private int _threshold;

		public CDIWaitUntilThresholdOption(int index, int threshold,
				int[] maxValues) {
			if (index < 0 || index >= maxValues.length) {
				throw new IndexOutOfBoundsException(
						"Index cannot be negative or larger than (or equal to) the length of the maxValues array.");
			}
			_noOrderAction = new FactoredActionImpl(new int[maxValues.length],
					maxValues);

			_index = index;
			_threshold = threshold;
		}

		@Override
		public FactoredAction policy(FactoredState state) {
			return _noOrderAction;
		}

		@Override
		public double terminationProb(FactoredState state, int duration) {
			if (state.component(_index) < _threshold) {
				return 1;
			} else {
				return 0;
			}
		}

		@Override
		public boolean inInitialSet(FactoredState state) {
			if (state.component(_index) > _threshold) {
				return true;
			} else {
				return false;
			}
		}

	}

	/**
	 * Constructs a simulator instance of the eight commodity inventory
	 * management problem.
	 * 
	 * @return a simulator for the eight commodity inventory management problem
	 */
	public static CyclicDemandInventorySimulator newEightCommoditySimulator() {
		return new CyclicDemandInventorySimulator(DEFAULT_CYCLE_TIME,
				DEFAULT_MAX_INVENTORY, buildNCommodities(8));
	}

	/**
	 * Constructs a simulator instance of the four commodity inventory
	 * management problem.
	 * 
	 * @return a simulator for the four commodity inventory management problem
	 */
	public static CyclicDemandInventorySimulator newFourCommoditySimulator() {
		return new CyclicDemandInventorySimulator(DEFAULT_CYCLE_TIME,
				DEFAULT_MAX_INVENTORY, buildNCommodities(4));
	}

	private static final int DEFAULT_CYCLE_TIME = 24;
	private static final int DEFAULT_MAX_INVENTORY = 500;

	private static final double DEFAULT_UNMET_DEMAND_BASE_COST = 2;
	private static final double DEFAULT_UNMET_DEMAND_SLOPE_COST = 10;

	private static final double DEFAULT_ORDER_BASE_COST = 8;
	private static final double DEFAULT_STORAGE_COST = 0;

	public static final int[] PEAK_TIMES = { 0, 4, 12, 18, 15, 22, 0, 9 };
	public static final double[] COSTS = { 1, 3, 1, 2, 0.5, 1, 1, 1 };
	public static final double[] STDS = { 2, 1, 2, 3, 2, 2, 1, 2 };
	public static final Interval[] DINTS = { new Interval(0, 16),
			new Interval(0, 10), new Interval(0, 20), new Interval(0, 4),
			new Interval(0, 10), new Interval(0, 9), new Interval(0, 20),
			new Interval(0, 16) };

	private static List<Commodity> buildNCommodities(int n) {
		List<Commodity> cs = new ArrayList<Commodity>();

		for (int i = 0; i < n; i++) {
			cs.add(new Commodity(COSTS[i] /* unit cost */, PEAK_TIMES[i] /*
																		 * peak
																		 * time
																		 */,
					DINTS[i] /*
							 * demand interval
							 */, STDS[i] /*
										 * demand standard deviation
										 */));
		}

		return cs;
	}

	/**
	 * This helper function enables the creation of simulators managing
	 * arbitrary numbers of commodities.
	 * 
	 * @param n
	 *            the number of commodities to add to the simulator
	 * @param cycleTime
	 *            the cycle time for the demand distributions
	 * @param maxInventory
	 *            the size of the inventory to use
	 * @return a new simulator
	 */
	public static CyclicDemandInventorySimulator buildNCommodityProblem(int n,
			int cycleTime, int maxInventory) {
		List<Commodity> cs = new ArrayList<Commodity>();
		for (int i = 0; i < n; i++) {
			double cost = Random.uniform(1, 4);
			int peakTime = Random.nextInt(cycleTime);
			Interval demandInterval = new Interval(0, Random.nextInt(25));
			double std = Random.uniform(1, 4);
			cs.add(new Commodity(cost, peakTime, demandInterval, std));
		}
		return new CyclicDemandInventorySimulator(cycleTime, maxInventory, cs);
	}

}
