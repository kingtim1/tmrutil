package tmrutil.learning.rl.sim.imgmt;

import tmrutil.stats.GenerativeDistribution;

/**
 * An instance of this class describes a type of widget in an inventory
 * management task.
 * 
 * @author Timothy A. Mann
 * 
 */
public class WidgetType {
	private String _widgetID;
	private GenerativeDistribution<Integer> _demand;

	public WidgetType(String widgetID,
			GenerativeDistribution<Integer> demand) {
		_widgetID = widgetID;
		_demand = demand;
	}

	public String id() {
		return _widgetID;
	}

	public Integer sampleDemand() {
		int demand = _demand.sample();
		return demand;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WidgetType) {
			WidgetType wt = (WidgetType) obj;
			if (wt.id().equals(id())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return id().hashCode();
	}

	@Override
	public String toString() {
		return id();
	}
}