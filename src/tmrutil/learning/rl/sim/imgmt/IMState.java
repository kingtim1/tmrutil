package tmrutil.learning.rl.sim.imgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Instances of this class describe the state of an inventory management problem
 * with one or more products.
 * 
 * @author Timothy A. Mann
 * 
 */
public class IMState {
	private List<WidgetType> _wtypes;
	private Map<WidgetType, Integer> _inventory;
	private Map<WidgetType, Integer> _prevDemand;

	public IMState(List<WidgetType> wtypes,
			Map<WidgetType, Integer> inventory,
			Map<WidgetType, Integer> previousDemand) {
		_wtypes = new ArrayList<WidgetType>(wtypes);
		_inventory = new HashMap<WidgetType, Integer>(inventory);
		_prevDemand = new HashMap<WidgetType, Integer>(previousDemand);
	}

	public List<WidgetType> wtypes() {
		return _wtypes;
	}
	
	public int[] stockArray()
	{
		int[] sarray = new int[_wtypes.size()];
		for(int i=0;i<_wtypes.size();i++){
			sarray[i] = inStock(_wtypes.get(i));
		}
		return sarray;
	}

	public Integer inStock(WidgetType wtype) {
		Integer amount = _inventory.get(wtype);
		if(amount == null){
			return new Integer(0);
		}else{
			return amount;
		}
	}

	public Integer previousDemand(WidgetType wtype) {
		Integer amount = _prevDemand.get(wtype);
		if(amount == null){
			return new Integer(0);
		}else{
			return amount;
		}
	}

	public Map<WidgetType, Integer> previousDemand() {
		return _prevDemand;
	}

	public boolean equals(Object obj) {
		if (obj instanceof IMState) {
			IMState os = (IMState) obj;
			return (os.wtypes().equals(wtypes())
					&& _inventory.equals(os._inventory) && _prevDemand
						.equals(os._prevDemand));
		} else {
			return false;
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Inventory : {");
		for (int i = 0; i < _wtypes.size(); i++) {
			if (i > 0) {
				sb.append(", ");
			}
			WidgetType wtype = _wtypes.get(i);
			String id = wtype.id();
			Integer amount = _inventory.get(wtype);
			if(amount == null){
				amount = new Integer(0);
			}
			sb.append(id + ":" + amount);
		}
		sb.append("}\n");
		sb.append("Demand : {");
		for (int i = 0; i < _wtypes.size(); i++) {
			if (i > 0) {
				sb.append(", ");
			}
			WidgetType wtype = _wtypes.get(i);
			String id = wtype.id();
			Integer amount = _prevDemand.get(wtype);
			if(amount == null){
				amount = new Integer(0);
			}
			sb.append(id + ":" + amount);
		}
		sb.append("}\n");
		return sb.toString();
	}
}