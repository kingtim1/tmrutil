package tmrutil.learning.rl.sim.imgmt;

import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.util.Interval;
import tmrutil.util.ListUtil;

public class NonzeroCostFunction extends LinearCostFunction {

	private static final long serialVersionUID = 3716973157059368753L;
	private int _nonzeroCost;
	
	
	public NonzeroCostFunction(WidgetType[] wtypes, int slope, int bias, HoldingConstraints holdingConstraints, int nonzeroCost)
	{
		super(wtypes, slope, bias, holdingConstraints);
		_nonzeroCost = nonzeroCost;
	}
	
	public NonzeroCostFunction(WidgetType[] wtypes, int[] slope, int[] bias, HoldingConstraints holdingConstraints, int nonzeroCost)
	{
		this(ListUtil.toList(wtypes), ListUtil.toList(slope), ListUtil.toList(bias), holdingConstraints, nonzeroCost);
	}
	
	public NonzeroCostFunction(List<WidgetType> wtypes, List<Integer> slope, List<Integer> bias, HoldingConstraints holdingConstraints, int nonzeroCost)
	{
		super(wtypes, slope, bias, holdingConstraints);
		_nonzeroCost = nonzeroCost;
	}
	
	public NonzeroCostFunction(Map<WidgetType, Integer> slope,
			Map<WidgetType, Integer> bias, HoldingConstraints holdingConstraints, int nonzeroCost) {
		super(slope, bias, holdingConstraints);
		_nonzeroCost = nonzeroCost;
	}

	@Override
	public Integer evaluate(Map<WidgetType, Integer> x)
			throws IllegalArgumentException {
		
		Integer c = super.evaluate(x);
		if(containsPositiveValue(x)){
			return _nonzeroCost + c;
		}else{
			return c;
		}
	}

	@Override
	public Interval range() {
		Interval r = super.range();
		return new Interval(r.getMin(), r.getMax() + _nonzeroCost);
	}

	public static boolean containsPositiveValue(Map<WidgetType,Integer> x)
	{
		Set<WidgetType> wtypes = x.keySet();
		for(WidgetType wtype : wtypes){
			if(x.get(wtype) > 0){
				return true;
			}
		}
		return false;
	}
	
}
