package tmrutil.learning.rl.sim.imgmt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.util.Interval;
import tmrutil.util.ListUtil;

/**
 * A linear cost function allows a cost to be expressed as a linear sum of the
 * amount of a type of widget.
 * 
 * @author Timothy A. Mann
 * 
 */
public class LinearCostFunction implements CostFunction {
	private static final long serialVersionUID = 2496401217981811238L;
	private List<WidgetType> _wtypes;
	private Map<WidgetType, Integer> _slope;
	private Map<WidgetType, Integer> _bias;
	private Interval _range;

	public LinearCostFunction(WidgetType[] wtypes, int slope, int bias,
			HoldingConstraints holdingConstraints) {
		this(ListUtil.toList(wtypes), slope, bias, holdingConstraints);
	}

	public LinearCostFunction(List<WidgetType> wtypes, int slope, int bias,
			HoldingConstraints holdingConstraints) {
		_wtypes = wtypes;
		_slope = new HashMap<WidgetType, Integer>();
		_bias = new HashMap<WidgetType, Integer>();
		for (WidgetType wtype : wtypes) {
			_slope.put(wtype, slope);
			_bias.put(wtype, bias);
		}
		_range = computeRange(_slope, _bias, holdingConstraints);
	}

	public LinearCostFunction(List<WidgetType> wtypes, int[] slope, int[] bias,
			HoldingConstraints holdingConstraints) {
		this(wtypes, ListUtil.toList(slope), ListUtil.toList(bias), holdingConstraints);
	}
	
	public LinearCostFunction(List<WidgetType> wtypes, List<Integer> slope, List<Integer> bias, HoldingConstraints holdingConstraints)
	{
		_slope = new HashMap<WidgetType, Integer>();
		_bias = new HashMap<WidgetType, Integer>();
		for (int i = 0; i < wtypes.size(); i++) {
			WidgetType wtype = wtypes.get(i);
			_slope.put(wtype, slope.get(i));
			_bias.put(wtype, bias.get(i));
		}

		_range = computeRange(_slope, _bias, holdingConstraints);
	}

	public LinearCostFunction(Map<WidgetType, Integer> slope,
			Map<WidgetType, Integer> bias, HoldingConstraints holdingConstraints) {
		_slope = slope;
		_bias = bias;

		_range = computeRange(slope, bias, holdingConstraints);
	}

	private Interval computeRange(Map<WidgetType, Integer> slope,
			Map<WidgetType, Integer> bias, HoldingConstraints holdingConstraints) {
		int min = Integer.MAX_VALUE;
		int max = 0;
		Set<WidgetType> wtypes = slope.keySet();
		for (WidgetType wtype : wtypes) {
			int m = slope.get(wtype);
			int b = bias.get(wtype);
			if (min > b) {
				min = b;
			}

			max += b + m * holdingConstraints.maxHoldings(wtype);
		}

		return new Interval(min, max);
	}

	@Override
	public Integer evaluate(Map<WidgetType, Integer> x)
			throws IllegalArgumentException {
		int cost = 0;
		Set<WidgetType> wtypes = x.keySet();
		for (WidgetType wtype : wtypes) {
			int v = x.get(wtype);
			int m = _slope.get(wtype);
			int b = _bias.get(wtype);
			int y = m * v + b;
			cost += y;
		}
		if (cost < 0) {
			return 0;
		} else {
			return cost;
		}
	}

	@Override
	public Interval range() {
		return new Interval(_range);
	}

	@Override
	public List<WidgetType> wtypes() {
		return _wtypes;
	}

}