package tmrutil.learning.rl.sim;

import java.awt.Point;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.math.VectorOps;
import tmrutil.math.metrics.EuclideanPointDistance;
import tmrutil.stats.Random;
import tmrutil.util.AStar;
import tmrutil.util.Interval;
import tmrutil.util.ListUtil;

/**
 * A simulator for the Taxi benchmark reinforcement learning task.
 * 
 * @author Timothy A. Mann
 * 
 */
public class TaxiSimulator extends Simulator<FactoredState,FactoredState, Integer> {

	public static final double MOVE_FAIL_PROB = 0.2;

	public static final int MIN_REWARD = -10;
	public static final int MAX_REWARD = 20;

	public static final int UP_ACTION = 0;
	public static final int DOWN_ACTION = 1;
	public static final int LEFT_ACTION = 2;
	public static final int RIGHT_ACTION = 3;
	public static final int PICKUP_ACTION = 4;
	public static final int DROP_ACTION = 5;
	public static final int NUM_ACTIONS = 6;

	public static final int TAXI_WIDTH = 5;
	public static final int TAXI_HEIGHT = 5;

	public static final int AGENT_X = 0;
	public static final int AGENT_Y = 1;
	public static final int DEST = 2;
	public static final int PASSENGER = 3;
	public static final int STATE_SIZE = 4;

	public static final Point RED = new Point(0, 4);
	public static final Point BLUE = new Point(3, 0);
	public static final Point GREEN = new Point(4, 4);
	public static final Point YELLOW = new Point(0, 0);

	public static final List<Point> LANDMARKS = new ArrayList<Point>();
	static {
		LANDMARKS.add(RED);
		LANDMARKS.add(BLUE);
		LANDMARKS.add(GREEN);
		LANDMARKS.add(YELLOW);
	}
	
	public static final int[] MAX_VALUES = {TAXI_WIDTH-1, TAXI_HEIGHT-1, LANDMARKS.size()-1, LANDMARKS.size()};

	public static final Set<Line2D> WALLS = new HashSet<Line2D>();
	static {
		WALLS.add(new Line2D.Double(1.5, 4, 1.5, 2.5));

		WALLS.add(new Line2D.Double(0.5, 0, 0.5, 1.5));

		WALLS.add(new Line2D.Double(2.5, 0, 2.5, 1.5));
	}
	
	private ReinforcementSignal<FactoredState,Integer> _rsignal;

	public TaxiSimulator() {
		_rsignal = rewardSignal();
	}

	private boolean isValidState(FactoredState state) {
		if (state.size() != STATE_SIZE) {
			return false;
		}
		Point agent = new Point(state.component(AGENT_X), state.component(AGENT_Y));
		if (agent.x < 0 || agent.x >= TAXI_WIDTH || agent.y < 0
				|| agent.y >= TAXI_HEIGHT) {
			return false;
		}
		if (state.component(DEST) < 0 || state.component(DEST) >= LANDMARKS.size()) {
			return false;
		}
		if (state.component(PASSENGER) < 0 || state.component(PASSENGER) > LANDMARKS.size()) {
			return false;
		}
		return true;
	}

	private static int landmarkNumber(Point loc) {
		for (int i = 0; i < LANDMARKS.size(); i++) {
			if (LANDMARKS.get(i).equals(loc)) {
				return i;
			}
		}
		return LANDMARKS.size();
	}

	private static boolean isLandmark(Point loc) {
		return LANDMARKS.contains(loc);
	}

	private static boolean isMoveAction(Integer action) {
		return action >= UP_ACTION && action <= RIGHT_ACTION;
	}

	private static boolean isHoldingPassenger(int[] state) {
		return (state[PASSENGER] == LANDMARKS.size());
	}

	private static boolean isValidLocation(Point p) {
		if (p.x < 0 || p.x >= TAXI_WIDTH || p.y < 0 || p.y >= TAXI_HEIGHT) {
			return false;
		}
		return true;
	}

	private static boolean isValidMove(Point prevLoc, Point newLoc) {
		if (isValidLocation(prevLoc) && isValidLocation(newLoc)) {
			Line2D line = new Line2D.Double(prevLoc, newLoc);
			for (Line2D wall : WALLS) {
				if (wall.intersectsLine(line)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private Point applyMoveAction(Point agent, Integer action) {
		Point newLoc = new Point(agent);
		if (action == UP_ACTION) {
			newLoc.y = agent.y + 1;
		}
		if (action == DOWN_ACTION) {
			newLoc.y = agent.y - 1;
		}
		if (action == LEFT_ACTION) {
			newLoc.x = agent.x - 1;
		}
		if (action == RIGHT_ACTION) {
			newLoc.x = agent.x + 1;
		}
		return newLoc;
	}

	@Override
	public OptionOutcome<FactoredState,FactoredState> samplePrimitive(FactoredState state,
			Integer action) {
		
		if (!isValidState(state)) {
			throw new IllegalArgumentException("Detected invalid state!");
		}

		int[] istate = new int[state.size()];
		for(int i=0;i<state.size();i++){
			istate[i] = state.component(i);
		}
		
		Point agent = new Point(istate[AGENT_X], istate[AGENT_Y]);
		int[] newState = Arrays.copyOf(istate, STATE_SIZE);

		if (istate[PASSENGER] == istate[DEST]) {
			FactoredState tstate = new FactoredStateImpl(newState,MAX_VALUES);
			return new OptionOutcome<FactoredState,FactoredState>(tstate, tstate, 0, 0, 1);
		}

		if (isMoveAction(action)) {

			Point newLoc = applyMoveAction(agent, action);
			double rand = Random.uniform();
			if (rand < MOVE_FAIL_PROB) {
				int raction = Random.nextInt(NUM_ACTIONS);
				newLoc = applyMoveAction(agent, raction);
			}
			if (isValidMove(agent, newLoc)) {
				newState[AGENT_X] = newLoc.x;
				newState[AGENT_Y] = newLoc.y;
			}

		} else {
			if (action == PICKUP_ACTION) {
				if (istate[PASSENGER] == landmarkNumber(agent)) {
					newState[PASSENGER] = LANDMARKS.size();
				}
			} else {
				if (isHoldingPassenger(istate)) {
					if (isLandmark(agent)) {
						newState[PASSENGER] = landmarkNumber(agent);
					}
				}
			}
		}

		double reward = _rsignal.evaluate(state, action);
		FactoredState fnewState = new FactoredStateImpl(newState,MAX_VALUES);
		OptionOutcome<FactoredState,FactoredState> outcome = new OptionOutcome<FactoredState,FactoredState>(fnewState, fnewState,
				reward, reward, 1);
		return outcome;
	}
	
	public static final ReinforcementSignal<FactoredState,Integer> rewardSignal(){
		return new ReinforcementSignal<FactoredState,Integer>(){

			@Override
			public double evaluate(FactoredState state, Integer action) {
				int[] istate = new int[state.size()];
				for(int i=0;i<state.size();i++){
					istate[i] = state.component(i);
				}
				
				Point agent = new Point(istate[AGENT_X], istate[AGENT_Y]);
				if(istate[DEST] == istate[PASSENGER]){
					return 0;
				}
				
				double reward = -1;
				if(action == DROP_ACTION){
					if(isHoldingPassenger(istate)){
						reward = MIN_REWARD;
						if(isLandmark(agent) && landmarkNumber(agent) == istate[DEST]){
							reward = MAX_REWARD;
						}
					}
				}
				return reward;
			}
			
		};
	}

	public static final Interval rewardInterval() {
		return new Interval(MIN_REWARD, MAX_REWARD);
	}

	public static final Collection<Integer> validActions() {
		return ListUtil.range(NUM_ACTIONS);
	}

	public static class NavigateOption implements Option<FactoredState, Integer> {
		private Point _target;
		private boolean[][] _amat;
		private List<Point> _nodes;

		public NavigateOption(Point target) {
			_target = target;
			_amat = buildAMat();
			_nodes = new ArrayList<Point>(TAXI_WIDTH * TAXI_HEIGHT);
			for (int i = 0; i < TAXI_WIDTH * TAXI_HEIGHT; i++) {
				_nodes.add(null);
			}
			for (int x = 0; x < TAXI_WIDTH; x++) {
				for (int y = 0; y < TAXI_HEIGHT; y++) {
					Point p = new Point(x, y);
					int ind = pointToInt(p);
					_nodes.set(ind, p);
				}
			}
		}

		@Override
		public Integer policy(FactoredState state) {
			if (state.component(PASSENGER) != state.component(DEST)) {
				Point s = new Point(state.component(AGENT_X), state.component(AGENT_Y));
				return recommendAction(s, _target);
			} else {
				return UP_ACTION;
			}
		}

		@Override
		public double terminationProb(FactoredState state, int duration) {
			Point s = new Point(state.component(AGENT_X), state.component(AGENT_Y));
			return _target.equals(s) ? 1 : 0;
		}

		@Override
		public boolean inInitialSet(FactoredState state) {
			Point s = new Point(state.component(AGENT_X), state.component(AGENT_Y));
			return !_target.equals(s);
		}

		private Integer recommendAction(Point agent, Point target) {
			AStar<Point> astar = new AStar<Point>(_nodes, _amat,
					new EuclideanPointDistance<Point>());
			List<Point> path = astar.findPath(agent, target);
			if (path.size() >= 2) {
				Point nextPoint = path.get(1);
				if (nextPoint.x - agent.x > 0) {
					return RIGHT_ACTION;
				}
				if (nextPoint.x - agent.x < 0) {
					return LEFT_ACTION;
				}
				if (nextPoint.y - agent.y > 0) {
					return UP_ACTION;
				}
				if (nextPoint.y - agent.y < 0) {
					return DOWN_ACTION;
				}
			}
			return DROP_ACTION;
		}

		private static Integer pointToInt(Point p) {
			return (p.y * TAXI_WIDTH) + p.x;
		}

		private static Point intToPoint(Integer i) {
			int x = i % TAXI_WIDTH;
			int y = i / TAXI_WIDTH;
			Point p = new Point(x, y);
			return p;
		}

		private static boolean[][] buildAMat() {
			boolean[][] amat = new boolean[TAXI_WIDTH * TAXI_HEIGHT][TAXI_WIDTH
					* TAXI_HEIGHT];
			for (int x1 = 0; x1 < TAXI_WIDTH; x1++) {
				for (int y1 = 0; y1 < TAXI_HEIGHT; y1++) {
					Point p1 = new Point(x1, y1);
					int i1 = pointToInt(p1);
					for (int x2 = 0; x2 < TAXI_WIDTH; x2++) {
						for (int y2 = 0; y2 < TAXI_HEIGHT; y2++) {
							Point p2 = new Point(x2, y2);
							int i2 = pointToInt(p2);

							boolean notEqual = !p1.equals(p2);
							boolean nearby = (Math.abs(x1 - x2)
									+ Math.abs(y1 - y2) <= 1);
							boolean validMove = isValidMove(p1, p2);
							boolean valid = notEqual && nearby && validMove;
							amat[i1][i2] = valid;
						}
					}
				}
			}
			return amat;
		}
	}

	@Override
	public FactoredState observe(Integer previousAction,
			FactoredState resultState) {
		return resultState;
	}

}
