package tmrutil.learning.rl.sim;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JFrame;

import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DrawableTaskObserver;
import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.PolicyWrapperLearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.stats.Random;

/**
 * A partially observable helicopter tracking simulation.
 * 
 * @author Timothy A. Mann
 * 
 */
public class HeliTrackSimulator extends
		Simulator<FactoredState, double[], Integer> {

	/**
	 * Draws the current state of the helicopter target tracking task.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class HeliTrackDrawable implements Drawable {
		private Dimension _envDims;
		private FactoredState _state;
		private List<Point> _hidden;
		private double _visualRadius;

		public HeliTrackDrawable() {
			_envDims = null;
			_state = null;
			_hidden = null;
			_visualRadius = 0;
		}

		public void update(FactoredState state, List<Point> hidden,
				Dimension dims, double visualRadius) {
			_state = state;
			_hidden = hidden;
			_envDims = dims;
			_visualRadius = visualRadius;
		}

		@Override
		public void draw(Graphics2D g, int width, int height) {
			if (_envDims != null && _state != null) {
				AffineTransform xform = new AffineTransform();
				xform.scale(width, height);
				BasicStroke bstroke = new BasicStroke(1.0f / Math.max(width,
						height));
				g.transform(xform);
				g.setStroke(bstroke);

				g.setColor(Color.WHITE);
				g.fill(new Rectangle2D.Double(0, 0, 1, 1));

				double twidth = 1.0 / _envDims.width;
				double theight = 1.0 / _envDims.height;

				g.setColor(Color.RED);
				Point hloc = helicopter(_state);
				Point tloc = target(_state);
				Rectangle2D target = new Rectangle2D.Double(tloc.x * twidth,
						tloc.y * theight, twidth, theight);
				g.fill(target);

				for (int x = 0; x < _envDims.width; x++) {
					for (int y = 0; y < _envDims.height; y++) {

						Rectangle2D cell = new Rectangle2D.Double(x * twidth, y
								* theight, twidth, theight);

						Rectangle2D trueCell = new Rectangle2D.Double(x, y, 1,
								1);
						Point2D center = new Point2D.Double(
								trueCell.getCenterX(), trueCell.getCenterY());

						Color color = null;
						Point xy = new Point(x, y);
						boolean isHidden = _hidden.contains(xy);
						boolean inVizRadius = center.distance(hloc) > _visualRadius;
						if (isHidden || inVizRadius) {
							if (isHidden && inVizRadius) {
								color = new Color(0, 0, 0, 200);
							} else {
								color = new Color(0, 0, 0, 100);
							}
						} else {
							color = new Color(0, 0, 0, 0);
						}
						g.setColor(color);
						g.fill(cell);
					}
				}

				g.setColor(new Color(0, 0, 255, 100));
				g.fill(new Rectangle2D.Double(hloc.x * twidth,
						hloc.y * theight, twidth, theight));
			}
		}

	}

	public static class RandomPolicy implements Policy<double[], Integer> {

		@Override
		public Integer policy(double[] state) {
			return Random.nextInt(NUM_ACTIONS);
		}

	}
	
	public static class RandomSmoothPolicy implements Policy<double[],Integer>{

		private Integer _lastAction;
		private double _termProb = 0.1;
		
		@Override
		public Integer policy(double[] state) {
			if(_lastAction == null || Random.uniform() < _termProb){
				_lastAction = Random.nextInt(NUM_ACTIONS);
			}
			return _lastAction;
		}
		
	}

	public static class HeliTrackTask extends Task<double[], Integer> implements
			Drawable {
		private FactoredState _state;
		private HeliTrackSimulator _sim;
		private double _visualRadius;
		private double _lastReward;

		private HeliTrackDrawable _drawable;

		public HeliTrackTask() {
			_state = null;
			_visualRadius = 10;
			
			Dimension dims = new Dimension(60, 60);
			List<Point> hidden = new ArrayList<Point>();
			for(int i=0; i< (dims.width * dims.height)/8; i++){
				int x = Random.nextInt(dims.width);
				int y = Random.nextInt(dims.height);
				Point h = new Point(x,y);
				hidden.add(h);
			}
			
			_sim = new HeliTrackSimulator(dims, hidden,
					_visualRadius, new RandomPolicy());

			_drawable = new HeliTrackDrawable();

			reset();
		}

		@Override
		public void draw(Graphics2D g, int width, int height) {
			_drawable.update(_state, _sim._hidden, _sim.environment(),
					_visualRadius);
			_drawable.draw(g, width, height);
		}

		@Override
		public double[] getState() {
			if (_state == null) {
				return new double[] { -1, -1, -1, -1 };
			} else {
				return _sim.observe(null, _state);
			}
		}

		@Override
		public void execute(Integer action) {
			OptionOutcome<FactoredState, double[]> oc = _sim.samplePrimitive(
					_state, action);
			_state = oc.terminalState();
			_lastReward = oc.cumR();
		}

		@Override
		public double evaluate() {
			return _lastReward;
		}

		@Override
		public void reset() {
			_lastReward = 0;

			int hx = Random.nextInt(_sim._dims.width);
			int hy = Random.nextInt(_sim._dims.height);
			int tx = Random.nextInt(_sim._dims.width);
			int ty = Random.nextInt(_sim._dims.height);
			int[] state = new int[NUM_STATE_VARS];
			state[TARGET_X] = tx;
			state[TARGET_Y] = ty;
			state[HELI_X] = hx;
			state[HELI_Y] = hy;

			_state = new FactoredStateImpl(state, _sim._maxVals);
		}

		@Override
		public boolean isFinished() {
			return false;
		}

	}

	/**
	 * The index of the target's X coordinate.
	 */
	public static final int TARGET_X = 0;
	/**
	 * The index of the target's Y coordinate.
	 */
	public static final int TARGET_Y = 1;
	/**
	 * The index of the helicopter's X coordinate.
	 */
	public static final int HELI_X = 2;
	/**
	 * The index of the helicopter's Y coordinate.
	 */
	public static final int HELI_Y = 3;
	public static final int NUM_STATE_VARS = 4;

	public static final int NULL_ACTION = 0;
	public static final int NORTH_ACTION = 1;
	public static final int SOUTH_ACTION = 2;
	public static final int EAST_ACTION = 3;
	public static final int WEST_ACTION = 4;
	public static final int NUM_ACTIONS = 5;

	private Dimension _dims;
	private List<Point> _hidden;
	private double _visualRadius;
	private Policy<double[], Integer> _targetPolicy;
	private int[] _maxVals;

	public HeliTrackSimulator(Dimension dimensions, Collection<Point> hidden,
			double visualRadius, Policy<double[], Integer> targetPolicy) {
		_dims = new Dimension(dimensions);
		_hidden = new ArrayList<Point>(hidden);
		_visualRadius = visualRadius;
		_targetPolicy = targetPolicy;

		_maxVals = new int[NUM_STATE_VARS];
		_maxVals[TARGET_X] = _dims.width;
		_maxVals[TARGET_Y] = _dims.height;
		_maxVals[HELI_X] = _dims.width;
		_maxVals[HELI_Y] = _dims.height;
	}

	/**
	 * Returns the dimensions of this environment.
	 * 
	 * @return the dimensions of this environment
	 */
	public Dimension environment() {
		return new Dimension(_dims);
	}

	@Override
	public double[] observe(Integer previousAction, FactoredState resultState) {

		Point hloc = helicopter(resultState);
		Point tloc = target(resultState);
		boolean isTargetVisible = false;
		if (hloc.distance(tloc) < _visualRadius && !_hidden.contains(tloc)) {
			isTargetVisible = true;
		}

		double[] obs = new double[NUM_STATE_VARS];
		obs[TARGET_X] = (isTargetVisible) ? tloc.x : -1;
		obs[TARGET_Y] = (isTargetVisible) ? tloc.y : -1;
		obs[HELI_X] = hloc.x;
		obs[HELI_Y] = hloc.y;

		return obs;
	}

	@Override
	public OptionOutcome<FactoredState, double[]> samplePrimitive(
			FactoredState state, Integer action) {

		Point hloc = helicopter(state);
		Point tloc = target(state);
		hloc = processAction(hloc, action);
		tloc = processAction(tloc, _targetPolicy.policy(observe(null, state)));

		int[] newStateA = new int[4];
		newStateA[TARGET_X] = tloc.x;
		newStateA[TARGET_Y] = tloc.y;
		newStateA[HELI_X] = hloc.x;
		newStateA[HELI_Y] = hloc.y;

		FactoredState newState = new FactoredStateImpl(newStateA, _maxVals);

		double reward = reward(newState);
		return new OptionOutcome<FactoredState, double[]>(newState, observe(
				action, newState), reward, reward, 1);
	}

	private Point processAction(Point p, int action) {
		Point cp = new Point(p);
		switch (action) {
		case NORTH_ACTION:
			int ny = cp.y - 1;
			if (ny > 0) {
				cp.y = ny;
			}
			break;
		case SOUTH_ACTION:
			ny = cp.y + 1;
			if (ny < _dims.height) {
				cp.y = ny;
			}
			break;
		case EAST_ACTION:
			int nx = cp.x + 1;
			if (nx < _dims.width) {
				cp.x = nx;
			}
			break;
		case WEST_ACTION:
			nx = cp.x - 1;
			if (nx > 0) {
				cp.x = nx;
			}
			break;
		case NULL_ACTION:
		default:
			// Do nothing
		}
		return cp;
	}

	public double reward(FactoredState state) {
		Point hloc = helicopter(state);
		Point tloc = target(state);
		if (hloc.distance(tloc) < _visualRadius) {
			return 1;
		} else {
			return 0;
		}
	}

	public static Point target(FactoredState state) {
		return new Point(state.component(TARGET_X), state.component(TARGET_Y));
	}

	public static Point helicopter(FactoredState state) {
		return new Point(state.component(HELI_X), state.component(HELI_Y));
	}

	public static void main(String[] args) throws IOException {
		HeliTrackTask task = new HeliTrackTask();
		RandomSmoothPolicy rpolicy = new RandomSmoothPolicy();
		PolicyWrapperLearningSystem<double[], Integer> agent = new PolicyWrapperLearningSystem<double[], Integer>(
				rpolicy);
		JFrame frame = new JFrame();
		frame.setSize(500, 500);
		DrawableTaskObserver<double[],Integer,HeliTrackTask> observer = new DrawableTaskObserver<double[],Integer,HeliTrackTask>(System.getProperty("user.home") + "/Documents/data/helitrack/", new Dimension(500, 500));
		Task.runTask(task, agent, 1, 500, false, observer, frame);
	}
}
