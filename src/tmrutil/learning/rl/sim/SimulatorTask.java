package tmrutil.learning.rl.sim;

import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.stats.Filter;
import tmrutil.stats.GenerativeDistribution;

/**
 * A task wrapper for classes that extend {@link Simulator}. The resulting task
 * uses the Markov states returned by the simulator rather than the partially
 * observable observations.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class SimulatorTask<S, A> extends Task<S, A> {

	private Simulator<S, ?, A> _sim;
	private GenerativeDistribution<S> _initStateDist;
	private Filter<S> _terminalStateFilter;

	private int _timestepsSinceFinish;
	private boolean _finished;
	private S _currentState;
	private OptionOutcome<S, ?> _outcome;

	public SimulatorTask(Simulator<S, ?, A> sim,
			GenerativeDistribution<S> initialStateDistribution,
			Filter<S> terminalStateFilter) {
		_sim = sim;
		_initStateDist = initialStateDistribution;
		_terminalStateFilter = terminalStateFilter;

		reset();
	}

	@Override
	public S getState() {
		return _currentState;
	}

	@Override
	public void execute(A action) {
		if (_finished) {
			_timestepsSinceFinish++;
			_outcome = new OptionOutcome<S, S>(_currentState, _currentState, 0, 0,
					1);
		} else {
			_outcome = _sim.samplePrimitive(_currentState, action);
			_currentState = _outcome.terminalState();
			if (_terminalStateFilter.accept(_currentState)) {
				_finished = true;
			}
		}
	}

	@Override
	public double evaluate() {
		if (_outcome == null) {
			return 0;
		} else {
			return _outcome.dCumR();
		}
	}

	@Override
	public void reset() {
		_timestepsSinceFinish = 0;
		_finished = false;
		_currentState = _initStateDist.sample();
		_outcome = null;
	}

	@Override
	public boolean isFinished() {
		return _finished && _timestepsSinceFinish > 0;
	}

}
