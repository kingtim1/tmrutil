package tmrutil.learning.rl.sim;

import java.util.ArrayList;
import java.util.List;

import tmrutil.learning.rl.DiscreteMarkovDecisionProcess;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;

/**
 * Converts an MDP model into a simulator.
 * 
 * @author Timothy A. Mann
 * 
 */
public class MDPSimulator extends Simulator<Integer, Integer, Integer> {
	
	public static enum RewardDistribution {DETERMINISTIC, BERNOULLI, NORMAL}
	
	private DiscreteMarkovDecisionProcess<Integer, Integer> _mdp;
	private RewardDistribution _rdistType;

	public MDPSimulator(DiscreteMarkovDecisionProcess<Integer, Integer> mdp, RewardDistribution rdistType) {
		_mdp = mdp;
		_rdistType = rdistType;
	}

	@Override
	public Integer observe(Integer previousAction, Integer resultState) {
		return resultState;
	}

	@Override
	public OptionOutcome<Integer, Integer> samplePrimitive(Integer state,
			Integer action) {
		List<Integer> nextStates = new ArrayList<Integer>(
				_mdp.successors(state));
		
		/*double[] cump = new double[nextStates.size()];
		for (int i = 0; i < nextStates.size(); i++) {
			Integer nextState = nextStates.get(i);
			if (i == 0) {
				cump[i] = _mdp.transitionProb(state, action, nextState);
			} else {
				cump[i] = _mdp.transitionProb(state, action, nextState)
						+ cump[i - 1];
			}
		}
		Integer nextState = nextStates.get(Random.sampleFromCDF(cump));*/
		
		double[] p = new double[nextStates.size()];
		for(int i=0;i<nextStates.size();i++){
			Integer nextState = nextStates.get(i);
			p[i] = _mdp.transitionProb(state, action, nextState);
		}
		Integer nextState = nextStates.get(drawRandom(p));
		
		double r = 0;
		switch(_rdistType){
		case BERNOULLI:
			r = Random.uniform() < _mdp.reinforcement(state, action)? 1 : 0;
			break;
		case NORMAL:
			r = Random.normal(_mdp.reinforcement(state, action), 1);
			break;
		case DETERMINISTIC:
		default:
			r = _mdp.reinforcement(state, action);
		}

		return new OptionOutcome<Integer, Integer>(nextState, nextState, r, r, 1);
	}

	/**
	 * Returns a reference to the underlying MDP model.
	 * 
	 * @return a reference to the underlying MDP model
	 */
	public DiscreteMarkovDecisionProcess<Integer, Integer> mdp() {
		return _mdp;
	}

	/**
	 * Draws a random sample according to the distribution specified by
	 * <code>dist</code>.
	 * 
	 * @param dist
	 *            an array containing elements between 0 and 1 whose sum is 1
	 * @return an integer between <code>0</code> and
	 *         <code>dist.length - 1</code>
	 */
	private int drawRandom(double[] dist) {
		double r = Random.uniform();
		double s = 0;
		for (int i = 0; i < dist.length; i++) {
			s += dist[i];
			if (r <= s) {
				return i;
			}
		}
		throw new DebugException(
				"Drawing a random sample from distribution failed!");
	}
}
