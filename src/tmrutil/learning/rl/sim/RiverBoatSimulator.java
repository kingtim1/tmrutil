package tmrutil.learning.rl.sim;

import java.awt.Color;
import java.awt.Graphics2D;

import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.stats.Random;

/**
 * An extremely simple simulator of a one-dimensional task. The purpose of this
 * simulator is primarily for debugging, since it is easy to visualize and
 * understand. The task represents a boat maintaining its position under strong
 * river currents.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RiverBoatSimulator extends Simulator<Integer,Integer, Integer> {

	private static final int ACTION_OFFSET = 1;

	private int _numStates;
	private int _rewardState;

	public RiverBoatSimulator(int numStates) {
		_numStates = numStates;
		_rewardState = (int) ((3.0 / 4.0) * numStates);
	}

	@Override
	public OptionOutcome<Integer,Integer> samplePrimitive(Integer state,
			Integer action) {

		int nextState = 0;
		double reward = 0;

		int a = action - ACTION_OFFSET;
		nextState = state + (int) Random.normal(a, ACTION_OFFSET);
		if (nextState < 0) {
			nextState = 0;
		}
		if (nextState >= _numStates) {
			nextState = _numStates - 1;
		}

		if (nextState == _rewardState) {
			reward = 1;
		}
		return new OptionOutcome<Integer,Integer>(nextState, nextState, reward, reward, 1);
	}

	@Override
	public Integer observe(Integer previousAction, Integer resultState) {
		return resultState;
	}

}
