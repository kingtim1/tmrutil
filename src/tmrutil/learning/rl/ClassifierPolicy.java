package tmrutil.learning.rl;

import tmrutil.learning.Classifier;

/**
 * A wrapper class for classifiers that represent policies.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public class ClassifierPolicy<S, A> implements Policy<S, A> {

	private Classifier<S,Integer> _classifier;
	private ActionSet<S,A> _actions;
	
	public ClassifierPolicy(Classifier<S,Integer> classifier, ActionSet<S,A> actions)
	{
		_classifier = classifier;
		_actions = actions;
	}
	
	@Override
	public A policy(S state) {
		Integer index = _classifier.classify(state);
		return _actions.action(index);
	}

}
