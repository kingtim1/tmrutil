package tmrutil.learning.rl;

import tmrutil.util.DataStore;

/**
 * A learning system interacts with its environment observing a state vector and
 * then performing one of a finite or infinite set of actions.
 * 
 * @author Timothy Mann
 * 
 * @param <A>
 *            the action type that the learning system can perform
 * @param <S>
 *            the state type that the learning system observes
 */
public interface LearningSystem<S, A> extends Policy<S,A> {
	/**
	 * Uses a policy to translate a state observation into an action.
	 * 
	 * @param state
	 *            a state observation
	 * @return an action
	 */
	public A policy(S state);

	/**
	 * Improves the learning system's policy by training on a single example.
	 * 
	 * @param prevState
	 *            the previously visited state
	 * @param action
	 *            the action executed at the previous state
	 * @param newState
	 *            the resulting state
	 * @param reinforcement
	 *            a reinforcement signal for the transition
	 */
	public void train(S prevState, A action, S newState, double reinforcement);

	/**
	 * Called while this learning system is being run on a task. This gives the
	 * learning system an opportunity to record data specific to the details of
	 * this learning algorithm.
	 * 
	 * @param dstore a data storage instance
	 * @param episodeNumber the episode number
	 * @param episodeTimestep the timestep in the current episode
	 */
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep);
}
