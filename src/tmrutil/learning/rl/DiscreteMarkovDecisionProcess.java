package tmrutil.learning.rl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tmrutil.stats.Statistics;
import tmrutil.util.Interval;

/**
 * Interface for discrete Markov decision processes.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface DiscreteMarkovDecisionProcess<S,A> extends
		MarkovDecisionProcess<S, A>
{
	/**
	 * The set of states.
	 * @return set of states
	 */
	public Set<S> states();
	
	/**
	 * The set of valid actions at a specified state.
	 * @param state a state
	 * @return set of valid actions from <code>state</code>
	 */
	public Set<A> actions(S state);
	
	/**
	 * Returns a set of states that are successors of a specified state (and the
	 * state itself). Successor states are states that can be reached from the
	 * specified state in a single action with nonzero probability.
	 * 
	 * @param state
	 *            a state
	 * @return a collection of successors
	 */
	public Set<S> successors(S state);

	/**
	 * Returns a set of states that are predecessors of a specified state (and
	 * the state itself). Predecessor states are states that can reach the
	 * specified state in a single action with nonzero probability.
	 * 
	 * @param state
	 *            a state
	 * @return a collection of predecessor states
	 */
	public Set<S> predecessors(S state);

	/**
	 * Returns the number of states in this MDP.
	 * 
	 * @return the number of states
	 */
	public int numberOfStates();

	/**
	 * Returns the number of actions in this MDP.
	 * 
	 * @return the number of actions
	 */
	public int numberOfActions();

	/**
	 * Returns a bound on the maximum possible one step reward.
	 * 
	 * @return a bound on the maximum possible one step reward.
	 */
	public double rmax();

	/**
	 * Returns a bound on the minimum possible one step reward.
	 * 
	 * @return a bound on the minimum possible one step reward
	 */
	public double rmin();

	/**
	 * If this Markov decision process is not estimated from data this method
	 * always returns true. If it is estimated from samples, then some
	 * state-action pairs may not have enough samples to be properly estimated.
	 * This method should return true if enough samples of the state-action pair
	 * have been observed so that it can be considered well-modeled. Otherwise
	 * this method should return false.
	 * 
	 * @param state a state
	 * @param action an action
	 * @return true if the state-action pair is well-modeled; otherwise false
	 */
	public boolean isKnown(Integer state, Integer action);

	/**
	 * A basic discrete Markov decision process model.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class Model implements DiscreteMarkovDecisionProcess<Integer,Integer>
	{
		private int _numStates;
		private int _numActions;
		private double[][][] _transProbs;
		private double[][] _reinforcements;
		private double _discountFactor;

		public Model(int numStates, int numActions, double[][][] transProbs,
				double[][] reinforcements, double discountFactor)
		{
			if (numStates < 1) {
				throw new IllegalArgumentException(
						"The number of states must be greater than 0.");
			}
			_numStates = numStates;
			if (numActions < 1) {
				throw new IllegalArgumentException(
						"The number of actions must be greater than 0.");
			}
			_numActions = numActions;

			setTransProbs(transProbs);
			setReinforcements(reinforcements);

			if (discountFactor < 0 || discountFactor > 1) {
				throw new IllegalArgumentException(
						"The discount factor cannot be outside of the range [0, 1].");
			}
			_discountFactor = discountFactor;
		}
		

		@Override
		public int numberOfActions()
		{
			return _numActions;
		}

		@Override
		public int numberOfStates()
		{
			return _numStates;
		}

		@Override
		public double getDiscountFactor()
		{
			return _discountFactor;
		}

		@Override
		public double rmax()
		{
			return Statistics.max(_reinforcements);
		}

		@Override
		public double rmin()
		{
			return Statistics.min(_reinforcements);
		}

		public void setReinforcements(double[][] reinforcements)
		{
			_reinforcements = reinforcements;
		}

		public void setTransProbs(double[][][] transProbs)
		{
			_transProbs = transProbs;
		}

		@Override
		public double reinforcement(Integer state, Integer action)
		{
			return _reinforcements[state][action];
		}

		@Override
		public double transitionProb(Integer state, Integer action,
				Integer resultState)
		{
			return _transProbs[state][action][resultState];
		}

		@Override
		public Set<Integer> predecessors(Integer state)
		{
			Set<Integer> preds = new HashSet<Integer>();
			for (int predState = 0; predState < numberOfStates(); predState++) {
				for (int a = 0; a < numberOfActions(); a++) {
					if (transitionProb(predState, a, state) > 0) {
						preds.add(predState);
						break;
					}
				}
			}
			return preds;
		}

		@Override
		public Set<Integer> successors(Integer state)
		{
			Set<Integer> succs = new HashSet<Integer>();
			for (int succState = 0; succState < numberOfStates(); succState++) {
				for (int a = 0; a < numberOfActions(); a++) {
					if (transitionProb(state, a, succState) > 0) {
						succs.add(succState);
						break;
					}
				}
			}
			return succs;
		}

		@Override
		public boolean isKnown(Integer state, Integer action)
		{
			return true;
		}


		@Override
		public Set<Integer> states() {
			Set<Integer> states = new HashSet<Integer>();
			for(int i=0;i<numberOfStates();i++){
				states.add(i);
			}
			return states;
		}


		@Override
		public Set<Integer> actions(Integer state) {
			Set<Integer> actions = new HashSet<Integer>();
			for(int i=0;i<numberOfActions();i++){
				actions.add(i);
			}
			return actions;
		}

	}

	/**
	 * An abstract estimator for discrete markove decision processes.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static abstract class Estimator implements
			DiscreteMarkovDecisionProcess<Integer,Integer>
	{
		private int _numStates;
		private int _numActions;

		private int[][] _numVisitsUntilKnown;

		private double _discountFactor;

		public Estimator(int numStates, int numActions, double discountFactor,
				int numVisitsUntilKnown)
		{
			this(numStates, numActions, discountFactor, buildNumVisitsArray(
					numStates, numActions, numVisitsUntilKnown));
		}

		public Estimator(int numStates, int numActions, double discountFactor,
				int[][] numVisitsUntilKnown)
		{
			if (numStates < 1) {
				throw new IllegalArgumentException(
						"The number of states must be positive.");
			}
			_numStates = numStates;
			if (numActions < 1) {
				throw new IllegalArgumentException(
						"The number of actions must be positive.");
			}
			_numActions = numActions;

			if (discountFactor < 0 || discountFactor > 1) {
				throw new IllegalArgumentException(
						"The discount factor must be in the interval [0, 1].");
			}
			_discountFactor = discountFactor;

			_numVisitsUntilKnown = new int[numStates][numActions];
			for (int s = 0; s < numStates; s++) {
				for (int a = 0; a < numActions; a++) {
					if (numVisitsUntilKnown[s][a] < 0) {
						throw new IllegalArgumentException(
								"The number of visits required before a state-action pair is known must be at least 0.");
					}
					_numVisitsUntilKnown[s][a] = numVisitsUntilKnown[s][a];
				}
			}
		}

		public static final int[][] buildNumVisitsArray(int numStates,
				int numActions, int numVisitsUntilKnown)
		{
			int[][] numVisits = new int[numStates][numActions];
			for (int s = 0; s < numStates; s++) {
				for (int a = 0; a < numActions; a++) {
					numVisits[s][a] = numVisitsUntilKnown;
				}
			}
			return numVisits;
		}

		/**
		 * Updates this Markov decision process model given a state transition
		 * observation.
		 * 
		 * @param state
		 *            the previous state
		 * @param action
		 *            the applied action at the previous state
		 * @param resultState
		 *            the resulting state
		 * @param reinforcement
		 *            the immediate reinforcement received
		 * @return true if the status of the state-action pair went from unknown
		 *         to known
		 */
		public abstract boolean update(Integer state, Integer action,
				Integer resultState, double reinforcement);

		@Override
		public double transitionProb(Integer state, Integer action,
				Integer resultState)
		{
			int saCount = getCount(state, action);
			if (saCount < visitsUntilKnown(state, action) || saCount == 0) {
				return transitionProbUnknown(state, action, resultState);
			} else {
				double tprob = getCount(state, action, resultState)
						/ (double) saCount;
				return tprob;
			}
		}

		/**
		 * This method is called when the transition probability for a state
		 * transition observation is unknown. The implementing subclass should
		 * provide some suitable default such as pessimistic initialization.
		 * 
		 * @param state
		 *            the previous state
		 * @param action
		 *            the action applied at the previous state
		 * @param resultState
		 *            the resulting state
		 * @return a probability
		 */
		public abstract double transitionProbUnknown(Integer state,
				Integer action, Integer resultState);

		/**
		 * Returns the number of visits to a particular state.
		 * 
		 * @param state
		 *            a state
		 * @return the number of visits
		 */
		public abstract int getCount(Integer state);

		/**
		 * Returns the number of visits to a particular state-action pair.
		 * 
		 * @param state
		 *            a state
		 * @param action
		 *            an action
		 * @return the number of visits
		 */
		public abstract int getCount(Integer state, Integer action);

		/**
		 * Returns the number of visits to a particular state-action-state
		 * observation.
		 * 
		 * @param state
		 *            a state
		 * @param action
		 *            an action
		 * @param resultState
		 *            a resulting state
		 * @return the number of visits
		 */
		public abstract int getCount(Integer state, Integer action,
				Integer resultState);

		@Override
		public int numberOfActions()
		{
			return _numActions;
		}

		@Override
		public int numberOfStates()
		{
			return _numStates;
		}

		@Override
		public double getDiscountFactor()
		{
			return _discountFactor;
		}

		/**
		 * Returns true if the specified state-action pair is considered known.
		 * Otherwise false is returned.
		 * 
		 * @param state
		 *            a state
		 * @param action
		 *            an action
		 * @return true if the state-action pair is known; otherwise false
		 */
		public boolean isKnown(Integer state, Integer action)
		{
			return getCount(state, action) >= visitsUntilKnown(state, action);
		}

		/**
		 * Returns an integer determining the number of visits to a state-action
		 * pair before it is assumed to be known.
		 * 
		 * @return the number of visits needed to a state-action pair before it
		 *         is assumed to be known
		 */
		public int visitsUntilKnown(Integer state, Integer action)
		{
			return _numVisitsUntilKnown[state][action];
		}

		/**
		 * Resets the estimator.
		 */
		public abstract void reset();
	}

	/**
	 * A memory intensive estimator for discrete Markov decision processes. The
	 * advantage is that look-up times for reinforcements and transition
	 * probabilities are constant.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static abstract class ArrayEstimator extends Estimator
	{
		private int[] _sVisits;
		private int[][] _saVisits;
		private int[][][] _sasVisits;
		private double[][] _reinforcements;
		private Map<Integer, Set<Integer>> _succs;
		private Map<Integer, Set<Integer>> _preds;

		private Interval _rewardInterval;

		public ArrayEstimator(int numStates, int numActions,
				double discountFactor, Interval rewardInterval,
				int numVisitsUntilKnown)
		{
			super(numStates, numActions, discountFactor, numVisitsUntilKnown);
			_sVisits = new int[numStates];
			_saVisits = new int[numStates][numActions];
			_sasVisits = new int[numStates][numActions][numStates];
			_reinforcements = new double[numStates][numActions];
			_succs = new HashMap<Integer, Set<Integer>>();
			_preds = new HashMap<Integer, Set<Integer>>();

			_rewardInterval = rewardInterval;
		}

		@Override
		public boolean update(Integer state, Integer action,
				Integer resultState, double reinforcement)
		{
			boolean oldPairStatus = getCount(state, action) >= visitsUntilKnown(
					state, action);

			// Increment the state visit counter
			_sVisits[resultState]++;

			// Increment the state-action visit counter
			_saVisits[state][action]++;

			// Increment the state-action-state visit counter
			_sasVisits[state][action][resultState]++;

			// Update the reinforcement estimate
			_reinforcements[state][action] += reinforcement;

			// Update the successor state list
			Set<Integer> succs = _succs.get(state);
			if (succs == null) {
				succs = new HashSet<Integer>();
				succs.add(resultState);
				_succs.put(state, succs);
			} else if (!succs.contains(resultState)) {
				succs.add(resultState);
			}

			// Update the predecessor state list
			Set<Integer> spred = _preds.get(resultState);
			if (spred == null) {
				spred = new HashSet<Integer>();
				spred.add(state);
				_preds.put(resultState, spred);
			} else if (!spred.contains(state)) {
				spred.add(state);
			}

			boolean newPairStatus = getCount(state, action) >= visitsUntilKnown(
					state, action);
			if (!oldPairStatus && newPairStatus) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		public double rmax()
		{
			if (_rewardInterval == null) {
				return 1;
			} else {
				return _rewardInterval.getMax();
			}
		}

		@Override
		public double rmin()
		{
			if (_rewardInterval == null) {
				return 0;
			} else {
				return _rewardInterval.getMin();
			}
		}

		@Override
		public double reinforcement(Integer state, Integer action)
		{
			Integer saCount = getCount(state, action);
			if (saCount < visitsUntilKnown(state, action)) {
				return reinforcementUnknown(state, action);
			} else {
				return _reinforcements[state][action] / saCount;
			}
		}

		@Override
		public Set<Integer> successors(Integer state)
		{
			Set<Integer> succs = _succs.get(state);
			if (succs == null) {
				succs = new HashSet<Integer>();
				succs.add(state);
				return succs;
			} else {
				succs.add(state);
				return new HashSet<Integer>(succs);
			}
		}

		protected Map<Integer, Set<Integer>> getSuccessorMap()
		{
			return _succs;
		}

		@Override
		public Set<Integer> predecessors(Integer state)
		{
			Set<Integer> spred = _preds.get(state);
			if (spred == null) {
				spred = new HashSet<Integer>();
				spred.add(state);
				return spred;
			} else {
				spred.add(state);
				return new HashSet<Integer>(spred);
			}
		}

		protected Map<Integer, Set<Integer>> getPredecessorMap()
		{
			return _preds;
		}

		/**
		 * This method is called when the reinforcement for a state transition
		 * observation is unknown. The implementing subclass should provide some
		 * suitable default reinforcement.
		 * 
		 * @param state
		 *            the previous state
		 * @param action
		 *            the action applied to the previous state
		 * @return a reinforcement
		 */
		public abstract double reinforcementUnknown(Integer state,
				Integer action);

		@Override
		public void reset()
		{
			int nStates = numberOfStates();
			int nActions = numberOfActions();
			for (int s1 = 0; s1 < nStates; s1++) {
				_sVisits[s1] = 0;
				for (int a = 0; a < nActions; a++) {
					_saVisits[s1][a] = 0;
					_reinforcements[s1][a] = 0;
					for (int s2 = 0; s2 < nStates; s2++) {
						_sasVisits[s1][a][s2] = 0;
					}
				}
			}
			_succs.clear();
			_preds.clear();
		}

		@Override
		public int getCount(Integer state)
		{
			return _sVisits[state];
		}

		@Override
		public int getCount(Integer state, Integer action)
		{
			return _saVisits[state][action];
		}

		@Override
		public int getCount(Integer state, Integer action, Integer resultState)
		{
			return _sasVisits[state][action][resultState];
		}
	}

}
