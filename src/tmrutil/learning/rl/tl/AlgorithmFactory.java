package tmrutil.learning.rl.tl;

import tmrutil.learning.rl.LearningSystem;

public interface AlgorithmFactory<S,A,L extends LearningSystem<S,A>,K>
{
	public L newInstance(K priorKnowledge);
}
