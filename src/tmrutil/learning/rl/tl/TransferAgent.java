package tmrutil.learning.rl.tl;

import tmrutil.learning.rl.LearningSystem;

/**
 * An interface for transfer learning agents.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 * @param <L> the learning algorithm type
 * @param <K> the prior knowledge type
 */
public interface TransferAgent<S,A,L extends LearningSystem<S,A>,K> extends AlgorithmFactory<S,A,L,K>, Library<K>
{
	public L newInstance(TaskDescription desc);
}
