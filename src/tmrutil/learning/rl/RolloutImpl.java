package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.List;

/**
 * A basic implementation of the rollout interface.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class RolloutImpl<S, A> implements Rollout<S, A> {
	private int _initTime;
	private List<S> _stateSeq;
	private List<A> _actionSeq;

	/**
	 * The sequence of rewards.
	 */
	private List<Double> _rSeq;

	/**
	 * Cumulative execution times of actions.
	 */
	private List<Integer> _cumExecTimes;
	/**
	 * Execution times of actions.
	 */
	private List<Integer> _execTimes;
	private int _lastTime;
	private double _rsum;

	public RolloutImpl(S initState, int initTimestep) {
		_initTime = initTimestep;
		_lastTime = initTimestep;

		_stateSeq = new ArrayList<S>();
		_stateSeq.add(initState);
		_actionSeq = new ArrayList<A>();
		_rSeq = new ArrayList<Double>();
		_execTimes = new ArrayList<Integer>();
		_cumExecTimes = new ArrayList<Integer>();
	}

	@Override
	public S firstState() {
		return _stateSeq.get(0);
	}

	@Override
	public S lastState() {
		return _stateSeq.get(_stateSeq.size() - 1);
	}

	@Override
	public double reinforcementSum() {
		return _rsum;
	}

	@Override
	public double reinforcementSum(double discountFactor) {
		double rsum = 0;
		int timestep = 0;
		for (int i = 0; i < _actionSeq.size(); i++) {
			int etime = _execTimes.get(i);

			for (int j = 1; j <= etime; j++) {
				rsum += Math.pow(discountFactor, timestep + j)
						* _rSeq.get(timestep + j);
			}
			timestep += etime;
		}
		return rsum;
	}

	@Override
	public List<S> stateSequence() {
		return _stateSeq;
	}

	@Override
	public List<A> actionSequence() {
		return _actionSeq;
	}

	@Override
	public List<Double> reinforcementSequence() {
		return _rSeq;
	}

	@Override
	public RLInstance<S, A> getInstance(int index, double discountFactor) {
		S state = _stateSeq.get(index);
		A action = _actionSeq.get(index);
		S nextState = _stateSeq.get(index + 1);
		int etime = _execTimes.get(index);

		int firstTimestep = (index > 0) ? _cumExecTimes.get(index - 1)
				: _initTime;
		double r = 0;
		for (int i = 0; i < etime; i++) {
			int t = (firstTimestep - _initTime) + i;
			r += Math.pow(discountFactor, i) * _rSeq.get(t);
		}

		RLInstance<S, A> instance = new RLInstance<S, A>(state, action,
				nextState, r, etime);
		return instance;
	}

	@Override
	public int numberOfRLInstances() {
		return _stateSeq.size() - 1;
	}

	@Override
	public int initTimestep() {
		return _initTime;
	}

	@Override
	public int lastTimestep() {
		return _lastTime;
	}

	/**
	 * Appends an action, next state, and reinforcement to the end of this
	 * rollout.
	 * 
	 * @param action
	 *            an action
	 * @param nextState
	 *            the resulting next state
	 * @param reinforcement
	 *            the granted reinforcement
	 */
	public void append(A action, S nextState, double reinforcement) {
		List<Double> r = new ArrayList<Double>(1);
		r.add(reinforcement);
		this.append(action, nextState, r);
	}

	/**
	 * Appends an action, terminating state, and a sequence of reinforcements to
	 * the end of this rollout. The length of the sequence of reinforcements
	 * represents the number of timesteps that the action executed before
	 * terminating.
	 * 
	 * @param action
	 *            an action
	 * @param nextState
	 *            the resulting next state
	 * @param reinforcements
	 *            the sequence of granted reinforcements
	 */
	public void append(A action, S nextState, List<Double> reinforcements) {
		_actionSeq.add(action);
		_stateSeq.add(nextState);

		for (Double r : reinforcements) {
			_rsum += r;
		}

		_rSeq.addAll(reinforcements);

		int execTime = reinforcements.size();
		_execTimes.add(execTime);
		_lastTime += execTime;
		_cumExecTimes.add(_lastTime);
	}

	@Override
	public Iterable<RLInstance<S, A>> instances(double discountFactor) {
		int n = numberOfRLInstances();
		List<RLInstance<S, A>> ilist = new ArrayList<RLInstance<S, A>>(n);
		for (int i = 0; i < n; i++) {
			ilist.add(getInstance(i, discountFactor));
		}
		return ilist;
	}

}
