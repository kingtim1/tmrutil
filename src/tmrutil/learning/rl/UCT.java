package tmrutil.learning.rl;

import java.util.Set;

import tmrutil.optim.Optimization;
import tmrutil.util.DataStore;

public class UCT<S, A> extends MonteCarloPlanner<S, A>
{

	private double _alpha;

	public UCT(GenerativeModel<S, A> gmodel, Set<A> validActions, int maxDepth, int numSamples,
			double discountFactor, Optimization opType, double alpha)
	{
		super(gmodel, validActions, maxDepth, discountFactor, opType, numSamples);
		_alpha = alpha;
	}

	@Override
	public void train(S prevState, A action, S newState, double reinforcement)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public boolean terminateSearch(S state, int depth)
	{
		return false;
	}

	@Override
	public A selectAction(int depth, S state)
	{
		boolean maximizing = isMaximizing();
		double bestVal = Double.POSITIVE_INFINITY;
		if (maximizing) {
			bestVal = Double.NEGATIVE_INFINITY;
		}
		A bestAction = null;

		for (A action : validActions()) {
			double q = actionValue(depth, state, action);

			int sCount = stateCount(depth, state);
			int saCount = stateActionCount(depth, state, action);
			double bonus = Double.NEGATIVE_INFINITY;
			if (maximizing) {
				bonus = Double.POSITIVE_INFINITY;
			}
			if (saCount > 0) {
				bonus = 2 * _alpha * Math.sqrt(Math.log(sCount)) / saCount;
			}

			double val = q + bonus;

			if (maximizing) {
				if (val > bestVal) {
					bestVal = val;
					bestAction = action;
				}
			} else {
				if (val < bestVal) {
					bestVal = val;
					bestAction = action;
				}
			}
		}

		return bestAction;
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

}
