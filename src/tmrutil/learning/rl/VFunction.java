package tmrutil.learning.rl;

/**
 * Interface for value functions.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 */
public interface VFunction<S> {
	/**
	 * Returns the value of a specified state.
	 * @param state a state
	 * @return the value
	 */
	public double value(S state);
}
