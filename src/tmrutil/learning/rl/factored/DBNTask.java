package tmrutil.learning.rl.factored;

import tmrutil.learning.rl.DiscreteActionTask;
import tmrutil.learning.rl.Task;
import tmrutil.util.Interval;

/**
 * Represents a task that is factored and represented by a dynamic Bayesian network.
 * @author Timothy A. Mann
 *
 */
public abstract class DBNTask extends DiscreteActionTask<FactoredState>
{
	private Interval _rewardInterval;
	
	public DBNTask(int numActions, Interval rewardInterval)
	{
		super(numActions);
		_rewardInterval = rewardInterval;
	}

	/**
	 * Returns the structure of this dynamic Bayesian network.
	 * @return the structure of this dynamic Bayesian network
	 */
	public abstract DBNStructure getStructure();
	
	/**
	 * Returns the number of states in this task.
	 * @return the number of states in this task
	 */
	public final int numberOfStates()
	{
		return getStructure().numberOfStates();
	}
	
	public double rmax()
	{
		return _rewardInterval.getMax();
	}
	
	public double rmin()
	{
		return _rewardInterval.getMin();
	}
}
