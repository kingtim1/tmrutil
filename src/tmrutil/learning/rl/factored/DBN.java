package tmrutil.learning.rl.factored;

import java.util.Set;
import tmrutil.learning.rl.MarkovDecisionProcess;

/**
 * A Dynamic Bayesian Network is a compact representation representing
 * transitions between states.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface DBN extends MarkovDecisionProcess<FactoredState, Integer>,
		DBNStructure
{

	/**
	 * Returns the probability that a state-action pair will transition to the
	 * specified result state.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param resultState
	 *            the result state
	 * @param resultStateIndex
	 *            the index of the resultState to determine the probability of
	 * @return the transition probability
	 */
	public double transProb(FactoredState state, Integer action,
			FactoredState resultState, int resultStateIndex);

	/**
	 * Returns the minimum immediate reward.
	 * 
	 * @return minimum reward
	 */
	public double rmin();

	/**
	 * Returns the maximum immediate reward.
	 * 
	 * @return maximum reward
	 */
	public double rmax();

	/**
	 * Returns the set of immediate successor states for the specified state.
	 * 
	 * @param state a state
	 * @return a set of successor states
	 */
	public Set<FactoredState> successors(FactoredState state);

}
