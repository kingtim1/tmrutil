package tmrutil.learning.rl.factored;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tmrutil.learning.rl.GenerativeModel;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * Represents an estimator for a dynamic Bayesian network. This algorithm
 * computes the conditional probability tables (CPTs) given the DBN's structure
 * and samples from the environment.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DBNEstimator implements DBN, GenerativeModel<FactoredState,Integer>
{

	private DBNStructure _structure;

	private Interval _rewardInterval;
	private double _discountFactor;

	private Map<Integer, CPTHelper> _helpers;

	public DBNEstimator(DBNStructure structure, Interval rewardInterval,
			double discountFactor, int numVisitsUntilKnown)
	{
		_structure = structure;
		_rewardInterval = rewardInterval;
		_discountFactor = discountFactor;

		_helpers = new HashMap<Integer, CPTHelper>();
		for (int a = 0; a < structure.numberOfActions(); a++) {
			CPTHelper h = new CPTHelper(a, structure, numVisitsUntilKnown);
			_helpers.put(a, h);
		}
	
	}

	/**
	 * Adds a sample to this estimator. The specified sample will be used to
	 * estimate the conditional probability tables (CPTs) for the DBN.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param resultState
	 *            the state that was transitioned to
	 * @param reinforcement
	 *            the reinforcement received
	 */
	public boolean addSample(FactoredState state, Integer action,
			FactoredState resultState, double reinforcement)
	{
		return _helpers.get(action).addSample(state, resultState, reinforcement);
	}

	@Override
	public double transitionProb(FactoredState state, Integer action,
			FactoredState resultState)
	{
		double tprob = 1;

		if (_helpers.get(action).isKnown(state)) {
			for (int i = 0; i < _structure.numberOfStateVariables(); i++) {
				tprob = tprob * transProb(state, action, resultState, i);
			}
		}else{
			if(!state.equals(resultState)){
				tprob = 0;
			}
		}
		return tprob;
	}

	@Override
	public double reinforcement(FactoredState state, Integer action)
	{
		return _structure.reinforcement(state, action);
	}

	@Override
	public double getDiscountFactor()
	{
		return _discountFactor;
	}

	@Override
	public Set<Integer> parents(Integer action, int componentIndex)
	{
		return _structure.parents(action, componentIndex);
	}

	@Override
	public int choices(int index)
	{
		return _structure.choices(index);
	}

	@Override
	public int numberOfStateVariables()
	{
		return _structure.numberOfStateVariables();
	}
	
	@Override
	public int numberOfStates()
	{
		return _structure.numberOfStates();
	}

	@Override
	public int numberOfActions()
	{
		return _structure.numberOfActions();
	}

	@Override
	public double transProb(FactoredState state, Integer action,
			FactoredState resultState, int resultStateIndex)
	{
		return _helpers.get(action).transProb(state, resultState,
				resultStateIndex);
	}

	@Override
	public Pair<FactoredState, Double> simulate(FactoredState state,
			Integer action)
	{
		CPTHelper h = _helpers.get(action);
		boolean known = h.isKnown(state);
		if(known){
			double reward = reinforcement(state, action);
			FactoredState sampledState = h.sample(state);
			return new Pair<FactoredState,Double>(sampledState, reward);
		}else{
			return new Pair<FactoredState,Double>(state, rmax());
		}
	}
	
	@Override
	public double rmax()
	{
		return _rewardInterval.getMax();
	}
	
	@Override
	public double rmin()
	{
		return _rewardInterval.getMin();
	}

	@Override
	public FactoredState decode(Integer iState)
	{
		return _structure.decode(iState);
	}

	@Override
	public Set<FactoredState> successors(FactoredState state)
	{
		Set<FactoredState> succs = new HashSet<FactoredState>();
		
		// Check which states might be successors by examining the conditional probability tables
		for(int a=0;a<numberOfActions();a++){
			CPTHelper h = _helpers.get(a);
			succs.addAll(h.generateSuccessors(state));
		}
		
		return succs;
	}

}
