package tmrutil.learning.rl.factored;

import tmrutil.learning.rl.LearningSystem;

/**
 * Represents a learning algorithm for problems with factored state spaces.
 * @author Timothy A. Mann
 *
 */
public abstract class FactoredStateLearningSystem implements
		LearningSystem<FactoredState, Integer>
{
	
	private DBNStructure _structure;
	
	public FactoredStateLearningSystem(DBNStructure structure)
	{
		_structure = structure;
	}

	/**
	 * Returns the DBN structure of the task.
	 * @return the DBN structure
	 */
	public DBNStructure getStructure()
	{
		return _structure;
	}
}
