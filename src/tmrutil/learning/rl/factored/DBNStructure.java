package tmrutil.learning.rl.factored;

import java.util.Set;

/**
 * Represents the structure of a dynamic Bayesian network.
 * @author Timothy A. Mann
 *
 */
public interface DBNStructure
{
	/**
	 * Determines the parents of a particular vector component.
	 * 
	 * @param action an action
	 * @param componentIndex
	 *            a component of a valid state vector
	 * @return a set containing the parents of this component
	 */
	public Set<Integer> parents(Integer action, int componentIndex);
	
	/**
	 * Returns the number of possible elements at a component of a vector
	 * specified by <code>index</code>
	 * 
	 * @param index
	 *            the index of a component of this vector
	 * @return the number of possible elements at a component indexed by
	 *         <code>index</code>
	 */
	public int choices(int index);
	
	/**
	 * Decodes a factored state from an integer.
	 * @param iState an integer representing a state
	 * @return a factored state
	 */
	public FactoredState decode(Integer iState);
	
	/**
	 * Returns the number of state variables in a factored state.
	 * @return the number of state variables
	 */
	public int numberOfStateVariables();
	
	/**
	 * Returns the number of states.
	 * @return the number of states
	 */
	public int numberOfStates();
	
	/**
	 * The number of actions.
	 * @return the number of actions
	 */
	public int numberOfActions();
	
	/**
	 * Returns the reinforcement for the specified state-action pair.
	 * @param state a factored state
	 * @param action an action
	 * @return the reinforcement associated with the specified state-action pair
	 */
	public double reinforcement(FactoredState state, Integer action);
}
