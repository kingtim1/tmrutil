package tmrutil.learning.rl.factored;

/**
 * An implementation of a factored state.
 * @author Timothy A. Mann
 *
 */
public class FactoredStateImpl extends FactoredImpl implements FactoredState
{
	
	public FactoredStateImpl(int[] values, int[] maxValues){
		super(values, maxValues);
	}

}
