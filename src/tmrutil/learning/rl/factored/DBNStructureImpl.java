package tmrutil.learning.rl.factored;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.stats.Statistics;
import tmrutil.util.ArrayOps;
import tmrutil.util.Pair;

/**
 * An implementation of DBN structure.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DBNStructureImpl implements DBNStructure
{
	private int _numStates;
	private int[] _maxValues;
	private int[] _upperLimits;
	private int _numActions;

	private List<Map<Integer, Set<Integer>>> _parents;
	private ReinforcementSignal<FactoredState, Integer> _rsignal;

	public DBNStructureImpl(int[] maxValues, int numActions, ReinforcementSignal<FactoredState,Integer> rsignal)
	{
		_maxValues = Arrays.copyOf(maxValues, maxValues.length);
		_upperLimits = new int[_maxValues.length];
		_numStates = 1;
		for(int i=0;i<_maxValues.length;i++){
			_upperLimits[i] = _maxValues[i]+1;
			_numStates *= _upperLimits[i];
		}
		_numActions = numActions;

		_parents = new ArrayList<Map<Integer, Set<Integer>>>();
		for (int i = 0; i < _maxValues.length; i++) {
			Map<Integer, Set<Integer>> pi = new HashMap<Integer, Set<Integer>>();
			for (int a = 0; a < numActions; a++) {
				pi.put(a, new HashSet<Integer>());
			}
			_parents.add(pi);
		}
		
		_rsignal = rsignal;
	}

	/**
	 * Adds a parent index to the specified component index.
	 * 
	 * @param action
	 *            an action
	 * @param componentIndex
	 *            a state variable index
	 * @param parentIndex
	 *            a parent index to add
	 */
	public void addParent(Integer action, int componentIndex, int parentIndex)
	{
		_parents.get(componentIndex).get(action).add(parentIndex);
	}
	
	public void addParents(Integer action, int componentIndex, int[] parents)
	{
		for(int p : parents){
			addParent(action, componentIndex, p);
		}
	}

	/**
	 * Sets the parent indices to a specified component index for an action
	 * (replacing any previously set parents).
	 * 
	 * @param action
	 *            an action
	 * @param componentIndex
	 *            a state variable index
	 * @param parents
	 *            a set of parent state variable indices
	 */
	public void setParents(Integer action, int componentIndex,
			Set<Integer> parents)
	{
		for (Integer i : parents) {
			if (i < 0 || i >= _maxValues.length) {
				throw new IllegalArgumentException(
						"Detected an illegal state variable index in the parents set.");
			}
		}
		_parents.get(componentIndex).put(action, parents);
	}

	@Override
	public Set<Integer> parents(Integer action, int componentIndex)
	{
		return _parents.get(componentIndex).get(action);
	}

	@Override
	public int choices(int index)
	{
		return _maxValues[index]+1;
	}

	@Override
	public int numberOfStateVariables()
	{
		return _maxValues.length;
	}

	@Override
	public int numberOfActions()
	{
		return _numActions;
	}

	@Override
	public double reinforcement(FactoredState state, Integer action)
	{
		return _rsignal.evaluate(state, action);
	}

	@Override
	public FactoredState decode(Integer iState)
	{
		int[] vals = ArrayOps.decode(iState.intValue(), _upperLimits);
		return new FactoredStateImpl(vals, _maxValues);
	}

	@Override
	public int numberOfStates()
	{
		return _numStates;
	}

}
