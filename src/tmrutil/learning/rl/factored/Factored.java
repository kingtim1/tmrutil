package tmrutil.learning.rl.factored;

import java.util.Set;

/**
 * A factored vector is an ordered list of <code>n</code> components. Each of the
 * <code>n</code> components can take one of a finite number of values.
 * 
 * @author Timothy A. Mann
 *
 */
public interface Factored {
	/**
	 * Returns the value of the component at the given index.
	 * 
	 * @param index
	 *            the index of a component of this vector
	 * @return the value of the component
	 */
	public int component(int index);

	/**
	 * Computes a unique identifier for this vector depending on the values of
	 * its components.
	 * 
	 * @return a unique identifier for this vector
	 */
	public int uniqueID();
	
	/**
	 * Computes a unique identifier for this state based on only a subset of the states components.
	 * @param components a set of components
	 * @return a unique identifier for this vector based on the specified subset of state variables
	 */
	public int uniqueID(Set<Integer> components);
	
	/**
	 * Returns the number of possible elements at a component of this vector
	 * specified by <code>index</code>
	 * 
	 * @param index
	 *            the index of a component of this vector
	 * @return the number of possible elements at a component indexed by
	 *         <code>index</code>
	 */
	public int choices(int index);
	
	/**
	 * Returns an array representing the values of each component of this factored state.
	 * @return an array of this state's components
	 */
	public int[] toArray();
	
	/**
	 * Returns a double array representing the values of each component of this factored state.
	 * @return a double array of this states components
	 */
	public double[] toDArray();
	
	/**
	 * Returns the number of components.
	 * 
	 * @return the number of components
	 */
	public int size();
}
