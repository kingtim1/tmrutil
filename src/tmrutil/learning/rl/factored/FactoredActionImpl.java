package tmrutil.learning.rl.factored;

/**
 * An implementation of a factored action.
 * @author Timothy A. Mann
 *
 */
public class FactoredActionImpl extends FactoredImpl implements FactoredAction
{
	
	public FactoredActionImpl(int[] values, int[] maxValues)
	{
		super(values, maxValues);
	}

}
