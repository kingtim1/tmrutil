package tmrutil.learning.rl.factored;

import java.util.Arrays;
import java.util.Set;
import tmrutil.stats.Statistics;
import tmrutil.util.ArrayOps;

/**
 * An implementation of a factored vector.
 * 
 * @author Timothy A. Mann
 * 
 */
public class FactoredImpl implements Factored {
	
	private int[] _values;
	private int[] _maxValues;
	private int[] _upperLimits;

	public FactoredImpl(int[] values, int[] maxValues) {
		if (values == null || maxValues == null) {
			throw new NullPointerException(
					"Cannot construct a factored vector with null values or maxValues arrays.");
		}
		if (values.length != maxValues.length) {
			throw new IllegalArgumentException(
					"The number of components in the values array differs from the number of components in the maxValues array.");
		}
		isValid(values, maxValues);
		
		_values = Arrays.copyOf(values, values.length);
		_maxValues = Arrays.copyOf(maxValues, maxValues.length);
		_upperLimits = Arrays.copyOf(maxValues, maxValues.length);
		for(int i=0;i<_upperLimits.length;i++){
			_upperLimits[i]++;
		}

	}

	private void isValid(int[] values, int[] maxValues) {
		for (int i = 0; i < values.length; i++) {
			if (maxValues[i] < 1) {
				throw new IllegalArgumentException(
						"The maximum number of values must be non-negative. Found "
								+ maxValues[i] + ".");
			}
			if (values[i] < 0 || values[i] > maxValues[i]) {
				throw new IllegalArgumentException(
						"Value for the ith element must be in [0, "
								+ maxValues[i] + "]. Found " + values[i] + ".");
			}
		}
	}

	@Override
	public int component(int index) {
		return _values[index];
	}

	@Override
	public int uniqueID() {
		return ArrayOps.encode(_values, _upperLimits);
	}

	@Override
	public int uniqueID(Set<Integer> components) {
		if(components.isEmpty()){
			return 1;
		}
		int[] values = new int[components.size()];
		int[] upperLimits = new int[components.size()];
		int i = 0;
		for (Integer compI : components) {
			values[i] = _values[compI];
			upperLimits[i] = _upperLimits[compI];
			i++;
		}
		return ArrayOps.encode(values, upperLimits);
	}

	@Override
	public int choices(int index) {
		return _upperLimits[index];
	}

	@Override
	public int size() {
		return _maxValues.length;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FactoredState) {
			FactoredState fs = (FactoredState) obj;
			if (size() == fs.size()) {
				for (int i = 0; i < size(); i++) {
					if (component(i) != fs.component(i)) {
						return false;
					}
				}
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return uniqueID();
	}

	@Override
	public String toString() {
		StringBuffer buff = new StringBuffer(getClass().getSimpleName() + ":[");
		for (int i = 0; i < _values.length; i++) {
			String v = String.valueOf(_values[i]);
			buff.append(v);
			if (i < _values.length - 1) {
				buff.append(", ");
			}
		}
		buff.append("]");
		return buff.toString();
	}

	@Override
	public int[] toArray() {
		return Arrays.copyOf(_values, _values.length);
	}

	@Override
	public double[] toDArray() {
		double[] v = new double[_values.length];
		for (int i = 0; i < _values.length; i++) {
			v[i] = _values[i] / (double) (_maxValues[i]);
		}
		return v;
	}

}
