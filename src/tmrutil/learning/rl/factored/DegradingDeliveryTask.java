package tmrutil.learning.rl.factored;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.stats.Random;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * A simulation of an autonomous delivery robot with a degrading suspension. The
 * robot's task is to pick up and deliver raw materials from a pickup location
 * to a drop off location. However, the raw materials are fragile and can be
 * damaged by driving over rough terrain. This problem only becomes worse over
 * time as the robot's suspension becomes damaged.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DegradingDeliveryTask extends DBNTask implements Drawable
{
	private double FORWARD_PROB = 0.8;
	private double LEFT_PROB = 0.1;
	private double RIGHT_PROB = 0.1;

	private int ROCKY_DAMAGE = 2;
	private int ROUGH_DAMAGE = 1;
	private int SMOOTH_DAMAGE = 0;

	private static final int NUM_ROWS = 6;
	private static final int NUM_COLS = 6;

	private static final int NUM_STATES = 2 * NUM_ROWS * NUM_COLS;

	private static final int NORTH_ACTION = 0;
	private static final int SOUTH_ACTION = 1;
	private static final int EAST_ACTION = 2;
	private static final int WEST_ACTION = 3;
	private static final int PICKUP_ACTION = 4;
	private static final int DROP_ACTION = 5;
	private static final int NUM_ACTIONS = 6;

	private static final Interval REWARD_INTERVAL = new Interval(0, 1);

	public static enum DamageType {
		NONE, GRADUAL, ABRUPT
	};
	
	private static final Interval SMOOTH_RUIN_PROB = new Interval(0,0.01);
	private static final Interval ROUGH_RUIN_PROB = new Interval(0,0.25);
	private static final Interval ROCKY_RUIN_PROB = new Interval(0,0.5);
	
	private static final int MAX_DAMAGE = 10000;

	private static enum Terrain {
		SOURCE, TARGET, SMOOTH, ROUGH, ROCKY
	};

	/**
	 * Represents the amount of damage to the vehicles suspension.
	 */
	private int _damageCount;

	/**
	 * Represents the agent's location.
	 */
	private Point _agentLocation;

	/**
	 * True if the agent is holding raw materials; otherwise the agent is not
	 * holding anything.
	 */
	private boolean _holdingMaterials;

	/**
	 * True if the agent just dropped off raw materials at the target location.
	 */
	private boolean _receivedReward;

	/**
	 * A map of the environment's terrain.
	 */
	private Terrain[][] _map;

	/**
	 * The type of damage that occurs to the vehicle.
	 */
	private DamageType _dtype;

	public DegradingDeliveryTask(DamageType dtype)
	{
		super(NUM_ACTIONS, REWARD_INTERVAL);
		_dtype = dtype;

		_agentLocation = new Point();

		_map = new Terrain[NUM_ROWS][NUM_COLS];
		for (int r = 0; r < NUM_ROWS; r++) {
			for (int c = 0; c < NUM_COLS; c++) {
				_map[r][c] = Terrain.SMOOTH;
			}
		}
		setupMap();

		reset();
	}

	private void setupMap()
	{
		_map[0][0] = Terrain.SOURCE;
		_map[NUM_ROWS - 1][NUM_COLS - 1] = Terrain.TARGET;

		_map[1][3] = Terrain.ROUGH;
		//_map[1][7] = Terrain.ROUGH;
		_map[2][4] = Terrain.ROUGH;
		//_map[3][6] = Terrain.ROUGH;
		//_map[3][8] = Terrain.ROUGH;
		_map[4][2] = Terrain.ROUGH;
		_map[4][5] = Terrain.ROUGH;
		//_map[4][7] = Terrain.ROUGH;
		_map[5][1] = Terrain.ROUGH;
		//_map[6][5] = Terrain.ROUGH;
		//_map[6][8] = Terrain.ROUGH;
		//_map[7][4] = Terrain.ROUGH;
		//_map[7][7] = Terrain.ROUGH;
		//_map[7][9] = Terrain.ROUGH;
		//_map[8][5] = Terrain.ROUGH;
		//_map[9][0] = Terrain.ROUGH;
		//_map[9][1] = Terrain.ROUGH;

		//_map[2][7] = Terrain.ROCKY;
		//_map[3][7] = Terrain.ROCKY;
		_map[4][3] = Terrain.ROCKY;
		_map[4][4] = Terrain.ROCKY;
		_map[5][3] = Terrain.ROCKY;
		//_map[5][5] = Terrain.ROCKY;
		//_map[5][6] = Terrain.ROCKY;
		//_map[5][7] = Terrain.ROCKY;
		//_map[6][4] = Terrain.ROCKY;
		//_map[6][7] = Terrain.ROCKY;
		//_map[7][8] = Terrain.ROCKY;
	}
	
	@Override
	public DBNStructure getStructure()
	{
		int[] maxValues = { NUM_COLS, NUM_ROWS, 2 };
		
		Map<FactoredState, Map<Integer,Double>> reinforcementMap = new HashMap<FactoredState,Map<Integer,Double>>();
		FactoredState state = new FactoredStateImpl(new int[]{NUM_COLS-1, NUM_ROWS-1, 1}, maxValues);
		Map<Integer,Double> amap = new HashMap<Integer,Double>();
		amap.put(DROP_ACTION, rmax());
		reinforcementMap.put(state, amap);
		
		DBNStructureImpl structure = new DBNStructureImpl(maxValues, NUM_ACTIONS, new ReinforcementSignal.MapReinforcementSignal<FactoredState, Integer>(reinforcementMap));
		structure.addParent(NORTH_ACTION, 0, 0);
		structure.addParent(NORTH_ACTION, 1, 1);
		structure.addParents(NORTH_ACTION, 2, new int[]{0, 1, 2});
		
		structure.addParent(SOUTH_ACTION, 0, 0);
		structure.addParent(SOUTH_ACTION, 1, 1);
		structure.addParents(SOUTH_ACTION, 2, new int[]{0, 1, 2});
		
		structure.addParent(EAST_ACTION, 0, 0);
		structure.addParent(EAST_ACTION, 1, 1);
		structure.addParents(EAST_ACTION, 2, new int[]{0, 1, 2});
		
		structure.addParent(WEST_ACTION, 0, 0);
		structure.addParent(WEST_ACTION, 1, 1);
		structure.addParents(WEST_ACTION, 2, new int[]{0, 1, 2});
		
		structure.addParent(PICKUP_ACTION, 0, 0);
		structure.addParent(PICKUP_ACTION, 1, 1);
		structure.addParents(PICKUP_ACTION, 2, new int[]{0, 1, 2});
		
		structure.addParent(DROP_ACTION, 0, 0);
		structure.addParent(DROP_ACTION, 1, 1);
		structure.addParents(DROP_ACTION, 2, new int[]{0, 1, 2});
		
		return structure;
	}

	@Override
	public FactoredState getState()
	{
		int[] aState = { _agentLocation.x, _agentLocation.y,
				_holdingMaterials ? 1 : 0 };
		int[] maxVals = { NUM_COLS, NUM_ROWS, 2 };

		return new FactoredStateImpl(aState, maxVals);
	}

	@Override
	public void execute(Integer action)
	{
		_receivedReward = false;

		Terrain t = _map[_agentLocation.y][_agentLocation.x];
		if (_holdingMaterials) {
			double r = Random.uniform();
			if (r < cargoRuinProbability(t)) {
				_holdingMaterials = false;
			}
		}

		switch (action) {
		case NORTH_ACTION:
			moveNorth();
			break;
		case SOUTH_ACTION:
			moveSouth();
			break;
		case EAST_ACTION:
			moveEast();
			break;
		case WEST_ACTION:
			moveWest();
			break;
		case PICKUP_ACTION:
			if (!_holdingMaterials && t.equals(Terrain.SOURCE)) {
				_holdingMaterials = true;
			}
			break;
		case DROP_ACTION:
			if (_holdingMaterials && t.equals(Terrain.TARGET)) {
				_receivedReward = true;
			}
			_holdingMaterials = false;
			break;
		}

		updateSuspensionDamage(_map[_agentLocation.y][_agentLocation.x]);
	}

	private boolean inMap(int x, int y)
	{
		return (x >= 0 && x < NUM_COLS && y >= 0 && y < NUM_ROWS);
	}

	private void moveNorth()
	{
		double r = Random.uniform();
		int y = _agentLocation.y - 1;
		int x = _agentLocation.x;
		if (r > FORWARD_PROB) {
			if (r < (FORWARD_PROB + LEFT_PROB)) {
				x--;
			} else {
				x++;
			}
		}

		if (inMap(x, y)) {
			_agentLocation.setLocation(x, y);
		}
	}

	private void moveSouth()
	{
		double r = Random.uniform();
		int y = _agentLocation.y + 1;
		int x = _agentLocation.x;
		if (r > FORWARD_PROB) {
			if (r < (FORWARD_PROB + LEFT_PROB)) {
				x++;
			} else {
				x--;
			}
		}

		if (inMap(x, y)) {
			_agentLocation.setLocation(x, y);
		}
	}

	private void moveEast()
	{
		double r = Random.uniform();
		int y = _agentLocation.y;
		int x = _agentLocation.x + 1;
		if (r > FORWARD_PROB) {
			if (r < (FORWARD_PROB + LEFT_PROB)) {
				y--;
			} else {
				y++;
			}
		}

		if (inMap(x, y)) {
			_agentLocation.setLocation(x, y);
		}
	}

	private void moveWest()
	{
		double r = Random.uniform();
		int y = _agentLocation.y;
		int x = _agentLocation.x - 1;
		if (r > FORWARD_PROB) {
			if (r < (FORWARD_PROB + LEFT_PROB)) {
				y++;
			} else {
				y--;
			}
		}

		if (inMap(x, y)) {
			_agentLocation.setLocation(x, y);
		}
	}

	@Override
	public double evaluate()
	{
		if (_receivedReward) {
			System.out.println("Reward!");
			return rmax();
		} else {
			return rmin();
		}
	}

	private double cargoRuinProbability(Terrain t)
	{
		double d = Math.max(1, _damageCount/MAX_DAMAGE);
		if (_dtype.equals(DamageType.GRADUAL)) {
			switch (t) {
			case ROCKY:
				return ROCKY_RUIN_PROB.getMin() + d * ROCKY_RUIN_PROB.getDiff();
			case ROUGH:
				return ROUGH_RUIN_PROB.getMin() + d * ROUGH_RUIN_PROB.getDiff();
			case SMOOTH:
				return SMOOTH_RUIN_PROB.getMin() + d * SMOOTH_RUIN_PROB.getDiff();
			default:
				return 0;
			}
		}
		
		if (_dtype.equals(DamageType.ABRUPT)) {
			boolean isDamaged = (_damageCount >= MAX_DAMAGE);
			switch (t) {
			case ROCKY:
				if(isDamaged){
					return ROCKY_RUIN_PROB.getMax();
				}else{
					return ROCKY_RUIN_PROB.getMin();
				}
			case ROUGH:
				if(isDamaged){
					return ROUGH_RUIN_PROB.getMax();
				}else{
					return ROUGH_RUIN_PROB.getMin();
				}
			case SMOOTH:
				if(isDamaged){
					return SMOOTH_RUIN_PROB.getMax();
				}else{
					return SMOOTH_RUIN_PROB.getMin();
				}
			default:
				return 0;
			}
		}
		
		if(_dtype.equals(DamageType.NONE)){
			switch(t){
			case ROCKY:
				return ROCKY_RUIN_PROB.getMin();
			case ROUGH:
				return ROUGH_RUIN_PROB.getMin();
			case SMOOTH:
				return SMOOTH_RUIN_PROB.getMin();
			default:
				return 0;
			}
		}
		
		return 0;
	}

	private void updateSuspensionDamage(Terrain t)
	{
		switch (t) {
		case ROCKY:
			_damageCount += ROCKY_DAMAGE;
			break;
		case ROUGH:
			_damageCount += ROUGH_DAMAGE;
			break;
		case SMOOTH:
			_damageCount += SMOOTH_DAMAGE;
			break;
		default:
			// No damage
		}
	}

	@Override
	public void reset()
	{
		_damageCount = 0;

		_holdingMaterials = false;
		_receivedReward = false;

		int ax = Random.nextInt(NUM_COLS);
		int ay = Random.nextInt(NUM_ROWS);
		_agentLocation.setLocation(ax, ay);
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		
		int twidth = Math.min(width, height) / Math.max(NUM_ROWS, NUM_COLS);
		
		for(int r=0;r<NUM_ROWS;r++){
			for(int c=0;c<NUM_COLS;c++){
				Rectangle2D tile = new Rectangle2D.Double(c * twidth, r * twidth, twidth, twidth);
				Terrain t = _map[r][c];
				switch(t){
				case SMOOTH:
					g2.setColor(Color.WHITE);
					break;
				case ROUGH:
					g2.setColor(Color.LIGHT_GRAY);
					break;
				case ROCKY:
					g2.setColor(Color.GRAY);
					break;
				case SOURCE:
					g2.setColor(Color.BLUE);
					break;
				case TARGET:
					g2.setColor(Color.GREEN);
					break;
				}
				g2.fill(tile);
				g2.setColor(Color.BLACK);
				g2.draw(tile);
			}
		}
		
		Ellipse2D agent = new Ellipse2D.Double(_agentLocation.x * twidth, _agentLocation.y * twidth, twidth, twidth);
		if(_holdingMaterials){
			g2.setColor(Color.YELLOW);
		}else{
			g2.setColor(Color.RED);
		}
		g2.fill(agent);
		g2.setColor(Color.BLACK);
		g2.draw(agent);
		
		g2.dispose();
	}
	
	public static void main(String[] args)
	{
		DegradingDeliveryTask task = new DegradingDeliveryTask(DamageType.NONE);
		
		double discountFactor = 0.95;
		int numVisitsUntilKnown = 10;
		DBNRMax agent = new DBNRMax(task.getStructure(), new Interval(task.rmin(),task.rmax()), discountFactor, numVisitsUntilKnown);
		
		int numEpisodes = 20;
		int episodeLength = 20000;
		
		Task.runTask(task, agent, numEpisodes, episodeLength, true, null, null, true);
		
		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		Task.runTask(task, agent, numEpisodes, episodeLength, true, null, frame);
		
	}

}
