package tmrutil.learning.rl.factored;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import tmrutil.stats.Statistics;
import tmrutil.util.ArrayOps;
import tmrutil.util.Pair;

/**
 * An implementation of DBN structure.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DBNStructureImpl implements DBNStructure
{
	private int _numStates;
	private int[] _stateVariables;
	private int _numActions;

	private List<Map<Integer, Set<Integer>>> _parents;
	private Map<Pair<FactoredState,Integer>,Double> _reinforcementMap;

	public DBNStructureImpl(int[] stateVariables, int numActions, Map<Pair<FactoredState,Integer>,Double> reinforcementMap)
	{
		_stateVariables = Arrays.copyOf(stateVariables, stateVariables.length);
		_numStates = Statistics.product(_stateVariables);
		_numActions = numActions;

		_parents = new ArrayList<Map<Integer, Set<Integer>>>();
		for (int i = 0; i < _stateVariables.length; i++) {
			Map<Integer, Set<Integer>> pi = new HashMap<Integer, Set<Integer>>();
			for (int a = 0; a < numActions; a++) {
				pi.put(a, new HashSet<Integer>());
			}
			_parents.add(pi);
		}
		
		_reinforcementMap = reinforcementMap;
	}

	/**
	 * Adds a parent index to the specified component index.
	 * 
	 * @param action
	 *            an action
	 * @param componentIndex
	 *            a state variable index
	 * @param parentIndex
	 *            a parent index to add
	 */
	public void addParent(Integer action, int componentIndex, int parentIndex)
	{
		_parents.get(componentIndex).get(action).add(parentIndex);
	}
	
	public void addParents(Integer action, int componentIndex, int[] parents)
	{
		for(int p : parents){
			addParent(action, componentIndex, p);
		}
	}

	/**
	 * Sets the parent indices to a specified component index for an action
	 * (replacing any previously set parents).
	 * 
	 * @param action
	 *            an action
	 * @param componentIndex
	 *            a state variable index
	 * @param parents
	 *            a set of parent state variable indices
	 */
	public void setParents(Integer action, int componentIndex,
			Set<Integer> parents)
	{
		for (Integer i : parents) {
			if (i < 0 || i >= _stateVariables.length) {
				throw new IllegalArgumentException(
						"Detected an illegal state variable index in the parents set.");
			}
		}
		_parents.get(componentIndex).put(action, parents);
	}

	@Override
	public Set<Integer> parents(Integer action, int componentIndex)
	{
		return _parents.get(componentIndex).get(action);
	}

	@Override
	public int choices(int index)
	{
		return _stateVariables[index];
	}

	@Override
	public int numberOfStateVariables()
	{
		return _stateVariables.length;
	}

	@Override
	public int numberOfActions()
	{
		return _numActions;
	}

	@Override
	public double reinforcement(FactoredState state, Integer action)
	{
		Pair<FactoredState,Integer> pair = new Pair<FactoredState,Integer>(state, action);
		Double r = _reinforcementMap.get(pair);
		if(r == null){
			return 0;
		}else{
			return r.doubleValue();
		}
	}

	@Override
	public FactoredState decode(Integer iState)
	{
		int[] vals = ArrayOps.decode(iState.intValue(), _stateVariables);
		return new FactoredStateImpl(vals, _stateVariables);
	}

	@Override
	public int numberOfStates()
	{
		return _numStates;
	}

}
