package tmrutil.learning.rl.factored;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * A partial factored state is derived by decomposing a factored state. In this
 * implementation, partial factored states contain a subset of components from a
 * full factored state. Once a factored state is decomposed not all actions may
 * be relevant to every partial state. This class also contains an abstract
 * method that determines which primitive actions are relevant to the partial
 * state.
 * 
 * @author Timothy A. Mann
 * 
 */
public abstract class PartialFactoredState<A> implements FactoredState {

	private FactoredStateImpl _fullState;
	private List<Integer> _indices;

	public PartialFactoredState(FactoredStateImpl fullState,
			List<Integer> indices) {
		_fullState = fullState;
		_indices = indices;
	}

	/**
	 * Returns a reference to the indices into the full factored state that
	 * define this partial factored state.
	 * 
	 * @return the list of indices in full factored state space that define this
	 *         partial factored state
	 */
	protected List<Integer> fullStateIndices() {
		return _indices;
	}

	/**
	 * Returns true if applying the given action makes sense given this partial
	 * state or false if it does not make sense to apply the given action with
	 * respect to this partial state.
	 * 
	 * @param action
	 *            a primitive action
	 * @return true if this action can be applied; otherwise false
	 */
	public abstract boolean isActionRelevant(A action);

	/**
	 * Filters out all actions that cannot be applied given this partial
	 * context.
	 * 
	 * @param actions
	 *            a collection of primitive actions
	 * @return a list of relevant actions
	 */
	public List<A> filterRelevantActions(Collection<A> actions) {
		List<A> ractions = new ArrayList<A>();
		for (A action : actions) {
			if (isActionRelevant(action)) {
				ractions.add(action);
			}
		}
		return ractions;
	}

	/**
	 * Returns the underlying full factored state.
	 * 
	 * @return the full state
	 */
	public FactoredStateImpl fullState() {
		return _fullState;
	}

	@Override
	public int component(int index) {
		Integer i = _indices.get(index);
		return _fullState.component(i);
	}

	@Override
	public int uniqueID() {
		return _fullState.uniqueID(new HashSet<Integer>(_indices));
	}

	@Override
	public int uniqueID(Set<Integer> components) {
		Set<Integer> fullComps = new HashSet<Integer>();
		for (Integer i : components) {
			fullComps.add(_indices.get(i));
		}
		return _fullState.uniqueID(fullComps);
	}

	@Override
	public int choices(int index) {
		Integer i = _indices.get(index);
		return _fullState.choices(i);
	}

	@Override
	public int[] toArray() {
		int[] x = new int[_indices.size()];
		for (int i = 0; i < _indices.size(); i++) {
			x[i] = _fullState.component(_indices.get(i));
		}
		return x;
	}

	@Override
	public int size() {
		return _indices.size();
	}
}
