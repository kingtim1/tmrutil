package tmrutil.learning.rl.factored;

/**
 * A vector factory is used to create vectors. A vector is an ordered list of
 * <code>n</code> components. Each of the <code>n</code> components can take one
 * of a finite number of values.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface VectorFactory
{

	/**
	 * Returns the number of possible elements at a component of this vector
	 * specified by <code>index</code>
	 * 
	 * @param index
	 *            the index of a component of this vector
	 * @return the number of possible elements at a component indexed by
	 *         <code>index</code>
	 */
	public int choices(int index);

	/**
	 * Returns the number of components.
	 * 
	 * @return the number of components
	 */
	public int size();
}
