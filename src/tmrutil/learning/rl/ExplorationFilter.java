package tmrutil.learning.rl;

/**
 * An exploration filter determines which state-action pairs, that an RL
 * algorithm is not allowed to explore.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface ExplorationFilter<S, A>
{
	/**
	 * Returns true if the algorithm that owns this exploration filter is
	 * allowed to explore the given (state, action) pair. False is returned to
	 * indicate that the (state, action) pair may not be explored.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return true if the given state-action pair can be explored; otherwise
	 *         false
	 */
	public boolean canExplore(S state, A action);
}
