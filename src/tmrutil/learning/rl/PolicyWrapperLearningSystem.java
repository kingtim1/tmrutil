package tmrutil.learning.rl;

import tmrutil.util.DataStore;

/**
 * A wrapper class that converts a policy into a learning system.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public class PolicyWrapperLearningSystem<S, A> implements LearningSystem<S, A> {

	private Policy<S,A> _policy;
	
	public PolicyWrapperLearningSystem(Policy<S,A> policy)
	{
		_policy = policy;
	}
	
	@Override
	public A policy(S state) {
		return _policy.policy(state);
	}

	@Override
	public void train(S prevState, A action, S newState, double reinforcement) {
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
	}

}
