package tmrutil.learning.rl;

import tmrutil.learning.LearningRate;
import tmrutil.util.DataStore;

/**
 * Implements the popular SARSA reinforcement learning algorithm with eligibility traces.
 * @author Timothy A. Mann
 *
 */
public class SarsaLambda extends
		AbstractDiscreteTemporalDifferenceLearning
{
	private ActionSelector<Integer,Integer> _actionSelector;
	private double _lambda;
	private double[][] _e;
	
	/**
	 * Constructs an instance of the SARSA reinforcement learning algorithm with eligibility traces.
	 * @param numStates the number of states in the Markov decision process to learn
	 * @param numActions the number of actions in the Markov decision process to learn
	 * @param actionSelector a mechanism for selecting actions
	 * @param learningRate the learning rate
	 * @param discountFactor the discount factor
	 * @param lambda the eligibility trace parameter
	 */
	public SarsaLambda(int numStates, int numActions, ActionSelector<Integer,Integer> actionSelector, LearningRate learningRate, double discountFactor, double lambda)
	{
		super(numStates, numActions, learningRate, discountFactor, actionSelector.getOptimization());
		_actionSelector = actionSelector;
		if(lambda < 0 || lambda > 1){
			throw new IllegalArgumentException("Eligibility trace parameter lambda must be within the interval [0, 1]");
		}
		_lambda = lambda;
		
		_e = new double[numStates][numActions];
		
		reset();
	}
	
	/**
	 * Allows elements of the underlying eligibility trace table to be set manually.
	 * @param state a state
	 * @param action an action
	 * @param val the value to set the state-action eligibility to
	 */
	public void setE(int state, int action, double val)
	{
		_e[state][action] = val;
	}

	@Override
	protected void resetImpl()
	{
		for(int s=0;s<numberOfStates();s++){
			for(int a=0;a<numberOfActions();a++){
				setQ(s,a, 0);
				_e[s][a] = 0;
			}
		}
	}

	@Override
	protected void trainImpl(Integer prevState, Integer action,
			Integer newState, double reinforcement)
	{
		double gamma = getDiscountFactor();
		double delta = reinforcement + gamma * evaluateQ(newState, policy(newState)) - evaluateQ(prevState, action);
		_e[prevState][action] = _e[prevState][action] + 1;
		
		double alpha = getLearningRate();
		
		for(int s=0;s<numberOfStates();s++){
			for(int a=0;a<numberOfActions();a++){
				setQ(s,a, evaluateQ(s,a) + alpha * delta * _e[s][a]);
				_e[s][a] = gamma * _lambda * _e[s][a]; 
			}
		}
	}

	@Override
	public Integer policy(Integer state)
	{
		return _actionSelector.policy(state, this);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<Integer> greedyValueFunction() {
		return new QFunction.GreedyVFunction<Integer>(this);
	}

	@Override
	public double value(Integer state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(Integer state) {
		return maxQ(state);
	}

}
