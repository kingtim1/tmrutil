package tmrutil.learning.rl;

import tmrutil.util.Pair;

/**
 * A generative model is a model that duplicates the behavior of another system.
 * Implementations of this interface should not maintain hidden state (the
 * Markov property applies). However, each state-action pair generates an
 * immediate next state and reinforcement according to their own distributions.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface GenerativeModel<S, A>
{
	/**
	 * Generates the immediate next state and reinforcement given a state and
	 * action. Given the same arguments, this method may return a different
	 * immediate state and reinforcement each time it is called. This is because
	 * the underlying system may be stochastic.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action applied to the system
	 * @return a pair containing the immediate next state and reinforcement
	 */
	public Pair<S, Double> simulate(S state, A action);
}
