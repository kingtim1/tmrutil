package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.List;

/**
 * An aggregator partitions the state space and determines whether or not two
 * instances belong to the same class. For instances <code>x, y, z</code> the
 * following properties hold
 * <OL>
 * <LI>(reflexive) </code>sameClass(x,x)</code> is always true</LI>
 * <LI>(symmetric) <code>sameClass(x, y) == sameClass(y, x)</LI>
 * <LI>(transitive) if <code>sameClass(x, y)</code> and
 * <code>sameClass(y, z)</code>, then <code>sameClass(x, z)</code></LI>
 * </OL>
 * It does not need to know the number of classes a priori, since it adds new
 * classes dynamically.
 * 
 * @author Timothy A. Mann
 * 
 * @param <X>
 *            the type to aggregate over
 */
public abstract class Aggregator<X> {
	public List<X> _prototypes;

	public Aggregator() {
		_prototypes = new ArrayList<X>();
	}

	/**
	 * Returns true if <code>left</code> and <code>right</code> belong to the
	 * same equivalence class. Returns false to indicate that the two instances
	 * do not belong to the same equivalence class.
	 * 
	 * @param left
	 *            an instance
	 * @param right
	 *            another instance
	 * @return true if the given instances belong to the same equivalence class;
	 *         otherwise false
	 */
	public abstract boolean sameClass(X left, X right);

	/**
	 * Aggregates an instance by assigning it to an equivalence class.
	 * 
	 * @param instance
	 *            an instance
	 * @return an integer representing the assigned equivalence class given to
	 *         <code>instance</code>
	 */
	public Integer aggregate(X instance) {
		for (int c = 0; c < _prototypes.size(); c++) {
			X p = _prototypes.get(c);
			if (sameClass(p, instance)) {
				return c;
			}
		}
		_prototypes.add(instance);
		return _prototypes.size() - 1;
	}
}
