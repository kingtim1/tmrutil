package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.HCMAC;
import tmrutil.learning.IncrementalFunctionApproximator;
import tmrutil.learning.LearningRate;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.util.DataStore;
import tmrutil.util.DebugException;

/**
 * Provides a continuous state-action implementation for Q-learning.
 * 
 * @author Timothy A. Mann
 * 
 */
public class QLearning extends
		AbstractTemporalDifferenceLearning<double[], double[]>
{
	public static final int DEFAULT_NUM_TILINGS = 14;
	public static final double DEFAULT_HYPERCUBE_DIMENSION = 0.2;

	private List<IncrementalFunctionApproximator<double[], double[]>> _actionValueFuncs;

	private ActionSelector<double[], double[]> _actionSelector;

	/**
	 * Constructs an instance of continuous state-action Q-learning.
	 * 
	 * @param stateSize
	 *            the number of dimensions in a valid state vector
	 * @param actionSelector
	 *            a strategy used for selecting actions
	 * @param learningRate
	 *            a learning rate instance
	 * @param discountFactor
	 *            the discount factor
	 */
	public QLearning(int stateSize,
			ActionSelector<double[], double[]> actionSelector,
			LearningRate learningRate, double discountFactor)
	{
		super(learningRate, discountFactor, actionSelector.getOptimization());
		_actionSelector = actionSelector;
		int numActions = _actionSelector.getActions().size();

		_actionValueFuncs = new ArrayList<IncrementalFunctionApproximator<double[], double[]>>(numActions);
		for (int a = 0; a < numActions; a++) {
			_actionValueFuncs.add(newFunctionApproximationInstance(stateSize));
		}
	}

	/**
	 * Creates new function approximator instances used for representing
	 * state-action values. This function can be overloaded to allow custom
	 * function approximators to be used instead.
	 * 
	 * @param inputSize the number of components of a valid state or input vector
	 * @return an incremental function approximator instance
	 */
	public IncrementalFunctionApproximator<double[], double[]> newFunctionApproximationInstance(
			int inputSize)
	{
		double[] hypercubeDims = new double[inputSize];
		for (int d = 0; d < inputSize; d++) {
			hypercubeDims[d] = DEFAULT_HYPERCUBE_DIMENSION;
		}
		return new HCMAC(inputSize, 1, DEFAULT_NUM_TILINGS, hypercubeDims);
	}

	@Override
	protected void resetImpl()
	{
		for (IncrementalFunctionApproximator<double[], double[]> fa : _actionValueFuncs) {
			fa.reset();
		}
	}

	@Override
	protected void trainImpl(double[] prevState, double[] action,
			double[] newState, double reinforcement)
	{
		int aInd = actionToIndex(action);
		IncrementalFunctionApproximator<double[],double[]> avfunc = _actionValueFuncs.get(aInd);
		
		int numActions = _actionSelector.getActions().size();
		double optVal = 0;
		if(_actionSelector.isMaximizing()){
			for(int a = 0; a < numActions; a++){
				IncrementalFunctionApproximator<double[],double[]> fa = _actionValueFuncs.get(a);
				double val = fa.evaluate(newState)[0];
				if(val > optVal || a == 0){
					optVal = val;
				}
			}
		}else{
			for(int a = 0; a < numActions; a++){
				IncrementalFunctionApproximator<double[],double[]> fa = _actionValueFuncs.get(a);
				double val = fa.evaluate(newState)[0];
				if(val < optVal || a == 0){
					optVal = val;
				}
			}
		}
		
		double gamma = getDiscountFactor();
		double[] target = {(1-gamma) * (reinforcement + gamma * optVal)};
		avfunc.train(prevState, target, getLearningRate());
	}

	public double evaluateQ(double[] state, double[] action)
	{
		int aInd = actionToIndex(action);
		IncrementalFunctionApproximator<double[],double[]> avfunc = _actionValueFuncs.get(aInd);
		return avfunc.evaluate(state)[0];
	}

	@Override
	public double[] policy(double[] state)
	{
		return _actionSelector.policy(state, this);
	}
	
	private int actionToIndex(double[] action)
	{
		Collection<double[]> actions = _actionSelector.getActions();
		int index = 0;
		for(double[] a : actions){
			if(areEqual(action, a)){
				return index;
			}else{
				index++;
			}
		}
		
		throw new DebugException("No action selected after comparing with all valid actions.");
	}
	
	private boolean areEqual(double[] v1, double[] v2)
	{
		if(v1.length == v2.length){
			for(int i=0;i<v1.length;i++){
				double diff = Math.abs(v1[i] - v2[i]);
				if(diff > tmrutil.math.Constants.EPSILON){
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * This operation is not supported by this class.
	 */
	public double maxQ(double[] state)
	{
		throw new UnsupportedOperationException("This operation is not supported by this class.");
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		
	}

	@Override
	public VFunction<double[]> greedyValueFunction() {
		return new QFunction.GreedyVFunction<double[]>(this);
	}

	@Override
	public double value(double[] state, double[] action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(double[] state) {
		return maxQ(state);
	}
}
