package tmrutil.learning.rl.continuous;

import tmrutil.util.DataStore;
import tmrutil.util.Interval;

/**
 * Iterative Uniform Rejection (IUR)
 * @author Timothy A. Mann
 *
 */
public class IUR extends BanditPolicySearch<Double>
{
	private Interval _armSpace;
	private double _rewardThreshold;
	private int _numSamplesBeforeDecision;
	
	private int _numSamples;
	private double _rsum;
	
	public IUR(Interval armSpace, double rewardThreshold, int numSamplesBeforeDecision)
	{
		_armSpace = armSpace;
		_rewardThreshold = rewardThreshold;
		_numSamplesBeforeDecision = numSamplesBeforeDecision;
	}

	@Override
	public void train(Integer prevState, Double action, Integer newState,
			double reinforcement)
	{
		_numSamples++;
		_rsum += reinforcement;
		
	}

	@Override
	public Double sampleNewArm()
	{
		return _armSpace.random();
	}

	@Override
	public boolean switchArm()
	{
		if(_numSamples > _numSamplesBeforeDecision){
			double r = _rsum / _numSamples;
			if(r < _rewardThreshold){
				_numSamples = 0;
				_rsum = 0;
				return true;
			}
		}
		return false;
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
	}

}
