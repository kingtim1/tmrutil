package tmrutil.learning.rl.continuous;

/**
 * An interface for continuous state and action transition models. Continuous
 * state and action transition models estimate the dynamics of an environment
 * and are updated online based on observations.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface TransitionModel
{
	/**
	 * Updates the underlying model parameters for this transition model
	 * provided a complete observation consisting of the previous state, the
	 * selected action, the resulting new state, and the reinforcement received
	 * upon transition.
	 * 
	 * @param previousState
	 *            the previous state
	 * @param action
	 *            the action selected at the previous state
	 * @param newState
	 *            the resulting new state
	 * @param reinforcement
	 *            the reinforcement received after this transition
	 */
	public void update(double[] previousState, double[] action,
			double[] newState, double reinforcement);

	/**
	 * Generates a prediction of the next state given the current state and a
	 * selected action using the parameters estimated by this transition model.
	 * Calls to this function may not be deterministic.
	 * 
	 * @param currentState
	 *            the current state
	 * @param action
	 *            the selected action
	 * @return a prediction of the next state
	 */
	public double[] generatePrediction(double[] currentState, double[] action);

	/**
	 * Generate a prediction of the change between the current state and the
	 * next state given a selected action using the parameters estimated by this
	 * transition model. Calls to this function may not be deterministic.
	 * 
	 * @param currentState
	 *            the current state
	 * @param action
	 *            the selected action
	 * @return a prediction of the change between the current state and the next
	 *         state
	 */
	public double[] generateDelta(double[] currentState, double[] action);

	/**
	 * Determines whether or not the specified state-action pair is sufficiently
	 * well modeled. True is returned to indicate that the specified
	 * state-action pair is sufficiently well modeled. False is returned
	 * otherwise.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return true if the state-action pair is sufficiently well modeled;
	 *         otherwise false
	 */
	public boolean isKnown(double[] state, double[] action);

	/**
	 * Returns the expected reinforcement after applying the specified action at
	 * a specified state.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return the expected reinforcement of a state-action pair
	 */
	public double reinforcement(double[] state, double[] action);
	
	/**
	 * Returns the number of components in a valid state vector.
	 * @return the number of components in a valid state vector
	 */
	public int getStateSize();
	
	/**
	 * Returns the number of components in a valid action vector
	 * @return the number of components in a valid action vector
	 */
	public int getActionSize();
	
	/**
	 * Resets the parameters of this transition model.
	 */
	public void reset();
}
