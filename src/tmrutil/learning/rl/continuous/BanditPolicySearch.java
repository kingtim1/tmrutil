package tmrutil.learning.rl.continuous;

import tmrutil.learning.rl.LearningSystem;

/**
 * 
 * @author Timothy A. Mann
 *
 * @param <A> the action type
 */
public abstract class BanditPolicySearch<A> implements LearningSystem<Integer, A>
{
	private A _currentArm;
	
	public BanditPolicySearch()
	{
		_currentArm = null;
	}
	
	/**
	 * Sample a new arm from the environment.
	 * @return a new arm
	 */
	public abstract A sampleNewArm();
	
	/**
	 * Decides whether to switch the current arm or not.
	 * @return true if the arm should be changed; false otherwise
	 */
	public abstract boolean switchArm();

	@Override
	public A policy(Integer state)
	{
		if(_currentArm == null || switchArm()){
			_currentArm = sampleNewArm();
		}
		return _currentArm;
	}

}
