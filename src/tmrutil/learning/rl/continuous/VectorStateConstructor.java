package tmrutil.learning.rl.continuous;

import tmrutil.learning.rl.StateConstructor;
import tmrutil.learning.rl.Task;
import tmrutil.util.Interval;

/**
 * An interface for state constructors that build vector representations of the current state.
 * @author Timothy A. Mann
 *
 * @param <T> the task type
 */
public interface VectorStateConstructor<T extends Task<?,?>> extends StateConstructor<double[], T>
{
	/**
	 * Returns the number of components in a valid state vector.
	 * @return the number of components in a valid state vector
	 */
	public int getStateSize();
	
	/**
	 * Returns the bounds on a legal state vector for each dimension.
	 * @return an array of intervals specifying the bounds on a legal state vector for each dimension
	 */
	public Interval[] getStateBounds();
}
