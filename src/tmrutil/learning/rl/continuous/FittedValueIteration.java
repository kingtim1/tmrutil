package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import tmrutil.math.Function;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Statistics;
import tmrutil.util.Interval;

/**
 * Fitted Value Iteration is a procedure for performing approximate dynamic
 * programming in continuous state and action spaces. It works by performing
 * dynamic programming on a fixed, finite set of states and actions.
 * Interpolation is used to estimate the values of state-action pairs not
 * utilized during fitted value iteration.
 * 
 * @author Timothy A. Mann
 * 
 */
public class FittedValueIteration
{

	private TransitionModel _tmodel;

	private List<double[]> _states;
	private List<double[]> _actions;

	private int _numPredictions;

	private double _discountFactor;
	private Optimization _opType;

	private List<Function<double[],Double>> _filters;
	
	private double[] _theta;
	private double[] _y;
	private double[][] _X;
	private OLSMultipleLinearRegression _regression;

	/**
	 * Constructs an instance of the fitted Value Iteration algorithm.
	 * 
	 * @param tmodel
	 *            a reference to the estimated transition model
	 * @param fixedStates
	 *            a collection of states to perform value iteration on
	 * @param actions
	 *            a collection of valid actions
	 * @param numPredictions
	 *            the number of predictions to use to estimate the expected
	 *            value of the next state (if the model has deterministic
	 *            dynamics this value should be set to 1)
	 * @param discountFactor
	 *            the discount factor of future reinforcements
	 * @param filters
	 *            a list of filters used to (nonlinearly) project states to
	 *            higher dimensions
	 * @param opType
	 *            the optimization type can be either MAXIMIZE or MINIMIZE.
	 */
	public FittedValueIteration(TransitionModel tmodel,
			Collection<double[]> fixedStates,
			Collection<double[]> actions, int numPredictions,
			double discountFactor, List<Function<double[], Double>> filters,
			Optimization opType)
	{
		_tmodel = tmodel;
		_states = new ArrayList<double[]>(fixedStates);
		_actions = new ArrayList<double[]>(actions);

		_numPredictions = numPredictions;
		setDiscountFactor(discountFactor);
		_opType = opType;

		_filters = new ArrayList<Function<double[],Double>>(filters);
		
		// _theta = new double[_tmodel.getStateSize()];
		_theta = new double[filters.size()];
		_y = new double[fixedStates.size()];
		_X = new double[fixedStates.size()][];
		int i = 0;
		for (double[] state : fixedStates) {
			_X[i] = featureVector(state);
			i++;
		}
		_regression = new OLSMultipleLinearRegression();
	}

	/**
	 * Runs the fitted Value Iteration algorithm for a specified number of
	 * iterations.
	 * 
	 * @param numIterations
	 *            the number of iterations to run the algorithm for
	 */
	public void valueIteration(int numIterations)
	{
		if (numIterations < 0) {
			throw new IllegalArgumentException(
					"Invalid number of value iterations : " + numIterations);
		}

		for (int i = 0; i < numIterations; i++) {
			for (int s = 0; s < _states.size(); s++) {
				double[] state = _states.get(s);
				double[] q = new double[_actions.size()];
				for (int a = 0; a < _actions.size(); a++) {
					double vSum = 0;
					for (int k = 0; k < _numPredictions; k++) {
						double reinforcement = _tmodel.reinforcement(state,
								_actions.get(a));
						double[] resultState = _tmodel.generatePrediction(
								state, _actions.get(a));
						vSum += reinforcement + getDiscountFactor()
								* evaluate(resultState);
					}
					q[a] = vSum / _numPredictions;
				}
				_X[s] = featureVector(state);
				if (_opType.equals(Optimization.MAXIMIZE)) {
					_y[s] = Statistics.max(q);
				} else {
					_y[s] = Statistics.min(q);
				}
			}

			_regression.newSampleData(_y, _X);
			_theta = _regression.estimateRegressionParameters();
		}
	}

	/**
	 * Computes the greedy policy based on the estimated value function.
	 * 
	 * @param state
	 *            a state
	 * @return the action selected by the greedy policy
	 */
	public double[] greedyPolicy(double[] state)
	{
		double[] q = new double[_actions.size()];
		for (int a = 0; a < _actions.size(); a++) {
			double vSum = 0;
			double[] action = _actions.get(a);
			for (int k = 0; k < _numPredictions; k++) {
				double reinforcement = _tmodel.reinforcement(state, action);
				double[] resultState = _tmodel.generatePrediction(state, action);
				vSum += reinforcement + getDiscountFactor()
						* evaluate(resultState);
			}
			q[a] = vSum / _numPredictions;
		}
		int i = Statistics.maxIndex(q);
		return _actions.get(i);
	}

	/**
	 * Evaluates the estimated value function for a specified state.
	 * 
	 * @param state
	 *            a state
	 * @return the current value estimate for the specified state
	 */
	public double evaluate(double[] state)
	{
		double[] input = featureVector(state);
		return VectorOps.dotProduct(_theta, input);
	}

	/**
	 * Constructs a feature vector from a state vector.
	 * 
	 * @param state
	 *            a state vector
	 * @return a feature vector
	 */
	public double[] featureVector(double[] state)
	{
		double[] fvector = new double[_filters.size()];
		for (int i=0;i<_filters.size();i++){
			fvector[i] = _filters.get(i).evaluate(state);
		}
		return fvector;
	}

	public double getDiscountFactor()
	{
		return _discountFactor;
	}

	public void setDiscountFactor(double discountFactor)
	{
		if (discountFactor < 0 || discountFactor >= 1) {
			throw new IllegalArgumentException(
					"The discount factor must be within the interval [0, 1)");
		}
		_discountFactor = discountFactor;
	}

	public void reset()
	{
		_theta = new double[_states.size()];
	}

	/**
	 * Generates an even grid over the state space specified by the bounded
	 * rectangular interval.
	 * 
	 * @param bounds
	 *            an array of bounded intervals
	 * @param numDiscrete
	 *            the number of discrete symbols to include in each dimension
	 * @return a collection of states
	 */
	public static final Collection<double[]> generateStates(Interval[] bounds,
			int numDiscrete)
	{
		int[] nDiscrete = new int[bounds.length];
		for (int i = 0; i < bounds.length; i++) {
			nDiscrete[i] = numDiscrete;
		}
		return generateStates(bounds, nDiscrete);
	}

	/**
	 * Generates an grid over the state space specified by the bounded
	 * rectangular interval.
	 * 
	 * @param bounds
	 *            an array of bounded intervals
	 * @param numDiscrete
	 *            an array containing the number of discrete symbols to include
	 *            in each dimension
	 * @return a collection of states
	 */
	public static final Collection<double[]> generateStates(Interval[] bounds,
			int[] numDiscrete)
	{
		if (bounds.length != numDiscrete.length) {
			throw new IllegalArgumentException(
					"The bounds array and numDiscrete array must contain the same number of components.");
		}
		int size = bounds.length;
		Collection<double[]> states = new ArrayList<double[]>(
				Statistics.product(numDiscrete));
		int[] dimCounts = new int[size];
		int[] maxCounts = numDiscrete;

		boolean finished = false;
		while (!finished) {
			for (int d = 0; d < size; d++) {

				if (dimCounts[d] < maxCounts[d]) {
					states.add(buildVector(bounds, dimCounts, maxCounts));
					dimCounts[d]++;
					break;
				} else {
					if (d == size - 1) {
						finished = true;
					} else {
						for (int j = 0; j <= d; j++) {
							dimCounts[j] = 0;
						}
						dimCounts[d + 1]++;
						if (dimCounts[d + 1] < maxCounts[d + 1]) {
							break;
						}
					}
				}

			}
		}

		return states;
	}

	private static final double[] buildVector(Interval[] bounds,
			int[] dimCounts, int[] maxCounts)
	{
		double[] v = new double[bounds.length];
		for (int d = 0; d < bounds.length; d++) {
			double inc = (bounds[d].getMax() - bounds[d].getMin())
					/ (maxCounts[d] - 1);
			v[d] = inc * dimCounts[d] + bounds[d].getMin();
		}
		return v;
	}
}
