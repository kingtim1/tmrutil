package tmrutil.learning.rl.continuous;

import tmrutil.learning.NNet;
import tmrutil.learning.TwoLayerPerceptron;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.optim.Optimization;
import tmrutil.stats.Random;
import tmrutil.util.DataStore;

/**
 * REINFORCE (REward Increment = Nonnegative Factor x Offset Reinforcement x
 * Characteristic Eligibility) is a popular reinforcement learning method that
 * learns an explicit policy. The algorithm trains a neural network using a
 * simple weight adjustment rule according to the direction of steepest descent
 * with respect to the error gradient.
 * <p/>
 * 
 * If weight ij is the weight applied to input to the ith node in a neural
 * network, the change in weight ij is equal to the learning rate times the
 * immediate reinforcement minus a baseline times the characteristic eligibility
 * for weight ij.
 * <p/>
 * $w_{ij} = \alpha_{ij} (r - b_{ij}) e_{ij}$
 * <p/>
 * 
 * This implementation assumes that optimization means maximizing rewards (NOT
 * minimizing costs).
 * <p/>
 * 
 * See R. J. Williams"Simple statistical gradient-following algorithms for connectionist reinforcement learning"
 * , 1992 for more details on REINFORCE algorithms.
 * 
 * @author Timothy Mann
 * 
 */
public class REINFORCE implements LearningSystem<double[], double[]>
{

	/** The learning rate. */
	private double _learningRate;
	/** The baseline. */
	private double _baseline;
	/** True if this learning system should use reinforcement comparison. */
	private boolean _reinfComp;

	/** The number of dimensions of a state description vector. */
	private int _stateSize;
	/** The number of dimensions of an action description vector. */
	private int _actionSize;
	/** The number of hidden units. */
	private int _numHidden;

	/**
	 * A neural network for estimating the action probability distribution
	 * parameters.
	 */
	private NNet _muNet;
	private NNet _sigmaNet;

	/**
	 * Determines whether this learning system seeks to maximize or minimize
	 * reinforcements.
	 */
	private Optimization _opType;

	/**
	 * Constructs a REINFORCE learning system for a system where the state
	 * description size and the action description size are known.
	 * 
	 * @param stateSize
	 *            the number of dimensions of a state description vector
	 * @param actionSize
	 *            the number of dimensions of an action description vector
	 * @param numHidden
	 *            the number of hidden units in the connectionist network
	 * @param learningRate
	 *            the learning rate
	 * @param baseline
	 *            the baseline
	 * @param reinfComp
	 *            true if the agent should modify the baseline for reinforcement
	 *            comparison
	 * @param opType
	 *            determines whether this learning system will seek to maximize
	 *            or minimize reinforcements
	 */
	public REINFORCE(int stateSize, int actionSize, int numHidden,
			double learningRate, double baseline, boolean reinfComp,
			Optimization opType)
	{
		_learningRate = learningRate;
		_baseline = baseline;
		_reinfComp = reinfComp;

		_stateSize = stateSize;
		_actionSize = actionSize;
		_numHidden = numHidden;

		_muNet = new TwoLayerPerceptron(stateSize, actionSize, numHidden);
		_sigmaNet = new TwoLayerPerceptron(stateSize, actionSize, numHidden);

		_opType = opType;
	}

	@Override
	public double[] policy(double[] state)
	{
		double[] mu = evaluateMu(state);
		double[] sigma = evaluateSigma(state);
		double[] action = new double[_actionSize];
		for (int i = 0; i < action.length; i++) {
			action[i] = Random.normal(mu[i], sigma[i]);
		}
		return action;
	}

	@Override
	public void train(double[] prevState, double[] action, double[] newState,
			double reinforcement)
	{
		// Update the baseline if reinforcement comparison is enabled
		if (_reinfComp) {
			_baseline = _baseline + _learningRate * (reinforcement - _baseline);
		}

		// Update the parameter neural networks
		double[] mu = evaluateMu(prevState);
		double[] sigma = evaluateSigma(prevState);
		double[] muTarget = new double[_actionSize];
		double[] sigmaTarget = new double[_actionSize];
		
		double reinfMinusBaseline = 0;
		if(_opType.equals(Optimization.MAXIMIZE)){
			reinfMinusBaseline = reinforcement - _baseline;
		}else{
			reinfMinusBaseline = -reinforcement - _baseline;
		}
		
		for (int i = 0; i < _actionSize; i++) {	
			double actionMinusMu = action[i] - mu[i];
			muTarget[i] = (mu[i] + (reinfMinusBaseline * (actionMinusMu)));
			sigmaTarget[i] = (sigma[i] + (reinfMinusBaseline * (((actionMinusMu * actionMinusMu) - (sigma[i] * sigma[i])) / sigma[i])));
		}

		double muError = _muNet.train(prevState, muTarget, _learningRate)[0];
		double sigmaError = _sigmaNet.train(prevState, sigmaTarget,
				_learningRate)[0];

		
	}

	/**
	 * Computes the mean values of each action vector component for a given
	 * state description vector.
	 * 
	 * @param state
	 *            a state description vector
	 * @return the mean values of each action vector component
	 */
	private double[] evaluateMu(double[] state)
	{
		return _muNet.evaluate(state);
	}

	/**
	 * Computes the standard deviation of each action vector component for a
	 * given state description vector.
	 * 
	 * @param state
	 *            a state description vector
	 * @return the standard deviation of each action vector component
	 */
	private double[] evaluateSigma(double[] state)
	{
		return _sigmaNet.evaluate(state);
	}

	/**
	 * Returns the number of weights used by the internal function
	 * approximators.
	 * 
	 * @return a number of weights
	 */
	public int numberOfWeights()
	{
		return _muNet.numberOfWeights() + _sigmaNet.numberOfWeights();
	}

	/**
	 * Sets the weights used by the internal function approximators.
	 * 
	 * @param weights
	 *            a vector of weights
	 */
	public void setWeights(double[] weights)
	{
		int numMuWeights = _muNet.numberOfWeights();
		int numSigmaWeights = _sigmaNet.numberOfWeights();
		if (numMuWeights + numSigmaWeights != weights.length) {
			throw new IllegalArgumentException(
					"The number of weights provided does not equal the number used by the learning system.");
		}
		double[] muWeights = new double[numMuWeights];
		double[] sigmaWeights = new double[numSigmaWeights];
		System.arraycopy(weights, 0, muWeights, 0, numMuWeights);
		System.arraycopy(weights, numMuWeights, sigmaWeights, 0,
				numSigmaWeights);
		_muNet.setWeights(muWeights);
		_sigmaNet.setWeights(sigmaWeights);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {		
	}

}
