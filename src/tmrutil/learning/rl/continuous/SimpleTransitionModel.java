package tmrutil.learning.rl.continuous;

import tmrutil.learning.NNet;
import tmrutil.math.VectorOps;
import tmrutil.util.ArrayOps;

/**
 * Implements a simple transition model that assumes system dynamics are close to deterministic.
 * @author Timothy A. Mann
 *
 */
public class SimpleTransitionModel implements TransitionModel
{
	private NNet _nnet;
	private NNet _rnet;
	private double _learningRate;
	
	public SimpleTransitionModel(NNet mnet, NNet rnet, double learningRate)
	{
		_nnet = mnet;
		_rnet = rnet;
		_learningRate = learningRate;
	}
	
	@Override
	public double[] generateDelta(double[] currentState, double[] action)
	{
		double[] input = ArrayOps.concat(currentState, action);
		return _nnet.evaluate(input);
	}

	@Override
	public double[] generatePrediction(double[] currentState, double[] action)
	{
		double[] delta = generateDelta(currentState, action);
		return VectorOps.add(currentState, delta);
	}

	@Override
	public int getActionSize()
	{
		return _nnet.getInputSize() - _nnet.getOutputSize();
	}

	@Override
	public int getStateSize()
	{
		return _nnet.getOutputSize();
	}

	@Override
	public boolean isKnown(double[] state, double[] action)
	{
		throw new UnsupportedOperationException("The isKnown operation is not supported by " + getClass().getName()); 
	}

	@Override
	public double reinforcement(double[] state, double[] action)
	{
		double[] input = ArrayOps.concat(state, action);
		return _rnet.evaluate(input)[0];
	}

	@Override
	public void update(double[] previousState, double[] action,
			double[] newState, double reinforcement)
	{
		double[] input = ArrayOps.concat(previousState, action);
		double[] target = VectorOps.subtract(newState, previousState);
		_nnet.train(input, target, _learningRate);
		_rnet.train(input, new double[]{reinforcement}, _learningRate);
	}
	
	public void reset()
	{
		_nnet.reset();
		_rnet.reset();
	}

}
