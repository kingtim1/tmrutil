package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.List;

import tmrutil.learning.HCMAC;
import tmrutil.learning.LearningRate;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.InitializationType;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.VFunction;
import tmrutil.optim.Optimization;
import tmrutil.util.DataStore;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * This is a convenient Q-learning-like algorithm for continuous state problems
 * that uses CMAC function approximators to represent the action-values.
 * 
 * @author Timothy A. Mann
 * 
 */
public class CMACQLearning extends
		AbstractTemporalDifferenceLearning<double[], Integer>
{
	public static final int DEFAULT_CMAC_LAYERS = 16;
	public static final double DEFAULT_GENERALIZATION = 0.2;

	private int _stateSize;
	private int _numActions;
	private ActionSelector<double[],Integer> _aSelector;
	private List<HCMAC> _q;

	private Interval _rewardInterval;
	private double _vmax;
	
	public CMACQLearning(int stateSize, int numActions, Interval rewardInterval, LearningRate learningRate, double discountFactor)
	{
		this(stateSize, ActionSelector.EpsilonGreedy.discreteEpsilonGreedy(numActions, 0, Optimization.MAXIMIZE), rewardInterval, InitializationType.OPTIMISTIC, learningRate, discountFactor);
	}

	/**
	 * Constructs an instance of the CMACQLearning algorithm. 
	 * @param stateSize the number of dimension of the vector used to describe a state
	 * @param numActions the number of discrete actions available to the algorithm
	 * @param rewardInterval the interval covering the minimum and maximum immediate rewards
	 * @param itype the initialization type for the Q-values (optimistic, pessimistic, etc.)
	 * @param learningRate a learning rate parameter
	 * @param discountFactor the discount factor (used for planning)
	 */
	public CMACQLearning(int stateSize, ActionSelector<double[],Integer> actionSelector, Interval rewardInterval, InitializationType itype,
			LearningRate learningRate, double discountFactor)
	{
		super(learningRate, discountFactor, Optimization.MAXIMIZE);
		_stateSize = stateSize;
		_numActions = actionSelector.getActions().size();
		_aSelector = actionSelector;

		_rewardInterval = rewardInterval;
		_vmax = _rewardInterval.getMax() / (1 - discountFactor);

		double[] hcubeDims = new double[stateSize];
		for (int i = 0; i < stateSize; i++) {
			hcubeDims[i] = DEFAULT_GENERALIZATION;
		}

		_q = new ArrayList<HCMAC>(_numActions);
		for (int a = 0; a < _numActions; a++) {
			double initVal = 0;
			switch(itype){
			case OPTIMISTIC:
				initVal = _vmax;
				break;
			case PESSIMISTIC:
				initVal = _rewardInterval.getMin() / (1-discountFactor);
				break;
			case RANDOM:
				initVal = _rewardInterval.random();
				break;
			}
			
			HCMAC qa = new HCMAC(stateSize, 1, DEFAULT_CMAC_LAYERS, hcubeDims,
					initVal);
			_q.add(qa);
		}
	}

	public int stateSize()
	{
		return _stateSize;
	}

	public int numberOfActions()
	{
		return _numActions;
	}

	public double rmax()
	{
		return _rewardInterval.getMax();
	}
	
	public double rmin()
	{
		return _rewardInterval.getMin();
	}

	public double vmax()
	{
		return _vmax;
	}
	
	public void setQ(double[] state, Integer action, double qVal)
	{
		HCMAC qa = _q.get(action);
		qa.set(state, new double[]{qVal});
	}


	public double evaluateQ(double[] state, Integer action)
	{
		return _q.get(action).evaluate(state)[0];
	}

	@Override
	public Integer policy(double[] state)
	{
		return _aSelector.policy(state, this);
	}

	@Override
	protected void resetImpl()
	{
		for (int a = 0; a < _q.size(); a++) {
			_q.get(a).reset();
		}
	}

	@Override
	protected void trainImpl(double[] prevState, Integer action,
			double[] newState, double reinforcement)
	{
		HCMAC qa = _q.get(action);
		double[] target = { reinforcement + getDiscountFactor()
				* maxQ(newState) };
		double learningRate = getLearningRate();
		qa.train(prevState, target, learningRate);
	}
	

	public double maxQ(double[] state)
	{
		return maxAQ(state).getB();
	}

	private Pair<Integer, Double> maxAQ(double[] state)
	{
		Integer bestAction = null;
		Double bestValue = null;
		for (int a = 0; a < numberOfActions(); a++) {
			double val = evaluateQ(state, a);
			if (bestValue == null || val > bestValue) {
				bestValue = val;
				bestAction = a;
			}
		}
		return new Pair<Integer, Double>(bestAction, bestValue);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<double[]> greedyValueFunction() {
		return new QFunction.GreedyVFunction<double[]>(this);
	}

	@Override
	public double value(double[] state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(double[] state) {
		return maxQ(state);
	}

}
