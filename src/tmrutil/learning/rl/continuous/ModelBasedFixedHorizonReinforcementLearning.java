package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.List;

import tmrutil.learning.LearningRate;
import tmrutil.learning.NNet;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.FixedHorizonReinforcementLearning;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.math.VectorOps;
import tmrutil.util.ArrayOps;
import tmrutil.util.DataStore;
import tmrutil.util.ThreeTuple;

/**
 * A model-based, fixed horizon reinforcement learning algorithm.
 * @author Timothy A. Mann
 *
 */
public class ModelBasedFixedHorizonReinforcementLearning implements
		FixedHorizonReinforcementLearning<double[], double[]>
{
	private long _time;
	private int _stateSize;
	private int _actionSize;
	private LearningRate _learningRate;
	private int _horizon;
	
	private NNet _tmodel;
	private NNet _vfunc;
	private ActionSelector<double[],double[]> _actionSelector;
	
	private List<ThreeTuple<double[],double[],Double>> _rsumBuff;
	
	/**
	 * Constructs a model-based, fixed horizon reinforcement learning algorithm.
	 * @param stateSize the number of components in a valid state description vector
	 * @param actionSize the number of components in valid action vectors
	 * @param tmodel a neural network to use as a transition model
	 * @param vfunc a neural network to use as a value function
	 * @param actionSelector an action selector
	 * @param learningRate a learning rate instance
	 * @param horizon the number of steps in the horizon
	 */
	public ModelBasedFixedHorizonReinforcementLearning(int stateSize, int actionSize, NNet tmodel, NNet vfunc, ActionSelector<double[],double[]> actionSelector, LearningRate learningRate, int horizon)
	{
		_time = 0;
		_stateSize = stateSize;
		_actionSize = actionSize;
		_learningRate = learningRate;
		_horizon = horizon;
		
		_tmodel = tmodel;
		_vfunc = vfunc;
		_actionSelector = actionSelector;
		
		_rsumBuff = new ArrayList<ThreeTuple<double[],double[],Double>>(horizon);
	}
	
	@Override
	public int getHorizon()
	{
		return _horizon;
	}

	@Override
	public long getTime()
	{
		return _time;
	}

	@Override
	public double[] policy(double[] state)
	{
		return _actionSelector.policy(state, this);
	}

	@Override
	public void train(double[] prevState, double[] action, double[] newState,
			double reinforcement)
	{
		// Update the time counter
		_time++;
		
		// Train the model
		double[] input = ArrayOps.concat(prevState, action);
		double[] target = VectorOps.subtract(newState, prevState);
		double alpha = _learningRate.value(getTime());
		_tmodel.train(input, target, alpha);
		
		// Train the value function
		_rsumBuff.add(new ThreeTuple<double[],double[],Double>(prevState, action, 0.0));
		for(ThreeTuple<double[],double[],Double> rsum : _rsumBuff){
			rsum.setC(rsum.getC() + reinforcement);
		}
		double error = 0;
		if(_rsumBuff.size() >= getHorizon()){
			ThreeTuple<double[],double[],Double> rsum = _rsumBuff.remove(0);
			double[] state = rsum.getA();
			double[] act = rsum.getB();
			input = ArrayOps.concat(state, act);
			double[] nextState = VectorOps.add(state, _tmodel.evaluate(input));
			error = _vfunc.train(nextState, new double[]{rsum.getC()}, alpha)[0];
		}
	}


	public double evaluateQ(double[] state, double[] action)
	{
		double[] input = ArrayOps.concat(state, action);
		double[] newState = VectorOps.add(state, _tmodel.evaluate(input));
		return _vfunc.evaluate(newState)[0];
	}
	
	/**
	 * Returns the number of components in a valid state description vector.
	 * @return a positive integer
	 */
	public int getStateSize()
	{
		return _stateSize;
	}
	
	/**
	 * Returns the number of components in a valid action vector.
	 * @return a positive integer
	 */
	public int getActionSize()
	{
		return _actionSize;
	}

	/**
	 * This operation is not supported by this class.
	 */
	public double maxQ(double[] state)
	{
		throw new UnsupportedOperationException("This operation is not supported by this class.");
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<double[]> greedyValueFunction() {
		return new QFunction.GreedyVFunction<double[]>(this);
	}

	@Override
	public double value(double[] state, double[] action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(double[] state) {
		return maxQ(state);
	}

	@Override
	public boolean isMaximizing() {
		return _actionSelector.isMaximizing();
	}

	@Override
	public boolean isMinimizing() {
		return _actionSelector.isMinimizing();
	}

}
