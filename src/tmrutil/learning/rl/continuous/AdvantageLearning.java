package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import tmrutil.learning.LearningRate;
import tmrutil.learning.NNet;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Random;
import tmrutil.util.ArrayOps;
import tmrutil.util.DataStore;
import tmrutil.util.Pair;

/**
 * Implements advantage learning reinforcement learning algorithm that is
 * similar to Q-learning but known to converge faster in continuous state
 * settings. In fact, Q-learning is a special case of advantage learning when
 * the time constant <code>k</code> is set to one.
 * 
 * Both minimization and maximization of the reinforcement signal are supported.
 * 
 * @author Timothy A. Mann
 * 
 */
public class AdvantageLearning extends AbstractTemporalDifferenceLearning<double[],double[]>
{
	/** The advantage function (analogous to Q-function). */
	protected NNet _afunc;
	/** The number of dimensions in a valid state description vector. */
	private int _stateSize;
	/** The number of dimensions in a valid action. */
	private int _actionSize;

	/** A time constant scaling factor. */
	private double _k;

	/**
	 * A policy for selecting actions.
	 */
	private ActionSelector<double[],double[]> _actionSelector;

	/**
	 * Constructs an advantage learning system for an environment with specified
	 * state and action dimensions.
	 * 
	 * @param stateSize
	 *            the dimension of a state description vector
	 * @param actionSize
	 *            the dimension of an action vector
	 * @param afunc
	 *            the neural network for approximating the advantage function
	 * @param actionSelector a policy for selecting actions
	 * @param k
	 *            a time constant (valid values are in the range (0, 1]).
	 * @param learningRate
	 *            the learning rate used for training the function approximator
	 *            (valid values are in the range (0, 1])
	 * @param discountFactor
	 *            the discount factor used for discounting future reinforcements
	 *            (valid values are in the range [0, 1))
	 */
	public AdvantageLearning(int stateSize, int actionSize, NNet afunc,
			ActionSelector<double[],double[]> actionSelector, double k, LearningRate learningRate,
			double discountFactor)
	{
		super(learningRate, discountFactor, actionSelector.getOptimization());
		
		// Check that the state and action size are positive numbers
		if (stateSize < 1 || actionSize < 1) {
			throw new IllegalArgumentException(
					"The number of state dimensions and action dimensions both must be greater than 0");
		}
		// Make sure that the neural network has valid input and output
		// dimensions
		if (stateSize + actionSize != afunc.getInputSize()
				|| 1 != afunc.getOutputSize()) {
			throw new IllegalArgumentException(
					"The input dimension of the neural network used to approximate the advantage function must equal the state dimensions plus actions dimensions and the output dimension must be one.");
		}
		_stateSize = stateSize;
		_actionSize = actionSize;

		_actionSelector = actionSelector;
		
		setTimeConstant(k);

		_afunc = afunc;
	}

	@Override
	public double[] policy(double[] state)
	{
		return _actionSelector.policy(state, this);
	}

	@Override
	protected void trainImpl(double[] prevState, double[] action, double[] newState,
			double reinforcement)
	{
		
		double[] input = ArrayOps.concat(prevState, action);
		
		double firstTerm = (1 / _k) * (reinforcement + Math.pow(getDiscountFactor(), _k) * evaluateQ(newState, policy(newState)));
		double secondTerm = (1 - (1/_k)) * evaluateQ(prevState, action);
		double[] target = new double[]{(firstTerm + secondTerm)};
		
		_afunc.train(input, target, getLearningRate());
	}

	/**
	 * Evaluates the advantage function approximator for a given state
	 * description vector and action.
	 * 
	 * @param state
	 *            a state description vector
	 * @param action
	 *            an action
	 * @return the value of the advantage function approximator at (state,
	 *         action)
	 */
	public double evaluateQ(double[] state, double[] action)
	{
		double[] input = ArrayOps.concat(state, action);
		return _afunc.evaluate(input)[0];
	}

	/**
	 * Returns the number of components in a valid state description vector.
	 * 
	 * @return the number of components in a valid state description vector
	 */
	public int getStateDimension()
	{
		return _stateSize;
	}

	/**
	 * Returns the number of components in a valid action.
	 * 
	 * @return the number of components in a valid action
	 */
	public int getActionDimension()
	{
		return _actionSize;
	}

	public double getTimeConstant()
	{
		return _k;
	}

	public void setTimeConstant(double k)
	{
		if (k <= 0 || k > 1) {
			throw new IllegalArgumentException(
					"The time constant must be in the interval (0, 1]");
		}
		_k = k;
	}
	
	@Override
	protected void resetImpl()
	{
		int numWeights = _afunc.numberOfWeights();
		double[] weights = VectorOps.random(numWeights, -1, 1);
		_afunc.setWeights(weights);
	}

	/**
	 * This operation is not supported by this class.
	 */
	public double maxQ(double[] state)
	{
		throw new UnsupportedOperationException("Advantage learning does not support maxQ.");
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<double[]> greedyValueFunction() {
		return new QFunction.GreedyVFunction<double[]>(this);
	}

	@Override
	public double value(double[] state, double[] action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(double[] state) {
		return maxQ(state);
	}
}
