package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.learning.LearningRate;
import tmrutil.learning.NNet;
import tmrutil.learning.Optimization;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;

/**
 * A continuous state implementation of the SARSA reinforcement learning
 * algorithm with a finite action set.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DiscreteActionSarsa extends
		AbstractTemporalDifferenceLearning<double[], Integer>
{
	private int _stateSize;
	private List<NNet> _qfuncs;

	private ActionSelector<double[], Integer> _actionSelector;

	public DiscreteActionSarsa(int stateSize, Collection<NNet> qfuncs,
			LearningRate learningRate, double discountFactor, double epsilon)
	{
		super(learningRate, discountFactor, Optimization.MAXIMIZE);
		_stateSize = stateSize;
		_qfuncs = new ArrayList<NNet>(qfuncs);

		Collection<Integer> validActions = new ArrayList<Integer>();
		for (int i = 0; i < qfuncs.size(); i++) {
			validActions.add(i);
		}
		_actionSelector = new ActionSelector.EpsilonGreedy<double[], Integer>(
				validActions, epsilon, Optimization.MAXIMIZE);
		// _actionSelector = new ActionSelector.Softmax<double[],
		// Integer>(validActions, alpha);
	}

	@Override
	protected void resetImpl()
	{
		for (NNet qfunc : _qfuncs) {
			qfunc.reset();
		}
	}

	@Override
	protected Double trainImpl(double[] prevState, Integer action,
			double[] newState, double reinforcement)
	{
		double[] target = { reinforcement + getDiscountFactor()
				* evaluateQ(newState, policy(newState)) };
		NNet qfunc = _qfuncs.get(action);
		return qfunc.train(prevState, target, getLearningRate())[0];
	}

	@Override
	public double evaluateQ(double[] state, Integer action)
	{
		return _qfuncs.get(action).evaluate(state)[0];
	}

	@Override
	public Integer policy(double[] state)
	{
		return _actionSelector.policy(state, this);
	}

	@Override
	public double maxQ(double[] state)
	{
		double maxQ = 0;
		for(int a=0;a<_qfuncs.size();a++){
			double qval = evaluateQ(state, a);
			if(qval > maxQ || a == 0){
				maxQ = qval;
			}
		}
		return maxQ;
	}

}
