package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.LearningRate;
import tmrutil.learning.NNet;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.math.DimensionMismatchException;
import tmrutil.math.Function;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.util.DataStore;
import tmrutil.util.DebugException;
import tmrutil.util.Interval;

/**
 * Implements the gradient temporal-difference, GTD, algorithm described in :
 * <p/>
 * Sutton et al. (2009)"A Convergent O(n) Algorithm for Off-policy Temporal-difference Learning with Linear Function Approximation"
 * <br/>
 * and <br/>
 * Sutton et al. (2009)"Fast Gradient-Descent Methods for Temporal-Difference Learning with Linear Function Approximation"
 * <p/>
 * 
 * The algorithm uses linear function approximation and is guaranteed to
 * converge even while performing off-policy updates. In addition, the algorithm
 * has O(n) runtime and memory usage.
 * 
 * @author Timothy A. Mann
 * 
 */
public class GradientTemporalDifference extends
		AbstractTemporalDifferenceLearning<double[], double[]>
{
	/**
	 * An enumeration of update rules that are supported by this class.
	 * <p/>
	 * 
	 * <OL>
	 * <LI>TD : standard temporal difference updates</LI>
	 * <LI>GTD : gradient temporal difference, GTD(0), updates with respect to
	 * Mean-Square Bellman Error (MSBE)</LI>
	 * <LI>GTD2: gradient temporal difference updates with respect to
	 * Mean-Square Projected Bellman Error (MSPBE)</LI>
	 * <LI>TDC : standard temporal difference updates with a gradient correction
	 * term (with respect to MSPBE)</LI>
	 * </OL>
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static enum UpdateRule {
		TD, GTD, GTD2, TDC
	};

	/**
	 * A list of filters used to generate a feature vector.
	 */
	private List<Function<double[], Double>> _filters;
	/**
	 * An estimate of the expectation of the temporal difference error times the
	 * input.
	 */
	private double[] _w;
	/**
	 * A vector of weights.
	 */
	private double[] _theta;

	/**
	 * Determines which update rule to apply while training.
	 */
	private UpdateRule _updateRule;

	/**
	 * Determines whether or not this algorithm assumes a motor compatible
	 * sensory representation. The consequence of this assumption is that the
	 * change in state will be predicted based only on the action encoding and
	 * not on the state.
	 */
	private boolean _motorCompatible;

	/**
	 * A neural network used to model the transitions of the environment.
	 */
	private NNet _transitionModel;

	/** The interval from which random weights are drawn. */
	private Interval _weightBounds;
	/** An action selection policy. */
	private ActionSelector<double[], double[]> _actionSelector;

	/**
	 * The number of components in a valid state vector.
	 */
	private int _stateSize;
	/**
	 * The number of components in a valid action vector.
	 */
	private int _actionSize;

	/**
	 * The state-action buffer. FOR INTERNAL USE ONLY.
	 */
	private double[] _stateActionBuff;
	private double[] _stateBuff;
	/**
	 * The feature vector buffer. FOR INTERNAL USE ONLY.
	 */
	private double[] _featureBuff;
	private double[] _featureBuffPrime;

	/**
	 * Constructs an instance of a gradient temporal-difference algorithm.
	 * 
	 * @param updateRule
	 *            determines the type of update rule used during training
	 * @param stateSize
	 *            the number of components in a valid state description vector
	 * @param actionSize
	 *            the number of components in a valid action vector
	 * @param model
	 *            a neural network used to predict the next state from the
	 *            current state and action
	 * @param filters
	 *            a collection of filters used to construct the feature vector
	 *            during evaluation
	 * @param weightBounds
	 *            the interval from which random weights are drawn
	 * @param actionSelector
	 *            a value-based action selection policy
	 * @param learningRate
	 *            a learning rate sequence
	 * @param discountFactor
	 *            a scalar value in the interval [0, 1] representing the
	 *            discount factor
	 * @param motorCompatible
	 *            true if the transition model should rely only on the selected
	 *            action's encoding to predict the change in state at each time
	 *            step
	 */
	public GradientTemporalDifference(UpdateRule updateRule, int stateSize,
			int actionSize, NNet model,
			Collection<Function<double[], Double>> filters,
			Interval weightBounds,
			ActionSelector<double[], double[]> actionSelector,
			LearningRate learningRate, double discountFactor,
			boolean motorCompatible)
	{
		super(learningRate, discountFactor, actionSelector.getOptimization());
		if (stateSize < 1) {
			throw new IllegalArgumentException(
					"State vectors must contain at least one element.");
		}
		if (actionSize < 1) {
			throw new IllegalArgumentException(
					"Action vectors must contain at least one element.");
		}
		if (filters.size() < 1) {
			throw new IllegalArgumentException(
					"Cannot construct GTD(0) without at least 1 filter.");
		}
		if (motorCompatible
				&& (model.getInputSize() != actionSize || model.getOutputSize() != stateSize)) {
			throw new IllegalArgumentException(
					"The model neural network must have input size (# of action elements) and output size # of state elements).");
		}
		if (!motorCompatible
				&& (model.getInputSize() != stateSize + actionSize || model.getOutputSize() != stateSize)) {
			throw new IllegalArgumentException(
					"The model neural network must have input size (# of state elements + # of action elements) and output size (# of state elements).");
		}

		_updateRule = updateRule;
		_motorCompatible = motorCompatible;
		_stateSize = stateSize;
		_actionSize = actionSize;
		_transitionModel = model;
		_actionSelector = actionSelector;

		_stateActionBuff = new double[stateSize + actionSize];
		_stateBuff = new double[stateSize];
		_featureBuff = new double[filters.size()];
		_featureBuffPrime = new double[filters.size()];

		_filters = new ArrayList<Function<double[], Double>>(filters);
		_w = new double[_filters.size()];
		_theta = new double[_filters.size()];
		_weightBounds = new Interval(weightBounds);
		
		reset();
	}

	/**
	 * Constructs an instance of a gradient temporal-difference algorithm that
	 * does not assume a motor-compatible sensory representation.
	 * 
	 * @param updateRule
	 *            determines the type of update rule used during training
	 * @param stateSize
	 *            the number of components in a valid state description vector
	 * @param actionSize
	 *            the number of components in a valid action vector
	 * @param transitionModel
	 *            a neural network used to predict the next state from the
	 *            current state and action
	 * @param filters
	 *            a collection of filters used to construct the feature vector
	 *            during evaluation
	 * @param weightBounds
	 *            the interval from which random weights are drawn
	 * @param actionSelector
	 *            a value-based action selection policy
	 * @param learningRate
	 *            a learning rate sequence
	 * @param discountFactor
	 *            a scalar value in the interval [0, 1] representing the
	 *            discount factor
	 */
	public GradientTemporalDifference(UpdateRule updateRule, int stateSize,
			int actionSize, NNet transitionModel,
			Collection<Function<double[], Double>> filters,
			Interval weightBounds,
			ActionSelector<double[], double[]> actionSelector,
			LearningRate learningRate, double discountFactor)
	{
		this(updateRule, stateSize, actionSize, transitionModel, filters,
				weightBounds, actionSelector, learningRate, discountFactor,
				false);
	}

	/**
	 * Constructs an instance of a gradient temporal-difference algorithm that
	 * does not assume a motor-compatible sensory representation and the
	 * state-value function initially has zero weights.
	 * 
	 * @param updateRule
	 *            determines the type of update rule used during training
	 * @param stateSize
	 *            stateSize the number of components in a valid state
	 *            description vector
	 * @param actionSize
	 *            the number of components in a valid action vector
	 * @param transitionModel
	 *            a neural network used to predict the next state from the
	 *            current state and action
	 * @param filters
	 *            a collection of filters used to construct the feature vector
	 *            during evaluation
	 * @param actionSelector
	 *            a value-based action selection policy
	 * @param learningRate
	 *            a learning rate sequence
	 * @param discountFactor
	 *            a scalar value in the interval [0, 1] representing the
	 *            discount factor
	 */
	public GradientTemporalDifference(UpdateRule updateRule, int stateSize,
			int actionSize, NNet transitionModel,
			Collection<Function<double[], Double>> filters,
			ActionSelector<double[], double[]> actionSelector,
			LearningRate learningRate, double discountFactor)
	{
		this(updateRule, stateSize, actionSize, transitionModel, filters,
				new Interval(0, 0), actionSelector, learningRate,
				discountFactor);
	}

	@Override
	public double[] policy(double[] state)
	{
		return _actionSelector.policy(state, this);
	}

	@Override
	protected void trainImpl(double[] prevState, double[] action,
			double[] newState, double reinforcement)
	{
		double alpha = getLearningRate();

		//
		// Train the model
		//
		double[] stateChange = VectorOps.subtract(newState, prevState,
				_stateBuff);
		if (_motorCompatible) {
			_transitionModel.train(action, stateChange, alpha);
		} else {
			double[] stateAction = constructInput(prevState, action,
					_stateActionBuff);
			_transitionModel.train(stateAction, VectorOps.subtract(newState,
					prevState, stateChange), alpha);
		}

		double beta = 0.1 * alpha;
		double gamma = getDiscountFactor();
		double[] phi = extractFeatures(prevState, _featureBuff);
		double[] phiPrime = extractFeatures(newState, _featureBuffPrime);
		double out = VectorOps.dotProduct(_theta, phi);
		double outPrime = VectorOps.dotProduct(_theta, phiPrime);

		//
		// Train the Value function
		//
		double delta = reinforcement + gamma * outPrime - out;
		switch (_updateRule) {
		case TD:
			updateTD(delta, phi, phiPrime, alpha, beta, gamma);
			break;
		case GTD:
			updateGTD(delta, phi, phiPrime, alpha, beta, gamma);
			break;
		case GTD2:
			updateGTD2(delta, phi, phiPrime, alpha, beta, gamma);
			break;
		case TDC:
			updateTDC(delta, phi, phiPrime, alpha, beta, gamma);
			break;
		}
		
		for(int i=0;i<_theta.length;i++){
			if(Double.isNaN(_theta[i]) || Double.isNaN(_w[i])){
				throw new DebugException("NaN weight was detected while training. This could be due to a number of reasons but the most likely is that the learning rate is too large. Try reducing the learning rate size.");
			}
		}

	}

	private void updateGTD(double delta, double[] phi, double[] phiPrime,
			double alpha, double beta, double gamma)
	{
		for (int i = 0; i < _w.length; i++) {
			_w[i] = _w[i] + beta * (delta * phi[i] - _w[i]);
		}

		double phiUDP = VectorOps.dotProduct(phi, _w);
		for (int i = 0; i < _theta.length; i++) {
			_theta[i] = _theta[i] + alpha * (phi[i] - gamma * phiPrime[i])
					* phiUDP;
		}
	}

	private void updateGTD2(double delta, double[] phi, double[] phiPrime,
			double alpha, double beta, double gamma)
	{
		double phiUDP = VectorOps.dotProduct(phi, _w);
		for (int i = 0; i < _theta.length; i++) {
			_theta[i] = _theta[i] + alpha * (phi[i] - gamma * phiPrime[i])
					* phiUDP;
		}

		for (int i = 0; i < _w.length; i++) {
			_w[i] = _w[i] + beta * (delta - phiUDP) * phi[i];
		}
	}

	private void updateTDC(double delta, double[] phi, double[] phiPrime,
			double alpha, double beta, double gamma)
	{
		double phiUDP = VectorOps.dotProduct(phi, _w);
		for (int i = 0; i < _theta.length; i++) {
			_theta[i] = _theta[i] + (alpha * delta * phi[i])
					- (alpha * gamma * phiPrime[i] * phiUDP);
		}

		for (int i = 0; i < _w.length; i++) {
			_w[i] = _w[i] + beta * (delta - phiUDP) * phi[i];
		}
	}

	private void updateTD(double delta, double[] phi, double[] phiPrime,
			double alpha, double beta, double gamma)
	{
		for (int i = 0; i < _theta.length; i++) {
			_theta[i] = _theta[i] + (alpha * delta * phi[i]);
		}
	}


	public double evaluateQ(double[] state, double[] action)
	{
		if (_motorCompatible) {
			_stateBuff = VectorOps.add(state,
					_transitionModel.evaluate(action), _stateBuff);
		} else {
			_stateActionBuff = constructInput(state, action, _stateActionBuff);
			_stateBuff = VectorOps.add(state, _transitionModel.evaluate(
					_stateActionBuff, _stateBuff));
		}
		_featureBuff = extractFeatures(_stateBuff, _featureBuff);
		return VectorOps.dotProduct(_theta, _featureBuff);
	}

	/**
	 * Constructs the input vector by concatenating the state and action vector
	 * and storing the results in the provided inputBuff vector. The inputBuff
	 * vector is returned.
	 * 
	 * @param state
	 *            a state description vector
	 * @param action
	 *            an action vector
	 * @param stateActionBuff
	 *            a buffer used to store the concatenation of the state action
	 *            vector
	 * @return a reference to stateActionBuff
	 */
	private double[] constructInput(double[] state, double[] action,
			double[] stateActionBuff)
	{
		if (state.length != _stateSize) {
			throw new DimensionMismatchException(
					"The provided state vector has an invalid number of dimensions.");
		}
		if (action.length != _actionSize) {
			throw new DimensionMismatchException(
					"The provided action vector has an invalid number of dimensions.");
		}
		System.arraycopy(state, 0, stateActionBuff, 0, _stateSize);
		System.arraycopy(action, 0, stateActionBuff, _stateSize, _actionSize);

		return stateActionBuff;
	}

	/**
	 * Extracts features from the input vector (state-action vector) by passing
	 * the input vector through each filter.
	 * 
	 * @param input
	 *            a vector derived by concatenating the state and action vector
	 * @param featureBuff
	 *            the feature buffer used to store the results of each filter
	 * @return a reference to featureBuff
	 */
	private double[] extractFeatures(double[] input, double[] featureBuff)
	{
		for (int i = 0; i < _filters.size(); i++) {
			featureBuff[i] = _filters.get(i).evaluate(input);
		}
		return featureBuff;
	}

	@Override
	protected void resetImpl()
	{
		for (int i = 0; i < _filters.size(); i++) {
			_w[i] = 0;
			_theta[i] = _weightBounds.random();
		}
	}

	/**
	 * This operation is not supported by this class.
	 */
	public double maxQ(double[] state)
	{
		throw new UnsupportedOperationException("This operation is not supported by this class.");
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
	}

	@Override
	public VFunction<double[]> greedyValueFunction() {
		return new QFunction.GreedyVFunction<double[]>(this);
	}

	@Override
	public double value(double[] state, double[] action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(double[] state) {
		return maxQ(state);
	}
}
