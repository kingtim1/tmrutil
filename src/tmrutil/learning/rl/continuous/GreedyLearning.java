package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.FunctionApproximator;
import tmrutil.learning.IncrementalFunctionApproximator;
import tmrutil.learning.LearningRate;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.optim.Optimization;
import tmrutil.util.DataStore;

/**
 * Implements reinforcement learning with function approximation and a greedy policy.
 * @author Timothy A. Mann
 *
 * @param <X> the state type
 */
public class GreedyLearning<X> extends AbstractTemporalDifferenceLearning<X,Integer>
{
	private List<IncrementalFunctionApproximator<X,Double>> _qApprox;

	public GreedyLearning(Collection<IncrementalFunctionApproximator<X,Double>> qApprox, LearningRate learningRate, double discountFactor)
	{
		super(learningRate, discountFactor, Optimization.MAXIMIZE);
		_qApprox = new ArrayList<IncrementalFunctionApproximator<X,Double>>(qApprox);
	}

	public double evaluateQ(X state, Integer action)
	{
		return _qApprox.get(action).evaluate(state);
	}

	public double maxQ(X state)
	{
		double bestQ = 0;
		for(int a=0;a<_qApprox.size();a++){
			double q = evaluateQ(state, a);
			if(q > bestQ || a == 0){
				bestQ = q;
			}
		}
		return bestQ;
	}

	@Override
	public Integer policy(X state)
	{
		Integer bestAct = 0;
		double bestQ = 0;
		for(int a=0;a<_qApprox.size();a++){
			double q = evaluateQ(state, a);
			if(q > bestQ || a == 0){
				bestQ = q;
				bestAct = a;
			}
		}
		return bestAct;
	}

	@Override
	protected void resetImpl()
	{
		for(int a=0;a<_qApprox.size();a++){
			_qApprox.get(a).reset();
		}
	}

	@Override
	protected void trainImpl(X prevState, Integer action,
			X newState, double reinforcement)
	{
		Double newQ = reinforcement + getDiscountFactor() * maxQ(newState);
		_qApprox.get(action).train(prevState, newQ, getLearningRate());
		
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<X> greedyValueFunction() {
		return new QFunction.GreedyVFunction<X>(this);
	}

	@Override
	public double value(X state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(X state) {
		return maxQ(state);
	}

}
