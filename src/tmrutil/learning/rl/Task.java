package tmrutil.learning.rl;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import tmrutil.graphics.Drawable;
import tmrutil.graphics.DrawableComponent;
import tmrutil.graphics.ProgressIterator;

/**
 * Represents a task that we can execute a policy or a learning system in. A task
 * <OL>
 * <LI>initializes a problem instance,</LI>
 * <LI>maintains state of the problem,</LI>
 * <LI>allows obtaining a description of the current state (or an observation if the task is partially observable),</LI>
 * <LI>allows evaluating the cost of the current state, and</LI>
 * <LI>allows reseting the problem instance to an initial state.</LI>
 * </OL>
 * 
 * @param <S>
 *            the type used to describe states (observation type if the task is partially observable)
 * @param <A>
 *            the type used to describe actions
 * 
 * @author Timothy Mann
 */
public abstract class Task<S, A> {
	/**
	 * Returns a description of the current state.
	 * 
	 * @return a description of the current state
	 */
	public abstract S getState();

	/**
	 * Executes a specified action and updates the task state.
	 * 
	 * @param action
	 *            an action description
	 */
	public abstract void execute(A action);

	/**
	 * Evaluates the cost of the current state.
	 * 
	 * @return the cost of the current state
	 */
	public abstract double evaluate();

	/**
	 * Resets the task to an initial state.
	 */
	public abstract void reset();

	/**
	 * Returns true if the task has finished; otherwise false is returned.
	 */
	public abstract boolean isFinished();

	/**
	 * Runs a task for a specified number of episodes.
	 * 
	 * @param <S>
	 *            the state description type
	 * @param <A>
	 *            the action type
	 * @param <R>
	 *            the reinforcement type
	 * @param <T>
	 *            the task type
	 * @param task
	 *            a task
	 * @param agent
	 *            a policy (if the policy is a {@link LearningSystem} and
	 *            <code>train == true</code>, then the agent's {@link
	 *            LearningSystem.train()} method will be called after each
	 *            timestep.
	 * @param numEpisodes
	 *            the number of episodes to run the task for
	 * @param episodeLength
	 *            the number of steps in an episode
	 * @param train
	 *            true if the learning system (agent) trains during the
	 *            episodes; otherwise false (does not train)
	 * @param taskObserver
	 *            an observer for collecting statistics during the episodes
	 *            (passing null ignores statistics collection)
	 */
	public static final <S, A, T extends Task<S, A>> void runTask(T task,
			Policy<S, A> agent, int numEpisodes, int episodeLength,
			boolean train, TaskObserver<S, A, T> taskObserver) {
		runTask(task, agent, numEpisodes, episodeLength, train, taskObserver,
				null, false);
	}

	/**
	 * Runs a task for a specified task until it is finished or a supplied
	 * maximum number of episodes.
	 * 
	 * @param <S>
	 *            the state description type
	 * @param <A>
	 *            the action type
	 * @param <R>
	 *            the reinforcement type
	 * @param <T>
	 *            the task type
	 * @param task
	 *            a task
	 * @param agent
	 *            a policy (if the policy is a {@link LearningSystem} and
	 *            <code>train == true</code>, then the agent's {@link
	 *            LearningSystem.train()} method will be called after each
	 *            timestep.
	 * @param numEpisodes
	 *            the maximum number of episodes to run the task for
	 * @param episodeLength
	 *            the number of steps in an episode
	 * @param train
	 *            true if the learning system (agent) trains during the
	 *            episodes; otherwise false (does not train)
	 * @param taskObserver
	 *            an observer for collecting statistics during the episodes
	 *            (passing null ignores statistics collection)
	 * @param frame
	 *            the frame used to display the task state while it is running
	 *            (passing null ignores the graphics)
	 */
	public static final <S, A, T extends Task<S, A>> void runTask(T task,
			Policy<S, A> agent, int numEpisodes, int episodeLength,
			boolean train, TaskObserver<S, A, T> taskObserver, JFrame frame) {
		runTask(task, agent, numEpisodes, episodeLength, train, taskObserver,
				frame, false);
	}

	/**
	 * Runs a task for a specified task for a supplied maximum number of
	 * episodes.
	 * 
	 * @param <S>
	 *            the state description type
	 * @param <A>
	 *            the action type
	 * @param <R>
	 *            the reinforcement type
	 * @param <T>
	 *            the task type
	 * @param task
	 *            a task
	 * @param agent
	 *            a policy (if the policy is a {@link LearningSystem} and
	 *            <code>train == true</code>, then the agent's {@link
	 *            LearningSystem.train()} method will be called after each
	 *            timestep.
	 * @param numEpisodes
	 *            the number of episodes to run the task for
	 * @param episodeLength
	 *            the maximum number of steps (i.e. decision epochs) in an
	 *            episode (a negative integer will result in an infinite maximum
	 *            value)
	 * @param train
	 *            true if the learning system (agent) trains during the
	 *            episodes; otherwise false (does not train)
	 * @param taskObserver
	 *            an observer for collecting statistics during the episodes
	 *            (passing null ignores statistics collection)
	 * @param frame
	 *            the frame used to display the task state while it is running
	 *            (passing null ignores the graphics)
	 * @param displayProgress
	 *            if true a progress bar will be constructed and display the
	 *            percentage of episodes that have been accomplished
	 */
	public static final <S, A, T extends Task<S, A>> void runTask(T task,
			Policy<S, A> agent, int numEpisodes, int episodeLength,
			boolean train, TaskObserver<S, A, T> taskObserver, JFrame frame,
			boolean displayProgress) {
		// Setup the frame
		if (frame != null && task instanceof Drawable) {
			DrawableComponent dcomp = new DrawableComponent((Drawable) task);
			frame.add(dcomp);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
			if (task instanceof KeyListener) {
				frame.addKeyListener((KeyListener) task);
			}
			if (task instanceof MouseListener) {
				frame.addMouseListener((MouseListener) task);
			}
		} else {
			frame = null;
		}

		// Create the progress iterator for displaying progress
		ProgressIterator piter = null;
		if (displayProgress) {
			piter = ProgressIterator.iterator(0, numEpisodes, 1);
		}

		// Simulate the specified number of episodes
		for (int episode = 0; (episode < numEpisodes) || (numEpisodes < 0); episode++) {

			// Reset the task to an initial state
			task.reset();
			// Observe the beginning of an episode
			if (taskObserver != null) {
				taskObserver.observeEpisodeBegin(task);
			}
			// If the agent is listening for the beginning of an episode, then
			// notify it that a new episode is beginning.
			if (agent instanceof EpisodeObserver) {
				((EpisodeObserver) agent).observeEpisodeBegin();
			}

			// Step forward in the simulation until the episode is complete
			for (int step = 0; (step < episodeLength || episodeLength < 0)
					&& !task.isFinished(); step++) {
				// Pain the scene if the frame is not null and visible
				if (frame != null && frame.isVisible()) {
					frame.repaint();
					try {
						Thread.sleep(100);
					} catch (InterruptedException ex) { /* Do Nothing */
					}
				}

				// Obtain a description of the current state
				S state = task.getState();
				// Allow the agent to select an action
				A action = agent.policy(state);
				// Execute the action selected by the agent
				task.execute(action);

				// Obtain a description of the new state
				S newState = task.getState();
				// Evaluate the agent's progress
				double reinforcement = task.evaluate();

				// Check to see if the agent should train on the
				// state-action-state-reinforcement sample
				if (train && agent instanceof LearningSystem) {
					// Train the agent
					((LearningSystem<S, A>) agent).train(state, action,
							newState, reinforcement);
				}
				// Observe the state of the task after a step
				if (taskObserver != null) {
					taskObserver.observeStep(task, state, action, newState,
							reinforcement);
				}
			}

			// Paint the scene if the frame is not null and visible
			if (frame != null && frame.isVisible()) {
				frame.repaint();
			}

			// Observe the end of an episode
			if (taskObserver != null) {
				taskObserver.observeEpisodeEnd(task);
			}
			// If the agent is listening for the end of an episode, then
			// notify it that the episode has ended.
			if (agent instanceof EpisodeObserver) {
				((EpisodeObserver) agent).observeEpisodeEnd();
			}

			// If the progress is being displated, then update it
			if (displayProgress) {
				piter.next();
			}
		}

		// Hide the progress bar or destroy it
		if (displayProgress) {
			java.awt.Container container = piter.getTopLevelAncestor();
			if (container != null) {
				container.setVisible(false);
				if (container instanceof java.awt.Window) {
					((java.awt.Window) container).dispose();
				}
			}
		}
	}

	/**
	 * Runs a task for a single episode.
	 * 
	 * @param <S>
	 *            the state type
	 * @param <A>
	 *            the action type
	 * @param <R>
	 *            the reinforcement type
	 * @param <T>
	 *            the task type
	 * @param task
	 *            the task
	 * @param agent
	 *            a policy (if the policy is a {@link LearningSystem} and
	 *            <code>train == true</code>, then the agent's {@link
	 *            LearningSystem.train()} method will be called after each
	 *            timestep.
	 * @param episodeLength
	 *            the number of steps in an episode
	 * @param train
	 *            true if the agent should use this episode to train
	 * @param taskObserver
	 *            a task observer to record data about this episode
	 * @param resetTask
	 *            if true, then the task will be reset to an initial state
	 *            before executing the episode
	 */
	public static final <S, A, T extends Task<S, A>> void runEpisode(T task,
			Policy<S, A> agent, int episodeLength, boolean train,
			TaskObserver<S, A, T> taskObserver, boolean resetTask) {
		// Reset the task to an initial state
		if (resetTask) {
			task.reset();
		}
		// Observe the beginning of an episode
		if (taskObserver != null) {
			taskObserver.observeEpisodeBegin(task);
		}
		// If the agent is listening for the beginning of an episode, then
		// notify it that a new episode is beginning.
		if (agent instanceof EpisodeObserver) {
			((EpisodeObserver) agent).observeEpisodeBegin();
		}

		// Step forward in the simulation until the episode is complete
		for (int step = 0; step < episodeLength && !task.isFinished(); step++) {

			// Obtain a description of the current state
			S state = task.getState();
			// Allow the agent to select an action
			A action = agent.policy(state);
			// Execute the action selected by the agent
			task.execute(action);

			// Obtain a description of the new state
			S newState = task.getState();
			// Evaluate the agent's progress
			double reinforcement = task.evaluate();

			// Check to see if the agent should train on the
			// state-action-state-reinforcement sample
			if (train && agent instanceof LearningSystem) {
				// Train the agent
				((LearningSystem<S, A>) agent).train(state, action, newState,
						reinforcement);
			}
			// Observe the state of the task after a step
			if (taskObserver != null) {
				taskObserver.observeStep(task, state, action, newState,
						reinforcement);
			}
		}

		// Observe the end of an episode
		if (taskObserver != null) {
			taskObserver.observeEpisodeEnd(task);
		}
		// If the agent is listening for the end of an episode, then
		// notify it that the episode has ended.
		if (agent instanceof EpisodeObserver) {
			((EpisodeObserver) agent).observeEpisodeEnd();
		}
	}
}
