package tmrutil.learning.rl.pomdp;

/**
 * A interface for POMDP policies.
 * @author Timothy A. Mann
 *
 * @param <O> the observation type
 * @param <A> the action type
 */
public interface POMDPPolicy<O, A> {
	
	
	public A policy(O observation);
	
	public void updateBelief(A previousAction, O resultObservation);
}
