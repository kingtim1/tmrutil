package tmrutil.learning.rl.pomdp;

import tmrutil.learning.rl.MarkovDecisionProcess;

/**
 * An interface for Partially Observable Markov Decsion Processes.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <O>
 *            the observation type
 * @param <A>
 *            the action type
 */
public interface POMDP<S, O, A> extends MarkovDecisionProcess<S, A> {

	/**
	 * Calculates the probability of making an observation given that the agent
	 * just executed <code>previousAction</code> and is in <code>state</code>.
	 * 
	 * @param observation
	 *            an observation
	 * @param state
	 *            a state
	 * @param previuosAction
	 *            the previously executed action
	 * @return probability of the observation given the state and previous action
	 */
	public double observationProbability(O observation, S state,
			A previousAction);
	
	/**
	 * Calculates the probability of making an observation based on the initial state.
	 * @param observation an observation
	 * @param state a state representing the initial state
	 * @return probability of the observation given the initial state
	 */
	public double initialObservationProbability(O observation, S state);
}
