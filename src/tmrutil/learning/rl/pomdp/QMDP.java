package tmrutil.learning.rl.pomdp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.rl.QFunction;

/**
 * This is an implementation of the QMDP policy for POMDPs, introduced by:
 * 
 * Littman, Cassandra, and Kaelbling,
 * "Learning Policies for Partially Observable Environments: Scaling Up",
 * Machine Learning, 1995.
 * 
 * The idea is to solve for the action-value function of the underlying MDP and
 * then select actions by sampling the belief state and choosing actions that
 * maximize the sum of sampled Q-values.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S> the state type
 * @param <O> the observation type
 * @param <A> the action type
 */
public class QMDP<S, O, A> implements POMDPPolicy<O,A> {
	
	private QFunction<S,A> _qfunc;
	private BeliefState<S, O, A> _bstate;
	private int _numSamples;
	private List<A> _actions;
	
	public QMDP(QFunction<S,A> qfunc, BeliefState<S,O,A> bstate, int numSamples, Collection<A> actions)
	{
		_qfunc = qfunc;
		_bstate = bstate;
		_numSamples = numSamples;
		_actions = new ArrayList<A>(actions);
	}

	@Override
	public A policy(O observation) {
		
		A bestAction = null;
		double bestValue = 0;
		
		List<S> sstates = new ArrayList<S>();
		for(int s=0;s<_numSamples;s++){
			S state = _bstate.sample();
			sstates.add(state);
		}
		
		for(A a : _actions){
			double qsum = 0;
			for(S s : sstates){
				qsum += _qfunc.value(s, a);
			}
			if(bestAction == null || isBetter(bestValue, qsum)){
				bestAction = a;
				bestValue = qsum;
			}
		}
		
		return bestAction;
	}
	
	private boolean isBetter(double existing, double proposed)
	{
		if(_qfunc.isMaximizing()){
			return existing < proposed;
		}else{
			return existing > proposed;
		}
	}

	@Override
	public void updateBelief(A previousAction, O resultObservation) {
		_bstate.update(previousAction, resultObservation);
	}
}
