package tmrutil.learning.rl;

import java.util.List;

/**
 * A history or rollout of (state, action, rewards/penalties) in an MDP.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface Rollout<S, A> {
	/**
	 * Returns the initial timestep of this rollout.
	 * 
	 * @return the initial timestep
	 */
	public int initTimestep();

	/**
	 * Returns the last timestep of this rollout corresponding to the timestep
	 * of the last state visited.
	 * 
	 * @return the last timestep
	 */
	public int lastTimestep();

	/**
	 * Returns the initial state in this history.
	 * 
	 * @return the initial state in this history
	 */
	public S firstState();

	/**
	 * Returns the last state in this history.
	 * 
	 * @return the last state in this history
	 */
	public S lastState();

	/**
	 * The undiscounted sum of reinforcements acquired during this history.
	 * 
	 * @return the undiscounted sum of reinforcements
	 */
	public double reinforcementSum();

	/**
	 * The discounted sum of reinforcements acquired during this history.
	 * 
	 * @param discountFactor
	 *            a discount factor in the interval [0, 1)
	 * @return the discounted sum of reinforcements
	 */
	public double reinforcementSum(double discountFactor);

	/**
	 * The entire sequence of states visited during this history.
	 * 
	 * @return the sequence of states visited
	 */
	public List<S> stateSequence();

	/**
	 * The sequence of actions or options executed during this history.
	 * 
	 * @return the sequence of actions/options executed
	 */
	public List<A> actionSequence();

	/**
	 * The entire sequence of reinforcements granted during this history.
	 * 
	 * @return the sequence of reinforcements granted
	 */
	public List<Double> reinforcementSequence();

	/**
	 * Returns an RL instance, which is a 4-tuple containing a state, action,
	 * next state, and reinforcement.
	 * 
	 * @param index
	 *            the index of the desired RL instance
	 * @param discountFactor
	 *            a discount factor in the interval [0, 1)
	 * @return an RL instance
	 */
	public RLInstance<S, A> getInstance(int index, double discountFactor);

	/**
	 * Returns the number of complete RL instances stored in this history.
	 * 
	 * @return the number of RL instances in this history
	 */
	public int numberOfRLInstances();

	/**
	 * Returns an instance that can iterate over all of the {@link RLInstance}s
	 * in this rollout.
	 * 
	 * @param discountFactor
	 *            a discount factor in the interval [0, 1)
	 * @return an iterable instance over RL samples
	 */
	public Iterable<RLInstance<S, A>> instances(double discountFactor);

}
