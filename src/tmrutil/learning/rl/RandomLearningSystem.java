package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.stats.Random;
import tmrutil.util.DataStore;

/**
 * Implements the {@see LearningSystem} interface, however, instances of this
 * class do not learn. Instances select actions according to a uniform random
 * distribution over a set of actions.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class RandomLearningSystem<S, A> implements LearningSystem<S, A> {
	private ActionSet<S, A> _actions;

	/**
	 * Constructs a random learning system with a collection of actions.
	 * 
	 * @param actions
	 *            a collection of actions
	 */
	public RandomLearningSystem(Collection<A> actions) {
		_actions = new ActionSet.Unrestricted<S, A>(actions);
	}

	/**
	 * Constructs a random learning system with an action set.
	 * 
	 * @param actionSet
	 *            a state sensitive collection of actions
	 */
	public RandomLearningSystem(ActionSet<S, A> actionSet) {
		_actions = actionSet;
	}

	@Override
	public A policy(S state) {
		List<A> actions = _actions.validList(state);
		int rind = Random.nextInt(actions.size());
		return actions.get(rind);
	}

	@Override
	public void train(S prevState, A action, S newState, double reinforcement) {
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

}
