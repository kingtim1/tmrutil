package tmrutil.learning.rl;

import tmrutil.learning.BinaryDecisionTree;

public class BinaryDecisionTreeWrapperPolicy<S> implements Policy<S, Integer>
{
	private BinaryDecisionTree<S> _btree;
	
	public BinaryDecisionTreeWrapperPolicy(BinaryDecisionTree<S> btree)
	{
		_btree = btree;
	}
	
	public BinaryDecisionTree<S> tree()
	{
		return _btree;
	}

	@Override
	public Integer policy(S state)
	{
		return _btree.classify(state);
	}

}
