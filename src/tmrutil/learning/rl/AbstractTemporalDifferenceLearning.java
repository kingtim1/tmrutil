package tmrutil.learning.rl;

import tmrutil.learning.LearningRate;
import tmrutil.optim.Optimization;

/**
 * Provides an abstract implementation for temporal-difference learning
 * algorithms.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public abstract class AbstractTemporalDifferenceLearning<S, A> implements
		TemporalDifferenceLearning<S, A>
{
	/** The time step. */
	private long _timeStep;
	/** The learning rate sequence. */
	private LearningRate _learningRate;
	/** The discount factor. */
	private double _discountFactor;
	/** The type of optimization performed by this algorithm. */
	private Optimization _opType;

	/**
	 * Constructs an instance of a temporal-difference learning algorithm with a
	 * learning rate sequence, discount factor, and the optimization type.
	 * 
	 * @param learningRate
	 *            the learning rate sequence to use during training
	 * @param discountFactor
	 *            the discount factor
	 * @param opType
	 *            the type of optimization that this algorithm should perform
	 *            (either minimizing or maximizing)
	 */
	public AbstractTemporalDifferenceLearning(LearningRate learningRate,
			double discountFactor, Optimization opType)
	{
		setLearningRate(learningRate);
		setDiscountFactor(discountFactor);
		_opType = opType;
	}

	/**
	 * Constructs an instance of a temporal-difference learning algorithm with a
	 * constant learning rate, discount factor, and optimization type.
	 * 
	 * @param learningRate
	 *            the learning rate to use during training
	 * @param discountFactor
	 *            the discount factor
	 * @param opType
	 *            the type of optimization that this algorithm should perform
	 *            (either minimizing or maximizing)
	 */
	public AbstractTemporalDifferenceLearning(double learningRate,
			double discountFactor, Optimization opType)
	{
		this(new LearningRate.Constant(learningRate), discountFactor, opType);
	}

	@Override
	public final double getDiscountFactor()
	{
		return _discountFactor;
	}

	@Override
	public final double getLearningRate()
	{
		return _learningRate.value(getTime());
	}

	@Override
	public final long getTime()
	{
		return _timeStep;
	}

	@Override
	public final boolean isMaximizing()
	{
		return Optimization.MAXIMIZE == _opType;
	}

	@Override
	public final boolean isMinimizing()
	{
		return Optimization.MINIMIZE == _opType;
	}

	@Override
	public final void reset()
	{
		_timeStep = 0;
		resetImpl();
	}

	/**
	 * Provides an implementation of the reset method.
	 */
	protected abstract void resetImpl();

	@Override
	public final void setDiscountFactor(double discountFactor)
			throws IllegalArgumentException
	{
		if (discountFactor < 0 || discountFactor > 1) {
			throw new IllegalArgumentException(
					"The discount factor must be a scalar value in the interval [0, 1].");
		}
		_discountFactor = discountFactor;
	}

	@Override
	public final void setLearningRate(LearningRate learningRate)
	{
		_learningRate = learningRate;
	}

	@Override
	public final void setLearningRate(double learningRate)
			throws IllegalArgumentException
	{
		_learningRate = new LearningRate.Constant(learningRate);
	}

	@Override
	public final void train(S prevState, A action, S newState, double reinforcement)
	{
		_timeStep++;
		trainImpl(prevState, action, newState, reinforcement);
	}

	/**
	 * Provides an implementation of the training procedure for a single
	 * observation.
	 * 
	 * @param prevState
	 *            a description of the previous state
	 * @param action
	 *            the selected action
	 * @param newState
	 *            a description of the state resulting from applying the
	 *            specified action while in the previous state
	 * @param reinforcement
	 *            the reinforcement received
	 */
	protected abstract void trainImpl(S prevState, A action, S newState,
			double reinforcement);
}
