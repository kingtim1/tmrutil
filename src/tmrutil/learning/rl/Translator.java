package tmrutil.learning.rl;

/**
 * An interface for translating states or actions from one representation to another.
 * 
 * @author Timothy A. Mann
 * 
 * @param <X> a foreign representation
 * @param <Y> the usable representation
 */
public interface Translator<X, Y>
{
	/**
	 * Translates an instance of the foreign representation into an instance of the usable representation.
	 * @param action an instance in the foreign representation
	 * @return an instance of the usable representation
	 */
	public Y translate(X action);
}
