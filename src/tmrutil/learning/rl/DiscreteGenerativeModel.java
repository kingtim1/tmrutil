package tmrutil.learning.rl;

/**
 * Represents a discrete state and action generative model.
 * @author Timothy A. Mann
 *
 */
public interface DiscreteGenerativeModel extends
		GenerativeModel<Integer, Integer>
{
	/**
	 * Returns the number of states in this generative model.
	 * @return the number of states
	 */
	public int numberOfStates();
	
	/**
	 * Returns the number of actions in this generative model.
	 * @return the number of actions
	 */
	public int numberOfActions();
}
