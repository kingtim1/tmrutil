package tmrutil.learning.rl;

import tmrutil.stats.Statistics;

/**
 * Implements value iteration for finite horizon Markov decision processes.
 * 
 * @author Timothy A. Mann
 */
public class FiniteHorizonValueIteration<S, A>
{
	private double[][][] _q;
	private DiscreteMarkovDecisionProcess _mdp;
	private int _horizon;
	
	public FiniteHorizonValueIteration(DiscreteMarkovDecisionProcess mdp, int horizon)
	{
		_mdp = mdp;
		_horizon = horizon;
		_q = new double[horizon][mdp.numberOfStates()][mdp.numberOfActions()];
	}
	
	public void solve()
	{
		for(int t=_horizon-1;t>=0;t--){
			for(int s=0;s<_mdp.numberOfStates();s++){
				for(int a=0;a<_mdp.numberOfActions();a++){
					double val = _mdp.reinforcement(s, a);
					for(int rs=0;rs<_mdp.numberOfStates();rs++){
						val += _mdp.transitionProb(s, a, rs) * getV(t+1, rs);
					}
					setQ(t, s, a, val);
				}
			}
		}
	}
	
	public double getV(int timestep, Integer state)
	{
		if(timestep == _horizon){
			return 0;
		}else{
			return Statistics.max(_q[timestep][state.intValue()]);
		}
	}
	
	public double getQ(int timestep, Integer state, Integer action)
	{
		if(timestep == _horizon){
			return 0;
		}else{
			return _q[timestep][state.intValue()][action.intValue()];
		}
	}
	
	private void setQ(int timestep, Integer state, Integer action, double value)
	{
		_q[timestep][state.intValue()][action.intValue()] = value;
	}
}
