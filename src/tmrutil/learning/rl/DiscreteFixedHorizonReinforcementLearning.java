package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.List;

import tmrutil.learning.LearningRate;
import tmrutil.stats.Statistics;
import tmrutil.util.DataStore;
import tmrutil.util.ThreeTuple;

/**
 * A discrete state-action, fixed horizon reinforcement learning algorithm.
 * @author Timothy A. Mann
 *
 */
public class DiscreteFixedHorizonReinforcementLearning implements
		FixedHorizonReinforcementLearning<Integer, Integer>
{
	private long _time;
	private double[][] _valueTable;
	private LearningRate _learningRate;
	private int _horizon;
	private int _numStates;
	private int _numActions;
	private List<ThreeTuple<Integer,Integer,Double>> _rsumBuff; 
	
	/**
	 * Constructs a simple discrete, fixed horizon reinforcement learning algorithm.
	 * @param numStates the number of states in the problem
	 * @param numActions the number of actions in the problem
	 * @param learningRate the learning rate used by this algorithm
	 * @param horizon the size of the horizon
	 */
	public DiscreteFixedHorizonReinforcementLearning(int numStates, int numActions, LearningRate learningRate, int horizon)
	{
		_time = 0;
		_numStates = numStates;
		_numActions = numActions;
		_valueTable = new double[numStates][numActions];
		_learningRate = learningRate;
		_horizon = horizon;
		_rsumBuff = new ArrayList<ThreeTuple<Integer,Integer,Double>>(horizon);
	}

	@Override
	public int getHorizon()
	{
		return _horizon;
	}
	
	@Override
	public long getTime()
	{
		return _time;
	}

	@Override
	public Integer policy(Integer state)
	{
		if(state < 0 || state > _numStates){
			throw new IllegalArgumentException("Invalid state detected :" + state);
		}
		return Statistics.maxIndex(_valueTable[state]);
	}

	@Override
	public void train(Integer prevState, Integer action, Integer newState,
			double reinforcement)
	{
		_time++;
		_rsumBuff.add(new ThreeTuple<Integer,Integer,Double>(prevState,action,0.0));
		for(ThreeTuple<Integer,Integer,Double> rsum : _rsumBuff){
			rsum.setC(rsum.getC() + reinforcement);
		}
		
		double error = 0.0;
		if(_rsumBuff.size() >= getHorizon()){
			ThreeTuple<Integer,Integer,Double> rsum = _rsumBuff.remove(0);
			Integer state = rsum.getA();
			Integer act = rsum.getB();
			double alpha = _learningRate.value(getTime());
			error = rsum.getC() - _valueTable[state][act];
			_valueTable[state][act] = _valueTable[state][act] + (alpha * error); 
		}
	}

	/**
	 * Returns the number of states.
	 * @return a positive integer
	 */
	public int getNumberOfStates()
	{
		return _numStates;
	}
	
	/**
	 * Returns the number of actions.
	 * @return a positive integer
	 */
	public int getNumberOfActions()
	{
		return _numActions;
	}


	public double evaluateQ(Integer state, Integer action)
	{
		return _valueTable[state][action];
	}


	public double maxQ(Integer state)
	{
		return Statistics.max(_valueTable[state]);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<Integer> greedyValueFunction() {
		return new QFunction.GreedyVFunction<Integer>(this);
	}

	@Override
	public double value(Integer state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(Integer state) {
		return maxQ(state);
	}

	@Override
	public boolean isMaximizing() {
		return true;
	}

	@Override
	public boolean isMinimizing() {
		return false;
	}
}
