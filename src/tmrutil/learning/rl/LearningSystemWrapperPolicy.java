package tmrutil.learning.rl;

/**
 * A wrapper class for {@link LearningSystem}s to make them {@link Policy}s.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public class LearningSystemWrapperPolicy<S, A> implements Policy<S, A>
{
	private LearningSystem<S,A> _agent;
	
	public LearningSystemWrapperPolicy(LearningSystem<S,A> agent)
	{
		_agent = agent;
	}
	
	@Override
	public A policy(S state)
	{
		return _agent.policy(state);
	}

}
