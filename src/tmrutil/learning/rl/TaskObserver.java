package tmrutil.learning.rl;


/**
 * An interface for collecting statistics while a task is running.
 * @author Timothy A. Mann
 *
 */
public interface TaskObserver<S, A, T extends Task<S,A>>
{
	/**
	 * Called after a step in an episode occurs.
	 * @param task a task
	 * @param prevState the state of the task environment at the start of the prior step
	 * @param action the action selected by the agent
	 * @param newState the state of the task environment at the end of the prior step
	 * @param reinforcement the value of the reinforcement signal for (prevState, action, newState)
	 */
	public void observeStep(T task, S prevState, A action, S newState, double reinforcement);
	
	/**
	 * Called before the first step of an episode.
	 * @param task a task
	 */
	public void observeEpisodeBegin(T task);
	
	/**
	 * Called after the final step of an episode has occurred. 
	 * @param task a task
	 */
	public void observeEpisodeEnd(T task);
	
	/**
	 * Resets the observer's internal state.
	 */
	public void reset();
}
