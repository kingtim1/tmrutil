package tmrutil.learning.rl;

import java.util.Arrays;

import org.rlcommunity.rlglue.codec.AgentInterface;
import org.rlcommunity.rlglue.codec.taskspec.TaskSpecVRLGLUE3;
import org.rlcommunity.rlglue.codec.types.Action;
import org.rlcommunity.rlglue.codec.types.Observation;
import org.rlcommunity.rlglue.codec.util.AgentLoader;

import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.math.Function;

/**
 * A wrapper class that enables using a {@link LearningSystem} in RL-Glue
 * environments.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RLGlueLearningSystemWrapper<S, A> implements AgentInterface {

	public static class ObservationToIntState implements
			Function<Observation, Integer> {
		private static final long serialVersionUID = -931515373376883009L;

		@Override
		public Integer evaluate(Observation x) throws IllegalArgumentException {
			return new Integer(x.intArray[0]);
		}
	}

	public static class ObservationToFactoredState implements
			Function<Observation, FactoredState> {
		private static final long serialVersionUID = -822931980873784209L;
		private int[] _maxValues;

		private ObservationToFactoredState(int[] maxValues) {
			_maxValues = Arrays.copyOf(maxValues, maxValues.length);
		}

		@Override
		public FactoredState evaluate(Observation x)
				throws IllegalArgumentException {
			FactoredState fstate = new FactoredStateImpl(x.intArray, _maxValues);
			return fstate;
		}

	}

	public static class ObservationToDoubleArray implements
			Function<Observation, double[]> {
		private static final long serialVersionUID = 5616696701444568331L;

		@Override
		public double[] evaluate(Observation x) throws IllegalArgumentException {
			return Arrays.copyOf(x.doubleArray, x.doubleArray.length);
		}

	}

	public static class ActionFromInt implements Function<Integer, Action> {
		private static final long serialVersionUID = 616488174190869625L;

		@Override
		public Action evaluate(Integer x) throws IllegalArgumentException {
			Action action = new Action(2, 0, 0);
			action.intArray[0] = x.intValue();
			return action;
		}
	}

	public static class ActionFromIntArray implements Function<int[], Action> {
		private static final long serialVersionUID = -6698556549062987390L;

		@Override
		public Action evaluate(int[] x) throws IllegalArgumentException {
			Action action = new Action(x.length, 0, 0);
			System.arraycopy(x, 0, action.intArray, 0, x.length);
			return action;
		}

	}

	private RLGlueAlgorithmFactory<S, A, ? extends LearningSystem<S, A>> _factory;
	private LearningSystem<S, A> _agent;
	private Function<Observation, S> _obsToState;
	private Function<A, Action> _aToAction;

	private S _prevState;
	private A _prevAction;

	private boolean _learning;

	public RLGlueLearningSystemWrapper(
			RLGlueAlgorithmFactory<S, A, ? extends LearningSystem<S, A>> factory) {
		_factory = factory;

		_agent = null;
		_obsToState = null;
		_aToAction = null;

		learning(true);
	}

	/**
	 * Sets whether this agent will train on the received observations.
	 * 
	 * @param learning
	 *            true if the agent should learn on the received samples;
	 *            otherwise false indicates that the agent will not learn
	 */
	public void learning(boolean learning) {
		_learning = learning;
	}

	/**
	 * Returns true if this agent trains on the received samples. Otherwise the
	 * agent does no learning.
	 * 
	 * @return true if this agent trains on the received samples; otherwise
	 *         false
	 */
	public boolean isLearning() {
		return _learning;
	}

	@Override
	public void agent_cleanup() {
	}

	@Override
	public void agent_end(double reinforcement) {
		try {
			if (isLearning()) {
				if (_prevState != null && _prevAction != null) {
					_agent.train(_prevState, _prevAction, _prevState,
							reinforcement);
				}
			}
		} catch (IllegalArgumentException ex) {
		}
	}

	@Override
	public void agent_init(String taskSpecification) {
		TaskSpecVRLGLUE3 taskSpec = new TaskSpecVRLGLUE3(taskSpecification);
		_agent = _factory.newInstance(taskSpec);
		_obsToState = _factory.obsToStateMapping(taskSpec);
		_aToAction = _factory.actionToActionMapping(taskSpec);

		_prevState = null;
		_prevAction = null;
	}

	@Override
	public String agent_message(String message) {
		return message;
	}

	@Override
	public Action agent_start(Observation state) {
		try {
			S s = _obsToState.evaluate(state);
			A a = _agent.policy(s);

			_prevState = s;
			_prevAction = a;

			Action action = _aToAction.evaluate(a);
			return action;
		} catch (IllegalArgumentException ex) {
			return new Action(2, 0, 0);
		}
	}

	@Override
	public Action agent_step(double reinforcement, Observation state) {
		S s = _obsToState.evaluate(state);

		try {
			if (isLearning()) {
				_agent.train(_prevState, _prevAction, s, reinforcement);
			}

			A a = _agent.policy(s);
			Action action = _aToAction.evaluate(a);

			_prevState = s;
			_prevAction = a;

			return action;
		} catch (IllegalArgumentException ex) {
			return new Action(2, 0, 0);
		}
	}

	public static <S, A> void init(LearningSystem<S, A> agent,
			Function<Observation, S> obsToState, Function<A, Action> aToAction) {
		init(new RLGlueAlgorithmFactory.Static<S, A, LearningSystem<S, A>>(
				agent, obsToState, aToAction));
	}

	public static <S, A, L extends LearningSystem<S, A>> void init(
			RLGlueAlgorithmFactory<S, A, L> factory) {
		AgentLoader theLoader = new AgentLoader(
				new RLGlueLearningSystemWrapper<S, A>(factory));
		theLoader.run();
	}

}
