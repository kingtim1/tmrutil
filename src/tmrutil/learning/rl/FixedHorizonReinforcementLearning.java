package tmrutil.learning.rl;


/**
 * An interface for fixed horizon reinforcement learning algorithms.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface FixedHorizonReinforcementLearning<S, A> extends
		ValueBasedLearningSystem<S, A>
{
	/**
	 * Returns the number of decision epochs this algorithm has faced.
	 * @return a nonnegative integer 
	 */
	public long getTime();
	
	/**
	 * Returns the horizon size considered by this reinforcement learning
	 * algorithm. The horizon size is the window of observations over which the
	 * algorithm considers when determining the long term value of a state.
	 * 
	 * @return a positive integer
	 */
	public int getHorizon();
}
