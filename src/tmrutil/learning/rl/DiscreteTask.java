package tmrutil.learning.rl;

import tmrutil.util.Interval;

/**
 * Represents a discrete state, discrete action reinforcement learning task.
 * @author Timothy A. Mann
 *
 */
public abstract class DiscreteTask extends DiscreteActionTask<Integer>
{
	private int _numStates;
	private Interval _rInterval;
	
	/**
	 * Constructs a discrete state, discrete action task.
	 * @param numStates the number of states
	 * @param numActions the number of actions
	 * @param reinforcementInterval the interval bounding minimum and maximum immediate reinforcements
	 */
	public DiscreteTask(int numStates, int numActions, Interval reinforcementInterval)
	{
		super(numActions);
		
		if(numStates < 1){
			throw new IllegalArgumentException("Cannot construct a task with less than 1 state.");
		}
		_numStates = numStates;
		
		if(reinforcementInterval == null){
			throw new NullPointerException("Cannot construct a task with a null reinforcement interval.");
		}
		_rInterval = reinforcementInterval;
	}

	/**
	 * Returns the number of states in this task.
	 * @return the number of states in this task
	 */
	public final int numberOfStates()
	{
		return _numStates;
	}

	/**
	 * Returns the maximum possible immediate reward in this task.
	 * @return the maximum possible immediate reward
	 */
	public final double rmax()
	{
		return _rInterval.getMax();
	}
	
	/**
	 * Returns the minimum possible immediate reward in this task.
	 * @return the minimum possible immediate reward
	 */
	public final double rmin()
	{
		return _rInterval.getMin();
	}
}
