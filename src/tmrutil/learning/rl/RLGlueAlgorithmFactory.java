package tmrutil.learning.rl;

import org.rlcommunity.rlglue.codec.taskspec.TaskSpecVRLGLUE3;
import org.rlcommunity.rlglue.codec.types.Action;
import org.rlcommunity.rlglue.codec.types.Observation;

import tmrutil.math.Function;

/**
 * Constructs new instance of a learning system from a task specification.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type of the learning system type
 * @param <A>
 *            the action type of the learning system type
 * @param <L>
 *            the learning system type
 */
public interface RLGlueAlgorithmFactory<S, A, L extends LearningSystem<S, A>> {

	/**
	 * Constructs a new learning system instance from an RL-Glue task
	 * specification.
	 * 
	 * @param taskSpec
	 *            a specification of a task
	 * @return a new learning system instance
	 * @throws IllegalArgumentException
	 *             if the type of learning system built by this factory cannot
	 *             be used for the specified task description
	 */
	public L newInstance(TaskSpecVRLGLUE3 taskSpec);

	/**
	 * Returns a mapping that can transform observations into a specified state
	 * type.
	 * 
	 * @param taskSpec
	 *            a specification of a task
	 * @return a mapping from observations to <code>S</code>
	 */
	public Function<Observation, S> obsToStateMapping(TaskSpecVRLGLUE3 taskSpec);

	/**
	 * Returns a mapping that can transform the learning system's action type to
	 * an RL-Glue action type.
	 * 
	 * @param taskSpec
	 *            a specification of a task
	 * @return a mapping from learning system actions to RL-Glue actions
	 */
	public Function<A, Action> actionToActionMapping(TaskSpecVRLGLUE3 taskSpec);
	
	/**
	 * A static factory in the sense that all of the methods return fixed values.
	 * @author Timothy A. Mann
	 *
	 * @param <S> the state type
	 * @param <A> the action type
	 * @param <L> the learning system type
	 */
	public static class Static<S,A,L extends LearningSystem<S,A>> implements RLGlueAlgorithmFactory<S,A,L>{

		private L _agent;
		private Function<Observation,S> _obsToState;
		private Function<A,Action> _aToAction;
		
		public Static(L agent, Function<Observation,S> obsToState, Function<A,Action> aToAction)
		{
			_agent = agent;
			_obsToState = obsToState;
			_aToAction = aToAction;
		}
		
		@Override
		public L newInstance(TaskSpecVRLGLUE3 taskSpec) {
			return _agent;
		}

		@Override
		public Function<Observation, S> obsToStateMapping(
				TaskSpecVRLGLUE3 taskSpec) {
			return _obsToState;
		}

		@Override
		public Function<A, Action> actionToActionMapping(
				TaskSpecVRLGLUE3 taskSpec) {
			return _aToAction;
		}
		
	}
}
