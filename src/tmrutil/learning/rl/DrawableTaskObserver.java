package tmrutil.learning.rl;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import tmrutil.graphics.Drawable;

/**
 * A task observer that dumps images of the task unfolding to a directory.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 * @param <T> the task type
 */
public class DrawableTaskObserver<S, A, T extends Task<S, A>> implements
		TaskObserver<S, A, T> {

	private File _imgDir;
	private Dimension _imgDims;

	private int _episodeNum = 0;
	private int _timestep = 0;

	public DrawableTaskObserver(String imgDir, Dimension imgDims)
			throws IOException {
		_imgDir = new File(imgDir);
		if (!_imgDir.exists()) {
			_imgDir.mkdirs();
		} else if (!_imgDir.isDirectory()) {
			throw new IOException(
					"Images must be stored in a directory. Given file: "
							+ imgDir);
		}
		_imgDims = imgDims;
	}

	private void dumpImage(T task) {
		if (task instanceof Drawable) {
			Drawable dtask = (Drawable) task;
			BufferedImage img = new BufferedImage(_imgDims.width,
					_imgDims.height, BufferedImage.TYPE_3BYTE_BGR);
			Graphics2D g2 = (Graphics2D) img.createGraphics();
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, _imgDims.width, _imgDims.height);
			
			g2.setColor(Color.BLACK);
			dtask.draw(g2, _imgDims.width, _imgDims.height);

			File outFile = new File(_imgDir, "episode_" + _episodeNum
					+ "_timestep_" + _timestep + ".png");

			try {
				ImageIO.write(img, "png", outFile);
			} catch (IOException ex) {
				// Drop the frame
			}
		}
	}

	@Override
	public void observeStep(T task, S prevState, A action, S newState,
			double reinforcement) {
		_timestep++;
		dumpImage(task);
	}

	@Override
	public void observeEpisodeBegin(T task) {
		_episodeNum++;
		_timestep = 0;
		//dumpImage(task);
	}

	@Override
	public void observeEpisodeEnd(T task) {
		_timestep++;
		dumpImage(task);
	}

	@Override
	public void reset() {
		_episodeNum = 0;
		_timestep = 0;
	}

}
