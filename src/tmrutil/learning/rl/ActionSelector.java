package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;
import tmrutil.util.Interval;
import tmrutil.util.ListUtil;

/**
 * A value-based action selector is used for selecting an action from either a
 * finite or infinite pool of actions according to some selection policy.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public interface ActionSelector<S, A>
{
	/**
	 * Softmax is an action selection strategy which balances exploration with
	 * exploitation using a single parameter alpha. If alpha is set to zero,
	 * then actions are selected according to a uniform random distribution. As
	 * alpha increases to infinity the distribution becomes more peaked over the
	 * action that maximizes the estimated Q-function.
	 * 
	 * @author Timothy A. Mann
	 * 
	 * @param <S>
	 *            the state type
	 * @param <A>
	 *            the action type
	 */
	public static class Softmax<S, A> implements ActionSelector<S, A>
	{
		/** A list of valid actions. */
		private List<A> _validActions;
		/** A buffer used to hold the values associated with each action. */
		private double[] _valueBuff;
		/** Parameter for controlling greediness of action selection. */
		private double _alpha;

		/**
		 * The exploration filter.
		 */
		private ExplorationFilter<S, A> _efilter;

		public Softmax(Collection<A> validActions, double alpha)
		{
			this(null, validActions, alpha);
		}

		public Softmax(ExplorationFilter<S, A> efilter,
				Collection<A> validActions, double alpha)
		{
			setAlpha(alpha);
			_efilter = efilter;
			_validActions = new ArrayList<A>(validActions);
			_valueBuff = new double[_validActions.size()];
		}

		@Override
		public Optimization getOptimization()
		{
			return Optimization.MAXIMIZE;
		}

		@Override
		public boolean isMaximizing()
		{
			return true;
		}

		@Override
		public boolean isMinimizing()
		{
			return false;
		}

		@Override
		public A policy(S state, QFunction<S, A> qfunc)
		{
			List<A> validActions = getValidActions(state);

			// Compute the probability of selecting each action
			double vsum = 0;
			for (int i = 0; i < _valueBuff.length; i++) {
				if (i < validActions.size()) {
					double qsa = qfunc.value(state, validActions.get(i));
					_valueBuff[i] = Math.exp(_alpha
							* qsa);
					vsum += _valueBuff[i];
				} else {
					_valueBuff[i] = 0;
				}
			}

			if (vsum <= 0.0) {
				return random(state);
			} else {
				// Select an action according to the generated probabilities
				double r = Random.uniform();
				double rsum = 0;
				for (int i = 0; i < validActions.size(); i++) {
					rsum += (_valueBuff[i] / vsum);
					if (r <= rsum) {
						return validActions.get(i);
					}
				}
			}
			throw new DebugException(
					"Evaluated all actions without selecting a winner. This should not happen!");
		}

		/**
		 * Returns the value of the parameter controlling the greediness of this
		 * softmax operation. If alpha is equal to zero, then the action
		 * selection will be uniformly distributed. As alpha goes to infinity
		 * this softmax operation becomes more greedy.
		 * 
		 * @return a nonnegative scalar value
		 */
		public double getAlpha()
		{
			return _alpha;
		}

		/**
		 * Sets the value of the parameter controlling the greediness of this
		 * softmax operation. If alpha is equal to zero, then the action
		 * selection will be uniformly distributed. As alpha goes to infinity
		 * this softmax operation becomes more greedy.
		 * 
		 * @param alpha
		 *            a nonnegative scalar value
		 */
		public void setAlpha(double alpha)
		{
			if (alpha < 0) {
				throw new IllegalArgumentException(
						"Alpha parameter cannot be negative.");
			}
			_alpha = alpha;
		}

		@Override
		public A random(S state)
		{
			List<A> validActions = getValidActions(state);
			return validActions.get(Random.nextInt(validActions.size()));
		}

		@Override
		public Collection<A> getActions()
		{
			return _validActions;
		}

		private List<A> getValidActions(S state)
		{
			if (_efilter == null) {
				return _validActions;
			} else {
				List<A> validActions = new ArrayList<A>();
				for (A action : _validActions) {
					if (_efilter.canExplore(state, action)) {
						validActions.add(action);
					}
				}
				return validActions;
			}
		}

	}

	/**
	 * Epsilon-greedy sequence is similar to the normal epsilon-greedy action
	 * selection strategy but allows epsilon to be decreased over time.
	 * 
	 * @author Timothy A. Mann
	 * 
	 * @param <S>
	 *            the state type
	 * @param <A>
	 *            the action type
	 */
	public static class EpsilonGreedySequence<S, A> implements
			ActionSelector<S, A>
	{
		private double _initEpsilon;
		private double _finalEpsilon;
		private List<A> _validActions;
		private Optimization _opType;
		private long _time;
		private long _sequenceLength;

		private ExplorationFilter<S,A> _efilter;
		
		/**
		 * Constructs an epsilon-greedy sequence where epsilon decreases over
		 * time
		 * 
		 * @param epsilonInterval
		 *            the interval containing the minimum and maximum values of
		 *            epsilon
		 * @param sequenceLength
		 *            the number of calls to the policy before epsilon takes on
		 *            the minimum value
		 * @param validActions
		 *            a collection of valid actions
		 * @param opType
		 *            the type of optimization to use (MINIMIZE or MAXIMIZE)
		 */
		public EpsilonGreedySequence(Interval epsilonInterval, int sequenceLength, Collection<A> validActions, Optimization opType)
		{
			this(null, epsilonInterval, sequenceLength, validActions, opType);
		}
		
		/**
		 * Constructs an epsilon-greedy sequence where epsilon decreases over
		 * time
		 * 
		 * @param efilter the exploration filter to use
		 * @param epsilonInterval
		 *            the interval containing the minimum and maximum values of
		 *            epsilon
		 * @param sequenceLength
		 *            the number of calls to the policy before epsilon takes on
		 *            the minimum value
		 * @param validActions
		 *            a collection of valid actions
		 * @param opType
		 *            the type of optimization to use (MINIMIZE or MAXIMIZE)
		 */
		public EpsilonGreedySequence(ExplorationFilter<S,A> efilter, Interval epsilonInterval,
				int sequenceLength, Collection<A> validActions,
				Optimization opType)
		{
			_efilter = efilter;
			_initEpsilon = epsilonInterval.getMax();
			_finalEpsilon = epsilonInterval.getMin();
			_sequenceLength = sequenceLength;
			_validActions = new ArrayList<A>(validActions);
			_opType = opType;
			_time = 0;
		}

		@Override
		public Collection<A> getActions()
		{
			return _validActions;
		}
		
		private List<A> getValidActions(S state)
		{
			if (_efilter == null) {
				return _validActions;
			} else {
				List<A> validActions = new ArrayList<A>();
				for (A action : _validActions) {
					if (_efilter.canExplore(state, action)) {
						validActions.add(action);
					}
				}
				return validActions;
			}
		}

		@Override
		public Optimization getOptimization()
		{
			return _opType;
		}

		@Override
		public boolean isMaximizing()
		{
			return _opType.equals(Optimization.MAXIMIZE);
		}

		@Override
		public boolean isMinimizing()
		{
			return _opType.equals(Optimization.MINIMIZE);
		}

		public double getEpsilon()
		{
			if (_time > _sequenceLength) {
				return _finalEpsilon;
			} else {
				double frac = _time / (double) _sequenceLength;
				double diff = _initEpsilon - _finalEpsilon;
				return _initEpsilon + (frac * diff);
			}
		}

		@Override
		public A policy(S state, QFunction<S, A> qfunc)
		{
			double epsilon = getEpsilon();
			_time++;

			List<A> validActions = getValidActions(state);
			if (Random.uniform() < epsilon) { // Draw an action from uniform
				// random distrobution
				int rInd = Random.nextInt(validActions.size());
				return validActions.get(rInd);
			} else { // Follow a greedy policy with respect to the value
						// function
				A bestAct = null;
				double bestVal = 0;

				// Select the best action
				int[] rperm = VectorOps.randperm(validActions.size());
				for (int i = 0; i < validActions.size(); i++) {
					if (bestAct == null) {
						A action = validActions.get(rperm[i]);
						bestVal = qfunc.value(state, action);
						bestAct = action;
					} else {
						A action = validActions.get(rperm[i]);
						double value = qfunc.value(state, action);
						if ((isMaximizing() && value > bestVal)
								|| (isMinimizing() && value < bestVal)) {
							bestVal = value;
							bestAct = action;
						}
					}
				}
				return bestAct;
			}
		}

		@Override
		public A random(S state)
		{
			List<A> validActions = getValidActions(state);
			int r = Random.nextInt(validActions.size());
			return validActions.get(r);
		}

	}

	/**
	 * Provides a simple epsilon-greedy action selection strategy. An
	 * epsilon-greedy action selection strategy normally follows the current
	 * estimated policy (exploitation), however, there is a probability
	 * <code>epsilon</code> that an action will instead be drawn from a uniform
	 * random distribution.
	 * 
	 * @author Timothy A. Mann
	 * 
	 * @param <S>
	 *            the state type
	 * @param <A>
	 *            the action type
	 */
	public static class EpsilonGreedy<S, A> implements ActionSelector<S, A>
	{
		private ExplorationFilter<S,A> _efilter;
		private double _epsilon;
		private List<A> _validActions;
		private Optimization _opType;

		/**
		 * Constructs an epsilon-greedy action selector with the specified
		 * number of actions.
		 * 
		 * @param numActions
		 *            the number of actions in the action selector {0, 1, ... ,
		 *            numActions-1}
		 * @param epsilon
		 *            the probability of taking an exploratory action
		 * @param opType
		 *            the optimization type
		 * @return an epsilon-greedy action selector
		 * @throws IllegalArgumentException
		 *             if numActions is less than 1 or epsilon is not in [0, 1]
		 */
		public static final EpsilonGreedy<double[], Integer> discreteEpsilonGreedy(
				int numActions, double epsilon, Optimization opType)
				throws IllegalArgumentException
		{
			return new EpsilonGreedy<double[], Integer>(
					ListUtil.range(numActions), epsilon, opType);
		}
		
		/**
		 * Constructs an epsilon-greedy action selector.
		 * 
		 * @param validActions
		 *            a collection of valid actions
		 * @param epsilon
		 *            the probability of selecting an action at random instead
		 *            of following the policy
		 * @param opType
		 *            the type of optimization (minimization or maximization)
		 * @throws IllegalArgumentException
		 *             if <code>epsilon < 0 || epsilon > 1</code>
		 */
		public EpsilonGreedy(Collection<A> validActions, double epsilon,
				Optimization opType){
			this(null, validActions, epsilon, opType);
		}

		/**
		 * Constructs an epsilon-greedy action selector.
		 * 
		 * @param efilter the exploration filter to use
		 * @param validActions
		 *            a collection of valid actions
		 * @param epsilon
		 *            the probability of selecting an action at random instead
		 *            of following the policy
		 * @param opType
		 *            the type of optimization (minimization or maximization)
		 * @throws IllegalArgumentException
		 *             if <code>epsilon < 0 || epsilon > 1</code>
		 */
		public EpsilonGreedy(ExplorationFilter<S, A> efilter, Collection<A> validActions, double epsilon,
				Optimization opType)
		{
			_efilter = efilter;
			_validActions = new ArrayList<A>(validActions);
			setEpsilon(epsilon);
			_opType = opType;
		}

		/**
		 * Sets the probability of drawing an action from a uniform random
		 * distribution rather than following the estimated policy.
		 * 
		 * @param epsilon
		 *            the probability of drawing an action from a uniform random
		 *            distribution rather than following the estimated policy
		 * @throws IllegalArgumentException
		 *             if <code>epsilon < 0 || epsilon > 1</code>
		 */
		public void setEpsilon(double epsilon) throws IllegalArgumentException
		{
			if (epsilon < 0 || epsilon > 1) {
				throw new IllegalArgumentException(
						"Epsilon must be in the interval [0, 1] because it represents a probability.");
			}
			_epsilon = epsilon;
		}

		/**
		 * Returns the probability of drawing an action from a uniform random
		 * distribution rather than following the estimated policy.
		 * 
		 * @return a scalar value in the interval [0, 1] representing a
		 *         probability
		 */
		public double getEpsilon()
		{
			return _epsilon;
		}

		@Override
		public A policy(S state, QFunction<S, A> qfunc)
		{
			List<A> validActions = getValidActions(state);
			if (Random.uniform() < _epsilon) { // Draw an action from uniform
				// random distrobution
				int rInd = Random.nextInt(validActions.size());
				return validActions.get(rInd);
			} else { // Follow a greedy policy with respect to the value
						// function
				A bestAct = null;
				double bestVal = 0;

				// Select the best action
				int[] rperm = VectorOps.randperm(validActions.size());
				for (int i = 0; i < rperm.length; i++) {
					if (bestAct == null) {
						A action = validActions.get(rperm[i]);
						bestVal = qfunc.value(state, action);
						bestAct = action;
					} else {
						A action = validActions.get(rperm[i]);
						double value = qfunc.value(state, action);
						if ((isMaximizing() && value > bestVal)
								|| (isMinimizing() && value < bestVal)) {
							bestVal = value;
							bestAct = action;
						}
					}
				}
				return bestAct;
			}
		}

		@Override
		public boolean isMaximizing()
		{
			return _opType == Optimization.MAXIMIZE;
		}

		@Override
		public boolean isMinimizing()
		{
			return _opType == Optimization.MINIMIZE;
		}

		@Override
		public Optimization getOptimization()
		{
			return _opType;
		}

		@Override
		public A random(S state)
		{
			List<A> validActions = getValidActions(state);
			return validActions.get(Random.nextInt(validActions.size()));
		}

		@Override
		public Collection<A> getActions()
		{
			return _validActions;
		}

		private List<A> getValidActions(S state)
		{
			if (_efilter == null) {
				return _validActions;
			} else {
				List<A> validActions = new ArrayList<A>();
				for (A action : _validActions) {
					if (_efilter.canExplore(state, action)) {
						validActions.add(action);
					}
				}
				return validActions;
			}
		}
	}

	/**
	 * Given a state, an action is selected according to some policy.
	 * 
	 * @param state
	 *            the description of a state
	 * @param qfunc
	 *            a function that determines the value of state-action pairs
	 * @return a valid action for the current state according to some policy
	 */
	public A policy(S state, QFunction<S, A> qfunc);

	/**
	 * Return true if this action selector is attempting to maximize the long
	 * term sum of reinforcements. Otherwise this action selector will return
	 * false indicating that it is trying to minimize the long term sum of
	 * reinforcements.
	 * 
	 * @return true if maximizing reinforcements; otherwise false
	 */
	public boolean isMaximizing();

	/**
	 * Return true if this action selector is attempting to minimize the long
	 * term sum of reinforcements. Otherwise this action selector will return
	 * false indicating that it is trying to maximize the long term sum of
	 * reinforcements.
	 * 
	 * @return true if minimizing reinforcement; otherwise false
	 */
	public boolean isMinimizing();

	/**
	 * Returns the type of optimization being performed by this action selector.
	 * 
	 * @return either MINIMIZE or MAXIMIZE
	 */
	public Optimization getOptimization();

	/**
	 * Selects and returns a valid action according to a uniform random
	 * distribution.
	 * 
	 * @param state a state
	 * 
	 * @return a randomly selected action
	 */
	public A random(S state);

	/**
	 * Returns the collection of valid actions. Implementing this method is
	 * optional.
	 * 
	 * @return a collection of valid actions
	 */
	public Collection<A> getActions();
}
