package tmrutil.learning.rl.smdp;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.Policy;
import tmrutil.stats.Random;

/**
 * A wrapper class for converting policies over option indices into policies
 * over primitive actions.
 * 
 * @author Timothy A. Mann
 *
 * @param <S>
 *            the state type
 * @param <A>
 *            the primitive action type
 */
public class OptionIndexToPrimitivePolicy<S, A> implements Policy<S, A> {

	private Policy<S, Integer> _optionPolicy;
	private Integer _currentOption;
	private ActionSet<S, ? extends Option<S, A>> _options;
	private int _duration;

	public OptionIndexToPrimitivePolicy(Policy<S, Integer> optionPolicy,
			ActionSet<S, ? extends Option<S, A>> options) {
		_optionPolicy = optionPolicy;
		_options = options;
		_currentOption = null;
		_duration = 0;
	}

	@Override
	public A policy(S state) {
		if (_currentOption == null
				|| _options.action(_currentOption).terminationProb(state,
						_duration) > Random.uniform()) {
			_currentOption = _optionPolicy.policy(state);
			_duration = 0;
		}

		_duration++;
		return _options.action(_currentOption).policy(state);
	}
}
