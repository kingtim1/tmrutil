package tmrutil.learning.rl.smdp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.learning.BatchFunctionApproximator;
import tmrutil.learning.CrossValidation;
import tmrutil.learning.CrossValidation.CrossValidationResult;
import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.learning.rl.ActionSet.Unrestricted;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.math.Metric;
import tmrutil.optim.Optimization;
import tmrutil.util.DebugException;
import tmrutil.util.Factory;
import tmrutil.util.FileDataStore;
import tmrutil.util.Pair;

/**
 * Implements Fitted Value Iteration (FVI).
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 */
public class FittedVI<S, F extends BatchFunctionApproximator<S, Double>> {

	public static final class SquaredError implements Metric<Double> {

		@Override
		public double distance(Double x, Double y) {
			double diff = x - y;
			return diff * diff;
		}

	}

	public static final class AbsError implements Metric<Double> {

		@Override
		public double distance(Double x, Double y) {
			double diff = x - y;
			return Math.abs(diff);
		}

	}

	public class VIResult {
		private VFunction<S> _vfunc;
		private List<Double> _bellmanSamples;
		private CrossValidationResult<S, Double> _cvResult;

		public VIResult(VFunction<S> vfunc, List<Double> bellmanSamples,
				CrossValidationResult<S, Double> cvResult) {
			_vfunc = vfunc;
			_bellmanSamples = bellmanSamples;
			_cvResult = cvResult;
		}

		public VFunction<S> vfunction() {
			return _vfunc;
		}

		public List<Double> bellmanSamples() {
			return _bellmanSamples;
		}

		public CrossValidationResult<S, Double> cvResult() {
			return _cvResult;
		}
	}

	public static final double DEFAULT_TRAIN_RATIO = 0.8;
	public static final int DEFAULT_NUM_CV_TESTS = 5;

	private Iterable<S> _states;
	private List<Factory<F>> _factories;
	private double _discountFactor;

	private int _numSamples;
	private Optimization _opType;

	private double _trainRatio;
	private int _numCVTests;

	public FittedVI(Iterable<S> states, Factory<F> factory, int numSamples,
			double discountFactor, Optimization opType) {
		this(states, oneFactoryList(factory), numSamples, discountFactor,
				opType, true);
	}

	public FittedVI(Iterable<S> states, List<Factory<F>> factories,
			int numSamples, double discountFactor, Optimization opType,
			boolean skipModelSelection) {
		this(states, factories, numSamples, discountFactor, opType,
				DEFAULT_TRAIN_RATIO, skipModelSelection? 0 : DEFAULT_NUM_CV_TESTS);
	}

	public FittedVI(Iterable<S> states, List<Factory<F>> factories,
			int numSamples, double discountFactor, Optimization opType,
			double trainRatio, int numCVTests) {
		_states = states;
		_factories = new ArrayList<Factory<F>>(factories);
		_numSamples = numSamples;
		_discountFactor = discountFactor;
		_opType = opType;

		_trainRatio = trainRatio;
		_numCVTests = numCVTests;
	}

	private static <S, F extends BatchFunctionApproximator<S, Double>> List<Factory<F>> oneFactoryList(
			Factory<F> factory) {
		List<Factory<F>> flist = new ArrayList<Factory<F>>(1);
		flist.add(factory);
		return flist;
	}

	/**
	 * Performs one iteration of FVI.
	 * 
	 * @param vfunc
	 *            an estimate of the optimal value function (this can be
	 *            arbitrarily far from optimal)
	 * @param options
	 *            a collection of options to perform updates
	 * @param sim
	 *            a simulator
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps that an option can execute for
	 * @return a pair where the first element is the new estimate of the optimal
	 *         value function and the second element is the list of empirical
	 *         estimates of the Bellman backup operator
	 */
	public <A> VIResult iterateV(VFunction<S> vfunc,
			Collection<Option<S, A>> options, Simulator<S, ?, A> sim,
			int maxTrajectoryLength) {
		ActionSet<S, Option<S, A>> optionSet = new ActionSet.Unrestricted<S, Option<S, A>>(
				options);
		return iterateV(vfunc, optionSet, sim, maxTrajectoryLength);
	}

	/**
	 * Performs one iteration of FVI.
	 * 
	 * @param vfunc
	 *            an estimate of the optimal value function (this can be
	 *            arbitrarily far from optimal)
	 * @param options
	 *            a collection of options to perform updates
	 * @param sim
	 *            a simulator
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps that an option can execute for
	 * @return a pair where the first element is the new estimate of the optimal
	 *         value function and the second element is the list of empirical
	 *         estimates of the Bellman backup operator
	 */
	public <A> VIResult iterateV(VFunction<S> vfunc,
			ActionSet<S, Option<S, A>> options, Simulator<S, ?, A> sim,
			int maxTrajectoryLength) {
		Collection<Pair<S, Double>> samples = new ArrayList<Pair<S, Double>>();
		List<Double> vhat = new ArrayList<Double>();
		for (S state : _states) {
			Double best = null;
			// try {
			List<Option<S, A>> validOptions = options.validList(state);
			for (Option<S, A> option : validOptions) {
				double sample = generateBellmanSample(vfunc, sim, state,
						option, maxTrajectoryLength);

				if (best == null) {
					best = sample;
				} else {

					boolean updateMaximize = (_opType
							.equals(Optimization.MAXIMIZE) && best < sample);
					boolean updateMinimize = (_opType
							.equals(Optimization.MINIMIZE) && best > sample);
					boolean update = updateMaximize || updateMinimize;

					if (update) {
						best = sample;
					}
				}
			}
			samples.add(new Pair<S, Double>(state, best));
			vhat.add(best);
			/*
			 * } catch (IllegalArgumentException ex) { ex.printStackTrace(); //
			 * Skip this state }
			 */
		}

		return finishIteration(samples, vhat);
	}

	public <A> VIResult iterateV(VFunction<S> vfunc,
			List<OptionSamples<S, A>> osamples) {
		Collection<Pair<S, Double>> samples = new ArrayList<Pair<S, Double>>(
				osamples.size());
		List<Double> vhat = new ArrayList<Double>(osamples.size());

		for (OptionSamples<S, A> os : osamples) {
			Double best = null;

			for (int i = 0; i < os.numberOfOptions(); i++) {
				double sample = generateBellmanSample(vfunc, os.outcomes(i));

				if (best == null) {
					best = sample;
				} else {
					if (isBetter(best, sample)) {
						best = sample;
					}
				}

			}
			
			if(best == null){
				best = 0.0;
			}

			samples.add(new Pair<S, Double>(os.state(), best));
			vhat.add(best);
		}

		return finishIteration(samples, vhat);
	}

	public <A> VIResult iterateV(VFunction<S> vfunc,
			StoredOptionSamples<S, A> storedSamples) {
		Collection<Pair<S, Double>> samples = new ArrayList<Pair<S, Double>>(
				storedSamples.numberOfStates());
		List<Double> vhat = new ArrayList<Double>(
				storedSamples.numberOfStates());

		for (int i = 0; i < storedSamples.numberOfStates(); i++) {
			S state = storedSamples.state(i);
			Collection<Integer> optionIndices = storedSamples.validOptions(i);

			Double best = null;
			for (Integer j : optionIndices) {
				try {
					List<OptionOutcome<S, ?>> ocs = storedSamples.samples(i, j);
					double score = generateBellmanSample(vfunc, ocs);
					if (best == null || isBetter(best, score)) {
						best = score;
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}

			if(best == null){
				best = 0.0;
			}
			
			samples.add(new Pair<S, Double>(state, best));
			vhat.add(best);
		}

		return finishIteration(samples, vhat);
	}
	
	private VIResult finishIteration(Collection<Pair<S,Double>> samples, List<Double> vhat){
		CrossValidationResult<S, Double> cvResult = null;
		Factory<F> factory = _factories.get(0);
		if (_numCVTests >= 1) {
			cvResult = modelSelection(samples);
			factory = _factories.get(cvResult.bestModelIndex());
		}
		VFunction<S> newV = train(samples, factory);
		return new VIResult(newV, vhat, cvResult);
	}

	private boolean isBetter(double baseScore, double newScore) {
		if (_opType.equals(Optimization.MAXIMIZE)) {
			return newScore > baseScore;
		} else {
			return newScore < baseScore;
		}
	}

	private CrossValidationResult<S, Double> modelSelection(
			Collection<Pair<S, Double>> samples) {
		List<BatchFunctionApproximator<S, Double>> models = new ArrayList<BatchFunctionApproximator<S, Double>>();
		for (int i = 0; i < _factories.size(); i++) {
			models.add(_factories.get(i).newInstance());
		}
		CrossValidation<S, Double> cv = new CrossValidation<S, Double>(
				new AbsError(), models, samples);
		CrossValidationResult<S, Double> result = cv.cv(_trainRatio,
				_numCVTests);
		return result;
	}

	private VFunction<S> train(Collection<Pair<S, Double>> samples,
			Factory<F> factory) {
		FittedVFunction<S> newV = new FittedVFunction<S>(factory, 0.0);
		newV.train(samples);
		return newV;
	}

	private <A> Double generateBellmanSample(VFunction<S> vfunc,
			Simulator<S, ?, A> sim, S state, Option<S, A> option,
			int maxTrajectoryLength) {
		double qsum = 0;
		for (int i = 0; i < _numSamples; i++) {
			OptionOutcome<S, ?> oc = sim.sample(state, option, _discountFactor,
					maxTrajectoryLength);
			double reward = oc.dCumR();
			int lt = oc.duration();
			S tstate = oc.terminalState();

			qsum += reward + Math.pow(_discountFactor, lt)
					* vfunc.value(tstate);
		}

		double qval = qsum / _numSamples;
		return qval;
	}

	/**
	 * Generates an average Bellman update given a collection of option samples
	 * from the same state-option pair.
	 * 
	 * @param vfunc
	 *            the value function
	 * @param outcomes
	 *            the outcomes of a state-option pair
	 * @return an estimate of the Bellman update
	 */
	private <A> double generateBellmanSample(VFunction<S> vfunc,
			Collection<OptionOutcome<S, ?>> outcomes) {
		double qsum = 0;
		for (OptionOutcome<S, ?> oc : outcomes) {
			double r = oc.dCumR();
			int lt = oc.duration();
			S tstate = oc.terminalState();
			qsum += r + Math.pow(_discountFactor, lt) * vfunc.value(tstate);
		}
		double qval = qsum / outcomes.size();
		return qval;
	}

	/**
	 * Generates a collection of samples from the simulator.
	 * 
	 * @param sim
	 *            the simulator
	 * @param options
	 *            the options used for sampling
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps that the option is allowed to
	 *            execute for
	 * @return a list of samples
	 */
	public <A> List<OptionSamples<S, A>> generateSamples(
			Simulator<S, ?, A> sim, List<Option<S, A>> options,
			int maxTrajectoryLength) {
		List<OptionSamples<S, A>> samples = new ArrayList<OptionSamples<S, A>>();
		for (S state : _states) {
			samples.add(generateOptionSamples(sim, state, options,
					maxTrajectoryLength));
		}
		return samples;
	}

	/**
	 * Generates a collection of samples from the simulator.
	 * 
	 * @param sim
	 *            the simulator
	 * @param options
	 *            the options used for sampling
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps that the option is allowed to
	 *            execute for
	 * @return a list of samples
	 */
	public <A> List<OptionSamples<S, A>> generateSamples(
			Simulator<S, ?, A> sim, ActionSet<S, Option<S, A>> options,
			int maxTrajectoryLength) {
		List<OptionSamples<S, A>> samples = new ArrayList<OptionSamples<S, A>>();
		for (S state : _states) {
			samples.add(generateOptionSamples(sim, state,
					options.validList(state), maxTrajectoryLength));
		}
		return samples;
	}

	/**
	 * Samples the simulator at a specified state returning a collection of
	 * option outcomes.
	 * 
	 * @param sim
	 *            the simulator
	 * @param state
	 *            a state
	 * @param options
	 *            a list of options
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps that the option is allowed to
	 *            execute for
	 * @return a collection of option outcomes for the specified state and a
	 *         list of options
	 */
	private <A> OptionSamples<S, A> generateOptionSamples(
			Simulator<S, ?, A> sim, S state, List<Option<S, A>> options,
			int maxTrajectoryLength) {

		List<Collection<OptionOutcome<S, ?>>> loutcomes = new ArrayList<Collection<OptionOutcome<S, ?>>>();
		for (Option<S, A> option : options) {
			if (option.inInitialSet(state)) {
				List<OptionOutcome<S, ?>> outcomes = new ArrayList<OptionOutcome<S, ?>>(
						_numSamples);
				for (int s = 0; s < _numSamples; s++) {
					OptionOutcome<S, ?> oc = sim.sample(state, option,
							_discountFactor, maxTrajectoryLength);
					outcomes.add(oc);
				}

				loutcomes.add(outcomes);
			}
		}
		return new OptionSamples<S, A>(state, options, loutcomes);
	}

}
