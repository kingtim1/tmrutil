package tmrutil.learning.rl.smdp;



import tmrutil.learning.rl.Policy;
import tmrutil.stats.Random;

/**
 * A wrapper class for converting option policies into policies over primitive actions.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the primitive action type
 */
public class OptionToPrimitivePolicy<S, A> implements Policy<S, A> {
	private Policy<S,? extends Option<S,A>> _optionPolicy;
	
	private Option<S,A> _currentOption;
	private int _duration;
	
	public OptionToPrimitivePolicy(Policy<S,? extends Option<S,A>> optionPolicy)
	{
		_optionPolicy = optionPolicy;
		_currentOption = null;
		_duration = 0;
	}

	@Override
	public A policy(S state) {
		if(_currentOption == null || _currentOption.terminationProb(state, _duration) > Random.uniform()){
			_currentOption = _optionPolicy.policy(state);
			_duration = 0;
		}
		
		_duration++;
		return _currentOption.policy(state);
	}

}
