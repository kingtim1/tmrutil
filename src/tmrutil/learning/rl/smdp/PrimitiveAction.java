package tmrutil.learning.rl.smdp;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * A wrapper class that makes a primitive action into an
 * option/skill/macro-action.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the primitive action type
 */
public class PrimitiveAction<S, A> extends AbstractOption<S, A>
{
	private A _action;
	private Set<S> _validStates;

	public PrimitiveAction(int index, A action)
	{
		this(index, action, null);
	}

	public PrimitiveAction(int index, A action, Collection<S> validStates)
	{
		super(index);
		_action = action;
		if (validStates != null) {
			_validStates = new HashSet<S>(validStates);
		} else {
			_validStates = null;
		}
	}

	@Override
	public A policy(S state)
	{
		return _action;
	}

	@Override
	public double terminationProb(S state, int duration)
	{
		return 1;
	}

	@Override
	public boolean inInitialSet(S state)
	{
		if(_validStates == null){
			return true;
		}else{
			return _validStates.contains(state);
		}
	}

	@Override
	public int hashCode()
	{
		return _action.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof PrimitiveAction){
			PrimitiveAction<?,?> pact = (PrimitiveAction<?,?>)obj;
			return (_action.equals(pact._action));
		}
		return false;
	}
}
