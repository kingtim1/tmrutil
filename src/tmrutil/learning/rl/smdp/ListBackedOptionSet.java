package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.rl.ActionSet;

/**
 * <p>
 * An action set over options. The options are stored in a list. This means that
 * determining the set of valid options requires searching the entire list. This
 * is not a problem if the given option set is small or the options can almost
 * all be executed throughout the entire state space.
 * </p>
 * 
 * <p>
 * In the case where the set of options is large and each state only has a small
 * number of valid options this class should not be used. A more clever method
 * of searching the option space is needed.
 * </p>
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the primitive action type
 */
public class ListBackedOptionSet<S, A> extends ActionSet<S, Option<S,A>> {

	private List<Option<S,A>> _options;

	public ListBackedOptionSet(Collection<? extends Option<S,A>> options) {
		_options = new ArrayList<Option<S,A>>(options);
	}
	
	public ListBackedOptionSet(ActionSet<S, ? extends Option<S,A>> options)
	{
		_options = new ArrayList<Option<S,A>>();
		for(int i=0;i<options.numberOfActions();i++){
			_options.add(options.action(i));
		}
	}

	@Override
	public List<Integer> validIndices(S state) {
		List<Integer> inds = new ArrayList<Integer>();

		for (int i = 0; i < _options.size(); i++) {
			Option<S, A> option = _options.get(i);
			if (option.inInitialSet(state)) {
				inds.add(i);
			}
		}

		return inds;
	}

	@Override
	public Option<S,A> action(int index) {
		return _options.get(index);
	}

	@Override
	public int numberOfActions() {
		return _options.size();
	}

}
