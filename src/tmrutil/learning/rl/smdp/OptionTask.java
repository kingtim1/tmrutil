package tmrutil.learning.rl.smdp;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JFrame;

import tmrutil.graphics.Drawable;
import tmrutil.graphics.DrawableComponent;
import tmrutil.graphics.ProgressIterator;
import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.Task;

/**
 * An option task combines a {@link Task} with a set of options. This constructs a special kind of
 * SMDP Task.
 * 
 * @author Timothy A. Mann
 *
 * @param <S>
 *            the state type
 * @param <A>
 *            the primitive action type
 */
public class OptionTask<S, A, O extends Option<S, A>> {

	public abstract class Observer {
		/**
		 * Called after a step in an episode occurs.
		 * 
		 * @param result
		 *            the result of executing an option
		 */
		public abstract void observeOption(Result result);

		/**
		 * Called before the first step of an episode.
		 */
		public abstract void observeEpisodeBegin();

		/**
		 * Called after the final step of an episode has occurred.
		 */
		public abstract void observeEpisodeEnd();
	}

	/**
	 * The result of executing an option.
	 * 
	 * @author Timothy A. Mann
	 *
	 */
	public class Result {
		public List<S> stateSequence;
		public List<A> actionSequence;
		public List<Double> rSequence;
		public int duration;

		public Result(List<S> stateSequence, List<A> actionSequence,
				List<Double> rSequence, int duration) {
			this.stateSequence = stateSequence;
			this.actionSequence = actionSequence;
			this.rSequence = rSequence;
			this.duration = duration;
		}
	}

	/**
	 * An option set that allows options to be replaced.
	 * 
	 * @author Timothy A. Mann
	 *
	 */
	public class MutableOptionSet extends ActionSet<S, Option<S,A>> {

		private List<Option<S,A>> _options;

		public MutableOptionSet(Collection<Option<S,A>> options) {
			_options = new ArrayList<Option<S,A>>(options);
		}

		@Override
		public List<Integer> validIndices(S state) {
			List<Integer> oinds = new ArrayList<Integer>();
			for (int oi = 0; oi < _options.size(); oi++) {
				if (_options.get(oi).inInitialSet(state)) {
					oinds.add(oi);
				}
			}
			return oinds;
		}

		@Override
		public Option<S,A> action(int index) {
			return _options.get(index);
		}

		@Override
		public int numberOfActions() {
			return _options.size();
		}

		public void set(int optionIndex, Option<S, A> newOption) {
			if (newOption == null) {
				throw new NullPointerException("Cannot set a null option.");
			}
			_options.set(optionIndex, newOption);
		}

	}

	private Task<S, A> _mdpTask;
	private MutableOptionSet _options;

	public OptionTask(Task<S, A> mdpTask, Collection<O> options) {
		_mdpTask = mdpTask;
		_options = new MutableOptionSet(new ArrayList<Option<S,A>>(options));
	}

	/**
	 * Executes an option in this task from the current state until the option
	 * terminates.
	 * 
	 * @param optionIndex
	 *            the index of the option to execute
	 * @param maxDuration
	 *            the maximum allowed duration of the option
	 * @return a result instance specifying the states, actions, and
	 *         reinforcements observed during the options execution
	 * 
	 * @throws IllegalArgumentException
	 *             if the specified option cannot be initialized from the
	 *             current state.
	 */
	public Result execute(int optionIndex, int maxDuration) {
		return execute(optionIndex, maxDuration, null);
	}

	/**
	 * Executes an option in this task from the current state until the option
	 * terminates.
	 * 
	 * @param optionIndex
	 *            the index of the option to execute
	 * @param maxDuration
	 *            the maximum allowed duration of the option
	 * @param frame
	 *            a frame to visualize the execution. If the frame is null then
	 *            no visualization occurs.
	 * @return a result instance specifying the states, actions, and
	 *         reinforcements observed during the options execution
	 * 
	 * @throws IllegalArgumentException
	 *             if the specified option cannot be initialized from the
	 *             current state.
	 */
	private Result execute(int optionIndex, int maxDuration, JFrame frame) {
		List<S> sSeq = new ArrayList<S>();
		List<A> aSeq = new ArrayList<A>();
		List<Double> rSeq = new ArrayList<Double>();

		S state = _mdpTask.getState();
		Option<S,A> option = _options.action(optionIndex);
		if (!option.inInitialSet(state)) {
			throw new IllegalArgumentException(
					"The specified option cannot be executed from the current state.");
		}

		int duration = 0;
		do {
			// Paint the scene if the frame is not null and visible
			if (frame != null && frame.isVisible()) {
				frame.repaint();
				try {
					Thread.sleep(100);
				} catch (InterruptedException ex) { /* Do Nothing */
				}
			}

			state = _mdpTask.getState();
			sSeq.add(state);

			A action = option.policy(state);
			aSeq.add(action);

			_mdpTask.execute(action);
			double r = _mdpTask.evaluate();
			rSeq.add(r);

			duration++;
		} while (!terminateOption(state, option, duration)
				&& duration <= maxDuration);

		sSeq.add(_mdpTask.getState());

		return new Result(sSeq, aSeq, rSeq, duration);
	}

	private boolean terminateOption(S state, Option<S,A> option, int duration) {
		double r = tmrutil.stats.Random.uniform();
		double tprob = option.terminationProb(state, duration);
		return r < tprob;
	}

	public void reset() {
		_mdpTask.reset();
	}

	/**
	 * Runs this task with a {@link Policy}. If the <code>policy</code> argument
	 * is an instance implementing {@link OptionAgent} and
	 * <code>train == true</code>, then this method will call the
	 * {@link OptionAgent#train} and {@link OptionAgent#updateOption} after each
	 * option finishes executing.
	 * 
	 * @param policy
	 *            either a {@link Policy} or {@link OptionAgent}
	 * @param numEpisodes
	 *            the number of episodes to run the policy for
	 * @param maxEpisodeLength
	 *            the maximum number of timesteps that an episode can execute
	 *            for
	 * @param train
	 *            if <code>policy</code> implements {@link OptionAgent}, then
	 *            setting this argument to true will train the policy on the
	 *            trajectory data. Otherwise this parameter has not effect.
	 * @param observers
	 *            a list of observers (used for monitoring what happens during
	 *            execution)
	 * @param frame
	 *            a frame for visualizing the task provided that the underlying
	 *            {@link Task} implements {@link Drawable} (if
	 *            <code>frame == null</code> then the task is not visualized)
	 * @param displayProgress
	 *            if true a progress bar will be displayed indicating the
	 *            percentage of episodes finished running. If false, no progress
	 *            bar is shown.
	 */
	public void run(Policy<S, Integer> policy, int numEpisodes,
			int maxEpisodeLength, boolean train, List<Observer> observers,
			JFrame frame, boolean displayProgress) {

		// Setup the frame
		if (frame != null && _mdpTask instanceof Drawable) {
			DrawableComponent dcomp = new DrawableComponent((Drawable) _mdpTask);
			frame.add(dcomp);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
			if (_mdpTask instanceof KeyListener) {
				frame.addKeyListener((KeyListener) _mdpTask);
			}
			if (_mdpTask instanceof MouseListener) {
				frame.addMouseListener((MouseListener) _mdpTask);
			}
		} else {
			frame = null;
		}

		// Create the progress iterator for displaying progress
		ProgressIterator piter = null;
		if (displayProgress) {
			piter = ProgressIterator.iterator(0, numEpisodes, 1);
		}

		for (int i = 0; (i < numEpisodes) || (numEpisodes < 0); i++) {
			// Reset the task to begin a new episode
			_mdpTask.reset();

			for (Observer obs : observers) {
				obs.observeEpisodeBegin();
			}

			int currentEpisodeLength = 0;
			while (currentEpisodeLength < maxEpisodeLength
					&& !_mdpTask.isFinished()) {
				S state = _mdpTask.getState();
				Integer optionIndex = policy.policy(state);
				Result result = execute(optionIndex, maxEpisodeLength
						- currentEpisodeLength, frame);

				for (Observer obs : observers) {
					obs.observeOption(result);
				}

				if (policy instanceof OptionAgent) {
					if (train) {
						OptionAgent<S, A> oagent = (OptionAgent<S, A>) policy;
						oagent.train(result.stateSequence,
								result.actionSequence, optionIndex,
								result.rSequence, result.duration, _mdpTask.isFinished());

						for (int oi = 0; oi < _options.numberOfActions(); oi++) {
							Option<S,A> option = _options.action(oi);
							Option<S,A> newOption = oagent.updateOption(option, oi);
							_options.set(oi, newOption);
						}
					}
				}
			}

			// Paint the scene if the frame is not null and visible
			if (frame != null && frame.isVisible()) {
				frame.repaint();
			}

			for (Observer obs : observers) {
				obs.observeEpisodeEnd();
			}

			// If the progress is being displated, then update it
			if (displayProgress) {
				piter.next();
			}
		}

		// Hide the progress bar or destroy it
		if (displayProgress) {
			java.awt.Container container = piter.getTopLevelAncestor();
			if (container != null) {
				container.setVisible(false);
				if (container instanceof java.awt.Window) {
					((java.awt.Window) container).dispose();
				}
			}
		}
	}
}
