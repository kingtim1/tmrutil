package tmrutil.learning.rl.smdp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.util.FileDataStore;

/**
 * A class for retrieving samples stored in a data directory. This class assumes
 * that we are working with {@link FactoredState} state-types.
 * 
 * @author Timothy A. Mann
 * 
 * @param <A>
 *            the action type
 */
public class FactoredStateStoredOptionSamples<A> implements
		StoredOptionSamples<FactoredState, A> {

	private String _prefix;
	private FileDataStore _dstore;
	private int[] _maxValues;
	private List<FactoredState> _states;
	private ActionSet<FactoredState, Option<FactoredState, A>> _options;

	public FactoredStateStoredOptionSamples(String prefix, File dataDir,
			ActionSet<FactoredState, Option<FactoredState, A>> options)
			throws IOException {
		_prefix = prefix;
		_dstore = new FileDataStore(dataDir);
		_options = options;
		_maxValues = readMaxValues();
		_states = readStates();
	}

	public FactoredStateStoredOptionSamples(String prefix,
			FileDataStore dstore,
			ActionSet<FactoredState, Option<FactoredState, A>> options)
			throws IOException {
		_prefix = prefix;
		_dstore = dstore;
		_options = options;
		_maxValues = readMaxValues();
		_states = readStates();
	}

	private int[] readMaxValues() throws IOException {
		double[][] dmaxValues = _dstore.getMatrix(maxStateValuesKey(_prefix));
		int[] maxValues = new int[dmaxValues[0].length];
		for (int i = 0; i < dmaxValues[0].length; i++) {
			maxValues[i] = (int) dmaxValues[0][i];
		}
		return maxValues;
	}

	private List<FactoredState> readStates() throws IOException {
		double[][] dstates = _dstore.getMatrix(_prefix + "states");

		List<FactoredState> states = new ArrayList<FactoredState>(
				dstates.length);
		for (int i = 0; i < dstates.length; i++) {
			int[] state = new int[_maxValues.length];
			for (int j = 0; j < dstates[i].length; j++) {
				state[j] = (int) dstates[i][j];
			}
			states.add(new FactoredStateImpl(state, _maxValues));
		}
		return states;
	}

	public FactoredState state(int index) {
		return _states.get(index);
	}

	public Option<FactoredState, A> option(int index) {
		return _options.action(index);
	}

	/**
	 * Returns the number of states that samples were drawn from in this data
	 * set.
	 * 
	 * @return the number of states
	 */
	public int numberOfStates() {
		return _states.size();
	}

	/**
	 * Returns the number of options.
	 * 
	 * @return the number of options
	 */
	public int numberOfOptions() {
		return _options.numberOfActions();
	}

	/**
	 * Retrieves the stored samples associated with a state-option pair
	 * specified by indices.
	 * 
	 * @param stateIndex
	 *            the index of a state
	 * @param optionIndex
	 *            the index of an option
	 * @return a list of option outcomes
	 * @throws IOException
	 *             if an I/O error occurs while retrieving the stored samples
	 */
	public List<OptionOutcome<FactoredState, ?>> samples(int stateIndex,
			int optionIndex) throws IOException {
		double[][] cumR = _dstore.getMatrix(cumRKey(_prefix, stateIndex,
				optionIndex));
		double[][] dCumR = _dstore.getMatrix(dCumRKey(_prefix, stateIndex,
				optionIndex));
		double[][] dduration = _dstore.getMatrix(durationKey(_prefix,
				stateIndex, optionIndex));
		double[][] dtstates = _dstore.getMatrix(terminalStateKey(_prefix,
				stateIndex, optionIndex));

		List<OptionOutcome<FactoredState, ?>> outcomes = new ArrayList<OptionOutcome<FactoredState, ?>>(
				dCumR.length);
		for (int i = 0; i < dCumR.length; i++) {
			int duration = (int) dduration[i][0];

			int[] svalues = new int[dtstates[i].length];
			for (int j = 0; j < dtstates[i].length; j++) {
				svalues[j] = (int) dtstates[i][j];
			}
			FactoredState tstate = new FactoredStateImpl(svalues, _maxValues);
			OptionOutcome<FactoredState, ?> oc = new OptionOutcome<FactoredState, FactoredState>(
					tstate, tstate, dCumR[i][0], cumR[i][0], duration);
			outcomes.add(oc);
		}
		return outcomes;
	}

	public static String maxStateValuesKey(String prefix) {
		return prefix + "max_state_values";
	}

	public static String cumRKey(String prefix, int stateIndex, int optionIndex) {
		return prefix + prefix(stateIndex, optionIndex) + "_cumr";
	}

	public static String dCumRKey(String prefix, int stateIndex, int optionIndex) {
		return prefix + prefix(stateIndex, optionIndex) + "_dcumr";
	}

	public static String durationKey(String prefix, int stateIndex,
			int optionIndex) {
		return prefix + prefix(stateIndex, optionIndex) + "_duration";
	}

	public static String terminalStateKey(String prefix, int stateIndex,
			int optionIndex) {
		return prefix + prefix(stateIndex, optionIndex) + "_terminal_state";
	}

	private static String prefix(int stateIndex, int optionIndex) {
		return "state_" + stateIndex + "_option_" + optionIndex;
	}

	/**
	 * For tasks with a {@link FactoredState} state-space, we can use this
	 * method to generate samples and them record them to the disk rather than
	 * having to store them all in memory.
	 * 
	 * @param dstore
	 *            the data storage instance to store the samples with
	 * @param sim
	 *            the simulator
	 * @param states
	 *            the states to generate samples for
	 * @param options
	 *            the options to generate samples for
	 * @param maxTrajectoryLength
	 *            the maximum trajectory length of an option
	 * @param numSamples
	 *            the number of samples to generate for each state-option pair
	 * @param discountFactor
	 *            the discount factor to use
	 */
	public static <A> void generateAndStoreOptionSamples(String prefix,
			FileDataStore dstore, Simulator<FactoredState, ?, A> sim,
			List<FactoredState> states,
			ActionSet<FactoredState, Option<FactoredState, A>> options,
			int maxTrajectoryLength, int numSamples, double discountFactor) {

		for (int i = 0; i < states.size(); i++) {
			FactoredState state = states.get(i);
			if (i == 0) {
				int[] maxValues = new int[state.size()];
				for (int q = 0; q < state.size(); q++) {
					maxValues[q] = state.choices(q);
				}
				dstore.bind(maxStateValuesKey(prefix), maxValues);
			}
			dstore.appendRow(prefix + "states", state.toArray());

			Collection<Integer> validIndices = options.validIndices(state);
			for (Integer j : validIndices) {
				Option<FactoredState, A> option = options.action(j);
				if (option.inInitialSet(state)) {
					for (int s = 0; s < numSamples; s++) {
						OptionOutcome<FactoredState, ?> oc = sim.sample(state,
								option, discountFactor, maxTrajectoryLength);

						dstore.appendRow(cumRKey(prefix, i, j),
								new double[] { oc.cumR() });
						dstore.appendRow(dCumRKey(prefix, i, j),
								new double[] { oc.dCumR() });
						dstore.appendRow(durationKey(prefix, i, j),
								new double[] { oc.duration() });
						dstore.appendRow(terminalStateKey(prefix, i, j), oc
								.terminalState().toArray());
					}
				}
			}
		}
	}

	@Override
	public Collection<Integer> validOptions(int stateIndex) {
		return _options.validIndices(_states.get(stateIndex));
	}

	@Override
	public ActionSet<FactoredState, ? extends Option<FactoredState, A>> optionSet() {
		return new ListBackedOptionSet<FactoredState, A>(_options);
	}

}
