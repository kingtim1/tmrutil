package tmrutil.learning.rl.smdp;

import tmrutil.learning.rl.Policy;

/**
 * An option is a generalization of actions to a temporally extended action, a
 * macro-action, or skill.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S> the state type
 * @param <A> the primitive action type
 */
public interface Option<S, A> extends Policy<S, A> {
	
	
	/**
	 * Returns the termination probability associated with a given state.
	 * 
	 * @param state
	 *            a state
	 * @param duration the number of timesteps that this option has been executing for
	 * @return the termination probability associated with <code>state</code>
	 */
	public double terminationProb(S state, int duration);

	/**
	 * Returns true if the given state is in a state where this option can be
	 * initialized from, or false otherwise.
	 * 
	 * @param state
	 *            a state
	 * @return true if <code>state</code> is in this option's initial state set;
	 *         otherwise false
	 */
	public boolean inInitialSet(S state);
}
