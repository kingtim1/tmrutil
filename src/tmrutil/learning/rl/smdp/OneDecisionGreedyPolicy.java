package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.optim.Optimization;

/**
 * This greedy policy only looks ahead a single option into the future. This
 * policy is mostly used for comparison purposes. We would like to ensure that
 * our derived policies are in fact better than the 1-decision greedy policy.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S> the state type
 * @param <A> the primitive action type
 */
public class OneDecisionGreedyPolicy<S, A> implements Policy<S, Option<S,A>> {

	private Simulator<S, ?, A> _sim;
	private List<Option<S, A>> _options;
	private double _discountFactor;
	private int _maxTrajectoryLength;
	private int _numSamples;
	private Optimization _opType;

	public OneDecisionGreedyPolicy(Simulator<S, ?, A> sim,
			Collection<Option<S, A>> options, double discountFactor, int maxTrajectoryLength, int numSamples,
			Optimization opType) {
		_sim = sim;
		_options = new ArrayList<Option<S, A>>(options);
		_discountFactor = discountFactor;
		_maxTrajectoryLength = maxTrajectoryLength;
		_numSamples = numSamples;
		_opType = opType;
	}

	public boolean isMaximizing() {
		return _opType.equals(Optimization.MAXIMIZE);
	}

	public boolean isMinimizing() {
		return !isMaximizing();
	}
	
	private double reinforcement(S state, Option<S,A> option)
	{
		double r = 0;
		for(int i=0;i<_numSamples;i++){
			OptionOutcome<S,?> oc = _sim.sample(state, option, _discountFactor, _maxTrajectoryLength);
			r += oc.dCumR();
		}
		
		return (r / _numSamples);
	}
	
	private boolean isBetterR(double r, double bestRSoFar)
	{
		if(isMaximizing()){
			return (r > bestRSoFar);
		}else{
			return (r < bestRSoFar);
		}
	}

	@Override
	public Option<S,A> policy(S state) {
		Double bestR = null;
		Option<S,A> bestO = null;
		
		for(Option<S,A> option : _options){
			double r = reinforcement(state, option);
			if(bestR == null || isBetterR(r, bestR)){
				bestR = r;
				bestO = option;
			}
		}
		
		return bestO;
	}

}
