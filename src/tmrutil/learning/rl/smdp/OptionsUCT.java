package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.Aggregator;
import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.optim.Optimization;
import tmrutil.util.Pair;

/**
 * An implementation of the UCT algorithm with options.
 * 
 * @author Timothy A. Mann
 * 
 */
public class OptionsUCT<S, A> implements Policy<S, Option<S, A>> {

	private class DepthNode {
		private int _totalVisits;
		private Map<Pair<Integer, Option<S, A>>, Double> _cumVals;
		private Map<Integer, Integer> _sCounts;
		private Map<Pair<Integer, Option<S, A>>, Integer> _saCounts;

		public DepthNode() {
			_totalVisits = 0;
			_cumVals = new HashMap<Pair<Integer, Option<S, A>>, Double>();
			_sCounts = new HashMap<Integer, Integer>();
			_saCounts = new HashMap<Pair<Integer, Option<S, A>>, Integer>();
		}

		public int totalVisits() {
			return _totalVisits;
		}

		public int visitCount(S state) {
			Integer visitCount = _sCounts.get(state);
			if (visitCount == null) {
				return 0;
			} else {
				return visitCount.intValue();
			}
		}

		public int visitCount(S state, Option<S, A> action) {
			Pair<S, Option<S, A>> key = new Pair<S, Option<S, A>>(state, action);
			Integer visitCount = _saCounts.get(key);
			if (visitCount == null) {
				return 0;
			} else {
				return visitCount.intValue();
			}
		}

		public double reward(S state, Option<S, A> action) {
			Pair<S, Option<S, A>> key = new Pair<S, Option<S, A>>(state, action);
			int visitCount = visitCount(state, action);
			if (visitCount == 0) {
				return 0.0;
			} else {
				Double cumVal = _cumVals.get(key);
				return cumVal.doubleValue() / visitCount;
			}
		}

		public void update(S state, Option<S, A> action, double value) {
			_totalVisits++;

			Integer sclass = _aggregator.aggregate(state);
			Integer sCount = _sCounts.get(state);
			if (sCount == null) {
				_sCounts.put(sclass, new Integer(1));
			} else {
				_sCounts.put(sclass, new Integer(sCount.intValue() + 1));
			}

			Pair<Integer, Option<S, A>> key = new Pair<Integer, Option<S, A>>(
					sclass, action);
			Integer visitCount = _saCounts.get(key);
			if (visitCount == null) {
				_saCounts.put(key, new Integer(1));
			} else {
				_saCounts.put(key, new Integer(visitCount.intValue() + 1));
			}

			Double cumVal = _cumVals.get(key);
			if (cumVal == null) {
				_cumVals.put(key, new Double(value));
			} else {
				_cumVals.put(key, new Double(cumVal.doubleValue() + value));
			}
		}
	}

	private Simulator<S, ?, A> _sim;
	private ActionSet<S, Option<S,A>> _options;
	private int _maxDepth;
	private int _numRollouts;
	private double _discountFactor;
	private int _maxTrajectoryLength;
	private Optimization _opType;
	private double _alpha;
	private Aggregator<S> _aggregator;

	private List<DepthNode> _tree;

	public OptionsUCT(Simulator<S, ?, A> sim, ActionSet<S, Option<S,A>> options,
			int maxDepth, int numRollouts, double discountFactor,
			int maxTrajectoryLength, Optimization opType, double alpha,
			Aggregator<S> aggregator) {
		_sim = sim;
		_options = options;
		maxDepth(maxDepth);
		numRollouts(numRollouts);
		discountFactor(discountFactor);
		maxTrajectoryLength(maxTrajectoryLength);
		_opType = opType;

		_alpha = alpha;
		_aggregator = aggregator;

		initTree();
	}

	public double discountFactor() {
		return _discountFactor;
	}

	private void discountFactor(double df) {
		if (df < 0 || df > 1) {
			throw new IllegalArgumentException(
					"Discount factor must be in the interval [0, 1].");
		}
		_discountFactor = df;
	}

	public int maxTrajectoryLength() {
		return _maxTrajectoryLength;
	}

	public void maxTrajectoryLength(int mtlength) {
		if (mtlength < 1) {
			throw new IllegalArgumentException(
					"Maximum trajectory length of options must be positive.");
		}
		_maxTrajectoryLength = mtlength;
	}

	public int numRollouts() {
		return _numRollouts;
	}

	public void numRollouts(int n) {
		if (n < 1) {
			throw new IllegalArgumentException(
					"The number of rollouts must be a positive integer.");
		}
		_numRollouts = n;
	}

	public int maxDepth() {
		return _maxDepth;
	}

	private void maxDepth(int d) {
		if (d < 1) {
			throw new IllegalArgumentException(
					"Maximum depth must be a positive integer.");
		}
		_maxDepth = d;
	}

	public boolean isMinimizing() {
		return !isMaximizing();
	}

	public boolean isMaximizing() {
		return _opType.equals(Optimization.MAXIMIZE);
	}

	public Option<S, A> policy(S state) {
		for (int r = 0; r < numRollouts() - 1; r++) {
			search(state, 1);
		}
		Pair<Option<S, A>, Double> p = search(state, 1);
		return p.getA();
	}

	public Pair<Option<S, A>, Double> search(S state, int d) {
			DepthNode dnode = _tree.get(d - 1);
			Option<S, A> option = selectOption(state, d);
			if (option == null) {
				return new Pair<Option<S, A>, Double>(null, 0.0);
			}
			OptionOutcome<S, ?> oc = _sim.sample(state, option,
					discountFactor(), maxTrajectoryLength());
			double r = oc.dCumR();
			int lt = oc.duration();
			S tstate = oc.terminalState();

			double q = r;
			if (d < maxDepth()) {
				Pair<Option<S, A>, Double> pv = search(tstate, d + 1);
				q = r + Math.pow(discountFactor(), lt) * pv.getB();
			}
			dnode.update(state, option, q);

			return new Pair<Option<S, A>, Double>(option, q);
	}

	private Option<S, A> selectOption(S state, int depth) {
		Option<S, A> bestO = null;
		Double bestS = null;
		List<Option<S, A>> options = _options.validList(state);
		for (Option<S, A> option : options) {
			double score = score(state, option, depth);
			double bonus = ucbBonus(state, option, depth);
			if (bestS == null || (isMaximizing() && bestS < score + bonus)
					|| (isMinimizing() && bestS > score - bonus)) {
				bestO = option;
				bestS = score;
			}
		}
		return bestO;
	}

	private double score(S state, Option<S, A> option, int depth) {
		DepthNode dnode = _tree.get(depth - 1);
		double reward = dnode.reward(state, option);
		return reward;
	}

	private double ucbBonus(S state, Option<S, A> option, int depth) {
		DepthNode dnode = _tree.get(depth - 1);
		int ns = dnode.visitCount(state);
		int nsa = dnode.visitCount(state, option);
		if (nsa == 0) {
			return Double.POSITIVE_INFINITY;
		}

		double bonus = _alpha * Math.sqrt(2 * Math.log(ns) / nsa);
		return bonus;
	}

	public void reset() {
		initTree();
	}

	private void initTree() {
		_tree = new ArrayList<DepthNode>(maxDepth());
		for (int d = 0; d < maxDepth(); d++) {
			_tree.add(new DepthNode());
		}
	}
}