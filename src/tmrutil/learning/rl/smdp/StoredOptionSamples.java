package tmrutil.learning.rl.smdp;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.rl.ActionSet;

/**
 * An interface for retrieving samples stored on hard disk.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public interface StoredOptionSamples<S, A> {
	
	/**
	 * Retrieves a state by its index.
	 * @param index the index of a state
	 * @return a state
	 */
	public S state(int index);
	
	/**
	 * Returns the number of states where samples have been generated.
	 * @return the number of state
	 */
	public int numberOfStates();
	
	/**
	 * Retrieves an option by its index.
	 * @param index the index of an option
	 * @return an option
	 */
	public Option<S,A> option(int index);
	
	/**
	 * Returns the number of options used to generate samples.
	 * @return the number of options
	 */
	public int numberOfOptions();
	
	/**
	 * Retrieves the collection of valid options given a specific state's index.
	 * @param stateIndex the index of a state
	 * @return a collection of option indices
	 */
	public Collection<Integer> validOptions(int stateIndex);
	
	/**
	 * Returns the {@link ActionSet} containing the options.
	 * @return the option set
	 */
	public ActionSet<S,? extends Option<S,A>> optionSet();
	
	/**
	 * Retrieves the options associated with a state-option pair via state and option indices.
	 * @param stateIndex a state index
	 * @param optionIndex an option index
	 * @return a list of option outcomes for the specified state-option pair
	 * @throws IOException if an I/O error occurs while retrieving the requested samples
	 */
	public List<OptionOutcome<S, ?>> samples(int stateIndex,
			int optionIndex) throws IOException;
}
