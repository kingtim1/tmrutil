package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import tmrutil.learning.BatchFunctionApproximator;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.optim.Optimization;
import tmrutil.util.Factory;
import tmrutil.util.Pair;

/**
 * A simple implementation of a fitted Q-function.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class FittedQFunction<S, A> implements QFunction<S, A> {

	private Factory<? extends BatchFunctionApproximator<S, Double>> _factory;

	private Map<A, BatchFunctionApproximator<S, Double>> _qfuncs;
	private Set<A> _actions;
	private double _defaultValue;
	private Optimization _opType;

	public FittedQFunction(
			Factory<? extends BatchFunctionApproximator<S, Double>> factory,
			double defaultValue, Optimization opType) {
		_factory = factory;
		_defaultValue = defaultValue;
		_opType = opType;

		_qfuncs = new HashMap<A, BatchFunctionApproximator<S, Double>>();
		_actions = new HashSet<A>();
	}

	@Override
	public double greedyValue(S state) {
		Double best = null;
		for (A action : _actions) {
			Double av = value(state, action);
			if (isMaximizing()) {
				if (best == null || best < av) {
					best = av;
				}
			} else {
				if (best == null || best > av) {
					best = av;
				}
			}
		}
		return best;
	}

	@Override
	public double value(S state, A action) {
		BatchFunctionApproximator<S, Double> qfunc = _qfuncs.get(action);
		if (qfunc != null) {
			return qfunc.evaluate(state);
		} else {
			return _defaultValue;
		}
	}

	public void train(Collection<Pair<Pair<S, A>, Double>> samples) {
		_actions.clear();
		_qfuncs.clear();

		Map<A, Collection<Pair<S, Double>>> tsamples = new HashMap<A, Collection<Pair<S, Double>>>();
		for (Pair<Pair<S, A>, Double> sample : samples) {
			Pair<S, A> key = sample.getA();
			S state = key.getA();
			A action = key.getB();
			Double val = sample.getB();

			_actions.add(action);
			Collection<Pair<S, Double>> atsamples = tsamples.get(action);
			if (atsamples == null) {
				atsamples = new ArrayList<Pair<S, Double>>();
				tsamples.put(action, atsamples);
			}
			atsamples.add(new Pair<S, Double>(state, val));
		}

		for (A action : _actions) {
			BatchFunctionApproximator<S, Double> qfunc = _factory.newInstance();
			Collection<Pair<S,Double>> atsamples2 = tsamples.get(action);
			qfunc.train(atsamples2);
			_qfuncs.put(action, qfunc);
		}
	}

	@Override
	public boolean isMaximizing() {
		return _opType.equals(Optimization.MAXIMIZE);
	}

	@Override
	public boolean isMinimizing() {
		return !isMaximizing();
	}

	@Override
	public VFunction<S> greedyValueFunction() {
		return new tmrutil.learning.rl.QFunction.GreedyVFunction<S>(this);
	}
}
