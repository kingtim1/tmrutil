package tmrutil.learning.rl.smdp;

import java.util.List;

import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.RLInstance;
import tmrutil.optim.Optimization;

/**
 * An option agent is a reinforcement learning agent that learns a policy over
 * options rather than primitive actions.
 * 
 * @author Timothy A. Mann
 *
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public abstract class OptionAgent<S, A> implements Policy<S, Integer> {

	private double _discountFactor;
	private Optimization _opType;

	public OptionAgent(double discountFactor, Optimization opType) {

		setDiscountFactor(discountFactor);
		if (opType == null) {
			throw new NullPointerException(
					"Optimization type must be MINIMIZE or MAXIMIZE. Found null.");
		}
		_opType = opType;
	}

	/**
	 * Trains this option learning system.
	 * 
	 * @param stateSequence
	 *            the sequence of states encountered during the execution of an
	 *            option (including the initial state and the terminal state)
	 * @param primitiveActionSequence
	 *            the sequence of primitive actions selected by the option
	 *            policy during execution of an option
	 * @param optionIndex
	 *            the index of the option selected by this agent from the
	 *            initial state in <code>stateSequence</code>
	 * @param rSequence
	 *            the sequence of reinforcements (rewards or costs) received
	 *            during the execution of an option
	 * @param duration
	 *            the number of actions taken during the executing of the option
	 * 
	 * @throws IllegalArgumentException
	 *             if <code>stateSequence.size() != duration + 1</code>,
	 *             <code>actionSequence.size() != duration</code> or
	 *             <code>rSequence.size() != duration</code>
	 */
	public void train(List<S> stateSequence,
			List<A> primitiveActionSequence, int optionIndex,
			List<Double> rSequence, int duration, boolean terminal){
		S state = stateSequence.get(0);
		S tstate = stateSequence.get(stateSequence.size()-1);
		double r = dCumR(rSequence);
		RLInstance<S,Integer> instance = new RLInstance<S,Integer>(state, optionIndex, tstate, r, duration, terminal);
		train(instance);
	}
	
	/**
	 * Trains this option learning system.
	 * 
	 * @param instance an RL instance
	 * 
	 * @throws IllegalArgumentException
	 *             if the instance is invalid
	 */
	public abstract void train(RLInstance<S,Integer> instance);

	/**
	 * Modifies the option with the specified option index using this agent's
	 * experience. This abstract method may be useful for implementing option
	 * interruption, etc.
	 * 
	 * @param option
	 *            an option
	 * @param optionIndex
	 *            the index of the option passed as the first argument
	 * @return a modified option that will replace <code>option</code>
	 */
	public abstract Option<S,A> updateOption(Option<S,A> option,
			int optionIndex);

	/**
	 * Returns the discount factor used by this agent. The discount factor is a
	 * scalar value in the interval [0, 1].
	 * 
	 * @return a scalar value in [0, 1]
	 */
	public final double discountFactor() {
		return _discountFactor;
	}

	/**
	 * Sets the discount factor used by this agent.
	 * 
	 * @param gamma
	 *            a scalar value in [0, 1]
	 */
	private final void setDiscountFactor(double gamma) {
		if (gamma < 0 || gamma > 1) {
			throw new IllegalArgumentException(
					"Discount factor must be in [0, 1]. Received " + gamma
							+ ".");
		}
		_discountFactor = gamma;
	}

	/**
	 * Returns the optimization type of this agent (either MINIMIZE or
	 * MAXIMIZE).
	 * 
	 * @return the optimization type
	 */
	public final Optimization opType() {
		return _opType;
	}

	/**
	 * Computes the discounted cumulative reinforcement of a sequence of
	 * reinforcements.
	 * 
	 * @param rSequence
	 *            a sequence of reinforcements
	 * @return the discounted cumulative reinforcement
	 */
	public final double dCumR(List<Double> rSequence) {
		double dCumR = 0;
		for (int i = rSequence.size() - 1; i >= 0; i--) {
			dCumR = rSequence.get(i) + discountFactor() * dCumR;
		}
		return dCumR;
	}

	/**
	 * Computes the undiscounted cumulative reinforcement of a sequence of
	 * reinforcements.
	 * 
	 * @param rSequence
	 *            a sequence of reinforcements
	 * @return the undiscounted cumulative reinforcement
	 */
	public final double cumR(List<Double> rSequence) {
		double cumR = 0;
		for (Double r : rSequence) {
			cumR += r;
		}
		return cumR;
	}
}
