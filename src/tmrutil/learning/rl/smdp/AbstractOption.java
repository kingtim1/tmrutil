package tmrutil.learning.rl.smdp;

/**
 * An abstract implementation of an option. Most implementations of options should extend this class.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public abstract class AbstractOption<S, A> implements Option<S, A>, Comparable<AbstractOption<S,A>> {

	private int _index;
	
	public AbstractOption(int index) {
		if(index < 0){
			throw new IllegalArgumentException("Indices must be non-negative. Received : " + index);
		}
		_index = index;
	}

	public int index() {
		return _index;
	}

	@Override
	public boolean equals(Object obj){
		if(obj instanceof AbstractOption){
			return index() == ((AbstractOption)obj).index();
		}else{
			return false;
		}
	}

	@Override
	public int compareTo(AbstractOption<S, A> o) {
		Integer index = new Integer(this.index());
		Integer oindex = new Integer(o.index());
		return index.compareTo(oindex);
	}

}
