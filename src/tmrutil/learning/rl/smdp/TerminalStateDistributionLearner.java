package tmrutil.learning.rl.smdp;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.util.Pair;

/**
 * A terminal state distribution learner tries to learn a generative model for
 * an option conditioned on a state.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state space
 */
public abstract class TerminalStateDistributionLearner<S> {

	/**
	 * Trains this learner based on a collection of samples obtained by
	 * executing the state-option pair.
	 * 
	 * @param initStates
	 *            a list of initial states
	 * @param observations
	 *            a list containing pairs of terminal states and duration times
	 */
	public abstract void train(List<S> initStates,
			List<? extends Collection<Pair<S, Integer>>> observations);

	/**
	 * Samples a pair containing a terminal state and duration time given an
	 * initial state.
	 * 
	 * @param initState
	 *            the initial state where the option was executed
	 * @return a pair containing a terminal state and duration time
	 */
	public abstract Pair<S, Integer> sample(S initState);

	/**
	 * A non-negative error score determining the error between this
	 * distribution and samples collected from the target distribution. A large
	 * error score means that this estimate poorly explains the target
	 * distribution.
	 * 
	 * @param initStates
	 *            a list of initial states
	 * @param observations
	 *            a collection of samples from the target distribution
	 * @return a non-negative error score
	 */
	public abstract double error(List<S> initStates,
			List<? extends Collection<Pair<S, Integer>>> observations);
}
