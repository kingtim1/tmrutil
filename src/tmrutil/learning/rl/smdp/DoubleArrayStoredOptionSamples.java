package tmrutil.learning.rl.smdp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.util.FileDataStore;

/**
 * A class for retrieving samples stored in a data directory. This class assumes
 * that we are working with <code>double[]</code> state-types.
 * 
 * @author Timothy A. Mann
 * 
 * @param <A>
 *            the action type
 */
public class DoubleArrayStoredOptionSamples<A> implements
		StoredOptionSamples<double[], A> {

	private FileDataStore _dstore;
	private List<double[]> _states;
	private List<Option<double[], A>> _options;
	private List<Collection<Integer>> _validOptions;

	public DoubleArrayStoredOptionSamples(File dataDir,
			List<Option<double[], A>> options) throws IOException {
		_dstore = new FileDataStore(dataDir);
		_options = options;
		_states = readStates();

		_validOptions = new ArrayList<Collection<Integer>>(_states.size());
		for (int i = 0; i < _states.size(); i++) {
			double[] state = _states.get(i);
			Set<Integer> validOptions = new HashSet<Integer>();
			for (int j = 0; j < _options.size(); j++) {
				Option<double[], A> option = _options.get(j);
				if (option.inInitialSet(state)) {
					validOptions.add(j);
				}
			}
			_validOptions.add(validOptions);
		}
	}

	private List<double[]> readStates() throws IOException {
		double[][] dstates = _dstore.getMatrix("states");

		List<double[]> states = new ArrayList<double[]>(dstates.length);
		for (int i = 0; i < dstates.length; i++) {
			double[] state = new double[dstates[i].length];
			for (int j = 0; j < dstates[i].length; j++) {
				state[j] = dstates[i][j];
			}
			states.add(state);
		}
		return states;
	}

	@Override
	public double[] state(int index) {
		return _states.get(index);
	}

	@Override
	public int numberOfStates() {
		return _states.size();
	}

	@Override
	public Option<double[], A> option(int index) {
		return _options.get(index);
	}

	@Override
	public int numberOfOptions() {
		return _options.size();
	}

	@Override
	public Collection<Integer> validOptions(int stateIndex) {
		return _validOptions.get(stateIndex);
	}

	@Override
	public List<OptionOutcome<double[], ?>> samples(int stateIndex,
			int optionIndex) throws IOException {
		double[][] cumR = _dstore.getMatrix(cumRKey(stateIndex, optionIndex));
		double[][] dCumR = _dstore
				.getMatrix(dCumRKey(stateIndex, optionIndex));
		double[][] dduration = _dstore.getMatrix(durationKey(stateIndex,
				optionIndex));
		double[][] dtstates = _dstore.getMatrix(terminalStateKey(stateIndex,
				optionIndex));

		List<OptionOutcome<double[], ?>> outcomes = new ArrayList<OptionOutcome<double[], ?>>(
				dCumR.length);
		for (int i = 0; i < dCumR.length; i++) {
			int duration = (int) dduration[i][0];

			double[] tstate = new double[dtstates[i].length];
			for (int j = 0; j < dtstates[i].length; j++) {
				tstate[j] = dtstates[i][j];
			}

			OptionOutcome<double[], ?> oc = new OptionOutcome<double[], double[]>(
					tstate, tstate, dCumR[i][0], cumR[i][0], duration);
			outcomes.add(oc);
		}
		return outcomes;
	}

	public static String cumRKey(int stateIndex, int optionIndex) {
		return prefix(stateIndex, optionIndex) + "_cumr";
	}

	public static String dCumRKey(int stateIndex, int optionIndex) {
		return prefix(stateIndex, optionIndex) + "_dcumr";
	}

	public static String durationKey(int stateIndex, int optionIndex) {
		return prefix(stateIndex, optionIndex) + "_duration";
	}

	public static String terminalStateKey(int stateIndex, int optionIndex) {
		return prefix(stateIndex, optionIndex) + "_terminal_state";
	}

	private static String prefix(int stateIndex, int optionIndex) {
		return "state_" + stateIndex + "_option_" + optionIndex;
	}

	/**
	 * For tasks with a <code>double[]</code> state-space, we can use this
	 * method to generate samples and them record them to the disk rather than
	 * having to store them all in memory.
	 * 
	 * @param dstore
	 *            the data storage instance to store the samples with
	 * @param sim
	 *            the simulator
	 * @param states
	 *            the states to generate samples for
	 * @param options
	 *            the options to generate samples for
	 * @param maxTrajectoryLength
	 *            the maximum trajectory length of an option
	 * @param numSamples
	 *            the number of samples to generate for each state-option pair
	 * @param discountFactor
	 *            the discount factor to use
	 */
	public static <A> void generateAndStoreOptionSamples(FileDataStore dstore,
			Simulator<double[], ?, A> sim, List<double[]> states,
			List<Option<double[], A>> options, int maxTrajectoryLength,
			int numSamples, double discountFactor) {

		for (int i = 0; i < states.size(); i++) {
			double[] state = states.get(i);

			dstore.appendRow("states", state);
			for (int j = 0; j < options.size(); j++) {
				Option<double[], A> option = options.get(j);
				if (option.inInitialSet(state)) {
					for (int s = 0; s < numSamples; s++) {
						OptionOutcome<double[], ?> oc = sim.sample(state,
								option, discountFactor, maxTrajectoryLength);

						dstore.appendRow(cumRKey(i, j),
								new double[] { oc.cumR() });
						dstore.appendRow(dCumRKey(i, j),
								new double[] { oc.dCumR() });
						dstore.appendRow(durationKey(i, j),
								new double[] { oc.duration() });
						dstore.appendRow(terminalStateKey(i, j),
								oc.terminalState());
					}
				}
			}
		}
	}

	@Override
	public ActionSet<double[], ? extends Option<double[], A>> optionSet() {
		return new ListBackedOptionSet<double[],A>(_options);
	}

}
