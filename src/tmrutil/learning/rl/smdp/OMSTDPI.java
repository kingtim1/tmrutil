package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.learning.BatchFunctionApproximator;
import tmrutil.learning.BatchLinearApproximator;
import tmrutil.learning.CrossValidation;
import tmrutil.learning.CrossValidation.CrossValidationResult;
import tmrutil.learning.LARS;
import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.RLInstance;
import tmrutil.learning.rl.VFunction;
import tmrutil.math.Metric;
import tmrutil.optim.ObjectiveFunction;
import tmrutil.optim.ObjectiveFunction.Loss;
import tmrutil.optim.ObjectiveFunction.Penalty;
import tmrutil.optim.Optimization;
import tmrutil.optim.SGD;
import tmrutil.stats.Statistics;
import tmrutil.util.Pair;

/**
 * Options Model Selection Temporal Difference + Policy Improvements (OMSTDPI)
 * algorithm. This algorithm goes through a sequence of iterations. It first
 * uses the temporal difference learning rule to evaluate the current policy
 * using a model selection procedure. Then the estimated value is used to
 * perform a greedy policy improvement step. These two steps are repeated.
 * 
 * @author Timothy A. Mann
 *
 */
public class OMSTDPI extends OptionAgent<double[], Integer> {

	public static class MSTDPIParams {
		public int numPolicyEvaluationSamples = 5000;
		public int minOptionSamples = 1;

		public int maxOptionDuration = 40;

		public double trainRatio = 0.75;
		public int numTests = 1;

		public double[] learningRates = { 0.01, 0.1 };
		public int numIterations = 20;
		public int numBatchIterations = 100;
		public double[] regularizers = { 0.0, 0.1, 0.3, 1.0, 3.0 };
	}

	public class FAPolicy implements Policy<double[], Integer> {

		private Random _rand = new Random();
		private List<BatchFunctionApproximator<RealVector, Double>> _qfuncs;
		private List<Boolean> _canUse;
		private List<Double> _cvScores;

		public FAPolicy(
				List<BatchFunctionApproximator<RealVector, Double>> qfuncs,
				List<Boolean> canUse, List<Double> cvScores) {
			_qfuncs = new ArrayList<BatchFunctionApproximator<RealVector, Double>>(
					qfuncs);
			_canUse = canUse;
			_cvScores = cvScores;
		}

		@Override
		public Integer policy(double[] state) {
			if (state == null) {
				return _rand.nextInt(_qfuncs.size());
			} else {
				RealVector vx = new ArrayRealVector(state);
				double[] qvals = new double[_qfuncs.size()];
				for (int a = 0; a < _qfuncs.size(); a++) {
					if (_canUse.get(a)) {
						//double score = _cvScores.get(a);
						qvals[a] = _qfuncs.get(a).evaluate(vx);// - score;
					} else {
						if (opType().equals(Optimization.MAXIMIZE)) {
							qvals[a] = Double.NEGATIVE_INFINITY;
						} else {
							qvals[a] = Double.POSITIVE_INFINITY;
						}
					}
				}

				if (opType().equals(Optimization.MAXIMIZE)) {
					return Statistics.maxIndex(qvals);
				} else {
					return Statistics.minIndex(qvals);
				}
			}
		}

	}

	public class FAVFunc implements VFunction<double[]> {

		private List<BatchFunctionApproximator<RealVector, Double>> _qfuncs;
		private List<Boolean> _canUse;

		public FAVFunc(
				List<BatchFunctionApproximator<RealVector, Double>> qfuncs,
				List<Boolean> canUse) {
			_qfuncs = new ArrayList<BatchFunctionApproximator<RealVector, Double>>(
					qfuncs);
			_canUse = canUse;
		}

		@Override
		public double value(double[] state) {
			RealVector vx = new ArrayRealVector(state);
			double[] qvals = new double[_qfuncs.size()];
			for (int a = 0; a < _qfuncs.size(); a++) {
				if (_canUse.get(a)) {
					qvals[a] = _qfuncs.get(a).evaluate(vx);
				} else {
					if (opType().equals(Optimization.MAXIMIZE)) {
						qvals[a] = Double.NEGATIVE_INFINITY;
					} else {
						qvals[a] = Double.POSITIVE_INFINITY;
					}
				}
			}

			if (opType().equals(Optimization.MAXIMIZE)) {
				return Statistics.max(qvals);
			} else {
				return Statistics.min(qvals);
			}
		}

	}
	
	public class FAQFunc implements QFunction<double[],Integer> {

		private List<BatchFunctionApproximator<RealVector, Double>> _qfuncs;
		private List<Boolean> _canUse;

		public FAQFunc(
				List<BatchFunctionApproximator<RealVector, Double>> qfuncs,
				List<Boolean> canUse) {
			_qfuncs = new ArrayList<BatchFunctionApproximator<RealVector, Double>>(
					qfuncs);
			_canUse = canUse;
		}

		@Override
		public VFunction<double[]> greedyValueFunction() {
			return new QFunction.GreedyVFunction<double[]>(this);
		}

		@Override
		public double value(double[] state, Integer action) {
			RealVector vx = new ArrayRealVector(state);
			double qval = 0;
			if (_canUse.get(action)) {
				qval = _qfuncs.get(action).evaluate(vx);
			} else {
				if (opType().equals(Optimization.MAXIMIZE)) {
					qval = Double.NEGATIVE_INFINITY;
				} else {
					qval = Double.POSITIVE_INFINITY;
				}
			}
			return qval;
		}

		@Override
		public double greedyValue(double[] state) {
			double[] qvals = new double[_qfuncs.size()];
			for (int a = 0; a < _qfuncs.size(); a++) {
				qvals[a] = value(state, a);
			}

			if (opType().equals(Optimization.MAXIMIZE)) {
				return Statistics.max(qvals);
			} else {
				return Statistics.min(qvals);
			}
		}

		@Override
		public boolean isMaximizing() {
			return opType().equals(Optimization.MAXIMIZE);
		}

		@Override
		public boolean isMinimizing() {
			return !isMaximizing();
		}

	}

	public class RandPolicy implements Policy<double[], Integer> {
		private Random _rand = new Random();

		@Override
		public Integer policy(double[] state) {
			return _rand.nextInt(_numOptions);
		}
	}

	/**
	 * Squared difference between two scalar values.
	 * 
	 * @author Timothy A. Mann
	 *
	 */
	public static class SquaredError implements Metric<Double> {

		@Override
		public double distance(Double x, Double y) {
			double diff = x - y;
			return diff * diff;
		}

	}

	public static class DummyVFunction implements VFunction<double[]> {

		@Override
		public double value(double[] state) {
			return 0;
		}

	}
	
	public class DummyQFunction implements QFunction<double[],Integer>{

		@Override
		public VFunction<double[]> greedyValueFunction() {
			return new DummyVFunction();
		}

		@Override
		public double value(double[] state, Integer action) {
			return 0;
		}

		@Override
		public double greedyValue(double[] state) {
			return 0;
		}

		@Override
		public boolean isMaximizing() {
			return opType().equals(Optimization.MAXIMIZE);
		}

		@Override
		public boolean isMinimizing() {
			return !isMaximizing();
		}
		
	}

	private int _stateDims;
	private int _numOptions;
	private MSTDPIParams _params;
	private double _epsilonGreedy;

	private Policy<double[], Integer> _opolicy;
	private List<List<RLInstance<double[], Integer>>> _osamples;
	private int _numSamples;
	
	private QFunction<double[],Integer> _qfunc;

	public OMSTDPI(int stateDimensions, int numActions, MSTDPIParams params,
			double discountFactor, double epsilonGreedy, Optimization opType) {
		super(discountFactor, opType);
		if (stateDimensions < 1) {
			throw new IllegalArgumentException(
					"Dimension of the state-space must be positive.");
		}
		_stateDims = stateDimensions;
		if (numActions < 1) {
			throw new IllegalArgumentException(
					"There must be at least 1 action.");
		}
		_numOptions = numActions;
		_qfunc = new DummyQFunction();

		if (params == null) {
			throw new NullPointerException(
					"Cannot use null MSTDPIParams instance to initialize MSTDPI.");
		}
		_params = params;

		setEpsilonGreedy(epsilonGreedy);

		reset();
	}

	public void reset() {
		// Initialize to a random policy
		_opolicy = new RandPolicy();
		_osamples = new ArrayList<List<RLInstance<double[], Integer>>>(
				_numOptions);
		for (int a = 0; a < _numOptions; a++) {
			_osamples.add(new ArrayList<RLInstance<double[], Integer>>());
		}
	}

	public int numberOfStateDimensions() {
		return _stateDims;
	}

	public int numberOfOptions() {
		return _numOptions;
	}

	public void setEpsilonGreedy(double epsilon) {
		if (epsilon < 0 || epsilon > 1) {
			throw new IllegalArgumentException(
					"Epsilon greedy parameter represents a probability. Expected value in [0, 1]. Received "
							+ epsilon + ".");
		}
		_epsilonGreedy = epsilon;
	}

	public double epsilonGreedy() {
		return _epsilonGreedy;
	}

	@Override
	public Integer policy(double[] state) {
		double r = tmrutil.stats.Random.uniform();
		if (r < epsilonGreedy()) {
			return tmrutil.stats.Random.nextInt(_numOptions);
		} else {
			Integer optionIndex = _opolicy.policy(state);
			return optionIndex;
		}
	}

	private CrossValidationResult<RealVector, Double> estimateQ(Integer action,
			VFunction<double[]> vfunc) {
		List<RLInstance<double[], Integer>> samples = _osamples.get(action);
		List<Pair<RealVector, Double>> vsamples = new ArrayList<Pair<RealVector, Double>>(
				samples.size());
		for (RLInstance<double[], Integer> sample : samples) {
			RealVector vstate = new ArrayRealVector(sample.state());
			double target = 0;
			if (sample.nextState() == null) {
				target = sample.reinforcement();
			} else {
				double gamma = Math.pow(discountFactor(),
						sample.duration());

				target = sample.reinforcement() + gamma
						* vfunc.value(sample.nextState());
			}
			Pair<RealVector, Double> vsample = new Pair<RealVector, Double>(
					vstate, target);
			vsamples.add(vsample);
		}

		List<BatchFunctionApproximator<RealVector, Double>> models = new ArrayList<BatchFunctionApproximator<RealVector, Double>>();
		models.add(new BatchLinearApproximator(_stateDims));
		for (double lr : _params.learningRates) {
			/*for (double lambda : _params.regularizers) {
				// models.add(new SGD(_stateDims, new ObjectiveFunction(
				// Loss.SQUARED_ERROR, Penalty.L1, lambda), lr,
				// _params.numBatchIterations));
				models.add(new SGD(_stateDims, new ObjectiveFunction(
						Loss.SQUARED_ERROR, Penalty.L2, lambda), lr,
						_params.numBatchIterations));
			}*/
			for(int maxNonzero=20;maxNonzero<=100;maxNonzero+=10){
				models.add(new LARS(_stateDims, maxNonzero, 0, Double.POSITIVE_INFINITY));
			}
		}

		CrossValidation<RealVector, Double> cv = new CrossValidation<RealVector, Double>(
				new SquaredError(), models, vsamples);

		CrossValidationResult<RealVector, Double> cvResult = cv.cv(
				_params.trainRatio, _params.numTests);

		return cvResult;
	}
	
	@Override
	public void train(RLInstance<double[],Integer> inst){
		/*
		 * Record the new sample.
		 */
		_numSamples++;
		Integer optionIndex = inst.action();
		RLInstance<double[], Integer> sample = inst;
		_osamples.get(optionIndex).add(sample);

		if (_numSamples >= _params.numPolicyEvaluationSamples) {

			System.out.println("Training");

			/*
			 * Estimate the action-value function.
			 */
			List<BatchFunctionApproximator<RealVector, Double>> qfuncs = new ArrayList<BatchFunctionApproximator<RealVector, Double>>();
			for (int a = 0; a < _numOptions; a++) {
				qfuncs.add(null);
			}
			List<Boolean> canUse = new ArrayList<Boolean>();
			List<Double> cvScoreSums = new ArrayList<Double>();
			for (int a = 0; a < _numOptions; a++) {
				cvScoreSums.add(0.0);
			}

			VFunction<double[]> vfunc = new DummyVFunction();

			for (int i = 0; i < _params.numIterations; i++) {
				System.out.println("\tIteration: " + (i + 1));
				for (int a = 0; a < _numOptions; a++) {
					System.out.println("\t\tAction: " + a);
					List<RLInstance<double[], Integer>> asamples = _osamples
							.get(a);
					if (asamples.size() >= _params.minOptionSamples) {
						canUse.add(true);

						CrossValidationResult<RealVector, Double> cvResult = estimateQ(
								a, vfunc);
						cvScoreSums.set(
								a,
								cvScoreSums.get(a)
										+ cvResult.score(cvResult
												.bestModelIndex()));
						BatchFunctionApproximator<RealVector, Double> fa = cvResult
								.bestModel();
						qfuncs.set(a, fa);
					} else {
						canUse.add(false);
					}
				}
				vfunc = new FAVFunc(qfuncs, canUse);
			}
			
			_qfunc = new FAQFunc(qfuncs, canUse);

			List<Double> cvScores = new ArrayList<Double>(_numOptions);
			for (int a = 0; a < _numOptions; a++) {
				cvScores.add(cvScoreSums.get(a) / _params.numIterations);
			}

			/*
			 * Update the policy.
			 */
			_opolicy = new FAPolicy(qfuncs, canUse, cvScores);

			/*
			 * Clear all of the samples.
			 */
			_numSamples = 0;
			for (int a = 0; a < _numOptions; a++) {
				_osamples.get(a).clear();
			}
		}
	}

	@Override
	public Option<double[],Integer> updateOption(Option<double[],Integer> option,
			int optionIndex) {
		if(option instanceof InterruptingOption){
			InterruptingOption ioOption = (InterruptingOption)option;
			return new InterruptingOption(ioOption.originalOption(), optionIndex, _qfunc);
		}else{
			return new InterruptingOption(option, optionIndex, _qfunc);
		}
	}
	
	public static class InterruptingOption implements Option<double[],Integer>{

		private Option<double[],Integer> _option;
		private int _optionIndex;
		private QFunction<double[],Integer> _qfunc;
		private double _minGap;
		
		public InterruptingOption(Option<double[],Integer> option, int optionIndex, QFunction<double[],Integer> qfunc, double minGap){
			_option = option;
			_optionIndex = optionIndex;
			_qfunc = qfunc;
			_minGap = minGap;
		}
		
		public InterruptingOption(Option<double[],Integer> option, int optionIndex, QFunction<double[],Integer> qfunc){
			this(option, optionIndex, qfunc, 0.0);
		}
		
		@Override
		public Integer policy(double[] state) {
			return _option.policy(state);
		}

		@Override
		public double terminationProb(double[] state, int duration) {
			double gval = _qfunc.greedyValue(state);
			double val = _qfunc.value(state, _optionIndex);
			if(gval > val + _minGap){
				return 1;
			}else{
				double origTProb = _option.terminationProb(state, duration);
				return origTProb;
			}
		}

		@Override
		public boolean inInitialSet(double[] state) {
			return _option.inInitialSet(state);
		}
		
		public Option<double[],Integer> originalOption(){
			return _option;
		}
		
	}

}
