package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.Classifier;
import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.ClassifierPolicy;
import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.optim.Optimization;
import tmrutil.util.Factory;
import tmrutil.util.Pair;

/**
 * A rollout based implementation of classification based policy iteration.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the primitive action type
 */
public class RolloutCAPI<S, A> extends AbstractAPI<S,A> {

	private Simulator<S, ?, A> _sim;
	private int _numSamples;
	private int _maxOptionTrajectoryLength;
	private int _rolloutLength;
	private double _discountFactor;

	private Factory<Classifier<S, Integer>> _cfactory;
	private Policy<S,Option<S,A>> _cpolicy;
	private Policy<S,Option<S,A>> _initPolicy;
	private Optimization _opType;

	public RolloutCAPI(Simulator<S, ?, A> sim,
			ActionSet<S, Option<S,A>> options, int numSamples,
			int maxOptionTrajectoryLength, int rolloutLength,
			double discountFactor, Factory<Classifier<S, Integer>> cfactory,
			Policy<S,Option<S,A>> initPolicy, Optimization opType) {
		super(options);
		_sim = sim;
		_numSamples = numSamples;
		_maxOptionTrajectoryLength = maxOptionTrajectoryLength;
		_rolloutLength = rolloutLength;
		_discountFactor = discountFactor;

		_cfactory = cfactory;
		_cpolicy = null;
		_initPolicy = initPolicy;

		_opType = opType;
	}

	public void reset() {
		_cpolicy = null;
	}

	public Policy<S,Option<S,A>> currentPolicy() {
		if (_cpolicy == null) {
			return _initPolicy;
		} else {
			return _cpolicy;
		}
	}

	public void improvePolicy(Iterable<S> states) {
		if (_cpolicy == null) {
			_cpolicy = _initPolicy;
		}
		Collection<Pair<S, Integer>> tset = buildTrainingSet(states, new OptionToPrimitivePolicy<S,A>(_cpolicy));
		Classifier<S, Integer> c = _cfactory.newInstance();
		c.train(tset);

		_cpolicy = new ClassifierPolicy<S, Option<S, A>>(c, options());
	}

	public void improvePolicy(Iterable<S> states, int numIterations) {
		for (int i = 0; i < numIterations; i++) {
			improvePolicy(states);
		}
	}

	public Collection<Pair<S, Integer>> buildTrainingSet(Iterable<S> states,
			Policy<S, A> policy) {
		List<Pair<S, Integer>> tset = new ArrayList<Pair<S, Integer>>();

		for (S state : states) {
			Pair<Integer, Double> pair = bestOption(state, policy);
			Pair<S, Integer> sample = new Pair<S, Integer>(state, pair.getA());
			tset.add(sample);
		}

		return tset;
	}

	public boolean isMaximizing() {
		return _opType.equals(Optimization.MAXIMIZE);
	}

	public boolean isMinimizing() {
		return _opType.equals(Optimization.MINIMIZE);
	}

	public Pair<Integer, Double> bestOption(S state, Policy<S, A> policy) {
		if (isMaximizing()) {
			return maxOption(state, policy);
		} else {
			return minOption(state, policy);
		}
	}

	private Pair<Integer, Double> minOption(S state, Policy<S, A> policy) {
		Double minValue = null;
		Integer bestIndex = null;
		Collection<Integer> indices = options().validIndices(state);
		for (Integer i : indices) {
			Option<S, A> option = option(i);
			double score = score(state, option, policy);
			if (minValue == null || score < minValue) {
				minValue = score;
				bestIndex = i;
			}
		}
		return new Pair<Integer, Double>(bestIndex, minValue);
	}

	private Pair<Integer, Double> maxOption(S state, Policy<S, A> policy) {
		Double maxValue = null;
		Integer bestIndex = null;
		Collection<Integer> indices = options().validIndices(state);
		for (Integer i : indices) {
			Option<S, A> option = option(i);
			double score = score(state, option, policy);
			if (maxValue == null || score > maxValue) {
				maxValue = score;
				bestIndex = i;
			}
		}
		return new Pair<Integer, Double>(bestIndex, maxValue);
	}

	public double score(S state, Option<S, A> option, Policy<S, A> policy) {
		double score = 0;
		for (int s = 0; s < _numSamples; s++) {
			OptionOutcome<S, ?> optionOutcome = _sim.sample(state, option,
					_discountFactor, _maxOptionTrajectoryLength);
			OptionOutcome<S, ?> policyOutcome = _sim.samplePolicy(
					optionOutcome.terminalState(), policy, _discountFactor,
					_rolloutLength);

			double optionR = optionOutcome.dCumR();
			int optionDuration = optionOutcome.duration();
			double policyR = policyOutcome.dCumR();

			score += optionR + Math.pow(_discountFactor, optionDuration)
					* policyR;
		}
		return score / _numSamples;
	}

	@Override
	public Policy<S, Option<S,A>> iterate(Policy<S, Option<S,A>> policy, Iterable<S> states) {
		Collection<Pair<S,Integer>> tset = buildTrainingSet(states, new OptionToPrimitivePolicy<S,A>(policy));
		
		Classifier<S, Integer> c = _cfactory.newInstance();
		c.train(tset);
		
		return new ClassifierPolicy<S, Option<S,A>>(c, options());
	}
}
