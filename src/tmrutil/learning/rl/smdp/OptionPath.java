package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.math.Metric;

/**
 * Represents a trajectory in a deterministic MDP under a sequence of options.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the primitive action type
 */
public class OptionPath<S, A> {

	private ActionSet<S, Option<S, A>> _optionSet;

	private List<S> _stateSeq;
	private List<Integer> _optionSeq;
	private List<Double> _reinfSeq;
	private List<Collection<Integer>> _untried;

	public OptionPath(S initState, ActionSet<S, Option<S, A>> optionSet) {
		_optionSet = optionSet;

		_stateSeq = new ArrayList<S>();
		_stateSeq.add(initState);
		_reinfSeq = new ArrayList<Double>();
		_untried = new ArrayList<Collection<Integer>>();
		_untried.add(_optionSet.validIndices(initState));

		_optionSeq = new ArrayList<Integer>();
	}

	public OptionPath(OptionPath<S, A> path) {
		_optionSet = path._optionSet;
		_stateSeq = new ArrayList<S>(path._stateSeq);
		_reinfSeq = new ArrayList<Double>(path._reinfSeq);
		_untried = new ArrayList<Collection<Integer>>(path._untried);
		_optionSeq = new ArrayList<Integer>(path._optionSeq);
	}

	public void append(int optionIndex, S resultState, double reinforcement) {
		_optionSeq.add(optionIndex);
		_stateSeq.add(resultState);

		_reinfSeq.add(reinforcement);

		Collection<Integer> indices = new ArrayList<Integer>(
				_optionSet.validIndices(resultState));
		indices.remove(optionIndex);

		_untried.add(indices);
	}

	public List<S> stateSequence() {
		return new ArrayList<S>(_stateSeq);
	}

	public int optionSequenceLength() {
		return _optionSeq.size();
	}

	/**
	 * Backs up the path by one step (removes the last executed option).
	 */
	public void backup() {
		_optionSeq.remove(_optionSeq.size() - 1);
		_stateSeq.remove(_stateSeq.size() - 1);
		_reinfSeq.remove(_reinfSeq.size() - 1);
		_untried.remove(_untried.size() - 1);
	}

	/**
	 * Returns the total reinforcement received along this path.
	 * 
	 * @return total reinforcement received along this path
	 */
	public double totalReinforcement() {
		double rsum = 0;
		for (Double r : _reinfSeq) {
			rsum += r;
		}
		return rsum;
	}

	public Option<S, A> peekAtIndex(int index) {
		return _optionSet.action(_optionSeq.get(index));
	}

	public Option<S, A> removeFirst() {
		Option<S, A> option = _optionSet.action(_optionSeq.remove(0));
		_stateSeq.remove(0);
		_untried.remove(0);
		return option;
	}

	/**
	 * The indices of untried actions at the last state in this path.
	 * 
	 * @return a collection of untried action indices
	 */
	public Collection<Integer> untriedIndices() {
		return _untried.get(_untried.size() - 1);
	}

	/**
	 * Returns the list of untried actions at the last state in this path.
	 * 
	 * @return a list of untried actions
	 */
	public List<Option<S, A>> untried() {
		Collection<Integer> indices = untriedIndices();
		List<Option<S, A>> untriedOptions = new ArrayList<Option<S, A>>(
				indices.size());
		for (Integer ind : indices) {
			untriedOptions.add(_optionSet.action(ind));
		}
		return untriedOptions;
	}

	/**
	 * Returns the last state on this path.
	 * 
	 * @return the last state on this path
	 */
	public S lastState() {
		return _stateSeq.get(_stateSeq.size() - 1);
	}

	/**
	 * Calculates and returns the index of the closest state in this path
	 * compared to a specified state under the given metric.
	 * 
	 * @param state
	 *            a state
	 * @param metric
	 *            a metric
	 * @return the index of the closest state on the path to <code>state</code>
	 */
	public int indexOfClosestStateOnPath(S state, Metric<S> metric) {
		double bestDist = 0;
		int bestInd = 0;
		for (int i = 0; i < _stateSeq.size(); i++) {
			S si = _stateSeq.get(i);
			double dist = metric.distance(state, si);
			if (i == 0 || dist < bestDist) {
				bestInd = i;
				bestDist = dist;
			}
		}
		return bestInd;
	}

	/**
	 * Calculates and returns the distance of the closest state in this path
	 * compared to a specified state under the given metric.
	 * 
	 * @param state
	 *            a state
	 * @param metric
	 *            a metric
	 * @return the distance from <code>state</code> to the closest state on this
	 *         path
	 */
	public double distanceOfClosestStateOnPath(S state, Metric<S> metric) {
		double bestDist = 0;
		for (int i = 0; i < _stateSeq.size(); i++) {
			S si = _stateSeq.get(i);
			double dist = metric.distance(state, si);
			if (i == 0 || dist < bestDist) {
				bestDist = dist;
			}
		}
		return bestDist;
	}

	/**
	 * Constructs a subpath of this path with a specified number of steps
	 * removed from the beginning of the path.
	 * 
	 * @param stepsToRemove
	 *            the number of steps to remove from the beginning of the path
	 * @return a subpath of this path
	 */
	public OptionPath<S, A> subpath(int stepsToRemove) {
		OptionPath<S, A> p = new OptionPath<S, A>(this);
		for (int i = 0; i < stepsToRemove; i++) {
			p.removeFirst();
		}
		return p;
	}

	/**
	 * Returns the distance from a state to the first state on this path.
	 * 
	 * @param state
	 *            a state
	 * @param metric
	 *            a metric
	 * @return the distance from <code>state</code> to the first state on this
	 *         path
	 */
	public double distanceToFirstStateOnPath(S state, Metric<S> metric) {
		return metric.distance(state, _stateSeq.get(0));
	}

	/**
	 * Returns the distance from a state to the last state on this path.
	 * 
	 * @param state
	 *            a state
	 * @param metric
	 *            a metric
	 * @return the distance from <code>state</code> to the last state on this
	 *         path
	 */
	public double distanceToLastStateOnPath(S state, Metric<S> metric) {
		return metric.distance(state, _stateSeq.get(_stateSeq.size() - 1));
	}

	/**
	 * Approximately follows this path when starting from a state that is not
	 * necessarily the same as the start state of this path.
	 * 
	 * @param dsim
	 *            a deterministic instance of the target simulator
	 * @param metric
	 *            a metric on the state space
	 * @param threshold
	 *            the distance threshold at which two states are considered to
	 *            have the same dynamics
	 * @param state
	 *            the state to select an option for
	 * @return the option to execute to approximately follow this path
	 */
	public Option<S, A> approxPolicy(Simulator<S, ?, A> dsim, Metric<S> metric,
			double threshold, S state, int maxTrajectoryLength) {
		int closestI = indexOfClosestStateOnPath(state, metric);
		double dist = metric.distance(state, _stateSeq.get(closestI));
		if (dist < threshold) { // Follow the original path
			int optionIndex = _optionSeq.get(closestI);
			return _optionSet.action(optionIndex);
		} else { // Try to get back on the path as quickly as possible
			/*
			 * double bestDist = 0; Option<S, A> bestOption = null;
			 * Collection<Integer> validInds = _optionSet.validIndices(state);
			 * for (Integer ind : validInds) { Option<S, A> option =
			 * _optionSet.action(ind); OptionOutcome<S, ?> oc =
			 * dsim.sample(state, option, 1, maxTrajectoryLength); double d =
			 * metric.distance(oc.terminalState(), _stateSeq.get(closestI)); if
			 * (bestOption == null || d < bestDist) { bestDist = d; bestOption =
			 * option; } } return bestOption;
			 */
			return null;
		}
	}
}
