package tmrutil.learning.rl.smdp;


import java.util.Collection;

import tmrutil.learning.BatchFunctionApproximator;
import tmrutil.learning.rl.VFunction;
import tmrutil.util.Factory;
import tmrutil.util.Pair;

/**
 * A basic value function implementation using a function approximator.
 * @author Timothy A. Mann
 * 
 * @param <S> the state type
 */
public class FittedVFunction<S> implements VFunction<S> {

	private Factory<? extends BatchFunctionApproximator<S, Double>> _factory;
	private BatchFunctionApproximator<S, Double> _vfunc;
	private double _defaultValue;

	public FittedVFunction(Factory<? extends BatchFunctionApproximator<S,Double>> factory)
	{
		this(factory, 0.0);
	}
	
	public FittedVFunction(Factory<? extends BatchFunctionApproximator<S, Double>> factory, double defaultValue)
	{
		_factory = factory;
		_defaultValue = defaultValue;
	}
	
	@Override
	public double value(S state) {
		if (_vfunc == null) {
			return _defaultValue;
		} else {
			return _vfunc.evaluate(state);
		}
	}

	public void train(Collection<Pair<S, Double>> samples) {
			_vfunc = _factory.newInstance();
			_vfunc.train(samples);
	}
}
