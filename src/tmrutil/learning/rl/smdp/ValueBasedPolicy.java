package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.VFunction;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.optim.Optimization;
import tmrutil.stats.Random;
import tmrutil.util.ListUtil;
import tmrutil.util.Pair;

/**
 * A value-based policy uses a value function and a simulator to select the
 * best course of action to take.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class ValueBasedPolicy<S, A> implements Policy<S, A> {
	private Simulator<S, ?, A> _sim;
	private VFunction<S> _vfunc;
	private int _numSamples;
	private double _discountFactor;
	private int _maxTrajectoryLength;
	private ActionSet<S,Option<S, A>> _options;
	private Optimization _opType;

	private Option<S, A> _currentOption;
	private int _currentOptionStepCount;

	public ValueBasedPolicy(Simulator<S, ?, A> sim, VFunction<S> vfunc,
			int numSamples, double discountFactor, int maxTrajectoryLength,
			ActionSet<S, Option<S,A>> options, Optimization opType) {
		_sim = sim;
		_vfunc = vfunc;
		_numSamples = numSamples;
		_discountFactor = discountFactor;
		_maxTrajectoryLength = maxTrajectoryLength;
		_options = options;
		_opType = opType;

		_currentOption = null;
		_currentOptionStepCount = 0;
	}

	@Override
	public A policy(S state) {

		double r = Random.uniform();
		if (_currentOption == null || _currentOptionStepCount >= _maxTrajectoryLength
				|| r < _currentOption.terminationProb(state, _currentOptionStepCount)) {
			List<Option<S,A>> validOptions = _options.validList(state);
			List<Pair<Option<S, A>, Double>> list = new ArrayList<Pair<Option<S, A>, Double>>(
					validOptions.size());
			for (Option<S, A> option : validOptions) {
				double value = 0;

				for (int s = 0; s < _numSamples; s++) {
					OptionOutcome<S, ?> outcome = _sim.sample(state,
							option, _discountFactor, _maxTrajectoryLength);

					value += outcome.dCumR()
							+ Math.pow(_discountFactor, outcome.duration())
							* _vfunc.value(outcome.terminalState());
				}

				Pair<Option<S, A>, Double> elm = new Pair<Option<S, A>, Double>(
						option, value);
				list.add(elm);
			}

			if (_opType.equals(Optimization.MAXIMIZE)) {
				_currentOption = ListUtil.maxElm(list).getA();
			} else {
				_currentOption = ListUtil.minElm(list).getA();
			}
			_currentOptionStepCount = 0;
		}
		A action = _currentOption.policy(state);
		_currentOptionStepCount++;
		return action;
	}

	public void reset() {
		_currentOption = null;
		_currentOptionStepCount = 0;
	}

}