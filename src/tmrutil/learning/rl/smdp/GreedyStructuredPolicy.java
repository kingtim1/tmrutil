package tmrutil.learning.rl.smdp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.factored.FactoredAction;
import tmrutil.learning.rl.factored.FactoredActionImpl;
import tmrutil.optim.Optimization;

/**
 * A structured decision maker builds actions sequentially and greedily with
 * respect to a given action-value function.
 * 
 * @author Timothy A. Mann
 * 
 */
public class GreedyStructuredPolicy<S> implements Policy<S, FactoredAction> {

	private QFunction<S, int[]> _qfunc;
	/**
	 * A list containing the set of valid variable choices for each component of
	 * an action.
	 */
	private int[] _maxValues;
	private Optimization _opType;

	public GreedyStructuredPolicy(QFunction<S, int[]> qfunc,
			int[] maxValues, Optimization opType) {
		_qfunc = qfunc;
		_maxValues = maxValues;
		_opType = opType;
	}

	public Set<Integer> validChoices(int index) {
		Set<Integer> choices = new HashSet<Integer>();
		for(int c=0;c<_maxValues[index];c++){
			choices.add(c);
		}
		return choices;
	}

	public int actionSize() {
		return _maxValues.length;
	}

	public int[] buildAction(S state) {
		int[] action = new int[actionSize()];
		List<Integer> remainingIndices = new ArrayList<Integer>();
		for (int i = 0; i < actionSize(); i++) {
			remainingIndices.add(i);
		}
		Collections.shuffle(remainingIndices);
		return buildAction(state, action, remainingIndices);
	}

	public int[] buildAction(S state, int[] action,
			List<Integer> remainingIndices) {
		if (remainingIndices.isEmpty()) {
			return action;
		} else {
			Integer ind = remainingIndices.remove(0);
			Integer bestChoice = null;
			Double bestVal = null;
			for (Integer choice : validChoices(ind)) {
				action[ind] = choice;
				double qval = _qfunc.value(state, action);

				boolean updateMaximize = (bestVal == null)
						|| (bestVal < qval && _opType
								.equals(Optimization.MAXIMIZE));
				boolean updateMinimize = (bestVal == null)
						|| (bestVal > qval && _opType
								.equals(Optimization.MINIMIZE));

				if (updateMaximize || updateMinimize) {
					bestVal = qval;
					bestChoice = choice;
				}
			}

			action[ind] = bestChoice;
			return buildAction(state, action, remainingIndices);
		}
	}

	@Override
	public FactoredAction policy(S state) {
		int[] action = buildAction(state);
		FactoredAction faction = new FactoredActionImpl(action, _maxValues);
		return faction;
	}

}
