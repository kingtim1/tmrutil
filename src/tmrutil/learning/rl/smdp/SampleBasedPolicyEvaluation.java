package tmrutil.learning.rl.smdp;

import tmrutil.learning.rl.DiscreteVFunction;
import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.VFunction;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.optim.Optimization;

/**
 * Implements Sample-Based Policy Evaluation (SBPE) given a set of states and a
 * set of generative option models.
 * 
 * @author Timothy A. Mann
 * 
 */
public class SampleBasedPolicyEvaluation<S,A> {

	private Simulator<S,?,A> _sim;
	private Iterable<S> _states;
	private Policy<S, Option<S,A>> _policy;
	private double _discountFactor;

	/**
	 * The number of samples taken per state at each iteration.
	 */
	private int _numSamples;

	public SampleBasedPolicyEvaluation(Simulator<S,?,A> sim, Iterable<S> states,
			Policy<S, Option<S,A>> policy, int numSamples,
			double discountFactor) {
		_sim = sim;
		_states = states;
		_policy = policy;
		_numSamples = numSamples;
		_discountFactor = discountFactor;
	}

	public VFunction<S> iterate(VFunction<S> vfunc) {
		DiscreteVFunction<S> newV = new DiscreteVFunction<S>();

		for (S state : _states) {
			OptionToPrimitivePolicy<S,A> ppolicy = new OptionToPrimitivePolicy<S,A>(_policy);
			double cumVs = 0;
			for (int i = 0; i < _numSamples; i++) {
				OptionOutcome<S,?> oc = _sim.samplePrimitive(state, ppolicy.policy(state));
				cumVs = cumVs
						+ (oc.dCumR() + Math.pow(_discountFactor,
								oc.duration())
								* vfunc.value(oc.terminalState()));
			}
			newV.update(state, cumVs / _numSamples);
		}

		return newV;
	}
}
