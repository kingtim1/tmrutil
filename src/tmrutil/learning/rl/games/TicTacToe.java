package tmrutil.learning.rl.games;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.games.TicTacToe.Board;
import tmrutil.util.Pair;

/**
 * Implements the game of Tic-Tac-Toe. The controlled agent plays by placing O's
 * and the opponent plays with X's.
 * 
 * @author Timothy A. Mann
 * 
 */
public class TicTacToe implements Game<Board, Pair<Integer, Integer>>, Drawable
{
	public static class Board
	{
		private static int EMPTY = 0;
		private static int X = 1;
		private static int O = 2;

		private int[][] _board;
		private int _size;

		public Board(int size)
		{
			_board = new int[size][size];
			_size = size;
		}

		public Board(Board copy)
		{
			this(copy.size());
			for (int r = 0; r < _size; r++) {
				for (int c = 0; c < _size; c++) {
					_board[r][c] = copy._board[r][c];
				}
			}
		}

		/**
		 * Returns the number of rows (or columns).
		 * 
		 * @return the number of rows (or columns)
		 */
		public int size()
		{
			return _size;
		}

		/**
		 * Returns true if the element is taken, and false if the element is not
		 * taken.
		 * 
		 * @param r
		 *            a row index
		 * @param c
		 *            a column index
		 * @return true if the element is taken (not empty); otherwise false
		 */
		public boolean isTaken(int r, int c)
		{
			return _board[r][c] != EMPTY;
		}

		/**
		 * Returns true if the specified element is marked by an X, and false
		 * otherwise.
		 * 
		 * @param r
		 *            a row index
		 * @param c
		 *            a column index
		 * @return true if the element is marked by an X; otherwise false
		 */
		public boolean isX(int r, int c)
		{
			return _board[r][c] == X;
		}

		/**
		 * Returns true if the specified element is marked by an O, and false
		 * otherwise.
		 * 
		 * @param r
		 *            a row index
		 * @param c
		 *            a column index
		 * @return true if the element is marked as an O; otherwise false
		 */
		public boolean isO(int r, int c)
		{
			return _board[r][c] == O;
		}

		public boolean setX(int r, int c)
		{
			if (!isTaken(r, c)) {
				_board[r][c] = X;
				return true;
			} else {
				return false;
			}
		}

		public boolean setO(int r, int c)
		{
			if (!isTaken(r, c)) {
				_board[r][c] = O;
				return true;
			} else {
				return false;
			}
		}

		public boolean isGameOver()
		{
			return didXWin() || didOWin();
		}

		public boolean didOWin()
		{
			return isWinner(O);
		}

		public boolean didXWin()
		{

			return isWinner(X);
		}

		private boolean isWinner(int player)
		{
			// Check if a row is completed
			for (int r = 0; r < _size; r++) {
				if (isWinner(player, r, 0, 1, 0)) {
					return true;
				}
			}
			// Check if a column is completed
			for (int c = 0; c < _size; c++) {
				if (isWinner(player, 0, c, 1, 0)) {
					return true;
				}
			}

			// Check if the diagonals are completed
			if (isWinner(player, 0, 0, 1, 1)
					|| isWinner(player, _size - 1, 0, -1, 1)) {
				return true;
			}

			return false;
		}

		private boolean isWinner(int player, int initRow, int initCol,
				int deltaR, int deltaC)
		{
			int count = 0;
			int r = initRow;
			int c = initCol;
			boolean notDone = false;
			do {
				if (_board[r][c] == player) {
					count++;
					notDone = true;
				}
				r += deltaR;
				c += deltaC;
			} while (notDone && r > 0 && r < _size && c > 0 && c < _size);
			return (count == _size);
		}
	}

	private static final int DEFAULT_BOARD_SIZE = 3;

	private Board _board;
	private int _boardSize;
	private int _whoseTurn;
	
	public TicTacToe()
	{
		this(DEFAULT_BOARD_SIZE);
	}

	public TicTacToe(int boardSize)
	{
		_boardSize = boardSize;

		reset();
	}

	@Override
	public double evaluate(int playerID)
	{
		if (playerID == 0 && _board.didOWin()) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public void execute(Pair<Integer, Integer> action)
	{
		if (_whoseTurn == 0) {
			if (_board.setO(action.getA(), action.getB())) {
				_whoseTurn = 1;
			}
		} else {
			if (_board.setX(action.getA(), action.getB())) {
				_whoseTurn = 0;
			}
		}
	}

	@Override
	public int numberOfPlayers()
	{
		return 2;
	}

	@Override
	public int numberOfOpponents()
	{
		return 1;
	}

	@Override
	public Board getState()
	{
		return new Board(_board);
	}

	@Override
	public void reset()
	{
		_board = new Board(_boardSize);
		_whoseTurn = 0;
	}

	@Override
	public boolean isFinished()
	{
		return _board.isGameOver();
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setColor(Color.BLACK);

		double twidth = width / _board.size();
		double theight = height / _board.size();

		Stroke thinStroke = new BasicStroke(1.0f);
		Stroke thickStroke = new BasicStroke(3.0f);

		for (int r = 0; r < _board.size(); r++) {
			for (int c = 0; c < _board.size(); c++) {
				Rectangle2D rect = new Rectangle2D.Double(r * twidth, c
						* theight, twidth, theight);
				g2.setStroke(thinStroke);
				g2.draw(rect);

				g2.setStroke(thickStroke);
				if (_board.isO(r, c)) {
					Ellipse2D o = new Ellipse2D.Double(rect.getX(),
							rect.getY(), rect.getWidth(), rect.getHeight());
					g2.draw(o);
				}
				if (_board.isX(r, c)) {
					Line2D l1 = new Line2D.Double(rect.getX(), rect.getY(),
							rect.getX() + rect.getWidth(), rect.getY()
									+ rect.getHeight());
					Line2D l2 = new Line2D.Double(
							rect.getX() + rect.getWidth(), rect.getY(),
							rect.getX(), rect.getY() + rect.getHeight());
					g2.draw(l1);
					g2.draw(l2);
				}
			}
		}

		g2.dispose();
	}

	@Override
	public int whoseTurnIsIt()
	{
		return _whoseTurn;
	}

	@Override
	public Set<Integer> winner()
	{
		Set<Integer> winners = new HashSet<Integer>();
		if (_board.didOWin()) {
			winners.add(0);
		}
		if (_board.didXWin()) {
			winners.add(1);
		}
		return winners;
	}
}
