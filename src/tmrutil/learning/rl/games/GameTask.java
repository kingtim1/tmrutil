package tmrutil.learning.rl.games;

import java.util.List;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.Task;

public class GameTask<S, A> extends Task<S, A>
{
	private static final int MY_ID = 0;
	
	private Game<S,A> _game;
	private List<LearningSystem<S,A>> _opponents;
	
	public GameTask(Game<S,A> game, List<LearningSystem<S,A>> opponents)
	{
		if(game.numberOfOpponents() != opponents.size()){
			throw new IllegalArgumentException("The number of opponents in the game does not match number of opponents given.");
		}
		
		_game = game;
		_opponents = opponents;
	}

	@Override
	public S getState()
	{
		int pid = _game.whoseTurnIsIt();
		while(pid != MY_ID){
			pid = _game.whoseTurnIsIt();
			S state = _game.getState();
			A oAction = _opponents.get(pid-1).policy(state);
			_game.execute(oAction);
		}
		return _game.getState();
	}

	@Override
	public void execute(A action)
	{
		_game.execute(action);
	}

	@Override
	public double evaluate()
	{
		return _game.evaluate(MY_ID);
	}

	@Override
	public void reset()
	{
		_game.reset();
	}

	@Override
	public boolean isFinished()
	{
		return _game.isFinished();
	}

}
