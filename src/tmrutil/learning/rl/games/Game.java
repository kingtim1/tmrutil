package tmrutil.learning.rl.games;

import java.util.Set;

/**
 * A game is similar to a task; however, the transition to a new state depends
 * on the actions selected by a controlled agent and some number of opponents.
 * 
 * @param <S>
 *            the state description type
 * @param <A>
 *            the agent's action type
 * 
 * @author Timothy A. Mann
 * 
 */
public interface Game<S, A>
{
	/**
	 * Returns the current state of the game.
	 * 
	 * @return the current state of the game
	 */
	public S getState();

	/**
	 * Executes the specified action <code>action</code> selected by the
	 * controlled agent. The opponents actions are also executed and the actions
	 * selected by the opponents are returned in a list.
	 * 
	 * @param action
	 *            the action selected by the controlled agent
	 * @return a list of action selected by opponents
	 */
	public void execute(A action);

	/**
	 * Evaluates the current state for the player identifier (an integer in the range 0 to n-1, where n is the number of players).
	 * 
	 * @return a scalar reward
	 */
	public double evaluate(int playerID);
	
	/**
	 * Returns the total number of players.
	 * @return the number of players
	 */
	public int numberOfPlayers();

	/**
	 * Returns the number of opponents. This is the same as the one minus the number of players.
	 * 
	 * @return the number of opponents
	 */
	public int numberOfOpponents();

	/**
	 * Returns the identifier for the player whose turn it is (an integer
	 * between 0 and n-1, where n is the number of players). If the game is
	 * over, then -1 is returned.
	 * 
	 * @return the identifier of a player or -1
	 */
	public int whoseTurnIsIt();

	/**
	 * Returns the identifiers of the winners (integers between 0 and n-1, where
	 * n is the number of players). If the game is not finished, then the empty set is returned.
	 * 
	 * @return the identifier of the winner or the empty set.
	 */
	public Set<Integer> winner();

	/**
	 * Returns true if the game is over; otherwise false is returned and the
	 * game continues.
	 * 
	 * @return true if the game is over
	 */
	public boolean isFinished();

	/**
	 * Resets the game to an initial setting.
	 */
	public void reset();
}
