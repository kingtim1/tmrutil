package tmrutil.learning.rl;

import java.util.Arrays;
import java.util.Set;

import tmrutil.graphics.MatrixDisplay;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcess.Estimator;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Statistics;
import tmrutil.util.DataStore;
import tmrutil.util.Interval;

/**
 * An implementation of the R-MAX reinforcement learning algorithm first
 * described in
 * <p>
 * R. I. Brafman and M. Tennenholtz. R-MAX - a general polynomial time algorithm
 * for near-optimal reinforcement learning. In
 * <em>Proceedings of the Seventeenth
 * International Joint Conference on Artificial Intelligence</em>, pages
 * 953-958, 2001.
 * </p>
 * 
 * @author Timothy A. Mann
 * 
 */
public class RMax extends AbstractDiscreteTemporalDifferenceLearning implements Explorer<Integer,Integer>, KnownStateActionLearner<Integer,Integer>
{
	public static final double CONVERGENCE_THRESHOLD = 0.01;
	public static final int MAX_ITERATIONS_PER_UPDATE = 50;
	public static final int NUM_THREADS = 10;

	public static final double DEFAULT_LEARNING_RATE = 0.0;

	private double _rmax;
	private double _vmax;
	//private int[][] _numVisitsUntilKnown;
	protected Estimator _mdp;
	private double[] _V;

	/**
	 * Constructs an instance of the R-MAX model-based, reinforcement learning
	 * algorithm.
	 * 
	 * @param numStates
	 *            the number of states in the MDP
	 * @param numActions
	 *            the number of actions in the MDP
	 * @param discountFactor
	 *            the discount factor
	 * @param rewardInterval
	 *            the interval containing the minimum and maximum reward
	 * @param numVisitsUntilKnown
	 *            the number of visits to each state-action pair before it is
	 *            considered "known"
	 */
	public RMax(int numStates, int numActions, double discountFactor,
			Interval rewardInterval, int numVisitsUntilKnown)
	{
		this(numStates, numActions, discountFactor, rewardInterval,
				numVisitsUntilKnown, false);
	}
	
	public RMax(int numStates, int numActions, double discountFactor, Interval rewardInterval, int numVisitsUntilKnown, boolean refreshSamples)
	{
		this(numStates, numActions, discountFactor, rewardInterval, Estimator.buildNumVisitsArray(numStates, numActions, numVisitsUntilKnown), refreshSamples);
	}

	/**
	 * Constructs an instance of the R-MAX model-based, reinforcement learning
	 * algorithm.
	 * 
	 * @param numStates
	 *            the number of states in the MDP
	 * @param numActions
	 *            the number of actions in the MDP
	 * @param discountFactor
	 *            the discount factor
	 * @param rewardInterval
	 *            the interval containing the minimum and maximum reward
	 * @param numVisitsUntilKnown
	 *            the number of visits to each state-action pair before it is
	 *            considered "known"
	 * @param refreshSamples
	 *            true if the model should be updated as new batches of samples
	 *            become available or false if only the first batch of samples
	 *            should be used
	 */
	public RMax(int numStates, int numActions, double discountFactor,
			Interval rewardInterval, int[][] numVisitsUntilKnown,
			boolean refreshSamples)
	{
		super(numStates, numActions, DEFAULT_LEARNING_RATE, discountFactor,
				Optimization.MAXIMIZE);
		// _mdp = new EnvelopeExploreModelEstimator(numStates, numActions,
		// discountFactor,
		// rewardInterval, numVisitsUntilKnown);

		_mdp = new RefreshEstimator(numStates, numActions, rewardInterval,
				discountFactor, numVisitsUntilKnown, refreshSamples);

		_V = new double[_mdp.numberOfStates()];

		_rmax = rewardInterval.getMax();
		_vmax = _rmax / (1 - discountFactor);
		//_numVisitsUntilKnown = numVisitsUntilKnown;

		reset();
	}

	public double rmax()
	{
		return _rmax;
	}

	public double vmax()
	{
		return _vmax;
	}

	//public int numVisitsUntilKnown()
	//{
	//	return _numVisitsUntilKnown;
	//}

	@Override
	protected void resetImpl()
	{
		fillQ(vmax());
		_mdp.reset();
	}

	@Override
	protected void trainImpl(Integer prevState, Integer action,
			Integer newState, double reinforcement)
	{
		boolean update = _mdp.update(prevState, action, newState, reinforcement);
		if (update) {
			//if(getDiscountFactor() > 0){
				valueIteration(_mdp, MAX_ITERATIONS_PER_UPDATE, CONVERGENCE_THRESHOLD);
			//}else{
			//	setQ(prevState, action, _mdp.reinforcement(prevState, action));
			//}
		}
	}

	protected void valueIteration(DiscreteMarkovDecisionProcess<Integer,Integer> mdp, int maxIterations, double threshold)
	{
		_V = new double[numberOfStates()];
		boolean converged = false;
		double gamma = mdp.getDiscountFactor();
		
		double vmax = mdp.rmax() / (1-gamma);
		double[][] U = getAdmissibleHeuristic();
		
		for (int i = 0; i < maxIterations && !converged; i++) {

			double delta = 0;
			for (int s = 0; s < numberOfStates(); s++) {
				Set<Integer> nextStates = mdp.successors(s);
				for (int a = 0; a < numberOfActions(); a++) {
					double oldValue = evaluateQ(s, a);
					if (mdp.isKnown(s, a)) {	
						double saValue = mdp.reinforcement(s, a);
						for (Integer ns : nextStates) {
							double tprob = mdp.transitionProb(s, a, ns);
							double Vns = _V[ns];
							saValue += gamma * tprob * Vns;
						}
						
						if(U != null && saValue > U[s][a]){
							saValue = U[s][a];
						}

						double diff = Math.abs(saValue - oldValue);
						delta = Math.max(delta, diff);
						setQ(s, a, saValue);
					}else{
						double newQ = (U == null)? vmax : U[s][a];
						double diff = Math.abs(newQ - oldValue);
						delta = Math.max(delta, diff);
						setQ(s, a, newQ);
					}
				}
				_V[s] = maxQ(s);
			}

			if (delta <= threshold) {
				converged = true;
			}
		}
	}

	public double[] getStateVisitCount()
	{
		double[] stateVisits = new double[_mdp.numberOfStates() - 1];
		for (int s1 = 0; s1 < _mdp.numberOfStates() - 1; s1++) {
			stateVisits[s1] = _mdp.getCount(s1);
		}
		return stateVisits;
	}

	public double[] getValueFunction()
	{
		return Arrays.copyOf(_V, _V.length);
	}

	public DiscreteMarkovDecisionProcess<Integer,Integer> getModel()
	{
		return _mdp;
	}

	public double[] getStateRewards()
	{
		int nStates = _mdp.numberOfStates();
		int nActions = _mdp.numberOfActions();
		double[] stateRewards = new double[nStates];
		for (int s1 = 0; s1 < nStates - 2; s1++) {
			for (int a = 0; a < nActions; a++) {
				double r = _mdp.reinforcement(s1, a);
				if (a == 0) {
					stateRewards[s1] = r;
				}
				stateRewards[s1] = Math.max(stateRewards[s1], r);
			}
		}
		return stateRewards;
	}
	
	@Override
	public boolean isKnown(Integer state, Integer action)
	{
		return _mdp.isKnown(state, action);
	}

	public double[][] knownMatrix()
	{
		double[][] known = new double[numberOfStates()][numberOfActions()];
		for (int s = 0; s < numberOfStates(); s++) {
			for (int a = 0; a < numberOfActions(); a++) {
				if (_mdp.isKnown(s, a)) {
					known[s][a] = 1.0;
				}
			}
		}
		return known;
	}

	public double[] knownStates()
	{
		double[] knownStates = new double[numberOfStates()];
		for (int s = 0; s < numberOfStates(); s++) {
			boolean known = true;
			for (int a = 0; a < numberOfActions(); a++) {
				if (!_mdp.isKnown(s, a)) {
					known = false;
				}
			}
			if (known) {
				knownStates[s] = 1;
			}
		}
		return knownStates;
	}

	@Override
	public boolean isExploration(Integer state, Integer action)
	{
		return !_mdp.isKnown(state, action);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<Integer> greedyValueFunction() {
		return new QFunction.GreedyVFunction<Integer>(this);
	}

	@Override
	public double value(Integer state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(Integer state) {
		return maxQ(state);
	}

}
