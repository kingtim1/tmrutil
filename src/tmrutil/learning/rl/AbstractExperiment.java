package tmrutil.learning.rl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.configuration.Configuration;

import tmrutil.util.DebugException;
import tmrutil.util.FileDataStore;

/**
 * An abstract experiment that records data to a {@link FileDataStore} instance.
 * 
 * @author Timothy A. Mann
 * 
 */
public abstract class AbstractExperiment implements Runnable {
	public static final String ALG_PREFIX = "alg.";
	public static final String EXP_PREFIX = "exp.";
	public static final String ENV_PREFIX = "env.";
	
	public static final String BASE_DATA_DIR_KEY = "base_data_dir";
	public static final String NUM_TRIALS_KEY = "num_trials";

	private Configuration _config;
	private File _baseDataDir;
	private int _numTrials;

	/**
	 * Constructs an abstract experiment.
	 * 
	 * @param config
	 *            a configuration instance for this experiment
	 * @throws IOException
	 *             if an I/O error occurs while constructing an instance
	 */
	public AbstractExperiment(Configuration config)
			throws IOException {
		
		File baseDataDir = new File(config.getString(BASE_DATA_DIR_KEY));
		if (!baseDataDir.exists()) {
			baseDataDir.mkdirs();
		}
		if (!baseDataDir.isDirectory()) {
			throw new IOException(
					"The specified base data directory is not a directory.");
		}
		_baseDataDir = baseDataDir;

		int numTrials = config.getInt(NUM_TRIALS_KEY);
		if (numTrials < 1) {
			throw new IllegalArgumentException(
					"Cannot conduct experiment with less than 1 trial.");
		}
		_numTrials = numTrials;
	}

	/**
	 * Returns the file instance representing the base data directory.
	 * 
	 * @return the base data directory
	 */
	public File baseDataDir() {
		return _baseDataDir;
	}

	/**
	 * Returns the number of independent trials in a complete experiment.
	 * 
	 * @return the number of trials
	 */
	public int numTrials() {
		return _numTrials;
	}
	
	/**
	 * Returns the instance used to configure experiments.
	 * @return a configuration instance
	 */
	public Configuration config()
	{
		return _config;
	}

	/**
	 * Runs a single independent trial of the experiment.
	 * 
	 * @param dstore
	 *            a reference to the underlying data storage instance
	 * @param trialNum
	 *            the trial number
	 */
	protected abstract void runTrial(FileDataStore dstore, int trialNum);

	/**
	 * A helper method for writing parameter values. This method is called at
	 * the beginning of an experiment and records experimental parameters and
	 * parameters about the algorithms and environments being tested.
	 * 
	 * @param dstore
	 *            a reference to the underlying data storage instance
	 */
	protected void writeParams(FileDataStore dstore) {
		dstore.bind(EXP_PREFIX + "num_trials", _numTrials);
	}

	/**
	 * Runs a complete experiment and stores the data into a timestamped data
	 * directory under the base data directory for this instance.
	 */
	public void runExperiment() {
		String tstamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar
				.getInstance().getTime());

		File dataDir = new File(_baseDataDir, tstamp);

		try {
			FileDataStore dstore = new FileDataStore(dataDir);

			writeParams(dstore);

			for (int i = 0; i < _numTrials; i++) {
				runTrial(dstore, (i + 1));
			}
		} catch (IOException ex) {
			throw new DebugException(ex);
		}
	}

	/**
	 * Same as {@link runExperiment}.
	 */
	@Override
	public void run() {
		runExperiment();
	}
}
