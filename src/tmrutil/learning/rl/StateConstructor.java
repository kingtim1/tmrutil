package tmrutil.learning.rl;

/**
 * An interface for classes that can construct a state from a task. 
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <T> the task type
 */
public interface StateConstructor<S, T extends Task<?, ?>>
{
	/**
	 * Constructs a state based on the current settings of the specified task instance.
	 * @param task a task instance
	 * @return a state based on the current task settings
	 */
	public S getState(T task);
}
