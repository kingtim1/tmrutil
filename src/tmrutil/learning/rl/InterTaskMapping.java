package tmrutil.learning.rl;

/**
 * Provides a common interface for inter-task mappings. Inter-task mappings map
 * states, actions, reinforcements, and/or long term values of state-action
 * pairs or states from a source task to a target task.
 * <p/>
 * 
 * Unsupported operations in this interface simply throw an
 * {@link java.lang.UnsupportedOperationException} indicating that the operation
 * is unsupported.
 * 
 * @param <SS> the source task state type
 * @param <SA> the source task action type
 * @param <TS> the target task state type
 * @param <TA> the target task action type
 * 
 * @author Timothy A. Mann
 * 
 */
public interface InterTaskMapping<SS, SA, TS, TA>
{
	/**
	 * Maps a source task state to a target task state.
	 * 
	 * @param state
	 *            a state from the source task
	 * @return a state from the target task
	 */
	TS mapState(SS state);

	/**
	 * Maps an action from a source task to an action from the target task.
	 * 
	 * @param action
	 *            an action from the source task
	 * @return an action from the target task
	 */
	TA mapAction(SA action);

	/**
	 * Maps the estimated long term value of a state-action pair in the source
	 * task to a long term value of a state-action pair in the target task.
	 * 
	 * @param state
	 *            a state from the source task
	 * @param action
	 *            an action from the source task
	 * @return the long term value of the specified state-action pair mapped to
	 *         the target task
	 */
	Double mapValue(SS state, SA action);

	/**
	 * Maps the estimated immediate reinforcement associated with a state-action
	 * pair in the source task to an immediate reinforcement in the target task.
	 * 
	 * @param state
	 *            a state from the source task
	 * @param action
	 *            an action from the source task
	 * @return the immediate reward of the specified state-action pair mapped to
	 *         the target task
	 */
	Double mapReinforcement(SS state, SA action);
}
