package tmrutil.learning.rl;

import java.util.Arrays;

/**
 * Represents a deterministic policy for a task with discrete states and actions.
 * @author Timothy A. Mann
 *
 */
public class DiscretePolicy implements Policy<Integer, Integer>
{
	private int[] _actionMap;

	public DiscretePolicy(int[] actionMap)
	{
		_actionMap = Arrays.copyOf(actionMap, actionMap.length);
	}

	@Override
	public Integer policy(Integer state)
	{
		return _actionMap[state];
	}

}
