package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.NonPositiveDefiniteMatrixException;
import org.apache.commons.math3.linear.QRDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;
import org.apache.commons.math3.util.Precision;

import tmrutil.stats.vector.ZScore;
import tmrutil.util.Pair;

/**
 * An implementation of the Least-Angle Regression (LARS) algorithm. LARS is an
 * efficient algorithm for performing feature selection via L1 regularization.
 * 
 * This implementation is based on: Efron, Hastie, Johnstone, and Tibshirani.
 * Least Angle Regression. 2003.
 * 
 * @author Timothy A. Mann
 *
 */
public class LARS implements BatchFunctionApproximator<RealVector, Double> {

	private static final long serialVersionUID = -8064230385435435492L;

	public static class LARSPath {
		private List<RealVector> _weights;
		private List<List<Integer>> _activeSet;
		private List<Double> _stepSizes;
		private List<RealVector> _updateVectors;
		private List<Double> _absMaxCorrs;

		private LARSPath() {
			_weights = new ArrayList<RealVector>();
			_activeSet = new ArrayList<List<Integer>>();
			_stepSizes = new ArrayList<Double>();
			_updateVectors = new ArrayList<RealVector>();
			_absMaxCorrs = new ArrayList<Double>();
		}

		public List<RealVector> weights() {
			return new ArrayList<RealVector>(_weights);
		}

		public RealVector weights(int index) {
			return _weights.get(index);
		}

		public double l1norm(int index) {
			return _weights.get(index).getL1Norm();
		}

		public List<List<Integer>> activeSets() {
			return new ArrayList<List<Integer>>(_activeSet);
		}

		public List<Integer> activeSet(int index) {
			return _activeSet.get(index);
		}

		public List<Double> stepSizes() {
			return new ArrayList<Double>(_stepSizes);
		}

		public int findIndexByLambda(double lambda) {
			for (int i = 0; i < length(); i++) {
				if (lambda > absMaxCorr(i)) {
					return i - 1;
				}
			}
			return length() - 1;
		}

		public int findIndexByMaxL1(double maxL1) {
			for (int i = 0; i < length(); i++) {
				if (maxL1 < l1norm(i)) {
					return i - 1;
				}
			}
			return length() - 1;
		}

		public List<RealVector> updateVectors() {
			return new ArrayList<RealVector>(_updateVectors);
		}

		public List<Double> absMaxCorrs() {
			return new ArrayList<Double>(_absMaxCorrs);
		}

		public double absMaxCorr(int index) {
			return _absMaxCorrs.get(index);
		}

		/**
		 * Returns the number of iterations in this path.
		 * 
		 * @return the length of this path
		 */
		public int length() {
			return _weights.size();
		}

		private void append(RealVector w, List<Integer> activeSet,
				double stepSize, RealVector updateVector, double absMaxCorr) {
			_weights.add(w);
			List<Integer> aset = new ArrayList<Integer>(activeSet);
			_activeSet.add(aset);

			_stepSizes.add(stepSize);
			_updateVectors.add(updateVector);
			_absMaxCorrs.add(absMaxCorr);
		}
	}

	private int _inputSize;
	private RealVector _w;
	private List<Integer> _nonzero;
	private ZScore _zscore;

	private int _maxNonzero;
	private double _lambda;
	private double _maxL1;

	/**
	 * Constructs a new LARS instance. The number of nonzero weight vectors is
	 * entirely controlled by the choice of <code>lambda</code>.
	 * 
	 * @param inputSize
	 *            the number of components in a valid input vector
	 * @param lambda
	 *            a regularization parameter (must be non-negative). This
	 *            parameter has no effect when set to 0 and it's effect
	 *            increases as its size increases.
	 */
	public LARS(int inputSize, double lambda) {
		this(inputSize, inputSize, lambda, Double.POSITIVE_INFINITY);
	}

	/**
	 * Constructs a new LARS instance.
	 * 
	 * @param inputSize
	 *            the number of components in a valid input vector
	 * @param maxNonzero
	 *            the maximum number of nonzero weights in the final solution
	 * @param lambda
	 *            a regularization parameter (must be non-negative). This
	 *            parameter has no effect when set to 0 and it's effect
	 *            increases as its size increases.
	 * @param maxL1
	 *            the maximum L1-norm of the weights (must be non-negative).
	 *            This parameter has no effect when set to
	 *            {@link Double#POSITIVE_INFINITY} and it's effect increases as
	 *            its size shrinks to 0.
	 */
	public LARS(int inputSize, int maxNonzero, double lambda, double maxL1) {
		if (inputSize < 1) {
			throw new IllegalArgumentException(
					"Dimension of the input must be a positive integer.");
		}
		_inputSize = inputSize;

		if (maxNonzero < 0 || maxNonzero > inputSize) {
			throw new IllegalArgumentException(
					"maxNonzero must be in [0, inputSize=" + inputSize
							+ "]. Received : " + maxNonzero);
		}
		_maxNonzero = maxNonzero;

		if (lambda < 0) {
			throw new IllegalArgumentException(
					"Regularization parameter lambda cannot be negative. Received : "
							+ lambda);
		}
		_lambda = lambda;

		if (maxL1 < 0) {
			throw new IllegalArgumentException(
					"The maxL1 parameter cannot be negative. Received : "
							+ maxL1);
		}
		_maxL1 = maxL1;

		reset();
	}

	/**
	 * A copy constructor for LARS instances.
	 * 
	 * @param other
	 *            another LARS instance to copy
	 */
	public LARS(LARS other) {
		_inputSize = other._inputSize;
		if (other._w != null) {
			_w = new ArrayRealVector(other._w);
		}
		if (other._nonzero != null) {
			_nonzero = new ArrayList<Integer>(other._nonzero);
		}
		_zscore = other._zscore;
		_maxNonzero = other._maxNonzero;
		_lambda = other._lambda;
		_maxL1 = other._maxL1;
	}

	/**
	 * Returns the value of the regularization parameter.
	 * 
	 * @return the value of the regularization parameter
	 */
	public double lambda() {
		return _lambda;
	}

	/**
	 * Returns the maximum acceptable L1-norm of the selected weight vector.
	 * 
	 * @return the maximum acceptable L1-norm of a solution
	 */
	public double maxL1() {
		return _maxL1;
	}

	/**
	 * Returns the maximum number of nonzero weights in the selected weight
	 * vector.
	 * 
	 * @return the maximum number of nonzero weights in a solution
	 */
	public double maxNonzero() {
		return _maxNonzero;
	}

	/**
	 * Returns the number of elements in a valid input vector.
	 * 
	 * @return the dimension of valid input vectors
	 */
	public int inputDimension() {
		return _inputSize;
	}

	@Override
	public void reset() {
		_w = new ArrayRealVector(_inputSize, 0.0);
		_zscore = null;
		_nonzero = new ArrayList<Integer>();
	}

	@Override
	public Double evaluate(RealVector x) throws IllegalArgumentException {
		if (_zscore == null) {
			throw new IllegalStateException(
					"Cannot evaluate points before training.");
		}
		RealVector nx = _zscore.normalizeInput(x);
		double dp = sparseDotProduct(nx, _w, _nonzero);
		return _zscore.unnormalizeOutput(dp);
	}

	/**
	 * Sparse computation of the dot product over an "active set" of indices.
	 * 
	 * @param x
	 *            a vector
	 * @param y
	 *            a vector
	 * @param nonzero
	 *            the set of indices to compute the dot product over
	 * @return the dot product
	 */
	private double sparseDotProduct(RealVector x, RealVector y,
			Collection<Integer> nonzero) {
		double sum = 0;
		for (Integer i : nonzero) {
			sum += x.getEntry(i) * y.getEntry(i);
		}
		return sum;
	}

	@Override
	public LARS newInstance() {
		return new LARS(_inputSize, _maxNonzero, _lambda, _maxL1);
	}

	/**
	 * Constructs an instance of LARS for each element along the specified LARS
	 * path.
	 * 
	 * @param path
	 *            a LARSPath instance generated by this instance
	 * @return a list of LARS instances (one instance for each node along the
	 *         LARSPath instance)
	 */
	public List<LARS> fromLARSPath(LARSPath path) {
		List<LARS> lars = new ArrayList<LARS>(path.length());
		for (int i = 0; i < path.length(); i++) {
			LARS l = new LARS(_inputSize, i + 1, _lambda, _maxL1);
			l._nonzero = path.activeSet(i);
			l._w = path.weights(i);
			l._zscore = this._zscore;
			lars.add(l);
		}
		return lars;
	}

	@Override
	public void train(Collection<Pair<RealVector, Double>> trainingSet) {
		reset();

		LARSPath path = lar(trainingSet);

		// Enforce that we have at most _maxVars variables
		int index = Math.min(_maxNonzero - 1, path.length() - 1);
		// Enforce the maximum L1-norm on the weights
		index = Math.min(index, path.findIndexByMaxL1(_maxL1));
		// Enforce the regularization constraint
		index = Math.min(index, path.findIndexByLambda(_lambda));

		if (index < 0) {
			_w = new ArrayRealVector(_inputSize, 0.0);
			_nonzero = new ArrayList<Integer>();
		} else {
			_w = path.weights(index);
			_nonzero = path.activeSet(index);
		}
	}

	/**
	 * Computes and returns the full LARS path given a training set.
	 * 
	 * @param trainingSet
	 *            a collection of pairs of input vectors and output targets
	 * @return the LARS path
	 */
	public LARSPath lar(Collection<Pair<RealVector, Double>> trainingSet) {

		/*
		 * Normalize the input and output so that they have zero mean and unit
		 * standard deviation
		 */
		_zscore = new ZScore(trainingSet);
		RealMatrix xmat = _zscore.nX();
		RealVector yvec = _zscore.nY();

		int numSamples = trainingSet.size();
		LARSPath path = new LARSPath();

		// Start with zero weights
		RealVector w = new ArrayRealVector(_inputSize, 0.0);
		// Initially the active set is null
		List<Integer> activeSet = new ArrayList<Integer>();

		/*
		 * Do LARS iterations until we have incorporated all of the input
		 * variables or used all of the training data. If we have less training
		 * samples than the number of inputs, then we can incorporate at most
		 * (n-1) non-zero weights, where n is the number of training samples.
		 */
		int maxIters = Math.min(_inputSize, numSamples - 1);
		maxIters = Math.min(maxIters, _maxNonzero);
		for (int i = 0; i < maxIters; i++) {

			RealVector responses = null;
			if (i == 0) {
				// Initial responses are all zeros because the weights are all
				// zero
				responses = new ArrayRealVector(numSamples, 0.0);

			} else {
				// Compute the responses given the current weights and active
				// set
				responses = responses(xmat, w, activeSet);
			}

			// Compute the residuals
			RealVector residuals = yvec.subtract(responses);
			// Compute the correlations of the input components with the
			// residual
			RealVector corrs = xmat.transpose().operate(residuals);
			// Find the absolute max. correlation
			double absMaxC = absMax(corrs);
			// Find the active set (the set of components with maximum abs.
			// correlation)
			activeSet = updateActiveSet(corrs, absMaxC, activeSet);

			LARSUpdate larsUpdate = computeUpdate(xmat, residuals, activeSet,
					absMaxC, corrs);
			RealVector wDelta = larsUpdate.updateVector
					.mapMultiply(larsUpdate.gamma);
			w = w.add(wDelta);
			path.append(w, activeSet, larsUpdate.gamma,
					larsUpdate.updateVector, absMaxC);
		}

		return path;
	}

	/**
	 * Computes the responses for a matrix where each row corresponds to an
	 * input vector.
	 * 
	 * @param xmat
	 *            a matrix where each row corresponds to an input vector
	 * @param weights
	 *            the weights that will be multiplied with each input vector
	 * @param activeSet
	 *            the current set of nonzero weights
	 * @return a vector with a response for each row of <code>xmat</code>
	 */
	public RealVector responses(RealMatrix xmat, RealVector weights,
			List<Integer> activeSet) {
		RealVector response = new ArrayRealVector(xmat.getRowDimension());
		for (int r = 0; r < xmat.getRowDimension(); r++) {
			RealVector x = new ArrayRealVector(xmat.getRow(r));
			double out = sparseDotProduct(x, weights, activeSet);
			response.setEntry(r, out);
		}
		return response;
	}

	/**
	 * Returns the maximum of the absolute value of a vector's components.
	 * 
	 * @param x
	 *            a vector
	 * @return the maximum of the absolute value of <code>x</code>'s components
	 */
	private double absMax(RealVector x) {
		double max = x.getEntry(0);
		for (int i = 1; i < x.getDimension(); i++) {
			double xi = x.getEntry(i);
			double absxi = Math.abs(xi);
			if (absxi > max) {
				max = absxi;
			}
		}
		return max;
	}

	/**
	 * Updates the active set by adding the index of the component with the
	 * highest correlation.
	 * 
	 * @param c
	 *            a vector of correlations
	 * @param absMaxC
	 *            the maximum of the absolute value of c's components
	 * @param prevActiveSet
	 *            the previous active set
	 * @return a new active set containing all of the indices from
	 *         <code>prevActiveSet</code> plus any components with maximum
	 *         absolute correlation
	 */
	private List<Integer> updateActiveSet(RealVector c, double absMaxC,
			List<Integer> prevActiveSet) {
		List<Integer> activeSet = new ArrayList<Integer>(prevActiveSet);
		int size = c.getDimension();
		for (int i = 0; i < size; i++) {
			double absC = Math.abs(c.getEntry(i));
			if (Precision.equals(absC, absMaxC, tmrutil.math.Constants.EPSILON)) {
				if (!activeSet.contains(i)) {
					activeSet.add(i);
				}
			}
		}

		return activeSet;
	}

	/**
	 * Returns a submatrix containing the columns of <code>xmat</code> in
	 * <code>activeSet</code>.
	 * 
	 * @param xmat
	 *            a matrix whose rows are valid input vectors
	 * @param activeSet
	 *            the current active set
	 * @return a submatrix of <code>xmat</code> containing the columns specified
	 *         by the indices in <code>activeSet</code>
	 */
	private RealMatrix XActiveSet(RealMatrix xmat, List<Integer> activeSet) {
		RealMatrix xamat = new Array2DRowRealMatrix(xmat.getRowDimension(),
				activeSet.size());

		for (int r = 0; r < xmat.getRowDimension(); r++) {
			for (int c = 0; c < activeSet.size(); c++) {
				int ac = activeSet.get(c);
				xamat.setEntry(r, c, xmat.getEntry(r, ac));
			}
		}

		return xamat;
	}

	/**
	 * Returns a vector of sign of each element whose index is specified by
	 * <code>activeSet</code>
	 * 
	 * @param v
	 *            a vector
	 * @param activeSet
	 *            the active set (which determines which elements of
	 *            <code>v</code> to compute the sign for)
	 * @return a vector with <code>activeSet.size()</code> elements containing
	 *         the signs of the corresponding elements in <code>v</code>
	 */
	private RealVector sign(RealVector v, List<Integer> activeSet) {
		RealVector s = new ArrayRealVector(activeSet.size(), 0);
		for (int i = 0; i < activeSet.size(); i++) {
			s.setEntry(i, Math.signum(v.getEntry(activeSet.get(i))));
		}
		return s;
	}

	private RealMatrix invert(RealMatrix mat) {
		SingularValueDecomposition decomp = new SingularValueDecomposition(mat);
		DecompositionSolver solver = decomp.getSolver();
		return solver.getInverse();
	}

	private double AActive(RealMatrix invGActive, RealVector signs) {
		RealVector signsInvGA = invGActive.preMultiply(signs);
		double val = signsInvGA.dotProduct(signs);
		double AActive = 1 / Math.sqrt(val);
		return AActive;
	}

	/**
	 * Computes the step size used for update the function approximation
	 * weights.
	 * 
	 * @param absMaxC
	 *            the maximum absolute correlation with the residuals
	 * @param corrs
	 *            the correlations of each component with the residuals
	 * @param AActive
	 * @param avec
	 * @param activeSetComplement
	 *            the complement of the active set
	 * @return the step size to multiply the equiangular update vector by
	 */
	private double computeStepSize(double absMaxC, RealVector corrs,
			double AActive, RealVector avec, List<Integer> activeSetComplement) {
		Double gamma = null;
		for (Integer i : activeSetComplement) {
			double ci = corrs.getEntry(i);
			double ai = avec.getEntry(i);
			double gA = (absMaxC - ci) / (AActive - ai);
			double gB = (absMaxC + ci) / (AActive + ai);

			double g = 0;
			if (gA <= 0 && gB <= 0) {
				continue;
			} else {
				if (gA < gB && gA > 0) {
					g = gA;
				} else {
					g = gB;
				}
			}

			if (gamma == null || g < gamma) {
				gamma = g;
			}
		}
		if (gamma == null) {
			return 0;
		}
		return gamma;
	}

	/**
	 * Expands a vector that only has <code>activeSet.size()</code> components
	 * to the original input dimensions.
	 * 
	 * @param v
	 *            a vector with dimension <code>activeSet.size()</code>
	 * @param activeSet
	 *            the active set
	 * @return a vector whose dimension is the same as a valid input vector
	 */
	private RealVector expand(RealVector v, List<Integer> activeSet) {
		RealVector ev = new ArrayRealVector(_inputSize, 0.0);

		for (int i = 0; i < activeSet.size(); i++) {
			ev.setEntry(activeSet.get(i), v.getEntry(i));
		}

		return ev;
	}

	/**
	 * Returns the complement of the active set.
	 * 
	 * @param activeSet
	 *            the active set indices for nonzero weights
	 * @return the complement of <code>activeSet</code>
	 */
	private List<Integer> complement(List<Integer> activeSet) {
		List<Integer> comp = new ArrayList<Integer>();
		for (int i = 0; i < _inputSize; i++) {
			if (!activeSet.contains(i)) {
				comp.add(i);
			}
		}
		return comp;
	}

	/**
	 * Computes the quantities used in a LARS update. This method does the main
	 * work of the LARS algorithm.
	 * 
	 * @param xmat
	 *            a matrix whose rows are valid input vectors
	 * @param residuals
	 *            the residuals given the current weights
	 * @param activeSet
	 *            the set of nonzero weights
	 * @param absMaxC
	 *            the maximum absolute correlation
	 * @param corrs
	 *            the current correlations between the input components and the
	 *            residuals
	 * @return a LARS update instance containing the equiangular update vector
	 *         and the step size gamma
	 */
	private LARSUpdate computeUpdate(RealMatrix xmat, RealVector residuals,
			List<Integer> activeSet, double absMaxC, RealVector corrs) {
		LARSUpdate larsUpdate = new LARSUpdate();

		RealMatrix XActive = XActiveSet(xmat, activeSet);
		RealMatrix XActiveT = XActive.transpose();
		RealMatrix gramActive = XActiveT.multiply(XActive);
		RealMatrix invGramActive = invert(gramActive);
		RealVector signs = sign(corrs, activeSet);

		double AActive = AActive(invGramActive, signs);

		RealVector delta = invGramActive.operate(signs).mapMultiply(AActive);

		RealVector avec = xmat.transpose().operate(XActive.operate(delta));
		larsUpdate.gamma = computeStepSize(absMaxC, corrs, AActive, avec,
				complement(activeSet));

		larsUpdate.updateVector = expand(delta, activeSet);
		return larsUpdate;
	}

	/**
	 * A LARS update containing the quantities needed to update the weights.
	 * 
	 * @author Timothy A. Mann
	 *
	 */
	private class LARSUpdate {
		/**
		 * The step size to apply to the udpate vector.
		 */
		public double gamma;
		/**
		 * The equiangular update vector.
		 */
		public RealVector updateVector;
	}
}
