package tmrutil.threads;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains a set of threads which can be used to execute tasks.
 * @author Timothy Mann
 *
 */
public class TaskManager
{
	class TaskRunner implements Runnable
	{
		private boolean _stop;
		private Thread _thread;
		private Task _task;
		
		public TaskRunner()
		{
			_stop = false;
			_thread = new Thread(this);
			_task = null;
		}
		
		public void setTask(Task task){
			_task = task;
		}
		
		public void run()
		{
			while(!_stop){
				if(_task != null && !_task.isDone()){
					_task.execute();
				}
			}
		}
		
		public void start()
		{
			_thread.start();
		}
		
		public void stop()
		{
			if(_task != null && !_task.isDone()){
				_task.stop();
			}
			_stop = true;
		}
	}
	
	private static final int DEFAULT_NUM_THREADS = 10;
	
	/**
	 * A set of active threads.
	 */
	private List<TaskRunner> _busyThreads;
	private List<TaskRunner> _idleThreads;

	/**
	 * Constructs a task manager with the default number of threads.
	 */
	public TaskManager(){
		_idleThreads = new ArrayList<TaskRunner>(DEFAULT_NUM_THREADS);
		_busyThreads = new ArrayList<TaskRunner>(DEFAULT_NUM_THREADS);
		for(int i=0;i<DEFAULT_NUM_THREADS;i++){
			TaskRunner tr = new TaskRunner();
			tr.start();
			_idleThreads.add(tr);
		}
	}
	
	public void request(Task task)
	{
		if(_idleThreads.size() > 0){
			TaskRunner tr = _idleThreads.remove(0);
			tr.setTask(task);
			_busyThreads.add(tr);
		}else{
			TaskRunner tr = new TaskRunner();
			tr.start();
			tr.setTask(task);
			_busyThreads.add(tr);
		}
	}
}
