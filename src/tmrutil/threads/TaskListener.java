package tmrutil.threads;

/**
 * A task listener is used to detect events related to tasks that run in
 * seperate threads of execution.
 * 
 * @author Timothy Mann
 * 
 */
public interface TaskListener
{
	/**
	 * Called when the task finishes execution or is stopped.
	 * 
	 * @param task
	 *            a task
	 */
	public void taskFinished(Task task);
}
