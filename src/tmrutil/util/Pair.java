/*
 * Pair.java
 */

package tmrutil.util;

import java.io.Serializable;

/**
 * A pair is a data structure that contains references to two objects.
 */
public class Pair<A, B> implements Serializable
{
	private static final long serialVersionUID = -3142847621670056523L;

	private A _a;
	private B _b;

	/**
	 * Constructs a pair containing two objects.
	 * @param a an object of type <code>A</code>
	 * @param b an object of type <code>B</code>
	 */
	public Pair(A a, B b)
	{
		_a = a;
		_b = b;
	}

	/**
	 * Returns a reference to the first object in this pair.
	 * @return an object of type <code>A</code>
	 */
	public final A getA()
	{
		return _a;
	}

	/**
	 * Sets the first object in this pair.
	 * @param a an object of type <code>A</code>
	 */
	public final void setA(A a)
	{
		_a = a;
	}

	/**
	 * Returns a reference to the second object in this pair.
	 * @return an object of type <code>B</code>
	 */
	public final B getB()
	{
		return _b;
	}

	/**
	 * Sets the second object in this pair.
	 * @param b an object of type <code>B</code>
	 */
	public final void setB(B b)
	{
		_b = b;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Pair<?, ?>) {
			Pair<?, ?> pair = (Pair<?, ?>) obj;
			return (_a.equals(pair.getA()) && _b.equals(pair.getB()));
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return _a.hashCode() + _b.hashCode();
	}
	
	@Override
	public String toString()
	{
		return "[" + getA() + ", " + getB() + "]";
	}
}