package tmrutil.util;

import java.util.Arrays;
import tmrutil.math.VectorOps;
import tmrutil.stats.Statistics;

/**
 * Useful operations on arrays that are missing from {@link java.util.Arrays}.
 * 
 * @author Timothy A. Mann
 * 
 */
public final class ArrayOps {
	/**
	 * Constructs a new array of length <code>a.length + b.length</code> and
	 * copies the elements of <code>a</code> followed by <code>b</code> into the
	 * new array. The newly constructed array is returned.
	 * 
	 * @param a
	 *            an array of doubles
	 * @param b
	 *            an array of doubles
	 * @return the concatenation of <code>a</code> and <code>b</code>
	 */
	public static final double[] concat(double[] a, double[] b) {
		double[] c = new double[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		return c;
	}

	/**
	 * Constructs a new array of length <code>a.length + b.length</code> and
	 * copies the elements of <code>a</code> followed by <code>b</code> into the
	 * new array. The newly constructed array is returned.
	 * 
	 * @param <T>
	 *            the type of data stored by the arrays
	 * @param a
	 *            an array of type <code>T</code>
	 * @param b
	 *            an array of type <code>T</code>
	 * @return the concatenation of <code>a</code> and <code>b</code>
	 */
	public static final <T> T[] concat(T[] a, T[] b) {
		T[] c = java.util.Arrays.copyOf(a, a.length + b.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		return c;
	}

	/**
	 * Appends the element <code>b</code> to the end of a copy of array
	 * <code>a</code>.
	 * 
	 * @param a
	 *            an array of doubles
	 * @param b
	 *            a scalar value
	 * @return a copy of array <code>a</code> with the value <code>b</code>
	 *         appended
	 */
	public static final double[] append(double[] a, double b) {
		double[] c = Arrays.copyOf(a, a.length + 1);
		c[a.length] = b;
		return c;
	}

	/**
	 * Appends the element <code>b</code> to the end of a copy of array
	 * <code>a</code>.
	 * 
	 * @param <T>
	 *            the type of data stored by the array
	 * @param a
	 *            an array of type <code>T</code>
	 * @param b
	 *            an element of type <code>T</code>
	 * @return a copy of array <code>a</code> with the value <code>b</code>
	 *         appended
	 */
	public static final <T> T[] append(T[] a, T b) {
		T[] c = Arrays.copyOf(a, a.length + 1);
		c[a.length] = b;
		return c;
	}

	/**
	 * Prefixes a copy of array <code>b</code> with element <code>a</code>.
	 * 
	 * @param a
	 *            a scalar value
	 * @param b
	 *            an array of doubles
	 * @return a copy of array <code>b</code> with the value <code>a</code>
	 *         prefixed
	 */
	public static final double[] prefix(double a, double[] b) {
		double[] c = new double[b.length + 1];
		System.arraycopy(b, 0, c, 1, b.length);
		c[0] = a;
		return c;
	}

	/**
	 * Prefixes a copy of array <code>b</code> with element <code>a</code>.
	 * 
	 * @param <T>
	 *            the type of data stored by the array
	 * @param a
	 *            an element of type <code>T</code>
	 * @param b
	 *            an array of type <code>T</code>
	 * @return a copy of array <code>b</code> with the value <code>a</code>
	 *         prefixed
	 */
	public static final <T> T[] prefix(T a, T[] b) {
		T[] c = Arrays.copyOf(b, b.length + 1);
		System.arraycopy(b, 0, c, 1, b.length);
		c[0] = a;
		return c;
	}

	/**
	 * Encodes a sequence of positive integer values into a single integer.
	 * 
	 * @param values
	 *            a sequence of integer values
	 * @param upperLimits
	 *            the upper limits on the integer values that can be the values
	 *            array (maxValues + 1)
	 * @return an encoding of the <code>values</code> array as a single integer
	 */
	public static final int encode(int[] values, int[] upperLimits) {
		int sum = 0;
		int prod = Statistics.product(upperLimits);
		for (int i = 0; i < values.length; i++) {
			prod = prod / upperLimits[i];
			sum = sum + values[i] * prod;
		}
		return sum;
	}

	/**
	 * Decodes a single integer into a sequence of positive integers given an
	 * array specifying the maximum integer value at each component of the
	 * sequence.
	 * 
	 * @param value
	 *            a positive integer value
	 * @param upperLimits
	 *            the upper bound on the limits of the values in the returned
	 *            decoded array. The upper limits represent the (maxValues+1).
	 * @return an array of integers decoded from the specified single integer
	 */
	public static final int[] decode(int value, int[] upperLimits) {
		int[] values = new int[upperLimits.length];
		int prod = Statistics.product(upperLimits);
		for (int i = 0; i < values.length; i++) {
			prod = prod / upperLimits[i];
			values[i] = value / prod;
			value = value % prod;
		}
		return values;
	}

	/**
	 * Private constructor so that it cannot be constructed.
	 */
	private ArrayOps() {
	}

	public static void main(String[] args) {
		int[] vals = { 2, 3, 1, 4 };
		int[] maxVals = { 4, 5, 6, 7 };
		int n = encode(vals, maxVals);
		int[] v = decode(n, maxVals);
		System.out.println(n);
		System.out.println(VectorOps.toString(v));
	}
}
