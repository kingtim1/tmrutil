package tmrutil.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Utility functions for reading and writing Comma Separated Value (CSV) files.
 * 
 * @author Timothy A. Mann
 * 
 */
public class CSVUtil
{
	public static final String COMMA = ",";
	
	/**
	 * Takes a map from attributes to string data elements and tries to parse those string elements to double floating point values.
	 * @param stringDataMap a map of attributes to string data
	 * @return a map of attributes to numerical data
	 */
	public static final Map<String, List<Double>> parseDouble(Map<String,List<String>> stringDataMap)
	{
		Map<String,List<Double>> doubleDataMap = new HashMap<String,List<Double>>();
		
		Set<String> keys = stringDataMap.keySet();
		for(String key : keys){
			List<String> stringData = stringDataMap.get(key);
			List<Double> data = new ArrayList<Double>(stringData.size());
			for(String element : stringData){
				double value = Double.parseDouble(element);
				data.add(value);
			}
			doubleDataMap.put(key, data);
		}
		
		return doubleDataMap;
	}

	/**
	 * Imports data from a comma separated value file as string data. This
	 * function assumes that the first column contains attribute names for the
	 * data and that all other columns contain string data.
	 * 
	 * @param filename the path to a CSV file to import data from
	 * @return a map of attribute names to string data
	 * @throws IOException if an I/O error occurs while reading from the CSV file
	 */
	public static final Map<String, List<String>> importRowsFromCSV(
			String filename) throws IOException
	{
		Map<String, List<String>> dataMap = new HashMap<String, List<String>>();

		// Open CSV file for reading
		BufferedReader fread = new BufferedReader(new FileReader(filename));

		String line = null;
		while ((line = fread.readLine()) != null) {
			String[] tokens = line.split(COMMA);
			String attribute = prepareAttribute(tokens[0]);
			List<String> data = new ArrayList<String>(tokens.length - 1);
			for (int i = 1; i < tokens.length; i++) {
				data.add(tokens[i]);
			}
			dataMap.put(attribute, data);
		}

		fread.close();
		return dataMap;
	}

	/**
	 * Imports data from a comma separated value file as string data. This
	 * function assumes that the first row contains attribute names for the data
	 * and that all other rows contain string data.
	 * 
	 * @param filename
	 *            the path to a CSV file to import data from
	 * @return a map of attribute names to string data
	 * @throws IOException
	 *             if an I/O error occurs while reading from the CSV file
	 */
	public static final Map<String, List<String>> importColumnsFromCSV(
			String filename) throws IOException
	{
		Map<String, List<String>> dataMap = new HashMap<String, List<String>>();

		// Open the CSV file for reading
		BufferedReader fread = new BufferedReader(new FileReader(filename));

		boolean readAttributes = false;
		String[] attributes = null;
		String line = null;

		while ((line = fread.readLine()) != null) {
			String[] tokens = line.split(COMMA);
			if (!readAttributes) {
				// Read the attribute names
				attributes = prepareAttributes(tokens);
				for (String key : attributes) {
					dataMap.put(key, new ArrayList<String>());
				}
				readAttributes = true;
			} else {
				for (int i = 0; i < tokens.length; i++) {
					dataMap.get(attributes[i]).add(tokens[i]);
				}
			}
		}

		fread.close();
		return dataMap;
	}

	/**
	 * Prepares an array of string as attributes.
	 * @param strs an array of strings
	 * @return an array of prepared strings
	 */
	public static final String[] prepareAttributes(String[] strs)
	{
		String[] attributes = new String[strs.length];
		for (int i = 0; i < strs.length; i++) {
			attributes[i] = prepareAttribute(strs[i]);
		}
		return attributes;
	}

	/**
	 * Prepares an entry in a CSV file to be used as an attribute by removing extra whitespace and quotation marks.
	 * @param str a string
	 * @return a string with leading and trailing whitespace trimmed and quotation marks removed
	 */
	public static final String prepareAttribute(String str)
	{
		String attribute = str.trim().replace("\"", "");
		return attribute;
	}
}
