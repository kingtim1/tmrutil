package tmrutil.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import tmrutil.math.DimensionMismatchException;
import tmrutil.math.Metric;
import tmrutil.math.VectorOps;
import tmrutil.math.metrics.EuclideanDistance;
import tmrutil.math.metrics.SquaredDistance;

/**
 * Ipmlements the k-dimensional tree construction and searching algorithms.
 * 
 * @author Timothy Mann
 */
public class KDTree<V> implements Iterable<Pair<double[], V>>
{

	private static class KDLeaf<V> extends KDNode<V>
	{
		private Pair<double[], V> _pair;

		KDLeaf(Pair<double[], V> pair, int depth)
		{
			super(depth);
			_pair = pair;
		}

		double[] getKey()
		{
			return _pair.getA();
		}

		public V getValue()
		{
			return _pair.getB();
		}

		public Pair<double[], V> getPair()
		{
			return _pair;
		}

		@Override
		public boolean isLeaf()
		{
			return true;
		}
	}

	private static class KDInternal<V> extends KDNode<V>
	{
		private double _split;
		private int _axis;
		private KDNode<V> _leftChild;
		private KDNode<V> _rightChild;

		public KDInternal(double split, int axis, int depth)
		{
			super(depth);
			_split = split;
			_axis = axis;
			_leftChild = null;
			_rightChild = null;
		}

		public boolean isLeft(double[] key)
		{
			return (key[_axis] < _split);
		}

		public boolean isLeft(KDNode<V> node)
		{
			if (node.isLeaf()) {
				KDLeaf<V> leaf = (KDLeaf<V>) node;
				return isLeft(leaf.getKey());
			} else {
				KDInternal<V> inode = (KDInternal<V>) node;
				return (inode._split < _split);
			}
		}

		public boolean isRight(double[] key)
		{
			return (key[_axis] >= _split);
		}

		public boolean isRight(KDNode<V> node)
		{
			if (node.isLeaf()) {
				KDLeaf<V> leaf = (KDLeaf<V>) node;
				return isRight(leaf.getKey());
			} else {
				KDInternal<V> inode = (KDInternal<V>) node;
				return (inode._split >= _split);
			}
		}

		public KDNode<V> getLeftChild()
		{
			return _leftChild;
		}

		public KDNode<V> getRightChild()
		{
			return _rightChild;
		}

		public void setLeftChild(KDNode<V> child)
		{
			child._parent = this;
			_leftChild = child;
		}

		public void setRightChild(KDNode<V> child)
		{
			child._parent = this;
			_rightChild = child;
		}

		@Override
		public boolean isLeaf()
		{
			return false;
		}
	}

	private static abstract class KDNode<V>
	{
		protected int _depth;
		protected KDInternal<V> _parent;

		KDNode(int depth)
		{
			_depth = depth;
			_parent = null;
		}

		public int getDepth()
		{
			return _depth;
		}

		public KDInternal<V> getParent()
		{
			return _parent;
		}

		public abstract boolean isLeaf();
	}

	/**
	 * The root of the kd-tree.
	 */
	private KDNode<V> _root;
	/**
	 * The dimension of the kd-tree elements.
	 */
	private int _k;

	/**
	 * The number of leaves of the kd-tree.
	 */
	private int _size;

	/**
	 * The metric used for comparing vectors.
	 */
	private Metric<double[]> _metric;

	/**
	 * Constructs an empty kd-tree.
	 * 
	 * @param k
	 *            the dimension of the points
	 */
	public KDTree(int k)
	{
		if (k < 1) {
			throw new IllegalArgumentException(
					"k must be greater than 0 : k = " + k);
		}
		_metric = new SquaredDistance();
		_k = k;
		_size = 0;
		_root = null;
	}

	/**
	 * Constructs a kd-tree.
	 * 
	 * @param points
	 *            a set of points
	 * @param k
	 *            the dimension of the points
	 * @throws IllegalArgumentException
	 *             if the list of <code>points</code> is empty
	 */
	public KDTree(List<Pair<double[], V>> points, int k)
			throws IllegalArgumentException
	{
		if (k < 1) {
			throw new IllegalArgumentException(
					"k must be greater than 0 : k = " + k);
		}
		_metric = new SquaredDistance();
		_k = k;
		_size = points.size();
		_root = buildTree(points, 0);
	}

	/**
	 * Constructs a kd-tree from a set of points and the current depth.
	 * 
	 * @param points
	 *            a set of points
	 * @param depth
	 *            the current depth
	 * @return the root of the constructed kd-tree
	 */
	private KDNode<V> buildTree(List<Pair<double[], V>> points, int depth)
	{
		if (points.size() == 0) {
			return null;
		} else if (points.size() == 1) {
			KDLeaf<V> leaf = new KDLeaf<V>(points.get(0), depth);
			return leaf;
		} else {
			int axis = depth % _k;
			List<Pair<double[], V>> left = new ArrayList<Pair<double[], V>>(
					points.size() / 2);
			List<Pair<double[], V>> right = new ArrayList<Pair<double[], V>>(
					points.size() / 2);
			int medianI = sortOnAxis(points, axis, left, right);
			KDInternal<V> node = new KDInternal<V>(
					points.get(medianI).getA()[axis], axis, depth);
			node.setLeftChild(buildTree(left, depth + 1));
			node.setRightChild(buildTree(right, depth + 1));
			return node;
		}
	}

	/**
	 * Sorts the list of points on the specified axis and returns the index of
	 * the median point.
	 * 
	 * @param points
	 *            a list of points
	 * @param axis
	 *            the axis to sort on
	 * @return the index of the median point
	 */
	private int sortOnAxis(List<Pair<double[], V>> points, int axis,
			List<Pair<double[], V>> left, List<Pair<double[], V>> right)
	{
		double[] vals = new double[points.size()];
		for (int i = 0; i < points.size(); i++) {
			vals[i] = points.get(i).getA()[axis];
		}
		int[] inds = Sort.sortIndex(vals, true);
		int medianI = inds.length / 2;
		for (int i = 0; i < inds.length; i++) {
			if (i < medianI) {
				left.add(points.get(inds[i]));
			} else if (i >= medianI) {
				right.add(points.get(inds[i]));
			}
		}
		return inds[medianI];
	}

	/**
	 * Insert a pair into the kd-tree.
	 * 
	 * @param pair
	 *            containing a key and value
	 */
	public void insert(Pair<double[], V> pair)
	{
		if (_root == null) {
			_root = new KDLeaf<V>(pair, 0);
		} else {
			insert(pair, _root, 0);
		}
		_size++;
	}

	/**
	 * Insert a value into the kd-tree.
	 * 
	 * @param value
	 */
	public void insert(double[] key, V value)
	{
		Pair<double[], V> pair = new Pair<double[], V>(key, value);
		insert(pair);
	}

	/**
	 * Inserts a value into the kd-tree.
	 * 
	 * @param pair
	 *            a key-value pair
	 * @param node
	 *            the current node
	 * @param depth
	 *            the current depth
	 */
	private void insert(Pair<double[], V> pair, KDNode<V> node, int depth)
	{
		int axis = depth % _k;

		if (node.isLeaf()) {
			KDInternal<V> parent = node.getParent();
			KDLeaf<V> leaf = (KDLeaf<V>) node;
			double split = Math.max(leaf.getKey()[axis], pair.getA()[axis]);

			KDInternal<V> newParent = new KDInternal<V>(split, axis, depth + 1);
			if (parent == null) {
				_root = newParent;
			} else {
				if (parent.isLeft(leaf)) {
					parent.setLeftChild(newParent);
				} else {
					parent.setRightChild(newParent);
				}
			}

			if (newParent.isLeft(pair.getA())) {
				newParent.setLeftChild(new KDLeaf<V>(pair, depth + 1));
				newParent.setRightChild(leaf);
			} else {
				newParent.setLeftChild(leaf);
				newParent.setRightChild(new KDLeaf<V>(pair, depth + 1));
			}
		} else {
			KDInternal<V> inode = (KDInternal<V>) node;
			if (inode.isLeft(pair.getA())) {
				insert(pair, inode.getLeftChild(), depth + 1);
			} else {
				insert(pair, inode.getRightChild(), depth + 1);
			}
		}
	}

	/**
	 * Remove a value from the kd-tree.
	 * 
	 * @param key
	 *            the key to be removed
	 * @return true if the value was removed
	 */
	public boolean remove(double[] key)
	{
		KDNode<V> node = findNode(key);
		if (node != null) {
			KDLeaf<V> leaf = (KDLeaf<V>) node;
			List<Pair<double[], V>> points = getSubtreeValues(_root);
			boolean removed = points.remove(leaf.getPair());
			if (removed) {
				_root = buildTree(points, 0);
				_size--;
			}
			return removed;
		} else {
			return false;
		}
	}

	/**
	 * Returns an iterator over all of the elements in this kd-tree.
	 * 
	 * @return an iterator over all elements in this kd-tree
	 */
	public Iterator<Pair<double[], V>> iterator()
	{
		return getSubtreeValues(_root).iterator();
	}

	/**
	 * Finds the closest value to <code>t</code> stored in the kd-tree. If the
	 * tree is empty then <code>null</code> is returned.
	 * 
	 * @param key
	 *            a key
	 * @return the closest value to <code>t</code> if the tree is nonempty.
	 *         Otherwise return <code>null</code>.
	 */
	public Pair<double[], V> nearestNeighbor(double[] key)
	{
		if (_root == null) {
			return null;
		} else {
			return nearestNeighbor(key, _root);
		}
	}

	private Pair<double[], V> nearestNeighbor(double[] key, KDNode<V> node)
	{
		if (node.isLeaf()) {
			KDLeaf<V> leaf = (KDLeaf<V>) node;
			return leaf.getPair();
		} else {
			KDInternal<V> inode = (KDInternal<V>) node;
			int axis = inode.getDepth() % _k;
			KDNode<V> nearer = null;
			KDNode<V> further = null;
			if (inode.isLeft(key)) {
				nearer = inode.getLeftChild();
				further = inode.getRightChild();
			} else {
				nearer = inode.getRightChild();
				further = inode.getLeftChild();
			}

			Pair<double[], V> newVal = nearestNeighbor(key, nearer);
			double distSqd = _metric.distance(newVal.getA(), key);
			double dSqd = Math.pow(inode._split - key[axis], 2);
			if (dSqd < distSqd) {
				Pair<double[], V> tmpVal = nearestNeighbor(key, further);
				double tmpDist = _metric.distance(tmpVal.getA(), key);
				if (tmpDist < distSqd) {
					newVal = tmpVal;
				}
			}
			return newVal;
		}
	}

	private List<Pair<double[], V>> getSubtreeValues(KDNode<V> node)
	{
		List<Pair<double[], V>> points = new ArrayList<Pair<double[], V>>();
		return getSubtreeValues(node, points);
	}

	private List<Pair<double[], V>> getSubtreeValues(KDNode<V> node,
			List<Pair<double[], V>> points)
	{
		if (node != null) {
			if (node.isLeaf()) {
				KDLeaf<V> leaf = (KDLeaf<V>) node;
				points.add(new Pair<double[], V>(leaf.getKey(), leaf.getValue()));
			} else {
				KDInternal<V> inode = (KDInternal<V>) node;
				getSubtreeValues(inode.getLeftChild(), points);
				getSubtreeValues(inode.getRightChild(), points);
			}
		}
		return points;
	}

	/**
	 * Returns a list of all key-value pairs where the key of the pair is within
	 * <code>radius</code> of the specified <code>point</code>.
	 * 
	 * @param point
	 *            a point in k-dimensional space
	 * @param radius
	 *            a positive real value
	 * @return a list of all key-value pairs where the key is within the radius
	 *         of a point
	 */
	public List<Pair<double[], V>> withinRadius(double[] point, double radius)
			throws IllegalArgumentException, DimensionMismatchException
	{
		if (radius < 0) {
			throw new IllegalArgumentException(
					"Radius must be a positive value.");
		}
		if (point.length != _k) {
			throw new DimensionMismatchException(
					"The specified point does not match the dimension of the kd-tree : k("
							+ _k + ") != point(" + point.length + ")");
		}
		List<Pair<double[], V>> points = new ArrayList<Pair<double[], V>>();
		points = withinRadius(point, radius, _root, 0, points);
		return points;
	}

	private List<Pair<double[], V>> withinRadius(double[] point, double radius,
			KDNode<V> node, int depth, List<Pair<double[], V>> points)
	{
		if (node.isLeaf()) {
			double radius2 = radius * radius;
			KDLeaf<V> leaf = (KDLeaf<V>) node;
			if (_metric.distance(point, leaf.getKey()) < radius2) {
				points.add(leaf.getPair());
			}
			return points;
		} else {
			KDInternal<V> inode = (KDInternal<V>) node;
			int axis = inode.getDepth() % _k;
			KDNode<V> nearer = null;
			KDNode<V> further = null;
			if (inode.isLeft(point)) {
				nearer = inode.getLeftChild();
				further = inode.getRightChild();
			} else {
				nearer = inode.getRightChild();
				further = inode.getLeftChild();
			}

			points = withinRadius(point, radius, nearer, depth + 1, points);
			if (inode._split - point[axis] < radius) {
				points = withinRadius(point, radius, further, depth + 1, points);
			}
			return points;
		}
	}

	private KDNode<V> findNode(double[] key)
	{
		return findNode(key, _root, 0);
	}

	private KDNode<V> findNode(double[] key, KDNode<V> node, int depth)
	{
		if (node.isLeaf()) {
			KDLeaf<V> leaf = (KDLeaf<V>) node;
			if (Arrays.equals(leaf.getKey(), key)) {
				return leaf;
			} else {
				return null;
			}
		} else {
			KDInternal<V> inode = (KDInternal<V>) node;
			if (inode.isLeft(key)) {
				return findNode(key, inode.getLeftChild(), depth + 1);
			} else {
				return findNode(key, inode.getRightChild(), depth + 1);
			}
		}
	}

	/**
	 * Returns the number of leaf nodes in this kd-tree.
	 * 
	 * @return the number of leaf nodes
	 */
	public int size()
	{
		return _size;
	}

	public static Pair<double[], Integer> findClosest(Metric<double[]> metric,
			List<Pair<double[], Integer>> points, double[] val)
	{
		double minD = metric.distance(val, points.get(0).getA());
		int minI = 0;
		for (int i = 1; i < points.size(); i++) {
			double dist = metric.distance(val, points.get(i).getA());
			if (minD > dist) {
				minD = dist;
				minI = i;
			}
		}
		return points.get(minI);
	}

	public static List<Pair<double[], Integer>> findWithinRadius(Metric<double[]> metric,
			List<Pair<double[], Integer>> points, double[] val, double radius)
	{
		double radius2 = radius * radius;
		List<Pair<double[], Integer>> inSet = new ArrayList<Pair<double[], Integer>>();
		for (int i = 0; i < points.size(); i++) {
			if (metric.distance(points.get(i).getA(), val) < radius2) {
				inSet.add(points.get(i));
			}
		}
		return inSet;
	}

	public static void main(String[] args)
	{
		int maxVal = 10;
		int size = 1000;
		int tests = 600;
		List<Pair<double[], Integer>> points = new ArrayList<Pair<double[], Integer>>(
				size);
		for (int i = 0; i < size; i++) {
			points.add(new Pair<double[], Integer>(new double[] {
					tmrutil.stats.Random.uniform(0, maxVal),
					tmrutil.stats.Random.uniform(0, maxVal) }, 1));
		}
		EuclideanDistance edist = new EuclideanDistance();
		KDTree<Integer> kdtree = new KDTree<Integer>(points, 2);
		// for (Pair<double[], Integer> p : points) {
		// kdtree.insert(p);
		// }

		int errors = 0;
		int sumDist = 0;
		for (int i = 0; i < tests; i++) {
			double[] v = { tmrutil.stats.Random.uniform(0, maxVal),
					tmrutil.stats.Random.uniform(0, maxVal) };
			Pair<double[], Integer> kdnn = kdtree.nearestNeighbor(v);
			Pair<double[], Integer> slnn = findClosest(edist, points, v);
			sumDist += VectorOps.distance(kdnn.getA(), slnn.getA());
			if (!VectorOps.equal(kdnn.getA(), slnn.getA())) {
				double kddist = VectorOps.distance(v, kdnn.getA());
				double sldist = VectorOps.distance(v, slnn.getA());
				System.out.println("kddist(" + kddist + ") < sldist(" + sldist
						+ ") = " + (kddist < sldist));
				System.out.println("nn(" + VectorOps.toString(v)
						+ ") produced kdnn != slnn : "
						+ VectorOps.toString(kdnn.getA()) + " != "
						+ VectorOps.toString(slnn.getA()));
				errors++;
			}
		}
		System.out.println("NN Error rate : " + (errors / (double) tests));
		System.out.println("NN Distance/Errors : "
				+ (sumDist / (double) errors));

		errors = 0;
		for (int i = 0; i < tests; i++) {
			double[] v = { tmrutil.stats.Random.uniform(0, maxVal),
					tmrutil.stats.Random.uniform(0, maxVal) };
			double radius = tmrutil.stats.Random.uniform(0, maxVal);
			List<Pair<double[], Integer>> kdlist = kdtree.withinRadius(v,
					radius);
			List<Pair<double[], Integer>> sllist = findWithinRadius(edist, points, v,
					radius);
			if (!kdlist.equals(sllist)) {
				System.out.println("Error in radius retrieval: kdlist.size() = "
						+ kdlist.size()
						+ " == sllist.size() = "
						+ sllist.size());
				for (int j = 0; j < kdlist.size(); j++) {
					if (!kdlist.get(j).equals(sllist.get(j))) {
						System.out.println(VectorOps.toString(kdlist.get(j).getA())
								+ " != "
								+ VectorOps.toString(kdlist.get(j).getA()));
					}
				}
				errors++;
			}
		}
		System.out.println("Radius Error rate : " + (errors / (double) tests));
	}
}
