package tmrutil.util;

public class FiveTuple<A,B,C,D,E> extends FourTuple<A,B,C,D> {
	private E _e;
	
	public FiveTuple(A a, B b, C c, D d, E e)
	{
		super(a,b,c,d);
		_e = e;
	}
	
	public final E getE()
	{
		return _e;
	}
	
	@Override
	public String toString()
	{
		return "[" + getA() + ", " + getB() + ", " + getC() + ", " + getD() + ", " + getE() + "]";
	}
}
