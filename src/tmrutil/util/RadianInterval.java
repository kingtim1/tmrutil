package tmrutil.util;

import tmrutil.math.Modulus;

/**
 * This is a utility method for dealing with intervals of radians. The radians
 * are standardized to the interval [-pi, pi] and intervals are described by the
 * clockwise region between starting and ending radians.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RadianInterval
{
	/**
	 * The clockwise beginning of this interval.
	 */
	private double _clockwiseBegin;
	/**
	 * The clockwise end of this interval.
	 */
	private double _clockwiseEnd;

	public RadianInterval(double clockwiseBegin, double clockwiseEnd)
	{
		_clockwiseBegin = normalizeRadian(clockwiseBegin);
		_clockwiseEnd = normalizeRadian(clockwiseEnd);
	}

	/**
	 * Determines whether or not the angle is within this interval.
	 * 
	 * @param angle
	 *            an angle in radians
	 * @return true if this interval contains the angle; otherwise false
	 */
	public boolean contains(double angle)
	{
		angle = normalizeRadian(angle);
		if (_clockwiseBegin < _clockwiseEnd) {
			return (angle < _clockwiseBegin || angle > _clockwiseEnd);
		} else {
			return (angle > _clockwiseEnd && angle < _clockwiseBegin);
		}
	}

	/**
	 * Standardizes a specified angle so that it falls within the interval [-pi,
	 * pi].
	 * 
	 * @param angle
	 *            an angle in radians
	 * @return the same angle in radians constrained to fall within [-pi, pi]
	 */
	public static final double normalizeRadian(double angle)
	{
		if (angle >= -Math.PI && angle <= Math.PI) {
			return angle;
		} else {
			double mod = 0;
			if (angle >= 0) {
				mod = angle - Math.floor(angle / (2 * Math.PI));
			} else {
				mod = Math.ceil(angle / (2 * Math.PI)) - angle;
			}
			if (mod > Math.PI) {
				return mod - (2 * Math.PI);
			} else {
				return mod;
			}
		}
	}

	/**
	 * Returns the difference between two angles specified in radians. The
	 * difference between two angles is a positive value between 0 and PI. The
	 * difference returned is the smaller difference around the unit circle. So
	 * if one angle is close to -PI and the other is close to PI the angle
	 * returned will be small even though the numbers representing the two
	 * angles are quite different.
	 * 
	 * @param angleA
	 *            an angle in radians
	 * @param angleB
	 *            an angle in radians
	 * @return the difference between the two angles
	 */
	public static final double diff(double angleA, double angleB)
	{
		angleA = normalizeRadian(angleA);
		angleB = normalizeRadian(angleB);

		double minAngle = Math.min(angleA, angleB);
		double maxAngle = Math.max(angleA, angleB);

		double rightAngleDiff = maxAngle - minAngle;
		double leftAngleDiff = (Math.PI + minAngle) + (Math.PI - maxAngle);
		return Math.min(leftAngleDiff, rightAngleDiff);
	}
}
