package tmrutil.util;

import java.lang.reflect.TypeVariable;
import java.util.Comparator;
import tmrutil.math.DimensionMismatchException;

/**
 * Provides lexicographical ordering for arrays of objects with natural ordering.
 * @author Timothy Mann
 *
 * @param <T> a type that implements the <code>Comparable</code> interface
 */
public class LexicographicalArrayComparator<T extends Comparable<T>> implements
		Comparator<T[]>
{

	@Override
	public int compare(T[] t1, T[] t2) throws DimensionMismatchException
	{
		if (t1.length == t2.length) {
			for (int i = 0; i < t1.length; i++) {
				T t1Obj = t1[i];
				T t2Obj = t2[i];
				int ans = t1Obj.compareTo(t2Obj);
				if (ans != 0) {
					return ans;
				}
			}
			return 0;
		}
		throw new DimensionMismatchException(
				"Lexicographical ordering is only defined for vectors of the same length.");
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof LexicographicalArrayComparator) {
			TypeVariable<?>[] ttp = getClass().getTypeParameters();
			TypeVariable<?>[] otp = obj.getClass().getTypeParameters();
			if (ttp.length == otp.length) {
				for (int i = 0; i < ttp.length; i++) {
					if (!ttp.equals(otp)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

}
