package tmrutil.util;

import java.util.Arrays;
import java.util.List;

import tmrutil.math.VectorOps;
import tmrutil.stats.Statistics;

/**
 * A helper class for discretizing a rectangular vector space. This class maps
 * each vector from the continuous space to an integer ({@link fromContinuous})
 * and maps each of those integers back to a representative vector in the
 * continuous space ({@link fromDiscrete}).
 * 
 * @author Timothy A. Mann
 * 
 */
public class Discretization {

	private List<Interval> _cdom;
	private int[] _nbins;
	private int _ndiscrete;

	/**
	 * Constructs a discretization mapping.
	 * 
	 * @param continuousDomain
	 *            a list of intervals describing the rectangular region of
	 *            continuous space that is to be mapped to integers
	 * @param binsPerDimension
	 *            the number of bins to create per dimension
	 */
	public Discretization(List<Interval> continuousDomain,
			List<Integer> binsPerDimension) {
		_cdom = continuousDomain;
		_nbins = new int[binsPerDimension.size()];
		for (int i = 0; i < _nbins.length; i++) {
			_nbins[i] = binsPerDimension.get(i);
		}

		_ndiscrete = (int) Statistics.product(ListUtil
				.toArray(binsPerDimension));
	}

	/**
	 * The total number of integers needed to represent this discretization of
	 * the continuous space.
	 * 
	 * @return total number of discrete cells
	 */
	public int numDiscrete() {
		return _ndiscrete;
	}

	/**
	 * Maps an integer to a vector in the continuous space.
	 * 
	 * @param x
	 *            an integer <code>(x >= 0 && x < numDiscrete())</code>
	 * @return a representative vector for x in the continuous space
	 * @throws IllegalArgumentException
	 *             if an invalid x is provided
	 */
	public double[] fromDiscrete(Integer x) {
		if (x < 0 || x >= _ndiscrete) {
			throw new IllegalArgumentException("Argument x = " + x
					+ " is not in the range of this discretization.");
		}
		int[] bins = ArrayOps.decode(x, _nbins);
		double[] cx = new double[bins.length];
		for (int i = 0; i < cx.length; i++) {
			Interval domain = _cdom.get(i);
			double scale = ((bins[i] + 0.5) / (_nbins[i] - 1));
			cx[i] = scale * domain.getDiff() + domain.getMin();
		}
		return cx;
	}

	/**
	 * Maps a vector from the continuous space to an integer.
	 * 
	 * @param x
	 *            a vector in the bounds of the rectangular space given by this
	 *            discretization
	 * @return an integer representing a region of the continuous space
	 *         containing <code>x</code>
	 * @throws IllegalArgumentException
	 *             if x is not in the domain of this discretization
	 */
	public Integer fromContinuous(double[] x) {
		if (!inDomain(x)) {
			throw new IllegalArgumentException(
					"Argument x (double[]," + VectorOps.toString(x) + ") is not in the domain of this discretization.");
		}
		return whichBin(x);
	}

	private int whichBin1D(double x, int numBins, Interval domain) {
		x = (x - domain.getMin()) / domain.getDiff();
		int bin = (int) (x * (numBins));
		if(bin == numBins){
			bin = numBins-1;
		}
		return bin;
	}

	private int whichBin(double[] x) {
		int[] bins = new int[_nbins.length];
		for (int i = 0; i < bins.length; i++) {
			bins[i] = whichBin1D(x[i], _nbins[i], _cdom.get(i));
		}
		return ArrayOps.encode(bins, _nbins);
	}

	/**
	 * Returns true if the vector x is in the domain of this discretization.
	 * Otherwise false is returned.
	 * 
	 * @param x
	 *            a vector
	 * @return true if the vector x is in the domain and false otherwise
	 */
	public boolean inDomain(double[] x) {
		if (x.length != _cdom.size()) {
			return false;
		}
		for (int i = 0; i < _cdom.size(); i++) {
			if (!_cdom.get(i).contains(x[i])) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Returns a deep copy of an integer array specifying the number of bins for each dimension.
	 * @return an integer array specifying the number of bins for each dimension.
	 */
	public int[] numberOfBins()
	{
		return Arrays.copyOf(_nbins, _nbins.length);
	}
}
