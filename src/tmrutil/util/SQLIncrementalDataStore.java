package tmrutil.util;

import java.io.IOException;
import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Implements an incremental data storage unit that can be used to store scalar
 * values, vectors, and matrices.
 * 
 * @author Timothy A. Mann
 * 
 */
public class SQLIncrementalDataStore implements DataStore
{
	public static final String VALID_ID_REGEX = "[\\p{Alpha}_][\\p{Alnum}_]*";

	public static final String UID_ATTR = "uid";
	public static final String ID_ATTR = "identifier";
	public static final String TYPE_ATTR = "type";
	public static final String VALUE_ATTR = "value";
	public static final String ROW_ATTR = "row";
	public static final String COL_ATTR = "col";
	public static final String NUM_ROW_ATTR = "nrows";
	public static final String NUM_COL_ATTR = "ncols";
	public static final String NAME_ATTR = "name";
	public static final String CREATION_TIME_ATTR = "creationTime";
	public static final String DESCRIPTION_ATTR = "description";

	public static final String SCALAR_TYPE = "b'0'";
	public static final String MATRIX_TYPE = "b'1'";
	public static final String BINDINGS_TABLE_POSTFIX = "_bindings";
	public static final String SCALARS_TABLE_POSTFIX = "_scalars";
	public static final String MATRICES_TABLE_POSTFIX = "_matrices";
	public static final String MATRIX_TABLE_MIDDLE = "_matrix_";

	private static final String SQL_DRIVER = "org.gjt.mm.mysql.Driver";

	private String _dbName;
	private String _experimentsTableName;
	private String _experimentName;

	private java.sql.Connection _connection;
	private java.sql.Statement _stmt;

	public SQLIncrementalDataStore(URI host, Pair<String, String> userPassword,
			String dbName, String experimentsTableName, String experimentName)
			throws SQLException
	{
		// Load the JDBC driver
		try {
			Class.forName(SQL_DRIVER);
		} catch (ClassNotFoundException ex) {
			throw new DebugException("Error loading SQL driver!", ex);
		}

		_dbName = dbName;
		_experimentsTableName = experimentsTableName;
		_experimentName = experimentName;
		_connection = java.sql.DriverManager.getConnection(host.toString(),
				userPassword.getA(), userPassword.getB());
		_stmt = _connection.createStatement();

		if (!isExperimentSetup()) {
			setupExperiment("");
		}
	}

	/**
	 * Returns true if the experiment is setup; otherwise false.
	 * 
	 * @return true if the experiment is setup; otherwise false
	 */
	private boolean isExperimentSetup()
	{
		try {
			String query = "SELECT * FROM " + _experimentsTableName
					+ " WHERE (" + NAME_ATTR + " = \"" + _experimentName
					+ "\");";
			ResultSet rs = _stmt.executeQuery(query);
			return rs.next();
		} catch (SQLException ex) {
			return false;
		}
	}

	/**
	 * Sets up the experiment tables.
	 * 
	 * @param description
	 *            a description of the experiment
	 */
	private void setupExperiment(String description)
	{
		try {
			if (!doesTableExist(_experimentsTableName)) {
				String createExperimentsTable = "CREATE TABLE "
						+ _experimentsTableName + " (" + NAME_ATTR
						+ " VARCHAR(200), " + CREATION_TIME_ATTR + " DATE, "
						+ DESCRIPTION_ATTR + " TEXT, PRIMARY KEY(" + NAME_ATTR
						+ "))";
				_stmt.executeUpdate(createExperimentsTable);
			}
			String insertExperiment = "INSERT INTO " + _experimentsTableName
					+ " VALUES (\"" + _experimentName + "\", now(), \""
					+ description + "\");";
			_stmt.executeUpdate(insertExperiment);
			String createBindingsTable = "CREATE TABLE " + bindingsTable()
					+ " (" + ID_ATTR + " VARCHAR(200), " + TYPE_ATTR
					+ " BIT, PRIMARY KEY(" + ID_ATTR + "));";
			_stmt.executeUpdate(createBindingsTable);
			String createScalarTable = "CREATE TABLE " + scalarsTable() + " ("
					+ ID_ATTR + " VARCHAR(200), " + VALUE_ATTR
					+ " DOUBLE PRECISION, PRIMARY KEY(" + ID_ATTR + "));";
			_stmt.executeUpdate(createScalarTable);
			String createMatrixTable = "CREATE TABLE " + matrixTable() + " ("
					+ UID_ATTR + " INTEGER NOT NULL AUTO_INCREMENT, " + ID_ATTR
					+ " VARCHAR(200), " + NUM_ROW_ATTR + " INTEGER, "
					+ NUM_COL_ATTR + " INTEGER, PRIMARY KEY(" + UID_ATTR
					+ "));";
			_stmt.executeUpdate(createMatrixTable);
		} catch (SQLException ex) {
			throw new DebugException(
					"Failed while setting up the experiment tables in the database.",
					ex);
		}
	}

	/**
	 * Checks if a table with the specified name exists.
	 * 
	 * @param tableName
	 *            a table name
	 * @return true if the table exists; otherwise false is returned
	 */
	private boolean doesTableExist(String tableName)
	{
		try {
			String query = "SHOW tables WHERE Tables_in_" + _dbName + " = '"
					+ tableName + "';";
			ResultSet rs = _stmt.executeQuery(query);
			return rs.next();
		} catch (SQLException ex) {
			throw new DebugException("Failed while checking existing tables.",
					ex);
		}
	}

	private void throwInvalidIdentifier(String identifier)
	{
		throw new IllegalArgumentException("Invalid Identifier : \""
				+ identifier + "\"");
	}

	private void throwNullIdentifier()
	{
		throw new NullPointerException("Detected null identifier.");
	}

	private void throwNullValue()
	{
		throw new NullPointerException("Detected null value.");
	}

	private String bindingsTable()
	{
		return _experimentName + BINDINGS_TABLE_POSTFIX;
	}

	private String scalarsTable()
	{
		return _experimentName + SCALARS_TABLE_POSTFIX;
	}

	private String matrixTable()
	{
		return _experimentName + MATRICES_TABLE_POSTFIX;
	}

	private String matrixDataTable(String identifier)
	{
		try {
			String query = "SELECT " + UID_ATTR + " FROM " + matrixTable()
					+ " WHERE (" + ID_ATTR + " = \"" + identifier + "\");";
			ResultSet rs = _stmt.executeQuery(query);
			rs.next();
			int uid = rs.getInt(UID_ATTR);
			return _experimentName + "_" + uid;
		} catch (SQLException ex) {
			throw new DebugException(
					"An error occurred while determining the name of table for storing \""
							+ identifier + "\" data.", ex);
		}
	}

	@Override
	public boolean isValid(String identifier)
	{
		if (identifier == null || !identifier.matches(VALID_ID_REGEX)) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean isBound(String identifier)
	{
		try {
			if (isValid(identifier)) {
				String query = "SELECT " + ID_ATTR + " FROM " + bindingsTable()
						+ " WHERE (" + ID_ATTR + " = \"" + identifier + "\");";
				ResultSet rs = _stmt.executeQuery(query);
				return rs.next();
			} else {
				return false;
			}
		} catch (SQLException ex) {
			throw new DebugException(
					"Failed while checking bound identifiers.", ex);
		}
	}

	@Override
	public boolean isScalar(String identifier)
	{
		try {
			if (isValid(identifier)) {
				String query = "SELECT " + ID_ATTR + ", " + TYPE_ATTR
						+ " FROM " + bindingsTable() + " WHERE (" + ID_ATTR
						+ " = '" + identifier + "' AND " + TYPE_ATTR + " = "
						+ SCALAR_TYPE + ");";
				ResultSet rs = _stmt.executeQuery(query);
				return rs.next();
			} else {
				return false;
			}
		} catch (SQLException ex) {
			throw new DebugException(
					"Failed while checking bound identifiers.", ex);
		}
	}

	@Override
	public boolean isMatrix(String identifier)
	{
		try {
			if (isValid(identifier)) {
				String query = "SELECT " + ID_ATTR + ", " + TYPE_ATTR
						+ " FROM " + bindingsTable() + " WHERE (" + ID_ATTR
						+ " = '" + identifier + "' AND " + TYPE_ATTR + " = "
						+ MATRIX_TYPE + ");";
				ResultSet rs = _stmt.executeQuery(query);
				return rs.next();
			} else {
				return false;
			}
		} catch (SQLException ex) {
			throw new DebugException(
					"Failed while checking bound identifiers.", ex);
		}
	}

	@Override
	public boolean remove(String identifier)
	{
		if (isScalar(identifier)) {
			return removeScalar(identifier);
		} else if (isMatrix(identifier)) {
			return removeMatrix(identifier);
		} else {
			return false;
		}
	}

	public boolean removeScalar(String identifier)
	{
		if (isScalar(identifier)) {
			try {
				String deleteFromBindings = "DELETE FROM " + bindingsTable()
						+ " WHERE " + ID_ATTR + " = '" + identifier + "';";
				_stmt.executeUpdate(deleteFromBindings);
				String deleteFromScalars = "DELETE FROM " + scalarsTable()
						+ " WHERE " + ID_ATTR + " = '" + identifier + "';";
				_stmt.executeUpdate(deleteFromScalars);
				return true;
			} catch (SQLException ex) {
				throw new DebugException("Failed while removing a scalar.", ex);
			}
		} else {
			return false;
		}
	}

	public boolean removeMatrix(String identifier)
	{
		if (isMatrix(identifier)) {
			try {
				String deleteFromBindings = "DELETE FROM " + bindingsTable()
						+ " WHERE " + ID_ATTR + " = '" + identifier + "';";
				_stmt.executeUpdate(deleteFromBindings);
				String deleteFromMatrices = "DELETE FROM " + matrixTable()
						+ " WHERE " + ID_ATTR + " = '" + identifier + "';";
				_stmt.executeUpdate(deleteFromMatrices);
				String deleteMatrixData = "DROP TABLE "
						+ matrixDataTable(identifier);
				_stmt.executeUpdate(deleteMatrixData);
				return true;
			} catch (SQLException ex) {
				throw new DebugException("Failed while removing a scalar.", ex);
			}
		} else {
			return false;
		}
	}

	private void addScalarBinding(String identifier) throws SQLException
	{
		String addBinding = "INSERT INTO " + bindingsTable() + " VALUES ('"
				+ identifier + "', " + SCALAR_TYPE + ");";
		_stmt.executeUpdate(addBinding);
	}

	private void addMatrixBinding(String identifier, int numRows, int numCols)
			throws SQLException
	{
		String addBinding = "INSERT INTO " + bindingsTable() + " VALUES ('"
				+ identifier + "', " + MATRIX_TYPE + ");";
		_stmt.executeUpdate(addBinding);
		String addMatrix = "INSERT INTO " + matrixTable() + " (" + ID_ATTR
				+ ", " + NUM_ROW_ATTR + ", " + NUM_COL_ATTR + ") VALUES ('"
				+ identifier + "', " + numRows + ", " + numCols + ");";
		_stmt.executeUpdate(addMatrix);
		String createMatrixTable = "CREATE TABLE "
				+ matrixDataTable(identifier) + " (" + ROW_ATTR + " INTEGER, "
				+ COL_ATTR + " INTEGER, " + VALUE_ATTR
				+ " DOUBLE PRECISION);"; //, PRIMARY KEY(" + ROW_ATTR + ", "+ COL_ATTR + "));";
		_stmt.executeUpdate(createMatrixTable);
	}

	/**
	 * Returns a pair containing the number of rows and columns in the specified
	 * matrix. The number of rows are stored in the first pair element, while
	 * the number of columns are stored in the second pair element.
	 * 
	 * @param identifier
	 *            the identifier of a bound matrix
	 * @return a pair containing the number of rows and columns of the specified
	 *         matrix
	 */
	public Pair<Integer, Integer> matrixSize(String identifier)
	{
		int nrows = matrixNumRows(identifier);
		int ncols = matrixNumCols(identifier);
		return new Pair<Integer, Integer>(nrows, ncols);
	}

	/**
	 * Returns the number of rows in a specified matrix.
	 * 
	 * @param identifier
	 *            the identifier of a bound matrix
	 * @return the number of rows in a specified matrix
	 */
	public int matrixNumRows(String identifier)
	{
		if (isMatrix(identifier)) {
			try {
				String query = "SELECT " + NUM_ROW_ATTR + " FROM "
						+ matrixTable() + " WHERE (" + ID_ATTR + " = \""
						+ identifier + "\")";
				ResultSet rs = _stmt.executeQuery(query);
				rs.next();
				return rs.getInt(NUM_ROW_ATTR);
			} catch (SQLException ex) {
				throw new DebugException(
						"An error occurred while retrieving the number of rows in matrix \""
								+ identifier + "\"", ex);
			}
		} else {
			throw new IllegalArgumentException("Identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}
	}

	/**
	 * Returns the number of columns in a specified matrix.
	 * 
	 * @param identifier
	 *            the identifier of a bound matrix
	 * @return the number of columns in a specified matrix
	 */
	public int matrixNumCols(String identifier)
	{
		if (isMatrix(identifier)) {
			try {
				String query = "SELECT " + NUM_COL_ATTR + " FROM "
						+ matrixTable() + " WHERE (" + ID_ATTR + " = \""
						+ identifier + "\")";
				ResultSet rs = _stmt.executeQuery(query);
				rs.next();
				return rs.getInt(NUM_COL_ATTR);
			} catch (SQLException ex) {
				throw new DebugException(
						"An error occurred while retrieving the number of columns in matrix \""
								+ identifier + "\"", ex);
			}
		} else {
			throw new IllegalArgumentException("Identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}
	}

	@Override
	public Set<String> boundIdentifiers()
	{
		Set<String> ids = new HashSet<String>();

		try {
			String query = "SELECT " + ID_ATTR + " FROM " + bindingsTable()
					+ ";";
			ResultSet rs = _stmt.executeQuery(query);
			while (rs.next()) {
				ids.add(rs.getString(ID_ATTR));
			}
		} catch (SQLException ex) {
			throw new DebugException(
					"An error occurred while retrieving the list of bound identifiers from the database.",
					ex);
		}

		return ids;
	}

	@Override
	public void clear()
	{
		Set<String> ids = boundIdentifiers();
		for (String id : ids) {
			remove(id);
		}
	}

	@Override
	public void bind(String identifier, double dscalar)
	{
		if (identifier == null) {
			throwNullIdentifier();
		}
		if (isValid(identifier)) {
			if (isBound(identifier)) {
				remove(identifier);
			}
			try {
				addScalarBinding(identifier);
				String bindScalar = "INSERT INTO " + scalarsTable()
						+ " VALUES ('" + identifier + "', " + dscalar + ");";
				_stmt.executeUpdate(bindScalar);
			} catch (SQLException ex) {
				throw new DebugException("Failed while binding scalar.", ex);
			}
		} else {
			throwInvalidIdentifier(identifier);
		}
	}

	@Override
	public void bind(String identifier, Number nscalar)
	{
		if (nscalar == null) {
			throwNullValue();
		}
		bind(identifier, nscalar.doubleValue());
	}

	@Override
	public void bind(String identifier, int[] ivector)
	{
		if (ivector == null) {
			throwNullValue();
		}
		int[][] vector = { ivector };
		bind(identifier, vector);
	}

	@Override
	public void bind(String identifier, float[] fvector)
	{
		if (fvector == null) {
			throwNullValue();
		}
		float[][] vector = { fvector };
		bind(identifier, vector);
	}

	@Override
	public void bind(String identifier, double[] dvector)
	{
		if (dvector == null) {
			throwNullValue();
		}
		double[][] vector = { dvector };
		bind(identifier, vector);
	}

	@Override
	public void bind(String identifier, Collection<? extends Number> cvector)
	{
		double[] vector = new double[cvector.size()];
		int index = 0;
		for (Number n : cvector) {
			vector[index] = n.doubleValue();
			index++;
		}
		bind(identifier, vector);
	}

	/**
	 * Checks if the object is a valid matrix.
	 * 
	 * @param matrix
	 *            an object
	 * @return true if the object is a rectangular int, float, or double 2d
	 *         array
	 */
	private boolean isValidMatrix(Object matrix)
	{
		if (matrix instanceof int[][]) {
			int[][] imat = (int[][]) matrix;
			int nrows = imat.length;
			if (nrows < 1) {
				return true;
			} else {
				int ncols = imat[0].length;
				for (int i = 1; i < nrows; i++) {
					if (imat[i].length != ncols) {
						return false;
					}
				}
				return true;
			}
		} else if (matrix instanceof float[][]) {
			float[][] fmat = (float[][]) matrix;
			int nrows = fmat.length;
			if (nrows < 1) {
				return true;
			} else {
				int ncols = fmat[0].length;
				for (int i = 1; i < nrows; i++) {
					if (fmat[i].length != ncols) {
						return false;
					}
				}
				return true;
			}
		} else if (matrix instanceof double[][]) {
			double[][] dmat = (double[][]) matrix;
			int nrows = dmat.length;
			if (nrows < 1) {
				return true;
			} else {
				int ncols = dmat[0].length;
				for (int i = 1; i < nrows; i++) {
					if (dmat[i].length != ncols) {
						return false;
					}
				}
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public void bind(String identifier, int[][] imatrix)
	{
		if (identifier == null) {
			throwNullIdentifier();
		}
		if (imatrix == null) {
			throwNullValue();
		}
		if (!isValidMatrix(imatrix)) {
			throw new IllegalArgumentException(
					"The specified matrix is invalid.");
		}

		if (isBound(identifier)) {
			remove(identifier);
		}

		if (isValid(identifier)) {
			try {
				if (imatrix.length < 1) {
					addMatrixBinding(identifier, 0, 0);
				} else {
					addMatrixBinding(identifier, imatrix.length,
							imatrix[0].length);
					for (int r = 0; r < imatrix.length; r++) {
						for (int c = 0; c < imatrix[r].length; c++) {
							if (imatrix[r][c] != 0) {
								String insertValue = "INSERT INTO "
										+ matrixDataTable(identifier)
										+ " VALUES (" + r + ", " + c + ", "
										+ imatrix[r][c] + ");";
								_stmt.executeUpdate(insertValue);
							}
						}
					}
				}
			} catch (SQLException ex) {
				throw new DebugException(
						"Failed while binding integer matrix.", ex);
			}
		} else {
			throwInvalidIdentifier(identifier);
		}
	}

	@Override
	public void bind(String identifier, float[][] fmatrix)
	{
		if (identifier == null) {
			throwNullIdentifier();
		}
		if (fmatrix == null) {
			throwNullValue();
		}
		if (!isValidMatrix(fmatrix)) {
			throw new IllegalArgumentException(
					"The specified matrix is invalid.");
		}

		if (isBound(identifier)) {
			remove(identifier);
		}

		if (isValid(identifier)) {
			try {
				if (fmatrix.length < 1) {
					addMatrixBinding(identifier, 0, 0);
				} else {
					addMatrixBinding(identifier, fmatrix.length,
							fmatrix[0].length);
					for (int r = 0; r < fmatrix.length; r++) {
						for (int c = 0; c < fmatrix[r].length; c++) {
							if (fmatrix[r][c] != 0) {
								String insertValue = "INSERT INTO "
										+ matrixDataTable(identifier)
										+ " VALUES (" + r + ", " + c + ", "
										+ fmatrix[r][c] + ");";
								_stmt.executeUpdate(insertValue);
							}
						}
					}
				}
			} catch (SQLException ex) {
				throw new DebugException("Failed while binding float matrix.",
						ex);
			}
		} else {
			throwInvalidIdentifier(identifier);
		}
	}

	@Override
	public void bind(String identifier, double[][] dmatrix)
	{
		if (identifier == null) {
			throwNullIdentifier();
		}
		if (dmatrix == null) {
			throwNullValue();
		}
		if (!isValidMatrix(dmatrix)) {
			throw new IllegalArgumentException(
					"The specified matrix is invalid.");
		}

		if (isBound(identifier)) {
			remove(identifier);
		}

		if (isValid(identifier)) {
			try {
				if (dmatrix.length < 1) {
					addMatrixBinding(identifier, 0, 0);
				} else {
					addMatrixBinding(identifier, dmatrix.length,
							dmatrix[0].length);
					for (int r = 0; r < dmatrix.length; r++) {
						for (int c = 0; c < dmatrix[r].length; c++) {
							if (dmatrix[r][c] != 0) {
								String insertValue = "INSERT INTO "
										+ matrixDataTable(identifier)
										+ " VALUES (" + r + ", " + c + ", "
										+ dmatrix[r][c] + ");";
								_stmt.executeUpdate(insertValue);
							}
						}
					}
				}
			} catch (SQLException ex) {
				throw new DebugException("Failed while binding double matrix.",
						ex);
			}
		} else {
			throwInvalidIdentifier(identifier);
		}
	}

	@Override
	public void appendRow(String identifier, int[] irow)
	{
		double[] drow = new double[irow.length];
		for (int i = 0; i < irow.length; i++) {
			drow[i] = irow[i];
		}
		appendRow(identifier, drow);
	}

	@Override
	public void appendRow(String identifier, float[] frow)
	{
		double[] drow = new double[frow.length];
		for (int i = 0; i < frow.length; i++) {
			drow[i] = frow[i];
		}
		appendRow(identifier, drow);
	}

	@Override
	public void appendRow(String identifier, double[] drow)
	{
		if (isMatrix(identifier)) {
			try {
				int nrows = matrixNumRows(identifier);
				int ncols = matrixNumCols(identifier);
				if (drow.length != ncols) {
					throw new IllegalArgumentException(
							"A row can only be appended to a matrix "
									+ "if the number of elements in the row matches "
									+ "the number of columns of the matrix.");
				}
				String updateNRows = "UPDATE " + matrixTable() + " SET "
						+ NUM_ROW_ATTR + " = " + (nrows + 1) + " WHERE "
						+ ID_ATTR + " = \"" + identifier + "\";";
				_stmt.executeUpdate(updateNRows);
				for (int c = 0; c < ncols; c++) {
					if (drow[c] != 0) {
						String insertValue = "INSERT INTO "
								+ matrixDataTable(identifier) + " VALUES ("
								+ nrows + ", " + c + ", " + drow[c] + ");";
						_stmt.executeUpdate(insertValue);
					}
				}
			} catch (SQLException ex) {
				throw new DebugException(
						"Error occurred while appending a row to an existing matrix.");
			}
		} else {
			bind(identifier, drow);
		}
	}

	@Override
	public void appendRow(String identifier, Collection<? extends Number> crow)
	{
		double[] drow = new double[crow.size()];
		int index = 0;
		for (Number n : crow) {
			drow[index] = n.doubleValue();
			index++;
		}
		appendRow(identifier, drow);
	}

	public void appendColumn(String identifier, int[] icolumn)
	{
		double[] dcol = new double[icolumn.length];
		for (int i = 0; i < icolumn.length; i++) {
			dcol[i] = icolumn[i];
		}
		appendColumn(identifier, dcol);
	}

	public void appendColumn(String identifier, float[] fcolumn)
	{
		double[] dcol = new double[fcolumn.length];
		for (int i = 0; i < fcolumn.length; i++) {
			dcol[i] = fcolumn[i];
		}
		appendColumn(identifier, dcol);
	}

	public void appendColumn(String identifier, double[] dcolumn)
	{
		if (isMatrix(identifier)) {
			try {
				int nrows = matrixNumRows(identifier);
				int ncols = matrixNumCols(identifier);
				if (dcolumn.length != nrows) {
					throw new IllegalArgumentException(
							"A column can only be appended to a matrix "
									+ "if the number of elements in the column matches "
									+ "the number of rows of the matrix.");
				}
				String updateNCols = "UPDATE " + matrixTable() + " SET "
						+ NUM_COL_ATTR + " = " + (ncols + 1) + " WHERE "
						+ ID_ATTR + " = \"" + identifier + "\";";
				_stmt.executeUpdate(updateNCols);
				for (int r = 0; r < nrows; r++) {
					if (dcolumn[r] != 0) {
						String insertValue = "INSERT INTO "
								+ matrixDataTable(identifier) + " VALUES (" + r
								+ ", " + ncols + ", " + dcolumn[r] + ");";
						_stmt.executeUpdate(insertValue);
					}
				}
			} catch (SQLException ex) {
				throw new DebugException(
						"Error occurred while appending a row to an existing matrix.");
			}
		} else {
			double[][] colMat = new double[dcolumn.length][1];
			for (int r = 0; r < dcolumn.length; r++) {
				colMat[r][0] = dcolumn[r];
			}
			bind(identifier, colMat);
		}
	}

	public void appendColumn(String identifier,
			Collection<? extends Number> ccolumn)
	{
		double[] dcol = new double[ccolumn.size()];
		int index = 0;
		for (Number n : ccolumn) {
			dcol[index] = n.doubleValue();
			index++;
		}
		appendColumn(identifier, dcol);
	}

	/**
	 * Retrieves a scalar from the database.
	 * 
	 * @param identifier
	 *            an identifier bound to a scalar value
	 * @return a scalar value
	 */
	public double getScalar(String identifier)
	{
		if (isScalar(identifier)) {
			try {
				String query = "SELECT " + VALUE_ATTR + " FROM "
						+ scalarsTable() + " WHERE " + ID_ATTR + " = \""
						+ identifier + "\";";
				ResultSet rs = _stmt.executeQuery(query);
				rs.next();
				return rs.getDouble(VALUE_ATTR);
			} catch (SQLException ex) {
				throw new DebugException(
						"An error occurred while retrieving a scalar from the database.",
						ex);
			}
		} else {
			throw new IllegalArgumentException("The identifier \"" + identifier
					+ "\" is not bound to a scalar.");
		}
	}

	/**
	 * Retrieves a matrix from the database.
	 * 
	 * @param identifier
	 *            an identifier bound to a matrix
	 * @return a 2D array of doubles representing a matrix
	 */
	public double[][] getMatrix(String identifier)
	{
		if (isMatrix(identifier)) {
			try {
				int nrows = matrixNumRows(identifier);
				int ncols = matrixNumCols(identifier);
				double[][] mat = new double[nrows][ncols];
				for (int r = 0; r < nrows; r++) {
					for (int c = 0; c < ncols; c++) {
						String query = "SELECT " + VALUE_ATTR + " FROM "
								+ matrixDataTable(identifier) + " WHERE "
								+ ROW_ATTR + " = " + r + " AND " + COL_ATTR
								+ " = " + c;
						ResultSet rs = _stmt.executeQuery(query);
						if (rs.next()) {
							mat[r][c] = rs.getDouble(VALUE_ATTR);
						} else {
							mat[r][c] = 0;
						}
					}
				}
				return mat;
			} catch (SQLException ex) {
				throw new DebugException(
						"An error occurred while retrieving a matrix from the database.",
						ex);
			}
		} else {
			throw new IllegalArgumentException("The identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}
	}

	/**
	 * Saves the contents of this data store to a matlab file.
	 * 
	 * @param filename
	 *            the file name to save to
	 * @throws IOException
	 *             if an I/O error occurs while saving the matlab file
	 */
	public void saveAsMatlab(String filename) throws IOException
	{
		try {
			Map<String, Double> scalars = new HashMap<String, Double>();
			Map<String, double[][]> matrices = new HashMap<String, double[][]>();
			String bindingsQuery = "SELECT " + ID_ATTR + ", " + TYPE_ATTR
					+ " FROM " + bindingsTable();
			ResultSet rs = _stmt.executeQuery(bindingsQuery);
			while (rs.next()) {
				String identifier = rs.getString(ID_ATTR);
				if (rs.getBoolean(TYPE_ATTR)) {
					matrices.put(identifier, getMatrix(identifier));
				} else {
					scalars.put(identifier, getScalar(identifier));
				}
			}
			MatlabUtil.save(filename, matrices, scalars);
		} catch (SQLException ex) {
			throw new DebugException(
					"Failed while saving database contents to a matlab file.",
					ex);
		}
	}
}
