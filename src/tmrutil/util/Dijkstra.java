/*
 * Dijkstra.java
 */

/* Package */
package tmrutil.util;

/* Imports */
import tmrutil.math.VectorOps;
import tmrutil.stats.Statistics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Implements the Dijkstra shortest path algorithm.
 */
public class Dijkstra
{
	private class Node
	{
		int[] _p;

		@Override
		public boolean equals(Object o)
		{
			if (o instanceof Node) {
				Node p = (Node) o;
				if (VectorOps.equal(p._p, _p)) {
					return true;
				}
			}
			return false;
		}

		@Override
		public int hashCode()
		{
			return Statistics.sum(_p);
		}

		public boolean isNeighbor(Node n)
		{
			if (!equals(n)) {
				double d = VectorOps.distance(_p, n._p);
				if (d < 2) {
					return true;
				}
			}
			return false;
		}

		Node(int[] p)
		{
			_p = p;
		}

		Node(int i, int j)
		{
			_p = new int[] { i, j };
		}
	}

	private final Comparator<Node> _shortestDistanceComparator = new Comparator<Node>() {
		@Override
		public int compare(Node left, Node right)
		{
			double result = _distances.get(left) - _distances.get(right);
			return (int) Math.floor(100 * result);
		}
	};

	/**
	 * Creates a map which associates each node to a list of its neighbors.
	 */
	private Map<Node, List<Node>> makeNeighborMap()
	{
		_all = new ArrayList<Node>(_rows * _cols);
		for (int i = 0; i < _rows; i++) {
			for (int j = 0; j < _cols; j++) {
				Node p = new Node(i, j);
				_all.add(p);
			}
		}

		Map<Node, List<Node>> neighborMap = new HashMap<Node, List<Node>>();
		for (Node n : _all) {
			List<Node> neighbors = new LinkedList<Node>();
			for (Node m : _all) {
				if (m.isNeighbor(n)) {
					neighbors.add(m);
				}
			}
			neighborMap.put(n, neighbors);
		}
		return neighborMap;
	}

	private void init()
	{
		// Clear all of the data structures
		_distances.clear();
		_predecessors.clear();
		_q.clear();
	}

	/** Represents a graph. */
	private double[][] _graph;

	private Map<Node, List<Node>> _neighborMap;

	private List<Node> _all;

	private int _rows;

	private int _cols;

	private Map<Node, Double> _distances;

	private Map<Node, Node> _predecessors;

	private PriorityQueue<Node> _q;

	public List<int[]> pathfind(int[] a, int[] b)
	{
		Node pta = new Node(a);
		Node ptb = new Node(b);

		init();

		_distances.put(pta, 0.0);
		_q.add(pta);

		Node v;
		while ((v = _q.poll()) != null) {
			List<Node> us = _neighborMap.get(v);
			for (Node u : us) {
				double weight = _graph[u._p[0]][u._p[1]];
				Double d = _distances.get(u);
				if (d == null || d > _distances.get(v) + weight) {
					// System.out.println(VectorOps.toString(v._p) + " <-- "
					// +VectorOps.toString(u._p));
					_distances.put(u, _distances.get(v) + weight);
					_predecessors.put(u, v);
					if (!_q.contains(u)) {
						_q.add(u);
					}
				}
			}
		}

		List<int[]> path = new LinkedList<int[]>();
		path.add(ptb._p);
		Node current = _predecessors.get(ptb);
		while (current != null && !current.equals(pta)) {
			path.add(current._p);
			current = _predecessors.get(current);
		}
		path.add(pta._p);
		Collections.reverse(path);
		return path;
	}

	/**
	 * Constructs a Dijkstra search object from a graph, but does not perform
	 * the search. A graph is represented by a square matrix. Nodes are
	 * represented by row numbers and column numbers. A graph that connects a
	 * node <code>i</code> to a node <code>j</code> would be represented by
	 * placing a positive value for the matrix at the <code>i</code>th row and
	 * <code>j</code>th column.
	 * 
	 * @param graph
	 *            a graph represented by a matrix
	 */
	public Dijkstra(double[][] graph)
	{
		_graph = graph;
		_rows = graph.length;
		_cols = graph[0].length;
		_neighborMap = makeNeighborMap();

		_distances = new HashMap<Node, Double>();
		_predecessors = new HashMap<Node, Node>();
		_q = new PriorityQueue<Node>(_rows * _cols, _shortestDistanceComparator);
	}
}