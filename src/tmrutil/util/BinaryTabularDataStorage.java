package tmrutil.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import tmrutil.math.VectorOps;

/**
 * A utility class for storing tabular data to a single random access binary
 * file.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BinaryTabularDataStorage implements Closeable {
	private class FileStructure {
		private List<String> _columnNames;
		private int _numRecords;
		private int _headerSizeInBytes;

		public FileStructure(List<String> columnNames, int numRecords,
				int headerSizeInBytes) {
			_columnNames = columnNames;
			_numRecords = numRecords;
			_headerSizeInBytes = headerSizeInBytes;
		}

		public List<String> columnNames() {
			return _columnNames;
		}

		public int numRecords() {
			return _numRecords;
		}

		public void setNumRecords(int n) {
			_numRecords = n;
		}

		public void incrementNumRecords() {
			_numRecords++;
		}

		public int headerSizeInBytes() {
			return _headerSizeInBytes;
		}
	}

	private static final int BYTE_SIZE = 1;
	private static final int DOUBLE_SIZE = 8;
	private static final int INT_SIZE = 4;

	private static final Charset UTF8 = Charset.forName("utf-8");

	private FileStructure _fstruct;
	private RandomAccessFile _raf;
	private FileChannel _fc;
	
	public BinaryTabularDataStorage(File dataFile) throws IOException
	{
		this(dataFile, null);
	}

	public BinaryTabularDataStorage(File dataFile, List<String> columnNames)
			throws IOException {
		_raf = new RandomAccessFile(dataFile, "rws");
		_fc = _raf.getChannel();

		if (columnNames == null) {
			_fstruct = readHeader();
		} else {
			// If the file is empty write the header
			if (_raf.length() == 0) {
				_fstruct = new FileStructure(columnNames, 0, 0);
				writeHeader();
			} else {
				FileStructure fstruct = readHeader();
				if (columnNames.size() != fstruct.columnNames()
						.size()) {
					throw new IOException(
							"The number of columns specified does not match the number in the existing file.");
				}

				for (int i = 0; i < columnNames.size(); i++) {
					String givenColName = columnNames.get(i);
					String fileColName = fstruct.columnNames().get(i);
					if (!givenColName.equals(fileColName)) {
						throw new IOException(
								"Column names given as argument do not match the column names in the existing file.");
					}
				}
				
				_fstruct = fstruct;
			}
		}
	}
	
	/**
	 * Deletes all of the stored records.
	 * @throws IOException if an I/O exception occurs while deleting records.
	 */
	public void clear() throws IOException
	{
		_raf.setLength(_fstruct.headerSizeInBytes());
		_fstruct.setNumRecords(0);
	}

	private void writeHeader() throws IOException {
		int headerSize = 0;
		// Write the number of columns
		_raf.writeInt(_fstruct.columnNames().size());
		headerSize += INT_SIZE;
		// Write that there are zero records stored in this file
		_raf.writeInt(0);
		headerSize += INT_SIZE;

		// For each column name write the number of bytes of its string followed
		// by the string itself.
		for (String columnName : _fstruct.columnNames()) {
			byte[] bs = columnName.getBytes(UTF8);
			_raf.writeInt(bs.length);
			headerSize += INT_SIZE;
			_raf.write(bs);
			headerSize += bs.length * BYTE_SIZE;
		}
		_fstruct._headerSizeInBytes = headerSize;
	}

	private FileStructure readHeader() throws IOException {
		int headerSize = 0;
		// Read the number of columns
		int numColumns = _raf.readInt();
		headerSize += INT_SIZE;
		// Read the number of records stored in this file
		int numRecords = _raf.readInt();
		headerSize += INT_SIZE;

		// For the number of columns
		// First read the number of bytes, then the string data itself
		List<String> columnNames = new ArrayList<String>(numColumns);
		for (int i = 0; i < numColumns; i++) {
			int numBytes = _raf.readInt();
			headerSize += INT_SIZE;
			headerSize += numBytes * BYTE_SIZE;

			byte[] b = new byte[numBytes];
			_raf.readFully(b);
			String colName = new String(b, UTF8);
			columnNames.add(colName);
		}

		FileStructure fstruct = new FileStructure(columnNames, numRecords,
				headerSize);
		return fstruct;
	}
	
	public List<String> columnNames()
	{
		return new ArrayList<String>(_fstruct.columnNames());
	}

	public void appendRecord(int[] record) throws IOException {
		double[] arecord = new double[record.length];
		for(int i=0;i<record.length;i++){
			arecord[i] = record[i];
		}
		appendRecord(arecord);
	}
	
	public void appendRecord(List<? extends Number> record) throws IOException {
		double[] arecord = new double[record.size()];
		for (int i = 0; i < record.size(); i++) {
			arecord[i] = record.get(i).doubleValue();
		}
		appendRecord(arecord);
	}

	public void appendRecord(double[] record) throws IOException {
		if (record.length != _fstruct.columnNames().size()) {
			throw new IllegalArgumentException(
					"Number of record elements does not match the number of columns.");
		}
		
		appendRecords(new double[][]{record});
	}
	
	/**
	 * Computes the size of a collection of records in bytes.
	 * @param records a set of records
	 * @return the size of the record data in bytes
	 */
	private int recordsSizeInBytes(double[][] records)
	{
		int sizeInBytes = 0;
		for(int i=0;i<records.length;i++){
			sizeInBytes += records[i].length * DOUBLE_SIZE;
		}
		return sizeInBytes;
	}
	
	//public static final int MAX_BYTE_BUFF = 8 * 1024;
	
	public void appendRecords(double[][] record) throws IOException {

		// Set the RAF to the end of the file
		long oldLength = _raf.length();
		int sizeInBytes = recordsSizeInBytes(record);
		_raf.setLength(oldLength + sizeInBytes);
		_raf.seek(oldLength);
		
		MappedByteBuffer mbb = _fc.map(MapMode.READ_WRITE, oldLength, sizeInBytes);

		for(int i=0;i<record.length;i++){
			for(int j=0;j<record[i].length;j++){
				mbb.putDouble(record[i][j]);
			}
		}
		
		/*
		// Convert the array of doubles to a byte array.
		// Then write the whole byte array at once
		ByteBuffer bbuff = ByteBuffer.allocate(MAX_BYTE_BUFF);
		for(int i=0;i<record.length;i++){
			for(int j=0;j<record[i].length;j++){
				bbuff.putDouble(record[i][j]);
			}
			if(bbuff.position() >= MAX_BYTE_BUFF){
				_raf.write(bbuff.array(), 0, bbuff.position());
				bbuff.position(0);
			}
		}
		_raf.write(bbuff.array(), 0, bbuff.position());
		*/
		
		// Update the number of stored records in the file structure
		_fstruct.setNumRecords(_fstruct.numRecords() + record.length);
		
		// Write the number of stored records
		_raf.seek(INT_SIZE);
		_raf.writeInt(_fstruct.numRecords());
	}

	private int recordSizeInBytes() {
		return DOUBLE_SIZE * _fstruct.columnNames().size();
	}

	/**
	 * Returns the number of records stored in the underlying file.
	 * @return the number of records stored
	 */
	public int numRecords() {
		return _fstruct.numRecords();
	}

	/**
	 * Reads a single record.
	 * 
	 * @param index
	 *            the index of the requested record
	 * @return a double array where the elements represent column data
	 * @throws IOException
	 *             if an I/O error occurs while reading the requested record
	 */
	public double[] readRecord(int index) throws IOException {
		if (index < 0 || index >= numRecords()) {
			throw new IndexOutOfBoundsException(
					"Cannot read record from index " + index
							+ ". There are only " + numRecords()
							+ " stored records.");
		}
		return readSlice(index, index+1)[0];
	}

	/**
	 * Reads a slice of records.
	 * 
	 * @param startIndex
	 *            the index of the first record to read (inclusive)
	 * @param endIndex
	 *            the index just after the last record to read (exclusive)
	 * @return a 2D double array where the first index accesses the requested
	 *         records and the second index accesses column data
	 * @throws IOException
	 *             if an I/O error occurs while reading the slice of requested
	 *             records
	 */
	public double[][] readSlice(int startIndex, int endIndex)
			throws IOException {
		if (startIndex < 0 || endIndex > numRecords()
				|| startIndex >= endIndex) {
			throw new IndexOutOfBoundsException(
					"Cannot read slice of records (" + startIndex + ", "
							+ endIndex + "). There are only " + numRecords()
							+ " stored records.");
		}
		int numRecords = endIndex - startIndex;
		double[][] record = new double[numRecords][_fstruct.columnNames().size()];
		int recordSize = recordSizeInBytes();
		int offset = startIndex * recordSize
				+ _fstruct.headerSizeInBytes();

		_raf.seek(offset);
		
		byte[] buff = new byte[recordSize * numRecords];
		_raf.readFully(buff);
		ByteBuffer bbuffer = ByteBuffer.wrap(buff);

		for (int i = 0; i < endIndex - startIndex; i++) {
			for (int j = 0; j < record[i].length; j++) {
				//record[i][j] = _raf.readDouble();
				int index = i * recordSize + (j * DOUBLE_SIZE);
				record[i][j] = bbuffer.getDouble(index);
			}
		}
		return record;
	}

	@Override
	public void close() throws IOException {
		_raf.close();
	}
	
	public static void main(String[] args) throws IOException
	{
		List<String> columns = new ArrayList<String>();
		columns.add("x");
		columns.add("y");
		columns.add("z");
		File dataFile = new File("/home/timotyman/Documents/tmp/tab_data");
		BinaryTabularDataStorage dstore = new BinaryTabularDataStorage(dataFile, columns);
		dstore.clear();
		for(int i=0;i<10;i++){
			double[] record = {0.5, 0.2, 1000.2045};
			dstore.appendRecord(record);
		}
		dstore.close();
		
		dstore = new BinaryTabularDataStorage(dataFile);
		double[][] records = dstore.readSlice(5, 7);
		for(int i=0;i<records.length;i++){
			System.out.println(VectorOps.toString(records[i]));
		}
		dstore.close();
	}
}
