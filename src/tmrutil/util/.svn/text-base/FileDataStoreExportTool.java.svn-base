package tmrutil.util;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

/**
 * A graphical tool for exporting scalar and matrix data stored in files to
 * useful formats for analysis.
 * 
 * @author Timothy A. Mann
 * 
 */
public class FileDataStoreExportTool
{
	public static final int DEFAULT_WIN_WIDTH = 200;
	public static final int DEFAULT_WIN_HEIGHT = 400;

	public static enum Operation {
		ALL, SUM_ROWS, MEAN_ROWS, STD_ROWS, MEAN_STD_ROWS, SUBSAMPLE_HALF_COLUMNS, SUBSAMPLE_QUARTER_COLUMNS, SUBSAMPLE_HUNDREDTH_COLUMNS
	};

	private JFrame _mainWin;
	private JComponent _mainComp;
	private JTable _table;
	private FileDataStoreTableModel _tmodel;

	public FileDataStoreExportTool()
	{
		setupUI();
	}

	private class OpenAction extends AbstractAction
	{
		public OpenAction()
		{
			super("Open");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			JFileChooser chooser = new JFileChooser();
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = chooser.showOpenDialog(_mainWin);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				if (_mainComp != null) {
					_mainWin.remove(_mainComp);
				}
				openDataStore(chooser.getSelectedFile());
			}
		}
	}

	private class ExitAction extends AbstractAction
	{

		public ExitAction()
		{
			super("Exit");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}

	private class ExportAction extends AbstractAction implements Runnable
	{
		public ExportAction()
		{
			super("Export");
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			new Thread(this).start();
		}

		@Override
		public void run()
		{
			if (_table != null && _tmodel != null) {
				int[] selected = _table.getSelectedRows();
				if (selected.length == 0) {
					selected = new int[_tmodel.getRowCount()];
					for (int i = 0; i < _tmodel.getRowCount(); i++) {
						selected[i] = i;
					}
				}
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int returnVal = chooser.showSaveDialog(_mainWin);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();

					Map<String, Double> scalars = new HashMap<String, Double>();
					Map<String, double[][]> matrices = new HashMap<String, double[][]>();

					try {
						JDialog loadDialog = openLoadingDialog();
						
						// Save all scalar values
						FileDataStore dstore = _tmodel.getDataStore();
						Set<String> ids = dstore.boundIdentifiers();
						for(String id : ids){
							if(dstore.isScalar(id)){
								scalars.put(id, dstore.getScalar(id));
							}
						}
						
						for (int i : selected) {
							String id = _tmodel.getIdentifier(i);
							Operation op = _tmodel.getOperation(i);
							switch(op){
							case ALL:
								matrices.put(id, dstore.getMatrix(id));
								break;
							case SUM_ROWS:
								matrices.put(id + "_" + Operation.SUM_ROWS, new double[][]{dstore.getMatrixRowSum(id)});
								break;
							case MEAN_ROWS:
								matrices.put(id + "_" + Operation.MEAN_ROWS, new double[][]{dstore.getMatrixRowMean(id)});
								break;
							case STD_ROWS:
								matrices.put(id + "_" + Operation.STD_ROWS, new double[][]{dstore.getMatrixRowStd(id)});
								break;
							case MEAN_STD_ROWS:
								matrices.put(id + "_" + Operation.MEAN_ROWS, new double[][]{dstore.getMatrixRowMean(id)});
								matrices.put(id + "_" + Operation.STD_ROWS, new double[][]{dstore.getMatrixRowStd(id)});
								break;
							case SUBSAMPLE_HALF_COLUMNS:
								matrices.put(id + "_HALF", dstore.getMatrixSubsampleColumns(id, 0.5));
								break;
							case SUBSAMPLE_QUARTER_COLUMNS:
								matrices.put(id + "_QUARTER", dstore.getMatrixSubsampleColumns(id, 0.25));
								break;
							case SUBSAMPLE_HUNDREDTH_COLUMNS:
								matrices.put(id + "_HUNDREDTH", dstore.getMatrixSubsampleColumns(id, 0.01));
							}
							
						}
						MatlabUtil.save(file.getAbsolutePath(), matrices,
								scalars);

						closeLoadingDialog(loadDialog);
					} catch (IOException ex) {
						JOptionPane.showMessageDialog(_mainWin,
								"An error occurred while exporting data to Matlab file.");
					}
				}
			} else {
				JOptionPane.showMessageDialog(_mainWin,
						"Please open a data store directory and select the elements for export.");
			}
		}
	}

	private JDialog openLoadingDialog()
	{
		JDialog dialog = new JDialog(_mainWin, "");
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		JProgressBar pbar = new JProgressBar();
		pbar.setString("Exporting");
		pbar.setStringPainted(true);
		pbar.setIndeterminate(true);
		dialog.add(pbar);
		
		dialog.pack();
		dialog.setVisible(true);
		
		return dialog;
	}

	private void closeLoadingDialog(JDialog dialog)
	{
		dialog.setVisible(false);
		dialog.dispose();
	}

	private void setupUI()
	{
		_mainWin = new JFrame(getClass().getName());
		_mainWin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		_mainWin.setSize(DEFAULT_WIN_WIDTH, DEFAULT_WIN_HEIGHT);

		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(new OpenAction());
		fileMenu.add(new ExitAction());
		menuBar.add(fileMenu);

		_mainWin.setJMenuBar(menuBar);
		_mainWin.setVisible(true);
	}

	private static class FileDataStoreTableModel extends AbstractTableModel
	{
		public static final String ID_COL = "Identifier";
		public static final String SIZE_COL = "Size";
		public static final String OPERATION_COL = "Operation";
		public static final String[] COL_NAMES = { ID_COL, SIZE_COL,
				OPERATION_COL };

		private FileDataStore _dstore;
		private List<String> _ids;
		private Map<String, Operation> _operations;

		public FileDataStoreTableModel(FileDataStore dstore)
		{
			_dstore = dstore;
			_ids = getIdentifiers();
			Collections.sort(_ids);

			_operations = new HashMap<String, Operation>();
			for (String id : _ids) {
				_operations.put(id, Operation.ALL);
			}
		}
		
		public Operation getOperation(int row)
		{
			return _operations.get(_ids.get(row));
		}

		public void setOperation(int row, Operation op)
		{
			_operations.put(_ids.get(row), op);
		}

		public String getIdentifier(int row)
		{
			return _ids.get(row);
		}
		
		public FileDataStore getDataStore()
		{
			return _dstore;
		}

		@Override
		public String getColumnName(int col)
		{
			return COL_NAMES[col];
		}

		@Override
		public Class<?> getColumnClass(int col)
		{
			if (getRowCount() > 0) {
				Class<?> c = getValueAt(0, col).getClass();
				return c;
			} else {
				return String.class;
			}
		}

		@Override
		public boolean isCellEditable(int row, int col)
		{
			String id = getColumnName(col);
			if (id.equals(OPERATION_COL)) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		public int getRowCount()
		{
			return _ids.size();
		}

		@Override
		public int getColumnCount()
		{
			return COL_NAMES.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex)
		{
			String id = _ids.get(rowIndex);

			String colStr = COL_NAMES[columnIndex];
			if (colStr.equals(ID_COL)) {
				return id;
			}
			if (colStr.equals(SIZE_COL)) {
				int nrows = _dstore.matrixNumRows(id);
				int ncols = _dstore.matrixNumCols(id);
				return nrows + "x" + ncols;
			}
			if (colStr.equals(OPERATION_COL)) {
				return _operations.get(id);
			}
			return null;
		}

		public List<String> getIdentifiers()
		{
			Set<String> ids = _dstore.boundIdentifiers();
			List<String> matrixIds = new ArrayList<String>();
			for(String id : ids){
				if(_dstore.isMatrix(id)){
					matrixIds.add(id);
				}
			}
			return matrixIds;
		}

	}

	private static class OperationListener implements ItemListener
	{
		private JTable _table;
		private FileDataStoreTableModel _tmodel;

		public OperationListener(JTable table, FileDataStoreTableModel tmodel)
		{
			_table = table;
			_tmodel = tmodel;
		}

		@Override
		public void itemStateChanged(ItemEvent e)
		{
			int index = _table.getSelectedRow();
			if (index != -1) {
				_tmodel.setOperation(index, (Operation) e.getItem());
			}
		}

	}

	private void openDataStore(File dataDir)
	{
		try {
			FileDataStore dstore = new FileDataStore(dataDir, true);
			_tmodel = new FileDataStoreTableModel(dstore);
			_table = new JTable(_tmodel);

			TableColumn tcolumn = _table.getColumnModel().getColumn(2);
			JComboBox opBox = new JComboBox(Operation.values());
			opBox.addItemListener(new OperationListener(_table, _tmodel));
			tcolumn.setCellEditor(new DefaultCellEditor(opBox));
			JScrollPane scroller = new JScrollPane(_table);

			JComponent exportComp = new JPanel();
			JButton exportButton = new JButton(new ExportAction());
			exportComp.add(exportButton);

			_mainComp = new JPanel();
			BoxLayout blayout = new BoxLayout(_mainComp, BoxLayout.Y_AXIS);
			_mainComp.setLayout(blayout);

			JLabel label = new JLabel(dataDir.getName(), JLabel.CENTER);
			_mainComp.add(label);
			_mainComp.add(scroller);
			_mainComp.add(exportComp);

			_mainWin.add(_mainComp);
			_mainWin.pack();

		} catch (IOException e) {
			JOptionPane.showMessageDialog(
					_mainWin,
					"An error occurred while trying to open the selected data directory.",
					"IOException", JOptionPane.ERROR_MESSAGE);
		} catch (IllegalStateException e) {
			JOptionPane.showMessageDialog(
					_mainWin,
					"An error occurred while trying to open the selected data directory. The directory may not exist or have an invalid format.",
					"IllegalStateException", JOptionPane.ERROR_MESSAGE);
		}
	}

	public static void main(String[] args)
	{
		new FileDataStoreExportTool();
	}
}
