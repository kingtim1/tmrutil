package tmrutil.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * An implementation of the {@link tmrutil.util.Table} interface using an
 * {@link java.util.ArrayList}.
 * 
 * @author Timothy Mann
 * 
 * @param <T>
 *            the type of data stored by this table
 */
public class ArrayTable<T> implements Table<T>, Serializable
{
	private static final long serialVersionUID = -847501069783889429L;
	
	/**
	 * The list used to store all elements in the table.
	 */
	private ArrayList<T> _table;
	/**
	 * The number of rows in this table.
	 */
	private int _rows;
	/**
	 * The number of columns in this table.
	 */
	private int _cols;

	/**
	 * Constructs a new empty table.
	 */
	public ArrayTable(int rows, int cols)
	{
		_rows = rows;
		_cols = cols;
		_table = new ArrayList<T>(rows * cols);
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				_table.add(null);
			}
		}
	}
	
	/**
	 * Constructs a new table filled with the specified value.
	 */
	public ArrayTable(int rows, int cols, T val)
	{
		_rows = rows;
		_cols = cols;
		_table = new ArrayList<T>(rows*cols);
		for(int r=0;r<rows;r++){
			for(int c=0;c<cols;c++){
				_table.add(val);
			}
		}
	}

	@Override
	public T get(int row, int col) throws IndexOutOfBoundsException
	{
		if (row < 0 || row >= _rows || col < 0 || col >= _cols) {
			throw new IndexOutOfBoundsException("The specified element (" + row
					+ ", " + col + ") does not exist in a table with " + _rows
					+ " rows and " + _cols + " columns.");
		}
		int index = row * _cols + col;
		return _table.get(index);
	}

	@Override
	public List<T> getColumn(int col) throws IndexOutOfBoundsException
	{
		if (col < 0 || col >= _cols) {
			throw new IndexOutOfBoundsException("The specified column (" + col
					+ ") does not exist in this table.");
		}
		List<T> column = new ArrayList<T>(_rows);
		for (int r = 0; r < _rows; r++) {
			column.add(get(r, col));
		}
		return column;
	}

	@Override
	public List<T> getRow(int row) throws IndexOutOfBoundsException
	{
		if (row < 0 || row >= _rows) {
			throw new IndexOutOfBoundsException("The specified row (" + row
					+ ") does not exist in this table.");
		}
		return _table.subList(row * _cols, row * (_cols + 1));
	}

	@Override
	public int numberOfCols()
	{
		return _cols;
	}

	@Override
	public int numberOfRows()
	{
		return _rows;
	}

	@Override
	public void set(T val, int row, int col)
	{
		_table.set(row*_cols + col, val);
	}

	@Override
	public Iterator<T> iterator()
	{
		return _table.iterator();
	}

}
