package tmrutil.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.jmatio.io.MatFileWriter;
import com.jmatio.types.MLArray;
import com.jmatio.types.MLDouble;

/**
 * Utility class for exporting data to Matlab files.
 * @author Timothy Mann
 *
 */
public class MatlabUtil
{

	/**
	 * Writes a set of matrices and scalar values to a file that can be read by
	 * Matlab.
	 * 
	 * @param filename
	 *            the name of the file to save the variables to
	 * @param matrices
	 *            a map of variable names to matrices
	 * @param scalars
	 *            a map of variable names to scalar values
	 * @throws IOException
	 *             if an I/O error occurs while writing to <code>filename</code>
	 */
	public static void save(String filename, Map<String, double[][]> matrices,
			Map<String, Double> scalars) throws IOException
	{
		List<MLArray> matObjs = new ArrayList<MLArray>();
		Set<String> varNames = matrices.keySet();
		Iterator<String> miter = varNames.iterator();
		while(miter.hasNext()){
			String varName = miter.next();
			matObjs.add(new MLDouble(varName, matrices.get(varName)));
		}
		
		varNames = scalars.keySet();
		Iterator<String> siter = varNames.iterator();
		while(siter.hasNext()){
			String varName = siter.next();
			matObjs.add(new MLDouble(varName, new double[][]{{scalars.get(varName)}}));
		}
		
		new MatFileWriter(filename, matObjs);
	}
}
