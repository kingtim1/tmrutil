package tmrutil.util;

/**
 * Represents a unit of measurement.
 * @author Timothy A. Mann
 *
 * @param <U> the unit type
 */
public interface Unit<U extends Unit<U>>
{
	/**
	 * Converts a scalar value from the unit type represented by this instance to a new unit type.
	 * @param val a scalar value
	 * @param newUnit the unit type to convert to
	 * @return the converted value
	 */
	public double convert(double val, U newUnit);
}
