package tmrutil.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * File system utilities.
 * 
 * @author Timothy Mann
 * 
 */
public class FSUtil {

	/**
	 * Creates a string (yyyy-mm-dd) based on the current date.
	 * 
	 * @return a string based on the current date
	 */
	public static final String date() {
		StringBuffer buff = new StringBuffer();
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		buff.append(year);
		buff.append("-");
		if (month < 10) {
			buff.append(0);
		}
		buff.append(month);
		buff.append("-");
		if (day < 10) {
			buff.append(0);
		}
		buff.append(day);
		String dirName = buff.toString();
		return dirName;
	}
	
	public static final File makeDateDir(String path)
	{
		File dir = new File(path + date());
		dir.mkdirs();
		return dir;
	}
	
	public static final File makeTimestampDir(String path){
		File dir = new File(path + timestamp());
		dir.mkdirs();
		return dir;
	}
	
	public static final File makeDataDir(String path)
	{
		File parent = new File(path);
		File dir = new File(parent, date());
		if(!dir.exists()){
			dir.mkdirs();
		}else{
			String dirname = dir.getName();
			File[] files = parent.listFiles();
			int max = 0;
			for(int i=0;i<files.length;i++){
				String filename = files[i].getName();
				if(filename.matches(Pattern.quote(dirname) + "\\s\\(\\d+\\)")){
					int ind = Integer.parseInt(filename.substring(dirname.length()+2,filename.length()-1));
					if(ind > max){
						max = ind;
					}
				}
			}
			dir = new File(dir.getPath() + " (" + (max + 1) + ")");
			dir.mkdirs();
		}
		return dir;
	}

	public static final String timestamp(){
		String tstamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar
				.getInstance().getTime());
		return tstamp;
	}
}
