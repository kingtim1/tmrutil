package tmrutil.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Enables easy collection of sets of objects mapped to arbitrary keys.
 * @author Timothy A. Mann
 *
 * @param <K> the key type
 * @param <V> the value type to store in sets
 */
public class SetCollector<K,V>
{
	private Map<K,Set<V>> _sets;
	
	public SetCollector()
	{
		_sets = new HashMap<K,Set<V>>();
	}
	
	public void insert(K key, V value)
	{
		Set<V> vset = _sets.get(key);
		if(vset == null){
			vset = new HashSet<V>();
			vset.add(value);
			_sets.put(key, vset);
		}else{
			vset.add(value);
		}
	}
	
	public Set<V> get(K key)
	{
		Set<V> vset = _sets.get(key);
		if(vset == null){
			return Collections.emptySet();
		}else{
			return Collections.unmodifiableSet(vset);
		}
	}
}
