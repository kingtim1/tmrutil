/*
 * RealDataSet.java
 */

package tmrutil.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A data set consisting of real valued vectors. All vectors in a data set must
 * have the same number of dimensions.
 * 
 * @author Timothy Mann
 * 
 */
public class RealDataSet implements DataSet<double[]>, Serializable
{
	private static final long serialVersionUID = 873368975476220360L;
	
	protected List<double[]> _data;
	protected int _dimension;

	public RealDataSet(int dimension) throws IllegalArgumentException
	{
		if (dimension < 1) {
			throw new IllegalArgumentException(
					"The size of data vectors must be larger than 0.");
		}
		_dimension = dimension;
		_data = new ArrayList<double[]>();
	}

	@Override
	public int size()
	{
		return _data.size();
	}

	@Override
	public void add(double[] element) throws IllegalArgumentException
	{
		if (_dimension != element.length) {
			throw new IllegalArgumentException(
					"The dimension of the data set (" + _dimension
							+ ") does not match the dimension of the element ("
							+ element.length + ").");
		}
		_data.add(element);
	}
	
	public double[] get(int index)
	{
		return _data.get(index);
	}

	@Override
	public Iterator<double[]> iterator()
	{
		return _data.iterator();
	}
}