package tmrutil.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * A record is a vector entry from a database or data generation process that
 * may contain numerical or categorical elements.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Record
{
	public static abstract class Type
	{
		public abstract boolean isNumerical();

		public boolean isCategorical()
		{
			return !isNumerical();
		}

		public abstract Interval bounds();

		public abstract boolean isValid(Object instance);
	}

	public static class NumericalType extends Type
	{
		private Interval _bounds;

		public NumericalType(Interval bounds)
		{
			_bounds = bounds;
		}

		@Override
		public boolean isNumerical()
		{
			return true;
		}

		@Override
		public Interval bounds()
		{
			return _bounds;
		}

		@Override
		public boolean isValid(Object instance)
		{
			if (instance instanceof Number) {
				Number val = (Number) instance;
				return isValid(val.doubleValue());
			} else {
				return false;
			}
		}

		public boolean isValid(double instance)
		{
			return _bounds.contains(instance);
		}
	}

	public static class CategoricalType extends Type
	{
		private List<?> _validCategories;
		private Interval _bounds;

		public CategoricalType(Set<?> validCategories)
		{
			_validCategories = new ArrayList<Object>(validCategories);
			_bounds = new Interval(0, true, _validCategories.size() - 1, true);
		}

		@Override
		public boolean isNumerical()
		{
			return false;
		}

		@Override
		public Interval bounds()
		{
			return _bounds;
		}

		@Override
		public boolean isValid(Object instance)
		{
			return _validCategories.contains(instance);
		}

		public Integer intCategory(Object instance)
		{
			for (int i = 0; i < _validCategories.size(); i++) {
				Object category = _validCategories.get(i);
				if (category.equals(instance)) {
					return i;
				}
			}
			throw new IllegalArgumentException("The specified instance \""
					+ instance + "\" is not a valid category.");
		}
		
		public Object categoryFromInt(Integer intCategory)
		{
			return _validCategories.get(intCategory);
		}
	}

	private List<Type> _recordTypes;
	private List<?> _entries;

	public Record(List<Type> recordTypes, Object[] entries)
	{
		this(recordTypes, Arrays.asList(entries));
	}
	
	public Record(List<Type> recordTypes, List<?> entries)
	{
		_recordTypes = new ArrayList<Type>(recordTypes);
		_entries = new ArrayList<Object>(entries);
		if (entries.size() != _recordTypes.size()) {
			throw new IllegalArgumentException(
					"The number of record entries does not match the number of record component types.");
		}
		for (int i = 0; i < _recordTypes.size(); i++) {
			Object entry = entries.get(i);
			Type type = _recordTypes.get(i);
			if (!type.isValid(entry)) {
				throw new IllegalArgumentException(
						"An invalid entry was detected while creating a record.");
			}
		}
	}
	
	public Object get(int componentIndex)
	{
		return _entries.get(componentIndex);
	}

	/**
	 * Creates an array of doubles (only containing numerical data) where each
	 * component of this record is transformed into a value in the range [0, 1].
	 * 
	 * @return a vector normalized so that it's components have values in the
	 *         range [0, 1]
	 */
	public double[] normalizedVector()
	{
		double[] nvector = new double[_entries.size()];
		for (int i = 0; i < nvector.length; i++) {
			nvector[i] = normalizedValue(i);
		}
		return nvector;
	}

	/**
	 * Returns the normalized value of a component in this record. A normalized
	 * value is a numerical value in the range [0, 1].
	 * 
	 * @param componentIndex
	 *            the index of the component to compute the normalized value for
	 * @return the normalized value of the specified component
	 */
	public double normalizedValue(int componentIndex)
	{
		Type type = componentType(componentIndex);
		Object comp = _entries.get(componentIndex);

		Number val = null;
		Interval bounds = type.bounds();
		if (type.isNumerical()) {
			val = (Number) comp;
		} else {
			CategoricalType ctype = (CategoricalType) type;
			val = ctype.intCategory(comp);
		}

		double nvalue = (val.doubleValue() - bounds.getMin())
				/ bounds.getDiff();
		return nvalue;
	}

	public Type componentType(int componentIndex)
	{
		return _recordTypes.get(componentIndex);
	}

	/**
	 * Returns the number of components in this record.
	 * 
	 * @return the length of this record
	 */
	public int length()
	{
		return _entries.size();
	}
}
