package tmrutil.util;

import java.io.Serializable;
import java.util.List;
import tmrutil.stats.Random;

/**
 * Represents an interval on the real number line. Intervals are represented by
 * minimum and maximum values and also may or may not include the minimum or
 * maximum value in the interval.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Interval implements Serializable
{
	private static final long serialVersionUID = -1317864401075695068L;

	/** Minimum value in the interval. */
	private double _min;
	/** Maximum value in the interval. */
	private double _max;
	/** True if the minimum value should be included in the interval. */
	private boolean _includeMin;
	/** True if the maximum value should be included in the interval. */
	private boolean _includeMax;

	/**
	 * Constructs an interval with the specified minimum and maximum values and
	 * determines whether the minimum and maximum values are included in the
	 * interval.
	 * 
	 * @param min
	 *            the minimum value of the interval
	 * @param includeMin
	 *            true if the minimum value should be included in the interval
	 * @param max
	 *            the maximum value of the interval
	 * @param includeMax
	 *            true if the maximum value should be included in the interval
	 */
	public Interval(double min, boolean includeMin, double max,
			boolean includeMax)
	{
		if (min > max) {
			throw new IllegalArgumentException(
					"The minimum value of an interval must be less than or equal to the maximum value.");
		}
		_min = min;
		_includeMin = includeMin;
		_max = max;
		_includeMax = includeMax;
	}

	/**
	 * Constructs a copy of the specified interval instance.
	 * 
	 * @param interval
	 *            an interval
	 */
	public Interval(Interval interval)
	{
		this(interval.getMin(), interval.includesMin(), interval.getMax(),
				interval.includesMax());
	}

	/**
	 * Constructs an inclusive interval with the specified minimum and maximum
	 * values.
	 * 
	 * @param min
	 *            the minimum value of the interval
	 * @param max
	 *            the maximum value of the interval
	 */
	public Interval(double min, double max)
	{
		this(min, true, max, true);
	}

	/**
	 * Returns true if the specified value is contained in this interval.
	 * 
	 * @param value
	 *            a real value
	 * @return true if the value is in this interval; otherwise false
	 */
	public boolean contains(double value)
	{
		if (value > _min && value < _max) {
			return true;
		} else if (value == _min && _includeMin) {
			return true;
		} else if (value == _max && _includeMax) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the minimum value of the interval.
	 * 
	 * @return the minimum value of the interval
	 */
	public double getMin()
	{
		return _min;
	}

	/**
	 * Returns true if the minimum value is in the interval.
	 * 
	 * @return true if the minimum value is in the interval; otherwise false
	 */
	public boolean includesMin()
	{
		return _includeMin;
	}

	/**
	 * Returns the maximum value of the interval.
	 * 
	 * @return the maximum value of the interval
	 */
	public double getMax()
	{
		return _max;
	}

	/**
	 * Returns true if the maximum value is included in the interval.
	 * 
	 * @return true if the maximum value is included in the interval; otherwise
	 *         false
	 */
	public boolean includesMax()
	{
		return _includeMax;
	}

	/**
	 * Returns true if the interval does not contain any values.
	 * 
	 * @return true if the interval does not contain any values; otherwise false
	 */
	public boolean isEmpty()
	{
		return (_min == _max && (!includesMin() || !includesMax()));
	}

	/**
	 * Returns the difference between the maximum and minimum value of this
	 * interval.
	 * 
	 * @return a positive scalar value
	 */
	public double getDiff()
	{
		return _max - _min;
	}

	/**
	 * Clips a specified value to fit within the valid range of this interval.
	 * If the value is greater than the maximum value, then the maximum is
	 * returned. If the value is less than the minimum, then the minimum is
	 * returned. Otherwise the value is returned.
	 * 
	 * @param val a scalar value
	 * @return a value within the valid range of this interval
	 */
	public double clip(double val)
	{
		if (val < _min) {
			return _min;
		}
		if (val > _max) {
			return _max;
		}
		return val;
	}

	/**
	 * Returns a vector containing <code>n</code> elements ranging from the
	 * minimum value to the maximum value of this interval. The values are in
	 * increasing order so that index zero refers to the minimum of the range
	 * and the largest index refers to approximately the maximum in the range.
	 * 
	 * @param n
	 *            the number of elements to break the interval into
	 * @return a vector containing <code>n</code> elements ranging from this
	 *         intervals minimum to maximum value
	 */
	public double[] range(int n)
	{
		double[] range = new double[n];
		double inc = getDiff() / n;
		for (int i = 0; i < n; i++) {
			range[i] = (i * inc) + getMin();
		}
		return range;
	}

	/**
	 * Returns a random uniformly distributed sample from this interval.
	 * 
	 * @return a uniformly distributed sample in this interval
	 * @throws IllegalStateException
	 *             if the interval is empty
	 */
	public double random() throws IllegalStateException
	{
		if (isEmpty()) {
			throw new IllegalStateException(
					"No random value can be generated because the interval is empty.");
		}
		return Random.uniform(getMin(), getMax());
	}
	
	@Override
	public String toString()
	{
		StringBuffer buff = new StringBuffer();
		if(includesMin()){
			buff.append('[');
		}else{
			buff.append('(');
		}
		buff.append(getMin());
		buff.append(',');
		buff.append(getMax());
		if(includesMax()){
			buff.append(']');
		}else{
			buff.append(')');
		}
		return buff.toString();
	}

	/**
	 * Generates a random vector from an array of intervals. The number of
	 * components of the vector is the same as the number of intervals.
	 * 
	 * @param intervals
	 *            an array of intervals
	 * @return a real valued vector
	 */
	public static final double[] random(Interval[] intervals)
	{
		double[] rand = new double[intervals.length];
		for (int i = 0; i < rand.length; i++) {
			rand[i] = intervals[i].random();
		}
		return rand;
	}
	
	/**
	 * Generates a random vector from a list of intervals. The number of components of the vector is the same as the number of intervals.
	 * 
	 * @param intervals a list of intervals
	 * @return a real valued vector
	 */
	public static final double[] random(List<Interval> intervals)
	{
		double[] rand = new double[intervals.size()];
		for(int i=0;i<rand.length;i++){
			rand[i] = intervals.get(i).random();
		}
		return rand;
	}
}
