package tmrutil.util;

import java.util.List;

/**
 * An 2-dimensional data structure.
 * 
 * @author Timothy Mann
 * 
 * @param <T>
 *            the type of data stored by this table
 */
public interface Table<T> extends Iterable<T>
{
	/**
	 * Returns the element stored at a specific row and column.
	 * 
	 * @param row
	 *            a row number
	 * @param col
	 *            a column number
	 * @return the element stored at <code>(row,col)</code>
	 * @throws IndexOutOfBoundsException
	 *             if the specified row or column are not in the table
	 */
	public T get(int row, int col) throws IndexOutOfBoundsException;

	/**
	 * Sets the value of this table at <code>(row,col)</code>.
	 * 
	 * @param val
	 *            a value
	 * @param row
	 *            a row number
	 * @param col
	 *            a column number
	 * @throws IndexOutOfBoundsException
	 *             if the specified row or column are not in the table
	 */
	public void set(T val, int row, int col) throws IndexOutOfBoundsException;

	/**
	 * Returns the elements of a specified row. The elements in the returned
	 * list are ordered by the lowest to highest column.
	 * 
	 * @param row
	 *            a row number
	 * @return the elements of a specified row
	 * @throws IndexOutOfBoundsException if the specified row is not in the table
	 */
	public List<T> getRow(int row) throws IndexOutOfBoundsException;

	/**
	 * Returns the elements of a specified column. The elements in the returned
	 * list are ordered by the lowest to highest row.
	 * 
	 * @param col
	 *            a column number
	 * @return the elements of a specified column
	 * @throws IndexOutOfBoundsException if the specified column is not in the table
	 */
	public List<T> getColumn(int col) throws IndexOutOfBoundsException;

	/**
	 * Returns the number of rows in this table.
	 * 
	 * @return the number of rows in this table
	 */
	public int numberOfRows();

	/**
	 * Returns the number of columns in this table.
	 * 
	 * @return the number of columns in this table
	 */
	public int numberOfCols();

}
