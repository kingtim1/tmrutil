package tmrutil.util;

/**
 * Each instance of this class represents a unit of angle. The primary purpose
 * of this class is to simplify the process of converting between units.
 * @author Timothy A. Mann
 *
 */
public class AngularUnit implements Unit<AngularUnit>
{
	public static final AngularUnit DEGREES = new AngularUnit("Degrees", 1);
	public static final AngularUnit RADIANS = new AngularUnit("Radians", 57.2957795);
	
	private String _unitLabel;
	private double _numDegrees;
	
	private AngularUnit(String unitLabel, double numDegrees)
	{
		if (unitLabel == null) {
			throw new NullPointerException("Unit label cannot be null.");
		}
		_unitLabel = unitLabel;
		_numDegrees = numDegrees;
	}

	@Override
	public double convert(double val, AngularUnit newUnit)
	{
		if(this == newUnit){
			return val;
		}else{
			double scale = _numDegrees / newUnit._numDegrees;
			return scale * val;
		}
	}
	
	@Override
	public String toString()
	{
		return _unitLabel;
	}

}
