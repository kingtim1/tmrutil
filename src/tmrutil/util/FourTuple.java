package tmrutil.util;

public class FourTuple <A,B,C,D> extends ThreeTuple<A,B,C>{

	private D _d;
	
	public FourTuple(A a, B b, C c, D d){
		super(a,b,c);
		_d = d;
	}
	
	public final D getD()
	{
		return _d;
	}
	
	@Override
	public String toString()
	{
		return "[" + getA() + ", " + getB() + ", " + getC() + ", " + getD() + "]";
	}
}
