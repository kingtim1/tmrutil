package tmrutil.util;

import java.lang.reflect.TypeVariable;
import java.util.Comparator;
import java.util.List;
import tmrutil.math.DimensionMismatchException;

/**
 * Provides lexicographical ordering for lists of objects with natural ordering.
 * @author Timothy Mann
 *
 * @param <T> a type that implements the <code>Comparable</code> interface
 */
public class LexicographicalListComparator<T extends Comparable<T>> implements
		Comparator<List<T>>
{
	@Override
	public int compare(List<T> t1, List<T> t2)
			throws DimensionMismatchException
	{
		if (t1.size() == t2.size()) {
			for (int i = 0; i < t1.size(); i++) {
				T t1Obj = t1.get(i);
				T t2Obj = t2.get(i);
				int ans = t1Obj.compareTo(t2Obj);
				if (ans != 0) {
					return ans;
				}
			}
			return 0;
		}
		throw new DimensionMismatchException(
				"Lexicographical ordering is only defined for vectors of the same length.");
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof LexicographicalListComparator) {
			TypeVariable<?>[] ttp = getClass().getTypeParameters();
			TypeVariable<?>[] otp = obj.getClass().getTypeParameters();
			if (ttp.length == otp.length) {
				for (int i = 0; i < ttp.length; i++) {
					if (!ttp.equals(otp)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
}
