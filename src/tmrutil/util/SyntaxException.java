package tmrutil.util;

/**
 * Thrown to indicate that expected syntax was (somehow) violated.
 * @author Timothy A. Mann
 *
 */
public class SyntaxException extends IllegalArgumentException
{

	public SyntaxException()
	{
		// TODO Auto-generated constructor stub
	}

	public SyntaxException(String s)
	{
		super(s);
		// TODO Auto-generated constructor stub
	}

	public SyntaxException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SyntaxException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
