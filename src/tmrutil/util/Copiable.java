package tmrutil.util;

/**
 * This interface should be implemented by classes that provide an
 * implementation for deep copy of an instance.
 * 
 * @author Timothy A. Mann
 * 
 * @param <T>
 *            the type of instances that can be copied
 */
public interface Copiable<T>
{
	/**
	 * Creates a new object, which is a deep copy of the instance referred to by
	 * <code>this</code>.
	 */
	public T deepCopy();
}
