package tmrutil.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A general purpose counter of objects.
 * @author Timothy A. Mann
 *
 * @param <K> the type of objects to count
 */
public class Counter<K>
{
	private Map<K, Integer> _counts;

	public Counter()
	{
		_counts = new HashMap<K, Integer>();
	}

	public void increment(K instance)
	{
		Integer count = _counts.get(instance);
		if (count == null) {
			_counts.put(instance, new Integer(1));
		} else {
			_counts.put(instance, new Integer(count.intValue() + 1));
		}
	}

	public int count(K instance)
	{
		Integer count = _counts.get(instance);
		if (count == null) {
			return 0;
		} else {
			return count.intValue();
		}
	}
	
	public Set<K> nonzero(){
		return _counts.keySet();
	}

	public void reset()
	{
		_counts.clear();
	}
}
