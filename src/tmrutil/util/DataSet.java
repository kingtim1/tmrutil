/*
 * DatasSet.java
 */

/* Package */
package tmrutil.util;

/**
 * A generic data set.
 * @author Timothy Mann
 *
 * @param <T> the type of data which that data set consists of.
 */
public interface DataSet<T> extends Iterable<T>
{
	/**
	 * Returns the number of elements contained in this data set.
	 * @return the number of elements contained in this data set.
	 */
	public int size();
	
	/**
	 * Adds an element to this data set.
	 * @param element a single element
	 * @throws IllegalArgumentException if the element is invalid
	 */
	public void add(T element) throws IllegalArgumentException;
}
