package tmrutil.util;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * A utility class for storing arbitrary strings to a zip file format. This
 * class supports random access reading and writing of strings.
 * 
 * @author Timothy A. Mann
 * 
 */
public class StringFileStorage implements Closeable {

	private static class ZipEntryToKeyIterator implements Iterator<String> {
		private Enumeration<? extends ZipEntry> _enum;
		private Iterator<String> _inMemory;

		public ZipEntryToKeyIterator(Enumeration<? extends ZipEntry> entries,
				Map<String, String> inMemory) {
			_enum = entries;
			_inMemory = inMemory.keySet().iterator();
		}

		@Override
		public boolean hasNext() {
			return (_enum.hasMoreElements() || _inMemory.hasNext());
		}

		@Override
		public String next() {
			if (_enum.hasMoreElements()) {
				ZipEntry entry = _enum.nextElement();
				return entry.getName();
			} else {
				return _inMemory.next();
			}
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException(
					"Removing labels is not supported by "
							+ getClass().getName() + ".");
		}
	}

	public static final int DEFAULT_BUFFER_SIZE = 100;

	private File _file;
	private byte[] _buff = new byte[1024];
	private Map<String, String> _buffMap;
	private int _bufferSize;

	/**
	 * Constructs a StringFileStorage instance for storing strings.
	 * 
	 * @param file
	 *            the zip archive file to store strings in; if the file does not
	 *            already exist, then a new file is created
	 * @param append
	 *            true indicates that new strings should be appended to the
	 *            specified file; false indicates that any content in the zip
	 *            file should be overwritten
	 * @throws IOException
	 *             if an I/O error occurs while opening the file for writing
	 */
	public StringFileStorage(File file) throws IOException {
		if (!file.exists()) {
			file.createNewFile();
		}
		_file = file;
		_buffMap = new HashMap<String, String>();
		_bufferSize = DEFAULT_BUFFER_SIZE;
	}

	/**
	 * Clears all of the data in this string storage.
	 * 
	 * @throws IOException
	 *             if an I/O exception occurs while clearing the data
	 */
	public void clear() throws IOException {
		Path filePath = Paths.get(_file.toURI());
		Files.deleteIfExists(filePath);
		_buffMap.clear();
	}

	/**
	 * Returns true if the key is being used by this string storage instance.
	 * 
	 * @param key
	 *            a string to check if it is a key
	 * @return true if <code>key</code> refers to a value
	 * @throws IOException
	 *             if an I/O error occurs while checking if a key exists
	 */
	public boolean containsKey(String key) throws IOException {
		return keys().contains(key);
	}

	/**
	 * Returns an iterator over the keys in this string storage archive.
	 * 
	 * @return an iterator over the keys
	 * @throws IOException
	 *             if an I/O error occurs while reading key information from the
	 *             underlying archive file
	 */
	public Iterator<String> keyIterator() throws IOException {
		ZipFile zipFile = new ZipFile(_file);
		Enumeration<? extends ZipEntry> keyEnum = zipFile.entries();
		return new ZipEntryToKeyIterator(keyEnum, _buffMap);
	}

	/**
	 * Returns a set containing all keys that are currently being used.
	 * 
	 * @return a set containing all keys
	 * @throws IOException
	 *             if an I/O error occurs while reading the keys
	 */
	public Set<String> keys() throws IOException {
		try {
			ZipFile zipFile = new ZipFile(_file);
			Enumeration<? extends ZipEntry> keyEnum = zipFile.entries();
			Set<String> keys = new HashSet<String>();
			while (keyEnum.hasMoreElements()) {
				ZipEntry entry = keyEnum.nextElement();
				keys.add(entry.getName());
			}
			keys.addAll(_buffMap.keySet());
			return keys;
		} catch (ZipException ex) {
			return new HashSet<String>();
		}
	}

	/**
	 * Flushes the in memory data to the archive file.
	 * 
	 * @throws IOException
	 *             if an I/O error occurs while flushing the in memory data to
	 *             the archive file
	 */
	public void flush() throws IOException {
		File tmpFile = new File(_file.getPath() + ".tmp");
		tmpFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(tmpFile);
		ZipOutputStream ostream = new ZipOutputStream(fos);

		try {
			ZipFile zipFile = new ZipFile(_file);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				String ename = entry.getName();
				if (!_buffMap.containsKey(ename)) {
					InputStream istream = zipFile.getInputStream(entry);
					ostream.putNextEntry(entry);

					int bytesRead = 0;
					while ((bytesRead = istream.read(_buff)) != -1) {
						ostream.write(_buff, 0, bytesRead);
					}

					ostream.closeEntry();
					istream.close();
				}
			}
		} catch (ZipException ex) {
			// Don't do anything. Just start assume the file was empty.
		}

		for (String key : _buffMap.keySet()) {
			String value = _buffMap.get(key);
			ostream.putNextEntry(new ZipEntry(key));
			byte[] bytes = value.getBytes();
			ostream.write(bytes);
			ostream.closeEntry();
		}
		ostream.close();

		Path tmpFilePath = Paths.get(tmpFile.toURI());
		Path filePath = Paths.get(_file.toURI());
		Files.move(tmpFilePath, filePath, StandardCopyOption.REPLACE_EXISTING);

		_buffMap.clear();
	}

	/**
	 * Stores the (key, value) pair in the underlying zip file.
	 * 
	 * @param key
	 *            the key for this string data
	 * @param value
	 *            the value of the string data
	 * @throws IOException
	 *             if an I/O error occurs while writing (key, value) to the zip
	 *             file
	 */
	public void store(String key, String value) throws IOException {
		if (key == null || value == null) {
			throw new NullPointerException(
					"Cannot store strings with null label or null value.");
		}

		_buffMap.put(key, value);
		if (_buffMap.size() >= _bufferSize) {
			flush();
		}
	}

	/**
	 * Retrieves a value string by its key.
	 * 
	 * @param key
	 *            a key string associated with some value in the underlying zip
	 *            file
	 * @return the value string associated with <code>key</code>
	 * @throws IOException
	 *             if an I/O error occurs while reading the value string
	 *             associated with <code>key</code>
	 */
	public String retrieve(String key) throws IOException {
		if (key == null) {
			throw new NullPointerException(
					"Cannot retrieve value string with null label.");
		}

		// Check if it's in the in memory buffer first
		String value = _buffMap.get(key);
		if(value != null){
			return value;
		}
		
		// Check if it's in the zip archive
		ZipFile zipFile = new ZipFile(_file);
		InputStream istream = zipFile.getInputStream(new ZipEntry(key));
		InputStreamReader reader = new InputStreamReader(istream);
		BufferedReader breader = new BufferedReader(reader);
		StringBuilder sbuff = new StringBuilder();
		String line = null;
		while ((line = breader.readLine()) != null) {
			sbuff.append(line);
		}
		return sbuff.toString();
	}

	/**
	 * Returns the number of (label, value) entries that are stored.
	 * 
	 * @return the number of stored entries
	 * @throws IOException
	 *             if an I/O error occurs while determining the number of
	 *             entries in the zip archive
	 */
	public int numberOfEntries() throws IOException {
		try {
			ZipFile zipFile = new ZipFile(_file);
			return zipFile.size();
		} catch (ZipException ex) {
			return 0;
		}
	}

	@Override
	public void close() throws IOException {
		flush();
	}

	public static void main(String[] args) throws Exception {
		String filename = System.getProperty("user.home")
				+ "/str_storage_test.zip";
		File file = new File(filename);
		StringFileStorage sfs = new StringFileStorage(file);
		Set<String> keys = sfs.keys();
		for (String key : keys) {
			System.out.println("Label: " + key);
			System.out.println("Value: " + sfs.retrieve(key));
		}

		sfs.store("a", "this is a test");
		sfs.store("b", "this is another test");
		sfs.store("c", "testing yet again");
		sfs.store("d", "how about another test");
		sfs.store("e", "more tests? ok this is getting old");
		sfs.store("f", "this is a test\nwith a new line character");
		sfs.store("g", "last test");

		sfs.close();
	}
}
