package tmrutil.util;

/**
 * Implements a heap tree-like data structure which is frequently used to
 * implement priority queues. All heaps satisfy the property that if X and Y are
 * nodes in a heap and Y is a child of X, then the key of X is greater than the
 * key of Y (if the heap is a max-heap and vice-versa if the heap is a min-heap).
 * 
 * @author Timothy A. Mann
 * 
 * @param <P>
 *            the priority type
 * @param <V>
 *            the value type
 */
public interface Heap<P, V>
{
	/**
	 * Inserts a priority-value pair into this heap.
	 * 
	 * @param priority
	 *            a priority
	 * @param value
	 *            a value
	 */
	public void insert(P priority, V value);
	
	/**
	 * Removes all heap entries that match the specified value.
	 * @param value the value to remove
	 * @return true if any entries are removed; otherwise false
	 */
	public boolean remove(V value);

	/**
	 * Removes the root of the heap and returns the key-value pair.
	 * 
	 * @return the key-value pair from the root of the heap
	 */
	public Pair<P, V> pop();

	/**
	 * Removes the root of the heap and returns its associated priority.
	 * 
	 * @return the priority from the root of the heap
	 */
	public P popPriority();

	/**
	 * Removes the root of the heap and returns its associated value.
	 * 
	 * @return the value from the root of the heap
	 */
	public V popValue();

	/**
	 * Returns a reference to the priority-value pair at the root of this heap but
	 * does not remove it from this heap.
	 * 
	 * @return the priority-value pair from the root of the heap
	 */
	public Pair<P, V> peek();

	/**
	 * Returns a reference to the priority at the root of this heap but does not
	 * remove it.
	 * 
	 * @return the priority from the root of the heap
	 */
	public P peekPriority();

	/**
	 * Returns a reference to the value at the root of this heap but does not
	 * remove it.
	 * 
	 * @return the value from the root of the heap
	 */
	public V peekValue();

	/**
	 * Returns the number of elements in this heap.
	 * 
	 * @return the number of elements in this heap
	 */
	public int size();
	
	/**
	 * Remove all elements from this heap.
	 */
	public void clear();
}
