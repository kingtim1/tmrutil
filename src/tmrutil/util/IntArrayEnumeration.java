package tmrutil.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import tmrutil.math.VectorOps;

/**
 * A utility class for enumerating all possible combinations of some set of
 * elements.
 * 
 * @author Timothy A. Mann
 * 
 */
public class IntArrayEnumeration implements Iterator<int[]>, Iterable<int[]>,
		Enumeration<int[]>
{
	private List<int[]> _valueSets;
	private int[] _setSizes;
	private int _numElements;

	private int[] _valCounts;
	private int _numGenerated;

	public IntArrayEnumeration(List<Set<Integer>> valueSets)
	{
		_valueSets = new ArrayList<int[]>(valueSets.size());
		_valCounts = new int[valueSets.size()];
		_numGenerated = 0;
		_setSizes = new int[valueSets.size()];
		_numElements = 1;
		for (int i = 0; i < valueSets.size(); i++) {
			Set<Integer> valSet = valueSets.get(i);
			if (valSet.isEmpty()) {
				throw new IllegalArgumentException(
						"Cannot enumerate int[] with an empty value set.");
			} else {
				_numElements *= valSet.size();
				_setSizes[i] = valSet.size();
			}

			int[] vals = new int[valSet.size()];
			int j = 0;
			for (Integer val : valSet) {
				vals[j] = val.intValue();
				j++;
			}
			_valueSets.add(vals);
		}

		reset();
	}
	
	public IntArrayEnumeration(int[] maxValues){
		this(buildValueSets(maxValues));
	}
	
	public static final List<Set<Integer>> buildValueSets(int[] maxValues)
	{
		List<Set<Integer>> valSets = new ArrayList<Set<Integer>>(maxValues.length);
		for(int i=0;i<maxValues.length;i++){
			Set<Integer> vset = new HashSet<Integer>();
			for(int v = 0; v <= maxValues[i]; v++){
				vset.add(v);
			}
			valSets.add(vset);
		}
		return valSets;
	}

	@Override
	public boolean hasMoreElements()
	{
		return hasNext();
	}

	@Override
	public int[] nextElement()
	{
		return next();
	}

	@Override
	public boolean hasNext()
	{
		return (_numGenerated < _numElements);
	}

	@Override
	public int[] next()
	{
		if (!hasNext()) {
			throw new NoSuchElementException(
					"No more elements are left in this enumeration.");
		}
		_numGenerated++;
		int[] g = new int[_valueSets.size()];

		for (int j = 0; j < g.length; j++) {
			g[j] = _valueSets.get(j)[_valCounts[j]];
		}

		if (_numGenerated < _numElements) {
			int i = 0;
			while (true) {
				_valCounts[i]++;
				if (_valCounts[i] >= _setSizes[i]) {
					for (int j = i; j >= 0; j--) {
						_valCounts[j] = 0;
					}
					i++;
				} else {
					break;
				}
			}
		}

		return g;
	}

	@Override
	public Iterator<int[]> iterator()
	{
		return this;
	}

	/**
	 * This optional operation is not implemented by this class. If this method
	 * is invoked, then a {@link DebugException} is thrown.
	 */
	@Override
	public void remove()
	{
		throw new DebugException("The remove operation is not implemented by"
				+ getClass() + ".");
	}

	/**
	 * Returns the total number of elements produced by this enumeration.
	 * 
	 * @return the total number of elements produced by this enumeration
	 */
	public int size()
	{
		return _numElements;
	}

	/**
	 * Returns the total number of elements produced by this enumeration.
	 * 
	 * @return the total number of elements produced by this enumeration
	 */
	public int numberOfElements()
	{
		return _numElements;
	}

	/**
	 * Resets this enumeration to the first element.
	 */
	public void reset()
	{
		_valCounts = new int[_setSizes.length];
		_numGenerated = 0;
	}

	public static void main(String[] args)
	{
		List<Set<Integer>> test = new ArrayList<Set<Integer>>();
		Set<Integer> setA = new HashSet<Integer>();
		setA.add(0);
		setA.add(1);
		setA.add(2);
		test.add(setA);

		Set<Integer> setB = new HashSet<Integer>();
		setB.add(3);
		setB.add(4);
		setB.add(5);
		test.add(setB);

		Set<Integer> setC = new HashSet<Integer>();
		setC.add(6);
		setC.add(7);
		test.add(setC);

		IntArrayEnumeration ienum = new IntArrayEnumeration(test);
		for (int[] a : ienum) {
			System.out.println(VectorOps.toString(a));
		}
	}
}
