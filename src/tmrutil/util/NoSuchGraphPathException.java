package tmrutil.util;

/**
 * Thrown to indicate that the path we are looking for does not exist in the
 * graph we are searching.
 * 
 * @author Timothy A. Mann
 * 
 */
public class NoSuchGraphPathException extends Exception
{

	private static final long serialVersionUID = -5938992834572197641L;

	public NoSuchGraphPathException()
	{
		super();
	}

	public NoSuchGraphPathException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public NoSuchGraphPathException(String message)
	{
		super(message);
	}

	public NoSuchGraphPathException(Throwable cause)
	{
		super(cause);
	}

}
