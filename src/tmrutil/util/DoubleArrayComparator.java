package tmrutil.util;

import java.util.Comparator;
import tmrutil.math.DimensionMismatchException;

public class DoubleArrayComparator implements Comparator<double[]>
{

	@Override
	public int compare(double[] o1, double[] o2)
	{
		if (o1.length == o2.length) {
			for (int i = 0; i < o1.length; i++) {
				Double t1Obj = o1[i];
				Double t2Obj = o2[i];
				int ans = t1Obj.compareTo(t2Obj);
				if (ans != 0) {
					return ans;
				}
			}
			return 0;
		}
		throw new DimensionMismatchException(
				"Lexicographical ordering is only defined for vectors of the same length.");
	}
	
	@Override
	public boolean equals(Object object)
	{
		return object instanceof DoubleArrayComparator;
	}

}
