/*
 * Sort.java
 */

/* Package */
package tmrutil.util;

/* Imports */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Various methods for sorting arrays.
 */
public class Sort
{
	private static final DoubleComparator	DOUBLE_COMPARATOR_ASCEND = new DoubleComparator(true);
	private static final IntegerComparator INTEGER_COMPARATOR_ASCEND = new IntegerComparator(true);
	private static final DoubleComparator	DOUBLE_COMPARATOR_DESCEND = new DoubleComparator(false);
	private static final IntegerComparator INTEGER_COMPARATOR_DESCEND = new IntegerComparator(false);

	static class DoubleComparator implements Comparator<Double>
	{
		private boolean _ascend;
		
		public DoubleComparator(boolean ascend){
			_ascend = ascend;
		}
		
		public int compare(Double o1, Double o2)
		{
			if(_ascend){
				return o1.compareTo(o2);
			}else{
				return o2.compareTo(o1);
			}
		}

		@Override
		public boolean equals(Object o)
		{
			if(o instanceof DoubleComparator){
				DoubleComparator dc = (DoubleComparator)o;
				return dc._ascend == _ascend;
			}
			return false;
		}
	}
	
	static class IntegerComparator implements Comparator<Integer>
	{
		private boolean _ascend;
		
		public IntegerComparator(boolean ascend)
		{
			_ascend = ascend;
		}
		
		public int compare(Integer o1, Integer o2)
		{
			if(_ascend){
			return o1.compareTo(o2);
			}else{
				return o2.compareTo(o1);
			}
		}
		
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof IntegerComparator){
				IntegerComparator ic = (IntegerComparator)o;
				return ic._ascend == _ascend;
			}
			return false;
		}
	}
	
	static class DoublePairComparator<T> implements Comparator<Pair<T,Double>>{
		private DoubleComparator _dcmp;
		
		public DoublePairComparator(boolean ascend){
			_dcmp = new DoubleComparator(ascend);
		}
		
		@Override
		public int compare(Pair<T, Double> o1, Pair<T, Double> o2) {
			return _dcmp.compare(o1.getB(), o2.getB());
		}
		
	}
	
	static class IntegerPairComparator<T> implements Comparator<Pair<T,Integer>>{
		private IntegerComparator _icmp;
		
		public IntegerPairComparator(boolean ascend)
		{
			_icmp = new IntegerComparator(ascend);
		}
		
		@Override
		public int compare(Pair<T, Integer> o1, Pair<T, Integer> o2) {
			return _icmp.compare(o1.getB(), o2.getB());
		}
		
	}

	public static final int[] sortIndex(double[] l, boolean ascend)
	{
		List<Pair<Integer,Double>> copy = new ArrayList<Pair<Integer,Double>>();
		for (int i = 0; i < l.length; i++)
		{
			copy.add(new Pair<Integer,Double>(i,l[i]));
		}
		Collections.sort(copy, new DoublePairComparator<Integer>(ascend));
		int[] inds = new int[copy.size()];
		for(int i=0;i<copy.size();i++){
			inds[i] = copy.get(i).getA();
		}
		return inds;
	}
	
	public static final int[] sortIndex(int[] l, boolean ascend)
	{
		List<Pair<Integer,Double>> copy = new ArrayList<Pair<Integer,Double>>();
		for (int i = 0; i < l.length; i++)
		{
			copy.add(new Pair<Integer,Double>(i,new Double(l[i])));
		}
		Collections.sort(copy, new DoublePairComparator<Integer>(ascend));
		int[] inds = new int[copy.size()];
		for(int i=0;i<copy.size();i++){
			inds[i] = copy.get(i).getA();
		}
		return inds;
	}

	public static final <T> int[] sortIndex(T[] l, Comparator<T> c, boolean ascend)
	{
		int[] ind = new int[l.length];
		for (int i = 0; i < l.length; i++)
		{
			ind[i] = i;
		}

		return mergeSortIndex(l, ind, c, 0, l.length - 1, ascend);
	}

	private static final <T> int[] mergeSortIndex(T[] l, int[] ind, Comparator<T> c, int firstInd,
			int lastInd, boolean ascend)
	{
		if (firstInd < lastInd)
		{
			int midInd = (firstInd + lastInd) / 2;
			mergeSortIndex(l, ind, c, firstInd, midInd, ascend);
			mergeSortIndex(l, ind, c, midInd + 1, lastInd, ascend);

			// Merge the index lists
			int[] b = Arrays.copyOfRange(ind, firstInd, lastInd + 1);

			int i = 0;
			int j = midInd + 1 - firstInd;
			int k = firstInd;

			while ((i + firstInd) <= midInd && (j + firstInd) <= lastInd)
			{
				if (ascend)
				{
					// if l[b[i-firstInd]] <= l[b[j-firstInd]]
					if (c.compare(l[b[i]], l[b[j]]) < 0)
					{
						ind[k++] = b[i++];
					}
					else
					{
						ind[k++] = b[j++];
					}
				}
				else
				{
					if (c.compare(l[b[i]], l[b[j]]) >= 0)
					{
						ind[k++] = b[i++];
					}
					else
					{
						ind[k++] = b[j++];
					}
				}
			}

			while ((i + firstInd) <= midInd)
			{
				ind[k++] = b[i++];
			}
		}
		return ind;
	}

	public static final double[] permute(double[] a, int[] ind)
	{
		double[] copy = Arrays.copyOf(a, a.length);
		for (int i = 0; i < a.length; i++)
		{
			copy[i] = a[ind[i]];
		}
		return copy;
	}
	
	public static final int[] permute(int[] a, int[] ind)
	{
		int[] copy = Arrays.copyOf(a, a.length);
		for(int i=0;i<a.length;i++){
			copy[i] = a[ind[i]];
		}
		return copy;
	}

	public static final <T> T[] permute(T[] a, int[] ind)
	{
		T[] copy = Arrays.copyOf(a, a.length);
		for (int i = 0; i < a.length; i++)
		{
			copy[i] = a[ind[i]];
		}
		return copy;
	}

	private Sort()
	{
	}

	public static void main(String[] args)
	{
		java.util.Random rand = new java.util.Random();
		double[] data = new double[10];
		for (int i = 0; i < data.length; i++)
		{
			data[i] = rand.nextInt() % 10;
		}
		System.out.println(tmrutil.math.VectorOps.toString(data));
		int[] ind = Sort.sortIndex(data, true);
		data = Sort.permute(data, ind);
		System.out.println(tmrutil.math.VectorOps.toString(data));
		ind = Sort.sortIndex(data, false);
		data = Sort.permute(data, ind);
		System.out.println(tmrutil.math.VectorOps.toString(data));
	}
}