package tmrutil.graphics3d;

import java.awt.Graphics2D;

/**
 * A shape that can be rendered on an instance of {@link tmrutil.graphics3d.Canvas3D}.
 * @author Timothy A. Mann
 *
 */
public interface Shape3D
{
	public void draw(Graphics2D g, Camera3D camera);
}
