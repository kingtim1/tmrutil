package tmrutil.graphics3d;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import tmrutil.math.VectorOps;
import tmrutil.math.geometry.Point3D;
import tmrutil.util.Pair;

public class Line3D implements Shape3D
{
	private Point3D _a;
	private Point3D _b;
	
	private double[] _slope;
	private double[] _intercept;

	public Line3D(Point3D a, Point3D b)
	{
		_a = a;
		_b = b;
		
		_slope = new double[3];
		_intercept = new double[3];
		
		solve(_a, _b);
	}

	private void solve(Point3D a, Point3D b)
	{
		_slope[0] = a.getX() - b.getX();
		_slope[1] = a.getY() - b.getY();
		_slope[2] = a.getZ() - b.getZ();
		
		_intercept[0] = a.getX() - _slope[0] * a.getZ();
		_intercept[1] = a.getY() - _slope[1] * a.getZ();
		_intercept[2] = a.getZ() - _slope[2] * a.getZ();
	}
	
	@Override
	public void draw(Graphics2D g, Camera3D camera)
	{
		Point3D a = _a;
		Point3D b = _b;
		
		Point3D camPos = camera.getPosition();
		if(a.getZ() - camPos.getZ() > 0 && b.getZ() - camPos.getZ() > 0){
			Point2D a2D = camera.project(a);
			Point2D b2D = camera.project(b);

			Line2D line = new Line2D.Double(a2D, b2D);
			g.draw(line);
		}
	}

	
	
	public Pair<Point3D,Point3D> clip(Camera3D camera)
	{
		Point3D camPos = camera.getPosition();
		if(_a.getZ() - camPos.getZ() < 0 && _b.getZ() - camPos.getZ() < 0){
			return new Pair<Point3D,Point3D>(_a, _b);
		}else if(_a.getZ() - camPos.getZ() < 0 && _b.getZ() - camPos.getZ() > 0){
			Point3D p0 = new Point3D(_intercept[0], _intercept[1], _intercept[2]);
			return new Pair<Point3D,Point3D>(p0,_b);
		}else if(_a.getZ() - camPos.getZ() > 0 && _b.getZ() - camPos.getZ() < 0){
			Point3D p0 = new Point3D(_intercept[0], _intercept[1], _intercept[2]);
			return new Pair<Point3D,Point3D>(_a, p0);
		}else{
			return new Pair<Point3D,Point3D>(_a, _b);
		}
	}
}
