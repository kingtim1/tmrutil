package tmrutil.graphics3d;

import java.awt.Graphics2D;
import tmrutil.math.geometry.Point3D;

public class Rectangle3D implements Shape3D
{
	private Point3D _origin;
	private double _width;
	private double _height;
	
	public Rectangle3D(Point3D origin, double width, double height)
	{
		_origin = new Point3D(origin.getX(), origin.getY(), origin.getZ());
		_width = width;
		_height = height;
	}
	
	@Override
	public void draw(Graphics2D g, Camera3D camera)
	{
		Point3D bottomLeft = _origin;
		Point3D bottomRight = new Point3D(_origin.getX()+_width,_origin.getY(), _origin.getZ());
		Point3D topLeft = new Point3D(_origin.getX(), _origin.getY(), _origin.getZ() + _height);
		Point3D topRight = new Point3D(_origin.getX()+_width, _origin.getY(), _origin.getZ() + _height);
		
		Line3D top = new Line3D(topLeft, topRight);
		Line3D left = new Line3D(topLeft, bottomLeft);
		Line3D right = new Line3D(topRight, bottomRight);
		Line3D bottom = new Line3D(bottomLeft, bottomRight);
		
		top.draw(g, camera);
		left.draw(g, camera);
		right.draw(g, camera);
		bottom.draw(g, camera);
	}

}
