package tmrutil.xml;

import org.w3c.dom.Element;

/**
 * Implemented by classes that are able to construct an instance of
 * <code>T</code> from an XML representation of an instance of <code>T</code>.
 * 
 * @author Timothy Mann
 * 
 * @param <T>
 */
public interface FromXML<T>
{
	/**
	 * Constructs an instance of <code>T</code> from an XML representation of an
	 * instance of <code>T</code>.
	 * 
	 * @param element
	 *            an XML representation of an instance of type <code>T</code>
	 * @return an instance of <code>T</code>
	 * @throws IllegalArgumentException
	 *             if <code>element</code> does not contain a sufficient
	 *             representation for constructing an instance of <code>T</code>
	 */
	public T fromXML(Element element) throws IllegalArgumentException;
}
