/*
 * XMLUtil.java
 */

/* Package */
package tmrutil.xml;

/* Imports */
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Attr;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLUtil
{

	private static DocumentBuilder DOC_BUILDER;
	static {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DOC_BUILDER = factory.newDocumentBuilder();
		} catch (ParserConfigurationException ex) {
			DOC_BUILDER = null;
		}
	};

	/**
	 * Returns a document builder object.
	 */
	public static final DocumentBuilder getDocumentBuilder()
	{
		return DOC_BUILDER;
	}

	/**
	 * Creates and returns an empty XML document.
	 */
	public static final Document createDocument()
	{
		return DOC_BUILDER.newDocument();
	}
	
	/**
	 * Parse an XML file producing a document.
	 */
	public static final Document parse(String filename)
		throws IOException, SAXException
	{
		return DOC_BUILDER.parse(new File(filename));
	}
	
	/**
	 * Parse an XML input stream producing a document.
	 * @param is an input stream
	 * @return an XML document
	 */
	public static final Document parse(InputStream is)
		throws IOException, SAXException
	{
		return DOC_BUILDER.parse(is);
	}
	
	/**
	 * Parse an XML file producing a document.
	 * @param file an XML file
	 * @return an XML document
	 * @throws IOException if an I/O error occurs while reading from the file
	 * @throws SAXException if an error occurs while parsing XML
	 */
	public static final Document parse(File file)
		throws IOException, SAXException
	{
		return DOC_BUILDER.parse(file);
	}
	
	/**
	 * Returns the string representation of the XML node.
	 * @param node a node
	 * @return the string representation of the XML node
	 */
	public static final String toString(Node node)
	{
		if(node instanceof Document){
			return toString((Document)node);
		}
		if(node instanceof Element){
			return toString((Element)node);
		}
		if(node instanceof CharacterData){
			return ((CharacterData)node).getData();
		}
		if(node instanceof Attr){
			return toString((Attr)node);
		}
		throw new IllegalArgumentException("The node type " + node.getClass() + " is not supported.");
	}
	
	private static final String toString(Document document)
	{
		String encoding = document.getXmlEncoding();
		if(encoding == null){
			encoding = "UTF-8";
		}
		String xmlString = "<?xml version=\""+document.getXmlVersion()+"\" encoding=\""+encoding+"\"?>\n\n";
		return xmlString + toString(document.getDocumentElement());
	}
	
	private static final String toString(Element element)
	{
		StringBuffer xmlBuff = new StringBuffer("<" + element.getTagName());
		NamedNodeMap attributes = element.getAttributes();
		for(int i=0;i<attributes.getLength();i++){
			Attr attr = (Attr)attributes.item(i);
			xmlBuff.append(" " + toString(attr));
		}
		NodeList children = element.getChildNodes();
		if(children.getLength() == 0){
			xmlBuff.append("/>\n");
		}else{
			xmlBuff.append(">\n");
			for(int i=0;i<children.getLength();i++){
				Node node = children.item(i);
				xmlBuff.append(toString(node));
			}
			xmlBuff.append("</" + element.getTagName() + ">\n");
		}
		return xmlBuff.toString();
	}
	
	private static final String toString(Attr attribute)
	{
		String xmlString = attribute.getName() + "=\"" + attribute.getValue() + "\"";
		return xmlString;
	}
	
	/**
	 * Cannot construct an XMLUtil object.
	 */
	private XMLUtil()
	{
	}
	
	public static void main(String[] args)
	 throws IOException,SAXException
	{
		javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();
		int answer = chooser.showOpenDialog(null);
		if(answer == javax.swing.JFileChooser.APPROVE_OPTION){
			File f = chooser.getSelectedFile();
			Document document = parse(f);
			System.out.println(toString(document));
		}
	}
}