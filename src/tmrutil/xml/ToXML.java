package tmrutil.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Implemented by classes that produce XML representations from instances of class <code>T</code>.
 * @author Timothy Mann
 *
 * @param <T>
 */
public interface ToXML<T>
{
	/**
	 * Constructs an XML representation from an instance of type <code>T</code>.
	 * @param t an instance
	 * @param document the document that the element will belong to
	 * @return an XML Element
	 */
	public Element toXML(T t, Document document);
}
