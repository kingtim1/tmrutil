package tmrutil.kinematics;

public interface ReinforcementSignal3<S, A>
{
	/**
	 * Evaluates the behavior of a reinforcement learning algorithm.
	 * @param prevState the previous state
	 * @param action the executed action
	 * @param newState the new state
	 * @return a scalar reinforcement
	 */
	public double evaluate(S prevState, A action, S newState);
}
