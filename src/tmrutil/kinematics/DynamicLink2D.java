package tmrutil.kinematics;

import tmrutil.util.Interval;

/**
 * Represents a dynamic link in a kinematic chain.
 * @author Timothy A. Mann
 *
 */
public class DynamicLink2D extends Link2D
{
	private double _mass;
	private double _cog;
	private double _inertia;
	private double _jFriction;

	public DynamicLink2D(double length, Interval bounds, double mass, double centerOfGravity, double inertia, double jointFriction)
	{
		super(length, bounds);
		if(mass <= 0){
			throw new IllegalArgumentException("Detected nonpositive mass : " + mass);
		}
		_mass = mass;
		_cog = centerOfGravity;
		_inertia = inertia;
		_jFriction = jointFriction;
	}
	
	public double getMass()
	{
		return _mass;
	}
	
	public double getCenterOfGravity()
	{
		return _cog;
	}
	
	public double getInertia()
	{
		return _inertia;
	}
	
	public double getJointFriction()
	{
		return _jFriction;
	}

}
