package tmrutil.kinematics;

import java.awt.Graphics2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;

/**
 * The objective of this task is to move the hand as close as possible to the
 * mouth and maintain the hand close to the mouth. The underlying model
 * represents a simple two joint arm and a neck so that the mouth's position can
 * move. There is a total of four Degrees of Freedom (DOFs): 2 for the arm and 2
 * for the neck and head.
 * 
 * @author Timothy A. Mann
 * 
 */
public class HandMouthTask extends Task<double[], double[]> implements Drawable
{
	private HandMouthModel _model;
	private double[] _prevState;
	private double[] _prevAction;
	private ReinforcementSignal<double[], double[]> _rsignal;

	/**
	 * Constructs an instance of the hand-mouth task.
	 * 
	 * @param model
	 *            the underlying model
	 * @param rsignal
	 *            the reinforcement signal to use
	 */
	public HandMouthTask(HandMouthModel model,
			ReinforcementSignal<double[], double[]> rsignal)
	{
		_model = model;
		_rsignal = rsignal;
		_prevState = new double[HandMouthModel.getStateSize()];
		_prevAction = new double[HandMouthModel.getActionSize()];

		reset();
	}

	@Override
	public double evaluate()
	{
		//double[] newState = getState();
		//return _rsignal.evaluate(_prevState, _prevAction, newState);

		double d2 = _model.getHandMouthDistanceSquared();
		if(d2 > 2.0){
			return 0.0;
		}else{
			return Math.exp(-d2);
		}
	}

	@Override
	public void execute(double[] action)
	{
		double[] state = getState();
		_model.execute(action);

		// Record the previous state and action
		System.arraycopy(state, 0, _prevState, 0, _prevState.length);
		System.arraycopy(action, 0, _prevAction, 0, _prevAction.length);
	}

	@Override
	public double[] getState()
	{
		return _model.getState();
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public void reset()
	{
		// Choose a random but valid state
		_model.random();

		// Set the previous state vector to the current state
		double[] state = _model.getState();
		for (int i = 0; i < _prevState.length; i++) {
			_prevState[i] = state[i];
		}
		// Zero out the previous action vector
		for (int i = 0; i < _prevAction.length; i++) {
			_prevAction[i] = 0;
		}
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_model.draw(g, width, height);
	}
}
