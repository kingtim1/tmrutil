package tmrutil.kinematics;

/**
 * A kinematic chain is a finite sequence of links where the first link (or
 * base) is connected to the environment at a specified point by a hinge or
 * universal joint. The following link is attached to the end of the first link
 * by another hinge or universal joint. The ith joint is attached to the (i-1)th
 * joint by a hinge or universal joint.
 * <p/>
 * 
 * A kinematic chain can exist in either 2-dimensional space (all hinge joints),
 * or 3-dimensional space.
 * 
 * @param <P>
 *            the type used to represent (2D or 3D) points in space
 * 
 * @author Timothy A. Mann
 * 
 */
public interface KinematicChain<P>
{
	/**
	 * Returns the location of the base of this chain.
	 * 
	 * @return the location of the base of this chain
	 */
	public P base();

	/**
	 * Returns the current location of the end of this chain.
	 * 
	 * @return the current location of the end of this chain
	 */
	public P endPoint();

	/**
	 * Returns the current location of the end of the link indexed by
	 * <code>i</code> where <code>i = 0, ..., N-1</code> and <code>N</code> is
	 * the number of links in this chain.
	 * 
	 * @param i
	 *            the index of a link in this chain
	 * @return the current location of the end of the linked indexed by
	 *         <code>i</code>
	 * @throws IndexOutOfBoundsException
	 *             if <code>i &lt; 0</code> or <code>i &ge; N</code> where
	 *             <code>N</code> is the number of links in this chain
	 */
	public P linkEndPoint(int i);

	/**
	 * Returns the length of the <code>i</code>th link, where
	 * <code>i = 0, ..., N-1</code> and <code>N</code> is the number of links in
	 * this chain.
	 * 
	 * @param i
	 *            the index of a link in this chain
	 * @return the length of the <code>i</code>th link
	 * @throws IndexOutOfBoundsException
	 *             if <code>i &lt; 0</code> or <code>i &ge; N</code> where
	 *             <code>N</code> is the number of links in this chain
	 */
	public double linkLength(int i);

	/**
	 * Returns the number of links in this chain.
	 * 
	 * @return the number of links in this chain
	 */
	public int size();
}
