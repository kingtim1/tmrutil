package tmrutil.kinematics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import tmrutil.graphics.ColorKey;
import tmrutil.graphics.ColorMap;
import tmrutil.graphics.Drawable;
import tmrutil.stats.Statistics;
import tmrutil.util.DebugException;
import tmrutil.util.Interval;

/**
 * A component for visualizing error associated with particular 2D points.
 * @author Timothy A. Mann
 *
 */
public class ErrorMapViz implements Drawable
{

	private List<Point2D> _points;
	private List<Double> _meanErrors;
	
	private Interval _displayRange;
	private ColorMap _colorMap;
	private ColorKey _colorKey;
	
	/**
	 * Constructs an error map visualization component.
	 * @param points a list of points
	 * @param meanErrors a list of corresponding errors (for each point)
	 * @param displayRange an interval which determines the range over which color is selected
	 * @param colorMap the color map used to display the error data at each point
	 */
	public ErrorMapViz(List<Point2D> points, List<Double> meanErrors, Interval displayRange, ColorMap colorMap)
	{
		if(points.size() != meanErrors.size()){
			throw new IllegalArgumentException("The number of points must match the number of errors.");
		}
		_points = points;
		_meanErrors = meanErrors;
		_displayRange = new Interval(displayRange);
		_colorMap = colorMap;
		_colorKey = new ColorKey(colorMap, _displayRange, new Rectangle2D.Double(0.88, 0.1, 0.08, 0.8));
	}
	
	public ErrorMapViz(List<Point2D> points, List<Double> meanErrors) throws IllegalArgumentException
	{
		this(points, meanErrors, new Interval(Statistics.min(meanErrors), Statistics.max(meanErrors)), ColorMap.RED_BLUE);
	}
	
	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		Rectangle2D bounds = findBounds();
		double minSide = Math.min(bounds.getWidth(), bounds.getHeight());
		double scale = 0.9 * Math.min(width/bounds.getWidth(), height/bounds.getHeight());
		double radius = 0.02 * Math.min(bounds.getWidth(), bounds.getHeight());
		AffineTransform xform = new AffineTransform();
		xform.scale(scale, scale);
		xform.translate(-bounds.getX() + 0.05 * minSide, -bounds.getY() + 0.05 * minSide);
		g2.transform(xform);
		
		g2.setStroke(new BasicStroke((float)(1/scale)));
		
		for(int i=0;i<_points.size();i++){
			Color color = colorMap(_meanErrors.get(i), _displayRange.getMin(), _displayRange.getMax());
			g2.setColor(color);
			
			Point2D point = _points.get(i);
			Ellipse2D ellipse = new Ellipse2D.Double(point.getX() - radius, point.getY() - radius, 2 * radius, 2 * radius);
			g2.fill(ellipse);
		}
		g2.dispose();
		
		_colorKey.draw(g, width, height);
	}
	
	private Rectangle2D findBounds()
	{
		double minX = 0, maxX = 0;
		double minY = 0, maxY = 0;
		for(int i=0;i<_points.size();i++){
			Point2D point = _points.get(i);
			if(i==0){
				minX = point.getX();
				maxX = point.getX();
				minY = point.getY();
				maxY = point.getY();
			}else{
				if(minX > point.getX()){
					minX = point.getX();
				}
				if(maxX < point.getX()){
					maxX = point.getX();
				}
				if(minY > point.getY()){
					minY = point.getY();
				}
				if(maxY < point.getY()){
					maxY = point.getY();
				}
			}
		}
		
		return new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
	}
	
	/**
	 * Retrieves the color assigned to a value given the minimum and maximum value.
	 * @param value an error value
	 * @param min the minimum value (beyond which the color doesn't change)
	 * @param max the maximum value (beyond which the color doesn't change)
	 * @return a color
	 */
	protected Color colorMap(double value, double min, double max)
	{
		return _colorMap.map(value, new Interval(min, max));
	}
}
