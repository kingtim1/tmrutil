package tmrutil.kinematics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import tmrutil.graphics.Drawable;
import tmrutil.math.DimensionMismatchException;
import tmrutil.util.ArrayOps;
import tmrutil.util.Interval;

/**
 * A model representing a simplified humanoid with hand and mouth. The arm is
 * represented by two links, while the mouth is also placed at the end of a two
 * link armature.
 * <p/>
 * 
 * All distances are reported in meters and all angles are measured in Radians.
 * 
 * @author Timothy A. Mann
 * 
 */
public class HandMouthModel implements Drawable
{
	/** Armature representing the neck and head. */
	private Arm _neckAndHead;
	/** Represents the humanoid's arm. */
	private Arm _arm;

	/**
	 * Constructs an arm-mouth model of a humanoid.
	 * 
	 * @param ulLength
	 *            the length of the arm's upper link in meters
	 * @param shoulderBounds
	 *            the shoulder joint's angle limits in Radians
	 * @param llLength
	 *            the length of the arm's lower link in meters
	 * @param elbowBounds
	 *            the elbow joint's angle limits in Radians
	 * @param neckLength
	 *            the length of the neck in meters
	 * @param neckBounds
	 *            the neck joint's angle limits in Radians
	 * @param headLength
	 *            the distance from the top of the neck to the mouth in meters
	 * @param headBounds
	 *            the head joint's angle limits in Radians
	 */
	public HandMouthModel(double ulLength, Interval shoulderBounds,
			double llLength, Interval elbowBounds, double neckLength,
			Interval neckBounds, double headLength, Interval headBounds)
	{
		if (ulLength <= 0) {
			throw new IllegalArgumentException(
					"The upper arm length must be positive.");
		}
		if (llLength <= 0) {
			throw new IllegalArgumentException(
					"The lower arm length must be positive.");
		}
		if (neckLength <= 0) {
			throw new IllegalArgumentException(
					"The neck length must be positive.");
		}
		if (headLength <= 0) {
			throw new IllegalArgumentException(
					"The head length must be positive.");
		}

		// Construct the arm model
		_arm = new Arm();
		_arm.addLink(ulLength, shoulderBounds.getMin(), shoulderBounds.getMax());
		_arm.addLink(llLength, elbowBounds.getMin(), elbowBounds.getMax());

		// Construct the neck and head model
		_neckAndHead = new Arm();
		_neckAndHead.addLink(neckLength, neckBounds.getMin(),
				neckBounds.getMax());
		_neckAndHead.addLink(headLength, headBounds.getMin(),
				headBounds.getMax());
	}

	/**
	 * Returns the location of the mouth (with respect to the origin) as a
	 * Cartesian coordinate.
	 * 
	 * @return location of the mouth
	 */
	public Point2D getMouthPosition()
	{
		return _neckAndHead.endPoint();
	}

	/**
	 * Returns the location of the hand (with respect to the origin) as a
	 * Cartesian coordinate.
	 * 
	 * @return location of the hand
	 */
	public Point2D getHandPosition()
	{
		return _arm.endPoint();
	}

	/**
	 * Returns the Euclidean distance between the hand and mouth.
	 * 
	 * @return Euclidean distance between hand and mouth
	 */
	public double getHandMouthDistance()
	{
		return _arm.endPoint().distance(_neckAndHead.endPoint());
	}

	/**
	 * Returns the squared Euclidean distance between the hand and mouth.
	 * 
	 * @return squared Euclidean distance between hand and mouth
	 */
	public double getHandMouthDistanceSquared()
	{
		return _arm.endPoint().distanceSq(_neckAndHead.endPoint());
	}

	/**
	 * Returns a deep copy of the neck and head armature.
	 * 
	 * @return an arm (representing neck and head)
	 */
	public Arm getNeckAndHead()
	{
		return new Arm(_neckAndHead);
	}

	/**
	 * Returns a deep copy of the arm.
	 * 
	 * @return an arm
	 */
	public Arm getArm()
	{
		return new Arm(_arm);
	}

	/**
	 * Returns the state of the model as a vector of joint angles (in Radians).
	 * The vector contains 4 components.
	 * <OL>
	 * <LI>state[0] : joint angle of the neck</LI>
	 * <LI>state[1] : joint angle of the head</LI>
	 * <LI>state[2] : joint angle of the shoulder</LI>
	 * <LI>state[3] : joint angle of the elbow</LI>
	 * </OL>
	 * 
	 * @return a vector containing the joint angles of the neck, head, shoulder,
	 *         and elbow.
	 */
	public double[] getState()
	{
		double[] state = ArrayOps.concat(_neckAndHead.getJointAngles(),
				_arm.getJointAngles());
		return state;
	}

	/**
	 * Executes the specified action. The components of the 4-dimensional action
	 * vector represent (changes of joint angles in Radians):
	 * <OL>
	 * <LI>action[0] : change in neck angle</LI>
	 * <LI>action[1] : change in head angle</LI>
	 * <LI>action[2] : change in shoulder angle</LI>
	 * <LI>action[3] : change in elbow angle</LI>
	 * </OL>
	 * 
	 * @param action
	 *            a 4-dimensional vector of scalar values
	 */
	public void execute(double[] action)
	{
		if (action.length != 4) {
			throw new DimensionMismatchException(
					"An action vector must contain 4 dimensions.");
		}

		double[] armState = _arm.getJointAngles();
		double[] headState = _neckAndHead.getJointAngles();

		headState[0] += action[0];
		headState[1] += action[1];

		_neckAndHead.setJointAngles(headState);

		armState[0] += action[2];
		armState[1] += action[3];

		_arm.setJointAngles(armState);
	}

	/**
	 * Places the model in a random (but valid) pose.
	 */
	public void random()
	{
		_neckAndHead.random();
		_arm.random();
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();

		// Set the colors for each component to be drawn
		Color mouthColor = Color.RED;
		Color handColor = Color.BLUE;
		Color linkColor = Color.BLACK;

		// Translate the coordinates using an Affine transform
		double maxArmLength = _arm.getMaxArmLength();
		double maxNeckAndHeadLength = _neckAndHead.getMaxArmLength();
		double fitLength = Math.max(2 * maxArmLength, 2 * maxNeckAndHeadLength);
		double scale = Math.min(width / fitLength, height / fitLength);
		AffineTransform xform = new AffineTransform();
		xform.translate(width / 2, height / 2);
		xform.scale(scale, -scale);
		g2.transform(xform);
		g2.setStroke(new BasicStroke((float) (1 / scale)));

		double radius = 0.015 * fitLength;

		//
		// Draw the links for neck, and head
		//
		Point2D origin = new Point2D.Double(0, 0);
		g2.setColor(linkColor);

		Point2D neckEP = _neckAndHead.getLinkEndPoint(0);
		Point2D headEP = _neckAndHead.getLinkEndPoint(1);
		Line2D neckLink = new Line2D.Double(origin, neckEP);
		Line2D headLink = new Line2D.Double(neckEP, headEP);
		g2.draw(neckLink);
		g2.draw(headLink);

		//
		// Draw the mouth
		//
		g2.setColor(mouthColor);
		Ellipse2D mouth = new Ellipse2D.Double(headEP.getX() - radius,
				headEP.getY() - radius, 2 * radius, 2 * radius);
		g2.fill(mouth);
		g2.setColor(linkColor);
		g2.draw(mouth);

		//
		// Draw the links for the arm
		//
		Point2D upperLinkEP = _arm.getLinkEndPoint(0);
		Point2D lowerLinkEP = _arm.getLinkEndPoint(1);
		Line2D upperLink = new Line2D.Double(origin, upperLinkEP);
		Line2D lowerLink = new Line2D.Double(upperLinkEP, lowerLinkEP);
		g2.draw(upperLink);
		g2.draw(lowerLink);

		//
		// Draw the hand
		//
		g2.setColor(handColor);
		Ellipse2D hand = new Ellipse2D.Double(lowerLinkEP.getX() - radius,
				lowerLinkEP.getY() - radius, 2 * radius, 2 * radius);
		g2.fill(hand);
		g2.setColor(linkColor);
		g2.draw(hand);

		// Destroy the graphics object
		g2.dispose();
	}

	/**
	 * Returns the number of components in a state vector.
	 * 
	 * @return number of elements in a state vector
	 */
	public static final int getStateSize()
	{
		return 4;
	}

	/**
	 * Returns the number of components in an action vector.
	 * 
	 * @return number of elements in an action vector
	 */
	public static final int getActionSize()
	{
		return 4;
	}

	/**
	 * Set the joint angle boundaries for the neck joint (in Radians).
	 * 
	 * @param bounds
	 *            the desired boundaries
	 */
	public void setNeckBounds(Interval bounds)
	{
		_neckAndHead.setJointBounds(0, bounds);
	}

	/**
	 * Sets the joint angle boundaries for the head joint (in Radians).
	 * 
	 * @param bounds
	 *            the desired boundaries
	 */
	public void setHeadBounds(Interval bounds)
	{
		_neckAndHead.setJointBounds(1, bounds);
	}

	/**
	 * Sets the joint angle boundaries for the shoulder (in Radians)
	 * 
	 * @param bounds
	 *            the desired boundaries
	 */
	public void setShoulderBounds(Interval bounds)
	{
		_arm.setJointBounds(0, bounds);
	}

	/**
	 * Sets the joint angle boundaries for the elbow (in Radians)
	 * 
	 * @param bounds
	 *            the desired boundaries
	 */
	public void setElbowBounds(Interval bounds)
	{
		_arm.setJointBounds(0, bounds);
	}

	/**
	 * Sets the angle of the shoulder joint (in Radians). If the value exceeds
	 * the bounds, then the angle is clipped to the nearest valid angle.
	 * 
	 * @param angle
	 *            an angle in Radians
	 */
	public void setShoulder(double angle)
	{
		double[] ja = _arm.getJointAngles();
		ja[0] = angle;
		_arm.setJointAngles(ja);
	}

	/**
	 * Sets the angle of the elbow joint (in Radians). If the value exceeds the
	 * bounds, then the angle is clipped to the nearest valid angle.
	 * 
	 * @param angle an angle in Radians
	 */
	public void setElbow(double angle)
	{
		double[] ja = _arm.getJointAngles();
		ja[1] = angle;
		_arm.setJointAngles(ja);
	}

	/**
	 * Scales all of the links equally.
	 * 
	 * @param scale
	 *            a scaling constant
	 * @return a new model instance where all links have been scaled by
	 *         <code>scale</code>
	 */
	public HandMouthModel grow(double scale)
	{
		Arm arm = getArm();
		Arm nh = getNeckAndHead();

		double ulLength = scale * arm.getLinkLength(0);
		Interval shoulderBounds = new Interval(arm.getLinkLowerBound(0),
				arm.getLinkUpperBound(0));
		double llLength = scale * arm.getLinkLength(1);
		Interval elbowBounds = new Interval(arm.getLinkLowerBound(1),
				arm.getLinkUpperBound(1));
		double neckLength = scale * nh.getLinkLength(0);
		Interval neckBounds = new Interval(nh.getLinkLowerBound(0),
				nh.getLinkUpperBound(0));
		double headLength = scale * nh.getLinkLength(1);
		Interval headBounds = new Interval(nh.getLinkLowerBound(1),
				nh.getLinkUpperBound(1));
		return new HandMouthModel(ulLength, shoulderBounds, llLength,
				elbowBounds, neckLength, neckBounds, headLength, headBounds);
	}

	/**
	 * Constructs an arm-mouth model with confinement to simulate prenatal
	 * conditions.
	 * 
	 * @return a prenatal arm-mouth model
	 */
	public static final HandMouthModel buildPrenatalModel()
	{
		Arm prenatalArm = Arm.buildPrenatalArm();
		double ulLength = prenatalArm.getLinkLength(0);
		Interval shoulderBounds = new Interval(
				prenatalArm.getLinkLowerBound(0),
				prenatalArm.getLinkUpperBound(0));
		double llLength = prenatalArm.getLinkLength(1);
		Interval elbowBounds = new Interval(prenatalArm.getLinkLowerBound(1),
				prenatalArm.getLinkUpperBound(1));
		double neckLength = ulLength / 2;
		Interval neckBounds = new Interval(1 * Math.PI / 6, 3 * Math.PI / 6);
		double headLength = ulLength / 3;
		Interval headBounds = new Interval(-2 * Math.PI / 6, -Math.PI / 6);

		return new HandMouthModel(ulLength, shoulderBounds, llLength,
				elbowBounds, neckLength, neckBounds, headLength, headBounds);
	}

	/**
	 * Constructs an arm-mouth model that represents postnatal conditions.
	 * 
	 * @return a postnatal arm-mouth model
	 */
	public static final HandMouthModel buildPostnatalModel()
	{
		Arm postnatalArm = Arm.buildPostnatalArm();
		double ulLength = postnatalArm.getLinkLength(0);
		Interval shoulderBounds = new Interval(
				postnatalArm.getLinkLowerBound(0),
				postnatalArm.getLinkUpperBound(0));
		double llLength = postnatalArm.getLinkLength(1);
		Interval elbowBounds = new Interval(postnatalArm.getLinkLowerBound(1),
				postnatalArm.getLinkUpperBound(1));
		double neckLength = ulLength / 2;
		Interval neckBounds = new Interval(1 * Math.PI / 6, 3 * Math.PI / 6);
		double headLength = ulLength / 3;
		Interval headBounds = new Interval(-2 * Math.PI / 6, -Math.PI / 6);

		return new HandMouthModel(ulLength, shoulderBounds, llLength,
				elbowBounds, neckLength, neckBounds, headLength, headBounds);
	}
}
