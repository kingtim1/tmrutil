package tmrutil.kinematics;

import tmrutil.util.Interval;

/**
 * A link used in a 2-dimensional kinematic chain.
 * @author Timothy A. Mann
 *
 */
public class Link2D extends Link
{
	/** The boundaries of this links hinge joint in radians. */
	private Interval _bounds;
	
	/**
	 * Constructs a 2-dimensional link with joint angle boundaries.
	 * @param length the length of the link in meters
	 * @param bounds the upper and lower boundaries of the joint angles for this link in radians
	 */
	public Link2D(double length, Interval bounds)
	{
		super(length);
		setBounds(bounds);
	}
	
	/**
	 * Constructs a 2-dimensional link with attributes copied from an existing link.
	 * @param link a link
	 */
	public Link2D(Link2D link)
	{
		this(link.getLength(), link._bounds);
	}
	
	@Override
	public boolean equals(Object object)
	{
		if(object instanceof Link2D){
			Link2D link = (Link2D)object;
			if(getLength() == link.getLength() && _bounds.equals(link._bounds));
		}
		return false;
	}
	
	/**
	 * Sets the upper and lower boundaries of this links joint in radians.
	 * @param bounds an interval
	 */
	public void setBounds(Interval bounds)
	{
		if(bounds == null){
			throw new NullPointerException("A links bounds cannot be null.");
		}
		_bounds = new Interval(bounds);
	}

	/**
	 * Gets the upper bound in radians of the joint at the base of this link.
	 * @return a scalar value in radians
	 */
	public double getLowerBound()
	{
		return _bounds.getMin();
	}
	
	/**
	 * Gets the lower bound in radians of the joint at the base of this link.
	 * @return a scalar value in radians
	 */
	public double getUpperBound()
	{
		return _bounds.getMax();
	}
}
