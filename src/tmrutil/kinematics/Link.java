package tmrutil.kinematics;

/**
 * Represents a single link in a kinematic chain.
 * @author Timothy A. Mann
 *
 */
public class Link
{
	/** This link's length in meters. */
	private double _length;
	
	/**
	 * Constructs a link with a specified length in meters.
	 * @param length a positive scalar value representing the length of the new link in meters
	 * @throws IllegalArgumentException if <code>length</code> is not a positive value
	 */
	public Link(double length)
	{
		setLength(length);
	}
	
	@Override
	public boolean equals(Object object)
	{
		if(object instanceof Link){
			Link link = (Link)object;
			if(getLength() == link.getLength()){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Sets the length of this link.
	 * @param length a positive scalar value representing the length of this link in meters
	 * @throws IllegalArgumentException if <code>length</code> is not a positive value
	 */
	public void setLength(double length)
	{
		if(length < 0){
			throw new IllegalArgumentException("Detected nonpositive length : " + length);
		}
		_length = length;
	}
	
	/**
	 * Returns the length of this link in meters.
	 * @return the length of this link in meters
	 */
	public double getLength()
	{
		return _length;
	}
}
