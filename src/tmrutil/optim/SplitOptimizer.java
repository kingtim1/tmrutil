package tmrutil.optim;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.math.Function;
import tmrutil.util.Interval;

/**
 * A simple optimization strategy for convex minimization over a rectangular
 * space. It works by choosing the center point in the space, evaluating the
 * gradient, dividing the space into regions and selecting only the region in
 * the direction of the gradient.
 * 
 * @author Timothy A. Mann
 * 
 */
public class SplitOptimizer {
	private List<Interval> _hyperspace;
	private Function<RealVector,RealVector> _gradient;
	
	public SplitOptimizer(List<Interval> hyperspace, Function<RealVector,RealVector> gradient)
	{
		_hyperspace = hyperspace;
		_gradient = gradient;
	}
	
	private RealVector hyperspaceCenter(List<Interval> hyperspace)
	{
		RealVector center = new ArrayRealVector(hyperspace.size());
		
		for(int i=0;i<hyperspace.size();i++){
			Interval dint = hyperspace.get(i);
			double centeri = dint.getDiff() / 2 + dint.getMin();
			center.setEntry(i, centeri);
		}
		
		return center;
	}
	
	private List<Interval> splitHyperspace(RealVector hcenter, RealVector gradient, List<Interval> hyperspace){
		List<Interval> hspace = new ArrayList<Interval>(hyperspace.size());
		for(int i=0;i<hyperspace.size();i++){
			double mini = 0;
			double maxi = 0;
			if(gradient.getEntry(i) > 0){
				mini = hyperspace.get(i).getMin();
				maxi = hcenter.getEntry(i);
			}else{
				mini = hcenter.getEntry(i);
				maxi = hyperspace.get(i).getMax();
			}
			hspace.add(new Interval(mini, maxi));
		}
		return hspace;
	}
	
	public RealVector optimize(int numIterations)
	{
		List<Interval> hspace = _hyperspace;
		RealVector hcenter = hyperspaceCenter(hspace);
		for(int i=0;i<numIterations;i++){
			RealVector g = _gradient.evaluate(hcenter);
			hspace = splitHyperspace(hcenter, g, hspace);
		}
		return hyperspaceCenter(hspace);
	}
}
