package tmrutil.optim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.learning.BatchFunctionApproximator;
import tmrutil.util.Pair;

/**
 * An implementation of Stochastic Gradient Descent.
 * @author Timothy A. Mann
 *
 */
public class SGD implements BatchFunctionApproximator<RealVector,Double> {

	private static final long serialVersionUID = 7631215281023311256L;
	
	public static enum ConvergenceCondition {NUM_ITERATIONS};
	
	private int _inputSize;
	private RealVector _weights;
	private ObjectiveFunction _objF;
	private double _stepSize;
	
	private int _maxIterations;
	
	/**
	 * Constructs an SGD instance.
	 * @param inputSize the number of dimensions of the input vectors
	 * @param objF the objective function to use
	 * @param stepSize the constant step size to use
	 * @param maxIterations the maximum number of iterations for training
	 */
	public SGD(int inputSize, ObjectiveFunction objF, double stepSize, int maxIterations) {
		_inputSize = inputSize;
		_weights = new ArrayRealVector(new double[inputSize]);
		_objF = objF;
		
		_stepSize = stepSize;
		
		setMaxIterations(maxIterations);
	}
	
	public void setMaxIterations(int maxIterations)
	{
		if(maxIterations < 1){
			throw new IllegalArgumentException("Max. iterations must be positive.");
		}
		_maxIterations = maxIterations;
	}
	
	public int maxIterations()
	{
		return _maxIterations;
	}

	@Override
	public void reset() {
		_weights = new ArrayRealVector(new double[_inputSize]);
	}

	@Override
	public Double evaluate(RealVector x) throws IllegalArgumentException {
		return _weights.dotProduct(x);
	}

	@Override
	public BatchFunctionApproximator<RealVector, Double> newInstance() {
		return new SGD(_inputSize, _objF, _stepSize, maxIterations());
	}

	@Override
	public void train(Collection<Pair<RealVector, Double>> trainingSet) {
		List<Pair<RealVector,Double>> tset = new ArrayList<Pair<RealVector,Double>>(trainingSet);
		for(int i=0;i<maxIterations();i++){
			Collections.shuffle(tset);
			
			for(Pair<RealVector,Double> sample : tset){
				RealVector x = sample.getA();
				Double y = sample.getB();
				singleSampleUpdate(_weights, x, y);
			}
		}
	}
	
	public void train(List<Pair<RealVector,Double>> trainingSet, RealVector sampleImportance)
	{
		
		for(int i=0;i<maxIterations();i++){
			
			
			for(int s=0;s<trainingSet.size();s++){
				Pair<RealVector,Double> sample = trainingSet.get(s);
				RealVector x = sample.getA();
				Double y = sample.getB();
				Double importance = sampleImportance.getEntry(s);
				singleSampledWeightedUpdate(_weights, x, y, importance);
			}
		}
	}
	
	private void singleSampleUpdate(RealVector weights, RealVector x, Double y)
	{
		double ss = _stepSize / weights.getDimension();
		RealVector g = _objF.gradient(weights, x, y);
		for(int i=0;i<g.getDimension();i++){
			double w = weights.getEntry(i) - ss * g.getEntry(i);
			weights.setEntry(i, w);
		}
	}
	
	private void singleSampledWeightedUpdate(RealVector weights, RealVector x, Double y, Double weight)
	{
		double ss = _stepSize / weights.getDimension();
		RealVector g = _objF.gradient(weights, x, y);
		for(int i=0;i<g.getDimension();i++){
			double w = weight * (weights.getEntry(i) - ss * g.getEntry(i));
			weights.setEntry(i, w);
		}
	}

}
