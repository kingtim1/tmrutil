package tmrutil.optim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.learning.BatchFunctionApproximator;
import tmrutil.learning.FeatureVector;
import tmrutil.util.Pair;

/**
 * An implementation of Stochastic Gradient Descent that uses sparse vectors.
 * @author Timothy A. Mann
 *
 */
public class SparseSGD implements BatchFunctionApproximator<FeatureVector,Double> {

	private static final long serialVersionUID = 7631215281023311256L;
	
	private FeatureVector _weights;
	private ObjectiveFunction _objF;
	private double _stepSize;
	
	private int _maxIterations;
	
	/**
	 * Constructs an SGD instance.
	 * @param inputSize the number of dimensions of the input vectors
	 * @param objF the objective function to use
	 * @param stepSize the constant step size to use
	 * @param maxIterations the maximum number of iterations for training
	 */
	public SparseSGD(ObjectiveFunction objF, double stepSize, int maxIterations) {
		_weights = new FeatureVector();
		_objF = objF;	
		_stepSize = stepSize;
		
		setMaxIterations(maxIterations);
	}
	
	public void setMaxIterations(int maxIterations)
	{
		if(maxIterations < 1){
			throw new IllegalArgumentException("Max. iterations must be positive.");
		}
		_maxIterations = maxIterations;
	}
	
	public int maxIterations()
	{
		return _maxIterations;
	}

	@Override
	public void reset() {
		_weights = new FeatureVector();
	}

	@Override
	public Double evaluate(FeatureVector x) throws IllegalArgumentException {
		return _weights.dotProduct(x);
	}

	@Override
	public BatchFunctionApproximator<FeatureVector, Double> newInstance() {
		return new SparseSGD(_objF, _stepSize, maxIterations());
	}

	@Override
	public void train(Collection<Pair<FeatureVector, Double>> trainingSet) {
		List<Pair<FeatureVector,Double>> tset = new ArrayList<Pair<FeatureVector,Double>>(trainingSet);
		for(int i=0;i<maxIterations();i++){
			Collections.shuffle(tset);
			
			for(Pair<FeatureVector,Double> sample : tset){
				FeatureVector x = sample.getA();
				Double y = sample.getB();
				singleSampleUpdate(_weights, x, y);
			}
		}
	}
	
	public void train(List<Pair<FeatureVector,Double>> trainingSet, FeatureVector sampleImportance)
	{
		
		for(int i=0;i<maxIterations();i++){
			
			Set<Integer> nonzeroSamples = sampleImportance.nonzeroIndices();
			for(Integer s : nonzeroSamples){
				Pair<FeatureVector,Double> sample = trainingSet.get(s);
				FeatureVector x = sample.getA();
				Double y = sample.getB();
				Double importance = sampleImportance.element(s);
				singleSampledWeightedUpdate(_weights, x, y, importance);
			}
		}
	}
	
	private void singleSampleUpdate(FeatureVector weights, FeatureVector x, Double y)
	{
		double ss = _stepSize;
		FeatureVector g = _objF.gradient(weights, x, y);
		Set<Integer> nonzero = g.nonzeroIndices();
		for(Integer i : nonzero){
			double w = weights.element(i) - ss * g.element(i);
			weights.set(i, w);
		}
	}
	
	private void singleSampledWeightedUpdate(FeatureVector weights, FeatureVector x, Double y, Double weight)
	{
		double ss = _stepSize;
		FeatureVector g = _objF.gradient(weights, x, y);
		Set<Integer> nonzero = g.nonzeroIndices();
		for(Integer i : nonzero){
			double w = weight * (weights.element(i) - ss * g.element(i));
			weights.set(i, w);
		}
	}

}
