package tmrutil.optim;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.learning.FeatureVector;
import tmrutil.math.Function;
import tmrutil.math.KernelFunction;
import tmrutil.math.functions.LinearFunction;
import tmrutil.util.DebugException;
import tmrutil.util.Pair;

/**
 * Represents an objective function for regularized linear optimization.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ObjectiveFunction {

	public static enum Loss {
		HINGE, LOGISTIC_REGRESSION, ABSOLUTE_ERROR, SQUARED_ERROR
	};

	public static enum Penalty {
		L1, L2
	};

	private Loss _ltype;
	private Penalty _ptype;
	private double _lambda;

	public ObjectiveFunction(Loss lossType, Penalty penType, double lambda) {
		_ltype = lossType;
		_ptype = penType;
		_lambda = lambda;
	}
	
	public FeatureVector gradient(FeatureVector w, FeatureVector x, Double y)
	{
		FeatureVector gPen = penaltyGradient(w);
		switch(_ltype){
		case HINGE:
			return hingeGradient(w, x, y).add(gPen);
		case LOGISTIC_REGRESSION:
			return logisticGradient(w, x, y).add(gPen);
		case ABSOLUTE_ERROR:
			return absoluteGradient(w, x, y).add(gPen);
		case SQUARED_ERROR:
		default:
			return squaredGradient(w, x, y).add(gPen);
		}
	}
	
	public RealVector gradient(RealVector w, RealVector x, Double y)
	{
		RealVector gPen = penaltyGradient(w);
		switch(_ltype){
		case HINGE:
			return hingeGradient(w, x, y).add(gPen);
		case LOGISTIC_REGRESSION:
			return logisticGradient(w, x, y).add(gPen);
		case ABSOLUTE_ERROR:
			return absoluteGradient(w, x, y).add(gPen);
		case SQUARED_ERROR:
		default:
			return squaredGradient(w, x, y).add(gPen);
		}
	}
	
	public RealVector hingeGradient(RealVector w, RealVector x, Double y)
	{
		// TODO Implement gradient for hinge loss
		throw new DebugException("Hinge loss gradient is currently not supported.");
	}
	
	public FeatureVector hingeGradient(FeatureVector w, FeatureVector x, Double y)
	{
		// TODO Implement gradient for hinge loss
		throw new DebugException("Hinge loss gradient is currently not supported.");
	}
	
	public RealVector logisticGradient(RealVector w, RealVector x, Double y)
	{
		// TODO Implement gradient for logistic regression
		throw new DebugException("Logistic regression gradient is currently not supported.");
	}
	
	public FeatureVector logisticGradient(FeatureVector w, FeatureVector x, Double y)
	{
		// TODO Implement gradient for logistic regression
		throw new DebugException("Logistic regression gradient is currently not supported.");
	}
	
	public RealVector absoluteGradient(RealVector w, RealVector x, Double y)
	{
		double output = w.dotProduct(x);
		double error = y - output;
		
		double[] g = new double[w.getDimension()];
		for(int i=0;i<g.length;i++){
			g[i] = Math.signum(error) * -x.getEntry(i);
		}
		return new ArrayRealVector(g);
	}
	
	public FeatureVector absoluteGradient(FeatureVector w, FeatureVector x, Double y)
	{
		double output = w.dotProduct(x);
		double error = y - output;
		
		FeatureVector g = new FeatureVector();
		Set<Integer> nonzero = x.nonzeroIndices();
		for(Integer i : nonzero){
			g.set(i, Math.signum(error) * -x.element(i));
		}
		return g;
	}
	
	public RealVector squaredGradient(RealVector w, RealVector x, Double y)
	{
		double output = w.dotProduct(x);
		double error = y - output;
		
		double[] g = new double[w.getDimension()];
		for(int i=0;i<g.length;i++){
			g[i] = 2 * error * -x.getEntry(i);
		}
		return new ArrayRealVector(g);
	}
	
	public FeatureVector squaredGradient(FeatureVector w, FeatureVector x, Double y)
	{
		double output = w.dotProduct(x);
		double error = y - output;
		FeatureVector g = new FeatureVector();
		Set<Integer> nonzero = x.nonzeroIndices();
		for(Integer i : nonzero){
			g.set(i, 2 * error * -x.element(i));
		}
		return g;
	}
	
	/**
	 * Evaluates the loss function for an input <code>x</code> and label
	 * <code>y</code>.
	 * 
	 * @param f
	 *            a function
	 * @param x
	 *            an input point
	 * @param y
	 *            the label for <code>x</code>
	 * @return the loss associated with <code>f</code>'s output on
	 *         <code>x</code>
	 */
	public Double loss(Function<RealVector, Double> f, RealVector x,
			Double y) {
		switch (_ltype) {
		case HINGE:
			return hingeLoss(f, x, y);
		case LOGISTIC_REGRESSION:
			return logisticLoss(f, x, y);
		case ABSOLUTE_ERROR:
			return absoluteLoss(f, x, y);
		case SQUARED_ERROR:
		default:
			return squaredLoss(f, x, y);
		}
	}
	
	public Double convexConjugate(Double u, Double y)
	{
		switch(_ltype){
		case HINGE:
			return convexConjugateHingeLoss(u, y);
		case LOGISTIC_REGRESSION:
			return convexConjugateLogisticLoss(u, y);
		case ABSOLUTE_ERROR:
			return convexConjugateAbsoluteLoss(u, y);
		case SQUARED_ERROR:
		default:
			return convexConjugateSquaredLoss(u, y);
		}
	}
	
	public Double convexConjugateHingeLoss(Double u, Double y)
	{
		return 1.0;
	}
	
	public Double convexConjugateLogisticLoss(Double u, Double y)
	{
		double z = (-1/y) * Math.log((u-1)/(y-u));
		return Math.log(1 + Math.exp(-y*z));
	}
	
	public Double convexConjugateAbsoluteLoss(Double u, Double y)
	{
		double z = y;
		return z * u;
	}
	
	public Double convexConjugateSquaredLoss(Double u, Double y)
	{
		double z = (u + 2 * y) / 2;
		return z * u - Math.pow(z - y, 2);
	}

	public Double logisticLoss(Function<RealVector, Double> f, RealVector x,
			Double y) {
		Double yhat = f.evaluate(x);
		return Math.log(1 + Math.exp(-y * yhat));
	}

	public Double squaredLoss(Function<RealVector, Double> f, RealVector x,
			Double y) {
		Double yhat = f.evaluate(x);
		return Math.pow(y - yhat, 2);
	}

	public Double absoluteLoss(Function<RealVector, Double> f, RealVector x,
			Double y) {
		Double yhat = f.evaluate(x);
		return Math.abs(y - yhat);
	}

	public Double hingeLoss(Function<RealVector, Double> f, RealVector x,
			Double y) {
		Double yhat = f.evaluate(x);
		return Math.max(0, 1 - y * yhat);
	}
	
	public double lambda()
	{
		return _lambda;
	}

	public double penalty(RealVector w) {
		switch (_ptype) {
		case L1:
			return (_lambda / 2) * w.getL1Norm();
		case L2:
		default:
			return (_lambda / 2) * w.getNorm();
		}
	}
	
	public FeatureVector penaltyGradient(FeatureVector w)
	{
		Set<Integer> nonzero = w.nonzeroIndices();
		FeatureVector g = new FeatureVector();
		switch(_ptype){
		case L1:
			for(Integer i : nonzero){
				g.set(i, _lambda * Math.signum(w.element(i)));
			}
			return g;
		case L2:
		default:
			for(Integer i : nonzero){
				g.set(i, _lambda * w.element(i));
			}
			return g;
		}
	}
	
	public RealVector penaltyGradient(RealVector w){
		double[] g = new double[w.getDimension()];
		switch(_ptype){
		case L1:
			for(int i=0;i<g.length;i++){
				g[i] = _lambda * Math.signum(w.getEntry(i));
			}
			return new ArrayRealVector(g);
		case L2:
		default:
			for(int i=0;i<g.length;i++){
				g[i] = _lambda * w.getEntry(i);
			}
			return new ArrayRealVector(g);
		}
	}
	
	public double primal(RealVector w, RealVector x, Double y)
	{
		LinearFunction f = new LinearFunction(w);
		return loss(f, x, y) + penalty(w);	
	}
	
	public double primal(RealVector w, Collection<Pair<RealVector,Double>> samples)
	{
		int n = samples.size();
		LinearFunction f = new LinearFunction(w);
		double primalSum = 0;
		for(Pair<RealVector,Double> sample : samples){
			RealVector x = sample.getA();
			Double y = sample.getB();
			primalSum += loss(f, x, y);
		}
		return primalSum / n + penalty(w);
	}
	
	public double dual(RealVector alpha, List<Pair<RealVector,Double>> samples)
	{
		int n = samples.size();
		double dualSum = 0;
		for(int i=0;i<alpha.getDimension();i++){
			Pair<RealVector,Double> sample = samples.get(i);
			dualSum += -convexConjugate(-alpha.getEntry(i), sample.getB());
		}
		
		return (dualSum/n) - penalty(weightsFromAlpha(alpha, samples));
	}
	
	public RealVector weightsFromAlpha(RealVector alpha, List<Pair<RealVector,Double>> samples)
	{
		int n = samples.size();
		Pair<RealVector,Double> firstSample = samples.get(0);
		RealVector wsum = new ArrayRealVector(firstSample.getA().getDimension());
		for(int i=0;i<n;i++){
			RealVector v = samples.get(0).getA().mapMultiply(alpha.getEntry(i));
			wsum = wsum.add(v);
		}
		return wsum.mapMultiply(1 / (n * _lambda));
	}
}
