package tmrutil.csr;

/**
 * Represents a rule in an expert reasoning system.
 * @author Timothy A. Mann
 *
 */
public interface Rule
{
	/**
	 * Evaluates a query based on this rule.
	 * @param q a query
	 * @return an outcome
	 */
	public Outcome evaluate(Query q);
}
