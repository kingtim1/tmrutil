package tmrutil.csr;

/**
 * An expert represents a collection of rules about a domain.
 * @author Timothy A. Mann
 *
 */
public interface Expert extends Rule
{

}
