package tmrutil.csr;

public enum Outcome {
	CONFIRMS, CONFLICTS, NOT_RELEVANT
}
