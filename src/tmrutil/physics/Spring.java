package tmrutil.physics;

import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import tmrutil.graphics.Drawable;
import tmrutil.graphics.DrawableComponent;
import tmrutil.math.Function;

/**
 * Implements a simple spring model.
 * @author Timothy A. Mann
 *
 */
public class Spring implements Drawable
{
	private Function<double[],Double> _accelDEQ = new Function<double[],Double>(){
		@Override
		public Double evaluate(double[] x)
		{
			return - ((_k/_mass)*(_x - _restLength)) - ((_damping/_mass)*_v);
		}
	};
	
	private Function<double[],Double> _velDEQ = new Function<double[],Double>(){
		@Override
		public Double evaluate(double[] x)
		{
			return _v;
		}
	};
	
	/** The position of the end of the spring. */
	private double _x;
	/** Velocity of the spring. */
	private double _v;
	
	/** Time in the simulation. */
	private double _t;
	
	/** A spring constant determining how stiff this spring is. */
	private double _k;
	/** The mass at the end of the spring. */
	private double _mass;
	
	/** The damping constant. */
	private double _damping;
	/** The resting length of the spring. */
	private double _restLength;
	
	/**
	 * Constructs a spring with a specific spring constant, mass, damping constant, and resting length.
	 * @param k spring constant
	 * @param mass mass of the object at the end of the spring
	 * @param damping a damping constant
	 * @param restLength the resting length of the constructed spring
	 */
	public Spring(double k, double mass, double damping, double restLength)
	{
		_k = k;
		_mass = mass;
		_damping = damping;
		_restLength = restLength;
		
		_x = 0;
		_v = 0;
	}
	
	public double getPosition()
	{
		return _x;
	}
	
	public void setPosition(double x)
	{
		_x = x;
	}
	
	public double getSpringConstant()
	{
		return _k;
	}
	
	public void setSpringConstant(double k)
	{
		if(k < 0){
			throw new IllegalArgumentException("The spring constant must be nonnegative.");
		}
		_k = k;
	}
	
	public double getDampingConstant()
	{
		return _damping;
	}
	
	public void setDampingConstant(double d)
	{
		if(d < 0){
			throw new IllegalArgumentException("The damping constant must be nonnegative.");
		}
		_damping = d;
	}

	/**
	 * Steps the simulation forward by a length of time specified in seconds.
	 * @param time a length of time in seconds
	 */
	public void step(double time)
	{
		_v = RungeKutta.solve(_v, _t, time, _accelDEQ);
		_x = RungeKutta.solve(_x, _t, time, _velDEQ);
		_t += time;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		double scale = 10;
		double h = height/2;
		double x = scale * _x;
		double rlength = scale * _restLength;
		double radius = scale * 0.5;
		g.setColor(java.awt.Color.BLACK);
		g.draw(new Line2D.Double(0,h,x,h));
		g.draw(new Line2D.Double(rlength, h-radius, rlength, h+radius));
		
		g.setColor(java.awt.Color.RED);
		Ellipse2D circle = new Ellipse2D.Double(x-radius, h-radius, 2*radius, 2*radius);
		g.fill(circle);
		g.setColor(java.awt.Color.BLACK);
		g.draw(circle);
		
		g.setColor(java.awt.Color.BLACK);
		String springState = "k = " + _k + ", d = " + _damping;
		FontMetrics fm = g.getFontMetrics();
		Rectangle2D rect = fm.getStringBounds(springState, g);
		g.drawString(springState, 5, (int)(5 + rect.getHeight()));
	}
	
	public static class SpringMouseListener implements MouseListener, MouseWheelListener
	{
		private Spring _spring;
		
		public SpringMouseListener(Spring spring)
		{
			_spring = spring;
		}
		
		@Override
		public void mouseClicked(MouseEvent e)
		{
			double x = e.getX();
			_spring.setPosition(x/10);
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseWheelMoved(MouseWheelEvent e)
		{
			int clicks = e.getWheelRotation();
			double damping = _spring.getDampingConstant() + (0.2 * clicks);
			if(damping > 0){
				_spring.setDampingConstant(damping);
			}else{
				_spring.setDampingConstant(0.0);
			}
		}
		
	}
	
	public static void main(String[] args){
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 100);
		
		double k = 3.0;
		double mass = 0.5;
		double damp = 0.5;
		double restLength = 12.5;
		double initPose = 5.0;
		
		Spring spring = new Spring(k, mass, damp, restLength);
		DrawableComponent drawable = new DrawableComponent(spring);
		SpringMouseListener listener = new SpringMouseListener(spring);
		drawable.addMouseListener(listener);
		drawable.addMouseWheelListener(listener);
		frame.add(drawable);
		frame.setVisible(true);
		
		spring.setPosition(initPose);
		
		while(frame.isVisible()){
			spring.step(0.1);
			drawable.repaint();
			try{
				Thread.sleep(100);
			}catch(InterruptedException ex){}
		}
	}
}
