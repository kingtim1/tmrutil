package tmrutil.physics;

/**
 * Represents a physical body.
 * 
 * @author Timothy Mann
 * 
 */
public abstract class Body
{
	/** The bodies position. */
	private double[] _pos;
	/** The velocity of this body. */
	private double[] _vel;

	/** The force accumulator. */
	private double[] _fa;
	/** The torque accumulator. */
	private double[] _ta;

	/** The inertia matrix of associated with this body. */
	private double[][] _I;

	/**
	 * Constructs a physical body.
	 */
	public Body()
	{
		_pos = new double[World.THREE_DIMENSIONS];
		_vel = new double[World.THREE_DIMENSIONS];
		_fa = new double[World.THREE_DIMENSIONS];
		_ta = new double[World.THREE_DIMENSIONS];
		_I = new double[4][4];
	}

	/**
	 * Sets the position of this body to the position specified by
	 * <code>pos</code>. The values of <code>pos</code> are copied to this
	 * body's position vector.
	 * 
	 * @param pos
	 *            a three dimensional vector
	 */
	public void setPosition(double[] pos)
	{
		System.arraycopy(pos, 0, _pos, 0, _pos.length);
	}

	/**
	 * Returns a deep copy of the vector describing the current position of this
	 * body.
	 * 
	 * @return a deep copy of the position vector
	 */
	public double[] getPosition()
	{
		return getPosition(new double[_pos.length]);
	}

	/**
	 * Copies the values of the position vector into the provided result vector.
	 * 
	 * @param result
	 *            a vector to copy the position vector values to
	 * @return the result vector
	 */
	public double[] getPosition(double[] result)
	{
		System.arraycopy(_pos, 0, result, 0, _pos.length);
		return result;
	}

	/**
	 * Returns a deep copy of the vector describing the current velocity of this
	 * body.
	 * 
	 * @return a deep copy of the velocity vector
	 */
	public double[] getVelocity()
	{
		return getVelocity(new double[_vel.length]);
	}

	/**
	 * Copies the values of the velocity vector into the provided result vector.
	 * 
	 * @param result
	 *            a vector to copy the velocity vector values to
	 * @return the result vector
	 */
	public double[] getVelocity(double[] result)
	{
		System.arraycopy(_vel, 0, result, 0, _vel.length);
		return result;
	}

	/**
	 * Returns a deep copy of the matrix describing the inertia of this body.
	 * 
	 * @return a deep copy of the inertia matrix
	 */
	public double[][] getInertia()
	{
		return getInertia(new double[4][4]);
	}

	/**
	 * Copies the values of the inertia matrix into the provided result matrix
	 * 
	 * @param result
	 *            a matrix to copy the inertia matrix values to
	 * @return the result matrix
	 */
	public double[][] getInertia(double[][] result)
	{
		for (int r = 0; r < _I.length; r++) {
			System.arraycopy(_I[r], 0, result[r], 0, _I[r].length);
		}
		return result;
	}
	
	/**
	 * Adds a specified force to the force accumulator of this body.
	 * @param force a force vector
	 */
	public void addForce(double[] force)
	{
		for(int i=0;i<_fa.length;i++){
			_fa[i] += force[i];
		}
	}
	
	/**
	 * Sets the values of the force accumulator to a specified force.
	 * @param force a force vector
	 */
	public void setForce(double[] force)
	{
		System.arraycopy(force, 0, _fa, 0, _fa.length);
	}
	
	/**
	 * Adds a specified torque to the torque accumulator of this body.
	 * @param torque a torque vector
	 */
	public void addTorque(double[] torque)
	{
		for(int i=0;i<_ta.length;i++){
			_ta[i] += torque[i];
		}
	}
	
	/**
	 * Sets the values of the torque accumulator to a specified torque.
	 * @param torque a torque vector
	 */
	public void setTorque(double[] torque)
	{
		System.arraycopy(torque, 0, _ta, 0, _ta.length);
	}
}
