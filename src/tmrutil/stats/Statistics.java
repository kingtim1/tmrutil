/*
 * Statistics.java
 */

package tmrutil.stats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import tmrutil.math.ArrayMatrix;
import tmrutil.math.DimensionMismatchException;
import tmrutil.math.Factorial;
import tmrutil.math.Matrix;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.stats.vector.ZeroDimensionException;

/**
 * Basic statistical operations.
 */
public class Statistics
{

	/**
	 * Calculates the sample mean of a univariate data set.
	 * 
	 * @param data
	 *            a data set where each element is one observation.
	 */
	public static final double mean(double[] data)
			throws InsufficientSamplesException
	{
		if (data.length < 1) {
			throw new InsufficientSamplesException(
					"The mean cannot be calculated without at least one sample.");
		}
		int n = data.length;
		double sum = 0;
		for (int i = 0; i < n; i++) {
			sum += data[i];
		}
		return sum / n;
	}

	/**
	 * Calculates the sample mean of a multidimensional data set.
	 * 
	 * @param data
	 *            a data set where each element is one observation.
	 */
	public static final double[] vectorMean(Collection<double[]> data)
			throws InsufficientSamplesException, ZeroDimensionException,
			DimensionMismatchException
	{
		if (data.size() < 1) {
			throw new InsufficientSamplesException(
					"The mean cannot be calculated without at least one sample.");
		}
		double[] mu = null;
		Iterator<double[]> iter = data.iterator();
		while (iter.hasNext()) {
			double[] item = iter.next();
			if (mu == null) {
				if (item.length == 0) {
					throw new ZeroDimensionException(
							"The specified data is zero dimensional.");
				}
				mu = new double[item.length];
			}
			mu = VectorOps.add(mu, item);
		}
		return VectorOps.multiply(mu, 1.0 / data.size());
	}

	/**
	 * Calculates the sample mean for a collection of scalar observations.
	 * 
	 * @param data
	 *            a collection of scalar values
	 * @param ignoreNaNandInfinite
	 *            if true the mean returned will ignore NaN values and (positive
	 *            or negative) infinite values
	 * @return the sample mean of the given collection
	 */
	public static final double mean(Collection<Double> data,
			boolean ignoreNaNandInfinite)
	{
		if (data.size() < 1) {
			throw new InsufficientSamplesException(
					"The mean cannot be calculated without at least one sample.");
		}
		int ignoreCount = 0;
		double mu = 0;
		Iterator<Double> iter = data.iterator();
		while (iter.hasNext()) {
			double val = iter.next();
			if (Double.isNaN(val) || Double.isInfinite(val)) {
				ignoreCount++;
			} else {
				mu += val;
			}
		}
		return mu / (data.size() - ignoreCount);
	}

	/**
	 * Calculates the sample mean for a collection of scalar observations.
	 * 
	 * @param data
	 *            a collection of scalar values
	 * @return the sample mean of the given collection
	 */
	public static final double mean(Collection<Double> data)
	{
		return mean(data, false);
	}

	/**
	 * Calculates the sample mean of a multidimensional data set.
	 * 
	 * @param data
	 *            a data set where each row is one observation.
	 */
	public static final double[] columnMean(double[][] data)
			throws InsufficientSamplesException, ZeroDimensionException,
			DimensionMismatchException
	{
		if (data.length < 1) {
			throw new InsufficientSamplesException(
					"The mean cannot be calculated without at least one sample.");
		}
		int n = data.length;
		if (data[0].length == 0) {
			throw new ZeroDimensionException(
					"The specified data is zero dimensional.");
		}
		int dim = data[0].length;

		double[] mu = new double[dim];

		for (int i = 0; i < n; i++) {
			if (mu.length != data[i].length) {
				throw new DimensionMismatchException(
						"One or more samples contains dimensions of different lengths.");
			}
			for (int j = 0; j < dim; j++) {
				mu[j] += data[i][j];
			}
		}

		for (int j = 0; j < dim; j++) {
			mu[j] = mu[j] / n;
		}

		return mu;
	}

	/**
	 * Calculates the average matrix from an {@link Iterable} source of
	 * matrices. We assume that the iterable source is finite and that each of
	 * the matrices have the same dimensions.
	 * 
	 * @param matrices a finite, iterable source of matrices with the same dimensions
	 * @return the average matrix
	 */
	public static final Matrix matrixMean(Iterable<Matrix> matrices)
	{
		int count = 0;
		Matrix sum = null;
		for (Matrix m : matrices) {
			if (sum == null) {
				sum = new ArrayMatrix(m.rows(), m.cols());
			}
			sum.addToThis(m);
			count++;
		}
		sum.multiplyThisBy(1.0 / count);
		return sum;
	}

	/**
	 * Calculates a specified percentile from the data set.
	 * 
	 * @param data
	 *            a set of data samples
	 * @param percentile
	 *            a value in the range [0, 100].
	 * @return the percentile value from the given data
	 */
	public static final double percentile(double[] data, double percentile)
			throws IllegalArgumentException
	{
		if (percentile < 0 || percentile > 100) {
			throw new IllegalArgumentException(
					"A percentile must be in the range [0, 100]. The percentile specified : "
							+ percentile);
		}
		double[] dataCopy = Arrays.copyOf(data, data.length);
		Arrays.sort(dataCopy);
		double placement = (data.length + 1) * (percentile / 100);
		if (placement > Math.floor(placement)) {
			return (dataCopy[(int) Math.floor(placement)] + dataCopy[(int) Math.ceil(placement)]) / 2;
		} else {
			return dataCopy[(int) Math.floor(placement)];
		}
	}

	/**
	 * Calculates the lower quartile value.
	 * 
	 * @param data
	 *            a set of data samples
	 * @return the lower quartile value for the given data
	 */
	public static final double lowerQuartile(double[] data)
	{
		return percentile(data, 25);
	}

	/**
	 * Calculates the upper quartile value.
	 * 
	 * @param data
	 *            a set of data samples
	 * @return the upper quartile value for the given data
	 */
	public static final double upperQuartile(double[] data)
	{
		return percentile(data, 75);
	}

	/**
	 * Calculates the median of a univariate data set.
	 * 
	 * @param data
	 *            a univariate data set where each element is one observation
	 */
	public static final double median(double[] data)
	{
		double[] dataCopy = Arrays.copyOf(data, data.length);
		Arrays.sort(dataCopy);
		if (dataCopy.length % 2 == 0) {
			return (dataCopy[dataCopy.length / 2 - 1] + dataCopy[dataCopy.length / 2]) / 2;
		} else {
			return dataCopy[dataCopy.length / 2];
		}
	}

	/**
	 * Calculates the median of a univariate data set.
	 * 
	 * @param data
	 *            a univariate data set where each element is one observation
	 */
	public static final double median(Collection<Double> data)
	{
		List<Double> dataCopy = new ArrayList<Double>(data);
		Collections.sort(dataCopy);
		if (dataCopy.size() % 2 == 0) {
			return (dataCopy.get(dataCopy.size() / 2 - 1) + dataCopy.get(dataCopy.size() / 2)) / 2;
		} else {
			return dataCopy.get(dataCopy.size() / 2);
		}
	}

	/**
	 * Calculates the sum of the squared elements of a vector <code>data</code>
	 * of samples.
	 * 
	 * @param data
	 *            an array of scalar sample values
	 * @return the sum of squares of a vector of samples
	 */
	public static final double sumOfSquares(double[] data)
	{
		double sumOfSquares = 0;
		for (int i = 0; i < data.length; i++) {
			sumOfSquares += data[i] * data[i];
		}
		return sumOfSquares;
	}

	/**
	 * Calculates the sum of the squared elements of a collection
	 * <code>data</code> of samples.
	 * 
	 * @param data
	 *            a collection of scalar sample values
	 * @return the sum of squares of the specified collection of samples
	 */
	public static final double sumOfSquares(Collection<Double> data)
	{
		double sumOfSquares = 0;
		for (Double datum : data) {
			sumOfSquares += datum * datum;
		}
		return sumOfSquares;
	}

	/**
	 * Computes the variance of a collection of multidimensional data vectors.
	 * 
	 * @param data
	 *            a collection of multidimensional data vectors
	 * @return a variance vector
	 * @throws InsufficientSamplesException
	 *             if the collection of data vectors is empty
	 */
	public static final double[] vectorVariance(Collection<double[]> data)
			throws InsufficientSamplesException
	{
		if (data.size() < 1) {
			throw new InsufficientSamplesException(
					"At least one sample is necessary to compute the variance.");
		}
		double[] mean = vectorMean(data);
		double[] sumOfSquares = new double[mean.length];
		for (double[] sample : data) {
			for (int i = 0; i < sample.length; i++) {
				sumOfSquares[i] += sample[i] * sample[i];
			}
		}

		int n = data.size();
		double[] result = new double[mean.length];
		for (int i = 0; i < sumOfSquares.length; i++) {
			result[i] = (sumOfSquares[i] - (n * mean[i] * mean[i])) / (n + 1);
		}

		return result;
	}

	/**
	 * Calculates the unbiased sample variance of a univariate data set.
	 * 
	 * @param data
	 *            a univariate data set where each element is one observation
	 * @param ignoreNaNandInfinite
	 *            if true NaN values and (positive or negative) infinite values
	 *            will be ignored
	 * @return the sample variance of the given collection of observations
	 */
	public static final double variance(Collection<Double> data,
			boolean ignoreNaNandInfinite) throws InsufficientSamplesException
	{
		if (data.size() == 1) {
			return 0;
		}
		double mu = mean(data, ignoreNaNandInfinite);
		int n = data.size();
		int ignoreCount = 0;

		double sum = 0;
		Iterator<Double> iter = data.iterator();
		while (iter.hasNext()) {
			double val = iter.next();
			if (Double.isNaN(val) || Double.isInfinite(val)) {
				ignoreCount++;
			} else {
				double x = val - mu;
				sum = sum + (x * x);
			}
		}
		return sum / ((n - ignoreCount) - 1);
	}

	/**
	 * Calculates the unbiased sample variance of a univariate data set.
	 * 
	 * @param data
	 *            a univariate data set where each element is one observation
	 * @return the sample variance for the given collection of observations
	 */
	public static final double variance(Collection<Double> data)
	{
		return variance(data, false);
	}

	/**
	 * Calculates the unbiased sample variance of a univariate data set.
	 * 
	 * @param data
	 *            a univariate data set where each element is one observation.
	 */
	public static final double variance(double[] data)
			throws InsufficientSamplesException
	{
		if (data.length == 1) {
			return 0;
		}

		double mu = mean(data);
		int n = data.length;

		double sum = 0;
		for (int i = 0; i < n; i++) {
			double x = (data[i] - mu);
			sum = sum + (x * x);
		}
		return sum / (n - 1);
	}
	
	/**
	 * Returns the standard deviation of each column in the specified dataset.
	 * @param data a rectangular data matrix
	 * @return the standard deviation of each column
	 */
	public static final double[] columnStd(double[][] data)
	{
		double[] std = new double[data[0].length];
		for(int c=0;c<std.length;c++){
			std[c] = std(MatrixOps.getColumn(data, c));
		}
		return std;
	}

	/**
	 * Calculates the unbiased sample standard deviation of a multivariate data
	 * set.
	 * 
	 * @param data
	 *            a collection of data vectors
	 * @return a standard deviation vector
	 */
	public static final double[] vectorStd(Collection<double[]> data)
	{
		double[] result = vectorVariance(data);
		for (int i = 0; i < result.length; i++) {
			result[i] = Math.sqrt(result[i]);
		}
		return result;
	}

	/**
	 * Calculates the sample standard deviation of a univariate data set.
	 * 
	 * @param data
	 *            a univariate data set where each element is one observation
	 * @param ignoreNaNandInfinite
	 *            if true then NaN values and (positive or negative) infinite
	 *            values will be ignored
	 */
	public static final double std(Collection<Double> data,
			boolean ignoreNaNandInfinite) throws InsufficientSamplesException
	{
		return Math.sqrt(variance(data, ignoreNaNandInfinite));
	}

	/**
	 * Calculates the sample standard deviation of a univariate data set.
	 * 
	 * @param data
	 *            a univariate data set where each element is one observation
	 * @return the sample standard deviation of the given collection of
	 *         observations
	 */
	public static final double std(Collection<Double> data)
	{
		return std(data, false);
	}

	/**
	 * Calculates the sample standard deviation of a univariate data set.
	 * 
	 * @param data
	 *            a univariate data set where each element is one observation.
	 */
	public static final double std(double[] data)
			throws InsufficientSamplesException
	{
		double var = variance(data);
		return Math.sqrt(var);
	}

	/**
	 * Calculates the unbiased sample covariance matrix of a multivariate data
	 * set.
	 * 
	 * @param data
	 *            a multivariate data set where each row is one observation.
	 */
	public static final double[][] covariance(double[][] data)
			throws InsufficientSamplesException, ZeroDimensionException,
			DimensionMismatchException
	{
		double[] mu = columnMean(data);
		int n = data.length;
		int dim = data[0].length;
		double[][] zeroMeanData = new double[n][];
		for (int i = 0; i < n; i++) {
			zeroMeanData[i] = VectorOps.subtract(data[i], mu);
		}

		double[][] cov = MatrixOps.multiply(MatrixOps.transpose(zeroMeanData),
				zeroMeanData);
		return MatrixOps.multiply(cov, 1.0 / (n - 1));
	}

	/**
	 * Calculates the unbiased sample covariance matrix of a multivariate data
	 * set.
	 * 
	 * @param data
	 *            a multivariate data set where each row is one observation
	 * @return the unbiased sample covariance matrix
	 * @throws InsufficientSamplesException
	 *             if <code>data.size() == 0</code>
	 * @throws ZeroDimensionException
	 *             if any of the vectors in <code>data</code> have zero
	 *             dimensions
	 * @throws DimensionMismatchException
	 *             if any of the vectors in <code>data</code> do not have the
	 *             same number of components
	 */
	public static final double[][] covariance(Collection<double[]> data)
			throws InsufficientSamplesException, ZeroDimensionException,
			DimensionMismatchException
	{
		int n = data.size();
		double[][] dataMat = new double[n][];
		int index = 0;
		for (double[] point : data) {
			dataMat[index] = point;
			index++;
		}

		return covariance(dataMat);
	}

	/**
	 * Returns a matrix of z-scores for a set of data points. The sample mean
	 * and sample standard deviations are computed based on the given points.
	 * 
	 * @param data
	 *            a matrix of data points (each row is a sample)
	 * @return z-scores for each point in data
	 */
	public static final double[][] zscore(double[][] data)
	{
		double[] mean = columnMean(data);
		double[] std = new double[mean.length];
		for (int i = 0; i < std.length; i++) {
			std[i] = std(MatrixOps.getColumn(data, i));
		}
		return zscore(data, mean, std);
	}

	/**
	 * Returns a matrix of z-scores for a set of data points, mean, and standard
	 * deviations.
	 * 
	 * @param data
	 *            a matrix of data points (each row is a sample)
	 * @param mean
	 *            the mean of the distribution
	 * @param std
	 *            the standard deviations for each component of a data point
	 * @return z-scores for each point in data
	 */
	public static final double[][] zscore(double[][] data, double[] mean,
			double[] std)
	{
		int numRows = MatrixOps.getNumRows(data);
		int numCols = MatrixOps.getNumCols(data);
		double[][] zscores = new double[numRows][numCols];
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < zscores[i].length; j++) {
				zscores[i][j] = (data[i][j] - mean[j]) / std[j];
			}
		}
		return zscores;
	}
	
	/**
	 * Returns the z-score of a single vector <code>x</code> given a mean and standard deviation vector.
	 * @param x a vector
	 * @param mean a vector with length equal to <code>x.length</code>
	 * @param std a vector containing nonzero positive components and length equal to <code>x.length</code>
	 * @return the z-score of <code>x</code>
	 */
	public static final double[] zscore(double[] x, double[] mean, double[] std)
	{
		double[] z = new double[x.length];
		for(int j = 0; j < z.length; j++){
			z[j] = (x[j] - mean[j]) / std[j];
		}
		return z;
	}

	/**
	 * Returns a matrix of z-scores for a collection of data points. The sample
	 * mean and sample standard deviations are computed from the given data
	 * points.
	 * 
	 * @param data
	 *            a collection of data points
	 * @return z-scores for each point in data
	 */
	public static final double[][] zscore(Collection<double[]> data)
	{
		double[][] dataMat = new double[data.size()][];
		int index = 0;
		for (double[] point : data) {
			dataMat[index] = point;
			index++;
		}
		return zscore(dataMat);
	}

	/**
	 * Returns a matrix of z-scores for a collection of data points, mean, and
	 * standard deviations.
	 * 
	 * @param data
	 *            a collection of data points
	 * @param mean
	 *            the mean of the distribution
	 * @param std
	 *            the standard deviations for each component of a data point
	 * @return z-scores for each point in data
	 */
	public static final double[][] zscore(Collection<double[]> data,
			double[] mean, double[] std)
	{
		int numSamples = data.size();
		double[][] zscores = new double[numSamples][mean.length];
		int index = 0;
		for (double[] point : data) {
			for (int j = 0; j < point.length; j++) {
				zscores[index][j] = (point[j] - mean[j]) / std[j];
			}
			index++;
		}
		return zscores;
	}

	/**
	 * Counts the number of nonzero components in a collection of real values.
	 * 
	 * @param c
	 *            a collection of real values
	 * @return the number of nonzero elements in <code>c</code>
	 */
	public static final int count(Collection<? extends Number> c)
	{
		int sum = 0;
		for (Number n : c) {
			double val = n.doubleValue();
			if (val != 0) {
				sum++;
			}
		}
		return sum;
	}

	/**
	 * Counts the number of nonzero components of a vector.
	 * 
	 * @param v
	 *            a vector
	 * @return the number of nonzero components contained by <code>v</code>
	 */
	public static final int count(double[] v)
	{
		int sum = 0;
		for (int i = 0; i < v.length; i++) {
			if (v[i] != 0.0) {
				sum++;
			}
		}
		return sum;
	}

	/**
	 * Returns the sum of all values in a collection.
	 * 
	 * @param c
	 *            a collection of doubles
	 * @return the sum of the collection <code>c</code>
	 * @throws InsufficientSamplesException
	 *             if c is empty
	 */
	public static final <N extends Number> double sum(Collection<N> c)
			throws InsufficientSamplesException
	{
		if (c.size() < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute a sum.");
		}
		double sum = 0;
		Iterator<N> iter = c.iterator();
		while (iter.hasNext()) {
			sum += iter.next().doubleValue();
		}
		return sum;
	}

	/**
	 * Returns a vector containing the sum of each of the columns.
	 */
	public static final double[] sum(double[][] m)
			throws InsufficientSamplesException
	{
		if (m.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute a sum.");
		}
		double[] s = new double[MatrixOps.getNumCols(m)];
		for (int c = 0; c < s.length; c++) {
			for (int r = 0; r < MatrixOps.getNumRows(m); r++) {
				s[c] += m[r][c];
			}
		}
		return s;
	}

	/**
	 * Returns the sum of all of the components in a vector.
	 */
	public static final double sum(double[] v)
			throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute a sum.");
		}
		double s = 0;
		for (int i = 0; i < v.length; i++) {
			s = s + v[i];
		}
		return s;
	}

	/**
	 * Returns the sum of all of the components in a vector.
	 */
	public static final int sum(int[] v) throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute a sum.");
		}
		int s = 0;
		for (int i = 0; i < v.length; i++) {
			s = s + v[i];
		}
		return s;
	}

	/**
	 * Returns the product of all of the components in a vector.
	 */
	public static final double product(double[] v)
			throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute a product.");
		}
		double p = 1;
		for (int i = 0; i < v.length; i++) {
			p = p * v[i];
		}
		return p;
	}

	/**
	 * Returns the product of all of the components in a vector.
	 */
	public static final int product(int[] v)
			throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute a product.");
		}
		int p = 1;
		for (int i = 0; i < v.length; i++) {
			p = p * v[i];
		}
		return p;
	}

	/**
	 * Returns the largest component of a specified vector.
	 */
	public static final double max(double[] v)
			throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the max.");
		}
		double max = v[0];
		for (int i = 1; i < v.length; i++) {
			if (max < v[i]) {
				max = v[i];
			}
		}
		return max;
	}

	/**
	 * Returns the largest value in this collection.
	 * 
	 * @param c
	 *            a collection of numbers
	 * @return the largest value in this collection
	 * @throws InsufficientSamplesException
	 *             if there are no elements in <code>c</code>
	 */
	public static final <N extends Number> double max(Collection<N> c)
			throws InsufficientSamplesException
	{
		if (c.size() < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the max.");
		}
		Iterator<N> iter = c.iterator();
		double max = iter.next().doubleValue();
		while (iter.hasNext()) {
			double val = iter.next().doubleValue();
			if (val > max) {
				max = val;
			}
		}
		return max;
	}

	/**
	 * Returns the largest value in an array of integers.
	 * 
	 * @param v
	 *            a vector of integers
	 * @return the maximum value in v
	 * @throws InsufficientSamplesException
	 *             if there are no elements in <code>v</code>
	 */
	public static final int max(int[] v) throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the max.");
		}
		int max = v[0];
		for (int i = 1; i < v.length; i++) {
			if (v[i] > max) {
				max = v[i];
			}
		}
		return max;
	}

	/**
	 * Returns the index of the first instance of the maximum component of a
	 * specified vector.
	 */
	public static final int maxIndex(double[] v)
			throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the max.");
		}
		double max = v[0];
		int index = 0;
		for (int i = 1; i < v.length; i++) {
			if (max < v[i]) {
				max = v[i];
				index = i;
			}
		}
		return index;
	}

	/**
	 * Returns the index of the first instance of the maximum component of a specified vector.
	 * @param v
	 * @return index of the first instance of the maximum component of <code>v</code>
	 */
	public static int maxIndex(int[] v)
	{
		if(v.length < 1){
			throw new InsufficientSamplesException("Must have at least one sample to compute the max.");
		}
		double max = v[0];
		int index = 0;
		for(int i=0;i<v.length;i++){
			if(max < v[i]){
				max = v[i];
				index = i;
			}
		}
		return index;
	}
	
	/**
	 * Returns the index of the first instance of the maximum component of a
	 * specified list of doubles.
	 */
	public static final int maxIndex(List<? extends Number> v)
			throws InsufficientSamplesException
	{
		if (v.size() < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the max.");
		}
		double max = v.get(0).doubleValue();
		int index = 0;
		for (int i = 1; i < v.size(); i++) {
			if (max < v.get(i).doubleValue()) {
				max = v.get(i).doubleValue();
				index = i;
			}
		}
		return index;
	}

	/**
	 * Returns the smallest value in a collection of doubles.
	 */
	public static final double min(Collection<? extends Number> c)
			throws InsufficientSamplesException
	{
		if (c.size() < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the min.");
		}
		Iterator<? extends Number> iter = c.iterator();
		double min = iter.next().doubleValue();
		while (iter.hasNext()) {
			double v = iter.next().doubleValue();
			if (min > v) {
				min = v;
			}
		}
		return min;
	}

	public static final int minIndex(List<? extends Number> c)
			throws InsufficientSamplesException
	{
		if (c.size() < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the min.");
		}
		double min = c.get(0).doubleValue();
		int minI = 0;
		for (int i = 1; i < c.size(); i++) {
			if (min > c.get(i).doubleValue()) {
				min = c.get(i).doubleValue();
				minI = i;
			}
		}
		return minI;
	}

	/**
	 * Returns the smallest component of a specified vector.
	 */
	public static final double min(double[] v)
			throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the min.");
		}
		double min = v[0];
		for (int i = 1; i < v.length; i++) {
			if (min > v[i]) {
				min = v[i];
			}
		}
		return min;
	}

	/**
	 * Returns the index of the first instance of the smallest component of a
	 * specified vector.
	 */
	public static int minIndex(double[] v) throws InsufficientSamplesException
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the min.");
		}
		double min = v[0];
		int index = 0;
		for (int i = 1; i < v.length; i++) {
			if (min > v[i]) {
				min = v[i];
				index = i;
			}
		}
		return index;
	}

	/**
	 * Returns the index of the first instance of the smallest component of a specified vector.
	 * @param v a vector
	 * @return the index of the first instance of the smallest component
	 */
	public static int minIndex(int[] v)
	{
		if (v.length < 1) {
			throw new InsufficientSamplesException(
					"Must have at least one sample to compute the min.");
		}
		double min = v[0];
		int index = 0;
		for (int i = 1; i < v.length; i++) {
			if (min > v[i]) {
				min = v[i];
				index = i;
			}
		}
		return index;
	}
	
	/**
	 * Returns the maximum element in the array.
	 */
	public static final double max(double[][] a)
	{
		double max = a[0][0];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				if (a[i][j] > max) {
					max = a[i][j];
				}
			}
		}
		return max;
	}

	/**
	 * Returns the minimum element in the array.
	 */
	public static final double min(double[][] a)
	{
		double min = a[0][0];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				if (a[i][j] < min) {
					min = a[i][j];
				}
			}
		}
		return min;
	}

	/**
	 * Returns the binomial coefficient.
	 * 
	 * @throws IllegalArgumentException
	 *             if <code>n &lt; r</code> or either <code>n</code> or
	 *             <code>r</code> are negative.
	 */
	public static final int nCr(int n, int r) throws IllegalArgumentException
	{
		if (n < r) {
			throw new IllegalArgumentException("n(" + n
					+ ") must be larger than r(" + r
					+ ") to compute the binomial coefficient");
		}
		if (n < 0) {
			throw new IllegalArgumentException(
					"n("
							+ n
							+ ") must be non-negative to compute the binomial coefficient");
		}
		if (r < 0) {
			throw new IllegalArgumentException(
					"r("
							+ r
							+ ") must be non-negative to compute the binomial coefficient");
		}

		int ncr = 1;
		for (int i = 1; i <= r; i++) {
			ncr *= (n - (r - i)) / i;
		}
		return ncr;

		// // Take advantage of the fact that the smaller r will provide the
		// same
		// // result
		// if (r > n / 2) {
		// r = n - r;
		// }
		// return Factorial.fact(n) / (Factorial.fact(r) * Factorial.fact(n -
		// r));
	}

	/**
	 * Returns the number of permutations.
	 */
	public static final int nPr(int n, int r) throws IllegalArgumentException
	{
		if (n < r) {
			throw new IllegalArgumentException("n(" + n
					+ ") must be larger than r(" + r
					+ ") to compute the number of permutations");
		}
		if (n < 0) {
			throw new IllegalArgumentException(
					"n("
							+ n
							+ ") must be non-negative to compute the number of permutations");
		}
		if (r < 0) {
			throw new IllegalArgumentException(
					"r("
							+ r
							+ ") must be non-negative to compute the number of permutations");
		}
		return Factorial.fact(n) / Factorial.fact(n - r);
	}

	/**
	 * Compute the sample covariance between two random variables.
	 * 
	 * @param x1
	 *            a vector of samples
	 * @param x2
	 *            a vector of samples
	 * @return the covariance between x1 and x2
	 */
	public static double covariance(double[] x1, double[] x2)
	{
		if (x1.length != x2.length) {
			throw new IllegalArgumentException(
					"Computation of the covariance requires an equal number of samples.");
		}
		double mu1 = mean(x1);
		double mu2 = mean(x2);
		return mean(VectorOps.multiply(x1, x2)) - (mu1 * mu2);
	}

	/**
	 * Compute the sample covariance between two random variables.
	 * 
	 * @param x1
	 *            a vector of samples
	 * @param x2
	 *            a vector of samples
	 * @return the correlation between x1 and x2
	 */
	public static double correlation(double[] x1, double[] x2)
	{
		double cov = covariance(x1, x2);
		double std1 = std(x1);
		double std2 = std(x2);
		double rho = cov / (std1 * std2);
		return rho;
	}

	private Statistics()
	{
	}

}