package tmrutil.stats;

/**
 * A conditional generative distribution.
 * @author Timothy A. Mann
 *
 * @param <X> the outcome type
 * @param <Y> the conditional information type
 */
public interface ConditionalGenerativeDistribution<X, Y> {

	/**
	 * Samples an outcome from this distribution conditional on <code>given</code>.
	 * @param given conditional information
	 * @return an outcome sampled according to P(.|given).
	 */
	public X sample(Y given);
}
