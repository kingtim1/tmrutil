package tmrutil.stats;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import tmrutil.util.Counter;

/**
 * Efficient sampling from a categorical distribution where only a small number
 * of outcomes have non-zero probability.
 * 
 * @author Timothy A. Mann
 *
 */
public class SparseCategoricalDistribution implements
		GenerativeDistribution<Integer> {

	private int[] _nonzeroOutcomes;
	private double[] _nonzeroCDF;
	
	public SparseCategoricalDistribution(Counter<Integer> counts, int total){
		this(toDistribution(counts, total));
	}
	
	public SparseCategoricalDistribution(Map<Integer, Double> nonzeroProbs) {
		int n = nonzeroProbs.size();
		Set<Integer> nonzeroOutcomes = nonzeroProbs.keySet();
		_nonzeroOutcomes = new int[n];
		int i=0;
		for(Integer oc : nonzeroOutcomes){
			_nonzeroOutcomes[i] = oc;
			i++;
		}
		
		_nonzeroCDF = new double[n];
		for(i=0;i<n;i++){
			int oc = _nonzeroOutcomes[i];
			double iprob = nonzeroProbs.get(oc);
			if(iprob < 0){
				throw new IllegalArgumentException("Invalid probability distribution. Detected negative probability (" + iprob + ") for outcome (" + oc + ").");
			}
			if(i==0){
				_nonzeroCDF[i] = iprob;
			}else{
				_nonzeroCDF[i] = _nonzeroCDF[i-1] + iprob;
			}
		}
		
		double sum = _nonzeroCDF[n-1];
		if(Math.abs(sum-1) > tmrutil.math.Constants.EPSILON){
			throw new IllegalArgumentException("Invalid probability distribution. Probabilities do not sum to 1. Their sum is : " + sum);
		}
	}

	@Override
	public Integer sample() {
		double r = Random.uniform();
		int index = Arrays.binarySearch(_nonzeroCDF, r);
		if(index >= 0){
			return _nonzeroOutcomes[index];
		}else{
			int insertionPoint = -index - 1;
			return _nonzeroOutcomes[insertionPoint];
		}
	}
	
	public static final Map<Integer,Double> toDistribution(Counter<Integer> counts, int total){
		Map<Integer,Double> dist = new HashMap<Integer,Double>();
		
		Set<Integer> nonzero = counts.nonzero();
		for(Integer oc : nonzero){
			double p = counts.count(oc) / (double) total;
			dist.put(oc, p);
		}
		
		return dist;
	}

}
