package tmrutil.stats;

import java.util.Arrays;

public final class Random {
	// public static final java.util.Random RAND = new
	// java.security.SecureRandom();
	public static final java.util.Random RAND = new java.util.Random();

	private Random() {
	}

	/**
	 * Generates a uniform random number in the range [0,1].
	 * 
	 * @return a uniform random number in the range [0,1].
	 */
	public static final double uniform() {
		return RAND.nextDouble();
	}

	/**
	 * Generates a uniform random number in the range [<code>lowerBound</code>,
	 * <code>upperBound</code> ].
	 * 
	 * @param lowerBound
	 *            a lower bound on the randomly generated value
	 * @param upperBound
	 *            an upper bound on the randomly generated value
	 * @return a uniform random number in the range [<code>lowerBound</code>,
	 *         <code>upperBound</code>].
	 */
	public static final double uniform(double lowerBound, double upperBound) {
		if (upperBound < lowerBound) {
			throw new IllegalArgumentException("lowerBound(" + lowerBound
					+ ") must be less than upperBound(" + upperBound + ").");
		}
		double diff = upperBound - lowerBound;
		return lowerBound + (RAND.nextDouble() * diff);
	}

	/**
	 * Generates a standard normal distributed random number.
	 * 
	 * @return a standard normal distribute random number.
	 */
	public static final double normal() {
		return RAND.nextGaussian();
	}

	/**
	 * Generates a normally distributed random number with mean <code>mu</code>
	 * and standard deviation <code>sigma</code>.
	 * 
	 * @param mu
	 *            the mean
	 * @param sigma
	 *            the standard deviation
	 */
	public static final double normal(double mu, double sigma) {
		return (normal() * sigma) + mu;
	}

	public static void main(String[] args) {
		double[] r = new double[100000];
		for (int i = 0; i < r.length; i++) {
			r[i] = normal(5, 0.4);
		}
		tmrutil.graphics.plot2d.Plot2D.histogram(r, 100, java.awt.Color.RED,
				"100000 Normal Samples", "X", "Y");
	}

	/**
	 * Generates an integer from a uniform distribution with a value of [0,
	 * n-1].
	 * 
	 * @param n
	 *            a positive integer
	 * @return an integer with a value in the range of [0, n-1]
	 */
	public static final int nextInt(int n) {
		return RAND.nextInt(n);
	}

	/**
	 * Generates an integer from a uniform distribution.
	 * 
	 * @return a random integer
	 */
	public static final int nextInt() {
		return RAND.nextInt();
	}

	/**
	 * Samples an integer according to the specified distribution.
	 * 
	 * @param dist
	 *            a vector with nonnegative components that sum to one
	 * @return an integer in the set {0, 1, ... , <code>dist.length-1</code>
	 *         distributed according to <code>dist</code>
	 */
	public static final int sample(double[] dist) {
		double[] cdf = new double[dist.length];
		cdf[0] = dist[0];
		for (int i = 1; i < dist.length; i++) {
			cdf[i] = cdf[i - 1] + dist[i];
		}
		return sampleFromCDF(cdf);
	}

	/**
	 * Samples an integer according to the distribution specified by a
	 * cumulative probability distribution.
	 * 
	 * @param cdf
	 *            a vector with nonnegative components in increasing order and
	 *            the last elements value is one
	 * @return an integer in the set {0, 1, ... , <code>cdf.length-1</code>
	 *         distributed according to <code>cdf</code>
	 */
	public static final int sampleFromCDF(double[] cdf) {
		double r = uniform();
		int index = Arrays.binarySearch(cdf, r);
		if (index >= 0) {
			return index;
		} else {
			return -index - 1;
		}
	}

	/**
	 * Returns a random permutation of an array. The returned array is a copy of
	 * the argument and does not affect the order of the array passed in as the
	 * argument.
	 */
	public static final <T> T[] rpermute(T[] v) {
		T[] copyV = Arrays.copyOf(v, v.length);
		for (int i = 0; i < v.length; i++) {
			int i1 = RAND.nextInt(copyV.length);
			int i2 = RAND.nextInt(copyV.length);
			T tmp = copyV[i1];
			copyV[i1] = copyV[i2];
			copyV[i2] = tmp;
		}
		return copyV;
	}

	/**
	 * Returns a random permutation of an array. The returned array is a copy of
	 * the argument and does not affect the order of the array passed in as the
	 * argument.
	 */
	public static final int[] rpermute(int[] v) {
		int[] copyV = Arrays.copyOf(v, v.length);
		for (int i = 0; i < v.length; i++) {
			int i1 = RAND.nextInt(copyV.length);
			int i2 = RAND.nextInt(copyV.length);
			int tmp = copyV[i1];
			copyV[i1] = copyV[i2];
			copyV[i2] = tmp;
		}
		return copyV;
	}

	/**
	 * Returns a random permutation of an array. The returned array is a copy of
	 * the argument and does not affect the order of the array passed in as the
	 * argument.
	 */
	public static final double[] rpermute(double[] v) {
		double[] copyV = Arrays.copyOf(v, v.length);
		for (int i = 0; i < v.length; i++) {
			int i1 = RAND.nextInt(copyV.length);
			int i2 = RAND.nextInt(copyV.length);
			double tmp = copyV[i1];
			copyV[i1] = copyV[i2];
			copyV[i2] = tmp;
		}
		return copyV;
	}

	/**
	 * Randomly generates a boolean value with equal probability for true and
	 * false.
	 * 
	 * @return a boolean value
	 */
	public static boolean nextBoolean() {
		return RAND.nextBoolean();
	}

	/**
	 * Samples a Bernoulli random variable with a specified probability of
	 * success.
	 * 
	 * @param probOfSuccess
	 *            the probability of a success
	 * @return true if the sample resulted in success and false if the sample
	 *         resulted in failure
	 */
	public static boolean bernoulli(double probOfSuccess) {
		double r = uniform();
		if (r < probOfSuccess) {
			return true;
		} else {
			return false;
		}
	}
}
