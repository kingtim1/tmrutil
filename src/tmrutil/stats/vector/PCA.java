/*
 * PCA.java
 */

package tmrutil.stats.vector;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.util.Sort;

/**
 * Utility class for performing Principal Components Analysis (PCA) on a set of
 * data samples.
 */
public class PCA {

	/** The dimension of the data. */
	private int _dimension;

	/** The sample covariance matrix. */
	private RealMatrix _covMatrix;

	/** The sample mean. */
	private RealVector _mean;
	/** The sample standard deviation. */
	private RealVector _std;

	/** The computed eigenvectors. */
	private RealMatrix _eigenVectors;

	/** The computed eigenvalues. */
	private RealVector _eigenValues;

	/**
	 * Creates a new PCA object given a data set.
	 * 
	 * @param data
	 *            a data set where each row is one sample.
	 */
	public PCA(RealMatrix data) {
		this(covariance(data));
	}

	private static CovarianceMeanStd covariance(RealMatrix data) {
		CovarianceEstimator covEst = new CovarianceEstimator(
				data.getColumnDimension());
		for (int i = 0; i < data.getRowDimension(); i++) {
			covEst.add(data.getColumn(i));
		}
		RealMatrix covMatrix = covEst.estimate();
		RealVector mean = covEst.estimateMean();
		RealVector std = covEst.estimateStd();
		return new CovarianceMeanStd(covMatrix, mean, std);
	}

	/**
	 * This private helper class bundles the covariance matrix with mean and
	 * standard deviation. This is used to allow reusing the same constructor
	 * method.
	 * 
	 * @author Timothy A. Mann
	 *
	 */
	private static class CovarianceMeanStd {
		RealMatrix cov;
		RealVector mean;
		RealVector std;

		public CovarianceMeanStd(RealMatrix covMatrix, RealVector mean,
				RealVector std) {
			this.cov = covMatrix;
			this.mean = mean;
			this.std = std;
		}
	}

	/**
	 * Performs PCA using the sample covariance matrix and sample mean.
	 * 
	 * @param covMatrix
	 *            the sample covariance matrix
	 * @param mean
	 *            the sample mean
	 */
	public PCA(RealMatrix covMatrix, RealVector mean, RealVector std) {
		this(new CovarianceMeanStd(covMatrix, mean, std));
	}

	private PCA(CovarianceMeanStd triple) {
		RealMatrix covMatrix = triple.cov;
		RealVector mean = triple.mean;
		RealVector std = triple.std;

		_dimension = mean.getDimension();
		_covMatrix = covMatrix;
		_mean = mean;
		_std = std;

		EigenDecomposition eigenDecomp = new EigenDecomposition(_covMatrix);
		RealMatrix eigenValues = eigenDecomp.getD();
		// Store the eigenvalues in a vector
		double[] deigenValues = new double[_dimension];
		for (int i = 0; i < _dimension; i++) {
			deigenValues[i] = eigenValues.getEntry(i, i);
		}

		// Establish the order of importance based on the size of the
		// eigenvalues
		int[] pcOrder = Sort.sortIndex(deigenValues, false);
		deigenValues = Sort.permute(deigenValues, pcOrder);
		_eigenValues = new ArrayRealVector(deigenValues);

		double[][] eigenVectors = eigenDecomp.getV().getData();
		double[][] deigenVectors = new double[_dimension][_dimension];
		for (int j = 0; j < _dimension; j++) {
			double[] column = MatrixOps.getColumn(eigenVectors, pcOrder[j]);
			// System.out.println(j + ". " + VectorOps.toString(column));
			for (int i = 0; i < _dimension; i++) {
				deigenVectors[i][j] = column[i];
			}
		}
		_eigenVectors = MatrixUtils.createRealMatrix(deigenVectors);
	}

	/**
	 * Returns a projection matrix with a specified number of dimensions.
	 * @param numDimensions
	 * @return a projection matrix
	 */
	public RealMatrix projectionEigs(int numDimensions) {
		double[][] linTran = new double[numDimensions][];
		for (int j = 0; j < numDimensions; j++) {
			double[] column = _eigenVectors.getColumn(j);
			linTran[j] = VectorOps.normalize(column);
		}
		return MatrixUtils.createRealMatrix(linTran);
	}

	/**
	 * Projects a single high dimensional point using the first <code>d</code>
	 * principal components.
	 * 
	 * @param input
	 *            a point in high dimensional space
	 * @param numDimensions
	 *            the dimension of the projected point
	 * @return a low dimensional projection of the input point
	 */
	public RealVector project(RealVector input, int numDimensions) {
		RealMatrix linTran = projectionEigs(numDimensions);

		input = preprocess(input);
		return linTran.operate(input);
	}

	private RealVector preprocess(RealVector input) {
		return input.subtract(_mean);
	}

	/**
	 * Returns a list of eigenvalues in order from largest (index 0) to smallest
	 * (index n).
	 * 
	 * @return a list of eigenvalues in order from largest to smallest
	 */
	public RealVector getEigenValues() {
		return _eigenValues;
	}

	/**
	 * Returns a matrix of eigenvectors where each column is an eigenvector. The
	 * eigenvectors are sorted according to decreasing value of their
	 * corresponding eigenvalues.
	 * 
	 * @return a matrix of eigenvectors
	 */
	public RealMatrix getEigenVectors() {
		return _eigenVectors;
	}

	/**
	 * Returns the sample mean of the raw data.
	 * 
	 * @return the sample mean vector
	 */
	public RealVector mean() {
		return _mean;
	}

	/**
	 * Returns the sample standard deviation of the raw data.
	 * 
	 * @return the sample standard deviation vector
	 */
	public RealVector std() {
		return _std;
	}

}