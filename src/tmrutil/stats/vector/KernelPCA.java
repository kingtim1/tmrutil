package tmrutil.stats.vector;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;

import tmrutil.math.KernelFunction;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.RadialBasisKernelFunction;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.ListUtil;
import tmrutil.util.Sort;

/**
 * An implementation of kernel-based Principal Component Analysis. Kernel PCA is
 * a nonlinear extension of the normal PCA algorithm.
 * 
 * @author Timothy A. Mann
 * 
 */
public class KernelPCA
{
	/**
	 * A kernel function.
	 */
	private KernelFunction<double[]> _kf;
	/** A copy of the sample data points. */
	private double[][] _data;
	/** The dot product kernel matrix. */
	private double[][] _K;
	
	/** Sample mean of the data. */
	private double[] _mean;
	
	private double[] _eigenValues;
	private double[][] _eigenVectors;
	
	/**
	 * Constructs an instance of nonlinear PCA with a specific kernel.
	 * @param kf a kernel function
	 * @param data a set of sample data where each row is one sample
	 */
	public KernelPCA(KernelFunction<double[]> kf, double[][] data)
	{
		_kf = kf;
		_data = MatrixOps.deepCopy(data);
		
		_mean = Statistics.columnMean(_data);
		int numSamples = _data.length;
		for(int i=0;i<numSamples;i++){
			_data[i] = VectorOps.subtract(_data[i], _mean);
		}
		
		_K = new double[numSamples][numSamples];
		for(int i=0;i<numSamples;i++){
			for(int j=0;j<numSamples;j++){
				_K[i][j] = _kf.evaluate(_data[i], _data[j]);
			}
		}
		
		EigenDecomposition eigenDecomp = new EigenDecomposition(
				new Array2DRowRealMatrix(_K));
		RealMatrix eigenValues = eigenDecomp.getD();
		// Store the eigenvalues in a vector
		_eigenValues = new double[numSamples];
		for (int i = 0; i < numSamples; i++) {
			_eigenValues[i] = eigenValues.getEntry(i, i);
		}

		// Establish the order of importance based on the size of the
		// eigenvalues
		int[] pcOrder = Sort.sortIndex(_eigenValues, false);
		_eigenValues = Sort.permute(_eigenValues, pcOrder);

		double[][] eigenVectors = eigenDecomp.getV().getData();
		_eigenVectors = new double[numSamples][numSamples];
		for (int j = 0; j < numSamples; j++) {
			double[] column = MatrixOps.getColumn(eigenVectors, pcOrder[j]);
			// System.out.println(j + ". " + VectorOps.toString(column));
			for (int i = 0; i < numSamples; i++) {
				_eigenVectors[i][j] = column[i];
			}
		}
	}
	
	public double[] getEigenValues()
	{
		return _eigenValues;
	}
	
	public double[][] getEigenVectors()
	{
		return _eigenVectors;
	}
	
	public double[] project(double[] x, int numDimensions)
	{
		x = VectorOps.subtract(x, _mean);
		double[] y = new double[numDimensions];
		for(int n=0;n<numDimensions;n++){
			for(int i=0;i<_data.length;i++){
				y[n] += _eigenVectors[i][n] * _kf.evaluate(_data[i],x);
			}
		}
		return y;
	}
	
	public static void main(String[] args){
	
		int numSamples = 100;
		int dimension = 2;
		double[][] data = new double[numSamples][2];
		for(int n=0;n<numSamples;n++){
			data[n][0] = Random.uniform(-1, 1);
			data[n][1] = data[n][0] * data[n][0] + Random.normal(0, 0.2);
		}
		
		//KernelPCA kPCA = new KernelPCA(new PolynomialKernelFunction(4), data);
		KernelPCA kPCA = new KernelPCA(new RadialBasisKernelFunction(2.0), data);
		
		double[][] scores = new double[numSamples][dimension];
		for(int n=0;n<numSamples;n++){
			scores[n] = kPCA.project(data[n], dimension);
		}
		
		tmrutil.graphics.plot2d.Plot2D.linePlot(ListUtil.toList(kPCA.getEigenValues()), java.awt.Color.RED, "Sorted Eigenvalues", "Eigenvalue Index", "Magnitude");
		tmrutil.graphics.plot2d.Plot2D.scatterPlot(scores, java.awt.Color.RED, "kPCA Scores", "X", "Y");
	}
}
