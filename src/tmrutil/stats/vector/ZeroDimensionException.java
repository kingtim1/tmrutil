/*
 * ZeroDimensionException.java
 */

package tmrutil.stats.vector;

import tmrutil.stats.StatisticsException;

/**
 * Thrown to indicate that an encountered sample has zero dimensions.
 */
public class ZeroDimensionException extends StatisticsException
{

	private static final long serialVersionUID = -4732538156885570528L;

	public ZeroDimensionException(String message)
    {
	super(message);
    }
}