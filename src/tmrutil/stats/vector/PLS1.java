package tmrutil.stats.vector;

import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.stats.Statistics;

/**
 * Utility class for performing the partial least squares algorithm for a single
 * column response (i.e. PLS1 algorithm).
 * 
 * @author Timothy A. Mann
 * 
 */
public class PLS1
{
	
	/**
	 * A matrix of input samples. Each row is a single input sample.
	 */
	private double[][] _X;
	/** A vector of responses. */
	private double[] _y;
	
	/** The sample mean of the input data. */
	private double[] _XMean;
	/** The sample mean of the response data. */
	private double _yMean;
	
	/** The number of dimensions in an input vector. */
	private int _inputDimensions;
	/** The number of reduced dimensions to project data to. */
	private int _reducedDimensions;
	
	private double[][] _W;
	/** Loadings matrix for the input data. */
	private double[][] _P;
	/** Score matrix (projections of the data). */
	private double[][] _T;
	private double[] _c;
	
	/**
	 * Constructs an instance of the PLS1 algorithm.
	 * @param X matrix of input samples (each row is an input sample)
	 * @param y vector of responses
	 * @param reducedDimensions the number of dimensions to project the high dimensional data to
	 * @throws IllegalArgumentException if the number of input samples does not match the number of responses
	 */
	public PLS1(double[][] X, double[] y, int reducedDimensions) throws IllegalArgumentException
	{
		if(X.length != y.length){
			throw new IllegalArgumentException("The number of input samples (" + X.length + ") does not equal the number of responses (" + y.length + ").");
		}
		_XMean = Statistics.columnMean(X);
		_yMean = Statistics.mean(y);
		_inputDimensions = X[0].length;
		_reducedDimensions = reducedDimensions;
		
		_X = new double[X.length][];
		_y = new double[y.length];
		for(int i=0;i<X.length;i++){
			_X[i] = VectorOps.subtract(X[i], _XMean);
			_y[i] = y[i] - _yMean;
		}
		
		_W = new double[_inputDimensions][_reducedDimensions];
		_P = new double[_inputDimensions][_reducedDimensions];
		_T = new double[_X.length][_reducedDimensions];
		_c = new double[_reducedDimensions];
		runPLS1();
	}
	
	private void runPLS1()
	{
		double[][] Xj = MatrixOps.deepCopy(_X);
		double[] yj = VectorOps.deepCopy(_y);
		
		for(int j=0;j<_reducedDimensions;j++){
			double[] wj = VectorOps.normalize(MatrixOps.multiply(MatrixOps.transpose(Xj), yj));
			double[] tj = MatrixOps.multiply(Xj, wj);
			_c[j] = VectorOps.dotProduct(tj, yj) / VectorOps.dotProduct(tj, tj);
			double[] pj = VectorOps.divide(MatrixOps.multiply(MatrixOps.transpose(Xj), tj), VectorOps.dotProduct(tj, tj));
			
			// Copy wj and pj into the jth columns of their corresponding matrices
			for(int row=0;row<_inputDimensions;row++){
				_W[row][j] = wj[row];
				_P[row][j] = pj[row];
			}
			// Copy projected point into the score matrix
			for(int n=0;n<_X.length;n++){
				_T[n][j] = tj[n];
			}
			
			// Update Xj
			Xj = MatrixOps.subtract(Xj, VectorOps.outerProduct(tj, pj));
			// Update yj
			yj = VectorOps.subtract(yj, VectorOps.multiply(tj, _c[j]));
		}
	}
	
	public double[] project(double[] x)
	{
		x = VectorOps.subtract(x, _XMean);
		double[] t = new double[_reducedDimensions];
		
		for(int j=0;j<_reducedDimensions;j++){
			t[j] = VectorOps.dotProduct(MatrixOps.getColumn(_W, j), x);
			x = VectorOps.subtract(x, VectorOps.multiply(t[j], MatrixOps.getColumn(_P, j)));
		}
		
		return t;
	}
}
