package tmrutil.stats;

/**
 * Represents a Weibull probability density function. Weibull distributions are
 * often used to model time-to-event probabilities.
 * 
 * @author Timothy A. Mann
 * 
 */
public class WeibullPDF extends PDF
{

	private static final long serialVersionUID = 7199692314513496957L;

	private GammaFunction _gamma;

	/** The shape parameter. */
	private double _k;
	/** The scale parameter. */
	private double _lambda;

	/**
	 * Constructs an instance of the Weibull distribution.
	 * 
	 * @param scale
	 *            the scale parameter
	 * @param shape
	 *            the shape parameter
	 */
	public WeibullPDF(double scale, double shape)
	{
		setScale(scale);
		setShape(shape);

		_gamma = new GammaFunction();
	}

	@Override
	public Double evaluate(Double x) throws IllegalArgumentException
	{
		if (x >= 0) {
			return eventRate(x) * Math.exp(-Math.pow(x / _lambda, _k));
		} else {
			return 0.0;
		}
	}

	/**
	 * Returns the scale parameter of this distribution.
	 * 
	 * @return the scale parameter of this distribution
	 */
	public double getScale()
	{
		return _lambda;
	}

	/**
	 * Sets the scale parameter of this distribution. The scale parameter must
	 * be a positive value.
	 * 
	 * @param lambda
	 *            a positive value
	 */
	public void setScale(double lambda)
	{
		if (lambda <= 0) {
			throw new IllegalArgumentException(
					"The scale parameter lambda must be positive.");
		}
		_lambda = lambda;
	}

	/**
	 * Returns the shape parameter of this distribution.
	 * 
	 * @return the shape parameter of this distribution
	 */
	public double getShape()
	{
		return _k;
	}

	/**
	 * Sets the shape parameter of this distribution. The shape parameter must
	 * be a positive value.
	 * 
	 * @param k
	 *            a positive value
	 */
	public void setShape(double k)
	{
		if (k <= 0) {
			throw new IllegalArgumentException(
					"The shape parameter k must be positive.");
		}
		_k = k;
	}

	@Override
	public double mean()
	{
		return _lambda * _gamma.evaluate(1 + (1 / _k));
	}

	@Override
	public double variance()
	{
		double lambda2 = _lambda * _lambda;
		return lambda2 * _gamma.evaluate(1 + (2 / _k)) - Math.pow(mean(), 2);
	}

	/**
	 * Computes the median of this distribution.
	 * 
	 * @return the median of this distribution
	 */
	public double median()
	{
		return _lambda * Math.pow(Math.log(2), 1 / _k);
	}

	/**
	 * Computes the mode of this distribution provided that the value of the
	 * shape parameter is greater than 1. If the shape parameter is less than
	 * one then the mode is not defined and an exception will be thrown. If the
	 * shape parameter is greater than one, then the mode of this distribution
	 * is returned.
	 * 
	 * @throws IllegalStateException
	 *             if the shape parameter of this distribution is less than 1
	 * @return the mode of this distribution
	 */
	public double mode()
	{
		if (_k <= 1) {
			throw new IllegalStateException(
					"Mode is undefined when shape parameter is less than or equal to 1.");
		} else {
			return _lambda * Math.pow(((_k - 1) / _k), 1 / _k);
		}
	}

	/**
	 * Computes the event rate at time <code>x</code> for this distribution.
	 * 
	 * @param x
	 *            a time
	 * @return the event rate at time <code>x</code>
	 */
	public double eventRate(double x)
	{
		if (x < 0) {
			return 0;
		} else {
			return (_k / _lambda) * Math.pow((x / _lambda), _k - 1);
		}
	}

	@Override
	public Double integrate(Double a, Double b) throws IllegalArgumentException
	{
		if (a > b) {
			throw new IllegalArgumentException(
					"The lower bound of the interval is greater than the upper bound [a="
							+ a + ", b=" + b + "]");
		}
		double lower = 1 - Math.exp(-Math.pow((a / _lambda), _k));
		double upper = 1 - Math.exp(-Math.pow((b / _lambda), _k));
		return upper - lower;
	}
}
