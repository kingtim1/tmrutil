package tmrutil.stats;

/**
 * An incremental estimator for the infemum statistic.
 * 
 * @author Timothy Mann
 * 
 */
public class InfEstimator extends AbstractIncrementalEstimator<Double,Double>
{
	private int _numSamples;
	private double _infSample;

	public InfEstimator()
	{
		_numSamples = 0;
		_infSample = 0;
	}

	@Override
	public void add(Double sample)
	{
		if (sample < _infSample || _numSamples == 0) {
			_infSample = sample;
		}
		_numSamples++;
	}

	@Override
	public Double estimate() throws InsufficientSamplesException
	{
		if (_numSamples > 0) {
			return _infSample;
		} else {
			throw new InsufficientSamplesException(
					"At least 1 sample is required to estimate the infemum.");
		}
	}

	@Override
	public int numSamples()
	{
		return _numSamples;
	}

	@Override
	public int minimumNumSamples()
	{
		return 1;
	}
}
