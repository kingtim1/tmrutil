/*
 * PDF.java
 */

/* Package */
package tmrutil.stats;

import tmrutil.math.RealFunction;

/**
 * Represents a univariate probability density function.
 */
public abstract class PDF extends RealFunction
{
	private static final long serialVersionUID = 6206189482860519334L;



	/**
	 * Creates an instance of a PDF.
	 */
	public PDF() throws IllegalArgumentException
	{
	}
	
	/**
	 * Calculate the mean of this PDF.
	 * @return the mean of this PDF
	 */
	public abstract double mean();
	
	/**
	 * Calculate the variance of this PDF.
	 * @return the variance of this PDF
	 */
	public abstract double variance();
	
	/**
	 * Calculates the standard deviation of this PDF.
	 * @return the standard deviation of this PDF
	 */
	public double std()
	{
		return Math.sqrt(variance());
	}
}