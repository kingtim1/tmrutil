/*
 * GammaFunction.java
 */

/* Package */
package tmrutil.stats;

import tmrutil.math.RealFunction;

/**
 * Implementation the gamma function.
 */
public class GammaFunction extends RealFunction
{
	private static final long serialVersionUID = -1481663584729798542L;
	
	private GammaHelper _f;

	private class GammaHelper extends RealFunction
	{
		private static final long serialVersionUID = 7878626654849597752L;
		
		private double _r;

		public void setR(double r)
		{
			_r = r;
		}

		@Override
		public Double evaluate(Double x)
		{
			return Math.pow(x, _r - 1) * Math.exp(-x);
		}

		GammaHelper(double r)
		{
			_r = r;
		}
	}

	@Override
	public Double evaluate(Double r) throws IllegalArgumentException
	{
		if (r <= 0) {
			throw new IllegalArgumentException("r must be greater than 0");
		}
		_f.setR(r);
		return _f.integrate(0.0, 30.0);
	}

	public GammaFunction()
	{
		_f = new GammaHelper(1);
	}
}