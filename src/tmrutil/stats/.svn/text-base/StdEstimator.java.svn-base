package tmrutil.stats;

/**
 * Incremental estimator for standard deviation.
 * 
 * @author Timothy Mann
 */
public class StdEstimator extends AbstractIncrementalEstimator<Double,Double>
{
	private VarianceEstimator _varianceEstimator;

	public StdEstimator()
	{
		_varianceEstimator = new VarianceEstimator();
	}

	@Override
	public void add(Double sample)
	{
		_varianceEstimator.add(sample);
	}

	@Override
	public Double estimate() throws InsufficientSamplesException
	{
		double variance = _varianceEstimator.estimate();
		return Math.sqrt(variance);
	}

	@Override
	public int numSamples()
	{
		return _varianceEstimator.numSamples();
	}

	@Override
	public int minimumNumSamples()
	{
		return _varianceEstimator.minimumNumSamples();
	}

}
