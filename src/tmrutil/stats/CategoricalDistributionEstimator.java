package tmrutil.stats;

import java.util.Collection;

/**
 * A simple estimator for a categorical distribution where the categories are the integers [0, n-1].
 * @author Timothy A. Mann
 *
 */
public class CategoricalDistributionEstimator implements
		IncrementalEstimator<Integer, double[]> {
			
	private int[] _counts;
	private int _total;
	
	public CategoricalDistributionEstimator(int numCategories){
		_counts = new int[numCategories];
		_total = 0;
	}

	@Override
	public double[] estimate() throws InsufficientSamplesException {
		double[] pdist = new double[_counts.length];
		for(int i=0;i<_counts.length;i++){
			pdist[i] = _counts[i] / (double) _total;
		}
		return pdist;
	}

	@Override
	public void add(Integer sample) {
		_counts[sample]++;
		_total++;
	}

	@Override
	public void add(Collection<Integer> samples) {
		for(Integer s : samples){
			_counts[s]++;
			_total++;
		}
	}

	@Override
	public void add(Integer[] samples) {
		for(Integer s : samples){
			_counts[s]++;
			_total++;
		}
	}

	@Override
	public int numSamples() {
		return _total;
	}

	@Override
	public boolean hasMinimumSamples() {
		return (_total >= minimumNumSamples());
	}

	@Override
	public int minimumNumSamples() {
		return 1;
	}

}
