/*
 * NormalPDF.java
 */

/* Package */
package tmrutil.stats;

import java.io.IOException;
import tmrutil.graphics.plot2d.Histogram;
import tmrutil.graphics.plot2d.Plot2D;

/**
 * Represents a univariate normal probability density function.
 */
public class NormalPDF extends PDF
{
	private static final long serialVersionUID = 8913801239720805333L;

	/** The mean of the distribution. */
	protected double _mean;
	/** The variance of the distribution. */
	protected double _variance;
	/** The standard deviation of the distribution. */
	protected double _std;
	
	/**
	 * Constructs a univariate normal distribution given a mean and variance.
	 * @param mean the mean
	 * @param variance the variance
	 */
	public NormalPDF(double mean, double variance)
	{
		if (_variance < 0) {
			throw new IllegalArgumentException(
					"Variances cannot be negative. Specified variance is : "
							+ variance);
		}
		_mean = mean;
		_variance = variance;
		_std = Math.sqrt(variance);
	}
	
	@Override
	public Double evaluate(Double x)
	{
		double y = (1.0 / (_std * Math.sqrt(2 * Math.PI)))
				* Math.exp(-Math.pow(x - _mean, 2) / (2 * _std * _std));
		return y;
	}
	
	/**
	 * Returns the expected value of this PDF.
	 */
	@Override
	public double mean()
	{
		return _mean;
	}

	/**
	 * Returns the variance of this PDF.
	 */
	@Override
	public double variance()
	{
		return _variance;
	}
	
	public static void main(String[] args)
		throws IOException
	{
		double[] x = new double[500000];
		for(int i=0;i<x.length;i++){
			x[i] = Random.normal();
		}
		Histogram hist = Plot2D.histogram(x, 100, java.awt.Color.RED, "Normal Histogram", "X", "# of values");
		hist.toPS("plot_test.ps");
	}
}