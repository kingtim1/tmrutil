package tmrutil.stats;

/**
 * An incremental estimator for the supremum statistic.
 * 
 * @author Timothy Mann
 * 
 */
public class SupEstimator extends AbstractIncrementalEstimator<Double,Double>
{
	private int _numSamples;
	private double _supSample;

	public SupEstimator()
	{
		_numSamples = 0;
		_supSample = 0;
	}

	@Override
	public void add(Double sample)
	{
		if (sample > _supSample || _numSamples == 0) {
			_supSample = sample;
		}
		_numSamples++;
	}

	@Override
	public Double estimate() throws InsufficientSamplesException
	{
		if (_numSamples > 0) {
			return _supSample;
		} else {
			throw new InsufficientSamplesException(
					"At least 1 sample is required to estimate the supremum.");
		}
	}

	@Override
	public int numSamples()
	{
		return _numSamples;
	}

	@Override
	public int minimumNumSamples()
	{
		return 1;
	}
}
