package tmrutil.stats;

/**
 * Incrementally estimates the variance of a random variable from observed samples.
 * @author Timothy A. Mann
 *
 */
public class VarianceEstimator extends AbstractIncrementalEstimator<Double,Double>
{
	private int _numSamples;
	private double _sumSamples;
	private double _sumSquaredSamples;

	public VarianceEstimator()
	{
		_numSamples = 0;
		_sumSamples = 0;
		_sumSquaredSamples = 0;
	}

	@Override
	public void add(Double sample)
	{
		// Evaluate new values given the sample
		double sumSamples = _sumSamples + sample;
		double sumSquaredSamples = _sumSquaredSamples + (sample * sample);
		int numSamples = _numSamples + 1;
		
		// Only update the sample if none of the new values are NaN or Infinite
		if(!Double.isNaN(sumSamples) && !Double.isNaN(sumSquaredSamples) &&
				!Double.isInfinite(sumSamples) && !Double.isInfinite(sumSquaredSamples)){
			_sumSamples = sumSamples;
			_sumSquaredSamples = sumSquaredSamples;
			_numSamples = numSamples;
		}
	}

	@Override
	public Double estimate() throws InsufficientSamplesException
	{
		double num = (_sumSquaredSamples)
				- ((_sumSamples * _sumSamples) / _numSamples);
		double den = _numSamples - 1;
		if (_numSamples >= minimumNumSamples()) {
			return num / den;
		} else {
			throw new InsufficientSamplesException(
					"Cannot calculate the variance without at least 2 samples.");
		}
	}
	
	public Double mean() throws InsufficientSamplesException
	{
		if(_numSamples == 0){
			throw new InsufficientSamplesException("Cannot calculate mean with zero samples.");
		}
		return _sumSamples / _numSamples;
	}

	@Override
	public int numSamples()
	{
		return _numSamples;
	}
	
	@Override
	public int minimumNumSamples()
	{
		return 2;
	}
}
