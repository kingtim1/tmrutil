package tmrutil.stats;

/**
 * An incremental estimator for the mean statistic.
 * 
 * @author Timothy Mann
 * 
 */
public class MeanEstimator extends AbstractIncrementalEstimator<Double,Double>
{
	private int _numSamples;
	private double _sumSamples;

	public MeanEstimator()
	{
		_numSamples = 0;
		_sumSamples = 0;
	}

	@Override
	public void add(Double sample)
	{
		// Evaluate the new values given a sample
		double sumSamples = _sumSamples + sample;
		int numSamples = _numSamples + 1;
		
		// Only update if the new values are not NaN or Infinite
		if(!Double.isNaN(sumSamples) && !Double.isInfinite(sumSamples)){
			_sumSamples = sumSamples;
			_numSamples = numSamples;
		}
	}

	@Override
	public Double estimate() throws InsufficientSamplesException
	{
		if (_numSamples > 0) {
			return _sumSamples / _numSamples;
		} else {
			throw new InsufficientSamplesException("At least one sample is needed to estimate the mean.");
		}
	}

	@Override
	public int numSamples()
	{
		return _numSamples;
	}

	@Override
	public int minimumNumSamples()
	{
		return 1;
	}

}
