/*
 * ZeroSamplesException.java
 */

package tmrutil.stats;

/**
 * Thrown to indicate that a statistical operation was performed on a data set
 * with when the operation required more samples.
 */
public class InsufficientSamplesException extends StatisticsException
{

	private static final long serialVersionUID = 4654422578800458885L;

	public InsufficientSamplesException(String message)
    {
	super(message);
    }
}