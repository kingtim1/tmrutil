package tmrutil.stats;

/**
 * Estimates a Bernoulli probability distribution from set of samples.
 * 
 * @author Timothy Mann
 * 
 */
public class BernoulliEstimate
{
	private int _successObserve;
	private int _totObserve;

	/**
	 * Constructs a probability estimate with zero observations and estimate
	 * zero.
	 */
	public BernoulliEstimate()
	{
		_totObserve = 0;
		_successObserve = 0;
	}

	/**
	 * Add an observation to this probability estimate.
	 * 
	 * @param success
	 */
	public void add(boolean success)
	{
		_totObserve += 1;
		if (success) {
			_successObserve += 1;
		}
	}

	/**
	 * Returns the estimate of the probability of the specified event.
	 * 
	 * @return the estimate of the probability of the specified event
	 */
	public double prob()
	{
		if (_totObserve == 0) {
			return 0;
		} else {
			return _successObserve / (double) _totObserve;
		}
	}
}
