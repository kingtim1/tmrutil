package tmrutil.math;

/**
 * A function from real vectors to real vectors.
 * 
 * @author Timothy Mann
 * 
 */
public abstract class RealVectorFunction implements Function<double[], double[]>
{
	private static final long serialVersionUID = -5543399894656318459L;
	
	/** The number of dimensions in a valid input vector. */
	private int _inputSize;
	/** The number of dimensions of an output vector. */
	private int _outputSize;

	/**
	 * Constructs a function over real vectors.
	 * 
	 * @param inputSize
	 *            the number of dimensions of a valid input vector
	 * @param outputSize
	 *            the number of dimensions of a valid output vector
	 * @throws IllegalArgumentException
	 *             if <code>inputSize &lt; 1</code> OR
	 *             <code>outputSize &lt; 1</code>
	 */
	public RealVectorFunction(int inputSize, int outputSize)
			throws IllegalArgumentException
	{
		if (inputSize < 1) {
			throw new IllegalArgumentException(
					"The input size of a function of real vectors must be greater than or equal to 1.");
		}
		if (outputSize < 1) {
			throw new IllegalArgumentException(
					"The output size of a function of real vectors must be greater than or equal to 1.");
		}
		_inputSize = inputSize;
		_outputSize = outputSize;
	}

	/**
	 * Evaluates this function at the specified input <code>x</code> and stores
	 * the result in <code>result</code>.
	 * 
	 * @param x
	 *            a vector
	 * @param result
	 *            a vector to store the result of evaluation
	 * @return the result vector
	 * @throws IllegalArgumentException
	 *             if <code>x</code> is invalid (i.e. not in the functions
	 *             domain)
	 */
	public abstract double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException;
	
	@Override
	public double[] evaluate(double[] x) throws IllegalArgumentException
	{
		return evaluate(x, new double[getOutputSize()]);
	}

	/**
	 * Returns the number of dimensions in a valid input vector.
	 * 
	 * @return the number of dimensions in a valid input vector
	 */
	public int getInputSize()
	{
		return _inputSize;
	}

	/**
	 * Returns the number of dimensions in an output vector.
	 * 
	 * @return the number of dimensions in an output vector
	 */
	public int getOutputSize()
	{
		return _outputSize;
	}
}
