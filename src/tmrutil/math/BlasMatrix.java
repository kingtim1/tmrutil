package tmrutil.math;

import java.util.Arrays;
import org.jblas.ComplexDoubleMatrix;
import org.jblas.DoubleMatrix;
import org.jblas.Eigen;
import org.jblas.Solve;
import tmrutil.util.Sort;

/**
 * A matrix implementation based on JBlas {@link DoubleMatrix}.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BlasMatrix extends AbstractMatrix
{
	protected DoubleMatrix _entries;

	public BlasMatrix(int numRows, int numCols)
	{
		super(numRows, numCols);
		_entries = DoubleMatrix.zeros(numRows, numCols);
	}

	public BlasMatrix(double[][] entries, int numRows, int numCols)
	{
		super(numRows, numCols);
		_entries = new DoubleMatrix(entries);
		if (numRows != _entries.rows || numCols != _entries.columns) {
			throw new IllegalArgumentException(
					"The specified 2-dimensional array does not match the specified number of rows and columns.");
		}
	}

	public BlasMatrix(DoubleMatrix mat)
	{
		super(mat.rows, mat.columns);
		_entries = mat;
	}
	
	public BlasMatrix(Matrix other)
	{
		super(other.rows(), other.cols());
		_entries = DoubleMatrix.zeros(other.rows(), other.cols());
		for (int r = 0; r < rows(); r++) {
			for (int c = 0; c < cols(); c++) {
				double val = other.get(r, c);
				_entries.put(r, c, val);
			}
		}
	}
	
	@Override
	public int hashCode()
	{
		return _entries.hashCode();
	}

	@Override
	public Matrix create(double[][] entries, int numRows, int numColumns)
	{
		return new BlasMatrix(entries, numRows, numColumns);
	}

	@Override
	public Matrix zeros(int numRows, int numCols)
	{
		return new BlasMatrix(numRows, numCols);
	}

	@Override
	public void copy(Matrix rhs)
	{
		setDimensions(rhs.rows(), rhs.cols());
		_entries = DoubleMatrix.zeros(rhs.rows(), rhs.cols());
		for (int r = 0; r < rows(); r++) {
			for (int c = 0; c < cols(); c++) {
				double val = rhs.get(r, c);
				_entries.put(r, c, val);
			}
		}
	}

	@Override
	public Matrix add(Matrix rhs) throws DimensionMismatchException
	{
		if (rows() != rhs.rows() || cols() != rhs.cols()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_ADD_ERROR);
		}
		if (rhs instanceof BlasMatrix) {
			BlasMatrix blasRhs = (BlasMatrix) rhs;
			DoubleMatrix sum = _entries.add(blasRhs._entries);
			return new BlasMatrix(sum);
		} else {
			DoubleMatrix sum = DoubleMatrix.zeros(rows(), cols());
			for (int r = 0; r < rows(); r++) {
				for (int c = 0; c < cols(); c++) {
					sum.put(r, c, _entries.get(r, c) + rhs.get(r, c));
				}
			}
			return new BlasMatrix(sum);
		}
	}

	@Override
	public Matrix subtract(Matrix rhs) throws DimensionMismatchException
	{
		if (rows() != rhs.rows() || cols() != rhs.cols()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_SUBTRACT_ERROR);
		}
		if (rhs instanceof BlasMatrix) {
			BlasMatrix blasRhs = (BlasMatrix) rhs;
			DoubleMatrix sub = _entries.sub(blasRhs._entries);
			return new BlasMatrix(sub);
		} else {
			DoubleMatrix sub = DoubleMatrix.zeros(rows(), cols());
			for (int r = 0; r < rows(); r++) {
				for (int c = 0; c < cols(); c++) {
					sub.put(r, c, _entries.get(r, c) - rhs.get(r, c));
				}
			}
			return new BlasMatrix(sub);
		}
	}

	@Override
	public Matrix multiply(double s)
	{
		DoubleMatrix smat = _entries.mul(s);
		return new BlasMatrix(smat);
	}

	@Override
	public Matrix multiply(Matrix rhs) throws DimensionMismatchException
	{
		if (cols() != rhs.rows()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_MULT_ERROR);
		}
		if (rhs instanceof BlasMatrix) {
			BlasMatrix blasRhs = (BlasMatrix) rhs;
			DoubleMatrix mmat = _entries.mmul(blasRhs._entries);
			return new BlasMatrix(mmat);
		} else {
			double[][] entries = new double[rows()][rhs.cols()];
			for (int nr = 0; nr < rows(); nr++) {
				for (int nc = 0; nc < rhs.cols(); nc++) {
					double dp = 0;
					for (int i = 0; i < cols(); i++) {
						dp += get(nr, i) * get(i, nc);
					}
					entries[nr][nc] = dp;
				}
			}
			return new BlasMatrix(entries, rows(), rhs.cols());
		}
	}

	@Override
	public Vector multiply(Vector rhs) throws DimensionMismatchException
	{
		if (cols() != rhs.size()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_VECTOR_MULT_ERROR);
		}
		if (rhs instanceof BlasVector) {
			BlasVector blasRhs = (BlasVector)rhs;
			DoubleMatrix mmat = null;
			if(cols() == blasRhs._components.rows){
				mmat = _entries.mmul(blasRhs._components);
			}else{
				mmat = _entries.mmul(blasRhs._components.transpose());
			}
			return new BlasVector(mmat.toArray());
		} else {
			double[] result = new double[rows()];
			for (int r = 0; r < rows(); r++) {
				double dp = 0;
				for (int c = 0; c < cols(); c++) {
					dp += get(r, c) * rhs.get(c);
				}
				result[r] = dp;
			}
			return new BlasVector(result);
		}
	}

	@Override
	public Matrix componentMultiply(Matrix rhs)
			throws DimensionMismatchException
	{
		if (rows() != rhs.rows() || cols() != rhs.cols()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_COMPWISE_MULT_ERROR);
		}
		if (rhs instanceof BlasMatrix) {
			BlasMatrix blasRhs = (BlasMatrix) rhs;
			DoubleMatrix entries = _entries.mul(blasRhs._entries);
			return new BlasMatrix(entries);
		} else {
			DoubleMatrix entries = DoubleMatrix.zeros(rows(), cols());
			for (int r = 0; r < rows(); r++) {
				for (int c = 0; c < cols(); c++) {
					entries.put(r, c, get(r, c) * rhs.get(r, c));
				}
			}
			return new BlasMatrix(entries);
		}
	}

	@Override
	public Matrix transpose()
	{
		DoubleMatrix tmat = _entries.transpose();
		return new BlasMatrix(tmat);
	}

	@Override
	public double get(int rowIndex, int colIndex)
	{
		return _entries.get(rowIndex, colIndex);
	}

	@Override
	public Vector getRow(int index)
	{
		DoubleMatrix row = _entries.getRow(index);
		return new BlasVector(row.toArray());
	}

	@Override
	public Vector getColumn(int index)
	{
		DoubleMatrix col = _entries.getColumn(index);
		return new BlasVector(col.toArray());
	}

	@Override
	public double[][] toArray()
	{
		return _entries.toArray2();
	}

	@Override
	public Matrix reshape(int numRows, int numCols)
			throws DimensionMismatchException
	{
		if (numRows * numCols != rows() * cols()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_RESHAPE_ERROR);
		}

		DoubleMatrix rmat = _entries.reshape(numRows, numCols);
		return new BlasMatrix(rmat);
	}

	@Override
	public double[] flatArray()
	{
		return _entries.toArray();
	}

	@Override
	public Matrix eigenvectors(int num)
	{
		ComplexDoubleMatrix[] eigMats = Eigen.eigenvectors(_entries);
		DoubleMatrix vecMat = eigMats[0].getReal();
		DoubleMatrix valMat = eigMats[1].getReal();
		
		double[] evals = new double[valMat.rows];
		for(int i=0;i<evals.length;i++){
			evals[i] = vecMat.get(i,0);
		}
		int[] inds = Sort.sortIndex(evals, false);
		int[] vecInds = new int[num];
		System.arraycopy(inds, 0, vecInds, 0, num);
		
		DoubleMatrix evecs = vecMat.getRows(vecInds).transpose();
		
		return new BlasMatrix(evecs);
	}

	@Override
	public Vector eigenvalues(int num)
	{
		ComplexDoubleMatrix emat = Eigen.eigenvalues(_entries);
		double[] evals = new double[emat.rows];
		for(int i=0;i<evals.length;i++){
			evals[i] = emat.get(i,0).real();
		}
		Arrays.sort(evals);
		double[] largestEvals = new double[num];
		for(int i=0;i<num;i++){
			largestEvals[i] = evals[evals.length-1-i];
		}
		return new BlasVector(largestEvals);
	}

	@Override
	public Matrix copy()
	{
		return new BlasMatrix(this);
	}

	@Override
	public Vector flatten()
	{
		return new BlasVector(_entries.toArray());
	}
	
	@Override
	public void set(int row, int col, double value)
	{
		_entries.put(row, col, value);
	}
	
	public Vector solve(Vector b)
	{
		BlasVector bb = null;
		if(b instanceof BlasVector){
			bb = (BlasVector)b;
		}else{
			bb = new BlasVector(b.toArray());
		}
		
		DoubleMatrix s = Solve.solve(_entries, bb._components);
		double[] v = new double[s.length];
		for(int i=0;i<s.length;i++){
			v[i] = s.get(i);
		}
		return new BlasVector(v);
	}
}
