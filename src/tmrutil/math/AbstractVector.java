package tmrutil.math;

/**
 * An abstract implementation of an immutable vector. This class provides
 * default implementations of some of the vector methods.
 * 
 * @author Timothy A. Mann
 * 
 */
public abstract class AbstractVector implements Vector
{

	/** The number of dimensions of this vector. */
	private int _size;

	/**
	 * Constructs an abstract vector with a specified number of dimensions.
	 * 
	 * @param size
	 *            the number of dimensions of the constructed vector
	 */
	public AbstractVector(int size)
	{
		if (size <= 0) {
			throw new IllegalArgumentException(
					"A vector must have a positive number of dimensions.");
		}
		_size = size;
	}
	
	@Override
	public boolean isZero(double val)
	{
		return NumericalUtil.isZero(val);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Vector){
			Vector rhs = (Vector)obj;
			if(size() != rhs.size()){
				return false;
			}
			for(int i=0;i<size();i++){
				if(!isZero(get(i) - rhs.get(i))){
					return false;
				}
			}
			return true;
		}else{
			return false;
		}
	}

	@Override
	public double dotProduct(Vector rhs) throws DimensionMismatchException
	{
		if (size() != rhs.size()) {
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_DOT_PRODUCT_ERROR);
		}
		double sum = 0;
		for (int i = 0; i < size(); i++) {
			sum += get(i) * rhs.get(i);
		}
		return sum;
	}
	
	@Override
	public double magnitudeSqd()
	{
		double sum = 0;
		for(int i=0;i<size();i++){
			sum += Math.pow(get(i),2);
		}
		return sum;
	}

	@Override
	public final double magnitude()
	{
		return Math.sqrt(magnitudeSqd());
	}
	
	@Override
	public final Vector negate()
	{
		return multiply(-1);
	}
	
	@Override
	public double distanceSqd(Vector rhs) throws DimensionMismatchException
	{
		if(size() != rhs.size()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_DISTANCE_ERROR);
		}
		double sum = 0;
		for(int i=0;i<size();i++){
			sum += Math.pow(get(i) - rhs.get(i), 2);
		}
		return sum;
	}

	@Override
	public final double distance(Vector rhs) throws DimensionMismatchException
	{
		return Math.sqrt(distanceSqd(rhs));
	}
	
	@Override
	public Vector add(Vector rhs) throws DimensionMismatchException
	{
		if(size() != rhs.size()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_ADD_ERROR);
		}
		double[] components = new double[size()];
		for(int i=0;i<size();i++){
			components[i] = get(i) + rhs.get(i);
		}
		return create(components);
	}
	
	@Override
	public Vector subtract(Vector rhs) throws DimensionMismatchException
	{
		if(size() != rhs.size()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_SUBTRACT_ERROR);
		}
		double[] components = new double[size()];
		for(int i=0;i<size();i++){
			components[i] = get(i) - rhs.get(i);
		}
		return create(components);
	}
	
	@Override
	public Vector componentMultiply(Vector rhs) throws DimensionMismatchException
	{
		if(size() != rhs.size()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_COMPWISE_MULT_ERROR);
		}
		double[] components = new double[size()];
		for(int i=0;i<size();i++){
			components[i] = get(i) * rhs.get(i);
		}
		return create(components);
	}
	
	@Override
	public Vector multiply(double s)
	{
		double[] components = new double[size()];
		for(int i=0;i<size();i++){
			components[i] = s * get(i);
		}
		return create(components);
	}

	@Override
	public final int size()
	{
		return _size;
	}
}
