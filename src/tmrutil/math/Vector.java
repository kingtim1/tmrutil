package tmrutil.math;

/**
 * Represents a multi-dimensional vector with real valued components.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface Vector
{

	/**
	 * Creates a new row vector given a nonempty array of components.
	 * 
	 * @param components
	 *            a nonempty array of components
	 * @return a row vector whose components are copied from the specified array
	 *         of components
	 */
	public Vector create(double[] components);

	/**
	 * Copies the components of <code>rhs</code> to <code>this</code> and
	 * resizes the vector, if necessary. This is a SIDE AFFECTING method,
	 * meaning that it changes the elements (and possibly the size) of
	 * <code>this</code> vector.
	 * 
	 * @param rhs a vector
	 */
	public void copy(Vector rhs);
	
	/**
	 * Creates a new copy of <code>this</code> matrix.
	 * @return a copy of <code>this</code> matrix
	 */
	public Vector copy();

	/**
	 * Returns the component value at a specified index.
	 * 
	 * @param index
	 *            an integer in the range <code>[0, this.size()]</code>
	 * @return the value of the component at <code>index</code>
	 */
	public double get(int index);

	/**
	 * Used by the vector implementation to decide whether or not a scalar value
	 * is "close enough" to be considered 0.
	 * 
	 * @param val
	 *            a scalar value
	 * @return true if the value is considered 0; otherwise false
	 */
	public boolean isZero(double val);

	/**
	 * Returns the number of components in this vector.
	 * 
	 * @return the number of components in this vector
	 */
	public int size();

	/**
	 * Returns a vector where each component has been multiplied by -1.
	 * 
	 * @return a new vector equal to the negation of <code>this</code> vector
	 */
	public Vector negate();

	/**
	 * Returns the squared Euclidean length of this vector.
	 * 
	 * @return the squared Euclidean length of this vector
	 */
	public double magnitudeSqd();

	/**
	 * Returns the Euclidean length of this vector.
	 * 
	 * @return the Euclidean length of this vector
	 */
	public double magnitude();

	/**
	 * Computes the squared Euclidean distance between <code>this</code> vector
	 * and <code>rhs</code>.
	 * 
	 * @param rhs
	 *            a vector with the same dimension as <code>this</code>
	 * @return the squared Euclidean distance between <code>this</code> vector
	 *         and <code>rhs</code>
	 * @throws DimensionMismatchException
	 *             if the dimension of <code>this</code> does not match
	 *             <code>rhs</code>
	 */
	public double distanceSqd(Vector rhs) throws DimensionMismatchException;

	/**
	 * Computes the Euclidean distance between <code>this</code> vector and
	 * <code>rhs</code>.
	 * 
	 * @param rhs
	 *            a vector with the same dimension as <code>this</code>
	 * @return the Euclidean distance between <code>this</code> vector and
	 *         <code>rhs</code>
	 * @throws DimensionMismatchException
	 *             if the dimension of <code>this</code> does not match
	 *             <code>rhs</code>
	 */
	public double distance(Vector rhs) throws DimensionMismatchException;

	/**
	 * Adds this vector to another and returns the resulting vector.
	 * 
	 * @param rhs
	 *            a vector with the same dimension as <code>this</code>
	 * @return the sum of <code>this</code> and <code>rhs</code>
	 * @throws DimensionMismatchException
	 *             if the dimension of <code>this</code> does not match
	 *             <code>rhs</code>
	 */
	public Vector add(Vector rhs) throws DimensionMismatchException;

	/**
	 * Subtracts <code>rhs</code> from <code>this</code> and returns the
	 * resulting vector.
	 * 
	 * @param rhs
	 *            a vector with the same dimension as <code>this</code>
	 * @return a new vector equal to <code>this - rhs</code>
	 * @throws DimensionMismatchException
	 *             if the dimension of <code>this</code> does not match
	 *             <code>rhs</code>
	 */
	public Vector subtract(Vector rhs) throws DimensionMismatchException;

	/**
	 * Multiplies each component of this vector by a scalar value and returns
	 * the resulting vector.
	 * 
	 * @param s
	 *            a scalar value
	 * @return a new vector equal to <code>s * this</code>
	 */
	public Vector multiply(double s);

	/**
	 * Performs an element-wise multiplication between <code>this</code> and
	 * <code>rhs</code> and returns the resulting vector.
	 * 
	 * @param rhs
	 *            a vector with the same dimension as <code>this</code>
	 * @return a new vector whose elements are equal to the component-wise
	 *         product between <code>this</code> and <code>rhs</code>
	 */
	public Vector componentMultiply(Vector rhs)
			throws DimensionMismatchException;

	/**
	 * Multiplies <code>this</code> vector by matrix <code>rhs</code> and
	 * returns the resulting vector.
	 * 
	 * @param rhs
	 *            a matrix whose number of rows equals the number of components
	 *            of this vector
	 * @return a new vector equal to <code>this * rhs</code>
	 * @throws DimensionMismatchException
	 *             if the number of components of <code>this</code> vector does
	 *             not match the number of rows of <code>rhs</code>
	 */
	public Vector multiply(Matrix rhs) throws DimensionMismatchException;

	/**
	 * Computes the inner product (or dot product) between <code>this</code> and
	 * <code>rhs</code>.
	 * 
	 * @param rhs
	 *            a vector with the same dimension as <code>this</code>
	 * @return the dot product of <code>this</code> and <code>rhs</code>
	 * @throws DimensionMismatchException
	 *             if the dimension of <code>this</code> does not match
	 *             <code>rhs</code>
	 */
	public double dotProduct(Vector rhs) throws DimensionMismatchException;

	/**
	 * Computes the outer product of <code>this</code> vector with
	 * <code>rhs</code>.
	 * 
	 * @param rhs
	 *            a vector
	 * @return the outer product of <code>this</code> and <code>rhs</code>
	 */
	public Matrix outerProduct(Vector rhs);
	
	/**
	 * Inflates this vector into a matrix with a specified number of rows and columns.
	 * @param numRows the number of rows in the new matrix
	 * @param numCols the number of columns in the new matrix
	 * @return a matrix
	 */
	public Matrix inflate(int numRows, int numCols) throws DimensionMismatchException;

	/**
	 * Returns a 1-dimensional array containing the vector elements.
	 * 
	 * @return a 1-dimensional array
	 */
	public double[] toArray();
}
