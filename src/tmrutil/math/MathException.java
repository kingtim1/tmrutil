/*
 * MathException.java
 */

package tmrutil.math;

/**
 * Thrown to indicate an error related to a mathematical function.
 */
public class MathException extends RuntimeException
{
	private static final long serialVersionUID = -3007161664341773408L;

	public MathException(String message)
    {
	super(message);
    }
}