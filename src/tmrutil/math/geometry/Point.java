package tmrutil.math.geometry;

/**
 * Represents a single point in space.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface Point<P extends Point<?>>
{
	/**
	 * Returns the number of dimensions this point is embedded in.
	 * 
	 * @return a nonnegative integer
	 */
	public int getDimensions();

	/**
	 * Returns the coordinates of this point in space.
	 * 
	 * @return an array of scalar values
	 */
	public double[] toArray();

	/**
	 * Calculates the square of the Euclidean distance between this point and a
	 * specified point.
	 * 
	 * @param point
	 *            a point
	 * @return squared distance between this point and another
	 */
	public double distanceSqd(P point);

	/**
	 * Calculates the Euclidean distance between this point and a specified
	 * point.
	 * 
	 * @param point
	 *            a point
	 * @return distance between this point and another
	 */
	public double distance(P point);

	/**
	 * Returns the scalar value at the component indexed by <code>index</code>.
	 * 
	 * @param index
	 *            the index of a component of the point
	 * @return the value of the component indexed by <code>index</code>
	 * @throws IndexOutOfBoundsException
	 *             if <code>index</code> is outside of the points bounds
	 */
	public double get(int index) throws IndexOutOfBoundsException;

	/**
	 * Determines whether this point is equal to another point. Two points are
	 * equal if their dimensions match and components have the same values.
	 * 
	 * @param object
	 *            an object
	 * @return true if the points are equal.
	 */
	public boolean equals(Object object);
}
