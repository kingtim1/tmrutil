/*
 * Line.java
 */

package tmrutil.math.geometry;

/**
 * Represents a line in N-dimensional space and operations that can be performed
 * on a line.
 */
public class Line {
	protected double[] _p0;
	protected double[] _p1;

	public Line(double[] p0, double[] p1) {
		_p0 = p0;
		_p1 = p1;
	}
}