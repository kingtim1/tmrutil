package tmrutil.math.geometry;

/**
 * Represents a point in 3-dimensional space.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Point3D implements Point<Point3D>
{
	private double[] _p;

	/**
	 * Constructs a point in 3D space at the origin.
	 */
	public Point3D()
	{
		this(0, 0, 0);
	}

	/**
	 * Constructs a point in 3D space.
	 * 
	 * @param x
	 *            the first component value
	 * @param y
	 *            the second component value
	 * @param z
	 *            the third component value
	 */
	public Point3D(double x, double y, double z)
	{
		_p = new double[3];
		set(x, y, z);
	}

	/**
	 * Sets the components of this point.
	 * 
	 * @param x
	 *            the first component value
	 * @param y
	 *            the second component value
	 * @param z
	 *            the third component value
	 */
	private void set(double x, double y, double z)
	{
		setX(x);
		setY(y);
		setZ(z);
	}

	/**
	 * Sets the value of the first component.
	 * 
	 * @param x
	 *            a scalar value
	 */
	private void setX(double x)
	{
		_p[0] = x;
	}

	/**
	 * Sets the value of the second component.
	 * 
	 * @param y
	 *            a scalar value
	 */
	private void setY(double y)
	{
		_p[1] = y;
	}

	/**
	 * Sets the value of the third component.
	 * 
	 * @param z
	 *            a scalar value
	 */
	private void setZ(double z)
	{
		_p[2] = z;
	}

	/**
	 * Returns the value of the first component.
	 * 
	 * @return a scalar value
	 */
	public double getX()
	{
		return _p[0];
	}

	/**
	 * Returns the value of the second component.
	 * 
	 * @return a scalar value
	 */
	public double getY()
	{
		return _p[1];
	}

	/**
	 * Returns the value of the third component.
	 * 
	 * @return a scalar value
	 */
	public double getZ()
	{
		return _p[2];
	}

	@Override
	public double distance(Point3D point)
	{
		return Math.sqrt(distanceSqd(point));
	}

	@Override
	public double distanceSqd(Point3D point)
	{
		double x2 = Math.pow(getX() - point.getX(), 2);
		double y2 = Math.pow(getY() - point.getY(), 2);
		double z2 = Math.pow(getZ() - point.getZ(), 2);
		return x2 + y2 + z2;
	}

	@Override
	public double[] toArray()
	{
		return new double[] { getX(), getY(), getZ() };
	}

	@Override
	public int getDimensions()
	{
		return 3;
	}

	@Override
	public double get(int index) throws IndexOutOfBoundsException
	{
		return _p[index];
	}

	@Override
	public boolean equals(Object object)
	{
		if (object instanceof Point3D) {
			Point3D p3d = (Point3D) object;
			return (getX() == p3d.getX() && getY() == p3d.getY() && getZ() == p3d.getZ());
		}
		return false;
	}

}
