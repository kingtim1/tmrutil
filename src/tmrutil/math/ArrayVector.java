package tmrutil.math;

import java.util.Arrays;
import org.jblas.DoubleMatrix;

/**
 * An immutable vector whose underlying data structure is a one-dimensional
 * array.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ArrayVector extends AbstractVector
{
	/**
	 * An array storing the components of this vector.
	 */
	private double[] _components;

	/**
	 * Constructs a vector from an array of real valued components. The array of
	 * components is copied internally and the resulting vector's dimension is
	 * the same as the length of the provided array.
	 * 
	 * @param components
	 *            an array with at least one element
	 */
	public ArrayVector(double[] components)
	{
		this(components, false);
	}

	/**
	 * Constructs a vector from an array of real valued components. The array of
	 * components may or may not be deeply copied and the resulting vector's
	 * dimension is the same as the provided array.
	 * 
	 * @param components
	 *            an array with at least one element
	 * @param noDeepCopy
	 *            if true, a reference to the component array is stored
	 *            internally; otherwise a deep copy is of the component array is
	 *            made
	 */
	protected ArrayVector(double[] components, boolean noDeepCopy)
	{
		super(components.length);
		if (components.length == 0) {
			throw new IllegalArgumentException(
					"Cannot construct a vector from an empty array.");
		}

		if (noDeepCopy) {
			_components = components;
		} else {
			_components = Arrays.copyOf(components, components.length);
		}
	}
	
	@Override
	public Vector create(double[] components)
	{
		return new ArrayVector(components);
	}
	
	@Override
	public boolean isZero(double val)
	{
		return NumericalUtil.isZero(val);
	}

	@Override
	public Vector add(Vector rhs) throws DimensionMismatchException
	{
		if (size() != rhs.size()) {
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_ADD_ERROR);
		}
		double[] sum = new double[size()];
		for (int i = 0; i < sum.length; i++) {
			sum[i] = get(i) + rhs.get(i);
		}
		return new ArrayVector(sum, true);
	}

	@Override
	public Vector componentMultiply(Vector rhs)
			throws DimensionMismatchException
	{
		if (size() != rhs.size()) {
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_COMPWISE_MULT_ERROR);
		}
		double[] result = new double[size()];
		for (int i = 0; i < result.length; i++) {
			result[i] = get(i) * rhs.get(i);
		}
		return new ArrayVector(result, true);
	}

	@Override
	public double get(int index)
	{
		return _components[index];
	}

	@Override
	public Vector multiply(double s)
	{
		double[] result = new double[size()];
		for(int i=0;i<size();i++){
			result[i] = s * get(i);
		}
		return new ArrayVector(result, true);
	}

	@Override
	public Vector multiply(Matrix rhs) throws DimensionMismatchException
	{
		if(size() != rhs.rows()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_MATRIX_MULT_ERROR);
		}
		double[] components = new double[rhs.cols()];
		for(int c=0;c<rhs.cols();c++){
			double dp = 0;
			for(int r=0;r<rhs.rows();r++){
				dp += get(r) * rhs.get(r, c);
			}
			components[c] = dp;
		}
		return new ArrayVector(components, true);
	}

	@Override
	public Matrix outerProduct(Vector rhs)
	{
		double[][] entries = new double[size()][rhs.size()];
		for(int r=0;r<size();r++){
			for(int c=0;c<rhs.size();c++){
				entries[r][c] = get(r) * rhs.get(c);
			}
		}
		return new ArrayMatrix(entries, size(), rhs.size(), true);
	}

	@Override
	public Vector subtract(Vector rhs) throws DimensionMismatchException
	{
		if(size() != rhs.size()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_SUBTRACT_ERROR);
		}
		double[] result = new double[size()];
		for(int i=0;i<size();i++){
			result[i] = get(i) - rhs.get(i);
		}
		return new ArrayVector(result, true);
	}

	@Override
	public double[] toArray()
	{
		return _components;
	}

	@Override
	public void copy(Vector rhs)
	{
		_components = new double[rhs.size()];
		if(rhs instanceof ArrayVector){
			ArrayVector arRhs = (ArrayVector)rhs;
			System.arraycopy(arRhs._components, 0, _components, 0, arRhs.size());
		}else{
			for(int i=0;i<rhs.size();i++){
				_components[i] = rhs.get(i);
			}
		}
	}

	@Override
	public Vector copy()
	{
		return new ArrayVector(_components);
	}

	@Override
	public Matrix inflate(int numRows, int numCols)
			throws DimensionMismatchException
	{
		if((numRows * numCols) != size()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_INFLATE_ERROR);
		}
		double[][] newMat = new double[numRows][numCols];
		int index = 0;
		for(int c=0;c<numCols;c++){
			for(int r=0;r<numRows;r++){
				newMat[r][c] = _components[index];
				index++;
			}
		}
		return new ArrayMatrix(newMat, numRows, numCols, true);
	}

}
