/*
 * MatrixOps.java
 */

/* Package */
package tmrutil.math;

/* Imports */
import tmrutil.graphics.DrawableComponent;
import tmrutil.graphics.LabeledMatrixDisplay;
import tmrutil.graphics.MatrixDisplay;
import tmrutil.graphics.LabeledMatrixDisplay.Normalization;
import tmrutil.util.UnsafeSideAffectException;
import tmrutil.xml.XMLUtil;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * A set of methods for treating two dimensional arrays as matrices. For all of
 * these methods a matrix is assumed to be a 2-dimensional array of doubles or
 * integers. Rows of a matrix are represented by the first dimension of the
 * 2-dimensional array. Columns are represented by the second dimension of the
 * 2-dimensional array.
 */
public final class MatrixOps
{

	public static final String MATRIX_ELM = "MATRIX";

	public static final String ROWS_ATTR = "ROWS";

	public static final String COLS_ATTR = "COLS";

	public static final String ROW_SPLIT = "\\\\";

	public static final String COL_SPLIT = ":";

	/**
	 * Add two matrices with an equal number of rows and columns. The sum of the
	 * two matrices is returned.
	 * 
	 * @param a
	 *            a matrix
	 * @param b
	 *            a matrix
	 * @return the sum of matrices <code>a</code> and <code>b</code>
	 * @throws DimensionMismatchException
	 *             if <code>a</code> does not have the same dimensions as
	 *             <code>b</code>
	 */
	public static final double[][] add(double[][] a, double[][] b)
			throws DimensionMismatchException
	{
		double[][] result = new double[a.length][a[0].length];
		return add(a, b, result);
	}

	/**
	 * Adds matrix a to matrix b and stores the result in the provided result
	 * matrix.
	 * 
	 * @param a
	 *            an M x N matrix
	 * @param b
	 *            an M x N matrix
	 * @param result
	 *            an M x N matrix for storing the results of the addition
	 * @return the result matrix
	 * @throws DimensionMismatchException
	 *             if the dimensions of <code>a</code> do not match the
	 *             dimensions of <code>b</code>
	 */
	public static final double[][] add(double[][] a, double[][] b,
			double[][] result) throws DimensionMismatchException
	{
		try {
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < a[i].length; j++) {
					result[i][j] = a[i][j] + b[i][j];
				}
			}
			return result;
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new DimensionMismatchException(
					"The number of rows and columns must match to perform matrix addition.");
		}
	}

	/**
	 * Subtracts matrix b from matrix a and returns the result.
	 * 
	 * @param a
	 *            an M x N matrix
	 * @param b
	 *            an M x N matrix
	 * @return the result of a - b
	 */
	public static final double[][] subtract(double[][] a, double[][] b)
			throws DimensionMismatchException
	{
		double[][] result = new double[a.length][a[0].length];
		return subtract(a, b, result);
	}

	/**
	 * Subtracts matrix b from matrix a and stores the resulting matrix in the
	 * provided result matrix.
	 * 
	 * @param a
	 *            an M x N matrix
	 * @param b
	 *            an M x N matrix
	 * @param result
	 *            an M x N matrix for storing the results of the addition
	 * @return the result matrix
	 * @throws DimensionMismatchException
	 *             if the dimensions of <code>a</code> do not match the
	 *             dimensions of <code>b</code>
	 */
	public static final double[][] subtract(double[][] a, double[][] b,
			double[][] result) throws DimensionMismatchException
	{
		try {
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < a[i].length; j++) {
					result[i][j] = a[i][j] - b[i][j];
				}
			}
			return result;
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new DimensionMismatchException(
					"The number of rows and columns must match to perform matrix subtraction.");
		}
	}

	/**
	 * Performs matrix multiplication between two matrices <code>a</code> and
	 * <code>b</code>.
	 * 
	 * @param a
	 *            an N x M matrix
	 * @param b
	 *            an M x P matrix
	 * @return the product of <code>a</code> and <code>b</code>
	 * @throws DimensionMismatchException
	 *             if the number of columns in <code>a</code> does not match the
	 *             number of rows in <code>b</code>.
	 */
	public static final double[][] multiply(double[][] a, double[][] b)
			throws DimensionMismatchException
	{
		double[][] result = new double[a.length][b[0].length];
		return multiply(a, b, result);
	}

	/**
	 * Multiplies matrix a and b (i.e. a * b) and stores the result in the
	 * provided result matrix.
	 * 
	 * @param a
	 *            an M x N matrix
	 * @param b
	 *            an N x P matrix
	 * @param result
	 *            an M x P matrix
	 * @return the result matrix (i.e. a * b)
	 * @throws DimensionMismatchException
	 *             if the number of columns in <code>a</code> does not match the
	 *             number of rows in <code>b</code>.
	 */
	public static final double[][] multiply(double[][] a, double[][] b,
			double[][] result) throws DimensionMismatchException
	{
		int rows = getNumRows(a);
		int cols = getNumCols(b);
		if (getNumCols(a) != getNumRows(b)) {
			throw new DimensionMismatchException(
					"Cannot multiply a("
							+ getNumRows(a)
							+ "x"
							+ getNumCols(a)
							+ ") by b("
							+ getNumRows(b)
							+ "x"
							+ getNumCols(b)
							+ ") because the number of columns of a does not match the number of rows of b : "
							+ getNumCols(a) + " != " + getNumRows(b));
		}

		//double[] aRow = new double[getNumCols(a)];
		//double[] bCol = new double[getNumRows(b)];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				result[i][j] = 0;
				for(int k=0;k<getNumRows(b);k++){
					result[i][j] += a[i][k] * b[k][j];
				}
				//result[i][j] = VectorOps.dotProduct(getRow(a, i, aRow),
				//		getColumn(b, j, bCol));
			}
		}
		return result;
	}

	/**
	 * Multiply each element of a matrix by a specified scalar value.
	 * 
	 * @param a
	 *            a matrix
	 * @param scalar
	 *            a scalar value
	 * @return the product of the specified matrix and scalar
	 */
	public static final double[][] multiply(double[][] a, double scalar)
	{
		double[][] result = new double[a.length][a[0].length];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				result[i][j] = a[i][j] * scalar;
			}
		}
		return result;
	}

	/**
	 * Multiplies a row vector with a matrix producing a row vector.
	 * 
	 * @param vector
	 *            a row vector
	 * @param a
	 *            a matrix
	 * @return a row vector
	 * @throws DimensionMismatchException
	 *             if the vector length is not equal to the number of matrix
	 *             rows
	 */
	public static final double[] multiply(double[] vector, double[][] a)
			throws DimensionMismatchException
	{
		double[] result = new double[a[0].length];
		return multiply(vector, a, result);
	}

	/**
	 * Multiplies a row vector with a matrix producing a row vector. The result
	 * of the multiplication is stored in the provided result buffer.
	 * 
	 * @param vector
	 *            a row vector
	 * @param a
	 *            a matrix
	 * @param result
	 *            a row vector to store the result of multiplication
	 * @return DimensionMismatchException if the vector length is not equal to
	 *         the number of matrix rows
	 */
	public static final double[] multiply(double[] vector, double[][] a,
			double[] result)
	{
		if (a.length != vector.length) {
			throw new DimensionMismatchException("Cannot multiply vector(" + 1
					+ "x" + vector.length + ") by matrix a(" + a.length + "x"
					+ a[0].length + ") on the left hand side.");
		}
		if (vector == result) {
			throw new UnsafeSideAffectException(
					"Use of the vector operand as the result vector is not supported.");
		}
		//double[] aCol = new double[getNumRows(a)];
		for (int i = 0; i < a[0].length; i++) {
			result[i] = 0;
			for(int j = 0; j < vector.length; j++){
				result[i] += vector[j] * a[j][i];
			}
			//result[i] = VectorOps.dotProduct(vector, getColumn(a, i, aCol));
		}
		return result;
	}

	/**
	 * Multiply a matrix with a column vector and store and return the result.
	 * 
	 * @param a
	 *            an M x N matrix
	 * @param vector
	 *            an N vector
	 * @return the result of multiplication
	 * @throws DimensionMismatchException
	 *             if the vector length is not equal to the number of matrix
	 *             columns
	 */
	public static final double[] multiply(double[][] a, double[] vector)
			throws DimensionMismatchException
	{
		double[] result = new double[a.length];
		return multiply(a, vector, result);
	}

	/**
	 * Multiply a matrix with a column vector and store the results in the
	 * provided result matrix.
	 * 
	 * @param a
	 *            an M x N matrix
	 * @param vector
	 *            an N vector
	 * @param result
	 *            an M vector
	 * @return the result vector
	 * @throws DimensionMismatchException
	 *             if the vector length is not equal to the number of matrix
	 *             columns
	 */
	public static final double[] multiply(double[][] a, double[] vector,
			double[] result) throws DimensionMismatchException
	{
		if (a[0].length != vector.length) {
			throw new DimensionMismatchException("Cannot multiply matrix a("
					+ a.length + "x" + a[0].length + ") by vector(" + 1 + "x"
					+ vector.length + ") on the right hand side.");
		}
		if (vector == result) {
			throw new UnsafeSideAffectException(
					"Use of the vector operand as the result vector is not supported.");
		}
		//double[] aRow = new double[getNumCols(a)];
		for (int i = 0; i < a.length; i++) {
			result[i] = 0;
			for(int j=0;j<vector.length;j++){
				result[i] += vector[j] * a[i][j];
			}
			//result[i] = VectorOps.dotProduct(getRow(a, i, aRow), vector);
		}
		return result;
	}
	
	public static final double[][] rowsSumToOne(double[][] a)
	{
		double[][] copy = deepCopy(a);
		for(int row=0;row<copy.length;row++){
			copy[row] = VectorOps.divide(copy[row], VectorOps.sum(copy[row]));
		}
		return copy;
	}
	
	/**
	 * Computes the normalization of each row of matrix <code>a</code>.
	 * @param a a matrix
	 * @return a matrix containing the normalized rows of a.
	 */
	public static final double[][] normalizeRows(double[][] a)
	{
		double[][] copy = MatrixOps.deepCopy(a);
		for(int row=0;row<copy.length;row++){
			copy[row] = VectorOps.normalize(copy[row]);
		}
		return copy;
	}

	/**
	 * Returns a deep copy of a specified row in the matrix.
	 * 
	 * @param a
	 *            a matrix
	 * @param r
	 *            the index of the row (starting at 0)
	 * @return a deep copy of a row
	 */
	public static final double[] getRow(double[][] a, int r)
	{
		double[] row = new double[a[r].length];
		return getRow(a, r, row);
	}

	/**
	 * Returns a deep copy of the specified row in the matrix.
	 * 
	 * @param a
	 *            a matrix
	 * @param r
	 *            the index of the row (starting at 0)
	 * @param result
	 *            a vector to store the resulting row values in
	 * @return the result vector containing the row values
	 */
	public static final double[] getRow(double[][] a, int r, double[] result)
	{
		int rowLength = a[r].length;
		for (int i = 0; i < rowLength; i++) {
			result[i] = a[r][i];
		}
		return result;
	}

	/**
	 * Returns a deep copy of a specified column in the matrix.
	 * 
	 * @param a
	 *            a matrix
	 * @param c
	 *            the index of the column (starting at 0)
	 * @return a deep copy of the column
	 */
	public static final double[] getColumn(double[][] a, int c)
	{
		double[] column = new double[a.length];
		return getColumn(a, c, column);
	}

	/**
	 * Returns a deep copy of a specified column in the matrix storing the
	 * elements in result.
	 * 
	 * @param a
	 *            a matrix
	 * @param c
	 *            the index of the column (starting at 0)
	 * @param result
	 *            a vector to store the resulting column values in
	 * @return the result vector containing the column values
	 */
	public static final double[] getColumn(double[][] a, int c, double[] result)
	{
		int colLength = a.length;
		for (int i = 0; i < colLength; i++) {
			result[i] = a[i][c];
		}
		return result;
	}

	/**
	 * Returns the number of rows in a matrix.
	 * 
	 * @param a
	 *            a 2-dimensional array
	 * @return the number of rows in a matrix <code>a</code>
	 */
	public static final int getNumRows(double[][] a)
	{
		return a.length;
	}

	/**
	 * Returns the number of columns in a matrix. This uses a lazy method which
	 * only checks the column in the first row. This might cause problems if
	 * other rows have different numbers of columns.
	 * 
	 * @param a
	 *            a 2-dimensional array
	 * @return the number of columns in a matrix <code>a</code>
	 */
	public static final int getNumCols(double[][] a)
	{
		return getNumCols(a, false);
	}

	/**
	 * Returns the number of columns in a matrix.
	 * 
	 * @param a
	 *            a 2-dimensional array
	 * @param thoroughCheck
	 *            check the length of all columns
	 * @return the number of columns in the matrix <code>a</code>
	 * @throws DimensionMismatchException
	 *             if the matrix <code>a</code> contains columns that differ in
	 *             the number of elements
	 */
	public static final int getNumCols(double[][] a, boolean thoroughCheck)
			throws DimensionMismatchException
	{
		int cols = a[0].length;
		if (thoroughCheck) {
			for (int i = 1; i < a.length; i++) {
				if (a[i].length != cols) {
					throw new DimensionMismatchException(
							"Two dimensional array contains columns with differing numbers of elements.");
				}
			}
		}
		return cols;
	}

	/**
	 * Uses Gauss-Jordan elimination to find the inverse of a non-singular,
	 * square matrix.
	 * 
	 * @param a
	 *            a non-singular, square matrix
	 * @param tolerance
	 *            value used to determine whether or not <code>a</code> is
	 *            singular
	 * @return the inverse of <code>a</code>
	 * @throws SquareMatrixException
	 *             if <code>a</code> is not a square matrix
	 * @throws SingularMatrixException
	 *             if <code>a</code> is a singular matrix
	 */
	public static final double[][] inverse(double[][] a, double tolerance)
			throws SquareMatrixException, SingularMatrixException
	{
		double[][] result = new double[getNumRows(a)][getNumCols(a)];
		return inverse(a, tolerance, result);
	}

	/**
	 * Uses Gauss-Jordan elimination to find the inverse of a non-singular,
	 * square matrix.
	 * 
	 * @param a
	 *            a non-singular, square matrix
	 * @param tolerance
	 *            value used to determine whether or not <code>a</code> is
	 *            singular
	 * @param result
	 *            a matrix to store the inverse in
	 * @return the result matrix with the inverse of <code>a</code>
	 * @throws SquareMatrixException
	 *             if <code>a</code> is not a square matrix
	 * @throws SingularMatrixException
	 *             if <code>a</code> is a singular matrix
	 */
	public static final double[][] inverse(double[][] a, double tolerance,
			double[][] result) throws SquareMatrixException,
			SingularMatrixException
	{
		if (!SquareMatrixException.isSquare(a)) {
			throw new SquareMatrixException(
					"Cannot take the inverse of a non-square matrix");
		}

		int numRows = getNumRows(a);
		int numCols = getNumCols(a);

		double[][] m = new double[numRows][2 * numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				m[r][c] = a[r][c];
				if (r == c) {
					m[r][numCols + c] = 1.0;
				}
			}
		}

		m = gaussJordanElimination(m, tolerance, m);

		// Copy the inverse matrix to result
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				result[r][c] = m[r][numCols + c];
			}
		}

		return result;
	}

	/**
	 * Performs Gauss-Jordan elimination for a matrix <code>a</code> without
	 * modifying <code>a</code>. The result is stored in the provided
	 * <code>result</code> matrix.
	 * 
	 * @param a
	 *            a matrix
	 * @param tolerance
	 *            a value used for determining whether or not <code>a</code> is
	 *            singular
	 * @param result
	 *            a matrix
	 * @return the result of Gauss-Jordan elimination
	 * @throws SingularMatrixException
	 *             if <code>a</code> is singular
	 */
	public static final double[][] gaussJordanElimination(double[][] a,
			double tolerance, double[][] result) throws SingularMatrixException
	{
		int numRows = getNumRows(a);
		int numCols = getNumCols(a);

		// Copy the matrix data from a to result
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				result[r][c] = a[r][c];
			}
		}

		int r = 0;
		for (int c = 0; c < numCols && r < numRows; c++) {
			// Find the pivot row
			int pivot = 0;
			for (int r2 = r; r2 < numRows; r2++) {
				if (Math.abs(result[r2][c]) > Math.abs(result[pivot][c])) {
					pivot = r2;
				}
			}

			// Check if the matrix is singular
			if (Math.abs(result[r][r]) < tolerance) {
				throw new SingularMatrixException(
						"Cannot perform Gauss-Jordan elimination on a singular matrix.");
			}

			// Swap the rows r and pivot
			for (int c2 = c; c2 < numCols; c2++) {
				double tmp = result[pivot][c2];
				result[pivot][c2] = result[r][c2];
				result[r][c2] = tmp;
			}

			// Normalize the pivot row
			double normC = result[r][c];
			for (int c2 = c; c2 < numCols; c2++) {
				result[r][c2] = result[r][c2] / normC;
			}

			// Eliminate the current column
			for (int r2 = 0; r2 < numRows; r2++) {
				if (r2 != r) {
					double m = result[r2][c] / result[r][c];
					for (int c2 = c; c2 < numCols; c2++) {
						result[r2][c2] = result[r2][c2] - (m * result[r][c2]);
					}
				}
			}

			r++;
		}
		return result;
	}

	/**
	 * Creates and returns a deep copy of a specified matrix.
	 * 
	 * @param a
	 *            the matrix to copy
	 * @return a deep copy of the matrix <code>a</code>
	 */
	public static final double[][] deepCopy(double[][] a)
	{
		double[][] b = new double[a.length][];
		for (int i = 0; i < b.length; i++) {
			b[i] = Arrays.copyOf(a[i], a[i].length);
		}
		return b;
	}
	
	/**
	 * Constructs a new matrix with all of the rows of <code>m</code> and an additional row specified by <code>row</code>.
	 * @param m an N x M matrix
	 * @param row a row with M values 
	 * @return a new (N + 1) x M matrix
	 * @throws IllegalArgumentException if the number of values in row does not match the number of columns in the matrix
	 */
	public static final double[][] appendRow(double[][] m, double[] row) throws IllegalArgumentException
	{
		int numRows = getNumRows(m);
		int numCols = getNumCols(m, true);
		if(row.length != numCols){
			throw new IllegalArgumentException("The length of the specified row does not equal the number of matrix columns.");
		}
		double[][] newM = new double[numRows + 1][numCols];
		
		for(int r=0;r<numRows;r++){
			for(int c=0;c<numCols;c++){
				newM[r][c] = m[r][c];
			}
		}
		for(int c=0;c<numCols;c++){
			newM[numRows][c] = row[c];
		}
		
		return newM;
	}

	/**
	 * Returns a square identity matrix with <code>size</code> rows and
	 * <code>size</code> columns.
	 * 
	 * @param size
	 *            the number of rows and the number of columns
	 * @return an identity matrix
	 */
	public static final double[][] identity(int size)
	{
		double[][] identity = new double[size][size];
		for (int i = 0; i < size; i++) {
			identity[i][i] = 1;
		}
		return identity;
	}

	/**
	 * Determines whether any of the components of this matrix have NaN values.
	 * 
	 * @param a
	 *            a matrix
	 * @return true if the matrix contains a NaN value; Otherwise false
	 */
	public static final boolean containsNaN(double[][] a)
	{
		for (int r = 0; r < a.length; r++) {
			if (VectorOps.containsNaN(a[r])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines whether any of the components of this matrix have Infinity or
	 * -Infinity values.
	 * 
	 * @param a
	 *            a matrix
	 * @return true if the matrix contains Infinity or -Infinity; Otherwise
	 *         false
	 */
	public static final boolean containsInfinite(double[][] a)
	{
		for (int r = 0; r < a.length; r++) {
			if (VectorOps.containsInfinite(a[r])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns a randomly generated matrix with <code>rows</code> rows and
	 * <code>cols</code> columns and values in the range [0,1].
	 * 
	 * @param rows
	 *            number of rows
	 * @param cols
	 *            number of columns
	 * @return a randomly generated matrix
	 */
	public static final double[][] random(int rows, int cols)
	{
		double[][] r = new double[rows][cols];
		java.util.Random rand = new java.util.Random();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				r[i][j] = rand.nextDouble();
			}
		}
		return r;
	}

	/**
	 * Returns a randomly generated matrix with <code>rows</code> rows and
	 * <code>cols</code> columns and values in the range [
	 * <code>lowerBound</code>,<code>upperBound</code>].
	 * 
	 * @param rows
	 *            number of rows
	 * @param cols
	 *            number of columns
	 * @return a randomly generated matrix
	 */
	public static final double[][] random(int rows, int cols,
			double lowerBound, double upperBound)
	{
		double[][] r = new double[rows][cols];
		java.util.Random rand = new java.util.Random();

		double diff = upperBound - lowerBound;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				r[i][j] = (rand.nextDouble() * diff) + lowerBound;
			}
		}
		return r;
	}

	/**
	 * Returns a string representation of a provided matrix.
	 * 
	 * @param a
	 *            a matrix
	 * @return a string representation of matrix <code>a</code>
	 */
	public static final String toString(double[][] a)
	{
		StringBuffer buf = new StringBuffer();
		DecimalFormat df = new DecimalFormat("0.00;-0.00");
		for (int i = 0; i < a.length; i++) {
			buf.append("[ ");
			for (int j = 0; j < a[i].length; j++) {
				if (j == a[i].length - 1) {
					buf.append(df.format(a[i][j]));
				} else {
					buf.append(df.format(a[i][j]) + ", ");
				}
			}
			buf.append("]\n");
		}
		return buf.toString();
	}

	/**
	 * Returns the transpose of matrix <code>a</code>.
	 * 
	 * @param a
	 *            a matrix
	 * @return the transpose of <code>a</code>
	 */
	public static final double[][] transpose(double[][] a)
	{
		double[][] result = new double[a[0].length][a.length];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				result[j][i] = a[i][j];
			}
		}
		return result;
	}

	/**
	 * Reshapes the specified vector into a matrix with <code>r</code> rows and
	 * <code>c</code> columns.
	 */
	public static final double[][] reshape(double[] v, int r, int c)
	{
		if (v.length != (r * c)) {
			throw new DimensionMismatchException(
					"The number of vector elements (" + v.length + ") does not match the number required  (" + (r * c) + ") for reshaping.");
		}
		double[][] m = new double[r][c];
		for (int i = 0; i < v.length; i++) {
			m[i / c][i % c] = v[i];
		}
		return m;
	}

	/**
	 * Displays the matrix as a 2D map where high values are light and low
	 * values are dark.
	 */
	public static final MatrixDisplay displayGraphically(double[][] a,
			String title)
	{
		JFrame frame = new JFrame(title);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(400, 400);
		MatrixDisplay disp = new MatrixDisplay(a);

		DrawableComponent dcomp = new DrawableComponent(disp);
		dcomp.addMouseMotionListener(disp);
		frame.add(dcomp);
		frame.setVisible(true);
		return disp;
	}

	/**
	 * Displays the matrix as a 2D map where high values are light and low
	 * values are dark.
	 */
	public static final LabeledMatrixDisplay displayGraphically(double[][] a,
			String title, String[] rowLabels, String[] colLabels)
	{
		JDialog dialog = new JDialog((JDialog) null, title);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		LabeledMatrixDisplay disp = new LabeledMatrixDisplay(rowLabels,
				colLabels, a, Normalization.FULL);
		dialog.add(disp);
		dialog.pack();
		dialog.setVisible(true);
		return disp;
	}

	/**
	 * Displays the matrix as a 2D map where high values are light and low
	 * values are dark.
	 */
	public static final LabeledMatrixDisplay displayGraphically(double[][] a,
			String title, String[] rowLabels, String[] colLabels,
			Normalization normalization)
	{
		JDialog dialog = new JDialog((JDialog) null, title);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		LabeledMatrixDisplay disp = new LabeledMatrixDisplay(rowLabels,
				colLabels, a, normalization);
		dialog.add(disp);
		dialog.pack();
		dialog.setVisible(true);
		return disp;
	}

	/**
	 * Saves the matrix to an XML file.
	 * 
	 * @param fileName
	 *            the name of the file to save the matrix to.
	 * @param matrix
	 *            the matrix to be saved.
	 */
	public static final void save(String fileName, double[][] matrix)
			throws IOException
	{
		StringBuffer xmlString = new StringBuffer(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n");

		xmlString.append("<" + MATRIX_ELM + " ROWS=\"" + matrix.length
				+ "\" COLS=\"" + matrix[0].length + "\">\n");
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[row].length; col++) {
				xmlString.append(matrix[row][col]);
				if (col + 1 == matrix[row].length) {
					xmlString.append(COL_SPLIT);
				} else {
					xmlString.append(ROW_SPLIT + "\n");
				}
			}
		}
		xmlString.append("</" + MATRIX_ELM + ">\n");

		FileWriter fw = new FileWriter(fileName);
		fw.write(xmlString.toString());
		fw.close();
	}

	/**
	 * Loads a matrix from an XML file.
	 * 
	 * @param fileName
	 *            the name of the file to load the matrix from.
	 */
	public static final double[][] load(String fileName) throws IOException,
			SAXException
	{
		Document doc = XMLUtil.getDocumentBuilder().parse(fileName);
		Element docElm = doc.getDocumentElement();
		int rows = Integer.parseInt(docElm.getAttribute(ROWS_ATTR));
		int cols = Integer.parseInt(docElm.getAttribute(COLS_ATTR));
		String[] content = docElm.getTextContent().split(ROW_SPLIT);
		double[][] matrix = new double[rows][cols];
		for (int row = 0; row < rows; row++) {
			String[] components = content[row].split(COL_SPLIT);
			for (int col = 0; col < cols; col++) {
				matrix[row][col] = Double.parseDouble(components[col]);
			}
		}
		return matrix;
	}

	/**
	 * Calculate the determinant of a square matrix. This is a naive and slow
	 * implementation.
	 */
	public static final double det(double[][] a)
	{
		List<Integer> rows = new ArrayList<Integer>(a.length);
		List<Integer> cols = new ArrayList<Integer>(a.length);

		for (int i = 0; i < a.length; i++) {
			rows.add(i);
			cols.add(i);
		}

		return det(a, rows, cols);
	}

	/**
	 * Calculate the determinant of a subregion of a matrix.
	 */
	private static final double det(double[][] a, List<Integer> rows,
			List<Integer> cols)
	{

		// Check whether the specified subregion is square
		if (rows.size() != cols.size()) {
			throw new SquareMatrixException(
					"The determinant can only be calculated for square matrices.");
		}

		double determinant = 0;

		// If a is a 2x2 matrix calculate the determinant outright
		if (rows.size() == 2) {
			determinant = a[rows.get(0)][cols.get(0)]
					* a[rows.get(1)][cols.get(1)] - a[rows.get(0)][cols.get(1)]
					* a[rows.get(1)][cols.get(0)];
			return determinant;
		}
		// Try to compute the determinant recursively
		else {
			List<Integer> nrows = new ArrayList<Integer>(rows);
			nrows.remove(0);
			List<Integer> ncols = new ArrayList<Integer>(cols);

			int paridy = 0;
			for (int j = 0; j < cols.size(); j++) {
				Integer previous = ncols.remove(j);

				double coeff = a[rows.get(0)][cols.get(0)];
				if (coeff != 0 && paridy % 2 != 0) {
					determinant -= coeff * det(a, nrows, ncols);
				} else {
					determinant += coeff * det(a, nrows, ncols);
				}
				ncols.add(j, previous);
				paridy++;
			}

			return determinant;
		}
	}

	private MatrixOps()
	{
	}

	public static void main(String[] args)
	{
		double[][] x = MatrixOps.random(2, 2);
		System.out.println("X = \n" + MatrixOps.toString(x));
		System.out.println("Gauss-Jordan Eliminiation : \n"
				+ MatrixOps.toString(MatrixOps.gaussJordanElimination(x,
						1.0 / Math.pow(10, 10), new double[2][2])));
		double[][] xInv = MatrixOps.inverse(x, 1.0 / Math.pow(10, 10));
		System.out.println("X^{-1} = \n" + MatrixOps.toString(xInv));
		System.out.println("X * X^{-1} = \n"
				+ MatrixOps.toString(MatrixOps.multiply(x, xInv)));
	}

	/**
	 * Allocates and returns a <code>numRows</code> x <code>numCols</code> matrix where every entry is set to one.
	 * @param numRows a number of rows
	 * @param numCols a number of columns
	 * @return a matrix with every element set to one
	 */
	public static double[][] ones(int numRows, int numCols)
	{
		return ones(numRows, numCols, new double[numRows][numCols]);
	}
	
	/**
	 * Assigns ones to every element of <code>result</code> less than <code>numRows</code> and less than <code>numCols</code>.
	 * @param numRows a number of rows
	 * @param numCols a number of columns
	 * @param result a matrix to store the result
	 * @return the modified result matrix
	 */
	public static double[][] ones(int numRows, int numCols, double[][] result)
	{
		for(int r=0;r<numRows;r++){
			for(int c=0;c<numCols;c++){
				result[r][c] = 1.0;
			}
		}
		return result;
	}
}