package tmrutil.math;

import java.util.Arrays;

/**
 * A matrix whose underlying data structure is a two-dimensional
 * array {@link double[]}.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ArrayMatrix extends AbstractMatrix
{
	private double[][] _entries;

	/**
	 * Constructs a zeros matrix with the specified number of rows and columns.
	 * 
	 * @param numRows
	 *            the number of rows
	 * @param numCols
	 *            the number of columns
	 */
	public ArrayMatrix(int numRows, int numCols)
	{
		super(numRows, numCols);
		_entries = new double[numRows][numCols];
	}

	/**
	 * Constructs a matrix from a two-dimensional array. The array of entries is
	 * deeply copied and the resulting matrix's dimension is the same as the
	 * provided array.
	 * 
	 * @param entries
	 *            a two-dimensional array of entries
	 * @param numRows
	 *            the number of rows of the matrix
	 * @param numCols
	 *            the number of columns of the matrix
	 */
	public ArrayMatrix(double[][] entries, int numRows, int numCols)
	{
		this(entries, numRows, numCols, false);
	}

	/**
	 * Constructs a matrix from a two-dimensional array. The array of entries
	 * may or may not be deeply copied and the resulting matrix's dimension is
	 * the same as the provided array.
	 * 
	 * @param entries
	 *            a two-dimensional array of entries
	 * @param numRows
	 *            the number of rows of the matrix
	 * @param numCols
	 *            the number of columns of the matrix
	 * @param noDeepCopy
	 *            if true, a reference to the component array is stored
	 *            internally; otherwise a deep copy is of the component array is
	 *            made
	 */
	protected ArrayMatrix(double[][] entries, int numRows, int numCols,
			boolean noDeepCopy)
	{
		super(numRows, numCols);
		_entries = new double[numRows][numCols];
		for (int r = 0; r < rows(); r++) {
			System.arraycopy(entries[r], 0, _entries[r], 0, cols());
		}
	}
	
	@Override
	public int hashCode()
	{
		return Arrays.hashCode(_entries);
	}

	@Override
	public boolean isZero(double val)
	{
		return NumericalUtil.isZero(val);
	}

	@Override
	public void copy(Matrix rhs)
	{
		setDimensions(rhs.rows(), rhs.cols());
		_entries = new double[rhs.rows()][rhs.cols()];
		if (rhs instanceof ArrayMatrix) {
			ArrayMatrix arRhs = (ArrayMatrix) rhs;
			for (int r = 0; r < rhs.rows(); r++) {
				System.arraycopy(arRhs._entries[r], 0, _entries[r], 0,
						arRhs.cols());
			}
		} else {
			for (int r = 0; r < rhs.rows(); r++) {
				for (int c = 0; c < rhs.cols(); c++) {
					_entries[r][c] = rhs.get(r, c);
				}
			}
		}
	}

	@Override
	public Matrix add(Matrix rhs) throws DimensionMismatchException
	{
		if (rows() != rhs.rows() || cols() != rhs.cols()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_ADD_ERROR);
		}
		double[][] entries = new double[rows()][cols()];
		for (int r = 0; r < rows(); r++) {
			for (int c = 0; c < cols(); c++) {
				entries[r][c] = get(r, c) + rhs.get(r, c);
			}
		}
		return new ArrayMatrix(entries, rows(), cols(), true);
	}

	@Override
	public Matrix componentMultiply(Matrix rhs)
			throws DimensionMismatchException
	{
		if (rows() != rhs.rows() || cols() != rhs.cols()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_COMPWISE_MULT_ERROR);
		}
		double[][] entries = new double[rows()][cols()];
		for (int r = 0; r < rows(); r++) {
			for (int c = 0; c < cols(); c++) {
				entries[r][c] = get(r, c) * rhs.get(r, c);
			}
		}
		return new ArrayMatrix(entries, rows(), cols(), true);
	}

	@Override
	public Matrix create(double[][] entries, int numRows, int numColumns)
	{
		return new ArrayMatrix(entries, numRows, numColumns);
	}
	
	@Override
	public Matrix zeros(int numRows, int numCols)
	{
		return new ArrayMatrix(numRows, numCols);
	}

	@Override
	public double get(int rowIndex, int colIndex)
	{
		return _entries[rowIndex][colIndex];
	}

	@Override
	public Vector getColumn(int index)
	{
		double[] column = new double[rows()];
		for (int r = 0; r < rows(); r++) {
			column[r] = get(r, index);
		}
		return new ArrayVector(column, true);
	}

	@Override
	public Vector getRow(int index)
	{
		return new ArrayVector(_entries[index], true);
	}

	@Override
	public Matrix multiply(double s)
	{
		double[][] entries = new double[rows()][cols()];
		for (int r = 0; r < rows(); r++) {
			for (int c = 0; c < cols(); c++) {
				entries[r][c] = s * get(r, c);
			}
		}
		return new ArrayMatrix(entries, rows(), cols(), true);
	}

	@Override
	public Matrix multiply(Matrix rhs) throws DimensionMismatchException
	{
		if (cols() != rhs.rows()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_MULT_ERROR);
		}
		double[][] entries = new double[rows()][rhs.cols()];
		for (int nr = 0; nr < rows(); nr++) {
			for (int nc = 0; nc < rhs.cols(); nc++) {
				double dp = 0;
				for (int i = 0; i < cols(); i++) {
					dp += get(nr, i) * get(i, nc);
				}
				entries[nr][nc] = dp;
			}
		}
		return new ArrayMatrix(entries, rows(), rhs.cols(), true);
	}

	@Override
	public Vector multiply(Vector rhs) throws DimensionMismatchException
	{
		if (cols() != rhs.size()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_VECTOR_MULT_ERROR);
		}
		double[] result = new double[rows()];
		for (int r = 0; r < rows(); r++) {
			double dp = 0;
			for (int c = 0; c < cols(); c++) {
				dp += get(r, c) * rhs.get(c);
			}
			result[r] = dp;
		}
		return new ArrayVector(result, true);
	}

	@Override
	public Matrix subtract(Matrix rhs) throws DimensionMismatchException
	{
		if (rows() != rhs.rows() || cols() != rhs.cols()) {
			throw new DimensionMismatchException(
					DimensionMismatchException.MATRIX_SUBTRACT_ERROR);
		}
		double[][] entries = new double[rows()][cols()];
		for (int r = 0; r < rows(); r++) {
			for (int c = 0; c < cols(); c++) {
				entries[r][c] = get(r, c) - rhs.get(r, c);
			}
		}
		return new ArrayMatrix(entries, rows(), cols(), true);
	}

	@Override
	public Matrix transpose()
	{
		double[][] entries = new double[cols()][rows()];
		for (int r = 0; r < rows(); r++) {
			for (int c = 0; c < cols(); c++) {
				entries[c][r] = get(r, c);
			}
		}
		return new ArrayMatrix(entries, cols(), rows(), true);
	}

	@Override
	public double[][] toArray()
	{
		return _entries;
	}
	
	@Override
	public double[] flatArray()
	{
		double[] flat = new double[rows() * cols()];
		int index = 0;
		for(int r=0;r<rows();r++){
			for(int c=0;c<cols();c++){
				flat[index] = _entries[r][c];
				index++;
			}
		}
		return flat;
	}

	@Override
	public Matrix reshape(int numRows, int numCols)
			throws DimensionMismatchException
	{
		if(numRows * numCols != rows() * cols()){
			throw new DimensionMismatchException(DimensionMismatchException.MATRIX_RESHAPE_ERROR);
		}
		double[][] rmat = MatrixOps.reshape(flatArray(), numRows, numCols);
		return new ArrayMatrix(rmat, numRows, numCols);
	}

	@Override
	public Matrix eigenvectors(int num)
	{
		BlasMatrix bmat = new BlasMatrix(toArray(), rows(), cols());
		return bmat.eigenvectors(num);
	}

	@Override
	public Vector eigenvalues(int num)
	{
		BlasMatrix bmat = new BlasMatrix(toArray(), rows(), cols());
		return bmat.eigenvalues(num);
	}

	@Override
	public Matrix copy()
	{
		Matrix newMat = new ArrayMatrix(rows(), cols());
		newMat.copy(this);
		return newMat;
	}

	@Override
	public Vector flatten()
	{
		double[] comps = new double[rows() * cols()];
		int index = 0;
		for(int c=0; c<cols(); c++){
			for(int r=0;r<rows();r++){
				comps[index] = get(r,c);
				index++;
			}
		}
		return new ArrayVector(comps, true);
	}
	
	@Override
	public void set(int row, int col, double value)
	{
		_entries[row][col] = value;
	}

}
