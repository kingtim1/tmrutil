/*
 * Function.java
 */

/* Package */
package tmrutil.math;

/* Imports */

/**
 * Represents a smooth function from a subset of the real numbers to a subset of
 * the real numbers.
 */
public abstract class RealFunction implements Function<Double, Double>
{
	public static final double EPSILON = 0.0001;

	/**
	 * Constructs an instance using the default constructor method of the
	 * specified function type.
	 * 
	 * @param <F>
	 *            the type of function
	 * @param functionClass
	 *            the class of the function
	 * @return an instance of the specified function type
	 * @throws InstantiationException
	 *             to indicate that an error occurred while creating an instance
	 *             of <code>F</code>
	 * @throws IllegalAccessException
	 *             to indicate the the default constructor method does is either
	 *             private or not accessable for some other reason
	 */
	public static final <F extends RealFunction> F makeFunction(
			Class<? extends F> functionClass) throws InstantiationException, IllegalAccessException
	{
		return functionClass.newInstance();
	}

	/**
	 * Differentiates this function at the point <code>x</code>.
	 */
	public Double differentiate(Double x)
	{
		double deltaX = EPSILON;
		double d = 0, dnext = 0;
		do {
			d = (evaluate(x + deltaX) - evaluate(x - deltaX)) / (2 * deltaX);
			deltaX = deltaX / 2;
			dnext = (evaluate(x + deltaX) - evaluate(x - deltaX))
					/ (2 * deltaX);
		} while (d > (dnext + deltaX));
		return d;
	}

	/**
	 * Integrates this function over the interval [a,b] using a trapezoidal
	 * approximation.
	 * 
	 * @param a
	 *            the lower bound on the interval
	 * @param b
	 *            the upper bound on the interval
	 * @throws IllegalArgumentException
	 *             if <code>a &gt; b</code>
	 */
	public Double integrate(Double a, Double b) throws IllegalArgumentException
	{
		double deltaX = EPSILON;
		int n = (int) Math.round((b - a) / deltaX);
		if (n < 0) {
			throw new IllegalArgumentException(
					"Second argument must be larger than the first argument in order to integrate over an interval [a,b].");
		}
		double sum = 0;
		for (int i = 1; i < n; i++) {
			double x1 = a + (deltaX * (i - 1));
			double x2 = a + (deltaX * i);
			double tpA = trapezoidArea(x1, evaluate(x1), x2, evaluate(x2));
			sum = sum + tpA;
		}
		return sum;
	}

	/**
	 * Integrates this function over the interval [a,b] using Monte Carlo
	 * approximation.
	 * 
	 * @param a
	 *            the lower bound on the interval
	 * @param b
	 *            the upper bound on the interval
	 * @param n
	 *            the number of samples to take for the approximation
	 * @throws IllegalArgumentException
	 *             if <code>a &gt; b</code>
	 */
	public Double integrateMC(Double a, Double b, int n)
			throws IllegalArgumentException
	{
		if (a > b) {
			throw new IllegalArgumentException(
					"Second argument must be larger than the first argument in order to integrate over an interval [a,b].");
		}
		java.util.Random rand = new java.util.Random();
		double diff = b - a;
		double sum = 0;
		double[] x = new double[n];
		for (int i = 0; i < n; i++) {
			x[i] = rand.nextDouble() * diff + a;
		}
		int[] ind = tmrutil.util.Sort.sortIndex(x, true);
		for (int i = 1; i < ind.length; i++) {
			sum = sum
					+ trapezoidArea(x[ind[i - 1]], evaluate(x[ind[i - 1]]),
							x[ind[i]], evaluate(x[ind[i]]));
		}
		return sum;
	}

	private double trapezoidArea(double x1, double y1, double x2, double y2)
	{
		double minX, minY, maxX, maxY;
		if (x1 < x2) {
			minX = x1;
			maxX = x2;
		} else {
			minX = x2;
			maxX = x1;
		}
		if (y1 < y2) {
			minY = y1;
			maxY = y2;
		} else {
			minY = y2;
			maxY = y1;
		}
		double xDiff = maxX - minX;
		double yDiff = maxY - minY;

		double rectArea = xDiff * minY;
		double triArea = (xDiff * yDiff) / 2;
		return rectArea + triArea;
	}
}