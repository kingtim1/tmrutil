package tmrutil.math;

/**
 * An abstract implementation of an immutable matrix. This class provides
 * default implementations for a number of matrix methods.
 * 
 * @author Timothy A. Mann
 * 
 */
public abstract class AbstractMatrix implements Matrix
{
	/** The number of rows of this matrix. */
	private int _nRows;
	/** The number of columns of this matrix. */
	private int _nCols;

	/**
	 * Constructs an abstract matrix with a specified number of rows and columns.
	 * @param numRows the number of matrix rows
	 * @param numCols the number of matrix columns
	 */
	public AbstractMatrix(int numRows, int numCols)
	{
		if (numRows <= 0 || numCols <= 0) {
			throw new IllegalArgumentException(
					"Matrices must have positive number of rows and columns.");
		}
		_nRows = numRows;
		_nCols = numCols;
	}
	
	@Override
	public boolean isZero(double val)
	{
		return NumericalUtil.isZero(val);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Matrix){
			Matrix rhs = (Matrix)obj;
			if(rows() != rhs.rows() || cols() != rhs.cols()){
				return false;
			}
			for(int r=0;r<rows();r++){
				for(int c=0;c<cols();c++){
					if(!isZero(get(r,c) - rhs.get(r,c))){
						return false;
					}
				}
			}
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public final boolean isSquare()
	{
		return rows() == cols();
	}
	
	/**
	 * Sets (or resets) the dimensions of this matrix.
	 * @param numRows the number of rows of this matrix
	 * @param numCols the number of columns of this matrix
	 */
	protected void setDimensions(int numRows, int numCols)
	{
		_nRows = numRows;
		_nCols = numCols;
	}

	@Override
	public final int cols()
	{
		return _nCols;
	}

	@Override
	public final int rows()
	{
		return _nRows;
	}

	@Override
	public final Matrix negate()
	{
		return multiply(-1);
	}

	@Override
	public void addToThis(Matrix rhs) throws DimensionMismatchException
	{
		Matrix sum = add(rhs);
		copy(sum);
	}

	@Override
	public void subtractFromThis(Matrix rhs) throws DimensionMismatchException
	{
		Matrix sub = subtract(rhs);
		copy(sub);
	}

	@Override
	public void multiplyThisBy(double s)
	{
		Matrix mul = multiply(s);
		copy(mul);
	}

	@Override
	public void multiplyThisBy(Matrix rhs) throws DimensionMismatchException
	{
		Matrix mul = multiply(rhs);
		copy(mul);
	}

	@Override
	public void componentMultiplyThisBy(Matrix rhs)
			throws DimensionMismatchException
	{
		Matrix mul = componentMultiply(rhs);
		copy(mul);
	}

	@Override
	public void transposeThis()
	{
		Matrix trans = transpose();
		copy(trans);
	}

	@Override
	public void negateThis()
	{
		Matrix neg = negate();
		copy(neg);
	}

	@Override
	public Matrix concatRows(Matrix bottom) throws DimensionMismatchException
	{
		if(cols() != bottom.cols()){
			throw new DimensionMismatchException(DimensionMismatchException.MATRIX_ROW_CONCAT_ERROR);
		}
		Matrix m = zeros(rows() + bottom.rows(), cols());
		for(int r=0;r<m.rows();r++){
			for(int c=0;c<cols();c++){
				if(r < rows()){
					m.set(r,c, get(r,c));
				}else{
					m.set(r,c, bottom.get(r-rows(), c));
				}
			}
		}
		return m;
	}

	@Override
	public Matrix concatCols(Matrix right) throws DimensionMismatchException
	{
		if(rows() != right.rows()){
			throw new DimensionMismatchException(DimensionMismatchException.MATRIX_COL_CONCAT_ERROR);
		}
		Matrix m = zeros(rows(), cols() + right.cols());
		for(int r=0;r<rows();r++){
			for(int c=0;c<m.cols();c++){
				if(c < cols()){
					m.set(r,c, get(r,c));
				}else{
					m.set(r,c, right.get(r, c-cols()));
				}
			}
		}
		return m;
	}
	
	
	
}
