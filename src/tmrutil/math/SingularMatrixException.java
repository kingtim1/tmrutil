/*
 * SingularMatrixException.java
 */

package tmrutil.math;

/**
 * Thrown to indicate that a singular (or near singular) matrix was passed
 * to an operation which assumes a nonsingular matrix.
 */
public class SingularMatrixException extends MatrixException
{
	private static final long serialVersionUID = -17895068696212908L;

	public SingularMatrixException(String message)
    {
	super(message);
    }
}