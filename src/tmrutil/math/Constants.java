package tmrutil.math;

/**
 * A collection of useful mathematical constants.
 * @author Timothy A. Mann
 *
 */
public interface Constants
{
	/** A small positive number. */
	public static final double EPSILON = Math.pow(10, -5);
	
	/** Euler's constant. */
	public static final double E = Math.E;
	/** The constant value Pi. */
	public static final double PI = Math.PI;
}
