package tmrutil.math.functions;

import java.util.Arrays;
import tmrutil.math.Function;

/**
 * A Gaussian-like function.
 * @author Timothy A. Mann
 *
 */
public class Gaussian implements Function<double[], Double>
{
	private static final long serialVersionUID = 3227152375722286484L;
	
	/** The mean of the Gaussian function. */
	private double[] _mean;
	/** The standard deviation of the Gaussian function. */
	private double _std;
	/** The variance of the Gaussian function. */
	private double _var;
	
	/** The expected input vector dimensions. */
	private int _inputSize;
	
	/**
	 * Constructs a Gaussian function with a specified mean and standard deviation.
	 * @param mean the mean of the function
	 * @param std the standard deviation of the function (must be a positive value)
	 */
	public Gaussian(double[] mean, double std)
	{
		setMean(mean);
		setStandardDeviation(std);
	}
	
	@Override
	public Double evaluate(double[] x) throws IllegalArgumentException
	{
		double d2 = 0;
		for(int i=0;i<x.length;i++){
			double diff = x[i] - _mean[i];
			d2 += diff * diff;
		}
		return Math.exp(-d2/_var);
	}
	
	/**
	 * Returns a deep copy of the mean of this Gaussian function.
	 * @return a vector
	 */
	public double[] getMean()
	{
		return Arrays.copyOf(_mean, _mean.length);
	}
	
	/**
	 * Sets the mean of this Gaussian function.
	 * @param mean a vector
	 */
	public void setMean(double[] mean)
	{
		if(mean.length < 1){
			throw new IllegalArgumentException("The mean must contain at least one dimension.");
		}
		_inputSize = mean.length;
		_mean = Arrays.copyOf(mean, _inputSize);
	}
	
	/**
	 * Returns the standard deviation of this Gaussian function. 
	 * @return a positive scalar value representing a standard deviation
	 */
	public double getStandardDeviation()
	{
		return _std;
	}
	
	/**
	 * Returns the variance of this Gaussian function.
	 * @return a positive scalar value representing a variance
	 */
	public double getVariance()
	{
		return _var;
	}
	
	/**
	 * Sets the standard deviation of this Gaussian function.
	 * @param std a positive scalar value
	 */
	public void setStandardDeviation(double std)
	{
		if(std <= 0){
			throw new IllegalArgumentException("The standard deviation must be a positive value.");
		}
		_std = std;
		_var = std * std;
	}

}
