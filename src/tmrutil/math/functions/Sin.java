package tmrutil.math.functions;

import tmrutil.math.RealFunction;

/**
 * Implements the mathematical sine function.
 * @author Timothy Mann
 *
 */
public class Sin extends RealFunction
{

	@Override
	public Double evaluate(Double x)
	{
		return Math.sin(x);
	}
	
	@Override
	public Double differentiate(Double x)
	{
		return Math.cos(x);
	}

}
