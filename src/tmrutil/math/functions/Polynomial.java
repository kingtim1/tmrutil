package tmrutil.math.functions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import tmrutil.math.Function;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.Sort;
import tmrutil.util.SyntaxException;
import tmrutil.util.Tokenizer;

/**
 * A polynomial is a function of one or more variables which uses only addition,
 * subtraction, multiplication, and non-negative positive integer exponents.
 * Given values for each variable the output is a single scalar value.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Polynomial implements Function<double[], Double>
{
	/**
	 * Represents a single term in a polynomial function.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class Term implements Function<double[], Double>
	{
		public static final String SCALAR_REGEX = "-?(\\d*\\.?\\d+|\\d+\\.\\d*|\\d+)";
		public static final String INTEGER_REGEX = "\\d+";

		/** The coefficient of the term. */
		private double _coeff;
		/** The indices of the variables used in this term. */
		private int[] _vars;
		/** The positive exponents of the variables used in this term. */
		private int[] _exps;

		/**
		 * Constructs a term object.
		 * 
		 * @param coefficient
		 *            the value that the term is scaled by
		 * @param varIndices
		 *            the indices of the variables that the term includes
		 * @param varPowers
		 *            the power that each of the variables specified in
		 *            <code>varIndices</code> is raised to in this term
		 */
		public Term(double coefficient, int[] varIndices, int[] varPowers)
		{
			if (varIndices.length != varPowers.length) {
				throw new IllegalArgumentException(
						"The number of variable indices specified must match the number of power arguments specified.");
			}
			Set<Integer> usedVars = new HashSet<Integer>();
			for (int i = 0; i < varIndices.length; i++) {
				if (usedVars.contains(varIndices[i])) {
					throw new IllegalArgumentException(
							"Detected an attempt to use the same variable twice in one term. A variable index can only be used once in a single term.");
				}
				usedVars.add(varIndices[i]);
				if (varIndices[i] < 0) {
					throw new IllegalArgumentException(
							"Variable indices must all be non-negative.");
				}
				if (varPowers[i] < 1) {
					throw new IllegalArgumentException(
							"Exponents for specified variables must be positive.");
				}
			}
			_coeff = coefficient;
			int[] sortedInds = Sort.sortIndex(varIndices, true);
			_vars = Sort.permute(varIndices, sortedInds);
			_exps = Sort.permute(varPowers, sortedInds);
		}

		/**
		 * Copy constructor for term objects.
		 * 
		 * @param scale
		 *            a value to scale the term's coefficient by
		 * @param term
		 *            a term
		 */
		public Term(double scale, Term term)
		{
			this(scale * term._coeff, term._vars, term._exps);
		}

		/**
		 * Copy constructor for term objects.
		 * 
		 * @param term
		 *            a term
		 */
		public Term(Term term)
		{
			this(term._coeff, term._vars, term._exps);
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException
		{
			double product = 1;
			for (int i = 0; i < _vars.length; i++) {
				product = product * Math.pow(x[_vars[i]], _exps[i]);
			}
			return _coeff * product;
		}

		/**
		 * Combines this term with another "like term" to construct a new term.
		 * 
		 * @param term
		 *            a term
		 * @return the combination of this term and the provided term
		 * @throws IllegalArgumentException
		 *             if the passed term is not a "like term" with this term
		 *             object
		 */
		public Term combineLikeTerm(Term term) throws IllegalArgumentException
		{
			if (isLikeTerm(term)) {
				double coeff = _coeff + term._coeff;
				Term newTerm = new Term(coeff, _vars, _exps);
				return newTerm;
			} else {
				throw new IllegalArgumentException(
						"Cannot combine unlike terms.");
			}
		}

		/**
		 * Determines if this term and another term are "like terms". Two terms
		 * are considered to be "like terms" if both operate on the same
		 * variables and each variable is taken to the same corresponding power.
		 * 
		 * @param term
		 *            another polynomial term
		 * @return true if the terms are like; otherwise false
		 */
		public boolean isLikeTerm(Term term)
		{
			if (_vars.length == term._vars.length) {
				boolean sameVars = true;
				boolean samePows = true;
				for (int i = 0; i < _vars.length; i++) {
					if (_vars[i] != term._vars[i]) {
						sameVars = false;
					}
					if (_exps[i] != term._exps[i]) {
						samePows = false;
					}
				}
				if (sameVars && samePows) {
					return true;
				}
			}
			return false;
		}

		/**
		 * The maximum indexed variable used in this term.
		 * 
		 * @return the maximum indexed variable in this term
		 */
		public int maxIndexVar()
		{
			if (_vars.length == 0) {
				return 0;
			} else {
				return Statistics.max(_vars);
			}
		}

		@Override
		public String toString()
		{
			StringBuffer buff = new StringBuffer(String.valueOf(_coeff));
			for (int i = 0; i < _vars.length; i++) {
				if (_exps[i] == 1) {
					buff.append("X[" + _vars[i] + "]");
				} else {
					buff.append("X[" + _vars[i] + "]^{" + _exps[i] + "}");
				}
			}
			return buff.toString();
		}
	}

	private int _inputSize;
	private List<Term> _terms;

	/**
	 * Constructs a polynomial function.
	 * 
	 * @param numVars
	 *            the number of variables used by the polynomial
	 * @param coefficients
	 *            the coefficients for each term of the polynomial
	 * @param termVarIndices
	 *            a list of arrays of indices specifying the variables used in
	 *            each term
	 * @param termVarPowers
	 *            a list of arrays of powers specifying the powers to raise each
	 *            variable used in each term
	 */
	public Polynomial(int numVars, double[] coefficients,
			List<int[]> termVarIndices, List<int[]> termVarPowers)
	{
		_inputSize = numVars;
		_terms = new ArrayList<Term>();
		if (coefficients.length != termVarIndices.size()) {
			throw new IllegalArgumentException(
					"The number of coefficients does not match the number of term variable index arrays.");
		}
		if (termVarIndices.size() != termVarPowers.size()) {
			throw new IllegalArgumentException(
					"The number of term variable index arrays and term variable power arrays do not match.");
		}

		for (int i = 0; i < coefficients.length; i++) {
			Term term = new Term(coefficients[i], termVarIndices.get(i),
					termVarPowers.get(i));
			_terms.add(term);
		}
	}

	/**
	 * Constructs a polynomial from a list of terms.
	 * 
	 * @param numVars
	 *            the number of variables
	 * @param terms
	 *            a list of terms
	 */
	public Polynomial(int numVars, List<Term> terms)
	{
		_inputSize = numVars;
		_terms = new ArrayList<Term>(terms.size());
		for (Term term : terms) {
			_terms.add(new Term(term));
		}
	}

	@Override
	public Double evaluate(double[] x) throws IllegalArgumentException
	{
		if (x.length != _inputSize) {
			throw new IllegalArgumentException(
					"The specified input vector does not have the same input size as this polynomial.");
		}
		double sum = 0;
		for (Term term : _terms) {
			sum = sum + term.evaluate(x);
		}
		return sum;
	}

	/**
	 * Returns the number of variables used by this polynomial.
	 * 
	 * @return the number of variables used by this polynomial
	 */
	public int getInputSize()
	{
		return _inputSize;
	}

	@Override
	public String toString()
	{
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < _terms.size(); i++) {
			buff.append(_terms.get(i).toString());
			if (i < _terms.size() - 1) {
				buff.append(" + ");
			}
		}
		return buff.toString();
	}

	/**
	 * Constructs a random polynomial given certain constraints.
	 * 
	 * @param numVars
	 *            the number of variables that can be used
	 * @param maxPower
	 *            the maximum exponent that can be used
	 * @return
	 */
	public static final Polynomial random(int numVars, int maxPower)
	{
		return random(-1, 1, numVars, maxPower, Random.nextInt(5) + 1);
	}

	/**
	 * Constructs a random polynomial given certain constraints.
	 * 
	 * @param coeffMin
	 *            the minimum coefficient value
	 * @param coeffMax
	 *            the maximum coefficient value
	 * @param numVars
	 *            the number of variables that can be used
	 * @param maxPower
	 *            the maximum exponent that can be used
	 * @param numTerms
	 *            the number of terms that will be generated
	 * @return a random polynomial
	 */
	public static final Polynomial random(double coeffMin, double coeffMax,
			int numVars, int maxPower, int numTerms)
	{
		List<Term> terms = new ArrayList<Term>(numTerms);

		for (int i = 0; i < numTerms; i++) {
			int numTermVars = Random.nextInt(numVars) + 1;
			int[] rperm = VectorOps.randperm(numVars);

			int[] varInds = new int[numTermVars];
			int[] varPows = new int[numTermVars];

			for (int j = 0; j < numTermVars; j++) {
				varInds[j] = rperm[j];
				varPows[j] = Random.nextInt(maxPower) + 1;
			}
			double coeff = Random.uniform(coeffMin, coeffMax);
			terms.add(new Term(coeff, varInds, varPows));
		}

		Polynomial p = new Polynomial(numVars, terms);
		return p;
	}

	/**
	 * Constructs a new polynomial from an existing polynomial where the output
	 * of all input dimensions between [-1,1] is constrained to be close to a
	 * desired minimum and maximum.
	 * 
	 * @param p
	 *            a polynomial
	 * @param numSamples
	 *            the number of samples used for estimating the minimum and
	 *            maximum of p
	 * @param outputMin
	 *            the desired minimum output
	 * @param outputMax
	 *            the desired maximum output
	 * @return a new polynomial with outputs that are constrained close to the
	 *         desired minimum and maximum
	 */
	public static final Polynomial scaleBounds(Polynomial p, int numSamples,
			double outputMin, double outputMax)
	{
		double[][] x = MatrixOps.random(numSamples, p.getInputSize(), -1, 1);
		double[] y = new double[numSamples];
		for (int i = 0; i < numSamples; i++) {
			y[i] = p.evaluate(x[i]);
		}
		double ymin = Statistics.min(y);
		double ymax = Statistics.max(y);

		double scale = (outputMax - outputMin) / (ymax - ymin);
		double bias = scale * (outputMin - ymin);

		List<Term> terms = new ArrayList<Term>(p._terms.size() + 1);
		for (int i = 0; i < p._terms.size(); i++) {
			Term pterm = p._terms.get(i);
			Term term = new Term(scale, pterm);
			terms.add(term);
		}
		// Add a final term to set the minimum
		terms.add(new Term(bias, new int[] {}, new int[] {}));

		return new Polynomial(p.getInputSize(), terms);
	}

	/**
	 * Parses a string into a polynomial object provided that the string matches
	 * the appropriate syntax.
	 * <p/>
	 * 
	 * <UL>
	 * <LI>polynomial := &lt;term&gt; {'+' &lt;term&gt;}</LI>
	 * <LI>term := &lt;scalar&gt;{&lt;variabl&gt; '^' '{' &lt; int &gt; '}'}</LI>
	 * <LI>scalar := '-'0-9.0-9</LI>
	 * <LI>variable := 'X' '[' &lt;int&gt; ']'</LI>
	 * <LI>int := 0-9{0-9}</LI>
	 * </UL>
	 * 
	 * @param polyStr
	 *            a string representing a polynomial
	 * @return a polynomial object
	 */
	public static final Polynomial polynomialParser(String polyStr)
	{
		String ignoreRegex = "\\s+"; // Ignore white space
		Tokenizer tokenizer = new Tokenizer(ignoreRegex, "\\{", "\\}", "\\^",
				"\\+", "\\[", "\\]", "X", Term.INTEGER_REGEX, Term.SCALAR_REGEX);
		List<String> tokens = tokenizer.tokenize(polyStr);

		List<Term> terms = new ArrayList<Term>();
		int tokenInd = 0;
		while (tokenInd < tokens.size()) {
			List<String> termTokens = Tokenizer.tokenSublist(tokens, tokenInd,
					"\\+");
			if (termTokens.get(termTokens.size() - 1).matches("\\+")) {
				termTokens.remove(termTokens.size() - 1); // Remove the '+'
															// token
			}
			terms.add(termParser(termTokens));
			tokenInd += termTokens.size() + 1;
		}

		int[] maxVarInds = new int[terms.size()];
		for (int i = 0; i < terms.size(); i++) {
			maxVarInds[i] = terms.get(i).maxIndexVar();
		}

		int numVars = Statistics.max(maxVarInds) + 1;
		Polynomial p = new Polynomial(numVars, terms);
		return p;
	}

	private static final Term termParser(List<String> termTokens)
	{
		if (termTokens.size() == 0) {
			throw new SyntaxException(
					"Terms cannot be constructed without a coefficient.");
		} else {
			try {
				double coeff = Double.parseDouble(termTokens.get(0));
				if (termTokens.size() == 1) {
					return new Term(coeff, new int[] {}, new int[] {});
				} else {
					List<Integer> varInds = new ArrayList<Integer>();
					List<Integer> varPows = new ArrayList<Integer>();
					int tokenIndex = 1;
					while (tokenIndex < termTokens.size()) {
						String nextToken = termTokens.get(tokenIndex);
						if (nextToken.equals("X")) {
							List<String> varTokens = Tokenizer.tokenSublist(
									termTokens, tokenIndex, "\\]");
							int varInd = varParser(varTokens);
							varInds.add(varInd);
							varPows.add(1);
							tokenIndex += varTokens.size();
						} else if (nextToken.equals("^")) {
							List<String> powTokens = Tokenizer.tokenSublist(
									termTokens, tokenIndex, "\\}");
							int varPow = powParser(powTokens);
							varPows.set(varPows.size() - 1, varPow);
							tokenIndex += powTokens.size();
						} else {
							throw new SyntaxException(
									"Expected 'X' or '^', found : " + nextToken);
						}
					}

					int[] varIndsA = new int[varInds.size()];
					int[] varPowsA = new int[varPows.size()];
					for (int i = 0; i < varInds.size(); i++) {
						varIndsA[i] = varInds.get(i);
						varPowsA[i] = varPows.get(i);
					}
					return new Term(coeff, varIndsA, varPowsA);
				}
			} catch (NumberFormatException ex) {
				throw new SyntaxException("Expected scalar, found : "
						+ termTokens.get(0));
			}
		}
	}

	/**
	 * Parse the variable grammar rule.
	 * 
	 * @param varTokens
	 *            a list of relevant tokens
	 * @return an integer determining the index of a variable
	 */
	private static final int varParser(List<String> varTokens)
	{
		try {
			if (!varTokens.get(0).equals("X")) {
				throw new SyntaxException("Expected 'X', found : "
						+ varTokens.get(0));
			}
			if (!varTokens.get(1).equals("[")) {
				throw new SyntaxException("Expected '[', found : "
						+ varTokens.get(1));
			}
			if (!varTokens.get(3).equals("]")) {
				throw new SyntaxException("Expected ']', found : "
						+ varTokens.get(3));
			}

			return Integer.parseInt(varTokens.get(2));
		} catch (NumberFormatException ex) {
			throw new SyntaxException("Expected integer, found : "
					+ varTokens.get(2));
		}
	}

	/**
	 * Parse the power grammar rule.
	 * 
	 * @param powTokens
	 *            a list of relevant tokens
	 * @return an integer determining the power specified by this expression
	 */
	private static final int powParser(List<String> powTokens)
	{
		try {
			if (!powTokens.get(0).equals("^")) {
				throw new SyntaxException("Expected '^', found : "
						+ powTokens.get(0));
			}
			if (!powTokens.get(1).equals("{")) {
				throw new SyntaxException("Expected '{', found : "
						+ powTokens.get(1));
			}
			if (!powTokens.get(3).equals("}")) {
				throw new SyntaxException("Expected '}', found : "
						+ powTokens.get(3));
			}

			return Integer.parseInt(powTokens.get(2));
		} catch (NumberFormatException ex) {
			throw new SyntaxException("Expected integer, found : "
					+ powTokens.get(2));
		}
	}

	public static void main(String[] args)
	{
		// Polynomial p = Polynomial.polynomialParser("2X[0]^{2} + 2X[0] + 0");
		Polynomial p = Polynomial.scaleBounds(Polynomial.random(1, 5), 1000, -1, 1);
		System.out.println("p = " + p.toString());

		double[] x = VectorOps.range(-1, 1, 1000);
		double[] y = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			y[i] = p.evaluate(new double[] { x[i] });
		}

		tmrutil.graphics.plot2d.Plot2D.linePlot(x, y, java.awt.Color.RED,
				p.toString(), "X", "Y");
	}
}
