package tmrutil.math.functions;

public class BellCurve extends Compose
{
	public BellCurve()
	{
		super(new Exp(), new Compose(new Negative(), new XSquared()));
	}
}
