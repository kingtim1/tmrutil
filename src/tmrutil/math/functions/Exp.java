package tmrutil.math.functions;

import tmrutil.math.RealFunction;

public class Exp extends RealFunction
{

	@Override
	public Double evaluate(Double x)
	{
		return Math.exp(x);
	}
	
	@Override
	public Double differentiate(Double x)
	{
		return Math.exp(x);
	}

}
