package tmrutil.math.functions;

import tmrutil.math.RealFunction;

public class OneOverX extends RealFunction
{
	
	@Override
	public Double evaluate(Double x) throws IllegalArgumentException
	{
		return 1.0 / x;
	}
	
	@Override
	public Double differentiate(Double x) throws IllegalArgumentException
	{
		return - (1.0 / (x * x));
	}

}
