package tmrutil.math.functions;

import tmrutil.math.RealFunction;

/**
 * Implements the mathematical arc tangent function.
 * @author Timothy A. Mann
 *
 */
public class Atan extends RealFunction
{

	@Override
	public Double evaluate(Double x) throws IllegalArgumentException
	{
		return Math.atan(x);
	}
	
	@Override
	public Double differentiate(Double x)
	{
		return 1.0 / (1.0 + (x*x));
	}

}
