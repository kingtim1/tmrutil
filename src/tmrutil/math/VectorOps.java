/*
 * VectorOps.java
 */

/* Package */
package tmrutil.math;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.stats.Statistics;

/* Imports */

/**
 * A set of useful non-side-affecting operations on arrays.
 */
public final class VectorOps
{
	/**
	 * Returns the absolute value of all components of the vector.
	 * 
	 * @param v
	 *            a vector
	 * @return a vector where each component is the absolute value of the
	 *         corresponding component in <code>v</code>.
	 */
	public static final double[] abs(double[] v)
	{
		double[] u = new double[v.length];
		return abs(v, u);
	}

	/**
	 * Returns the absolute value of all components of the vector.
	 * 
	 * @param v
	 *            a vector
	 * @param result
	 *            a vector to hold this functions response
	 * @return a vector where each component is the absolute value of the
	 *         corresponding component in <code>v</code>.
	 */
	public static final double[] abs(double[] v, double[] result)
	{
		for (int i = 0; i < v.length; i++) {
			result[i] = Math.abs(v[i]);
		}
		return result;
	}

	/**
	 * Returns a vector such that each component of the specified vector is
	 * raised to a specified power.
	 * 
	 * @param v
	 *            a vector
	 * @param x
	 *            the power to raise each component by.
	 * @return a vector where each component is the value of the corresponding
	 *         component in <code>v</code> raise to the <code>x</code> power.
	 */
	public static final double[] pow(double[] v, double x)
	{
		double[] u = new double[v.length];
		return pow(v, x, u);
	}

	/**
	 * Returns a vector such that each component of the specified vector is
	 * raised to a specified power.
	 * 
	 * @param v
	 *            a vector
	 * @param x
	 *            the power to raise each component by.
	 * @param result
	 *            a vector to hold this functions response
	 * @return a vector where each component is the value of the corresponding
	 *         component in <code>v</code> raise to the <code>x</code> power.
	 */
	public static final double[] pow(double[] v, double x, double[] result)
	{
		for (int i = 0; i < v.length; i++) {
			result[i] = Math.pow(v[i], x);
		}
		return result;
	}

	/**
	 * Returns the dot (inner) product of two vectors.
	 */
	public static final double dotProduct(double[] a, double[] b)
			throws DimensionMismatchException
	{
		if (a.length != b.length) {
			throw new DimensionMismatchException("The dimensions of a("
					+ a.length + ") and b(" + b.length
					+ ") must match in order to take their dot product.");
		}
		double dp = 0;
		for (int i = 0; i < a.length; i++) {
			dp = dp + (a[i] * b[i]);
		}
		return dp;
	}

	/**
	 * Computes the inner product between two vectors <code>a</code> and
	 * <code>b</code> using a <code>kernel</code> matrix.
	 * 
	 * @param a
	 *            a vector with N components
	 * @param kernel
	 *            an N x M matrix
	 * @param b
	 *            a vector with M components
	 * @return the inner product equal to <code>a * kernel * b</code>
	 * @throws DimensionMismatchException
	 *             if the number of components of <code>a</code> does not match
	 *             the number of rows of <code>kernel</code> or the number of
	 *             components of <code>b</code> does not match the number of
	 *             columns of <code>kernel</code>
	 */
	public static final double innerProduct(double[] a, double[][] kernel,
			double[] b) throws DimensionMismatchException
	{
		if (a.length != MatrixOps.getNumRows(kernel)
				|| MatrixOps.getNumCols(kernel) != b.length) {
			throw new DimensionMismatchException(
					"Cannot calculate inner product because either the length of the length of the left hand side vector does not match the number of kernel matrix rows or the length of the right hand side vector does not match the number of kernel matrix columns.");
		}
		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i] * dotProduct(kernel[i], b);
		}
		return sum;
	}

	/**
	 * Returns the outer product of two vectors.
	 */
	public static final double[][] outerProduct(double[] a, double[] b)
	{
		double[][] outer = new double[a.length][b.length];
		return outerProduct(a, b, outer);
	}

	/**
	 * Returns the outer product of two vectors and stores the values in the
	 * provided response matrix.
	 */
	public static final double[][] outerProduct(double[] a, double[] b,
			double[][] response)
	{
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b.length; j++) {
				response[i][j] = a[i] * b[j];
			}
		}
		return response;
	}

	/**
	 * Computes the cross product for two 3-dimensional vectors.
	 * 
	 * @param a
	 *            a 3-dimensional vector
	 * @param b
	 *            a 3-dimensional vector
	 * @return the cross product of <code>a</code> and <code>b</code>
	 */
	public static final double[] crossProduct(double[] a, double[] b)
	{
		return crossProduct(a, b, new double[3]);
	}

	/**
	 * Computes the cross product for two 3-dimensional vectors and stores the
	 * result in the specified <code>result</code> vector.
	 * 
	 * @param a
	 *            a 3-dimensional vector
	 * @param b
	 *            a 3-dimensional vector
	 * @param result
	 *            a buffer to store the result
	 * @return the cross product of <code>a</code> and <code>b</code> and a
	 *         reference to <code>result</code>
	 */
	public static final double[] crossProduct(double[] a, double[] b,
			double[] result)
	{
		if (a.length != 3 || b.length != 3) {
			throw new IllegalArgumentException(
					"Cross product is only defined for vectors in 3-dimensional space.");
		}
		result[0] = (a[1] * b[2]) - (a[2] * b[1]);
		result[1] = (a[2] * b[0]) - (a[0] * b[2]);
		result[2] = (a[0] * b[1]) - (a[1] * b[0]);
		return result;
	}

	/**
	 * Returns the specified vector with each component modified by adding a
	 * scalar value to it.
	 */
	public static final double[] add(double[] a, double s)
	{
		double[] v = new double[a.length];
		return add(a, s, v);
	}

	/**
	 * Returns the specified vector with each component modified by adding a
	 * scalar value to it.
	 */
	public static final double[] add(double[] a, double s, double[] result)
	{
		for (int i = 0; i < a.length; i++) {
			result[i] = a[i] + s;
		}
		return result;
	}

	/**
	 * Returns the vector sum of two vectors added together.
	 */
	public static final double[] add(double[] a, double[] b)
			throws DimensionMismatchException
	{
		double[] s = new double[a.length];
		return add(a, b, s);
	}

	/**
	 * Returns the vector sum of two vectors added together stored in the
	 * supplied response vector.
	 */
	public static final double[] add(double[] a, double[] b, double[] result)
			throws DimensionMismatchException
	{
		if (a.length != b.length) {
			throw new DimensionMismatchException("The dimensions of a("
					+ a.length + ") and b(" + b.length
					+ ") must match in order to perform vector addition.");
		}
		for (int i = 0; i < a.length; i++) {
			result[i] = a[i] + b[i];
		}
		return result;
	}

	/**
	 * Returns the specified vector multiplied by a scalar value.
	 */
	public static final double[] multiply(double[] a, double scalar)
	{
		double[] s = new double[a.length];
		return multiply(a, scalar, s);
	}

	/**
	 * Returns the specified vector multiplied by a scalar value and stores the
	 * result in the supplied response vector.
	 */
	public static final double[] multiply(double[] a, double scalar,
			double[] result)
	{
		for (int i = 0; i < a.length; i++) {
			result[i] = a[i] * scalar;
		}
		return result;
	}

	/**
	 * Returns the specified vector multiplied by a scalar value.
	 */
	public static final double[] multiply(double scalar, double[] a)
	{
		double[] s = new double[a.length];
		return multiply(scalar, a, s);
	}

	/**
	 * Returns the specified vector multiplied by a scalar value.
	 */
	public static final double[] multiply(double scalar, double[] a,
			double[] result)
	{
		for (int i = 0; i < a.length; i++) {
			result[i] = scalar * a[i];
		}
		return result;
	}

	/**
	 * Returns the component-wise multiplication of two vectors.
	 * 
	 * @param a
	 *            a vector
	 * @param b
	 *            a vector
	 * @throws DimensionMismatchException
	 *             if either of the vector arguments are not the same length
	 */
	public static final double[] multiply(double[] a, double[] b)
			throws DimensionMismatchException
	{
		double[] result = new double[a.length];
		return multiply(a, b, result);
	}

	/**
	 * Returns the component-wise multiplication of two vectors which stores the
	 * result in the provided vector.
	 * 
	 * @param a
	 *            a vector
	 * @param b
	 *            a vector
	 * @param result
	 *            a vector to store the result in
	 * @throws DimensionMismatchException
	 *             if either of the vector arguments are not the same length
	 */
	public static final double[] multiply(double[] a, double[] b,
			double[] result) throws DimensionMismatchException
	{
		if (a.length != b.length) {
			throw new DimensionMismatchException(
					"The dimensions of a("
							+ a.length
							+ ") and b("
							+ b.length
							+ ") must match in order to perform component-wise vector multiplication.");
		}
		for (int i = 0; i < a.length; i++) {
			result[i] = a[i] * b[i];
		}
		return result;
	}

	/**
	 * Returns the component-wise division of two vectors.
	 * 
	 * @param a
	 *            a vector
	 * @param b
	 *            a vector
	 * @throws DimensionMismatchException
	 *             if either of the vector arguments are not the same length
	 */
	public static final double[] divide(double[] a, double[] b)
			throws DimensionMismatchException
	{
		if (a.length != b.length) {
			throw new DimensionMismatchException(
					"The dimensions of a("
							+ a.length
							+ ") and b("
							+ b.length
							+ ") must match in order to perform component-wise vector division.");
		}
		double[] s = new double[a.length];
		for (int i = 0; i < s.length; i++) {
			s[i] = a[i] / b[i];
		}
		return s;
	}

	/**
	 * Computes each component of the vector <code>a</code> divided by the
	 * scalar <code>s</code> and stores the result in the provided vector.
	 * 
	 * @param a
	 *            a vector
	 * @param s
	 *            a scalar
	 * @return the result of the division
	 */
	public static final double[] divide(double[] a, double s)
	{
		return divide(a, s, new double[a.length]);
	}

	/**
	 * Computes each component of the vector <code>a</code> divided by the
	 * scalar <code>s</code> and stores the result in the provided vector.
	 * 
	 * @param a
	 *            a vector
	 * @param s
	 *            a scalar
	 * @param result
	 *            a vector used to store the result
	 * @return the result vector
	 */
	public static final double[] divide(double[] a, double s, double[] result)
	{
		for (int i = 0; i < a.length; i++) {
			result[i] = a[i] / s;
		}
		return result;
	}

	/**
	 * Subtracts one vector from another and returns the result in a new array
	 * of doubles.
	 */
	public static final double[] subtract(double[] a, double[] b)
			throws DimensionMismatchException
	{
		return subtract(a, b, new double[a.length]);
	}

	/**
	 * Subtracts the components of <code>b</code> from <code>a</code> and stores
	 * the result in the provided vector.
	 * 
	 * @param a
	 *            a vector
	 * @param b
	 *            a vector
	 * @param result
	 *            a vector used to store the results of subtraction
	 * @return the result vector
	 * @throws DimensionMismatchException
	 *             if a and b are of different lengths
	 */
	public static final double[] subtract(double[] a, double[] b,
			double[] result) throws DimensionMismatchException
	{
		if (a.length != b.length) {
			throw new DimensionMismatchException("The dimensions of a("
					+ a.length + ") and b(" + b.length
					+ ") must match in order to perform vector subtraction.");
		}
		for (int i = 0; i < a.length; i++) {
			result[i] = a[i] - b[i];
		}
		return result;
	}

	/**
	 * Computes the sum of all of the elements of vector <code>v</code>.
	 * 
	 * @param v
	 *            a vector
	 * @return the sum of elements in <code>v</code>
	 */
	public static final double sum(double[] v)
	{
		double sum = 0;
		for (int i = 0; i < v.length; i++) {
			sum += v[i];
		}
		return sum;
	}

	/**
	 * Returns the length (i.e. Euclidean distance) of a specified vector.
	 * 
	 * @param a
	 *            a vector
	 */
	public static final double length(double[] a)
	{
		// double d = Math.sqrt(Statistics.sum(pow(a, 2)));
		// return d;
		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i] * a[i];
		}
		return Math.sqrt(sum);
	}

	/**
	 * Returns the normalized version of a specified array of doubles.
	 * 
	 * @param a
	 *            a vector
	 */
	public static final double[] normalize(double[] a)
	{
		return normalize(a, new double[a.length]);
	}

	/**
	 * Calculates the normalized version of a specified array of doubles and
	 * stores it in the provided result vector.
	 * 
	 * @param a
	 *            a vector
	 * @param result
	 *            a vector for storing the result of the normalization
	 * @return the result vector containing the normalized version of
	 *         <code>a</code>
	 */
	public static final double[] normalize(double[] a, double[] result)
	{
		return divide(a, length(a), result);
	}

	/**
	 * Returns the Euclidean distance between two vectors.
	 * 
	 * @param a
	 *            a vector
	 * @param b
	 *            a vector
	 * @return the Euclidean distance
	 * @throws DimensionMismatchException
	 *             if <code>a.length != b.length</code>
	 */
	public static final double distance(double[] a, double[] b)
			throws DimensionMismatchException
	{
		return Math.sqrt(distanceSqd(a, b));
	}

	/**
	 * Returns the square of the Euclidean distance between two vectors.
	 * 
	 * @param a
	 *            a vector
	 * @param b
	 *            a vector
	 * @return the square of the Euclidean distance
	 * @throws DimensionMismatchException
	 *             if <code>a.length != b.length</code>
	 */
	public static final double distanceSqd(double[] a, double[] b)
			throws DimensionMismatchException
	{
		if (a.length != b.length) {
			throw new DimensionMismatchException(
					"Calculating the squared distance between two vectors a and b requires that they have an equal number of dimensions. a.length(="
							+ a.length + ") != b.length(=" + b.length + ")");
		}
		double d = 0;
		for (int i = 0; i < a.length; i++) {
			d = d + Math.pow(a[i] - b[i], 2);
		}
		return d;
	}

	/**
	 * Returns the Euclidean distance between two vectors.
	 */
	public static final double distance(int[] a, int[] b)
			throws DimensionMismatchException
	{
		if (a.length != b.length) {
			throw new DimensionMismatchException(
					"Calculating the distance between two vectors a and b requires that they have an equal number of dimensions. a.length(="
							+ a.length + ") != b.length(=" + b.length + ")");
		}
		double d = 0;
		for (int i = 0; i < a.length; i++) {
			d = d + Math.pow(a[i] - b[i], 2);
		}
		return Math.sqrt(d);
	}

	/**
	 * Calculates the Manhattan or Taxicab distance between two vectors.
	 * 
	 * @param a
	 *            a vector
	 * @param b
	 *            a vector
	 * @return the distance between <code>a</code> and <code>b</code>
	 */
	public static final double manhattanDistance(double[] a, double[] b)
	{
		if (a.length != b.length) {
			throw new DimensionMismatchException(
					"Calculating the Manhattan (or Taxicab) distance between two vectors requires that they have equal number of dimensions.");
		}
		double d = 0;
		for (int i = 0; i < a.length; i++) {
			d = d + Math.abs(a[i] - b[i]);
		}
		return d;
	}

	/**
	 * Returns true if the two vectors are equal.
	 */
	public static final boolean equal(double[] a, double[] b)
	{
		if (a.length != b.length) {
			return false;
		}
		for (int i = 0; i < a.length; i++) {
			if (a[i] != b[i]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns true if the two vectors are equal.
	 */
	public static final boolean equal(int[] a, int[] b)
	{
		if (a.length != b.length) {
			return false;
		}
		for (int i = 0; i < a.length; i++) {
			if (a[i] != b[i]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Constructs a deep copy of the vector <code>a</code>.
	 * 
	 * @param a
	 *            a vector
	 * @return a deep copy of <code>a</code>
	 */
	public static final double[] deepCopy(double[] a)
	{
		return Arrays.copyOf(a, a.length);
	}

	/**
	 * Constructs a deep copy of the vector <code>a</code>.
	 * 
	 * @param a
	 *            a vector
	 * @return a deep copy of <code>a</code>
	 */
	public static final int[] deepCopy(int[] a)
	{
		return Arrays.copyOf(a, a.length);
	}

	/**
	 * Copies the components of <code>a</code> to <code>result</code>.
	 * 
	 * @param a
	 *            a vector
	 * @param result
	 *            a vector to store the components of <code>a</code> in
	 * @return the result vector
	 */
	public static final double[] copy(double[] a, double[] result)
	{
		System.arraycopy(a, 0, result, 0, a.length);
		return result;
	}

	/**
	 * Copies the components of <code>a</code> to <code>result</code>.
	 * 
	 * @param a
	 *            a vector
	 * @param result
	 *            a vector to store the components of <code>a</code> in
	 * @return the result vector
	 */
	public static final int[] copy(int[] a, int[] result)
	{
		System.arraycopy(a, 0, result, 0, a.length);
		return result;
	}

	/**
	 * Converts an array of integers to an array of doubles.
	 * 
	 * @param v
	 *            an array of integers
	 * @return an array of doubles containing the values from <code>v</code>
	 */
	public static final double[] toDoubleArray(int[] v)
	{
		return toDoubleArray(v, new double[v.length]);
	}

	/**
	 * Converts an array of integers to an array of doubles provided a result
	 * vector to store the values.
	 * 
	 * @param v
	 *            an integer array
	 * @param result
	 *            a double array
	 * @return the result vector
	 */
	public static final double[] toDoubleArray(int[] v, double[] result)
	{
		for (int i = 0; i < v.length; i++) {
			result[i] = v[i];
		}
		return result;
	}

	/**
	 * Converts an array of doubles to an array of integers.
	 * 
	 * @param v
	 *            an array of doubles
	 * @return an array of integers containing the casted values from
	 *         <code>v</code>
	 */
	public static final int[] toIntArray(double[] v)
	{
		return toIntArray(v, new int[v.length]);
	}

	/**
	 * Converts an array of doubles to an array of integers provided a result
	 * vector to store the values.
	 * 
	 * @param v
	 *            a double array
	 * @param result
	 *            an integer array
	 * @return the result vector
	 */
	public static final int[] toIntArray(double[] v, int[] result)
	{
		for (int i = 0; i < v.length; i++) {
			result[i] = (int) v[i];
		}
		return result;
	}

	/**
	 * Sets each component within the specified index range to zero.
	 * 
	 * @param v
	 *            a vector
	 * @param begin
	 *            an index
	 * @param end
	 *            an index
	 */
	public static final double[] zero(double[] v, int begin, int end)
	{
		double[] s = java.util.Arrays.copyOf(v, v.length);
		for (int i = begin; i <= end; i++) {
			s[i] = 0;
		}
		return s;
	}

	/**
	 * Returns a vector with <code>n</code> components where the first component
	 * is <code>min</code>, the last component is <code>max</code>, and each
	 * component between <code>min</code> and <code>max</code> increases by
	 * <code>(max-min)/n</code>.
	 */
	public static final double[] range(double min, double max, int n)
	{
		double step = (max - min) / n;
		double[] ans = new double[n];
		for (int i = 0; i < n; i++) {
			ans[i] = min + i * step;
		}
		return ans;
	}

	/**
	 * Returns a vector with <code>n</code> components where each component is a
	 * randomly generated real value in the range [<code>lowerBound</code>,
	 * <code>upperBound</code>].
	 * 
	 * @param n
	 *            the number of components in the created vector
	 * @param lowerBound
	 *            the minimum value that any component could possibly have
	 * @param upperBound
	 *            the maximum value that any component could possibly have
	 * @return a vector with randomly generated components
	 */
	public static final double[] random(int n, double lowerBound,
			double upperBound)
	{
		return random(n, lowerBound, upperBound, new double[n]);
	}

	/**
	 * Returns the <code>result</code> vector with <code>n</code> components
	 * where each component is a randomly generated real value in the range [
	 * <code>lowerBound</code>, <code>upperBound</code>].
	 * 
	 * @param n
	 *            the number of components in the created vector
	 * @param lowerBound
	 *            the minimum value that any component could possibly have
	 * @param upperBound
	 *            the maximum value that any component could possibly have
	 * @param result
	 *            a vector to store the results
	 * @return a vector with randomly generated components
	 */
	public static final double[] random(int n, double lowerBound,
			double upperBound, double[] result)
	{
		double diff = upperBound - lowerBound;
		java.util.Random rand = new java.util.Random();
		for (int i = 0; i < n; i++) {
			result[i] = (rand.nextDouble() * diff) + lowerBound;
		}
		return result;
	}

	/**
	 * Creates a vector with <code>n</code> components all set to zero.
	 * 
	 * @param n
	 *            the number of dimensions of the vector to create
	 * @return a zero vector
	 */
	public static double[] zeros(int n)
	{
		return new double[n];
	}

	/**
	 * Creates a vector with <code>n</code> components all set to one.
	 * 
	 * @param n
	 *            the number of dimensions of the vector to create
	 * @return a vector of ones
	 */
	public static double[] ones(int n)
	{
		double[] ones = new double[n];
		Arrays.fill(ones, 1);
		return ones;
	}

	/**
	 * Returns a random permutation of integral values from 0 to
	 * <code>n - 1</code>.
	 * 
	 * @param n
	 *            the number of integral values
	 * @throws IllegalArgumentException
	 *             if <code>n &lt; 0</code>
	 */
	public static final int[] randperm(int n) throws IllegalArgumentException
	{
		return randperm(n, new int[n]);
	}

	/**
	 * Returns a random permutation of integral values from 0 to
	 * <code>n - 1</code>.
	 * 
	 * @param n
	 *            the number of integral values
	 * @param result
	 *            a vector to store the integers
	 * @return a random permutation of <code>n</code> values
	 * @throws IllegalArgumentException
	 *             if <code>n &lt; 0</code>
	 */
	public static final int[] randperm(int n, int[] result)
			throws IllegalArgumentException
	{
		if (n < 0) {
			throw new IllegalArgumentException(
					"Cannot construct a random permutation with negative number of elements : "
							+ n);
		}
		for (int i = 0; i < n; i++) {
			result[i] = i;
		}
		for (int i = 0; i < n; i++) {
			int i1 = tmrutil.stats.Random.RAND.nextInt(n);
			int i2 = tmrutil.stats.Random.RAND.nextInt(n);
			int tmp = result[i1];
			result[i1] = result[i2];
			result[i2] = tmp;
		}
		return result;
	}

	/**
	 * Checks whether any of the components in <code>a</code> contain a value of
	 * <code>NaN</code>.
	 * 
	 * @param a
	 *            a vector
	 * @return true if any components of <code>a</code> contain a value of
	 *         <code>NaN</code>
	 */
	public static final boolean containsNaN(double[] a)
	{
		for (int i = 0; i < a.length; i++) {
			if (Double.isNaN(a[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks whether any of the components in <code>a</code> contain a value of
	 * <code>Infinity</code> or <code>-Infinity</code>.
	 * 
	 * @param a
	 *            a vector
	 * @return true if any components of <code>a</code> contain a value of
	 *         <code>Infinity</code> or <code>-Infinity</code>
	 */
	public static final boolean containsInfinite(double[] a)
	{
		for (int i = 0; i < a.length; i++) {
			if (Double.isInfinite(a[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks whether any of the components in <code>a</code> contain a value of
	 * 0.
	 * 
	 * @param a
	 *            a vector
	 * @return true if any of the components of <code>a</code> are zero;
	 *         otherwise false
	 */
	public static final boolean containsZero(double[] a)
	{
		for (int i = 0; i < a.length; i++) {
			if (a[i] == 0.0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the string representation of a vector.
	 * 
	 * @param a
	 *            a vector
	 * @return the string representation of a vector
	 */
	public static final String toString(double[] a)
	{
		if (a.length == 0) {
			return "[]";
		}
		StringBuffer b = new StringBuffer();
		DecimalFormat df = new DecimalFormat("0.00;-0.00");
		b.append("[" + df.format(a[0]));
		for (int i = 1; i < a.length; i++) {
			b.append(", " + df.format(a[i]));
		}
		b.append("]");
		return b.toString();
	}

	/**
	 * Returns the string representation of a vector.
	 * 
	 * @param a
	 *            a vector
	 * @return the string representation of a vector
	 */
	public static final String toString(int[] a)
	{
		if (a.length == 0) {
			return "[]";
		}
		StringBuffer b = new StringBuffer();
		b.append("[" + a[0]);
		for (int i = 1; i < a.length; i++) {
			b.append(", " + a[i]);
		}
		b.append("]");
		return b.toString();
	}

	/**
	 * Creates a 1xN matrix from a vector of length N.
	 * 
	 * @param v
	 *            a vector of length N
	 * @return a 1xN matrix
	 */
	public static final double[][] toMatrix(double[] v)
	{
		double[][] m = new double[1][];
		m[0] = deepCopy(v);
		return m;
	}

	/**
	 * Creates a 1xN matrix from a vector of length N.
	 * 
	 * @param v
	 *            a vector of length N
	 * @return a 1xN matrix
	 */
	public static final int[][] toMatrix(int[] v)
	{
		int[][] m = new int[1][];
		m[0] = deepCopy(v);
		return m;
	}

	/**
	 * Creates a clipped version of vector <code>v</code> such that any
	 * component that is less than the minimum value is set to the minimum value
	 * and any value that is greater than the maximum value is set to the
	 * maximum value. The new vector is stored in the provided
	 * <code>result</code> vector. The <code>result</code> vector is returned.
	 * 
	 * @param v
	 *            a vector
	 * @param min
	 *            the minimum value
	 * @param max
	 *            the maximum value
	 * @param result
	 *            the vector where the new values are stored
	 * @return a clipped version of <code>v</code> which is also a reference to
	 *         <code>result</code>
	 */
	public static final double[] clip(double[] v, double min, double max,
			double[] result)
	{
		if (min > max) {
			throw new IllegalArgumentException(
					"Minimum must be less than or equal to maximum.");
		}
		for (int i = 0; i < v.length; i++) {
			result[i] = v[i];
			if (result[i] < min) {
				result[i] = min;
			}
			if (result[i] > max) {
				result[i] = max;
			}
		}
		return result;
	}

	/**
	 * Creates a clipped version of <code>v</code> such that any component that
	 * is less than the minimum value is set to the minimum value and any
	 * component that is greater than the maximum value is set to the maximum
	 * value.
	 * 
	 * @param v
	 *            a vector
	 * @param min
	 *            the minimum value
	 * @param max
	 *            the maximum value
	 * @return a clipped version of <code>v</code>
	 */
	public static final double[] clip(double[] v, double min, double max)
	{
		return clip(v, min, max, new double[v.length]);
	}

	/**
	 * Applies the specified function to each component of a vector.
	 * 
	 * @param f
	 *            a function of a single variable
	 * @param x
	 *            a vector of real values
	 * @return a vector whose components are the value of f applied to each
	 *         component
	 */
	public static final double[] apply(RealFunction f, double[] x)
	{
		return apply(f, x, new double[x.length]);
	}

	/**
	 * Applies the specified function to each component of a vector.
	 * 
	 * @param f
	 *            a function of a single variable
	 * @param x
	 *            a vector of real values
	 * @param result
	 *            a vector to store the result in
	 * @return a vector whose components are the value of f applied to each
	 *         component
	 */
	public static final double[] apply(RealFunction f, double[] x,
			double[] result)
	{
		for (int i = 0; i < result.length; i++) {
			result[i] = f.evaluate(x[i]);
		}
		return result;
	}

	/**
	 * Constructs a new vector with the elements of <code>vector</code> in the
	 * order specified by the indices of <code>order</code>.
	 * 
	 * @param vector
	 *            a vector of elements
	 * @param order
	 *            a list of indices
	 * @return the rearranged vector
	 * @throws IllegalArgumentException
	 *             if the number of elements in the vector does not match the
	 *             number of indices
	 */
	public static double[] rearrange(double[] vector, int[] order)
			throws IllegalArgumentException
	{
		if (vector.length != order.length) {
			throw new IllegalArgumentException(
					"The number of elements in the vector does not match the number of indices");
		}
		double[] copy = Arrays.copyOf(vector, vector.length);
		for (int i = 0; i < vector.length; i++) {
			copy[i] = vector[order[i]];
		}
		return copy;
	}

	/**
	 * Creates a list of vectors that cover a multidimensional space in
	 * grid-like fashion.
	 * 
	 * @param dimMins
	 *            the minimum value for each dimension
	 * @param dimMaxs
	 *            the maximum value for each dimension
	 * @param density
	 *            the density of nodes for each dimension
	 * @return a list of vectors that form a grid over the specified
	 *         multidimensional space
	 * @throws IllegalArgumentException
	 *             if the number of elements in dimMins, dimMaxs, or density do
	 *             not match
	 */
	public static final List<double[]> computeGrid(double[] dimMins,
			double[] dimMaxs, int[] density) throws IllegalArgumentException
	{
		if (dimMins.length != dimMaxs.length) {
			throw new IllegalArgumentException(
					"Number of dimensions specified by dimMins and dimMaxs do not match");
		}
		if (dimMins.length != density.length) {
			throw new IllegalArgumentException(
					"Number of dimensions specified by density does not match dimMins or dimMaxs");
		}

		int product = Statistics.product(density);
		List<double[]> grid = new ArrayList<double[]>(product);
		double[] diffs = subtract(dimMaxs, dimMins);

		int[] count = new int[density.length];
		for (int i = 0; i < product; i++) {

			// Create a new vector
			double[] v = new double[density.length];
			// Populate the vector
			for (int d = 0; d < density.length; d++) {
				double inc = diffs[d] / (density[d]-1);
				v[d] = (inc * count[d]) + dimMins[d];
			}
			// Add it to the grid
			grid.add(v);

			// Update the count vector
			count[0] += 1;
			for (int d = 0; d < density.length; d++) {
				if (count[d] > density[d]) {
					count[d] = 0;
					if (d + 1 < density.length) {
						count[d + 1] += 1;
					}
				}
			}
		}

		return grid;
	}

	/**
	 * This class cannot and should not be instantiated.
	 */
	private VectorOps()
	{
	}
}