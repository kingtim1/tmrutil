package tmrutil.math;

/**
 * Represents a kernel function.
 * @author Timothy A. Mann
 *
 */
public interface KernelFunction<V>
{
	/**
	 * Implicitly evaluates the dot product in higher dimensional space.
	 * @param a a vector of length N
	 * @param b a vector of length N
	 * @return result of the dot product in high dimensional space
	 */
	public double evaluate(V a, V b) throws IllegalArgumentException;
}
