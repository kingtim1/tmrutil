package tmrutil.math;

/**
 * A set of useful side-affecting matrix operations.
 * 
 * @author Timothy Mann
 * 
 */
public class SAMatrixOps
{
	/**
	 * Add matrix <code>b</code> two matrix <code>a</code> where <code>a</code>
	 * will be modified during the addition.
	 * 
	 * @param a
	 *            a matrix which will be modified during addition
	 * @param b
	 *            a matrix
	 * @return a reference to the matrix <code>a</code>
	 */
	public static final double[][] add(double[][] a, double[][] b)
			throws DimensionMismatchException
	{
		try {
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < a[i].length; j++) {
					a[i][j] = a[i][j] + b[i][j];
				}
			}
			return a;
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new DimensionMismatchException(
					"The number of rows and columns must match to perform matrix addition.");
		}
	}

	/**
	 * Sets the components of the matrix <code>r</code> to uniformly distributed
	 * random values in [0,1].
	 * 
	 * @param r
	 *            a matrix that will be modified
	 * @return a reference to <code>r</code>
	 */
	public static final double[][] random(double[][] r)
	{
		for (int i = 0; i < r.length; i++) {
			for (int j = 0; j < r[i].length; j++) {
				r[i][j] = tmrutil.stats.Random.uniform();
			}
		}
		return r;
	}

	/**
	 * Sets the components of the matrix <code>r</code> to uniformly distributed
	 * random values in [<code>lowerBound</code>,<code>upperBound</code>].
	 * 
	 * @param r
	 *            a matrix that will be modified
	 * @param lowerBound
	 *            the lower bound for the randomly generated components
	 * @param upperBound
	 *            the upper bound for the randomly generated components
	 * @return a reference to <code>r</code>
	 */
	public static final double[][] random(double[][] r, double lowerBound, double upperBound)
	{
		for (int i = 0; i < r.length; i++) {
			for (int j = 0; j < r[i].length; j++) {
				r[i][j] = tmrutil.stats.Random.uniform(lowerBound, upperBound);
			}
		}
		return r;
	}
}
