/*
 * Modulus.java
 */

/* Package */
package tmrutil.math;

/* Imports */

/**
 * Utilities for modulus.
 */
public class Modulus {
	
	public static final int mod(int a, int b) {
		if (b == 0) {
			throw new ArithmeticException("Division by 0 is not defined.");
		}
		int m = a % b;
		if (m < 0) {
			return b + m;
		}
		return m;
	}
	
	public static final double mod(double a, double b) {
		if (b == 0) {
			throw new ArithmeticException("Division by 0 is not defined.");
		}
		double m = a % b;
		if (m < 0) {
			return b + m;
		}
		return m;
	}

	private Modulus() {
	}
}