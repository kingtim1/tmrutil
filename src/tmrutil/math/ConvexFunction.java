package tmrutil.math;

/**
 * A tag interface for convex functions.
 * @author Timothy A. Mann
 *
 * @param <D> the domain type
 * @param <R> the range type
 */
public interface ConvexFunction<D, R> extends Function<D, R> {

}
