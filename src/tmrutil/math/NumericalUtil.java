package tmrutil.math;

/**
 * Useful numerical operations.
 * @author Timothy A. Mann
 *
 */
public final class NumericalUtil
{
	private NumericalUtil(){}
	
	/**
	 * Returns true if the specified scalar value is equal to zero.
	 * @param val a scalar value
	 * @return true if <code>val == 0.0</code>; otherwise false
	 */
	public static final boolean isZero(double val)
	{
		return val == 0.0;
	}
	
	/**
	 * Returns true if the specified scalar value is close to zero (i.e. within some threshold).
	 * @param val a scalar value
	 * @param threshold a small positive value used to determine whether <code>val</code> is close to zero or not
	 * @return true if <code>|val| <= threshold
	 */
	public static final boolean isCloseToZero(double val, double threshold)
	{
		if(threshold < 0.0){
			throw new IllegalArgumentException("Threshold must be positive.");
		}
		return Math.abs(val) <= threshold;
	}
	
	
}
