package tmrutil.math.metrics;

import tmrutil.math.DimensionMismatchException;
import tmrutil.math.Metric;
import tmrutil.math.VectorOps;

/**
 * Implements a distance measure based on whether or not two vectors have the
 * same hyper angle. The values range from [0, 1], where zero means that the
 * angles are identical, 1 means that the angles are opposite, and 0.5 means
 * that the vectors are orthogonal.<p/>
 * 
 * Computes the dot product between the normalized forms of two vectors, then
 * the negative of <code>-(1 - dp)/2</code> is returned, where <code>dp</code>
 * is the dot product. Because the dot product of normalized vectors ranges from
 * [-1, 1], subtracting the one from the dot product gives the new range [-2, 0].
 * After taking the negative and dividing by 2 the final range is [0, 1].
 * 
 * @author Timothy A. Mann
 * 
 */
public class HyperAngleDistance implements Metric<double[]>
{

	@Override
	public double distance(double[] x, double[] y)
	{
		if(x.length != y.length){
			throw new DimensionMismatchException("The dimension of x and y must match.");
		}
		double[] normX = VectorOps.normalize(x);
		double[] normY = VectorOps.normalize(y);
		double dp = VectorOps.dotProduct(normX, normY);
		return -(dp - 1) / 2;
	}

}
