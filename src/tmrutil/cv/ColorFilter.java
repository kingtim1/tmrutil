package tmrutil.cv;

import java.awt.Color;
import tmrutil.stats.Filter;

/**
 * A color filter accepts or rejects a pixel based on its color.
 * @author Timothy A. Mann
 *
 */
public interface ColorFilter extends Filter<Color>
{
}
