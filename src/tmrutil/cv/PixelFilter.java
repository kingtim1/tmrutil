package tmrutil.cv;

import tmrutil.stats.Filter;

/**
 * A pixel filter accepts or rejects a pixel based on its color and position in the image.
 * @author Timothy A. Mann
 *
 */
public interface PixelFilter extends Filter<Pixel>
{
}
