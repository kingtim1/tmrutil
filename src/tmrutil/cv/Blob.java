package tmrutil.cv;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A blob is a set containing adjacent pixels or pixels that are directly
 * connected through an adjacency chain that are all accepted by the same pixel
 * filter.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Blob
{
	private boolean[][] _inBlob;
	private int _width;
	private int _height;
	private int _size;

	/**
	 * Constructs a blob with a specified image width and height.
	 * 
	 * @param width
	 *            the width of the image that this blob refers to
	 * @param height
	 *            the height of the image that this blob refers to
	 */
	protected Blob(int width, int height)
	{
		_inBlob = new boolean[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				_inBlob[x][y] = false;
			}
		}
		_width = width;
		_height = height;
		_size = 0;
	}

	/**
	 * Determines whether the blob contains a specified pixel.
	 * 
	 * @param p
	 *            a pixel
	 * @return true if this blob contains <code>p</code>; otherwise false
	 */
	public boolean contains(Pixel p)
	{
		return _inBlob[p.getX()][p.getY()];
	}

	/**
	 * Determines whether the blob contains a specified point.
	 * 
	 * @param p
	 *            a point
	 * @return true if this blob contains <code>p</code>; otherwise false
	 */
	public boolean contains(Point p)
	{
		return _inBlob[p.x][p.y];
	}

	/**
	 * Determines whether the blob contains a specified point.
	 * 
	 * @param x
	 *            an X-coordinate
	 * @param y
	 *            a Y-coordinate
	 * @return true if this blob contains <code>(x, y)</code>; otherwise false
	 */
	public boolean contains(int x, int y)
	{
		return _inBlob[x][y];
	}

	/**
	 * Determines whether this blob overlaps another blob.
	 * 
	 * @param blob
	 *            a blob
	 * @return true if this blob overlaps with the argument blob
	 */
	public boolean isOverlapping(Blob blob)
	{
		int width = Math.min(_width, blob._width);
		int height = Math.min(_height, blob._height);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (_inBlob[x][y] && blob.contains(x, y)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Merges two blobs together into a new blob.
	 * 
	 * @param blobA
	 *            a blob
	 * @param blobB
	 *            a blob
	 * @return a new blob containing all pixels from both arguments
	 */
	public static final Blob merge(Blob blobA, Blob blobB)
	{
		int width = Math.max(blobA._width, blobB._width);
		int height = Math.max(blobA._height, blobB._height);
		Blob blob = new Blob(width, height);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (blobA.contains(x, y) || blobB.contains(x, y)) {
					blob._inBlob[x][y] = true;
					blob._size++;
				}
			}
		}
		return blob;
	}

	protected static final Blob constructBlob(boolean[][] acceptedPixels,
			Point initPoint, int width, int height)
	{
		Blob blob = new Blob(width, height);
		List<Point> activeList = new ArrayList<Point>();
		activeList.add(initPoint);
		
		while (!activeList.isEmpty()) {
			Point p = activeList.remove(0);

			// Add the point to the blob
			blob._inBlob[p.x][p.y] = true;
			blob._size++;

			// Expand the blob by searching for adjacent accepted pixels
			for (int dx = -1; dx <= 1; dx++) {
				for (int dy = -1; dy <= 1; dy++) {
					int newX = p.x + dx;
					int newY = p.y + dy;
					Point newPoint = new Point(newX, newY);
					boolean in = inBounds(newX, newY, width, height);
					if (in && acceptedPixels[newX][newY]
							&& !blob._inBlob[newX][newY]
							&& !activeList.contains(newPoint)) {
						activeList.add(newPoint);
					}
				}
			}
		}
		return blob;
	}

	/**
	 * Returns true if the point p is within the bounds of the underlying image;
	 * otherwise false is returned.
	 * 
	 * @param p
	 *            a point
	 * @return true if <code>p</code> is within the bounds of the underlying
	 *         image; otherwise false
	 */
	public boolean inBounds(Point p)
	{
		return inBounds(p.x, p.y, _width, _height);
	}

	/**
	 * Returns true if the point (x, y) is within the bounds of the underlying
	 * image; otherwise false is returned.
	 * 
	 * @param x
	 *            an X-coordinate
	 * @param y
	 *            a Y-coordinate
	 * @return true if the point (x, y) is within the bounds of the underlying
	 *         image; otherwise false
	 */
	public static boolean inBounds(int x, int y, int width, int height)
	{
		return (x >= 0 && x < width && y >= 0 && y < height);
	}

	/**
	 * Returns the number of pixels in this blob.
	 * 
	 * @return the number of pixels in this blob
	 */
	public int size()
	{
		return _size;
	}

	/**
	 * Returns the width of this blob's image in pixels.
	 * 
	 * @return the width of this blob's image in pixels
	 */
	public int getImageWidth()
	{
		return _width;
	}

	/**
	 * Returns the height of this blob's image in pixels.
	 * 
	 * @return the height of this blob's image in pixels
	 */
	public int getImageHeight()
	{
		return _height;
	}

	/**
	 * Returns the ratio of area taken up by this blob compared to the total
	 * area in the image.
	 * 
	 * @return ratio of blob area to total image area
	 */
	public double areaRatio()
	{
		return _size / (double)(_width * _height);
	}

	/**
	 * Computes the average point in this blob.
	 * 
	 * @return the average point in this blob
	 */
	public Point2D average()
	{
		double xSum = 0;
		double ySum = 0;

		for (int x = 0; x < _width; x++) {
			for (int y = 0; y < _height; y++) {
				if (_inBlob[x][y]) {
					xSum += x;
					ySum += y;
				}
			}
		}

		double xMu = xSum / _size;
		double yMu = ySum / _size;
		return new Point2D.Double(xMu, yMu);
	}

	/**
	 * Constructs a masked image where all of the pixels belonging to this blob
	 * are marked by a specified mask color.
	 * 
	 * @param image
	 *            the image to mask
	 * @param maskColor
	 *            the mask color
	 * @return a masked image
	 */
	public Image maskedImage(BufferedImage image, Color maskColor)
	{
		BufferedImage maskedImage = new BufferedImage(image.getWidth(),
				image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = maskedImage.createGraphics();
		g.drawImage(image, 0, 0, null);
		g.dispose();

		int w = Math.min(_width, image.getWidth());
		int h = Math.min(_height, image.getHeight());
		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				if (_inBlob[x][y])
					maskedImage.setRGB(x, y, maskColor.getRGB());
			}
		}
		return maskedImage;
	}
}
