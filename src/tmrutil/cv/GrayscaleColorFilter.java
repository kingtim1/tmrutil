package tmrutil.cv;

import java.awt.Color;

public class GrayscaleColorFilter implements ColorFilter
{
	private double _threshold;
	
	public GrayscaleColorFilter(double threshold)
	{
		_threshold = threshold;
	}
	
	@Override
	public boolean accept(Color x)
	{	
		double avgVal = (x.getRed() + x.getGreen() + x.getBlue()) / (3 * 255.0);
		return _threshold > avgVal;
	}

}
