package tmrutil.cv;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import tmrutil.graphics.Drawable;
import tmrutil.graphics.DrawableComponent;
import tmrutil.stats.Random;

/**
 * A calligrapher environment contains an agent and an image. At each timestep
 * (depending on the pressure being applied to the surface of the canvas) the
 * agent draws on the canvas. Both the pressure applied and the velocity of the
 * agent determines the thickness and direction of painted strokes.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Calligrapher implements Drawable
{
	public static final double NO_PRESSURE = 0;
	public static final double MIN_PRESSURE = -10;
	public static final double MAX_PRESSURE = 10;

	public static final double MIN_PRESSURE_VELOCITY = -1;
	public static final double MAX_PRESSURE_VELOCITY = 1;
	public static final double MIN_XY_VELOCITY = -10;
	public static final double MAX_XY_VELOCITY = 10;

	private BufferedImage _image;
	private Point2D _agent;
	private double _pressure;

	private double _velocityX;
	private double _velocityY;
	private double _velocityP;

	/**
	 * Constructs a calligrapher environment with an empty canvas.
	 * 
	 * @param width
	 *            the canvas number of pixels wide
	 * @param height
	 *            the canvas number of pixels high
	 */
	public Calligrapher(int width, int height)
	{
		_image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		_agent = new Point2D.Double();

		reset();
	}
	
	public void step(double dvX, double dvY, double dvP)
	{
		_velocityX = clip(_velocityX + dvX, MIN_XY_VELOCITY, MAX_XY_VELOCITY);
		_velocityY = clip(_velocityY + dvY, MIN_XY_VELOCITY, MAX_XY_VELOCITY);
		_velocityP = clip(_velocityP + dvP, MIN_PRESSURE_VELOCITY, MAX_PRESSURE_VELOCITY);
		
		double prevX = _agent.getX();
		double prevY = _agent.getY();
		double prevP = _pressure;
		
		double newX = clip(_agent.getX() + _velocityX, 0, _image.getWidth());
		double newY = clip(_agent.getY() + _velocityY, 0, _image.getHeight());
		double newP = clip(_pressure + _velocityP, MIN_PRESSURE, MAX_PRESSURE);
		
		_agent.setLocation(newX, newY);
		_pressure = newP;
		
		// Draw the stroke
		if(prevP > 0 || newP > 0){
			if(prevP < 0){
				prevP = 0;
			}
			if(newP < 0){
				newP = 0;
			}
			Graphics2D g = (Graphics2D)_image.createGraphics();
			drawEllipsoid(g, new Point2D.Double(prevX, prevY), prevP, _agent, newP);
		}
	}
	
	private void drawEllipsoid(Graphics2D g, Point2D p1, double radius1, Point2D p2, double radius2)
	{
		double d1 = 2 * radius1;
		double d2 = 2 * radius2;
		Ellipse2D c1 = new Ellipse2D.Double(p1.getX() - radius1, p1.getY() - radius1, d1, d1);
		Ellipse2D c2 = new Ellipse2D.Double(p2.getX() - radius2, p2.getY() - radius2, d2, d2);
		g.setColor(Color.BLACK);
		g.fill(c1);
		g.fill(c2);
	}
	
	private double clip(double val, double min, double max)
	{
		if(val < min){
			return min;
		}else if(val > max){
			return max;
		}else{
			return val;
		}
	}

	/**
	 * Returns the current image produced by the calligrapher.
	 * 
	 * @return the current image
	 */
	public BufferedImage getImage()
	{
		return _image;
	}

	/**
	 * Resets the calligrapher's drawing to a blank slate and puts the agent in
	 * the center.
	 */
	public void reset()
	{
		int width = _image.getWidth();
		int height = _image.getHeight();
		int whiteRGB = Color.WHITE.getRGB();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				_image.setRGB(x, y, whiteRGB);
			}
		}
		_agent.setLocation(width / 2.0, height / 2.0);
		_pressure = MIN_PRESSURE;
		_velocityX = 0;
		_velocityY = 0;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		g.drawImage(_image, 0, 0, width, height, null);

		Color backup = g.getColor();
		double radius = Math.min(width, height) / 30;
		double scaleX = width / (double)_image.getWidth();
		double scaleY = height / (double)_image.getHeight();
		Ellipse2D agent = new Ellipse2D.Double((scaleX * _agent.getX()) - radius,
				(scaleY * _agent.getY()) - radius, 2 * radius, 2 * radius);
		g.setColor(Color.RED);
		if(_pressure > 0){
			g.fill(agent);
		}else{
			g.draw(agent);
		}
		g.setColor(backup);
	}
	
	public static void main(String[] args)
	{
		Calligrapher cal = new Calligrapher(200, 200);
		DrawableComponent dcomp = new DrawableComponent(cal);
		
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		frame.setSize(200, 200);
		frame.add(dcomp);
		frame.setVisible(true);
		
		while(true){
			try{
				Thread.sleep(200);
			}catch(InterruptedException ex){}
			
			double dvX = Random.uniform(MIN_XY_VELOCITY, MAX_XY_VELOCITY);
			double dvY = Random.uniform(MIN_XY_VELOCITY, MAX_XY_VELOCITY);
			double dvP = Random.uniform(MIN_PRESSURE_VELOCITY, MAX_PRESSURE_VELOCITY);
			cal.step(dvX, dvY, dvP);
			
			dcomp.repaint();
		}
	}
}
