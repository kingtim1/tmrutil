package tmrutil.crypt;

/**
 * A symbol-value pair.
 * @author Timothy A. Mann
 *
 * @param <S> the symbol type
 */
public class ValueSymbol<S> implements Comparable<ValueSymbol<?>>
{
	private double _value;
	private S _symbol;
	
	public ValueSymbol(S symbol, double value)
	{
		_symbol = symbol;
		_value = value;
	}
	
	/**
	 * Returns the value associated with this symbol.
	 * @return the value associated with this symbol
	 */
	public double value()
	{
		return _value;
	}
	
	/**
	 * Returns the underlying symbol.
	 * @return the underlying symbol
	 */
	public S symbol()
	{
		return _symbol;
	}
	
	@Override
	public int hashCode()
	{
		return new Double(_value).hashCode();
	}

	@Override
	public int compareTo(ValueSymbol<?> o)
	{
		return Double.compare(_value, o._value);
	}
	
	@Override
	public String toString()
	{
		return _symbol.toString();
	}

}
