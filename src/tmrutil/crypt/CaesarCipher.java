package tmrutil.crypt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implements ciphering and deciphering of the Caesar shift cipher.
 * @author Timothy A. Mann
 *
 */
public class CaesarCipher extends MonoAlphabeticCipher<Character,Character>
{
	
	/**
	 * Constructs a Caesar cipher where <code>shift</code> is the number of alphabet places to shift.
	 * @param shift the number of alphabet places to shift
	 */
	public CaesarCipher(int shift)
	{
		super(buildKey(shift));
	}
	
	private static Map<Character,Character> buildKey(int shift)
	{
		if(shift > 26 || shift < 0){
			throw new IllegalArgumentException("Shift must be an integer from 0 to 26.");
		}
		Map<Character,Character> cipherKey = new HashMap<Character,Character>();
		for(char i='a';i<='z';i++){
			char value = (char)(i + shift);
			if(value > 'z'){
				value = (char)('a' + ((i+shift) - 'z' - 1));
			}
			cipherKey.put(i, value);
			//System.out.println("\'" + i + "\' : " + value);
		}
		return cipherKey;
	}
	
	public void setShift(Character shift)
	{
		Character lshift = Character.toLowerCase(shift);
		int ishift = lshift - 'a';
		setShift(ishift);
	}
	
	public void setShift(int shift)
	{
		setCipherKey(buildKey(shift));
	}
	
	public List<Character> decipher(CharSequence ciphertext)
	{
		List<Character> clist = new ArrayList<Character>(ciphertext.length());
		for (int i = 0; i < ciphertext.length(); i++) {
			clist.add(ciphertext.charAt(i));
		}
		return decipher(clist);
	}
	
	public List<Character> encipher(CharSequence plaintext)
	{
		List<Character> plist = new ArrayList<Character>(plaintext.length());
		for (int i = 0; i < plaintext.length(); i++) {
			plist.add(plaintext.charAt(i));
		}
		return encipher(plist);
	}
	
	@Override
	public Character encipherSymbol(Character psymbol)
	{
		psymbol = Character.toLowerCase(psymbol);
		return super.encipherSymbol(psymbol);
	}
	
	@Override
	public Character decipherSymbol(Character csymbol)
	{
		csymbol = Character.toLowerCase(csymbol);
		return super.decipherSymbol(csymbol);
	}
}
