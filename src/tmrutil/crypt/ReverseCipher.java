package tmrutil.crypt;

/**
 * A reverse permutation cipher/decipher. This cipher reverses the sequence of symbols.
 * @author Timothy A. Mann
 *
 * @param <S> the symbol type
 */
public class ReverseCipher<S> extends PermuteCipher<S>
{

	@Override
	public int encipherPermutation(int plaintextIndex, int plaintextLength)
	{
		return (plaintextLength-1) - plaintextIndex;
	}

	@Override
	public int decipherPermutation(int ciphertextIndex, int ciphertextLength)
	{
		return (ciphertextLength-1) - ciphertextIndex;
	}

}
