package tmrutil.crypt;

import java.util.List;

/**
 * An interface for sequences of symbols over a text.
 * @author Timothy A. Mann
 *
 * @param <S> the symbol type
 */
public interface Sequence<S>
{
	/**
	 * Returns the next index given the previous index.
	 * @param prevIndex the previous index
	 * @return the next index
	 */
	public int nextIndex(int prevIndex);
	
	/**
	 * Returns the next symbol in the sequence given the complete symbol sequence and the previous index.
	 * @param text the complete text sequence
	 * @param prevIndex the previous index
	 * @return the symbol at the next index
	 */
	public S nextSymbol(List<S> text, int prevIndex);
}
