package tmrutil.crypt;


/**
 * Class that implements composition of two decipher objects.
 * @author Timothy A. Mann
 *
 * @param <PT> the plaintext type
 * @param <IT> an intermediate type
 * @param <CT> the ciphertext type
 */
public class ComposeDecipher<PT, IT, CT> implements Decipher<PT, CT>
{
	private Decipher<PT,IT> _f;
	private Decipher<IT,CT> _g;
	
	public ComposeDecipher(Decipher<PT,IT> f, Decipher<IT,CT> g)
	{
		_f = f;
		_g = g;
	}
	
	@Override
	public PT decipher(CT ciphertext)
	{
		IT intemediate = _g.decipher(ciphertext);
		PT plaintext = _f.decipher(intemediate);
		return plaintext;
	}

}
