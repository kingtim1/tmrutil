package tmrutil.crypt;

/**
 * An interface for deciphering messages.
 * @author Timothy A. Mann
 *
 * @param <PT> the plaintext type
 * @param <CT> the ciphertext type
 */
public interface Decipher<PT,CT>
{
	/**
	 * Deciphers the specified ciphertext into a plaintext form.
	 * @param ciphertext a ciphertext
	 * @return a plaintext
	 */
	public PT decipher(CT ciphertext);
}
