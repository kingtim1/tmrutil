package tmrutil.crypt;

import java.util.ArrayList;
import java.util.List;

/**
 * An abstract class for implementing permutation ciphers.
 * @author Timothy A. Mann
 *
 * @param <S> the symbol type
 */
public abstract class PermuteCipher<S> implements Cipher<Iterable<S>, List<S>>, Decipher<List<S>, Iterable<S>>
{

	@Override
	public List<S> decipher(Iterable<S> ciphertext)
	{
		List<S> clist = new ArrayList<S>();
		List<S> plist = new ArrayList<S>();
		for(S csymbol : ciphertext){
			clist.add(csymbol);
			plist.add(null);
		}
		
		for(int i=0;i<clist.size();i++){
			int pindex = decipherPermutation(i, clist.size());
			plist.set(pindex, clist.get(i));
		}
		
		return plist;
	}

	@Override
	public List<S> encipher(Iterable<S> plaintext)
	{
		List<S> plist = new ArrayList<S>();
		List<S> clist = new ArrayList<S>();
		for(S psymbol : plaintext){
			plist.add(psymbol);
			clist.add(null);
		}
		
		for(int i=0;i<plist.size();i++){
			int cindex = encipherPermutation(i, plist.size());
			clist.set(cindex, plist.get(i));
		}
		return clist;
	}

	
	public abstract int encipherPermutation(int plaintextIndex, int plaintextLength);
	
	public abstract int decipherPermutation(int ciphertextIndex, int ciphertextLength);
}
