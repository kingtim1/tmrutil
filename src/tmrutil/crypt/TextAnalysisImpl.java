package tmrutil.crypt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import tmrutil.stats.InformationTheory;

/**
 * An implementation of the text analysis interface.
 * @author Timothy A. Mann
 *
 * @param <S> the symbol type
 */
public class TextAnalysisImpl<S> implements TextAnalysis<S>
{
	private List<S> _text;
	private Set<S> _symbols;
	private Map<S,Integer> _symbolCounts;
	
	public TextAnalysisImpl(S[] text)
	{
		List<S> ltext = new ArrayList<S>(text.length);
		for(S symbol : text){
			ltext.add(symbol);
		}
		setup(ltext);
	}
	
	public TextAnalysisImpl(List<S> text)
	{
		setup(text);
	}
	
	private void setup(List<S> text)
	{
		_text = new ArrayList<S>(text);
		_symbols = new HashSet<S>();
		_symbolCounts = new HashMap<S,Integer>();
		
		for(S symbol : text){
			_symbols.add(symbol);
			
			Integer count = _symbolCounts.get(symbol);
			if(count == null){
				_symbolCounts.put(symbol, 1);
			}else{
				_symbolCounts.put(symbol, count.intValue() + 1);
			}
		}
	}
	
	@Override
	public Iterator<S> iterator()
	{
		return _text.iterator();
	}

	@Override
	public Map<S, Double> symbolFrequencies()
	{
		Map<S,Double> sfreqs = new HashMap<S,Double>();
		double tlength = size();
		for(S symbol : _symbols){
			int count = _symbolCounts.get(symbol);
			sfreqs.put(symbol, count / tlength);
		}
		return sfreqs;
	}
	
	public double conditionalSymbolEntropy(S symbol, int offset)
	{
		Map<S, Double> sfreqs = conditionalSymbolFrequencies(symbol, offset);
		return InformationTheory.entropy(sfreqs);
	}
	
	public Map<S, Double> conditionalSymbolFrequencies(S symbol, int offset)
	{
		Map<S, Integer> scounts = new HashMap<S, Integer>();
		int numSymbols = 0;
		for(int i=0;i<_text.size();i++){
			S tsymbol = _text.get(i);
			if(symbol.equals(tsymbol)){
				int offI = i + offset;
				if(offI >= 0 && offI < _text.size()){
					S newSymbol = _text.get(offI);
					Integer count = scounts.get(newSymbol);
					if(count == null){
						scounts.put(newSymbol, 1);
					}else{
						scounts.put(newSymbol, count.intValue() + 1);
					}
					numSymbols++;
				}
			}
		}
		
		Map<S, Double> sfreqs = new HashMap<S,Double>();
		for(S s : _symbols){
			Integer count = scounts.get(s);
			if(count == null){
				sfreqs.put(s, 0.0);
			}else{
				double p = count.intValue() / (double) numSymbols;
				sfreqs.put(s, p);
			}
		}
		return sfreqs;
	}

	@Override
	public Map<S, Integer> symbolCounts()
	{
		return new HashMap<S,Integer>(_symbolCounts);
	}

	@Override
	public Set<S> symbols()
	{
		return new HashSet<S>(_symbols);
	}

	@Override
	public int numberOfSymbols()
	{
		return _symbols.size();
	}

	@Override
	public int size()
	{
		return _text.size();
	}

	@Override
	public String toString()
	{
		StringBuffer buff = new StringBuffer();
		for(S symbol : _text){
			if(symbol == null){
				buff.append('_');
			}else{
				buff.append(symbol.toString());
			}
		}
		return buff.toString();
	}
	
	public static void main(String[] args){
		String textStr = "This is a test to see what happens when" +
				" I just write some stuff and then analyze the text." +
				" I'm not really sure what will happen, but I suspect " +
				"that there will be lots of structure in this text." +
				" On the other hand, there may not be much structure. " +
				"I guess it just depends. It may be more interesting if " +
				"I make sure to use all of the letters. Even the unusual" +
				" letters such as q and z. So I won't quit writing until" +
				" I have used all of the letters in the alphabet. " +
				"I saw a man at the zoo playing the xylophone. " +
				"When he saw me he turned and ran. He was as fast as a fox," +
				" but I was able to catch up with him and discovered his" +
				" secret layer. If we look hard enough we should see a very" +
				" very obvious level of structure. I don't exactly know what" +
				" I am looking for, but I hope to find it before I quit typing." +
				" There ought to be a lot of certainty about the letters that" +
				" follow q for example, because q is usually followed by u, as" +
				" in the words quickly, quietly, quoted, quotient, quiver, etc." +
				" It may be a good idea to find a long piece of text on-line. " +
				"That will make it easier to analyze the text, instead of having" +
				" to make up a stupid story or something very childish just for " +
				"the purposes of testing. Maybe I can perform some kind of analysis " +
				"on the Sherlock Holmes stories. There is probably some kind of " +
				"interesting message trapped in those stories.";
		textStr = textStr.toLowerCase();
		List<Character> text = new ArrayList<Character>(textStr.length());
		for(int i=0;i<textStr.length();i++){
			Character c = textStr.charAt(i);
			if(Character.isLetter(c)){
				text.add(c);
			}
		}
		
		TextAnalysisImpl<Character> tanalysis = new TextAnalysisImpl<Character>(text);
		System.out.println("Entropy : " + InformationTheory.entropy(tanalysis.symbolFrequencies()));
//		for(char i='a';i<='z';i++){
//			System.out.println("Entropy '" + i + "' : " + tanalysis.conditionalSymbolEntropy(i, 1));
//		}
		
		VignereCipher vcipher = new VignereCipher("testkey");
		List<Character> ciphertext = vcipher.encipher(text);
		TextAnalysisImpl<Character> canalysis = new TextAnalysisImpl<Character>(ciphertext);
		System.out.println("=======================================");
		System.out.println("Entropy : " + InformationTheory.entropy(canalysis.symbolFrequencies()));
//		for(char i='a';i<='z';i++){
//			System.out.println("Entropy '" + i + "' : " + canalysis.conditionalSymbolEntropy(i, 1));
//		}
	}
}
