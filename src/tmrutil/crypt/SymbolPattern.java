package tmrutil.crypt;

import java.util.List;

/**
 * Represents a pattern of symbols in a text.
 * @author Timothy A. Mann
 *
 * @param <S> the symbol type
 */
public interface SymbolPattern<S>
{
	/**
	 * Returns the start index of the first instance that matches this pattern. If no match is found, then -1 is returned. 
	 * @param text a sequence of symbols representing a text
	 * @param offsetIndex the offset into the text at which to begin the search
	 * @return the start index of the first instance that matches this patter; otherwise -1 (if no match is found)
	 */
	public int findNextMatchIndex(List<S> text, int offsetIndex);
	
	/**
	 * Determines if a specified word matches this pattern.
	 * @param word a sequence of symbols
	 * @return true if the word matches this pattern; otherwise false
	 */
	public boolean matches(List<S> word);
}
