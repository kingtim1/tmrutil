package tmrutil.crypt;

import java.util.Map;
import java.util.Set;

/**
 * Provides an interface for analysis on text.
 * @author Timothy A. Mann
 *
 * @param <S> the symbol type
 */
public interface TextAnalysis<S> extends Iterable<S>
{
	/**
	 * Returns a map from each symbol in this text to its frequency (a number in the interval (0, 1]).
	 * @return a map of symbol frequencies
	 */
	public Map<S,Double> symbolFrequencies();
	
	/**
	 * Returns a map from each symbol in this text to the number of times it occurred in this text.
	 * @return a map from symbols to the number of times the symbol occurred in this this text
	 */
	public Map<S,Integer> symbolCounts();
	
	/**
	 * Returns a set of all symbols contained in this text.
	 * @return a set of symbols contained in this text
	 */
	public Set<S> symbols();
	
	/**
	 * Counts the number of distinct symbols in this text.
	 * @return the number of distinct symbols in this text
	 */
	public int numberOfSymbols();
	
	/**
	 * Returns the length of this text.
	 * @return the length of this text
	 */
	public int size();
}
