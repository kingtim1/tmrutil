package tmrutil.graphics;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import tmrutil.graphics.plot2d.LinePlot;
import tmrutil.math.MatrixOps;
import tmrutil.stats.Statistics;
import tmrutil.util.GenericComparator;
import tmrutil.util.ListUtil;

/**
 * A component for displaying multidimensional real valued data and quickly and
 * easily creating plots for visualizing part or all of the data set.
 * 
 * @author Timothy Mann
 * 
 */
public class DataAnalysisTable extends JTable
{
	private static class SumAction extends AbstractAction
	{
		private static final long serialVersionUID = -7278595979765636854L;
		private DataAnalysisTable _table;

		public SumAction(DataAnalysisTable table)
		{
			super("Sum");
			_table = table;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			double value = Double.NaN;
			if (selection.length > 0) {
				value = Statistics.sum(Statistics.sum(selection));
			}
			JOptionPane.showMessageDialog(_table, "SUM(SELECTION) = " + value,
					"SUM", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private static class MaxAction extends AbstractAction
	{
		private static final long serialVersionUID = -9211670724265592249L;
		private DataAnalysisTable _table;

		public MaxAction(DataAnalysisTable table)
		{
			super("Max");
			_table = table;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			double value = Double.NaN;
			if (selection.length > 0) {
				value = Statistics.max(selection);
			}
			JOptionPane.showMessageDialog(_table, "MAX(SELECTION) = " + value,
					"MAX", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private static class MinAction extends AbstractAction
	{
		private static final long serialVersionUID = 6234779449781323377L;
		private DataAnalysisTable _table;

		public MinAction(DataAnalysisTable table)
		{
			super("Min");
			_table = table;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			double value = Double.NaN;
			if (selection.length > 0) {
				value = Statistics.min(selection);
			}
			JOptionPane.showMessageDialog(_table, "MIN(SELECTION) = " + value,
					"MIN", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private static class MeanAction extends AbstractAction
	{
		private static final long serialVersionUID = 712179826819479716L;
		private DataAnalysisTable _table;

		public MeanAction(DataAnalysisTable table)
		{
			super("Mean");
			_table = table;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			double value = Double.NaN;
			if (selection.length > 0) {
				value = Statistics.sum(Statistics.sum(selection))
						/ (MatrixOps.getNumRows(selection) * MatrixOps.getNumCols(selection));
			}
			JOptionPane.showMessageDialog(_table, "MEAN(SELECTION) = " + value,
					"MEAN", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private static class MedianAction extends AbstractAction
	{
		private static final long serialVersionUID = -3154524196616600565L;
		private DataAnalysisTable _table;

		public MedianAction(DataAnalysisTable table)
		{
			super("Median");
			_table = table;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			double value = Double.NaN;
			if (selection.length > 0) {
				int numRows = MatrixOps.getNumRows(selection);
				int numCols = MatrixOps.getNumCols(selection);
				double[] samples = new double[numRows * numCols];
				for (int i = 0; i < numRows; i++) {
					for (int j = 0; j < numCols; j++) {
						samples[(i * numCols) + j] = selection[i][j];
					}
				}
				value = Statistics.median(samples);
			}
			JOptionPane.showMessageDialog(_table, "MEDIAN(SELECTION) = "
					+ value, "MEDIAN", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private static class StdAction extends AbstractAction
	{
		private static final long serialVersionUID = 1338276979162014680L;
		private DataAnalysisTable _table;

		public StdAction(DataAnalysisTable table)
		{
			super("Standard Deviation");
			_table = table;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			double value = Double.NaN;
			if (selection.length > 0) {
				int numRows = MatrixOps.getNumRows(selection);
				int numCols = MatrixOps.getNumCols(selection);
				double[] samples = new double[numRows * numCols];
				for (int i = 0; i < numRows; i++) {
					for (int j = 0; j < numCols; j++) {
						samples[(i * numCols) + j] = selection[i][j];
					}
				}
				value = Statistics.std(samples);
			}
			JOptionPane.showMessageDialog(_table, "STD(SELECTION) = " + value,
					"STD", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private static class HistogramAction extends AbstractAction
	{
		private static final long serialVersionUID = 3874098081459679014L;
		private DataAnalysisTable _table;
		private int _bins;

		public HistogramAction(DataAnalysisTable table, int bins)
		{
			super(String.valueOf(bins));
			_table = table;
			_bins = bins;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			if (selection.length > 0) {
				int numRows = MatrixOps.getNumRows(selection);
				int numCols = MatrixOps.getNumCols(selection);
				double[] samples = new double[numRows * numCols];
				for (int i = 0; i < numRows; i++) {
					for (int j = 0; j < numCols; j++) {
						samples[(i * numCols) + j] = selection[i][j];
					}
				}
				tmrutil.graphics.plot2d.Plot2D.histogram(samples, _bins,
						java.awt.Color.RED, "Histogram", "X", "Y");
			} else {
				JOptionPane.showMessageDialog(_table,
						"Insufficient data selected.", "HISTOGRAM WARNING",
						JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private static class LinePlotAction extends AbstractAction
	{
		private static final long serialVersionUID = -2343265983990404387L;

		private enum MultipleRowColumnPolicy {
			MEAN_OF_ROWS, MEAN_OF_COLUMNS, SEPARATE_ROWS, SEPARATE_COLUMNS;

			public String getName()
			{
				switch (this) {
				case MEAN_OF_ROWS:
					return "Mean of Rows";
				case MEAN_OF_COLUMNS:
					return "Mean of Columns";
				case SEPARATE_ROWS:
					return "Separate Rows";
				case SEPARATE_COLUMNS:
					return "Separate Columns";
				default:
					return "Undefined";
				}
			}
		};

		private DataAnalysisTable _table;
		private MultipleRowColumnPolicy _policy;

		public LinePlotAction(DataAnalysisTable table,
				MultipleRowColumnPolicy policy)
		{
			super(policy.getName());
			_table = table;
			_policy = policy;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			if (selection.length > 0) {
				int numRows = MatrixOps.getNumRows(selection);
				int numCols = MatrixOps.getNumCols(selection);
				if (numRows == 1 || numCols == 1) {
					double[] y = new double[Math.max(numRows, numCols)];
					for (int i = 0; i < y.length; i++) {
						if (numCols > numRows) {
							y[i] = selection[0][i];
						} else {
							y[i] = selection[i][0];
						}
					}
					tmrutil.graphics.plot2d.Plot2D.linePlot(ListUtil.toList(y),
							java.awt.Color.RED, "Line Plot", "X", "Y");
				} else {
					if (_policy.equals(MultipleRowColumnPolicy.MEAN_OF_ROWS)) {
						double[] y = Statistics.columnMean(selection);
						tmrutil.graphics.plot2d.Plot2D.linePlot(
								ListUtil.toList(y), java.awt.Color.RED,
								"Line Plot (MEAN OF ROWS)", "X", "Y");
					}
					if (_policy.equals(MultipleRowColumnPolicy.MEAN_OF_COLUMNS)) {
						double[] y = Statistics.columnMean(MatrixOps.transpose(selection));
						tmrutil.graphics.plot2d.Plot2D.linePlot(
								ListUtil.toList(y), java.awt.Color.RED,
								"Line Plot (MEAN OF COLUMNS)", "X", "Y");
					}
					if (_policy.equals(MultipleRowColumnPolicy.SEPARATE_ROWS)) {
						LinePlot plot = tmrutil.graphics.plot2d.Plot2D.linePlot(
								ListUtil.toList(selection[0]), COLORS[0],
								"Line Plot (SEPARATE ROWS)", "X", "Y");
						for (int i = 1; i < selection.length; i++) {
							List<double[]> data = new ArrayList<double[]>(
									selection[i].length);
							for (int j = 0; j < selection[i].length; j++) {
								data.add(new double[] { j, selection[i][j] });
							}
							plot.addData(data, COLORS[i % COLORS.length],
									"Row " + i);
						}
					}
					if (_policy.equals(MultipleRowColumnPolicy.SEPARATE_COLUMNS)) {
						selection = MatrixOps.transpose(selection);
						LinePlot plot = tmrutil.graphics.plot2d.Plot2D.linePlot(
								ListUtil.toList(selection[0]), COLORS[0],
								"Line Plot (SEPARATE COLUMNS)", "X", "Y");
						for (int i = 1; i < selection.length; i++) {
							List<double[]> data = new ArrayList<double[]>(
									selection[i].length);
							for (int j = 0; j < selection[i].length; j++) {
								data.add(new double[] { j, selection[i][j] });
							}
							plot.addData(data, COLORS[i % COLORS.length],
									"Row " + i);
						}
					}
				}
			} else {
				JOptionPane.showMessageDialog(_table,
						"Insufficient data selected.", "LINE PLOT WARNING",
						JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
	private class MatrixDisplayAction extends AbstractAction
	{
		private static final long serialVersionUID = -679825284921134596L;
		private DataAnalysisTable _table;
		
		public MatrixDisplayAction(DataAnalysisTable table)
		{
			super("Matrix Display");
			_table = table;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			double[][] selection = _table.getSelection();
			MatrixOps.displayGraphically(selection, "Matrix Display");
		}
	}

	private static final long serialVersionUID = -1212431885012379946L;

	private static final Color[] COLORS = { Color.RED, Color.GREEN, Color.BLUE,
			Color.CYAN, Color.DARK_GRAY, Color.ORANGE, Color.MAGENTA,
			Color.YELLOW, Color.PINK, Color.WHITE, Color.BLACK };

	private JPopupMenu _menu;
	private JMenu _plotMenu;
	private JMenu _statisticsMenu;
	private JMenu _visualizeMenu;

	/**
	 * Constructs a data analysis table with a specified data set and labels for
	 * each of the columns.
	 * 
	 * @param data
	 *            a row data set containing real values
	 * @param columnNames
	 *            labels for each column of the data set
	 */
	public DataAnalysisTable(double[][] data, String[] columnNames)
	{
		super();
		Object[][] odata = new Object[data.length][columnNames.length];
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				odata[i][j] = new Double(data[i][j]);
			}
		}

		Object[] cNames = new Object[columnNames.length];
		for (int i = 0; i < columnNames.length; i++) {
			cNames[i] = columnNames[i];
		}

		DefaultTableModel tableModel = new DefaultTableModel(odata, cNames);
		TableRowSorter<DefaultTableModel> rowSorter = new TableRowSorter<DefaultTableModel>(
				tableModel);
		GenericComparator<Double> doubleComparator = new GenericComparator<Double>();
		for (int i = 0; i < columnNames.length; i++) {
			rowSorter.setComparator(i, doubleComparator);
		}

		setColumnSelectionAllowed(true);
		setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

		setModel(tableModel);
		setRowSorter(rowSorter);

		_menu = new JPopupMenu("DataAnalysisMenu");
		_plotMenu = new JMenu("Plot");
		_menu.add(_plotMenu);
		_statisticsMenu = new JMenu("Statistics");
		_menu.add(_statisticsMenu);
		_visualizeMenu = new JMenu("Visualization");
		_menu.add(_visualizeMenu);
		
		JMenu linePlotMenu = new JMenu("Line Plot");
		linePlotMenu.add(new LinePlotAction(this,
				LinePlotAction.MultipleRowColumnPolicy.MEAN_OF_ROWS));
		linePlotMenu.add(new LinePlotAction(this,
				LinePlotAction.MultipleRowColumnPolicy.MEAN_OF_COLUMNS));
		linePlotMenu.add(new LinePlotAction(this,
				LinePlotAction.MultipleRowColumnPolicy.SEPARATE_ROWS));
		linePlotMenu.add(new LinePlotAction(this,
				LinePlotAction.MultipleRowColumnPolicy.SEPARATE_COLUMNS));
		_plotMenu.add(linePlotMenu);
		JMenu scatterPlotMenu = new JMenu("Scatter Plot");
		_plotMenu.add(scatterPlotMenu);
		JMenu histogramMenu = new JMenu("Histogram");
		histogramMenu.add(new HistogramAction(this, 5));
		histogramMenu.add(new HistogramAction(this, 10));
		histogramMenu.add(new HistogramAction(this, 15));
		histogramMenu.add(new HistogramAction(this, 50));
		histogramMenu.add(new HistogramAction(this, 100));
		_plotMenu.add(histogramMenu);

		_statisticsMenu.add(new MaxAction(this));
		_statisticsMenu.add(new MeanAction(this));
		_statisticsMenu.add(new MedianAction(this));
		_statisticsMenu.add(new MinAction(this));
		_statisticsMenu.add(new StdAction(this));
		_statisticsMenu.add(new SumAction(this));

		_visualizeMenu.add(new MatrixDisplayAction(this));
		
		setComponentPopupMenu(_menu);
	}

	/**
	 * Returns the selected region of the table as a real valued data matrix.
	 * 
	 * @return the selected region of the table as a real valued data matrix
	 */
	public double[][] getSelection()
	{
		int[] rows = getSelectedRows();
		int[] cols = getSelectedColumns();
		double[][] selection = new double[rows.length][cols.length];
		for (int i = 0; i < rows.length; i++) {
			for (int j = 0; j < cols.length; j++) {
				selection[i][j] = (Double) getValueAt(rows[i], cols[j]);
			}
		}
		return selection;
	}

	public static final void displayTable(double[][] data, String[] columnNames)
	{
		DataAnalysisTable table = new DataAnalysisTable(data, columnNames);
		JScrollPane scroller = new JScrollPane(table);

		JFrame frame = new JFrame("DataAnalysisTable");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.add(scroller);
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args)
	{
		String[] labels = { "T0", "T1", "T2", "T3", "T4", "T5", "T6" };
		double[][] mat = MatrixOps.random(10, labels.length);

		displayTable(mat, labels);
	}

}
