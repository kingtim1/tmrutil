/*
 * Histogram.java
 */

/* Package */
package tmrutil.graphics.plot2d;

/* Imports */
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.graphics.ContinuousAxis;
import tmrutil.stats.Statistics;
import tmrutil.util.DebugException;

/**
 * Plots a 2D histogram given a data set and number of bins.
 * 
 * @author Timothy Mann
 * 
 */
public class Histogram extends Plot2D
{

	private static final long serialVersionUID = -2218515959050799031L;

	/**
	 * The amount of of additional space to provide in the Y-axis when
	 * automatically resizing.
	 */
	private static final double Y_ADJUSTMENT = 0.1;

	/** Reference to a data set. */
	private double[] _data;
	/** Color of the histogram bars. */
	private Color _color;
	/** Description of the data. */
	private String _description;

	/**
	 * A list containing the number of elements in each bin.
	 */
	private List<Integer> _binCounts;

	/** The number of bins. */
	private int _bins;

	public void setNumberOfBins(int bins)
	{
		ContinuousAxis xaxis = (ContinuousAxis) _xaxis;
		ContinuousAxis yaxis = (ContinuousAxis) _yaxis;
		_bins = bins;
		_binCounts.clear();
		Arrays.sort(_data);
		double min = Statistics.min(_data);
		double max = Statistics.max(_data);
		xaxis.setRange(min, max);

		double inc = (max - min) / bins;
		int ind = 0;
		for (int i = 0; i < bins; i++) {
			double binMin = min + (i * inc);
			int count = 0;
			while (ind < _data.length && _data[ind] <= binMin + inc) {
				ind++;
				count++;
			}
			_binCounts.add(count);
		}
		if (_bins != _binCounts.size()) {
			throw new DebugException(
					"When setting the number of bins, the number of bins specified "
							+ "is not the same as the number of bins counted. This should never happen!");
		}
		int maxBin = (int)Statistics.max(_binCounts);
		yaxis.setRange(0, maxBin + (maxBin * Y_ADJUSTMENT));
	}

	public int getNumberOfBins()
	{
		return _bins;
	}

	/**
	 * Constructs a 2D histogram from a set of observations.
	 * 
	 * @param data
	 *            a set of observations
	 * @param bins
	 *            the number of bins in the histogram
	 * @param color
	 *            the color of the histogram
	 * @param description
	 *            a description of the data
	 */
	public Histogram(double[] data, int bins, Color color, String description)
	{
		super();
		_xaxis = new ContinuousAxis("");
		_yaxis = new ContinuousAxis("");
		_data = Arrays.copyOf(data, data.length);
		_color = color;
		_description = description;
		_binCounts = new ArrayList<Integer>(bins);
		setNumberOfBins(bins);
	}

	@Override
	public void paintPlot(Graphics g2, int width, int height)
	{
		ContinuousAxis xaxis = (ContinuousAxis) _xaxis;
		ContinuousAxis yaxis = (ContinuousAxis) _yaxis;
		DecimalFormat df = DEFAULT_DECIMAL_FORMAT;

		// Paint the title
		int titleWidth = (int) (width * TITLE_X);
		int titleHeight = (int) (height * TITLE_Y);
		String title = getTitle();
		FontMetrics fm = g2.getFontMetrics();
		Rectangle2D titleRect = fm.getStringBounds(title, g2);
		g2.setColor(Color.BLACK);
		g2.drawString(title, (int) (titleWidth / 2 - titleRect.getWidth() / 2),
				(int) titleRect.getHeight());

		// Paint the axes
		// X axis first
		int xAxisX = (int) ((1 - XAXIS_X) * width);
		int xAxisY = (int) ((1 - XAXIS_Y) * height);
		int xAxisWidth = width - xAxisX;
		String xlabel = _xaxis.getLabel();
		Rectangle2D xlabelRect = fm.getStringBounds(xlabel, g2);
		g2.drawString(xlabel, xAxisX
				+ (int) (xAxisWidth / 2 - xlabelRect.getWidth() / 2), height);
		List<Double> ticks = xaxis.getTicks();
		for (int i = 0; i < ticks.size(); i++) {
			int x = xaxis.compute2DCoord(ticks.get(i), xAxisWidth, xAxisX,
					false);
			int y = xAxisY;
			String tick = df.format(ticks.get(i));
			Rectangle2D tickRect = fm.getStringBounds(tick, g2);
			g2.drawString(tick, (int) (x - (tickRect.getWidth() / 2)),
					(int) (y + tickRect.getHeight()));
		}

		// Y axis
		int yAxisY = titleHeight;
		int yAxisWidth = (int) (width * YAXIS_X);
		int yAxisHeight = height - titleHeight - (int) (height * XAXIS_Y);
		String ylabel = _yaxis.getLabel();
		Rectangle2D ylabelRect = fm.getStringBounds(ylabel, g2);
		try {
			Graphics2D g2tmp = (Graphics2D) g2.create();
			g2tmp.transform(AffineTransform.getRotateInstance(-Math.PI / 2));
			g2tmp.drawString(ylabel, -(yAxisHeight + titleHeight)
					+ (int) (yAxisHeight / 2 - ylabelRect.getWidth() / 2),
					(int) (ylabelRect.getHeight()));
			g2tmp.dispose();
		} catch (ClassCastException ex) {
			System.err.println("Warning : Cannot write vertical text.");
		}
		ticks = yaxis.getTicks();
		for (int i = 0; i < ticks.size(); i++) {
			int x = yAxisWidth;
			int y = yaxis.compute2DCoord(ticks.get(i), yAxisHeight,
					titleHeight, true);
			String tick = df.format(ticks.get(i));
			Rectangle2D tickRect = fm.getStringBounds(tick, g2);
			g2.drawString(tick, (int) (x - tickRect.getWidth()),
					(int) (y + tickRect.getHeight() / 2));
		}

		g2.setClip(xAxisX, yAxisY, xAxisWidth, yAxisHeight);

		double max = Statistics.max(_data);
		double min = Statistics.min(_data);
		double inc = (max - min) / _bins;
		double bottom = min;
		for (Integer count : _binCounts) {
			int x = xaxis.compute2DCoord(bottom, xAxisWidth, xAxisX, false);
			int y = yaxis.compute2DCoord(count, yAxisHeight, yAxisY, true);
			int w = xaxis.compute2DCoord(bottom + inc, xAxisWidth, xAxisX,
					false)
					- x;
			int h = yaxis.compute2DCoord(0, yAxisHeight, yAxisY, true) - y;
			g2.setColor(_color);
			g2.fillRect(x, y, w - 1, h - 1);
			g2.setColor(Color.BLACK);
			g2.drawRect(x, y, w - 1, h - 1);
			bottom += inc;
		}
		g2.setColor(Color.BLACK);
		g2.drawRect(xAxisX, yAxisY, xAxisWidth - 1, yAxisHeight - 1);
	}
	
	/**
	 * Returns a description of this histogram.
	 * @return a description of this histogram
	 */
	public String getDescription()
	{
		return _description;
	}
}