/*
 * Plot2D.java
 */

/* Package */
package tmrutil.graphics.plot2d;

/* Imports */
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import tmrutil.graphics.Axis;
import tmrutil.graphics.DataProperties;
import tmrutil.graphics.NullAxis;
import tmrutil.graphics.Plot;
import tmrutil.util.ListUtil;

/**
 * Represents a 2D graphical plot and contains helper functions for easily
 * plotting data with one of the supported plot types.
 */
public abstract class Plot2D extends Plot
{
	private static final long serialVersionUID = 904081099141208507L;

	public static final double XAXIS_X = 0.9;
	public static final double XAXIS_Y = 0.1;
	public static final double YAXIS_X = 0.1;
	public static final double YAXIS_Y = 0.83;

	/** The font used for rendering axis labels. */
	private Font _axisLabelFont;
	/** The font used for rendering marks on an axis. */
	private Font _axisMarkFont;

	/** The X-axis. */
	protected Axis _xaxis;
	/** The Y-axis. */
	protected Axis _yaxis;

	public Plot2D()
	{
		super();
		_xaxis = new NullAxis();
		_yaxis = new NullAxis();
		_axisLabelFont = getPlotFont();
		_axisMarkFont = getPlotFont();

		JPopupMenu menu = new JPopupMenu("Plot Menu");
		JMenu labels = new JMenu("Labels");
		labels.add(new SetTitleAction(this));
		labels.add(new SetXLabelAction(this));
		labels.add(new SetYLabelAction(this));
		menu.add(labels);
		JMenu axes = new JMenu("Axes");
		menu.add(axes);
		JMenu print = new JMenu("Print");
		print.add(new EPSPrintAction(this));
		print.add(new PDFPrintAction(this));
		print.add(new PSPrintAction(this));
		menu.add(print);
		JMenu font = new JMenu("Font");
		menu.add(font);
		JMenu fontStyle = new JMenu("Style");
		font.add(fontStyle);
		fontStyle.add(new AdjustFontStyleAction(this, Font.PLAIN));
		fontStyle.add(new AdjustFontStyleAction(this, Font.BOLD));
		fontStyle.add(new AdjustFontStyleAction(this, Font.ITALIC));
		JMenu fontSize = new JMenu("Size");
		font.add(fontSize);
		fontSize.add(new AdjustFontSizeAction(this, 9));
		fontSize.add(new AdjustFontSizeAction(this, 10));
		fontSize.add(new AdjustFontSizeAction(this, 11));
		fontSize.add(new AdjustFontSizeAction(this, 12));
		fontSize.add(new AdjustFontSizeAction(this, 13));
		fontSize.add(new AdjustFontSizeAction(this, 14));
		fontSize.add(new AdjustFontSizeAction(this, 15));
		fontSize.add(new AdjustFontSizeAction(this, 16));
		fontSize.add(new AdjustFontSizeAction(this, 17));
		fontSize.add(new AdjustFontSizeAction(this, 18));
		fontSize.add(new AdjustFontSizeAction(this, 19));
		fontSize.add(new AdjustFontSizeAction(this, 20));
		fontSize.add(new AdjustFontSizeAction(this, 21));
		fontSize.add(new AdjustFontSizeAction(this, 22));
		fontSize.add(new AdjustFontSizeAction(this, 23));
		fontSize.add(new AdjustFontSizeAction(this, 24));
		fontSize.add(new AdjustFontSizeAction(this, 25));
		menu.add(new ReplotAction(this));

		setComponentPopupMenu(menu);
	}

	/**
	 * Set the X axis label to specified string.
	 * 
	 * @param label
	 *            a label
	 */
	public void setXLabel(String label)
	{
		_xaxis.setLabel(label);
	}

	/**
	 * Set the Y axis label to a specified string.
	 * 
	 * @param label
	 *            a label
	 */
	public void setYLabel(String label)
	{
		_yaxis.setLabel(label);
	}

	/**
	 * Returns the font used to render the axis labels.
	 * 
	 * @return the font used to render the axis labels
	 */
	public Font getAxisLabelFont()
	{
		return _axisLabelFont;
	}

	/**
	 * Sets the font used to render the axis labels.
	 * 
	 * @param font
	 *            a font
	 */
	public void setAxisLabelFont(Font font)
	{
		_axisLabelFont = font;
	}

	/**
	 * Returns the font used to render the axis marks.
	 * 
	 * @return the font used to render the axis marks
	 */
	public Font getAxisMarkFont()
	{
		return _axisMarkFont;
	}

	/**
	 * Sets the font used to render the axis marks.
	 * 
	 * @param font
	 *            a font
	 */
	public void setAxisMarkFont(Font font)
	{
		_axisMarkFont = font;
	}

	/**
	 * Constructs a frame and a line plot displaying curves representing the
	 * mean and plus/minus standard deviation. The line plot is added to the
	 * constructed frame and made visible. When the frame is closed, the frame
	 * is disposed of so that the frame does not stall termination of the AWT
	 * thread (and as a result termination of the program).
	 * <p/>
	 * 
	 * The constructed line plot instance is returned so that additional data
	 * can be plotted to it.
	 * 
	 * @param x
	 *            a list of X-coordinates
	 * @param avg
	 *            a list of mean estimates corresponding to each X-coordinate
	 * @param std
	 *            a list of standard deviations estimates corresponding to each
	 *            X-coordinate
	 * @param title
	 *            the plot title
	 * @param xlabel
	 *            the X-axis label
	 * @param ylabel
	 *            the Y-axis label
	 * @return a LinePlot object
	 */
	public static final LinePlot errorPlot(List<Double> x, List<Double> avg,
			List<Double> std, String title, String xlabel, String ylabel)
	{
		LinePlot plot = linePlot(x, avg, Color.BLUE, title, xlabel, ylabel);
		List<double[]> stdAbove = new ArrayList<double[]>(avg.size());
		List<double[]> stdBelow = new ArrayList<double[]>(avg.size());
		for (int i = 0; i < avg.size(); i++) {
			double[] above = new double[2];
			double[] below = new double[2];
			above[0] = x.get(i);
			above[1] = avg.get(i) + std.get(i);
			below[0] = x.get(i);
			below[1] = avg.get(i) - std.get(i);
			stdAbove.add(above);
			stdBelow.add(below);
		}
		plot.addData(new DataProperties(stdAbove,
				DataProperties.DisplayStyle.DASHED, Color.MAGENTA,
				"1 std above the mean"));
		plot.addData(new DataProperties(stdBelow,
				DataProperties.DisplayStyle.DASHED, Color.MAGENTA,
				"1 std below the mean"));
		return plot;
	}

	public static final StreamingLinePlot streamingLinePlot(int maxPoints,
			String title, String xlabel, String ylabel)
	{
		StreamingLinePlot plot = new StreamingLinePlot(maxPoints);
		plot.setTitle(title);
		plot.setXLabel(xlabel);
		plot.setYLabel(ylabel);

		javax.swing.JFrame f = new javax.swing.JFrame("StreamingLinePlot : "
				+ title);
		f.add(plot);
		f.setSize(DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		f.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
		f.setVisible(true);

		return plot;
	}

	public static final LinePlot linePlot(List<Double> y, Color color,
			String title, String xlabel, String ylabel)
	{
		double[][] data = new double[y.size()][2];
		for (int i = 0; i < data.length; i++) {
			data[i][0] = i;
			data[i][1] = y.get(i);
		}
		return linePlot(data, color, title, xlabel, ylabel);
	}

	/**
	 * Create a frame and adds a line plot that displays a single line curve with a custom color.
	 * <p/>
	 * 
	 * The constructed line plot instance is returned so that additional data
	 * can be plotted to it.
	 * 
	 * @param x a list of X-coordinates
	 * @param y a list of Y-coordinates corresponding to each X-coordinate
	 * @param color the color of the curve
	 * @param title the plot title string
	 * @param xlabel the X-coordinate label
	 * @param ylabel the Y-coordinate label
	 * @return a LinePlot object
	 */
	public static final LinePlot linePlot(List<Double> x, List<Double> y,
			Color color, String title, String xlabel, String ylabel)
	{
		double[][] data = new double[x.size()][2];
		for (int i = 0; i < data.length; i++) {
			data[i][0] = x.get(i);
			data[i][1] = y.get(i);
		}
		return linePlot(data, color, title, xlabel, ylabel);
	}

	/**
	 * Create a frame and adds a line plot that displays a single line curve with a custom color.
	 * <p/>
	 * 
	 * The constructed line plot instance is returned so that additional data
	 * can be plotted to it.
	 * 
	 * @param x an array of X-coordinates
	 * @param y an array of Y-coordinates corresponding to each X-coordinate
	 * @param color the color of the curve
	 * @param title the plot title string
	 * @param xlabel the X-coordinate label
	 * @param ylabel the Y-coordinate label
	 * @return a LinePlot object
	 */
	public static final LinePlot linePlot(double[] x, double[] y, Color color,
			String title, String xlabel, String ylabel)
	{
		double[][] data = new double[x.length][2];
		for (int i = 0; i < data.length; i++) {
			data[i][0] = x[i];
			data[i][1] = y[i];
		}
		return linePlot(data, color, title, xlabel, ylabel);
	}

	public static final LinePlot linePlot(List<Double> x1, List<Double> y1,
			Color color1, List<Double> x2, List<Double> y2, Color color2,
			String title, String xlabel, String ylabel)
	{
		return linePlot(ListUtil.toArray(x1), ListUtil.toArray(y1), color1,
				ListUtil.toArray(x2), ListUtil.toArray(y2), color2, title,
				xlabel, ylabel);
	}

	public static final LinePlot linePlot(double[] x1, double[] y1,
			Color color1, double[] x2, double[] y2, Color color2, String title,
			String xlabel, String ylabel)
	{
		double[][] data1 = new double[x1.length][2];
		for (int i = 0; i < data1.length; i++) {
			data1[i][0] = x1[i];
			data1[i][1] = y1[i];
		}
		double[][] data2 = new double[x2.length][2];
		for (int i = 0; i < data2.length; i++) {
			data2[i][0] = x2[i];
			data2[i][1] = y2[i];
		}
		return linePlot(data1, color1, data2, color2, title, xlabel, ylabel);
	}

	public static final LinePlot linePlot(double[][] data, Color color,
			String title, String xlabel, String ylabel)
	{
		List<double[]> ldata = new ArrayList<double[]>(data.length);
		for (int i = 0; i < data.length; i++) {
			ldata.add(data[i]);
		}

		LinePlot lp = new LinePlot();
		lp.addData(ldata, DataProperties.DisplayStyle.LINE, color, "Data");
		lp.setTitle(title);
		lp.setXLabel(xlabel);
		lp.setYLabel(ylabel);

		javax.swing.JFrame f = new javax.swing.JFrame("LinePlot : " + title);
		f.add(lp);
		f.setSize(DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		f.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
		f.setVisible(true);

		return lp;
	}

	public static final LinePlot linePlot(double[][] data1, Color color1,
			double[][] data2, Color color2, String title, String xlabel,
			String ylabel)
	{
		List<double[]> ldata1 = new ArrayList<double[]>(data1.length);
		for (int i = 0; i < data1.length; i++) {
			ldata1.add(data1[i]);
		}
		List<double[]> ldata2 = new ArrayList<double[]>(data2.length);
		for (int i = 0; i < data2.length; i++) {
			ldata2.add(data2[i]);
		}

		LinePlot lp = new LinePlot();
		lp.addData(ldata1, DataProperties.DisplayStyle.LINE, color1, "Data1");
		lp.addData(ldata2, DataProperties.DisplayStyle.LINE, color2, "Data2");
		lp.setTitle(title);
		lp.setXLabel(xlabel);
		lp.setYLabel(ylabel);

		javax.swing.JFrame f = new javax.swing.JFrame("LinePlot : " + title);
		f.add(lp);
		f.setSize(DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		f.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
		f.setVisible(true);

		return lp;
	}

	public static final LinePlot scatterPlot(double[][] data, Color color,
			String title, String xlabel, String ylabel)
	{
		List<double[]> ldata = new ArrayList<double[]>(data.length);
		for (int i = 0; i < data.length; i++) {
			ldata.add(data[i]);
		}

		LinePlot lp = new LinePlot();
		lp.addData(ldata, DataProperties.DisplayStyle.POINT, color, "Data");
		lp.setTitle(title);
		lp.setXLabel(xlabel);
		lp.setYLabel(ylabel);

		javax.swing.JFrame f = new javax.swing.JFrame("LinePlot : " + title);
		f.add(lp);
		f.setSize(DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		f.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
		f.setVisible(true);

		return lp;
	}

	public static final LinePlot scatterPlot(double[][] data1, Color color1,
			double[][] data2, Color color2, String title, String xlabel,
			String ylabel)
	{
		List<double[]> ldata1 = new ArrayList<double[]>(data1.length);
		for (int i = 0; i < data1.length; i++) {
			ldata1.add(data1[i]);
		}
		List<double[]> ldata2 = new ArrayList<double[]>(data2.length);
		for (int i = 0; i < data2.length; i++) {
			ldata2.add(data2[i]);
		}

		LinePlot lp = new LinePlot();
		lp.addData(ldata1, DataProperties.DisplayStyle.POINT, color1, "Data1");
		lp.addData(ldata2, DataProperties.DisplayStyle.POINT, color2, "Data2");
		lp.setTitle(title);
		lp.setXLabel(xlabel);
		lp.setYLabel(ylabel);

		javax.swing.JFrame f = new javax.swing.JFrame("LinePlot : " + title);
		f.add(lp);
		f.setSize(DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		f.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
		f.setVisible(true);

		return lp;
	}

	public static final Histogram histogram(double[] data, int bins,
			Color color, String title, String xlabel, String ylabel)
	{

		Histogram hist = new Histogram(data, bins, color, "None");
		hist.setTitle(title);
		hist.setXLabel(xlabel);
		hist.setYLabel(ylabel);

		javax.swing.JFrame f = new javax.swing.JFrame("Histogram : " + title);
		f.add(hist);
		f.setSize(DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		f.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
		f.setVisible(true);

		return hist;
	}

	private static class SetTitleAction extends AbstractAction
	{
		private static final long serialVersionUID = -3459784152341938212L;
		private Plot2D _plot2d;

		public SetTitleAction(Plot2D plot2d)
		{
			super("Set Title");
			_plot2d = plot2d;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			String title = JOptionPane.showInputDialog(_plot2d,
					"Enter title : ");
			_plot2d.setTitle(title);
			_plot2d.repaint();
		}
	}

	private static class SetXLabelAction extends AbstractAction
	{
		private static final long serialVersionUID = 245594694629502564L;
		private Plot2D _plot2d;

		public SetXLabelAction(Plot2D plot2d)
		{
			super("Set X Label");
			_plot2d = plot2d;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			String xlabel = JOptionPane.showInputDialog(_plot2d,
					"Enter X label : ");
			_plot2d.setXLabel(xlabel);
			_plot2d.repaint();
		}
	}

	private static class SetYLabelAction extends AbstractAction
	{
		private static final long serialVersionUID = 6331407862219395355L;
		private Plot2D _plot2d;

		public SetYLabelAction(Plot2D plot2d)
		{
			super("Set Y Label");
			_plot2d = plot2d;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			String ylabel = JOptionPane.showInputDialog(_plot2d,
					"Enter Y label : ");
			_plot2d.setYLabel(ylabel);
			_plot2d.repaint();
		}
	}

	private static class ReplotAction extends AbstractAction
	{
		private Plot2D _plot2d;

		public ReplotAction(Plot2D plot2d)
		{
			super("Replot");
			_plot2d = plot2d;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			_plot2d.repaint();
		}
	}

	private static class PSPrintAction extends AbstractAction
	{
		private static final long serialVersionUID = 1250916213618674489L;
		private Plot2D _plot2d;

		public PSPrintAction(Plot2D plot2d)
		{
			super("Print to PS");
			_plot2d = plot2d;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			JFileChooser chooser = new JFileChooser(".");
			int ans = chooser.showSaveDialog(_plot2d);
			if (ans == JFileChooser.APPROVE_OPTION) {
				try {
					_plot2d.toPS(chooser.getSelectedFile().getAbsolutePath());
				} catch (IOException ex) {
					JOptionPane.showMessageDialog(_plot2d,
							"Error printing plot to file!", "File I/O Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private static class PDFPrintAction extends AbstractAction
	{
		private Plot2D _plot2d;

		public PDFPrintAction(Plot2D plot2d)
		{
			super("Print to PDF");
			_plot2d = plot2d;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			JFileChooser chooser = new JFileChooser(".");
			int ans = chooser.showSaveDialog(_plot2d);
			if (ans == JFileChooser.APPROVE_OPTION) {
				try {
					_plot2d.toPDF(chooser.getSelectedFile().getAbsolutePath());
				} catch (IOException ex) {
					JOptionPane.showMessageDialog(_plot2d,
							"Error printing plot to file!", "File I/O Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private static class EPSPrintAction extends AbstractAction
	{
		private Plot2D _plot2d;

		public EPSPrintAction(Plot2D plot2d)
		{
			super("Print to EPS");
			_plot2d = plot2d;
		}

		public void actionPerformed(ActionEvent e)
		{
			JFileChooser chooser = new JFileChooser(".");
			int ans = chooser.showSaveDialog(_plot2d);
			if (ans == JFileChooser.APPROVE_OPTION) {
				try {
					_plot2d.toEPS(chooser.getSelectedFile().getAbsolutePath());
				} catch (IOException ex) {
					JOptionPane.showMessageDialog(_plot2d,
							"Error printing plot to file!", "File I/O Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	private static class AdjustFontSizeAction extends AbstractAction
	{
		private Plot2D _plot2d;
		private int _size;

		public AdjustFontSizeAction(Plot2D plot2d, int size)
		{
			super(String.valueOf(size));
			_plot2d = plot2d;
			_size = size;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			Font font = _plot2d.getPlotFont();
			Font newFont = new Font(font.getFontName(), font.getStyle(), _size);
			_plot2d.setPlotFont(newFont);
			_plot2d.repaint();
		}
	}

	private static class AdjustFontStyleAction extends AbstractAction
	{
		private Plot2D _plot2d;
		private int _style;

		public AdjustFontStyleAction(Plot2D plot2d, int style)
		{
			super(getStyleString(style));
			_plot2d = plot2d;
			_style = style;
		}

		public static String getStyleString(int style)
		{
			if (style == Font.BOLD) {
				return "Bold";
			} else if (style == Font.ITALIC) {
				return "Italic";
			} else {
				return "Plain";
			}
		}

		public void actionPerformed(ActionEvent e)
		{
			Font font = _plot2d.getPlotFont();
			Font newFont = new Font(font.getFontName(), _style, font.getSize());
			_plot2d.setPlotFont(newFont);
			_plot2d.repaint();
		}
	}
}