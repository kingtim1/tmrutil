package tmrutil.graphics.plot2d;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import tmrutil.graphics.ContinuousAxis;
import tmrutil.graphics.DataProperties;

public class StreamingLinePlot extends Plot2D
{

	private static final long serialVersionUID = 6385546763011841862L;

	private static final double Y_ADJUSTMENT = 0.1;

	private List<Double> _data;
	private int _maxPoints;
	private boolean _autoResize;

	public StreamingLinePlot(int maxPoints) throws IllegalArgumentException
	{
		super();
		if (maxPoints < 1) {
			throw new IllegalArgumentException(
					"The maximum number of points displayable by a streaming line plot must be a positive integer.");
		}
		_data = new ArrayList<Double>(maxPoints);
		_maxPoints = maxPoints;
		_xaxis = new ContinuousAxis("");
		((ContinuousAxis) _xaxis).setRange(0, maxPoints);
		_yaxis = new ContinuousAxis("");
		_autoResize = true;
		updateAxes();
	}

	private void updateAxes()
	{
		double ymin = Double.MAX_VALUE, ymax = Double.MIN_VALUE;
		synchronized (_data) {
			List<Double> ds = _data;
			for (int i = 0; i < ds.size(); i++) {
				double d = ds.get(i);
				if (d < ymin) {
					ymin = d;
				}
				if (d > ymax) {
					ymax = d;
				}
			}
			if (ds.size() == 0) {
				ymin = 0;
				ymax = 1;
			}
		}
		double ydiff = ymax - ymin;
		setYAxis(ymin - Y_ADJUSTMENT * ydiff, ymax + Y_ADJUSTMENT * ydiff);
		repaint();
	}

	public void setYAxis(double min, double max)
	{
		((ContinuousAxis) _yaxis).setRange(min, max);
	}

	public boolean isAutoResize()
	{
		return _autoResize;
	}

	public void setAutoResize(boolean autoResize)
	{
		_autoResize = autoResize;
	}

	public void addPoint(double y)
	{
		synchronized (_data) {
			_data.add(y);
			if (_data.size() > _maxPoints) {
				_data.remove(0);
			}
			updateAxes();
			repaint();
		}
	}

	@Override
	public void paintPlot(Graphics g2, int width, int height)
	{
		ContinuousAxis xaxis = (ContinuousAxis) _xaxis;
		ContinuousAxis yaxis = (ContinuousAxis) _yaxis;
		DecimalFormat df = DEFAULT_DECIMAL_FORMAT;

		// Paint the title
		int titleWidth = (int) (width * TITLE_X);
		int titleHeight = (int) (height * TITLE_Y);
		String title = getTitle();
		FontMetrics fm = g2.getFontMetrics();
		Rectangle2D titleRect = fm.getStringBounds(title, g2);
		g2.setColor(Color.BLACK);
		g2.drawString(title, (int) (titleWidth / 2 - titleRect.getWidth() / 2),
				(int) titleRect.getHeight());

		// Paint the axes
		// X axis first
		int xAxisX = (int) ((1 - XAXIS_X) * width);
		int xAxisY = (int) ((1 - XAXIS_Y) * height);
		int xAxisWidth = width - xAxisX;
		String xlabel = _xaxis.getLabel();
		Rectangle2D xlabelRect = fm.getStringBounds(xlabel, g2);
		g2.drawString(xlabel, xAxisX
				+ (int) (xAxisWidth / 2 - xlabelRect.getWidth() / 2), height);
		List<Double> ticks = xaxis.getTicks();
		for (int i = 0; i < ticks.size(); i++) {
			int x = xaxis.compute2DCoord(ticks.get(i), xAxisWidth, xAxisX,
					false);
			int y = xAxisY;
			String tick = df.format(ticks.get(i));
			Rectangle2D tickRect = fm.getStringBounds(tick, g2);
			g2.drawString(tick, (int) (x - (tickRect.getWidth() / 2)),
					(int) (y + tickRect.getHeight()));
		}

		// Y axis
		int yAxisY = titleHeight;
		int yAxisWidth = (int) (width * YAXIS_X);
		int yAxisHeight = height - titleHeight - (int) (height * XAXIS_Y);
		String ylabel = _yaxis.getLabel();
		Rectangle2D ylabelRect = fm.getStringBounds(ylabel, g2);
		try {
			Graphics2D g2tmp = (Graphics2D) g2.create();
			g2tmp.transform(AffineTransform.getRotateInstance(-Math.PI / 2));
			g2tmp.drawString(ylabel, -(yAxisHeight + titleHeight)
					+ (int) (yAxisHeight / 2 - ylabelRect.getWidth() / 2),
					(int) (ylabelRect.getHeight()));
			g2tmp.dispose();
		} catch (ClassCastException ex) {
			System.err.println("Warning : Cannot write vertical text.");
		}
		ticks = yaxis.getTicks();
		for (int i = 0; i < ticks.size(); i++) {
			int x = yAxisWidth;
			int y = yaxis.compute2DCoord(ticks.get(i), yAxisHeight,
					titleHeight, true);
			String tick = df.format(ticks.get(i));
			Rectangle2D tickRect = fm.getStringBounds(tick, g2);
			g2.drawString(tick, (int) (x - tickRect.getWidth()),
					(int) (y + tickRect.getHeight() / 2));
		}

		g2.setClip(xAxisX, yAxisY, xAxisWidth, yAxisHeight);

		// Paint the plot
		g2.setColor(Color.RED);
		synchronized (_data) {
			List<Double> ds = _data;
			for (int i = 1; i < ds.size(); i++) {
				double p1 = ds.get(i - 1);
				double p2 = ds.get(i);
				int x1 = xaxis.compute2DCoord(i - 1, xAxisWidth, xAxisX, false);
				int y1 = yaxis.compute2DCoord(p1, yAxisHeight, yAxisY, true);
				int x2 = xaxis.compute2DCoord(i, xAxisWidth, xAxisX, false);
				int y2 = yaxis.compute2DCoord(p2, yAxisHeight, yAxisY, true);
				g2.drawLine(x1, y1, x2, y2);
			}
		}

		g2.setColor(Color.BLACK);
		g2.drawRect(xAxisX, yAxisY, xAxisWidth - 1, yAxisHeight - 1);
	}

}
