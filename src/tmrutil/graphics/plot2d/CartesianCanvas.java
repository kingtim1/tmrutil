package tmrutil.graphics.plot2d;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.border.LineBorder;
import tmrutil.graphics.ContinuousAxis;
import tmrutil.graphics.DataProperties;
import tmrutil.util.RealDataSet;

/**
 * A canvas for displaying a rectangular region of a 2D plane where points are
 * expressed in Cartesian coordinates.
 * 
 * @author Timothy Mann
 * 
 */
public class CartesianCanvas extends JComponent
{
	private static final long serialVersionUID = 6122241272399956500L;

	private static final int X = 0;
	private static final int Y = 1;
	
	private ContinuousAxis _xaxis;
	private ContinuousAxis _yaxis;
	
	private List<RealDataSet> _dataSets;
	private Map<RealDataSet,DataProperties> _properties;
	
	public CartesianCanvas(ContinuousAxis xaxis, ContinuousAxis yaxis)
	{
		_dataSets = new ArrayList<RealDataSet>();
		_xaxis = xaxis;
		_yaxis = yaxis;
		setBorder(new LineBorder(Color.BLACK));
	}
	
	private int getX(double x)
	{
		return _xaxis.compute2DCoord(x, getWidth(), 0, false);
	}
	
	private int getY(double y)
	{
		return _yaxis.compute2DCoord(y, getHeight(), 0, true);
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		for(RealDataSet dataSet : _dataSets)
		{
			DataProperties properties = _properties.get(dataSet);
			switch(properties.getStyle()){
			case LINE:
				drawLines(g2, dataSet, properties);
				break;
			case POINT:
				drawPoints(g2, dataSet, properties);
				break;
			case DOTTED:
				drawDotted(g2, dataSet, properties);
				break;
			case DASHED:
				drawDashed(g2, dataSet, properties);
				break;
			case TRIANGLE_LINE:
				drawTriangleLines(g2, dataSet, properties);
				break;
			case SQUARE_LINE:
				drawSquareLines(g2, dataSet, properties);
				break;
			case CIRCLE_LINE:
				drawCircleLines(g2, dataSet, properties);
				break;
			}
		}
		g.dispose();
	}
	
	private void drawLines(Graphics2D g2, RealDataSet data, DataProperties properties)
	{
		int lineWidth = properties.getLineWidth();
		
		g2.setStroke(new BasicStroke(lineWidth));
		g2.setColor(properties.getColor());
		for(int i=1;i<data.size();i++){
			double[] pointA = data.get(i-1);
			double[] pointB = data.get(i);
			g2.drawLine(getX(pointA[X]), getY(pointA[Y]), getX(pointB[X]), getY(pointB[Y]));
		}
	}
	
	private void drawPoints(Graphics2D g2, RealDataSet data, DataProperties properties)
	{
		int pointWidth = properties.getPointWidth();
		int halfPointWidth = pointWidth / 2;
		
		g2.setColor(properties.getColor());
		for(double[] point : data)
		{
			g2.fillOval(getX(point[X])-halfPointWidth, getY(point[Y])-halfPointWidth, pointWidth, pointWidth);
		}
	}
	
	private void drawDotted(Graphics2D g2, RealDataSet data, DataProperties properties)
	{
		// TODO implement drawing dotted lines
	}
	
	private void drawDashed(Graphics2D g2, RealDataSet data, DataProperties properties)
	{
		// TODO implement drawing dashed lines
	}
	
	private void drawTriangleLines(Graphics2D g2, RealDataSet data, DataProperties properties)
	{
		// TODO implement drawing triangle lines
	}
	
	private void drawSquareLines(Graphics2D g2, RealDataSet data, DataProperties properties)
	{
		// TODO implement drawing square lines
	}
	
	private void drawCircleLines(Graphics2D g2, RealDataSet data, DataProperties properties)
	{
		// TODO implement drawing circle lines
	}
	
	/**
	 * Adds a data set to this Cartesian canvas.
	 * @param dataSet a 2-dimensional data set
	 * @param properties properties for rendering the data
	 */
	public void add(RealDataSet dataSet, DataProperties properties)
	{
		if(!_dataSets.contains(dataSet)){
			_dataSets.add(dataSet);
			_properties.put(dataSet, properties);
		}
	}
	
	/**
	 * Removes a data set from this Cartesian canvas.
	 * @param dataSet a 2-dimensional data set
	 */
	public void remove(RealDataSet dataSet)
	{
		if(_dataSets.contains(dataSet)){
			_dataSets.remove(dataSet);
			_properties.remove(dataSet);
		}
	}
}
