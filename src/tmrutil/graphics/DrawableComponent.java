package tmrutil.graphics;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Destination;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import tmrutil.util.TextFileUtil;

/**
 * A component for rendering drawable objects.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DrawableComponent extends JComponent
{
	private static final long serialVersionUID = 518307029354163704L;

	/**
	 * A reference to a drawable object.
	 */
	private List<Drawable> _drawables;

	/**
	 * Constructs a drawable component that references a null drawable object.
	 * Nothing is painted.
	 */
	public DrawableComponent()
	{
		this(null);
	}

	/**
	 * Constructs a drawable component that references the specified drawable
	 * object. The specified drawable object is rendered.
	 * 
	 * @param drawable a drawable object
	 */
	public DrawableComponent(Drawable drawable)
	{
		_drawables = new ArrayList<Drawable>();
		setDrawable(drawable);
		
		setupUI();
	}
	
	private void setupUI()
	{
		JPopupMenu menu = new JPopupMenu();
		JMenuItem saveToPS = new JMenuItem("Save to PS");
		
		final JComponent parent = this;
		
		saveToPS.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				JFileChooser chooser = new JFileChooser();
				int response = chooser.showSaveDialog(parent);
				if(response == JFileChooser.APPROVE_OPTION){
					String filename = chooser.getSelectedFile().getPath();
					try{
						toPS(filename, getWidth(), getHeight());
					}catch(IOException ex){
						JOptionPane.showMessageDialog(parent, "An I/O error occurred while trying to save the postscript file : " + ex.getMessage());
					}
				}
			}
			
		});
		
		menu.add(saveToPS);
		setComponentPopupMenu(menu);
	}

	/**
	 * Set the drawable object. If the specified object is null nothing is rendered.
	 * @param drawable a drawable object or null
	 */
	public void setDrawable(Drawable drawable)
	{
		_drawables.clear();
		if(drawable != null){
			_drawables.add(drawable);
		}
	}
	
	/**
	 * Adds a drawable to the list of drawable objects rendered by this component.
	 * @param drawable 
	 */
	public void addDrawable(Drawable drawable)
	{
		_drawables.add(drawable);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g.create();
		for(int i=0;i<_drawables.size();i++){
			Drawable drawable = _drawables.get(i);
			drawable.draw(g2, getWidth(), getHeight());
		}
		g2.dispose();
	}
	
	public void toPS(String filename, int width, int height) throws IOException
	{
		double llx = 72;
		double lly = (72 * 11) - (72 + height);
		double urx = llx + width;
		double ury = lly + height;
		
		toPS(filename, width, height, llx, lly, urx, ury);
	}
	
	public void toPS(String filename, final int width, final int height, double llx, double lly, double urx, double ury) throws IOException
	{
		Printable printable = new Printable() {
			public int print(Graphics g, PageFormat format, int pageIndex)
			{
				if (pageIndex == 0) {
					Graphics2D g2 = (Graphics2D) g;
					g2.translate(format.getImageableX(), format.getImageableY());
					g2.setClip(0, 0, width, height);

					for(int i=0;i<_drawables.size();i++){
						Drawable drawable = _drawables.get(i);
						drawable.draw(g2, width, height);
					}
					return Printable.PAGE_EXISTS;
				}
				return Printable.NO_SUCH_PAGE;
			}
		};

		PageFormat format = new PageFormat();

		Destination dest = new Destination(new File(filename).toURI());
		PrintRequestAttributeSet aSet = new HashPrintRequestAttributeSet();
		aSet.add(dest);
		PrinterJob pJob = PrinterJob.getPrinterJob();
		pJob.setPrintable(printable, format);
		try {
			// This writes the PostScript to a file
			pJob.print(aSet);

			// Add bounding box information which is necessary for embedding the
			// PostScript in Latex documents.
			// The arguments for the bounding box are :
			// bottom left x, bottom left y, top right x, top right y
			TextFileUtil.insertLineIntoFile("%%BoundingBox: " + llx + " " + lly + " " + urx + " " + ury
					/* "%%BoundingBox: 70 360 545 720" */, 1,
					filename);
			// We also have to remove instances of setpagedevice so that it
			// works with Latex
			TextFileUtil.findReplace("setpagedevice", "% setpagedevice",
					filename);

		} catch (PrinterException ex) {
			System.err.println("A printer exception occurred!");
			ex.printStackTrace();
		}
	}
}
