package tmrutil.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import javax.swing.JComponent;
import tmrutil.learning.AbstractSelfOrganizingMap;
import tmrutil.learning.SelfOrganizingMap;

/**
 * A component for visualizing the structure of 1D and 2D self organizing (i.e.
 * Kohonen) maps.
 * 
 * @author Timothy Mann
 * 
 */
public class SOMViz extends JComponent
{
	private static final long serialVersionUID = -7575852322941360010L;

	private static final int POINT_SIZE = 4;
	private static final int X = 1;
	private static final int Y = 0;

	private int[] _gridProperties;
	private List<double[]> _weights;

	/**
	 * Constructs a component for visualizing a 1D or 2D SOM.
	 * 
	 * @param weights
	 *            a set of weights
	 * @param gridProperties
	 *            each integer in the array represents a dimension and the
	 *            number of neurons in the grid along that dimension
	 */
	public SOMViz(List<double[]> weights, int[] gridProperties)
	{
		_weights = weights;
		_gridProperties = gridProperties;
	}

	private double findMax(int d)
	{
		double max = _weights.get(0)[d];
		for (int i = 1; i < _weights.size(); i++) {
			double val = _weights.get(i)[d];
			if (max < val) {
				max = val;
			}
		}
		return max;
	}

	private double findMin(int d)
	{
		double min = _weights.get(0)[d];
		for (int i = 1; i < _weights.size(); i++) {
			double val = _weights.get(i)[d];
			if (min > val) {
				min = val;
			}
		}
		return min;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		paintPlot(g, getWidth(), getHeight());
	}

	public void paintPlot(Graphics g, int width, int height)
	{
		double minX = findMin(X);
		double maxX = findMax(X);
		double minY = findMin(Y);
		double maxY = findMax(Y);

		double diffX = maxX - minX;
		double diffY = maxY - minY;

		Color backupColor = g.getColor();
		if (_gridProperties.length == 1 || _gridProperties.length == 2) {
			g.setColor(Color.BLUE);
			
			//
			// Draw all of the points
			//
			for (int i = 0; i < _weights.size(); i++) {
				double[] w = _weights.get(i);
				g.fillOval(toComponentCoord(w[X], diffX, minX, width) - (POINT_SIZE/2),
						toComponentCoord(w[Y], diffY, minY, height) - (POINT_SIZE/2),
						POINT_SIZE, POINT_SIZE);
			}

			// Draw the edges connecting the SOM
			if (_gridProperties.length == 1) {
				// 1D Drawing
				for (int i = 1; i < _weights.size(); i++) {
					double[] p = _weights.get(i - 1);
					double[] c = _weights.get(i);
					g.drawLine(toComponentCoord(p[X], diffX, minX, width),
							toComponentCoord(p[Y], diffY, minY, height),
							toComponentCoord(c[X], diffX, minX, width),
							toComponentCoord(c[Y], diffY, minY, height));
				}
			} else {
				// 2D Drawing
				int[] coord = new int[2];
				for(int i=0;i<_gridProperties[X];i++){
					for(int j=0;j<_gridProperties[Y];j++){
						coord[X] = i;
						coord[Y] = j;
						double[] p = _weights.get(AbstractSelfOrganizingMap.coordToIndex(coord, _gridProperties));
						if(i-1 > 0){
							coord[X] = i-1;
							coord[Y] = j;
							double[] c = _weights.get(AbstractSelfOrganizingMap.coordToIndex(coord, _gridProperties));
							g.drawLine(toComponentCoord(p[X], diffX, minX, width),
									toComponentCoord(p[Y], diffY, minY, height),
									toComponentCoord(c[X], diffX, minX, width),
									toComponentCoord(c[Y], diffY, minY, height));
						}
						if(i+1 < _gridProperties[X]){
							coord[X] = i+1;
							coord[Y] = j;
							double[] c = _weights.get(AbstractSelfOrganizingMap.coordToIndex(coord, _gridProperties));
							g.drawLine(toComponentCoord(p[X], diffX, minX, width),
									toComponentCoord(p[Y], diffY, minY, height),
									toComponentCoord(c[X], diffX, minX, width),
									toComponentCoord(c[Y], diffY, minY, height));
						}
						if(j-1 > 0){
							coord[X] = i;
							coord[Y] = j-1;
							double[] c = _weights.get(AbstractSelfOrganizingMap.coordToIndex(coord, _gridProperties));
							g.drawLine(toComponentCoord(p[X], diffX, minX, width),
									toComponentCoord(p[Y], diffY, minY, height),
									toComponentCoord(c[X], diffX, minX, width),
									toComponentCoord(c[Y], diffY, minY, height));
						}
						if(j+1 < _gridProperties[Y]){
							coord[X] = i;
							coord[Y] = j+1;
							double[] c = _weights.get(AbstractSelfOrganizingMap.coordToIndex(coord, _gridProperties));
							g.drawLine(toComponentCoord(p[X], diffX, minX, width),
									toComponentCoord(p[Y], diffY, minY, height),
									toComponentCoord(c[X], diffX, minX, width),
									toComponentCoord(c[Y], diffY, minY, height));
						}
					}
				}
			}

		} else {
			g.setColor(Color.BLACK);
			g.drawString(
					"Warning : Cannot paint SOM with more than 2 dimensions",
					0, 0);
		}
		g.setColor(backupColor);
	}

	private int toComponentCoord(double v, double axisDiff, double axisMin,
			int axisSize)
	{
		int buffer = (int) (axisSize * 0.1);
		double ratio = (v - axisMin) / axisDiff;
		return (buffer / 2) + (int) (ratio * (axisSize - buffer));
	}
}
