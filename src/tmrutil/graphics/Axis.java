/*
 * Axis.java
 */

/* Package */
package tmrutil.graphics;

import java.util.ArrayList;
import java.util.List;

/**
 * A continuous or discrete axis used in a plot.
 */
public abstract class Axis
{
	private String _label;
	private List<AxisChangeListener> _listeners;

	public Axis(String label)
	{
		_label = label;
		_listeners = new ArrayList<AxisChangeListener>();
	}

	/**
	 * Sets the label to a specified value.
	 * @param label a new label
	 */
	public void setLabel(String label)
	{
		_label = label;
		axisChanged();
	}

	/**
	 * Returns the axis' label.
	 * @return the label
	 */
	public String getLabel()
	{
		return _label;
	}
	
	/**
	 * Adds an axis change listener to this axis.
	 * @param listener an axis change listener
	 */
	public void addAxisChangeListener(AxisChangeListener listener)
	{
		_listeners.add(listener);
	}
	
	/**
	 * Removes a specified axis change listener from this axis.
	 * @param listener an axis change listener
	 */
	public void removeAxisChangeListener(AxisChangeListener listener)
	{
		_listeners.remove(listener);
	}

	/**
	 * This method should be called by methods that modify the state of an axis.
	 */
	protected void axisChanged()
	{
		for (AxisChangeListener l : _listeners) {
			l.axisChanged(this);
		}
	}
}