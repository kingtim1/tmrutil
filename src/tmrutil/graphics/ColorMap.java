package tmrutil.graphics;

import java.awt.Color;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.geom.Point2D;
import tmrutil.util.Interval;

/**
 * An enumeration of color maps that can be used to visualize the value of data
 * scaled between minimum and maximum values.
 * 
 * @author Timothy A. Mann
 * 
 */
public enum ColorMap {
	GRAYSCALE, HEAT, RED_BLUE, RED_GREEN_BLUE;
	
	private float[] _fractions;
	private Color[] _colors;
	
	static{
		GRAYSCALE._fractions = new float[]{0f, 1f};
		GRAYSCALE._colors = new Color[]{Color.BLACK, Color.WHITE};
		
		HEAT._fractions = new float[]{0f, 0.3f, 0.6f, 1.f};
		HEAT._colors = new Color[]{Color.BLUE, Color.RED, Color.YELLOW, Color.WHITE};
		
		RED_BLUE._fractions = new float[]{0f, 1f};
		RED_BLUE._colors = new Color[]{new Color(0f,0f,0.5f), Color.RED};
		
		RED_GREEN_BLUE._fractions = new float[]{0f,0.5f,1f};
		RED_GREEN_BLUE._colors = new Color[]{Color.BLUE, Color.GREEN, Color.RED};
	}

	/**
	 * Maps a scalar value to a color where the values are scaled between the
	 * range. If the specified value is outside of the provided range, then the
	 * value is clipped to within the range and then assigned a color.
	 * 
	 * @param val
	 *            a scalar value to be mapped to a color
	 * @param range
	 *            the minimum and maximum scalar values represented by color
	 * @return a color
	 */
	public Color map(double val, Interval range)
	{
		float normVal = (float)((val - range.getMin()) / (range.getMax() - range.getMin()));
		return linearGradient(normVal, this._fractions, this._colors);
	}
	
	/**
	 * Gets the gradient paint for this color map.
	 * @param p1 the first point
	 * @param p2 the second point
	 * @return a paint instance
	 */
	public Paint getPaint(Point2D p1, Point2D p2)
	{
		return new LinearGradientPaint(p1, p2, this._fractions, this._colors);
	}
	
	/**
	 * Finds a color given a value scaled between 0 and 1.
	 * @param val a value scaled between 0 and 1
	 * @param fractions an array containing a range of fractions in ascending order between 0 and 1
	 * @param colors an array of colors
	 * @return a color
	 */
	private Color linearGradient(float val, float[] fractions, Color[] colors)
	{
		if(val < fractions[0]){
			return colors[0];
		}
		
		for(int i=1;i<fractions.length;i++){
			if(val < fractions[i]){
				float diff = fractions[i] - fractions[i-1];
				float coeff = (val - fractions[i-1])/diff;
				float[] upper = colors[i].getComponents(null);
				float[] lower = colors[i-1].getComponents(null);
				
				float r = (1-coeff) * lower[0] + (coeff) * upper[0];
				float g = (1-coeff) * lower[1] + (coeff) * upper[1];
				float b = (1-coeff) * lower[2] + (coeff) * upper[2];
				
				return new Color(r, g, b);
			}
		}
		
		int end = fractions.length - 1;
		return colors[end];
	}
}
