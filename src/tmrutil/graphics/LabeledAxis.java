package tmrutil.graphics;

import java.util.List;

public class LabeledAxis extends Axis
{
	private List<String> _labels;
	
	public LabeledAxis(String label, List<String> labels)
	{
		super(label);
		_labels = labels;
	}
	
	public List<String> getLabels()
	{
		return _labels;
	}
}
