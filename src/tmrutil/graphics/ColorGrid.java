package tmrutil.graphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JComponent;
import javax.swing.JFrame;

public class ColorGrid extends JComponent implements Drawable
{
	private Color[] _colorKey;
	private int[][] _grid;
	
	public ColorGrid(int[][] grid, Color[] colorKey)
	{
		_grid = grid;
		_colorKey = colorKey;
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g.create();
		
		draw(g2, getWidth(), getHeight());
		
		g2.dispose();
	}
	
	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		int pwidth = width;
		int pheight = height;
		
		int cheight = pheight / _grid.length;
		int cwidth = pwidth / _grid[0].length;
		
		for(int i=0;i<_grid.length;i++){
			for(int j=0;j<_grid[i].length;j++){
				g.setColor(_colorKey[_grid[i][j]]);
				Rectangle2D rect = new Rectangle2D.Double(cwidth * j, cheight * i, cwidth * (j+1), cheight * (i+1));
				g.fill(rect);
			}
		}
		
		g.setColor(Color.BLACK);
		g.setStroke(new BasicStroke(2));
		g.drawRect(0, 0, pwidth, pheight);
	}
	
	public static final void showColorGrid(int[][] grid, Color[] colorKey)
	{
		JFrame frame = new JFrame("Color Grid");
		frame.add(new ColorGrid(grid, colorKey));
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(100, 100);
		
		frame.setVisible(true);
		//frame.repaint();
	}
}
