package tmrutil.graphics;

import java.util.List;

/**
 * Represents a legend in a plot.
 * @author Timothy Mann
 *
 */
public class Legend
{
	/**
	 * True if this legend is painted on the plot.
	 */
	private boolean _visible;
	
	/**
	 * A list of data sets.
	 */
	private List<DataProperties> _dataSets;
	
	/**
	 * Constructs a legend from a collection of data sets.
	 * @param dataSets a collection of data sets
	 */
	public Legend(List<DataProperties> dataSets){
		_dataSets = dataSets;
	}
	
	/**
	 * Adds a data set to the legend.
	 * @param data a data set
	 */
	public void add(DataProperties data)
	{
		_dataSets.add(data);
	}
	
	/**
	 * Determines if this legend is painted on the plot.
	 * @param visible true if the legend should be painted
	 */
	public void setVisible(boolean visible)
	{
		_visible = visible;
	}
	
	/**
	 * True if the legend is visible.
	 */
	public boolean isVisible()
	{
		return _visible;
	}
}
