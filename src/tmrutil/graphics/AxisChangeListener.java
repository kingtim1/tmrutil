package tmrutil.graphics;

/**
 * Implemented by classes that listen for changes to an axis.
 * @author Timothy Mann
 */
public interface AxisChangeListener
{
	/**
	 * Called when an axis is changed.
	 * @param axis the axis that changed
	 */
	public void axisChanged(Axis axis);
}
