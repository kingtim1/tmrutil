package tmrutil.graphics.plotx;

import java.awt.Color;
import java.awt.Font;

/**
 * A list of properties associated with a plot.
 * @author Timothy A. Mann
 *
 */
public class PlotProperties
{
	public static final Font DEFAULT_TITLE_FONT = new Font(Font.SANS_SERIF, Font.BOLD, 18);
	public static final Font DEFAULT_XLABEL_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 14);
	public static final Font DEFAULT_YLABEL_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 14);
	public static final Font DEFAULT_XTICK_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
	public static final Font DEFAULT_YTICK_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
	
	public static final Color DEFAULT_TITLE_COLOR = Color.BLACK;
	public static final Color DEFAULT_XLABEL_COLOR = Color.BLACK;
	public static final Color DEFAULT_YLABEL_COLOR = Color.BLACK;
	public static final Color DEFAULT_XTICK_COLOR = Color.BLACK;
	public static final Color DEFAULT_YTICK_COLOR = Color.BLACK;
	
	private Font _titleFont;
	private Color _titleColor;
	private Font _xLabelFont;
	private Color _xLabelColor;
	private Font _yLabelFont;
	private Color _yLabelColor;
	private Font _xTickFont;
	private Color _xTickColor;
	private Font _yTickFont;
	private Color _yTickColor;
	
	/**
	 * Constructs a plot properties instance with default values.
	 */
	public PlotProperties()
	{
		_titleFont = DEFAULT_TITLE_FONT;
		_titleColor = DEFAULT_TITLE_COLOR;
		_xLabelFont = DEFAULT_XLABEL_FONT;
		_xLabelColor = DEFAULT_XLABEL_COLOR;
		_yLabelFont = DEFAULT_YLABEL_FONT;
		_yLabelColor = DEFAULT_YLABEL_COLOR;
		_xTickFont = DEFAULT_XTICK_FONT;
		_xTickColor = DEFAULT_XTICK_COLOR;
		_yTickFont = DEFAULT_YTICK_FONT;
		_yTickColor = DEFAULT_YTICK_COLOR;
	}
	
	/**
	 * Returns the title font.
	 * @return a font
	 */
	public Font getTitleFont()
	{
		return _titleFont;
	}
	
	/**
	 * Return the title color.
	 * @return a color
	 */
	public Color getTitleColor()
	{
		return _titleColor;
	}
	
	/**
	 * Returns the X-axis label font.
	 * @return a font
	 */
	public Font getXLabelFont()
	{
		return _xLabelFont;
	}
	
	/**
	 * Return the X-axis label color.
	 * @return a color
	 */
	public Color getXLabelColor()
	{
		return _xLabelColor;
	}
	
	/**
	 * Returns the Y-axis label font.
	 * @return a font
	 */
	public Font getYLabelFont()
	{
		return _yLabelFont;
	}
	
	/**
	 * Return the Y-axis label color.
	 * @return a color
	 */
	public Color getYLabelColor()
	{
		return _yLabelColor;
	}
	
	/**
	 * Returns the X-axis tick label font.
	 * @return a font
	 */
	public Font getXTickFont()
	{
		return _xTickFont;
	}
	
	/**
	 * Returns the X-axis tick label color.
	 * @return a color
	 */
	public Color getXTickColor()
	{
		return _xTickColor;
	}
	
	/**
	 * Returns the Y-axis tick label font.
	 * @return a font
	 */
	public Font getYTickFont()
	{
		return _yTickFont;
	}
	
	/**
	 * Returns the Y-axis tick label color.
	 * @return a color
	 */
	public Color getYTickColor()
	{
		return _yTickColor;
	}
}
