package tmrutil.graphics.plotx;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Represents a 2-dimensional axis.
 * @author Timothy A. Mann
 *
 */
public class Axis
{
	private Rectangle2D _rect;
	
	/**
	 * Constructs a default axis instance.
	 */
	public Axis()
	{
		this(new Rectangle2D.Double(0, 0, 1, 1));
	}
	
	/**
	 * Constructs an axis instance from a rectangle.
	 * @param rect a rectangle
	 */
	public Axis(Rectangle2D rect)
	{
		_rect = new Rectangle2D.Double(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
	}
	
	/**
	 * Sets the limits of the X-axis.
	 * @param xmin a scalar value less than xmax
	 * @param xmax a scalar value
	 */
	public void setXLim(double xmin, double xmax)
	{
		double width = xmax - xmin;
		if(width <= 0){
			throw new IllegalArgumentException("X minimum must be smaller than X maximum.");
		}
		_rect = new Rectangle2D.Double(xmin, _rect.getY(), width, _rect.getHeight());
	}
	
	/**
	 * Sets the limits of the Y-axis.
	 * @param ymin a scalar value less than ymax
	 * @param ymax a scalar value
	 */
	public void setYLim(double ymin, double ymax)
	{
		double height = ymax - ymin;
		if(height <= 0){
			throw new IllegalArgumentException("Y minimum must be smaller than Y maximum.");
		}
		_rect = new Rectangle2D.Double(_rect.getX(), ymin, _rect.getWidth(), height);
	}
	
	public Point transformDataToPixel(Point2D dataPoint, Rectangle2D axisRect, Rectangle2D componentRect)
	{
		double axisPixelOffsetX = axisRect.getX() * componentRect.getWidth();
		double axisPixelOffsetY = axisRect.getY() * componentRect.getHeight();
		double axisPixelWidth = axisRect.getWidth() * componentRect.getWidth();
		double axisPixelHeight = axisRect.getHeight() * componentRect.getHeight();
		
		double x = axisPixelOffsetX + (((dataPoint.getX()-_rect.getX())/_rect.getWidth()) * axisPixelWidth);
		double y = axisPixelHeight - (axisPixelOffsetY + (((dataPoint.getY()-_rect.getY())/_rect.getHeight()) * axisPixelHeight));
		
		return new Point((int)x, (int)y);
	}
	
	public Rectangle transformDataToPixel(Rectangle2D dataRect, Rectangle2D axisRect, Rectangle2D componentRect)
	{
		Point2D lowerLeft = new Point2D.Double(dataRect.getX(), dataRect.getY());
		Point2D upperRight = new Point2D.Double(dataRect.getX() + dataRect.getWidth(), dataRect.getY() + dataRect.getHeight());
		Point llp = transformDataToPixel(lowerLeft, axisRect, componentRect);
		Point urp = transformDataToPixel(upperRight, axisRect, componentRect);
		
		return new Rectangle(llp.x, urp.y, urp.x - llp.x, llp.y - urp.y);
	}
	
	public static final Point noAxisTransformDataToPixel(Point2D dataPoint, Rectangle2D axisRect, Rectangle2D componentRect)
	{
		double x = dataPoint.getX() * componentRect.getWidth();
		double y = componentRect.getHeight() - dataPoint.getY() * componentRect.getHeight();
		return new Point((int)x, (int)y);
	}
	
	public static final Rectangle noAxisTransformDataToPixel(Rectangle2D dataRect, Rectangle2D axisRect, Rectangle2D componentRect)
	{
		Point2D lowerLeft = new Point2D.Double(dataRect.getX(), dataRect.getY());
		Point2D upperRight = new Point2D.Double(dataRect.getX() + dataRect.getWidth(), dataRect.getY() + dataRect.getHeight());
		Point llp = noAxisTransformDataToPixel(lowerLeft, axisRect, componentRect);
		Point urp = noAxisTransformDataToPixel(upperRight, axisRect, componentRect);
		
		return new Rectangle(llp.x, urp.y, urp.x - llp.x, llp.y - urp.y);
	}
}
