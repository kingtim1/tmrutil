package tmrutil.graphics.plotx;

import java.awt.Point;

/**
 * A coordinate system translates from a space <code>P</code> to a 2D pixel coordinate.
 * @author Timothy A. Mann
 *
 */
public interface RenderCoordinateTranslator<P>
{
	/**
	 * Translates the coordinate <code>coord</code> to a 2D pixel coordinate.
	 * @param coord a coordinate
	 * @return a pixel coordinate
	 */
	public Point translate(P coord);
}
