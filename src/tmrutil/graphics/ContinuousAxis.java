package tmrutil.graphics;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a continuous axis of a plot.
 * 
 * @author Timothy Mann
 * 
 */
public class ContinuousAxis extends Axis
{
	/**
	 * The minimum value of the axis.
	 */
	private double _min;

	/**
	 * The maximum value of the axis.
	 */
	private double _max;

	/**
	 * Constructs a continuous axis over the range [0,1].
	 * 
	 * @param label
	 *            the axis label
	 */
	public ContinuousAxis(String label)
	{
		super(label);
		_min = 0.0;
		_max = 1.0;
	}

	/**
	 * Constructs a continuous axis over the range [<code>min</code>,
	 * <code>max</code>].
	 * 
	 * @param label
	 *            the axis label
	 * @param min
	 *            the minimum axis value
	 * @param max
	 *            the maximum axis value
	 * @throws IllegalArgumentException
	 *             if <code>min &gt; max</code>
	 */
	public ContinuousAxis(String label, double min, double max)
			throws IllegalArgumentException
	{
		super(label);
		setRange(min, max);
	}

	/**
	 * The minimum axis value.
	 * 
	 * @return minimum axis value
	 */
	public double getMin()
	{
		return _min;
	}

	/**
	 * The maximum axis value.
	 * 
	 * @return maximum axis value
	 */
	public double getMax()
	{
		return _max;
	}

	/**
	 * Sets the range of this axis.
	 * 
	 * @param min
	 *            the new minimum axis value
	 * @param max
	 *            the new maximum axis value
	 * @throws IllegalArgumentException
	 *             if <code>min &gt; max</code>
	 */
	public void setRange(double min, double max)
			throws IllegalArgumentException
	{
		if (min > max) {
			throw new IllegalArgumentException(
					"Argument min cannot be greater than max : min(" + min
							+ ") > max(" + max + ")");
		}
		_min = min;
		_max = max;
		axisChanged();
	}

	/**
	 * Helper function for computing the 2D painting coordinate for a real value
	 * given this axis.
	 * 
	 * @param x
	 *            a real value
	 * @param pixelLength
	 *            the length of the axis in pixels
	 * @param bias
	 *            the offset of the axis
	 * @param vertical
	 *            true if the axis is vertical, otherwise, assume horizontal
	 * @return the coordinate to paint <code>x</code> at
	 */
	public int compute2DCoord(double x, int pixelLength, int bias,
			boolean vertical)
	{
		double diff = _max - _min;
		int coord = (int) (((x - _min) / diff) * pixelLength);
		if (vertical) {
			return bias + (pixelLength - coord);
		} else {
			return bias + coord;
		}
	}

	/**
	 * Helper function for computing all of the ticks to paint.
	 */
	public List<Double> getTicks()
	{
		List<Double> ticks = new ArrayList<Double>();
		double diff = _max - _min;
		double l10 = Math.log(diff) / Math.log(10);
		double power = 0;
		power = Math.floor(l10);
		double inc = Math.pow(10, power);
		if(diff < 1.9*inc){
			inc = (Math.pow(10, power)*0.1);
		}
		double mintick = ((int) (_min / inc)) * inc;
		if (inc > 0) {
			for (double ptick = mintick; ptick < _max; ptick += inc) {
				ticks.add(ptick);
			}
		}
		return ticks;
	}
}
