/*
 * Plot.java
 */

/* Package */
package tmrutil.graphics;

/* Imports */
import gnu.jpdf.PDFJob;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Destination;
import javax.swing.JComponent;
import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;
import tmrutil.util.TextFileUtil;

/**
 * Represents a graphical plot.
 */
public abstract class Plot extends JComponent
{

	private static final long serialVersionUID = -5845896234436066561L;

	public static final DecimalFormat DEFAULT_DECIMAL_FORMAT = new DecimalFormat(
			"#####.#####;-#####.#####");

	public static final double TITLE_X = 1.0;
	public static final double TITLE_Y = 0.07;

	public static final int DEFAULT_DPI = 72;
	public static final int DEFAULT_PLOT_WIDTH = 640;
	public static final int DEFAULT_PLOT_HEIGHT = 480;
	public static final Color CLEAR = new Color(0, 0, 0, 0);

	/** The default font used when none is specified. */
	private static Font DEFAULT_PLOT_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 14);

	/**
	 * Sets the default plot font to a specified font.
	 * 
	 * @param font
	 *            a font
	 */
	public static final void setDefaultPlotFont(Font font)
	{
		DEFAULT_PLOT_FONT = font;
	}

	/**
	 * Retrieves the default plot font.
	 * 
	 * @return the default plot font
	 */
	public static final Font getDefaultPlotFont()
	{
		return DEFAULT_PLOT_FONT;
	}

	/** The title of the plot. */
	private String _title;
	/** The font used when no other font is specified. */
	private Font _plotFont;
	/** The font used for the title. */
	private Font _titleFont;
	
	/**
	 * Constructs an empty plot with no title and the default font.
	 */
	public Plot()
	{
		this(DEFAULT_PLOT_FONT, DEFAULT_PLOT_FONT);
	}
	
	/**
	 * Constructs an empty plot with no title with a specified plot font and title font.
	 * @param plotFont the font used for text when no other font is specified
	 * @param titleFont the font used for the title
	 */
	public Plot(Font plotFont, Font titleFont)
	{
		this("", plotFont, titleFont);
	}
	
	/**
	 * Constructs an empty plot with a title and specified plot font and title font.
	 * @param plotFont the font used for text when no other font is specified
	 * @param titleFont the font used for the title
	 */
	public Plot(String title, Font plotFont, Font titleFont){
		setTitle(title);
		_plotFont = plotFont;
		_titleFont = titleFont;
	}

	/**
	 * Sets the title of this plot to specified string.
	 * 
	 * @param title
	 *            the desired title string
	 */
	public void setTitle(String title)
	{
		_title = title;
	}

	/**
	 * Returns the current title of this plot.
	 * 
	 * @return the title of this plot
	 */
	public String getTitle()
	{
		return _title;
	}
	
	/**
	 * Returns the font used when no other font is specified.
	 * @return the current plot font
	 */
	public Font getPlotFont()
	{
		return _plotFont;
	}
	
	/**
	 * Sets the font that is used when no other font is specified.
	 * @param font a font
	 */
	public void setPlotFont(Font font)
	{
		_plotFont = font;
	}
	
	/**
	 * Returns the font used for rendering the plot title.
	 * @return the font used for rendering the plot title
	 */
	public Font getTitleFont()
	{
		return _titleFont;
	}
	
	/**
	 * Sets the font used for rendering the plot title.
	 * @param font a font
	 */
	public void setTitleFont(Font font)
	{
		_titleFont = font;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setFont(DEFAULT_PLOT_FONT);
		paintPlot(g, getWidth(), getHeight());
	}

	/**
	 * Paints the plot using the provided graphics object.
	 * 
	 * @param g
	 *            a graphics object
	 * @param width
	 *            the width of the painting surface
	 * @param height
	 *            the height of the painting surface
	 */
	public abstract void paintPlot(Graphics g, int width, int height);

	/**
	 * Writes plot image data to a PNG file.
	 * 
	 * @param filename
	 * @throws IOException
	 */
	public void toPNG(String filename) throws IOException
	{
		BufferedImage image = new BufferedImage(getWidth(), getHeight(),
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) image.getGraphics();
		g2.setFont(DEFAULT_PLOT_FONT);
		paintPlot(g2, DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		ImageIO.write(image, "png", new File(filename));
	}

	/**
	 * Writes plot image data to a PDF file.
	 * 
	 * @param filename
	 * @throws IOException
	 */
	public void toPDF(String filename) throws IOException
	{
		PDFJob pdfJob = new PDFJob(new FileOutputStream(filename));
		PageFormat pformat = new PageFormat();
		pformat.setPaper(new PlotPaper());
		Graphics2D g2 = (Graphics2D) pdfJob.getGraphics(pformat);
		g2.setFont(DEFAULT_PLOT_FONT);
		paintPlot(g2, DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		g2.dispose();
		pdfJob.end();
	}

	/**
	 * Writes plot image data to an EPS file.
	 * 
	 * @param filename
	 * @throws IOException
	 */
	public void toEPS(String filename) throws IOException
	{
		Graphics2D g2 = new EpsGraphics("EPS Plot", new FileOutputStream(
				filename), 0, 0, DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT,
				ColorMode.COLOR_RGB);
		paintPlot(g2, DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
		g2.dispose();
	}

	/**
	 * Writes plot image data to a PostScript file.
	 * 
	 * @param filename
	 * @throws IOException
	 */
	public void toPS(String filename) throws IOException
	{
		Printable printable = new Printable() {
			public int print(Graphics g, PageFormat format, int pageIndex)
			{
				if (pageIndex == 0) {
					Graphics2D g2 = (Graphics2D) g;
					g2.setFont(DEFAULT_PLOT_FONT);
					g2.translate(format.getImageableX(), format.getImageableY());
					g2.setClip(0, 0, DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
					double width = format.getImageableWidth();
					double height = format.getImageableHeight();
					double xRatio = width / DEFAULT_PLOT_WIDTH;
					double yRatio = height / DEFAULT_PLOT_HEIGHT;
					if (xRatio < yRatio) {
						g2.scale(xRatio, xRatio);
					} else {
						g2.scale(yRatio, yRatio);
					}

					paintPlot(g2, DEFAULT_PLOT_WIDTH, DEFAULT_PLOT_HEIGHT);
					return Printable.PAGE_EXISTS;
				}
				return Printable.NO_SUCH_PAGE;
			}
		};

		PageFormat format = new PageFormat();
		// format.setOrientation(PageFormat.LANDSCAPE);

		Destination dest = new Destination(new File(filename).toURI());
		PrintRequestAttributeSet aSet = new HashPrintRequestAttributeSet();
		aSet.add(dest);
		PrinterJob pJob = PrinterJob.getPrinterJob();
		pJob.setPrintable(printable, format);
		try {
			// This writes the PostScript to a file
			pJob.print(aSet);

			// Add bounding box information which is necessary for embedding the
			// PostScript in Latex documents.
			// The arguments for the bounding box are :
			// bottom left x, bottom left y, top right x, top right y
			TextFileUtil.insertLineIntoFile("%%BoundingBox: 70 360 545 720", 1,
					filename);
			// We also have to remove instances of setpagedevice so that it
			// works with Latex
			TextFileUtil.findReplace("setpagedevice", "% setpagedevice",
					filename);

		} catch (PrinterException ex) {
			System.err.println("A printer exception occurred!");
			ex.printStackTrace();
		}
	}

	private static class PlotPaper extends Paper
	{
		public PlotPaper()
		{
			super();
			setImageableArea(0, 0, Plot.DEFAULT_PLOT_WIDTH,
					Plot.DEFAULT_PLOT_HEIGHT);
		}

		@Override
		public double getHeight()
		{
			return Plot.DEFAULT_PLOT_HEIGHT;
		}

		@Override
		public double getWidth()
		{
			return Plot.DEFAULT_PLOT_WIDTH;
		}
	}
}